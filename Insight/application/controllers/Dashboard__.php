<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model('Dashboard_m');
        $this->load->library('Excel'); //load PHPExcel library 
         $this->load->library('SWpt'); 
        error_reporting(0);

        $valid_login = $this->session->userdata('user_id');
        if ($valid_login == '') {
            redirect('Welcome');
            // echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Welcome'</script>";
        }
    }

    
    public function index() {

        $this->load->view('index');
    }
    
    
    public function abc() {
        if ($_FILES['file']['name']) {

            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $_FILES['file']['name'];
            $file_size = $_FILES['file']['size'];
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_type = $_FILES['file']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['file']['name'])));
            $expensions = array("xls", "xlsx");
            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                $this->session->set_flashdata('per_message', 'File Format Not Suppoted');
                redirect('Dashboard/dashboard', 'refresh');
            } else {
                move_uploaded_file($file_tmp, $file_name);
                $data['file'] = $file_name;
            }
        }
        $this->demodata($file_name);
    }

     public function demodata($par1) {
        $uid = $this->session->userdata('user_id');
        $inputfilename = $par1;
        $inputfiletype = PHPExcel_IOFactory::identify($inputfilename);
        $objReader = PHPExcel_IOFactory::createReader($inputfiletype);
        $objPHPExcel = $objReader->load($inputfilename);
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
         
        //print_r($highestRow);print_r($highestColumn);die;
        
         $r1 = 2;
         $rowData = $sheet->rangeToArray('A' . $r1 . ':' . $highestColumn . $r1, NULL, TRUE, FALSE);
         
        foreach($rowData as $rd){
             $count_col = count($rd);
              $new_row =  array_filter($rd);
            }
            
            $library = $this->db->get_where('library', array('name' => $new_row[0], 'creator' => $uid))->result_array();
           
            $grp_name = $this->db->get_where('groups',array('name'=>$new_row[1]))->result_array();
       
           
          
            if ($library[0]['library_id'] == '') {
                $result_lib = array(
                            'name' => $new_row[0],
                            'group_name' => $grp_name[0]['id'],
                            'discription' => $new_row[2],
                            'access_type' =>  $new_row[3],
                            'creator' => $uid,
                            'status' => $new_row[4],
                            'date' => date('Y-m-d'),
                            'color' => $new_row[5]
                            );
            
            
            
         $r2 = 3;
           $colData = $sheet->rangeToArray('C' . $r2 . ':' . $highestColumn . $r2, NULL, TRUE, FALSE);
         
            foreach($colData as $cd)
            {
              foreach($cd as $c){
                   if($c != "Type")
                   {
                       $colname[] = $c;
                   }
              }
             
            }
            
           $check_dup  = $this->has_dupes($colname);
            if($check_dup)
            {
                
                  $this->session->set_flashdata('error_msg','Column Name Must be Different');
                   redirect('index.php/Dashboard/dashboard');
            }
            else
            {
               
                  $this->db->insert('library',$result_lib);
                
                  $library_id = $this->db->insert_id();
            }
          
       for($r3  = 4; $r3<=$highestRow ; $r3++)
       {
           $valueData = $sheet->rangeToArray('A' . $r3 . ':' . $highestColumn . $r3, NULL, TRUE, FALSE);
         
          $i = 1;
          foreach($valueData as $key=>$vd)
          {
              $count_col = count($vd);
//              $rowResult = array(
//                                    'row_name'=> $vd[0],
//                                    'sub'=> $vd[1]
//              );
              
               $rowResult = array('row_col_name' => $vd[0],
                                   'substitute' => $vd[1],
                            'library_id' => $library_id);

                        if($this->db->insert('library_info', $rowResult))
                        {
                          $count_arr[] = "1";
                             $row_id = $this->db->insert_id();
                        }
                       
             
               $index = 0;
              for($col_no = 2 ; $col_no <= $count_col ; $col_no++ )
              {
                  if($col_no % 2 == 0){
                       $type = $vd[$col_no+1];
                    $col_name = $colname[$index];
                    $type_data  = $this->db->get_where('column_type',array('name'=>$type))->result_array();
                    
                    
                      if($type_data[0]['type'] == "4")
                      { $col_value  = $vd[$col_no];
                        $value_type = $type_data[0]['id'];
                         $check = "1";
                      }
                      if($type_data[0]['type'] == "3"){
                          $data = $this->db->get_where('column_type_values',array('value'=>$vd[$col_no]))->result_array();
                         
                          if(count($data) != 0){
                          $col_value = $data[0]['id'];
                           $value_type = $type_data[0]['id'];
                           $check = "1";
                          
                          }
                          else
                          {
                              
                              $invalid_data[] = $vd[col_no];
                              $row_array[] = $row_id;
                          }
                      }
                      if($type_data[0]['type'] == "2")
                      {
                            $website = $this->test_input($vd[$col_no]);
                           // check if URL address syntax is valid
                            if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
//                                $this->session->set_flashdata('flash_message', 'Invalid URL');
//                                redirect('index.php/Dashboard/dashboard', 'refresh');
                                  $invalid_data[] = $vd[$col_no]; 
                                  $row_array[] = $row_id;
                                  $check = "0";
                            }
                            else
                            {
                                $col_value  = $vd[$col_no];
                                 $value_type = $type_data[0]['id'];
                                $check = "1";
                            }
                      }
                      if($type_data[0]['type'] == "1")
                      {
                                
                                 
                                 $dd = date_format($vd[$col_no], "m/d/Y");
                                    $date1 = new DateTime($dd);
                                   $lab_date = $date1->format('m/d/Y');
                                    $col_value  = $lab_date;
                                 $value_type = $type_data[0]['id'];
                                 $check= "1";
                      }
                      
                  if($check == "1")
                     {
                         $coldata = array(
                                'library_id' => $library_id,
                                'row_id' => $row_id,
                                'row_col_name' => $col_name,
                                'value' => $col_value,
                                'value_type'=> $value_type,
                                'user_Id' => $uid,);
                            $this->db->insert('library_info', $coldata);
                            $col_id = $this->db->insert_id();
                             //$this->db->last_query();
                     }
                     
                    
                $index++;
                  }
                  
                   
              }
              
            
          }
       }
      
        if(count($row_array) != 0 ){
               $del = 0;
                foreach($row_array as $ra)  
                {  $this->db->where('id',$ra);
                   if($this->db->delete('library_info'))
                   {
                       $count_del[] = "2";
                   }
                  $this->db->where('row_id',$ra);
                  $this->db->delete('library_info');
                }
                
                if(count($count_arr) == count($count_del))
                {
                     $this->db->where('library_id',$library_id);
                  $this->db->delete('library'); 
                   $this->session->set_flashdata('error_msg', 'Invalid data in all columns,therefore Library is deleted');
                   redirect('index.php/Dashboard/dashboard', 'refresh');
                }
              }
              

              
             
           $this->session->set_flashdata('flash_message', 'Record inserted Successfully');
                   redirect('index.php/Dashboard/dashboard', 'refresh');
             
            }
            else
            {
                 $this->session->set_flashdata('error_msg', 'Record already exists');
                   redirect('index.php/Dashboard/dashboard', 'refresh');
            }
     
    // print_r($colname);
     
   //  print_r($valueData);
    
     //print_r($rowResult);
     
     //print_r($result_lib);
     
    // die;
        
        
 
    }
   
    public  function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

  public  function has_dupes($array){
 $dupe_array = array();
 foreach($array as $val){
  if(++$dupe_array[$val] > 1){
   return true;
  }
 }
 return false;
}

    public function dashboard() {


        $data['groups'] = $this->Dashboard_m->getall_groups();
        $data['libraries'] = $this->Dashboard_m->getall_library();

        $uid = $this->session->userdata('user_id');
        $data['libraybyid'] = $this->Dashboard_m->get_librarybyid($uid);
        $data['banner'] = $this->db->get('tblgeneralsetting')->result_array();

        $data['menu_value'] = "Home";
        $this->load->view('dashboard', $data);
    }

    public function newlibrary() {
        $data['groups'] = $this->Dashboard_m->getall_groups();
        $data['country'] = $this->db->get('country')->result_array();
        $data['state'] = $this->db->get('state')->result_array();
         $data['banner'] = $this->db->get('tblgeneralsetting')->result_array();
        //$this->load->view('header');
        $this->load->view('addnewlibrary', $data);
    }

    public function demo() {
        $this->load->view('demo');
    }

    public function email($param1 = '') {
        //echo $param1;die;
        $data['groups'] = $param1;
        $data['banner'] = $this->db->get('tblgeneralsetting')->result_array();
        $this->load->view('emailtemplate1', $data);
    }

    public function invite_user() {
        //echo $param1;'die';
        $var1 = $this->input->post('mail1');


        $library = $this->input->post('library');
        $sender = $this->input->post('subject');
        $subject = $this->input->post('sender');
        $content1 = $this->input->post('content');
        $content2 = trim($content1, '<p> ');
        $content = trim($content2, '</p> ');
        $date = date('Y-m-d');
        $user_id = $this->session->userdata('user_id');
        $notification = '1';

        if ($var1) {
            foreach ($var1 as $res) {
                if ($res != '') {
                   // echo "<br>".$res."<br>";
//                    $config = Array(
//                        'charset' => 'utf-8',
//                        'wordwrap' => TRUE,
//                        'mailtype' => 'html',
//                    );
//                    $this->load->library('email', $config);
//                    $this->email->from($sender);
//                    $this->email->to($res);
//                    $this->email->subject($subject);
//                    $this->email->message("Hello Mr/Mrs $res 
//                             \n $content ");
//                    $mail_success = $this->email->send();
//                    
                    
                    $to  = $res;
                    $from = $sender;  
                    $subject1 = $subject;
                    
                    $message = "Hello Mr/Mrs $res \n $content"; 
                     $header = "From:$from \r\n";
		     $header .= "MIME-Version: 1.0\r\n";
                     $header .= "Content-type: text/html\r\n";       
                            
                   
                   // if($mail_success){
                    if (mail($to,$subject1,$message,$header)) {
                     $send = '1';
                    } else {
                     $send = '0';
                    }
                    $insert_data = array(
                        'user_email' => $res,
                        'sender' => $sender,
                        'subject' => $subject,
                        'content' => $content,
                        'library_id' => $library,
                        'user_id' => $user_id,
                        'notification' => $notification,
                        'date' => $date,
                        'email_send' => $send
                    );
                    $this->db->insert('invite_user', $insert_data);
                    
                   // echo $this->db->last_query();

                }
                
            }
                    $this->session->set_flashdata('flash_message', 'User Invited Successfully');
                    redirect('index.php/Dashboard/dashboard', 'refresh');
        } else {
            $this->session->set_flashdata('pre_message', 'User Invited Unsuccessfully');
            redirect('index.php/Dashboard/dashboard', 'refresh');
        }
    }

    public function edit_profile($id) {

        $data['userinfo'] = $this->Dashboard_m->getall_userbyId($id);
        $data['banner'] = $this->db->get('tblgeneralsetting')->result_array();
        $data['menu_value']="edit_profile";
        $this->load->view('edit_profile', $data);
    }

    public function change_pass() {
        $data['menu_value'] = "change";
        $data['banner'] = $this->db->get('tblgeneralsetting')->result_array();
        $this->load->view('change_password',$data);
    }

    public function change_password() {
        $uid = $this->session->userdata('user_id');
        $oldpassword = $this->input->post('old_password');
        $newpassword = $this->input->post('new_password');
        $confirmpassword = $this->input->post('confirm_password');
        $data = array(
            'user_password' => $this->input->post('new_password')
        );



        $query = $this->db->get_where('user', array('user_id' => $uid))->result_array();

        if (($query[0]['user_password'] == $oldpassword) && $newpassword) {
            $this->db->where('user_id', $uid);
            $this->db->update('user', $data);
            $this->session->set_flashdata('flash_message', 'password Updated Successfully');
            //redirect('Dashboard/change_pass', 'refresh');
            echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Dashboard/change_pass'</script>";
        } else {
            $this->session->set_flashdata('permission_message', 'Current Password did not match');
            //redirect('Dashboard/change_pass', 'refresh');
            echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Dashboard/change_pass'</script>";
        }
    }

    public function save_library() {

        $name = $this->input->post('library_name');
        $group = $this->input->post('group_name');

        $description = $this->input->post('description');
        $access_type = $this->input->post('access_type');
        $uid = $this->session->userdata('user_id');
        $color = $this->input->post('color');
        $data = array(
            'name' => $name,
            'group_name' => $group,
            'discription' => $description,
            'access_type' => $access_type,
            'creator' => $uid,
            'status' => 'active',
            'date' => date("Y-m-d"),
            'color' => $color,
        );
        if ($this->db->insert('library', $data)) {

            $insert_id = $this->db->insert_id();
            $colname = $this->input->post('colname');
             $coltype = $this->input->post('coltype');
            $rowname = $this->input->post('rowname');
            $search_name = $this->input->post('search_name');
           // $mail_list = $this->input->post('mail1');
            
           //  print_r($colname);hh
            $i1=0;
            foreach ($rowname as $key1 => $row) {

                $rowdata = array('row_col_name' => $row,
                    'substitute'=> $search_name[$key1],
                    'library_id' => $insert_id);
               $this->db->insert('library_info', $rowdata);
                
              // print_r($rowdata);
                $row_id = $this->db->insert_id();

                $counter = 1;
                foreach ($colname as $key2 => $col) {
                    
                   // echo $col;
                    $type = $col . "valtype";
                    $typename = $this->input->post($type);
                  $b = $coltype[$key2];

                  $val = $col . "val";
                  $val21="valrowapp".$i1.$counter;
                    $colval = $this->input->post($val);
                  //  print_r($colval);
              //  echo '<pre>';   print_r($_FILES);
                    if ($_FILES[$val21]['name']) {
                       // $er=0;
                        foreach ($_FILES[$val21]['name'] as $key => $dd) {

                            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                            $file_name = $random . $dd;
                            $file_size = $_FILES[$val21]['size'];
                            $file_tmp = $_FILES[$val21]['tmp_name'];

                            $file_type = $_FILES[$val21]['type'];
                            $file_ext = strtolower(end(explode('.', $dd)));
                            $expensions = array("jpg", "gif", "png", "jpeg");
                            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                                $this->session->set_flashdata('per_message', 'File Format Not Suppoted');
                                // redirect('Dashboard/newlibrary', 'refresh');
                            } else {
                                
                               
                                
                                $aa = move_uploaded_file($_FILES[$val21]['tmp_name'][$key], "uploads/library/" . $file_name);

                                $data[$val] = $file_name;
                                
                               // echo $er;
                           
                                $a21 = $file_name;
                              
                               
                            }
                           // $j++; $er++;
                        }
                    }
                    
                   
               //    print_r($a1);
                    
                   
                    foreach ($colval as $key3 => $value) {
                        $va = $value[$key3];
                    }

                
                    
                
                    if ($_FILES[$val21]['name'][0]) {

                    $a=$a21;
                       
                  }
                   else
                    {
                        $a = $colval[$key1];
                        
                         
                    }
                   
$coldata= array(
                        'library_id' => $insert_id,
                        'row_id' => $row_id,
                        'row_col_name' => $col,
                        'value' => $a,
                        'value_type' => $b,
                        'user_Id' => $uid,
                    );
                 
                    $this->db->insert('library_info', $coldata);
//                    if ($colval[$key1]) {
//
//                     $a = $colval[$key1];
//                    }
                    
                   // echo $this->db->last_query();
                    $counter++;
                } $i1++;
            }
              
//               $we=0;
//                    foreach($coldata as $key=>$coldata1)
//                    {
//                        if($coldata[$key]['value'])
//                        {
//                            
//                        }else
//                        {
//                      $coldata[$key]['value']=$a1[$we];
//                       $we++; }
//                       $this->db->insert('library_info', $coldata[$key]);
//                       
//                      // echo $this->db->last_query();
//                    }
             
            
             $var1 = $this->input->post('mail1');

        if ($var1) {
        $library = $insert_id;
       // $sender = $this->input->post('sender');
        $subject = $this->input->post('subject');
        $content1 = $this->input->post('content');
        $content2 = trim($content1, '<p> ');
        $content = trim($content2, '</p> ');
        $date = date('Y-m-d');
        $user_id = $this->session->userdata('user_id');
        $notification = '1';
       
        $mail_data = $this->db->get('mail_settings')->result_array();
       
            foreach ($var1 as $res) {
                if ($res != '') {
                 
                $to = $res;
$from = $mail_data[0]['username'];
$subject = $subject;
$body =  $content;
//
	include('swiftmailer/swift_required.php' );
	$mailer = new Swift_Mailer(new Swift_MailTransport()); 
//
//
	$smpt=$mail_data[0]['smtp_server'];
		$port=$mail_data[0]['port'];
		$username= $mail_data[0]['username'];
		$password=$mail_data[0]['password'];
			
		@$transport = Swift_SmtpTransport::newInstance($smpt, $port)
		->setUsername($username)
		->setPassword($password);
		$mailer = Swift_Mailer::newInstance($transport);

	 $to = $to;
	$from = $from;
	$subject = $subject;
	$body = stripslashes( $body );
	
	@$message = Swift_Message::newInstance()
	->setSubject($subject) 
	->setFrom($from) 
	->setTo($to)
	->setContentType("text/html; charset=UTF-8")
	->setBody($body, 'text/html');
        //echo $mailer->send($message);
	if($mailer->send($message))
        {
           $send='1';
        }
        else
        {
          $send='0';
        }
                    $insert_data = array(
                        'user_email' => $res,
                        'sender' => $from,
                        'subject' => $subject,
                        'content' => $content,
                        'library_id' => $library,
                        'user_id' => $user_id,
                        'notification' => $notification,
                        'date' => $date,
                        'email_send' => $send
                    );
                    $this->db->insert('invite_user', $insert_data);
                    
                   // echo $this->db->last_query();

                }
            
            }
        }
        
      // die;
          $this->session->set_flashdata('flash_message', 'Library created Successfully');
            //echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Dashboard/newlibrary'</script>";
            redirect('index.php/Dashboard/dashboard', 'refresh');
        }
    }
        
    
    public function save_user($id = '') {


        if ($id != '') {

            $update = array(
                'username' => $this->input->post('username'),
                'user_firstname' => $this->input->post('first_name'),
                'user_lastname' => $this->input->post('last_name'),
                'user_email' => $this->input->post('email'),
                'user_DOB' => $this->input->post('dob'),
                'user_zipcode' => $this->input->post('zip'),
                'status' => 'active',
                'user_occupation' => $this->input->post('occupation'),
                'user_gender' => $this->input->post('gen'),
            );
            $this->db->where('user_id', $id);
            $resultdata = $this->db->update('user', $update);
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                redirect('index.php/Dashboard/dashboard', 'refresh');
                //echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Dashboard/dashboard'</script>";
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted Successfully');
                //echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Dashboard/dashboard'</script>";
                redirect('index.php/Dashboard/dashboard', 'refresh');
            }
        }
    }

    public function delete_library() {
        $id = $this->input->post('id');
        $this->db->where('library_id', $id);
        $this->db->delete('library');
        $this->db->where('library_id', $id);
        $this->db->delete('library_info');
        $this->session->set_flashdata('flash_message', 'Library  Deleted Successfully');
        redirect('index.php/Dashboard/dashboard', 'refresh');
        // echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Dashboard/dashboard'</script>";
    }

    public function edit_library($id) {


        $data['country'] = $this->db->get('country')->result_array();
        $data['state'] = $this->db->get('state')->result_array();
        $data['groups'] = $this->Dashboard_m->getall_groups();
        $data['libraybyid'] = $this->Dashboard_m->get_libraryedit($id);
        
  
   // echo '<pre>'; print_r($data['libraybyid']);
   //  die;
         $data['banner'] = $this->db->get('tblgeneralsetting')->result_array();
        $this->load->view('edit_library', $data);
    }

    
    
    
    
    
    
    
  public function save_Edit($id) {

        $name = $this->input->post('library_name');
        $group = $this->input->post('group_name');

        $description = $this->input->post('description');
        $access_type = $this->input->post('access_type');
        $uid = $this->session->userdata('user_id');

        $updatedata = array('name' => $name,
            'name' => $name,
            'group_name' => $group,
            'discription' => $description,
            'access_type' => $access_type,
            'creator' => $uid,
            'status' => 'active',
            'color' => $this->input->post('color')
        );
        $this->db->where('library_id', $id);
        $resultdata = $this->db->update('library', $updatedata);
        $colname = $this->input->post('colname');
        $rowname = $this->input->post('rowname');
        $search_name = $this->input->post('search_name');
        $coltype = $this->input->post('coltype');
        
    $this->db->where('library_id', $id);
     $this->db->delete('library_info');
        $insert_id = $id;
        $colname = $this->input->post('colname');
       
        $rowname = $this->input->post('rowname');
       
      $i=0; $lal=0;
        foreach ($rowname as $key1 => $row) {

            $rowdata = array('row_col_name' => $row,
                  'substitute'=> $search_name[$key1],
                'library_id' => $insert_id);
     $this->db->insert('library_info', $rowdata);
            $row_id = $this->db->insert_id();

           $counter = 1; $calu=1; 
            foreach ($colname as $key2 => $col) {
                
                
                $type = $col . "valtype";
                $typename = $this->input->post($type);
                 $b = $coltype[$key2];
              $val=$col."val";
                $val21 ="valrowapp".$lal.$calu;
                $colval = $this->input->post($val);

                $val3 = "val3rowapp".$lal.$calu;
                $img = $this->input->post($val3);
            
             if($img[0]!='')
             {
                 $ss21=$img[0];
             }else
             {
                 //image 
             
                 
                 if ($_FILES[$val21]['name']) {
                    foreach ($_FILES[$val21]['name'] as $key => $dd) {

                        $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                        $file_name = $random . $dd;
                        $file_size = $_FILES[$val]['size'];
                        $file_tmp = $_FILES[$val]['tmp_name'];

                        $file_type = $_FILES[$val]['type'];
                        $file_ext = strtolower(end(explode('.', $dd)));
                        $expensions = array("jpg", "gif", "png", "jpeg");
                        if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                            $this->session->set_flashdata('per_message', 'File Format Not Suppoted');
                            //redirect('Dashboard/newlibrary', 'refresh');
                        } else {
                       
                        
                         
                            $data[$val21] = $file_name;
                    
                            $aa = move_uploaded_file($_FILES[$val21]['tmp_name'][$key], "uploads/library/" . $file_name);
                    $ss21= $file_name; 
                        }
                        $j++;
                  }
                }
                 
             }
             // echo  $_FILES[$val]['name'];
                
                
               

                foreach ($colval as $key3 => $value)
                {
                    $va = $value[$key3];
                }



                if ($colval[$key1]) {

                    $a = $colval[$key1];
                    
             
              
                } else {

                     $a = $ss21;
                
                    }
             
                
                
                //echo $a; 
                $coldata= array(
                    'library_id' => $insert_id,
                    'row_id' => $row_id,
                    'row_col_name' => $col,
                    'value' => $a,
                    'value_type' => $b,
                    'user_Id' => $uid,
                );
//echo '<pre>';
         // print_r($coldata);
              $this->db->insert('library_info', $coldata);
                
            
              $counter++;
           $i++;  
           
              $calu++;  }
            
            
            
            
    $lal++;   }
    
    
        // die;
    
    
          
            
        
            
            
          
     //  echo '<pre>'; print_r($coldata); die;
            
             $var1 = $this->input->post('mail1');
        //echo "fdgdfgh";   print_r($var1); die;
        if ($var1) {
        $library = $id;
        $sender = $this->input->post('sender');
        $subject = $this->input->post('subject');
        $content1 = $this->input->post('content');
        $content2 = trim($content1, '<p> ');
        $content = trim($content2, '</p> ');
        $date = date('Y-m-d');
        $user_id = $this->session->userdata('user_id');
        $notification = '1';
       
        $mail_data = $this->db->get('mail_settings')->result_array();
       
            foreach ($var1 as $res) {
                if ($res != '') {
              $to = $res;
                $from = $mail_data[0]['username'];
                $subject = $subject;
                $body =  $content;
//
	include('swiftmailer/swift_required.php' );
	$mailer = new Swift_Mailer(new Swift_MailTransport()); 
//
//
	$smpt=$mail_data[0]['smtp_server'];
		$port=$mail_data[0]['port'];
		$username= $mail_data[0]['username'];
		$password=$mail_data[0]['password'];
			
		@$transport = Swift_SmtpTransport::newInstance($smpt, $port)
		->setUsername($username)
		->setPassword($password);
		$mailer = Swift_Mailer::newInstance($transport);

	 $to = $to;
	$from = $from;
	$subject = $subject;
	$body = stripslashes( $body );
	
	@$message = Swift_Message::newInstance()
	->setSubject($subject) 
	->setFrom($from) 
	->setTo($to)
	->setContentType("text/html; charset=UTF-8")
	->setBody($body, 'text/html');
     // $mailer->send($message);
	if($mailer->send($message))
        {
            // echo "yes";
                  $send = '1';
                    } else {
                    $send = '0';
                    }
                    $insert_data = array(
                        'user_email' => $res,
                        'sender' => $from,
                        'subject' => $subject,
                        'content' => $content,
                        'library_id' => $library,
                        'user_id' => $user_id,
                        'notification' => $notification,
                        'date' => $date,
                        'email_send' => $send
                    );
                    
                  
                    $this->db->insert('invite_user', $insert_data);
                    
                   // echo $this->db->last_query();

                }
            
            }
      
      //echo "<script>window.location='http://pueobusinesssolutions.com/BetaInsight/index.php/Dashboard/dashboard'</script>";
    }
      
        $this->session->set_flashdata('flash_message', 'Library Edited Successfully');
        redirect('index.php/Dashboard/dashboard', 'refresh');
   }

    public function filter_group() {

        $gid = $this->input->post('grp');
        if ($gid == '') {
            $this->db->where('creator', $this->session->userdata('user_id'));

            $this->db->select('*');
            $queryuser = $this->db->get('library');
            if ($queryuser->num_rows() > 0) {
                foreach (($queryuser->result()) as $rowlib) {
                    $this->db->where('id', $rowlib->group_name);
                    $this->db->select('*');
                    $query2 = $this->db->get('groups')->result_array();
                    foreach ($query2 as $namedata) {
                        $rowlib->groupName = $namedata['name'];
                    }
                    ?>
                    <tr>
                        <td style="width:15%;"><?php echo $rowlib->name; ?></td>
                        <td style="width:15%;"><?php echo $rowlib->groupName; ?></td>
                        <td style="width:25%;"><?php echo $rowlib->discription; ?> </td>
                        <td style="width:15%;"><?php echo $rowlib->date; ?></td>
                        <td style="width:12%;"><input type="text" style="text-align:center;width:80px;color:white;background-color:#<?php echo $rowlib->color; ?>; " disabled=""  value="<?php //echo $lib->color;   ?>"></td>
                        <td style="width:12%;"><?php echo $rowlib->access_type; ?></td>
                        <td style="width:12%;"><?php 
    //                                               if($lib['user_id']){
    //                                                $subscriber = $this->db->get_where("library", array('user_id' => $lib['user_id']))->result_array(); 
    //                                                echo count($subscriber);
    //                                               }
                         $subscriber = $this->db->get_where('library',array('name'=>$rowlib->name,'subscribe_status'=>"subscribe"))->result_array();
                         //echo $this->db->last_query();
                         echo count($subscriber);
                        ?></td> 
                        <td style="width:25%;"><?php echo $rowlib->status; ?> </td>
                        <td style="width:15%;"> <a href="<?php echo base_url(); ?>Dashboard/edit_library/<?php echo $rowlib->library_id; ?>" class="editbut" >EDIT</a></td>
                        <td> <a class="editbut" href="#" onclick="return deletedata(<?php echo $rowlib->library_id; ?>);" >DELETE</a></td>
                         <td><input style="margin: 4px 4px 0;float:right" type="checkbox" id="chk" class="case" name="userchkbox[]" value="<?php echo $rowlib->library_id; ?>"></td>
                    </tr>





                    <?php
                }
            }
            echo "||";
//            $this->db->where('access_type', 'public');
//            $this->db->select('*');
//            $query = $this->db->get('library');
            $creator  = $this->session->userdata('user_id');
            
          //  $moved_lib = $this->db->get_where('library',array('move_status'=>"moved"))->result_array();
             $moved_lib = $this->db->query("SELECT * FROM `library` WHERE `access_type`='public' AND `creator` !=  $creator")->result_array();
         $subscriber_data = $this->db->get_where('library',array('access_type'=>"private",'status'=>"active",'creator'=>$creator,'subscribe_status'=>"subscribe"))->result_array();
         
        
        // print_r($moved_lib);
         
         
        // print_r($subscriber_data);
         
         
      
          foreach($moved_lib as $ml){
             $move_name[] = $ml['name'] ;
          }
          
          foreach($subscriber_data as $sd){
              $sub_name[] = $sd['name'];
          }
          
        //  print_r($move_name);
          
         // print_r($sub_name);
          
        if(!empty($move_name))  {
          foreach($move_name as $mn){
              
              if(in_array($mn,$sub_name))
              {
                    
              }
              else
              {
                   $condn.="'$mn',";
              }
             
          }
        }
     
      $result = rtrim($condn,", ");
        $query = $this->db->query("SELECT * FROM `library` WHERE `access_type` = 'public' AND `status` = 'active' AND name IN ($result)");
//        $query = $this->db->query("SELECT * FROM `library` WHERE `access_type` = 'public' AND `status` = 'active' AND `creator` != $creator AND `move_status` IS NULL");
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $this->db->where('id', $row->group_name);
                    $this->db->select('*');
                    $query2 = $this->db->get('groups')->result_array();
                    foreach ($query2 as $namedata) {
                        $row->groupName = $namedata['name'];
                    }
                    ?>
                    <tr>
                        <td style="width:10%;"><?php echo $row->name; ?> </td>
                        <td style="width:10%;"><?php echo $row->groupName; ?> </td>
                        <td style="width:20%;"><?php echo $row->discription; ?></td>
                            <td style="width:20%;"><?php echo $row->date; ?> </td>
                                                     <td style="width:12%;"><?php 
//                                               if($lib['user_id']){
//                                                $subscriber = $this->db->get_where("library", array('user_id' => $lib['user_id']))->result_array(); 
//                                                echo count($subscriber);
//                                               }
                                                 $subscriber1 = $this->db->get_where('library',array('name'=>$row->name,'subscribe_status'=>"subscribe"))->result_array();
                                                 //echo $this->db->last_query();
                                                 echo count($subscriber1);
                                                ?>
                         <input style="margin: 4px 4px 0;float:right" type="checkbox" id="chk2" class="case2" name="userchkbox2[]" value="<?php echo $row->library_id; ?>"></td>
                    </tr>





                    <?php
                } echo "||";
            } else {

                return false;
            }
        }
        if ($gid != '') {
            $this->db->where('creator', $this->session->userdata('user_id'));
            $this->db->where('group_name', $gid);
            $this->db->select('*');
            $queryuser = $this->db->get('library');
            if ($queryuser->num_rows() > 0) {
                foreach (($queryuser->result()) as $rowlib) {
                    $this->db->where('id', $rowlib->group_name);
                    $this->db->select('*');
                    $query2 = $this->db->get('groups')->result_array();
                    foreach ($query2 as $namedata) {
                        $rowlib->groupName = $namedata['name'];
                    }
                    ?>
                    <tr>
                        <td style="width:15%;"><?php echo $rowlib->name; ?></td>
                        <td style="width:15%;"><?php echo $rowlib->groupName; ?></td>
                        <td style="width:25%;"><?php echo $rowlib->discription; ?> </td>
                        <td style="width:15%;"><?php echo $rowlib->date; ?></td>
                        <td style="width:12%;"><input type="text" style="text-align:center;width:80px;color:white;background-color:#<?php echo $rowlib->color; ?>; " disabled=""  value="<?php //echo $lib->color;   ?>"></td>
                        <td style="width:12%;"><?php echo $rowlib->access_type; ?></td>
                        <td style="width:12%;"><?php 
    //                                               if($lib['user_id']){
    //                                                $subscriber = $this->db->get_where("library", array('user_id' => $lib['user_id']))->result_array(); 
    //                                                echo count($subscriber);
    //                                               }
                         $subscriber = $this->db->get_where('library',array('name'=>$rowlib->name,'subscribe_status'=>"subscribe"))->result_array();
                         //echo $this->db->last_query();
                         echo count($subscriber);
                        ?></td> 
                        <td style="width:15%;"><?php echo $rowlib->status; ?></td>
                        <td style="width:15%;"> <a href="<?php echo base_url(); ?>Dashboard/edit_library/<?php echo $rowlib->library_id; ?>" class="editbut" >EDIT</a></td>
                        <td style="width:15%;"><a class="editbut" href="#" onclick="return deletedata(<?php echo $rowlib->library_id; ?>);" >DELETE</a></td>
                         <td><input style="margin: 4px 4px 0;float:right" type="checkbox" id="chk" class="case" name="userchkbox[]" value="<?php echo $rowlib->library_id; ?>"></td>
                    </tr>





                    <?php
                }
            }

            echo "||";
//            $this->db->where('group_name', $gid);
//            $this->db->where('access_type', 'public');
//            $this->db->select('*');
//            $query = $this->db->get('library');
             $creator  = $this->session->userdata('user_id');
             
             $creator  = $this->session->userdata('user_id');
            
          //  $moved_lib = $this->db->get_where('library',array('move_status'=>"moved"))->result_array();
         $moved_lib = $this->db->query("SELECT * FROM `library` WHERE `access_type`='public' AND `creator` !=  $creator")->result_array();
         $subscriber_data = $this->db->get_where('library',array('access_type'=>"private",'status'=>"active",'creator'=>$creator,'subscribe_status'=>"subscribe"))->result_array();
         
        
        // print_r($moved_lib);
         
         
        // print_r($subscriber_data);
         
         
      
          foreach($moved_lib as $ml){
             $move_name[] = $ml['name'] ;
          }
          
          foreach($subscriber_data as $sd){
              $sub_name[] = $sd['name'];
          }
          
        //  print_r($move_name);
          
         // print_r($sub_name);
          
        if(!empty($move_name))  {
          foreach($move_name as $mn){
              
              if(in_array($mn,$sub_name))
              {
                    
              }
              else
              {
                   $condn.="'$mn',";
              }
             
          }
        }
     
      $result = rtrim($condn,", ");
        $query = $this->db->query("SELECT * FROM `library` WHERE `access_type` = 'public' AND `status` = 'active' AND name IN ($result) AND group_name = $gid");
       // $query = $this->db->query("SELECT * FROM `library` WHERE `access_type` = 'public' AND `status` = 'active' AND `creator` != $creator AND `move_status` IS NULL");
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $this->db->where('id', $row->group_name);
                    $this->db->select('*');
                    $query2 = $this->db->get('groups')->result_array();
                    foreach ($query2 as $namedata) {
                        $row->groupName = $namedata['name'];
                    }
                    ?>
                    <tr>
                        <td style="width:10%;"><?php echo $row->name; ?> </td>
                        <td style="width:10%;"><?php echo $row->groupName; ?> </td>
                        <td style="width:20%;"><?php echo $row->discription; ?> </td>
                        <td style="width:20%;"><?php echo $row->date; ?> </td>
                                                     <td style="width:12%;"><?php 
//                                               if($lib['user_id']){
//                                                $subscriber = $this->db->get_where("library", array('user_id' => $lib['user_id']))->result_array(); 
//                                                echo count($subscriber);
//                                               }
                                                 $subscriber1 = $this->db->get_where('library',array('name'=>$row->name,'subscribe_status'=>"subscribe"))->result_array();
                                                 //echo $this->db->last_query();
                                                 echo count($subscriber1);
                                                ?>
                            <input style="margin: 4px 4px 0;float:right" type="checkbox" id="chk2" class="case2" name="userchkbox2[]" value="<?php echo $row->library_id; ?>"></td>

                    </tr>





                    <?php
                } echo "||";
            } else {

                return false;
            }
        }
    }

    public function setprivacy() {
        foreach ($_POST['detail'] as $data) {

            $arr = explode('|', $data);
            $library = $arr[0];
            $status = $arr[1];

            $data2['status'] = $status;
            $this->db->where('library_id', $library);
            $this->db->update('library', $data2);
            if ($status == "active") {
                echo "active";
                $this->session->set_flashdata('flash_message', 'Library Active Successfully');
                //redirect('Dashboard/dashboard', 'refresh');
            }

            if ($status == "inactive") {
                echo "inactive";
                $this->session->set_flashdata('flash_message', 'Library Inactive Successfully');
                //redirect('Dashboard/dashboard', 'refresh');
            }
        }
    }

    function chk_lib() {
        $library = $this->input->post('lib');
        $this->db->where('creator', $this->session->userdata('user_id'));
        $this->db->where('name', $library);
        $this->db->select('*');
        $query = $this->db->get('library');

        if ($query->num_rows() > 0) {
            echo "success||";
        } else {
            echo "fail||";
        }
    }

    public function move_library() {


        foreach ($_POST['detail'] as $data) {
            $id = $data;
            $this->db->where('library_id', $id);
            $this->db->select('*');
            $query = $this->db->get('library');
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $name = $row->name;
                    $group = $row->group_name;
                    $email = $row->display_email;
                    $desc = $row->discription;
                    $access = 'private';
                    $status = $row->status;
                    $user_id = $row->creator;
                    $color = $row->color;
                    $data = array(
                        'user_id' => $user_id,
                        'name' => $name,
                        'group_name' => $group,
                        'display_email' => $email,
                        'discription' => $desc,
                        'access_type' => $access,
                        'status' => $status,
                        'creator' => $this->session->userdata('user_id'),
                        'date' => date('Y-m-d'),
                        'color' => $color,
                        'subscribe_status' => "subscribe"
                    );
                    $this->db->insert('library', $data);
                    $insert = $this->db->insert_id();
                    $this->db->where('library_id', $id);
                    $this->db->select('*');
                    $query2 = $this->db->get('library_info');
                    if ($query2->num_rows() > 0) {
                        $ik = 0;
                        foreach (($query2->result()) as $row2) {
                            $col = $row2->row_col_name;
                            $colval = $row2->value;
                            $row = $row2->row_id;
                            $coldata = array(
                                'library_id' => $insert,
                                'row_col_name' => $col,
                                'value' => $colval,
                                'user_Id' => $this->session->userdata('user_id'),
                            );

                            if ($row == '0') {
                                $coldata['row_id'] = $row;
                            } else {
                                $coldata['row_id'] = $in_id;
                            }
                            $this->db->insert('library_info', $coldata);
                            if ($row == '0') {
                                $ik = 0;
                                if ($ik == 0) {
                                    $in_id = $this->db->insert_id();
                                }
                            }
                            $ik++;
                        }
                    }
                }
                
                $this->db->set('move_status',"moved");
                $this->db->where('library_id', $id);
                $this->db->update('library');
                
                // $this->db->where('library_id', $id);
                 // $this->db->delete('library_info');
            }
        }echo "success";
    }

    public function user_template() {
        //echo "trg";die;
        $id = $this->session->userdata('user_id');
        $sender = $this->input->post('sender');
        $subject = $this->input->post('subject');
        $content = $this->input->post('content');
        $status = 'active';
        $data = array(
            'sender' => $sender,
            'subject' => $subject,
            'content' => $content,
            'status' => $status,
            'user_id' => $id
        );
        //print_r($data);
        $this->db->insert('usertemplate', $data);
        $balance = "SELECT * FROM `usertemplate`where user_id=" . $id . " ORDER BY `usertemplate`.`id` DESC limit 1";
        $query = $this->db->query($balance);
        $result1 = $query->result_array();
    }

    public function user_templatesave() {
        //echo "trg";die;
        $id = $this->session->userdata('user_id');
        $sender = $this->input->post('sender');
        $subject = $this->input->post('subject');
        $content = $this->input->post('content');
        $status = 'active';
        $data = array(
            'sender' => $sender,
            'subject' => $subject,
            'content' => $content,
            'status' => $status,
            'user_id' => $id
        );
        //print_r($data);
        $this->db->insert('usertemplate', $data);
        $balance = "SELECT * FROM `usertemplate`where user_id=" . $id . " ORDER BY `usertemplate`.`id` DESC limit 1";
        $query = $this->db->query($balance);
        $result1 = $query->result_array();

        echo "yes";
    }

    public function move_library_accept() {

        $user1 = $this->session->userdata('user_email');
        $library1 = $_POST['library'];

        $this->db->set('notification', '0');
        $this->db->where('user_email', $user1);
        $this->db->where('library_id', $library1);
        $this->db->update('invite_user');
        // echo"retr";
        // echo $this->db->last_query();
        //die;
        foreach ($_POST['detail'] as $data) {
            $id = $data;
            $this->db->where('library_id', $id);
            $this->db->select('*');
            $query = $this->db->get('library');
            if ($query->num_rows() > 0) {
                foreach (($query->result()) as $row) {
                    $name = $row->name;
                    $group = $row->group_name;
                    $email = $row->display_email;
                    $desc = $row->discription;
                    $access = 'private';
                    $status = $row->status;
                    $user_id = $row->creator;
                    $color = $row->color;
                    $data = array(
                        'user_id' => $user_id,
                        'name' => $name,
                        'group_name' => $group,
                        'display_email' => $email,
                        'discription' => $desc,
                        'access_type' => $access,
                        'status' => $status,
                        'creator' => $this->session->userdata('user_id'),
                        'date' => date('Y-m-d'),
                        'color' => $color,
                        'subscribe_status'=> "subscribe"
                    );
                    $this->db->insert('library', $data);
                    $insert = $this->db->insert_id();
                    $this->db->where('library_id', $id);
                    $this->db->select('*');
                    $query2 = $this->db->get('library_info');
                    if ($query2->num_rows() > 0) {
                        $ik = 0;
                        foreach (($query2->result()) as $row2) {
                            $col = $row2->row_col_name;
                            $colval = $row2->value;
                            $row = $row2->row_id;
                            $coldata = array(
                                'library_id' => $insert,
                                'row_col_name' => $col,
                                'value' => $colval,
                                'user_Id' => $this->session->userdata('user_id'),
                            );

                            if ($row == '0') {
                                $coldata['row_id'] = $row;
                            } else {
                                $coldata['row_id'] = $in_id;
                            }
                            $this->db->insert('library_info', $coldata);
                            if ($row == '0') {
                                $ik = 0;
                                if ($ik == 0) {
                                    $in_id = $this->db->insert_id();
                                }
                            }
                            $ik++;
                        }
                    }
                }
            }
        }echo "success";
    }

    public function move_library_reject() {
        $user1 = $this->session->userdata('user_email');
        $library1 = $_POST['library'];

        $this->db->set('notification', '0');
        $this->db->where('user_email', $user1);
        $this->db->where('library_id', $library1);
        $this->db->update('invite_user');

        echo "success";
    }



        function get_records() {
            //echo $_POST['data'];die;
            //if(isset($_POST['token']))
//{
            $data = json_decode($_POST['data']);
            $counter = 1;
            $a1 = $this->db->get_where('library')->result_array();
            foreach ($a1 as $key => $row) {
                $b1[$row['library_id']] = $data[$key];
            }
            foreach ($data as $key => $val) {
                $this->db->set('order', $counter);
                $this->db->where('library_id', $val);
                $this->db->update('library');
                //save_record($val,$counter);
                $counter++;
            }
            //echo "saved";
            //echo  $this->db->last_query();
//}
            redirect('index.php/Dashboard/dashboard', 'refresh');
        }
       
	   
	   public function show_list_data($param1='')
	   {
	        $col_value = $this->input->post('column_value');
                
                 $col_type = $this->db->get_where('column_type',array('id'=>$col_value))->result_array();
                   $frow = $this->input->post('frow');
			$fcol = $this->input->post('fcol');
			$rclass = $this->input->post('rclass');
		       $search_val = $this->input->post('search_val');
//               if($col_type){      
                if($param1 == "first")   
                   
                {   
                   
		  if($col_type){      
		 
		  if($col_type[0]['type'] == "1")
		   { 
		     
		      ?>
                    
                   
		     <td><?php echo $frow ?><input type ="hidden" name="rowname[]" value= "<?php echo $frow ?>"></td>
                     <td><input type ="text" class='form-control inputform sub' name="search_name[]" value= "<?php echo $search_val ?>" onkeypress="return check_Sub(event);"></td>
			 <td class='1'>
			 <input type='text' name="<?php echo $fcol;?>val[]" class='form-control usedatepicker inputform r0 c0' readonly  placeholder='date'>
			 <input type='hidden' name="<?php echo $fcol ?>valtype[]" class=' form-control inputform r0 c0 ' value="<?php echo $col_type[0]['name'] ?>">
			 </td> <td id="del_btn"><a href='#' onclick ="del('rowapp0')">DELETE</a></td>
			  <script>
                              // alert("dg");
                                $('body').on('focus', ".usedatepicker", function(){
                               $(this).not('.hasDatePicker').datepicker();     
                                $(this).datepicker(); 
                              
                         });
 
    
           
                          </script>
                         
           <?php 
		   }
		   
		   if($col_type[0]['type'] == "2")
		   { 
		     
		      ?>
		     <td><?php echo  $frow ?><input type ='hidden' name='rowname[]' value= "<?php echo $frow ?>"></td>
                     <td><input type ="text"  class='form-control inputform sub' name="search_name[]" value= "<?php echo $search_val ?>" onkeypress="return check_Sub(event);"></td>
			 <td class='1'><input type='text' name='<?php echo $fcol?>val[]' class='form-control abcurl inputform r0 c0' id='url' placeholder='enter Url'><input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $fcol ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'> </td> <td  ><a href='#' onclick ="del('rowapp0')">DELETE</a></td>
			  
           <?php 
		   }
		   
		   
		   
		   if($col_type[0]['type'] == "3")
		   { 
		      $data = $this->db->get_where('column_type_values',array('column_type_id'=>$col_value))->result_array();
			  
			  	//print_r($data);
		      ?>
		      <td><?php echo $frow?><input type="hidden" name="<?php echo $fcol?>valtype[]" value="<?php echo $col_type[0]['name'] ?>">
			  <input type="hidden" name="rowname[]" value="<?php echo $frow?>"></td>
                      <td><input type ="text" class='form-control inputform sub' name="search_name[]" value= "<?php echo $search_val ?>" onkeypress="return check_Sub(event);"></td><td class="1"><select  class="form-control inputform r0 c0 dropdown" name="<?php echo $fcol?>val[] ">
                               <option value="">Select</option>
                              <?php foreach ($data as $cou) { ?>
			  <option value="<?php echo $cou['id']; ?>"><?php echo $cou['value']; ?> </option>
			  <?php } ?>
			  </select>
			  </td><td><a href=# onclick="del('rowapp0')">DELETE</a></td>
			  
           <?php 
		   }
		   if($col_type[0]['type'] == "4")
		   { 
		     
		      ?>
                          <td><?php echo $frow ?><input type ='hidden' name='rowname[]' value= "<?php echo $frow ?>"></td>
                 <td><input type ="text"  class='form-control inputform sub' name="search_name[]" value= "<?php echo $search_val ?>" onkeypress="return check_Sub(event);"></td>
                
                   </td>
		   <td class='1'><input type='text' name='<?php echo $fcol?>val[]' class='form-control inputform r0 c0 textbx'  placeholder='enter text'> <input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $fcol ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'></td> <td  ><a href='#' onclick ="del('rowapp0')">DELETE</a></td>
			  
           <?php 
		   }
		   
		   
		   if($col_type[0]['type'] == "5")
		   { 
		     
		      ?>
		    <td><?php echo $frow ?><input type ='hidden' name='rowname[]' value= "<?php echo $frow ?>"></td>
                    <td><input type ="text"   class='form-control inputform sub' name="search_name[]" value= "<?php echo $search_val ?>" onkeypress="return check_Sub(event);"></td>
			<td class='1'><input type='file' name='valrowapp01[]' class=' form-control inputform r0 c0 file_val kr'  placeholder='image'  onchange='ValidateSingleInput(this);'><input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $fcol ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'></td> <td><a href='#' onclick ="del('rowapp0')">DELETE</a></td>
			  
           <?php 
		   }
		   
                  }
                  else{
                     ?> 
                      <td><?php echo $frow ?><input type ='hidden' name='rowname[]' value= "<?php echo $frow ?>"></td>
                      <td><input type ="text"  class='form-control inputform sub' name="search_name[]" value= "<?php echo $search_val ?>" onkeypress="return check_Sub(event);"></td>
		   <td class='1'><input type='text' name='<?php echo $fcol?>val[]' class='form-control inputform r0 c0 open'  placeholder='enter text'> <input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $fcol ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'></td> 
                   <td><a href='#' onclick ='del('<?php echo $rclass ?>')'>DELETE</a></td>
                   <?php   
                  }
                  }
           
                if($param1 == "column")
                {
                   // echo $col_value;
                    
                  $col = $this->input->post('col');
                  
                    if($col_type){
                     if($col_type[0]['type'] == "1")
                     {
                         
                         ?>
                   <td><input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $col ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'><input type='text' name='<?php echo $col ?>val[]' class='form-control usedatepicker inputform r0 c0' readonly  placeholder='select date'> 
                     
                             
                         <?php    
                             }
                     if($col_type[0]['type'] == "2")
                     {
                         ?>
                   <td><input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $col ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'><input type='text' name='<?php echo $col ?>val[]' class='form-control abcurl inputform r0 c0'  placeholder='enter Url' id="url"> 
                     
                             
                         <?php    
                    }
                    
                    if($col_type[0]['type'] == "4")
                     {
                        
                        
                         ?>
                   <td><input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $col ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'>
                              <input type="text" name='<?php echo $col ?>val[]' class='form-control inputform r0 c0 textbx'  placeholder='enter text'>
                             
                         <?php    
                    }
                    if($col_type[0]['type'] == "3")
                     {
                         $data = $this->db->get_where('column_type_values',array('column_type_id'=>$col_value))->result_array();
			  
                         
                        // print_r($data);
                         ?>
                 <td><input type="hidden" name="<?php echo $col ?>valtype[]"  value="<?php echo $col_type[0]['name'] ?>">
                     <select class='form-control inputform r0 c0 dropdown' name='<?php echo $col ?>val[]'>
                         <option value="">Select</option>
                         <?php foreach ($data as $cou) { ?>
                          <option value='<?php echo $cou['id']; ?>'>
                                 <?php echo $cou['value']; ?></option>
                         <?php } ?>
                     </select> </td>
                         <?php    
                    }
                    if($col_type[0]['type'] == "5")
                     {
                         ?>
                   <td><input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $col ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'> <input type='file' name='val<?php echo $_POST['rowid'].$_POST['colclass']; ?>[]' class='form-control inputform r0 c0 file_val kr'   onchange='ValidateSingleInput(this);'> 
                     
                             
                         <?php    
                    }
                          
                    
                }else{
                    ?>

		   <td class='1'><input type='text' name='<?php echo $col?>val[]' class='form-control inputform r0 c0 open'  placeholder='enter text' > <input type='hidden' class=' form-control inputform r0 c0 ' name='<?php echo $col ?>valtype[]' value='<?php echo $col_type[0]['name'] ?>'></td> 
                   
			  
           <?php   
                }
           }
           
           
		
	   }
    }
    ?>

