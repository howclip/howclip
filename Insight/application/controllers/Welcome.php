<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Welcome extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        //$this->load->library('csvimport');
        $this->load->database();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->model('Dashboard_m');
         error_reporting(0);
        //error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
    }

    public function index() {
        $data['banner'] = $this->db->get('tblgeneralsetting')->result_array();
        $this->load->view('index',$data);
    }

    public function dashboard() {

        $data['groups'] = $this->Dashboard_m->getall_groups();
        $data['libraries'] = $this->Dashboard_m->getall_library();
        $uid = $this->session->userdata('user_id');
        $data['libraybyid'] = $this->Dashboard_m->get_librarybyid($uid);

        $this->load->view('dashboard', $data);
    }

    public function login() {

        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $sql = "SELECT * FROM `user` WHERE (`username` = '$user' OR `user_email` = '$user') AND `user_password` = '$pass'";
        $user = $this->db->query($sql)->row_array();

        if ($user) {

            $userrecord = array(
                'user_id' => $user['user_id'],
                'username' => $user['username'],
                'user_email' => $user['user_email'],
                'user_firstname' => $user['user_firstname'],
            );


            $this->session->set_userdata($userrecord);

            $this->session->set_flashdata('flash_message', 'login Successfull');
            
            redirect('index.php/Dashboard/dashboard', 'refresh');
        } else {
            
            $this->session->set_flashdata('permission_message', 'username and Password is invalid');
            // redirect($_SERVER[HTTP_REFFER]);
            redirect('index.php/Dashboard/index', 'refresh');
        }
    }

    public function admin_login() {
        $user = $this->input->post('username');
        $pass = $this->input->post('password');
        $sql = "SELECT * FROM `user` WHERE (`username` = '$user' OR `user_email` = '$user') AND `user_password` = '$pass' AND `user_type` = 'admin'";
        $user = $this->db->query($sql)->row_array();

        if ($user) {

            $userrecord = array(
                'user_id' => $user['user_id'],
                'username' => $user['username'],
                'user_email' => $user['user_email'],
                'user_firstname' => $user['user_firstname'],
            );
            //print_r($userrecord); die;

            $this->session->set_userdata($userrecord);
            // print_r($userrecord);
            $this->session->set_flashdata('flash_message', 'login Successfull');


            redirect('Welcome2/dashboard', 'refresh');
        } else {
            $this->session->set_flashdata('permission_message', 'username and Password is invalid');
            // redirect($_SERVER[HTTP_REFFER]);
            redirect('Admin/index', 'refresh');
        }
    }

    public function userregistration() {
        //echo "hii"; die;
        $this->load->view('registration');
    }
 public function privacy_policy() {
        //echo "hii"; die;
        $this->load->view('privacy_policy');
    }

    public function save_user($id = '') {

        if ($id == '') {
            $user = $this->db->get_where('user', array('username' => $this->input->post('username')))->row_array();
            $data = array(
                'username' => $this->input->post('username'),
                'user_firstname' => $this->input->post('first_name'),
                'user_lastname' => $this->input->post('last_name'),
                'user_email' => $this->input->post('email'),
                'user_password' => $this->input->post('password'),
                'user_DOB' => $this->input->post('dob'),
                'user_zipcode' => $this->input->post('zip'),
                'status' => 'active',
                'user_gender' => $this->input->post('gen'),
                'user_occupation' => $this->input->post('occupation'),
                'user_date' => date("Y-m-d"),
            );


            if ($user) {
                $this->session->set_flashdata('permission_message', 'Username already exist.');
                redirect('Welcome/userregistration', 'refresh');
            } else {


                $result = $this->db->insert('user', $data);
                $insert = $this->db->insert_id();
                $userrecord = array(
                    'user_id' => $insert,
                    'username' => $this->input->post('username'),
                    'user_email' => $this->input->post('email'),
                    'user_firstname' => $this->input->post('first_name'),
                );
                //print_r($userrecord); die;

                $this->session->set_userdata($userrecord);
                if (!$result) {
                    $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                    redirect('Welcome/userregistration', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_message', 'Registration Complete');
                    redirect('index.php/Dashboard/dashboard', 'refresh');
                }
            }
        }
        if ($id != '') {

            $update = array(
                'username' => $this->input->post('username'),
                'user_firstname' => $this->input->post('first_name'),
                'user_lastname' => $this->input->post('last_name'),
                'user_email' => $this->input->post('email'),
                'user_DOB' => $this->input->post('dob'),
                'user_zipcode' => $this->input->post('zip'),
                'status' => 'active',
            );
            $this->db->where('user_id', $id);
            $resultdata = $this->db->update('user', $update);
            if (!$result) {
                $this->session->set_flashdata('permission_message', 'Failed to insert the record.');
                redirect('Welcome/dashboard', 'refresh');
            } else {
                $this->session->set_flashdata('flash_message', 'record inserted Successfully');
                redirect('index.php/Dashboard/dashboard', 'refresh');
            }
        }
    }

    public function logout() {
        //$array_items = array('user_id' => '', 'username' => '','user_email' => '','user_firstname' => '');
        // $this->session->unset_userdata($array_items);
        //$this->session->unset_userdata();
        $this->session->sess_destroy();
        $this->session->set_flashdata('flash_message', 'Successfully Logout');

        redirect('Welcome/index', 'refresh');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
