<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">

    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <img src="<?php echo base_url(); ?>assets/images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-7 col-xs-6">
                            <img src="<?php echo base_url(); ?>assets/images/edit.png" class="img-responsive crea">

                        </div>
                        <div class="col-md-2 col-xs-6">
                            <div class="dropdown" style="text-align:right;">
                                <button class="btn logdrop dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img src="<?php echo base_url(); ?>assets/images/user-group-icon.png" class="img-responsive dropimg"> <?php echo ucwords($this->session->userdata('username')); ?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left:initial;">
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/dashboard">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/change_pass">Change Password</a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/edit_profile/<?php echo $this->session->userdata('user_id'); ?>">Change Profile</a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/column_type">Column Type</a></li>
                                      <li><a href="<?php echo base_url(); ?>index.php/Dashboard/column_list">List</a></li>
<!--                                    <li><a href="<?php echo base_url('Dashboard'); ?>/groups">Groups</a></li>-->
                                    <li><a href="<?php echo base_url('Welcome'); ?>/logout">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="paragraph">
                                Create or edit your own personal library with any text records you would like Insight to identify and associated data you would like to display when reading or typing in your favorite Web apps (Facebook, Email, Chat).  Make it accessible to all, distribute the Access ID to your friends, or keep it entirely to yourself – the choice is yours and we hope you enjoy.

                            </div>
                        </div> 
                    </div>
                    <?php foreach ($libraybyid as $lib) { ?>
                        <form class="form-inline" action="<?php echo base_url(); ?>index.php/Dashboard/save_Edit/<?php echo $lib['library_id']; ?>" method = "post" enctype="multipart/form-data">


                            <div class="row topspc">
                                <div class="col-md-3 spc"> 
                                    <div class="label-form"> Library Name</div>
                                </div>
                                <div class="col-md-3 spc">
                                    <input type="text" name="library_name" value="<?php echo $lib['name']; ?>" class="form-control inputform" id="lib_name" placeholder="">
                                </div>
                                <div class="col-md-3 spc">
                                    <div class="label-form"> Assign Color</div>
                                </div>
                                <div class="col-md-3 spc">
                                    <input  class="jscolor form-control inputform" value="<?php echo $lib['color']; ?>" name="color" id="color">
                                </div>
                            </div>

                            <div class="row topspc">
                                <div class="col-md-3 spc">

                                    <div class="label-form"> Description</div>

                                </div>
                                <div class="col-md-9 spc">

                                    <input type="text" name="description" value="<?php echo $lib['discription']; ?>" class="form-control inputform" id="desc" placeholder="">

                                </div>

                            </div>
                            <div class="row topspc">

                                <div class="col-md-3 spc">
                                    <div class="label-form"> Access Type</div>


                                </div>
                                <div class="col-md-3 spc">

                                    <select class="form-control inputform" name="access_type" id="access">
                                        <option value=""> Select Access Type </option>
                                        <option <?php
                                        if ($lib['access_type'] == 'private') {
                                            echo "selected";
                                        }
                                        ?> value="private"> Private </option>
                                        <option <?php
                                        if ($lib['access_type'] == 'public') {
                                            echo "selected";
                                        }
                                        ?> value="public"> Public </option>

                                    </select>

                                </div>
                                <div class="col-md-3 spc">
                                    <div class="label-form"> Library Group</div>
                                </div>
                                <div class="col-md-3 spc">
                                    <select class="form-control inputform" name="group_name" id="lib_group">
                                        <option value=""> Select Group </option>
                                        <?php foreach ($groups as $group) { ?>
                                            <option <?php
                                            if ($lib['group_name'] == $group->id) {
                                                echo "selected";
                                            }
                                            ?> value="<?php echo $group->id; ?>"> <?php echo $group->name; ?> </option>
                                            <?php } ?>
                                    </select>
                                </div></div>
                            
                            <div class="row topspc">

                                <div class="col-md-3 spc">
                                    <div class="label-form"> Date</div>


                                </div>
                                <div class="col-md-3 spc">

                                   <input type="text" readonly name="lib_date" value="<?php echo $lib['date']; ?>" class="form-control inputform" id="lib_date" placeholder="">

                                </div>
                                <div class="col-md-3 spc">
                                    <div class="label-form"> Subscriber</div>
                                </div>
                                <div class="col-md-3 spc">
                                    <?php $subscriber1 = $this->db->get_where('library',array('name'=>$lib['name'],'subscribe_status'=>"subscribe"))->result_array();
                                                 //echo $this->db->last_query();
                                                 ?>
                                     <input type="text" readonly name="subscriber" value="<?php echo count($subscriber1); ?>" class="form-control inputform" id="lib_sub" placeholder="">
                                </div></div>
                            <?php ?>
                            <div class="table-responsive">
                                <input type="hidden" id="count_column" value="<?php echo count($libraybyid[0]['colinfo'])?>">
                                <table class="table table-striped empbox" id="mtable">
                                    <thead>

                                        <tr id="headapp">
                                            <th> </th> <th> </th>
                                            <?php
                                            foreach ($libraybyid as $lkey1 => $lb1) {
                                                $ps = 1;
                                                foreach ($lb1['colinfo'] as $lkey2 => $lb2) {
                                                    ?>
                                             <th class="<?php echo $ps; ?>"><a href="#" onclick="delcol(<?php echo $ps; ?>);"> DELETE</a></th>


                                                    <?php
                                                    $ps++;
                                                }
                                            }
                                            ?>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="red" id="redapp">
                                            <td class="red"></td>
                                             <td class="red">Substitute</td>
                                            <?php
                                            foreach ($libraybyid as $k1 => $library1) {

                                                $gs = 1;
                                                //                                                print_r($library1['colinfo']);

                                                foreach ($library1['colinfo'] as $k2 => $library2) {
                                                    ?>

                                            <td class="<?php echo $gs; ?>"><?php echo $library2['row_col_name']; ?><input type ="hidden" name="colname[]" value= "<?php echo $library2['row_col_name']; ?>"><input type ='hidden' name='coltype[]' value= "<?php echo $library2['value_type']?>"></td> 
                                                    <?php
                                                    $gs++;
                                                }
                                            }
                                            ?>

                                            <td></td>
                                        </tr>
                                        <?php
                                        foreach ($libraybyid as $key1 => $lib1) {

                                            $k = 0;
                                            foreach ($lib1['rowinfo'] as $key2 => $lib2) {
                                                ?>
                                                <tr id="<?php echo "rowapp" . $k; ?>" class="rowapp">
                                                    <td><?php echo $lib2['row_col_name']; ?><input type ='hidden' name='rowname[]' value= "<?php echo $lib2['row_col_name']; ?>"> </td><td><input type ='text' class='form-control inputform sub' name='search_name[]' value= "<?php echo $lib2['substitute'];?>"> </td>
                                                    <?php
                                                    $ri = 1;
                                                    foreach ($lib2['ii'] as $key3 => $lib3) {
                                                        
                                                        $data_type = $this->db->get_where('column_type',array('id'=>$lib3['value_type']))->result_array();
                                                       
                                                       
                                                        if($data_type){
                                                        if ($data_type[0]['type'] == '1') {
                                                            ?>

                                                            <td class="<?php echo $ri; ?>">   <input type="hidden" name= "<?php echo $lib3['row_col_name'] . "valtype[]"; ?>" class="form-control " value="<?php echo $lib3['value_type']; ?>"><input type="text" name= "<?php echo $lib3['row_col_name'] . "val[]"; ?>" class="form-control usedatepicker21 inputform " readonly="" value="<?php echo $lib3['value']; ?>"></td>
                                                            <?php
                                                        }
                                                        if ($data_type[0]['type'] == '2') {
                                                            
                                                              ?>
                                                            <td class="<?php echo $ri; ?>">   <input type="hidden" name= "<?php echo $lib3['row_col_name'] . "valtype[]"; ?>" class="form-control " value="<?php echo $lib3['value_type']; ?>"><input type="text" name= "<?php echo $lib3['row_col_name'] . "val[]"; ?>" class="form-control  abcurl inputform " value="<?php echo $lib3['value']; ?>"></td>
                                                            <?php
                                                        }
                                                        
                                                        if ($data_type[0]['type'] == '3') {
                                                            
                                                            $col_data = $this->db->get_where('column_type_values',array('column_type_id'=>$lib3['value_type']))->result_array();
                                                              ?>
                                                            <td class="<?php echo $ri; ?>">
                                                                <input type="hidden" name= "<?php echo $lib3['row_col_name'] . "valtype[]"; ?>" class="form-control " value="<?php echo $lib3['value_type']; ?>">
                                                                <select name= "<?php echo $lib3['row_col_name'] . "val[]"; ?>" class="form-control inputform dropdown">
                                                                    <option value="">Select</option>
                                                                         <?php
                                                                    foreach ($col_data as $cou) {
                                                                        ?>
                                                                        <option value="<?php echo $cou['id']; ?>" <?php
                                                                        if ($cou['id'] == $lib3['value']) {
                                                                            echo "selected";
                                                                        }
                                                                        ?>><?php echo $cou['value']; ?></option>


                                                                    <?php } ?>
                                                                </select>
                   <!--                                                            <input type="text" name= "<?php echo $lib3['row_col_name'] . "val[]"; ?>" class="form-control inputform " value="<?php echo $lib3['value']; ?>">-->
                                                            </td>
                                                            <?php
                                                        }
                                                        
                                                        
                                                        if($data_type[0]['type'] == '4')
                                                        {
                                                            ?>
                                                               <td class="<?php echo $ri; ?>"><input type="hidden" name= "<?php echo $lib3['row_col_name'] . "valtype[]"; ?>" class="form-control " value="<?php echo $lib3['value_type']; ?>"><input type="text" name= "<?php echo $lib3['row_col_name'] . "val[]"; ?>" class="form-control inputform textbx" value="<?php echo $lib3['value']; ?>"></td>
                                                            <?php
                                                        }
                                                        if($data_type[0]['type'] == '5')
                                                        {
                                                            ?>
                                                              <td style ="width:150px" class="<?php echo $ri; ?>">
                                                                <input type="hidden" name= "<?php echo "val3rowapp". $k.$ri."[]"; ?>" class="form-control ki" value="<?php echo $lib3['value']; ?>">
<!--                                                                <input type="hidden" name= "<?php echo $lib3['row_col_name'] . "val[]"; ?>" class="form-control " value="<?php echo $lib3['value']; ?>">-->

                                                                <input type="hidden" name= "<?php echo $lib3['row_col_name'] . "valtype[]"; ?>" class="form-control " value="<?php echo $lib3['value_type']; ?>">
                                                                <input type="file" name= "<?php echo "valrowapp". $k.$ri."[]"; ?>" class="form-control pop  kr" style ="width:150px"  onchange="ValidateSingleInput(this);hg(this);">
                                                                <img src="<?php echo base_url(); ?>uploads/library/<?php echo $lib3['value']; ?>" style="height:20px;width:20px;">

                                                            </td> 
                                                            <?php
                                                        }
                                                        }else{
                                                        
                                                            ?>
                                                              <td class="<?php echo $ri; ?>"><input type="hidden" name= "<?php echo $lib3['row_col_name'] . "valtype[]"; ?>" class="form-control " value="<?php echo $lib3['value_type']; ?>"><input type="text" name= "<?php echo $lib3['row_col_name'] . "val[]"; ?>" class="form-control inputform open" value="<?php echo $lib3['value']; ?>"></td>
                                                            <?php
                                                        
                                                        }
                                                  //   print_r($data_type);
                                                    // die;
                                                     $ri++;     
                                              }
                                            ?>
                                                    <td><a href="#" onclick="del('<?php echo "rowapp" . $k; ?>')">DELETE</a></td>
                                                </tr>
                                                <?php
                                                $k++;
                                            }
                                                   
                                        }
                                        ?>


                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                        <div class="row">
                             <div class="col-md-5 col-sm-3" style="width:33%">
                                <a href="#" onclick="create_col();"> <img src="<?php echo base_url(); ?>images/buttoin.png" class="img-responsive addons">
                                    <h4 class="addhead" onclick="create_col();">Add New Column (Up to 10)</h4></a>
                            </div>
                              <div class="col-md-5 col-sm-3" style="width:36%">
                                <a href="#" onclick="create_row();"> <img src="<?php echo base_url(); ?>images/buttoin.png" class="img-responsive addons">
                                    <h4 class="addhead" onclick="create_row();">Add New Record (Up to 1000)</h4></a>
                            </div>
                             <div class="col-md-1 col-sm-1">
                                 <button style="height:52px;width:100%" type="button" class="btn liberary" style="height: 50px;" onclick="show_invite()">Invite User</button>
                                <!--<button type ="submit" id="btnSubmit" style="padding:0px;border-radius: 10px;" onclick="return validate();"> <a href="#"> <img src="<?php echo base_url(); ?>images/submit.png" class="img-responsive subimage"></a></button>-->
                            </div>
                           
                            <div class="col-md-2 col-sm-2">
                                <!--                             onclick="return validate();"-->
                                <button type ="submit" id="btnSubmit" style="padding:0px;border-radius: 10px;width:82%" onclick="return validate();" > <a href="#"> <img src="<?php echo base_url(); ?>images/submit.png" class="img-responsive subimage"></a></button>
                            </div>
                        </div>
                </div>
            </div>
       
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Record  Title</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="row_title" class="form-control inputform"  id="row_title" placeholder="Record">
                        <div id="div11" style="margin-bottom: 10px">
                                            <input name="type1" class="form-control inputform " style="margin-top:20px;" id="search"  placeholder="Record Substitutes" >
                                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="add_row();">Add Row</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="first" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Enter Your First Record</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="row_title" class="form-control inputform " style="margin-bottom:20px;" id="first_row" placeholder="Record">
                        <input type="text" name="col_title" class="form-control inputform " style="margin-bottom:20px;" id="first_col" placeholder="Record Data Field" onkeypress="return isNumber(event)" >
                        
                        <div id="div1" style="margin-bottom: 10px">
                                            <input name="type1" class="form-control inputform " style="margin-top:20px;" id="search"  placeholder="Record Substitutes" >
                                        </div>
                        <select name="type" class="form-control inputform " style="margin-bottom:20px;" id="type">
                            <option value=""> Select Field Type</option>
                            <?php
                            $row1 = $this->db->get_where('column_type')->result_array();
                            foreach ($row1 as $new) {
                                ?> 
                                <option value="<?php echo $new['id']; ?>"><?php echo ucwords($new['name']); ?></option> 


                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="check_col_name('first');">Add</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="colModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Column Title</h4>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="row_title" class="form-control inputform" id="col_title" placeholder="Record Data Field" onkeypress="return isNumber(event)" >
                        <select name="type" class="form-control inputform " style="margin-top:20px;" id="type1">
                            <option value=""> Select Field Type</option>
                            <?php
                            $row1 = $this->db->get_where('column_type')->result_array();
                            foreach ($row1 as $new) {
                                ?> 
                                <option value="<?php echo $new['id']; ?>"><?php echo ucwords($new['name']); ?></option> 


                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" onclick="check_col_name('column');">Add Column</button>
                    </div>
                </div>
            </div>
        </div>
                    
         <div class="modal fade" id="invite_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Invite User</h4>
                                    </div>
                                    <div class="modal-body"> 
                          <div class="row">
                <div class="col-md-12">
                    <div id="polina1">
                       
                        <!--<form role="form" action="<?php echo base_url(); ?>index.php/Dashboard/invite_user" method = "post">-->
                            <div class="col-md-12">
                                <?php $data=$this->db->get('email')->result_array();
                                     // $data = $this->db->get()->result_
                                ?>
                                <h1>Invite Users</h1>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <label style="font-size:15px;"> Add User Email Id</label>
                                  </div>
                                    <div class="col-md-6" style="color:red;margin-top: 10px">
                                         
                                        
                                        
                                        <input type="hidden" class="form-control" name="library"   value="<?php print_r($groups); ?>">
                                 
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 1" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="etext" class="form-control" name="mail1[]"   placeholder="Enter user email 2" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 3" value="" onblur="validateEmail(this);" >
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 4" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 5" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"  placeholder="Enter user email 6" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 7" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 8" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 9" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 10" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 11" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 12" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 13" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 14" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 15" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"  placeholder="Enter user email 16" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 17" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 18" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 19" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 20" value="" onblur="validateEmail(this);">
                                   </div>
                        <div class="form-group col-md-12" style="margin-top:4%">
                                <label style="font-size:15px;"><?php echo ucwords($data[0]['general_title']); ?></label>
                                <input type="text" class="form-control" name="subject" id="subject"   value="<?php echo $data[0]['general_value']; ?>">
		
                                                        
                            </div>
                        <div class="form-group col-md-12" style="display:none">
                                <label style="font-size:15px;"><?php echo ucwords($data[1]['general_title']); ?></label>
                                <input type="text" class="form-control" readonly="" rows="4" cols="20" id="sender" name="sender" onblur="validateEmail(this);" value="<?php echo $data[1]['general_value']; ?>"  >
		 
                                                        
                            </div>
                             <div class="form-group col-md-12">
<!--                                <label style="font-size:15px;"><?php echo ucwords($data[3]['general_title']); ?></label>
                                <textarea  style="resize:none; height:150px" class="form-control" rows="4" cols="20" id="privacy_content" name="privacy_content"   ><?php echo $data[3]['general_value']; ?></textarea>
		 
                                 <select class="form-control " name="status" id="status">
                                    
                                      <option value="Active" <?php
                                                                if ($data[3]['general_value'] == 'active') {
                                                                    echo "selected";
                                                                }
                                                                ?>>Active</option>
                                        <option value="Inactive" <?php
                                                                if ($data[3]['general_value']== 'inactive') {
                                                                    echo "selected";
                                                                }
                                                                ?>>Inactive</option>
                                </select>                        -->
                            </div>
                <div class="form-group col-md-12">
                    <label style="font-size:15px;"><?php echo ucwords($data[2]['general_title']); ?></label>
                          <textarea name="content" class="ckeditor form-control " id="content"><?php echo $data[2]['general_value']; ?></textarea>

<!--                    <textarea  style="resize:none; height:150px" class="form-control"  id="content" name="content" ><?php echo $data[2]['general_value']; ?></textarea>-->
<!--		 <input type="hidden" class="form-control" 
                                                       name="name1"  value="<?php echo $data[0]['general_id']; ?>" >
                                                <input type="hidden" class="form-control" 
                                                       name="name2"  value="<?php echo $data[1]['general_id']; ?>" >
                                                 <input type="hidden" class="form-control" 
                                                       name="name3"  value="<?php echo $data[2]['general_id']; ?>" >
                                                 <input type="hidden" class="form-control" 
                                                       name="name4"  value="<?php echo $data[3]['general_id']; ?>" >-->
                                                        
                            </div>
                            <button type="submit" class="btn submitfrm" onclick="return valid();" style="width:100px;margin-left: 650px;visibility:hidden;">Send</button>
                            
                        <!--</form>-->
                        
                    </div>
                </div>

                <div class="col-md-4"></div>
               
            </div>
                                        
                                        </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="click_submit();">Send</button>
                                        <!--<button type="button" class="btn btn-primary" onClick="createcolumn();">Add Column</button>-->
                                    </div>
                                </div>
                            </div>
                        </div>            
                    
                    
    </form>

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jscolor.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>

<!--        <script src="<?php echo base_url(); ?>assets/js/jscolor.js"></script>
jQuery (necessary for Bootstrap's JavaScript plugins) 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
Include all compiled plugins (below), or include individual files as needed 
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>-->
<script>
    
    
    
     function show_invite()
            {
              //  alert("fg");
              
              
              $('#invite_modal').modal('show');
            }
         
     function click_submit()
            {
                $("#btnSubmit").click();
            }    
            
            
         var arr = [];    
      $(document).ready(function () {
          
         
          var value = $("#count_column").val();
          for(var a=0;a<value;a++){
              var y = a+1;
              arr[a] = $('.'+y+" input:first").val()
          }
          
          console.log(arr);
      });
      
             function check_col_name(abc)
            { 
              //  var value;
             
               //alert(value);
               var value = "";
               if($("#first_col").val()){
                 value = $("#first_col").val();
               }
               if($("#col_title").val()){
                   value = $("#col_title").val();
               }
              // alert(value);
            //   alert(arr.length);
             //  return false;
             if(value){
               if(arr.length == 0)
               {arr.push(value);
                   addpop();
               }
               else
               {

                     if(jQuery.inArray(value,arr) >= 0)
                     {
                        // alert("dsfd");
                         alert("Column Name Already Exists");
                          return false;
                     }
                     else
                     {
                          arr.push(value);
                         createcolumn();
                          
                     }



                     }
                 }
                 console.log(arr);
                 
               }
               
               function isNumber(evt) {
      
      
    //  alert("dfdsf");
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
		
		
      
        if (charCode == 32) {
           // alert("Enter only numbers");
            return false;

        }
        return true;
		
    }
    
    
//    function check_Sub(evt) {
//        evt = (evt) ? evt : window.event;
//        var charCode = (evt.which) ? evt.which : evt.keyCode;
//		
//	//alert(charCode);	
//      
//        if ((charCode >= 97  && charCode <= 122) || (charCode >= 65  && charCode <= 90)  || (charCode >= 48  && charCode <= 57) || charCode == 59) {
//           // alert("Enter only numbers");
//            return true;
//
//        }
//        else{
//        return false;
//        }	
//    }
      
//                            function checkUrl(url)
//                            {
//                            //regular expression for URL
//                           // var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
//                           var pattern = /\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
//                            if (pattern.test(url)){
//
//                            return true;
//                            } else {
//                            $.fallr.show({
//                            content: '<p>Url is not valid</p>',
//                                    position: 'center'
//                            });
//                            return false;
//                            }
//                            }
                            $(document).ready(function() {
// Datepicker Popups calender to Choose date.
                            $(function() {
                            $("#datepicker").datepicker();
                            $(".usedatepicker").datepicker();
                         
                            $("#format").change(function() {
                            $("#datepicker").datepicker("option", "dateFormat", $(this).val());
                            $(".usedatepicker").datepicker("option", "dateFormat", $(this).val());
                            });
                            });
                            });
                            $('body').on('focus', ".usedatepicker", function(){
                               $(this).not('.hasDatePicker').datepicker();     
                                $(this).datepicker(); 
                              
                         });
                       $('body').on('focus', ".usedatepicker21", function(){
                               $(this).not('.hasDatePicker').datepicker();     
                                $(this).datepicker(); 
                              
                         });
                            $('.usedatepicker').each(function(){
                                  $(this).not('.hasDatePicker').datepicker();  
                            $(this).datepicker();
                          
                            });
                            
                            
//                            function suggestion(id){
//                            //alert(id);  
//                            var abc111 = '';
//                            $.ajax({
//                            url: "<?php echo base_url(); ?>index.php/Dashboard/auto_sug",
//                                    type: "POST",
//                                    data: {'id': id
//
//                                    },
//                                    success: function (response)
//                                    {console.log(response);
//                                    if (response) {
//
//
//                                    $("#div1").html(response);
//                                    $("#div1").show();
//                                    //abc111 = jQuery.parseJSON(response);
//
//                                    }
//
//                                    }
//
//                            });
//                            }
//                            function suggestion12(id){
//                            //alert(id);  
//                            var abc111 = '';
//                            $.ajax({
//                            url: "<?php echo base_url(); ?>index.php/Dashboard/auto_sug",
//                                    type: "POST",
//                                    data: {'id': id
//
//                                    },
//                                    success: function (response)
//                                    {//console.log(response);
//                                    if (response) {
//
//
//                                    $("#div11").html(response);
//                                    $("#div11").show();
//                                    //abc111 = jQuery.parseJSON(response);
//
//                                    }
//                                    else{
//                                    $("#div11").hide();
//                                    }
//                                    }
//
//                            });
//                            }
//                            function suggestion1(){
//                            $("#div1").hide();
//                            $("#div11").hide();
//                            }
                            function changevalue(){
                            var sug = $('#search').val();
                            document.getElementById('first_row').value = sug;
//document.getElementById('row_title').value=sug;
//$("#div1").hide();
                            }
                            function changevalue1(){
                            var sug = $('#search').val();
                            document.getElementById('row_title').value = sug;
//$("#div1").hide();
                            }
                            
                         
                            var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
                            function ValidateSingleInput(oInput) {
                          
                            if (oInput.type == "file") { 
                            var sFileName = oInput.value;
                            if (sFileName.length > 0) {
                                 
                            var blnValid = false;
                            for (var j = 0; j < _validFileExtensions.length; j++) {
                            var sCurExtension = _validFileExtensions[j];
                            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            break;
                            }
                            
                            }

                            if (!blnValid) {
                            alert("Sorry, File Format is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                            oInput.value = "";
                            return false;
                            }
                            }
                            }
                        
                            return true;
                            }
</script>
<script>




    var gap = 20;
    var boxH = $(window).height() - gap;
    var boxW = $(window).width() - gap * 2;</script>
<script type="text/javascript">

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);
    (function () {
    var ga = document.createElement('script');
    ga.type = 'text/javascript';
    ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(ga, s);
    })();</script>
<script>
    function openpopup()
    {

    $('#myModal').modal('show');
    }
    window.abc = 0;
    function add_row()
    {
     // $(".usedatepicker").removeClass("hasDatepicker"); 
    //   $(".usedatepicker21").removeClass("hasDatepicker");
    var title = document.getElementById('row_title').value;
    if (title != '') {
    var clonehead = $("#mtable thead tr th:last").clone();
      var search_val = document.getElementById('search').value;
    var clonered = $("#mtable tbody tr td:last").clone();
    var clonerow = $("#mtable tbody tr td:first").clone();
    var genrow = $("#mtable tbody tr:last").clone().find('input').val('').end();
    $(".usedatepicker").removeAttr('id');
     $(".usedatepicker21").removeAttr('id');
    var len = $(".rowapp").length + 1;
    //alert(len);
    if (len > 1000) {
    alert("maximum row size is 1000");
    return false
    }
      var row_id = "rowapp" + (+ len - 1);
      //alert(row_id);
            var prev_id = "rowapp"+ (+ len - 2);
           // alert(prev_id);
            $("#"+prev_id+" "+".substitute").removeAttr('id');
    
    genrow.attr('id', "rowapp" + ( + len - 1));
    $('#mtable tbody').append(genrow);
    //var delid = "'rowapp" + (+abc + 1) + "'";
    var delid = "'rowapp" + ( + len - 1) + "'";
    var rtitle = "<input type ='hidden' name='rowname[]' value= " + title + "> <input type ='hidden' class='substitute' id='search_sub'  value= '"+ search_val +"' >";
    $("#mtable tbody tr:last td:first").html(title + rtitle);
    $('#rowapp' + ( + len - 1)).find("td:last").html('<a href="#" onclick ="del(' + delid + ')">DELETE</a>');
     $("#"+row_id+" "+".sub").val($("#search_sub").val());
      $("#"+row_id+" "+"img").css("visibility","hidden");
     $("#mtable tbody tr:last td").find("input:file").addClass("file_val");
    hg();
    
 $("#search_sub").val('');
     
    
    $('#myModal').modal('hide');
    abc++;
    var title = document.getElementById('row_title').value = '';
   
    } else {
    alert('Enter Row Title');
    return false;
    }
    }
    function addpop()
    {
    //var val=  $("#type option:selected").text();
    //console.log(val1);
    var val1 = $('#type').val();
    //var e = document.getElementById("type");
    //var type = e.options[e.selectedIndex].value;

    var column_val_type = val1.toLowerCase();
            if(column_val_type)
            {
                var val = column_val_type;
            }  
            else{
                 var val = "abc";
            }   
    //alert(val);
    var rclass = '"rowapp0"';
    var cclass = '1';
    var frow = document.getElementById('first_row').value;
    var fcol = document.getElementById('first_col').value;
    var search_val = document.getElementById('search').value;
    if ((frow != '') && (fcol != ''))
    {
    var head = "<th></th><th></th><th class='1'><a href='#' onclick='delcol(" + cclass + ")' > DELETE</a></th><th></th>";
    var red = "<td  class='red'></td><td  class='red'>Substitute</td><td  class='1'>" + fcol + "<input type ='hidden' name='colname[]' value= " + fcol + "><input type ='hidden' name='coltype[]' value= " + val + "></td><td > </td>";
   $.ajax({
    url: '<?php echo base_url();?>index.php/Dashboard/show_list_data/first',
    type:"POST",
    data:{'column_value': val,'frow':frow,'fcol':fcol,'rclass':rclass,'search_val':search_val
      },
    success: function(response)
    {
    // alert(response);
	 // return false; 
            $('#headapp').append(head);
            $('#redapp').append(red);
            $('.rowapp').append(response);
            
            $('#first').modal('hide');
            
            
//              $('body').on('focus', ".usedatepicker", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker").removeClass("hasDatepicker");
//    });

    
         $("#del_btn").css("background-color", "#333333"); 
         $("#del_btn").css("border", "1px solid #333333"); 
    }
    
  });  
//    if (val == "date"){
//
//    var row = "<td>" + frow + "<input type ='hidden' name='rowname[]' value= " + frow + "></td><td class='1'><input type='text' name='" + fcol + "val[]' class='form-control usedatepicker inputform r0 c0' readonly  placeholder='date'><input type='hidden' name='" + fcol + "valtype[]' class=' form-control inputform r0 c0 ' value='" + val + "'></td> <td  ><a href='#' onclick ='del(" + rclass + ");'>DELETE</a></td>";
//    $(".usedatepicker").removeClass("hasDatepicker");
//     
//    }
//    else if (val == "image"){
//
//    var row = "<td>" + frow + "<input type ='hidden' name='rowname[]' value= " + frow + "></td><td class='1'><input type='file' name='" + fcol + "val[]' class=' form-control inputform r0 c0 '  placeholder='image' id='file' onchange='ValidateSingleInput(this);'><input type='hidden' class=' form-control inputform r0 c0 ' name='" + fcol + "valtype[]' value='" + val + "'></td> <td  ><a href='#' onclick ='del(" + rclass + ");'>DELETE</a></td>";
//    }
//    else if (val == 'link'){
//
//    var row = "<td>" + frow + "<input type ='hidden' name='rowname[]' value= " + frow + "></td><td class='1'><input type='text' name='" + fcol + "val[]' class='form-control abcurl inputform r0 c0' id='url' placeholder='enter Url' onblur='checkUrl(this.value);'><input type='hidden' class=' form-control inputform r0 c0 ' name='" + fcol + "valtype[]' value='" + val + "'> </td> <td  ><a href='#' onclick ='del(" + rclass + ");'>DELETE</a></td>";
//    }
//    else if (val == 'state'){
//
//
//    var row = <?php echo '"<td>"' ?> + frow + <?php echo '"<input type=' ?><?php echo "hidden" ?> <?php echo 'name=' ?><?php echo '"'; ?> + fcol + "valtype[]" +<?php echo '"'; ?><?php echo ' value=' ?><?php echo '"'; ?> + val +<?php echo '"'; ?><?php echo '><input type=' ?><?php echo "hidden"; ?><?php echo ' name=' ?><?php echo "rowname[]"; ?><?php echo' value=' ?><?php echo '"'; ?> + frow +<?php echo '"'; ?><?php echo' ></td><td><select  class=' ?><?php echo "'form-control inputform r0 c0'" . 'name="'; ?> + fcol + 'val[]' + <?php echo '"'; ?>  <?php echo '>'; ?><?php foreach ($state as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td><td><a href=# onclick=' ?><?php echo 'del(' ?> + rclass +<?php echo');' ?><?php echo'>DELETE</a></td>"'; ?>
//
//
//    }
//    else if (val == 'country'){
//    var row = <?php echo '"<td>"' ?> + frow + <?php echo '"<input type=' ?><?php echo "hidden" ?> <?php echo 'name=' ?><?php echo '"'; ?> + fcol + "valtype[]" +<?php echo '"'; ?><?php echo ' value=' ?><?php echo '"'; ?> + val +<?php echo '"'; ?><?php echo '><input type=' ?><?php echo "hidden"; ?><?php echo ' name=' ?><?php echo "rowname[]"; ?><?php echo' value=' ?><?php echo '"'; ?> + frow +<?php echo '"'; ?><?php echo' ></td><td><select  class=' ?><?php echo "'form-control inputform r0 c0'" . 'name="'; ?> + fcol + 'val[]' + <?php echo '"'; ?>  <?php echo '>'; ?><?php foreach ($country as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td><td><a href=# onclick=' ?><?php echo 'del(' ?> + rclass +<?php echo');' ?><?php echo'>DELETE</a></td>"'; ?>
//
//    }
//    else {
//
//    var row = "<td>" + frow + "<input type ='hidden' name='rowname[]' value= " + frow + "></td><td class='1'><input type='text' name='" + fcol + "val[]' class='form-control inputform r0 c0' id='exampleInputEmail3' placeholder='enter text'> <input type='hidden' class=' form-control inputform r0 c0 ' name='" + fcol + "valtype[]' value='" + val + "'></td> <td  ><a href='#' onclick ='del(" + rclass + ");'>DELETE</a></td>";
//    }
//
//    $('#headapp').append(head);
//    $('#redapp').append(red);
//    $('#rowapp0').append(row);
//    $('#first').modal('hide');
    } else {
    alert('Enter Row and Column Title');
    return false;
    }

    }
    function create_col()
    {

    $('#headapp').each(function () {
    if ($(this).find('th').length == 0) {
    $('#first').modal('show');
    $('#first_row').val("");
    $('#first_col').val("");
    } else
    {
    $('#colModal').modal('show');
    }
    
    // $(".usedatepicker").removeClass("hasDatepicker");
      //$(".usedatepicker21").removeClass("hasDatepicker");
    });
//    
//      $('body').on('focus', ".usedatepicker", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker").removeClass("hasDatepicker");
//      });
//      
//     $('body').on('focus', ".usedatepicker21", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker21").removeClass("hasDatepicker");
//     });
    }


    function createcolumn() {
    var val1 = $('#type1').val();
    //var e = document.getElementById("type");
    //var type = e.options[e.selectedIndex].value;

    var column_val_type = val1.toLowerCase();
            if(column_val_type)
            {
                var val = column_val_type;
            }  
            else{
                 var val = "abc";
            }   
    //alert(val);
    var col = document.getElementById('col_title').value;
    if (col != '') {
    var len = $("#mtable thead tr th");
    if (len.length > '11') {
    alert("Maximum column size is 10");
    return false;
    }
    var clonerow = $(".rowapp td:last").clone();
    var clonered = $("#redapp td:last").clone();
    var count = document.getElementById('redapp').getElementsByTagName("td").length;
    var count = count - 2;
    var clonered = "<td class= "+ count + ">" + col + "<input type ='hidden' name='colname[]' value= " + col + "><input type ='hidden' name='coltype[]' value =" + val +">";
    var clonehead = '<th class = ' + count + '><a href="#" onclick="delcol(' + count + ')" > DELETE</a></th>';
    $('#headapp').find("th:last").before(clonehead);
    $('#redapp').find("td:last").before(clonered);
    $('.rowapp').each(function () {
       
    var count1 = document.getElementById(this.id).getElementsByTagName("td").length;
    var row = document.getElementById(this.id);
    var x = row.insertCell(count1 - 1);
    x.className = count;
    
     $.ajax({
    url: '<?php echo base_url();?>index.php/Dashboard/show_list_data/column',
    type:"POST",
    data:{'column_value': val,
           'rowid':$(this).prop('id'),
           'colclass':count,
            'col':col
        },
    success: function(response)
    {
      // alert(response);
	 x.innerHTML = response;  
//         $('body').on('focus', ".usedatepicker", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker").removeClass("hasDatepicker");
//    });
//    $('body').on('focus', ".usedatepicker21", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker21").removeClass("hasDatepicker");
//    });
    }
    
  });
//    if (val == "date"){
//
//    x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='" + val + "'><input type='text' name='" + col + "val[]' class='form-control usedatepicker inputform r0 c0' readonly  placeholder='select date'> ";
// 
//    }
//    else if (val == "image"){
//    x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='" + val + "'> <input type='file' name='" + col + "val[]' class='form-control inputform r0 c0'  onchange='ValidateSingleInput(this);'> ";
//    }
//    else if (val == "link"){
//    x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='" + val + "'><input type='text' name='" + col + "val[]' class='form-control abcurl inputform r0 c0'  placeholder='enter Url' onblur='checkUrl(this.value);'> ";
//    }
//    else if (val == "state"){
//
//    x.innerHTML = <?php echo '"<td><input type=' ?><?php echo "hidden" ?><?php echo ' name='; ?><?php echo '"'; ?> + col + "valtype[]" +<?php echo '"'; ?><?php echo ' value='; ?><?php echo '"'; ?> + val +<?php echo '"'; ?><?php echo '><select class=' ?><?php echo "'form-control inputform r0 c0'" . 'name="'; ?> + col + 'val[]' + <?php echo '"'; ?>  <?php echo '>'; ?><?php foreach ($state as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
    // x.innerHTML= <?php echo '"<td>"' ?><?php echo '"<input type=' ?><?php echo "hidden" ?> <?php echo 'name=' ?><?php echo '"'; ?>+col+"valtype[]"+<?php echo '"'; ?><?php echo ' value=' ?><?php echo '"'; ?>+val+<?php echo '"'; ?><?php echo' ></td><td><select  class=' ?><?php echo "'form-control inputform r0 c0'" . 'name="'; ?> + col + 'val[]' + <?php echo '"'; ?>  <?php echo '>'; ?><?php foreach ($state as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
//
//    }
//    else if (val == "country"){
//
//    x.innerHTML = <?php echo '"<td><input type=' ?><?php echo "hidden" ?><?php echo ' name='; ?><?php echo '"'; ?> + col + "valtype[]" +<?php echo '"'; ?><?php echo ' value='; ?><?php echo '"'; ?> + val +<?php echo '"'; ?><?php echo '><select class=' ?><?php echo "'form-control inputform r0 c0'" . 'name="'; ?> + col + 'val[]' + <?php echo '"'; ?>  <?php echo '>'; ?><?php foreach ($country as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
    // x.innerHTML= <?php echo '"<td>"' ?><?php echo '"<input type=' ?><?php echo "hidden" ?> <?php echo 'name=' ?><?php echo '"'; ?>+col+"valtype[]"+<?php echo '"'; ?><?php echo ' value=' ?><?php echo '"'; ?>+val+<?php echo '"'; ?><?php echo' ></td><td><select  class=' ?><?php echo "'form-control inputform r0 c0'" . 'name="'; ?> + col + 'val[]' + <?php echo '"'; ?>  <?php echo '>'; ?><?php foreach ($country as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
//
//    }
//    else {
//
//    x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='" + val + "'><input type='text' name='" + col + "val[]' class='form-control inputform r0 c0' id='exampleInputEmail3' placeholder=''> ";
//    }

    // x.innerHTML = "<td><input type='text' name='" + col + "val[]' class='form-control inputform r0 c0' id='exampleInputEmail3' placeholder=''> ";





    });
    document.getElementById('col_title').value = '';
    $('#colModal').modal('hide');
    } else {
    alert('Enter Column Title');
    return false;
    }
    }


    function create_row()
    {

    $('#headapp').each(function () {
    if ($(this).find('th').length == 0) {
    $('#first').modal('show');
     $('#first_row').val("");
    $('#first_col').val("");
    } else
    {
    $('#myModal').modal('show');
    }
    });
      $(".usedatepicker").removeClass("hasDatepicker");
      $(".usedatepicker21").removeClass("hasDatepicker");
              $("#search").val("");
    }
    function del(x)
    {
    a = x;
    $.fallr.show({
    buttons: {
    button1: {text: 'Yes', danger: true, onclick: clicked2},
            button2: {text: 'Cancel'}
    },
            content: '<p>Are you sure you want to delete?</p>',
            icon: 'error',
            position: 'center'
    });
    }

    var clicked2 = function () {
   
            
     //alert(re);
            
           // alert($('#mtable').find('tr').length);        
            
    var re = ''
      //alert($('#mtable').find('tr').length);
            //if ($('.rowapp').find('td').length == 3) {
            if ($('#mtable').find('tr').length == 3) {
                
                arr = [];
  

    $('.red').empty();
    $('.rowapp').empty();
    $('#headapp').empty();
    re = 1;
    a = '';
    } else {
    var row = document.getElementById(a);
    row.parentNode.removeChild(row);
    re = 1;
    a = '';
    }
    if (re == 1) {
    $.fallr.hide();
    a = '';
    }
   
       // console.log(arr);
        set_delete();
    };
    var clicked = function () {
        
       // alert($('.red').find('td').length);
    var col_name =  $('.'+a+" input:first").val();
    //   alert(col_name);
             var index = $.inArray(col_name, arr);
             if (index != -1)
             {    arr.splice(index, 1);
             }
            
             console.log(arr);
    var re = '';
    if ($('.red').find('td').length == 4) {
    $('.red').empty();
    $('.rowapp').empty();
    var count_class  = document.getElementsByClassName("rowapp");
    for(var x = 1;x <= count_class.length ; x++){
        if(document.getElementById('rowapp'+x)){
      var elem = document.getElementById('rowapp'+x);
    elem.parentNode.removeChild(elem);
        }
    }
    $('#headapp').empty();
    re = 1;
    a = '';
    } else {
    $('.' + a).remove();
    re = 1;
    a = '';
    }


    if (re == 1) {
    $.fallr.hide();
    a = '';
    }
        set_delete();
    };
    window.a = '';
    function delcol(y)
    {
    a = y;
    $.fallr.show({
    buttons: {
    button1: {text: 'Yes', danger: true, onclick: clicked},
            button2: {text: 'Cancel'}
    },
            content: '<p>Are you sure you want to delete?</p>',
            icon: 'error',
            position: 'center'
    });
    }
//    function validate() {
//    var lib = document.getElementById('lib_name').value;
//    var lib_group = document.getElementById('lib_group').value;
//    var access = document.getElementById('access').value;
//    var desc = document.getElementById('desc').value;
//    var count = document.getElementById('redapp').getElementsByTagName("td").length;
//    if (lib == '') {
//    //alert('Enter Library Name');
//    $.fallr.show({
//    content: '<p>Enter Library Name</p>',
//            position: 'center'
//    });
//    return false;
//    }
//    if (lib_group == '') {
//
//    $.fallr.show({
//    content: '<p>Enter Library Group Name</p>',
//            position: 'center'
//    });
//    return false;
//    }
//
//
//    if (desc == '') {
//
//    $.fallr.show({
//    content: '<p>Enter Library Description</p>',
//            position: 'center'
//    });
//    return false;
//    }
//    if (access == '') {
//
//    $.fallr.show({
//    content: '<p>Enter Access Type</p>',
//            position: 'center'
//    });
//    return false;
//    }
//    if (count == '0') {
//
//    $.fallr.show({
//    content: '<p>Enter Row and Colunm</p>',
//            position: 'center'
//    });
//    return false;
//    }
//    var lib1 = document.getElementById('url').value;
//    if (lib1 == '') {
//    //alert('Enter Library Name');
//    $.fallr.show({
//    content: '<p>Enter valid URL</p>',
//            position: 'center'
//    });
//    return false;
//    }
//
//    }

       function validate() {
            var lib = document.getElementById('lib_name').value;
            var color = document.getElementById('color').value;
            var lib_group = document.getElementById('lib_group').value;
            var access = document.getElementById('access').value;
            var desc = document.getElementById('desc').value;
            var count = document.getElementById('redapp').getElementsByTagName("td").length;
//            if(document.getElementsByClassName('file_val')){
//                 var file = document.getElementsByClassName('file_val');
//               for( var i = 0;i < file.length;i++)
//               {
//                   if(file[i].value == "")
//                   {
//                      $.fallr.show({
//                        content: '<p>Enter Image</p>',
//                                position: 'center'
//                        });
//                        return false;  
//                   }
//               }
//            }
           if(document.getElementsByClassName('textbx')){
               var textbx = document.getElementsByClassName('textbx'); 
               
               for( var t = 0;t < textbx.length ; t++)
               {
                   if(textbx[t].value == "")
                   {
                      $.fallr.show({
                        content: '<p>Enter Text</p>',
                                position: 'center'
                        });
                        return false;  
                   }
               }
           }
           
            if (lib == '') {
            //alert('Enter Library Name');
            $.fallr.show({
            content: '<p>Enter Library Name</p>',
                    position: 'center'
            });
            return false;
            }
           
             if (color == '') {

            $.fallr.show({
            content: '<p>Please Select Color</p>',
                    position: 'center'
            });
            return false;
            }
            
               if (lib_group == '') {

            $.fallr.show({
            content: '<p>Enter Library Group Name</p>',
                    position: 'center'
            });
            return false;
            }
           
              if (access == '') {

            $.fallr.show({
            content: '<p>Enter Access Type</p>',
                    position: 'center'
            });
            return false;
            }
          
              if (desc == '') {

            $.fallr.show({
            content: '<p>Enter Library Description</p>',
                    position: 'center'
            });
            return false;
            }
          
             if (count == '0') {

            $.fallr.show({
            content: '<p>Enter Row and Colunm</p>',
                    position: 'center'
            });
            return false;
            }
       

        
//          if (textbx == '') {
//                //  alert("fddg");
//             $.fallr.show({
//            content: '<p>Enter Text</p>',
//                    position: 'center'
//            });
//            return false;
//            }
           
           if(document.getElementsByClassName('open')){
           var open_val = document.getElementsByClassName('open');
           
              for(var op=0;op< open_val.length ; op++)
              {
                     if(open_val[op].value == "")
                     {
                           $.fallr.show({
                            content: '<p>Enter Text</p>',
                                     position: 'center'
                             });
                             return false;
                     }
              }
           
           }
           
           
           if(document.getElementsByClassName('usedatepicker')){
            var date_pick =  document.getElementsByClassName('usedatepicker');
                  
                  for(var d=0 ; d<date_pick.length ; d++){
                     if(date_pick[d].value == ""){
                      $.fallr.show({
                            content: '<p>Enter Date</p>',
                                     position: 'center'
                             });
                             return false;
                     }
                  }
           }
           
           if(document.getElementsByClassName('usedatepicker21')){
            var date_pick1 =  document.getElementsByClassName('usedatepicker21');
                  
                  for(var d1=0 ; d1<date_pick1.length ; d1++){
                     if(date_pick1[d1].value == ""){
                      $.fallr.show({
                            content: '<p>Enter Date</p>',
                                     position: 'center'
                             });
                             return false;
                     }
                  }
           }
           
           
            var lib1 = document.getElementsByClassName('abcurl')
            for( var l=0; l< lib1.length ; l++){
            if (lib1[l].value != '') {
             // alert("dfg");
            var pattern = /\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
                    if (pattern.test(lib1[l].value)){
                   
                   // return true;
                    } else {
                    
                    
           $.fallr.show({
            content: '<p>Url is not valid</p>',
                    position: 'center'
            });
            return false;
                    //alert("Invalid Url");
                   // return false;gg
                    }

            }
            else
            {
                //alert("no");
              $.fallr.show({
            content: '<p>Enter Url</p>',
                    position: 'center'
            });
             return false;
            }
            
            } 
           
            if(document.getElementsByClassName('dropdown')){
            var dropdown = document.getElementsByClassName('dropdown');
            
               for( var d = 0;d<dropdown.length;d++){
               
                 if(dropdown[d].value == ""){
                                        $.fallr.show({
                              content: '<p>Enter List</p>',
                                      position: 'center'
                              });
                               return false;
                     
                 }
               }
            }

            }





    var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
    function ValidateSingleInput(oInput) {
    if (oInput.type == "file") {
    var sFileName = oInput.value;
    if (sFileName.length > 0) {
    var blnValid = false;
    for (var j = 0; j < _validFileExtensions.length; j++) {
    var sCurExtension = _validFileExtensions[j];
    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
    blnValid = true;
    break;
    }
    }

    if (!blnValid) {
    alert("Sorry, File Format is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
    oInput.value = "";
    return false;
    }
    }
    }
    return true;
    }
    $(document).ready(function () {
    $('.abc').change(function () {
    var val = $(this).val().toLowerCase();
    var regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|bmp|ppt|xls)$");
    if (!(regex.test(val))) {
    $(this).val('');
    alert('Please select correct file format');
    } }); });
    
    

//$('#btnSubmit').click(function() {
//    var exit = false;
//    $('input.usedatepicker').each(function() {
//        if ($(this).val().length == 0) {
//             $.fallr.show({
//            content: '<p>Enter Date</p>',
//                    position: 'center'
//            });
//            exit = true;
//            return false;
//        }
//         //regular expression for URL
//           
//    });
//
//    if( exit ) {
//        return false;
//    }
//
//    //alert("This alerts even when loop returns false");
//});
//$('#btnSubmit').click(function() {
//    var exit = false;
//    $('input.inputform').each(function() {
//        if ($(this).val().length == 0) {
//             $.fallr.show({
//            content: '<p>Enter Value</p>',
//                    position: 'center'
//            });
//            exit = true;
//            return false;
//        }
//         //regular expression for URL
//           
//    });
//
//    if( exit ) {
//        return false;
//    }
//
//    //alert("This alerts even when loop returns false");
//});
//$('body').on('focus', ".usedatepicker", function(){
//            $(this).datepicker(); });
//            $('.usedatepicker').each(function(){
//            $(this).datepicker();
//            });

 //$(".usedatepicker").removeClass("hasDatepicker");
$( document ).on( 'focus', ':input', function(){
        $( this ).attr( 'autocomplete', 'off' );
    });
    $('.kr').on('change',function() {
//alert();
 $('#'+$(this).closest('tr').prop('id')+' .'+$(this).closest('td').prop('class')+' .ki').val('');
});
function hg()
{
    //$(param).closest('tr').prop('id');
   // alert($("#mtable tbody tr:last").closest('td').prop('class'));
     $("#mtable tbody tr:last td").each(function () { 
         if(typeof $(this).find("input:file").prop('name') !== "undefined")
         {
             var val3="val3"+$("#mtable tbody tr:last").prop('id')+$(this).prop('class')+"[]";
             var val="val"+$("#mtable tbody tr:last").prop('id')+$(this).prop('class')+"[]";
            // alert(val3+val);
            // alert($(this).prop('class'));
            $(this).find(".ki").attr("name", val3);
              $(this).find(".kr").attr("name", val);
         }
         
     });
}


function set_delete()
{
    
       // alert($("#mtable thead tr th").length);
        if($("#mtable thead tr th").length != 0){
            
             //alert("cdgf");
             
             
             
    var dd=0; $("#mtable tbody tr").each(function () 
    
           { 
           if(dd!='0'){
               $(this).removeAttr('id');
               var row_id = "rowapp"+ (dd-1);
               
               $(this).attr("id",row_id);
               var quote_str =  "'" + row_id + "'";
              $(this).find("td:last").html('<a href="#" onclick ="del(' + quote_str + ')">DELETE</a>');
           }
       //alert($(this).find("td").length);
       set_id(this);
      
               
         
  
    
         dd=dd+1; 
    });
    
    
     set_id21($("#mtable thead tr"));
    }
    else
    {
         arr = [];
         $('.red').empty();
            $('.rowapp').empty();
            $('#headapp').empty();
    }
}

 function set_id(par)
 {
    // alert("qwe");
       var yy=0;
        $(par).find("td").each(function (){
              if(yy>1 && yy<= ($("#mtable thead tr th").length - 2) ){
             
              $(this).removeAttr('class');            
             var x = yy-1;
             $(this).attr("class",x);
            
            
            
               
           
             }
           if(typeof $(this).find("input:file").prop('name') !== "undefined")
                 {
                   //alert();
                 
                      var val1="val"+$(par).attr('id')+$(this).prop('class')+"[]";
                            // alert(val3+val);
                           //  alert($(this).prop('class'));
                      var val4="val3"+$(par).attr('id')+$(this).prop('class')+"[]";
            
            // alert(val3+val);
            // alert($(this).prop('class'));
            $(this).find(".ki").attr("name", val4);
             
                    $(this).find(".kr").attr("name", val1);
                 }

          yy++;
          });
 }
  
   function set_id21(par)
 {
    // alert("qwe");
       var yy=0;
        $(par).find("th").each(function (){
              if(yy>1 && yy<= ($("#mtable thead tr th").length - 2) ){
             
               $(this).removeAttr('class');            
             var x = yy-1;
             $(this).attr("class",x);
               
             $(this).html("<a href='#' onclick='delcol("+x+")'> DELETE</a>");
             
           
             }
          

          yy++;
          });
 }      
</script>
</body>
</html>