
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
         <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">

                        </div>
                       <div class="col-md-2 col-xs-4">
                            <div class="dropdown" style="text-align:right;">
                                <button class="btn logdrop dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> <?php echo ucwords($this->session->userdata('username')); ?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left:initial;">
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/dashboard">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/change_pass">Change Password</a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/edit_profile/<?php echo $this->session->userdata('user_id'); ?>">Change Profile</a></li>
                                    <!--<li><a href="<?php echo base_url(); ?>index.php/Dashboard/column_type">Column Type</a></li>-->
                                      <li><a href="<?php echo base_url(); ?>index.php/Dashboard/column_list">List</a></li>
<!--                                    <li><a href="<?php echo base_url('Dashboard'); ?>/groups">Groups</a></li>-->
                                    <li><a href="<?php echo base_url('Welcome'); ?>/logout">Logout</a></li>
                                </ul>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php echo $banner[4]['general_value']?> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
               
                    <div class="container">
                         <?php if ($this->session->flashdata('per_message')) { ?>
                            <div class="alert alert-block alert-success fade in" style="padding:5px;background-color: red">
                                        <a class="close" data-dismiss="alert" onclick="return closemsg();" href="#" 
                                           aria-hidden="true">X</a>
                                <h4 style="margin-bottom: 0;color:white;" ><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('per_message');
                                            ?>  <i class="fa fa-thumbs-down"></i></h4>
                                    </div>
                                    <?php
                                }?>
                        <?php if ($this->session->flashdata('flash_message')) { ?>
                            <div class="alert alert-block alert-success fade in" style="padding:5px;">
                                        <a class="close" data-dismiss="alert" onclick="return closemsg();" href="#" 
                                           aria-hidden="true">X</a>
                                        <h4 style="margin-bottom: 0;"><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('flash_message');
                                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                                    </div>
                                    <?php
                                }?>
                        <?php if($groupedit == ''){?>
                         <form role="form" action="<?php echo base_url(); ?>Welcome2/add_columntype" method = "post">
<!--                        <div class="row">
                           
                            <div class="col-md-2">

                            </div>
                            <div class="col-md-6">
                                <input type="button" value="Column Type" class="searchbutton" />
                                <input type="text" name="group_name" id="grp" class="form-control addgroup"/>
                            </div>
                            
                             <div class="col-md-2">
                                <button type="submit" onclick="return chkgrp();" class="btn liberary">+ CREATE NEW TYPE</button>
                                
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>-->
                             </form>
                        <?php }?>
                        
                     
                      
                       
                        
                        <div class="row">
                          
                          
                            <div class="col-md-12">
                                <h4 class="mnage">COLUMN TYPE</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped mytable">
                                        <thead>
                                        
                                        <th style="width:15%;">COLUMN TYPE</th>
                                        <th style="width:15%;">TYPE</th>
                                      
                                        </thead>
                                        <tbody>
                                             <?php  $groups=$this->db->get_where("column_type")->result_array();
                                
                                             $i =1;foreach($groups as $group) {?>
                                            <tr>  
                                                
                                                <td style="width:15%;"><?php echo $group['name'];?> </td>
                                                <td style="width:15%;"><?php $type_data = $this->db->get_where('type',array('id'=>$group['type']))->result_array();
                                                                      echo $type_data[0]['value'];
                                                ?> </td>
                                               
                                            </tr>
                                             <?php $i++;}?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                           
                        </div>

                    </div>
               
            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>




                                            var gap = 20;
                                            var boxH = $(window).height() - gap;
                                            var boxW = $(window).width() - gap * 2;
                                           


        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <script>
             window.cc = '';

            function deletedata(x) {
           // alert(x);
            cc = x;
            //alert(cc);
                $.fallr.show({
                    buttons: {
                        button1: {text: 'Yes', danger: true, onclick: clicked},
                        button2: {text: 'Cancel'}
                    },
                    content: '<p>Are you sure you want to delete?</p>',
                    icon: 'error',
                    position: 'center'
                });

            }
            var clicked = function () {
           
          // alert(cc);return false;
                 var post_data = {
                    'id': cc,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/Welcome2/delete_columntype",
                    data: post_data,
                    success: function (data) {
                       // alert(data);
                        if(data == "2")
                        {
                            alert("Assigned to library so cannot delete it");
                             $.fallr.hide();
                            cc = ''; 
                        }
                       
                        if (data == "1") {
                            
                                window.location = "<?php echo base_url() ?>index.php/Welcome2/add_column_type/";
                                $.fallr.hide();
                            cc = ''; 
                             
                             
                        }
                        
                    }
                });
               


            };
            function chkgrp()
            {
              var grp =  document.getElementById('grp').value;
              if(grp == ''){
                   $.fallr.show({
                        content: '<p>Type is Required</p>',
                        position: 'center'
                    });
            return false;
              }
            }
            </script>
    </body>
</html>