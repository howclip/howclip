<html lang="en">
  <head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Chrome Extension</title>
  <!-- Bootstrap -->
  <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
  </head>
  <body>
  <div  class="wrapper">
  <div  class="header">
  <div class="container">
  <div class="row">
   <div class="col-md-3 col-xs-12">
  <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
  </div>
   <div class="col-md-7 col-xs-6">
  <img src="<?php echo base_url(); ?>images/create.png" class="img-responsive crea">
  
  </div>
  <div class="col-md-2 col-xs-6">
  <div class="dropdown" style="text-align:right;">
   <button class="btn logdrop dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
   <img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> Demo
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left:initial;">
  <li><a href="#">Change Profile</a></li>
  <li><a href="<?php echo base_url('Dashboard'); ?>/logout">Logout</a></li>
  <li><a href="<?php echo base_url('Dashboard'); ?>/groups">Groups</a></li>
  </ul>
  </div>
  </div>
  </div>
  </div>