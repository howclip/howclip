<?php
$name = $this->session->userdata('user_firstname');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
         <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">

                        </div>
                        <div class="col-md-2 col-xs-4">
<div class="list-group">
            <a href="#menupos1" class="list-group-item logdrop" data-toggle="collapse" data-parent="#mainmenu"><img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> Demo <span class="menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
            <div class="collapse pos-absolute" id="menupos1">
              
             <a href="<?php echo base_url('Welcome2'); ?>/dashboard" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Home</a>
             <a href="<?php echo base_url('Welcome2'); ?>/change_pass" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Password</a>
             <a href="<?php echo base_url('Welcome2'); ?>/edit_profile/<?php echo $this->session->userdata('user_id'); ?>" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Profile</a>
             <a href="<?php echo base_url('Welcome2'); ?>/general_setting" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Setting</a> 
                        <a href="<?php echo base_url('Welcome2'); ?>/add_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Column Type</a>
             <a href="<?php echo base_url('Welcome2'); ?>/all_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">List</a>
             <!--<a href="#submenu1" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Column Field<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>-->
<!--                  <div class="collapse list-group-submenu" id="submenu1">
                    <a href="<?php echo base_url('Welcome2'); ?>/library_country" class="list-group-item sub-sub-item" data-parent="#submenu1">Country</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_state" class="list-group-item sub-sub-item" data-parent="#submenu1">State</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_link" class="list-group-item sub-sub-item" data-parent="#submenu1">Link Type</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_image" class="list-group-item sub-sub-item" data-parent="#submenu1">Image Type</a>
                  </div>-->
             
                  <a href="<?php echo base_url('Welcome2'); ?>/all_library" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Library</a>
             <a href="#submenu2" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Email<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu2">
                    <a href="<?php echo base_url('Welcome2'); ?>/email_template" class="list-group-item sub-sub-item" data-parent="#submenu1">Invite Email</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/user_invite_email" class="list-group-item sub-sub-item" data-parent="#submenu1">User Email</a>
                   
                  </div>
             <a href="#submenu3" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Add Record<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu3">
                    <a href="<?php echo base_url('Welcome2'); ?>/occupation" class="list-group-item sub-sub-item" data-parent="#submenu1">Occupation</a>
<!--                    <a href="<?php echo base_url('Welcome2'); ?>/groups" class="list-group-item sub-sub-item" data-parent="#submenu1">Groups</a>-->
                    <a href="<?php echo base_url('Welcome2'); ?>/link" class="list-group-item sub-sub-item" data-parent="#submenu1">Links</a>
                   
                  </div>
              <a href="<?php echo base_url('Welcome2'); ?>/mail_settings" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Mail Settings</a> 
             <a href="<?php echo base_url('Welcome'); ?>/logout" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Logout</a>    
              </div>
            </div>
                        </div>
                    </div>
                </div>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php 
                                
                                //print_r($banner);
                                  echo $banner[4]['general_value'];    
                                ?>  </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
               
                    <div class="container">
                         <?php if ($this->session->flashdata('per_message')) { ?>
                            <div class="alert alert-block alert-success fade in" style="padding:5px;background-color: red">
                                        <a class="close" data-dismiss="alert" onclick="return closemsg();" href="#" 
                                           aria-hidden="true">X</a>
                                <h4 style="margin-bottom: 0;color:white;" ><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('per_message');
                                            ?>  <i class="fa fa-thumbs-down"></i></h4>
                                    </div>
                                    <?php
                                }?>
                        <?php if ($this->session->flashdata('flash_message')) { ?>
                            <div class="alert alert-block alert-success fade in" style="padding:5px;">
                                        <a class="close" data-dismiss="alert" onclick="return closemsg();" href="#" 
                                           aria-hidden="true">X</a>
                                        <h4 style="margin-bottom: 0;"><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('flash_message');
                                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                                    </div>
                                    <?php
                                }?>
                        <?php if($groupedit == ''){?>
                         <form role="form" action="<?php echo base_url(); ?>Welcome2/add_groups" method = "post">
                        <div class="row">
                           
                            <div class="col-md-2">

                            </div>
                            <div class="col-md-6">
                                <input type="button" value="Group Name" class="searchbutton" />
                                <input type="text" name="group_name" id="grp" class="form-control addgroup"/>
                            </div>
                            
                             <div class="col-md-2">
                                <button type="submit" onclick="return chkgrp();" class="btn liberary">+ CREATE NEW GROUP</button>
                                
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                             </form>
                        <?php }?>
                         <?php if($groupedit != ''){?>
                        <?php foreach($groupedit as $gr){?>
                         <form role="form" action="<?php echo base_url(); ?>Welcome2/add_groups/<?php echo $gr->id;?>" method = "post">
                        <div class="row">
                            
                            <div class="col-md-2">

                            </div>
                            <div class="col-md-6">
                                <input type="submit" value="Group Name" class="searchbutton" />
                                <input type="text" name="group_name" id="grp"  class="form-control addgroup" value="<?php echo $gr->name;?>"/>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn liberary" style="height: 50px;"> SAVE GROUP</button>
                                
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                        <?php }?>
                         </form>
                        <?php }?>
                        
                        <div class="row">
                          
                          
                            <div class="col-md-12">
                                <h4 class="mnage">GROUP NAME</h4>
                                <div class="table-responsive">
                                    <table class="table table-striped mytable">
                                        <thead>
<!--                                        <th style="width:15%;">S no. </th>-->
                                        <th style="width:15%;">Name </th>
                                        <th style="width:15%;">Action </th>
                                        </thead>
                                        <tbody>
                                             <?php $i =1;foreach($groups as $group) {?>
                                            <tr>  
<!--                                                <td style="width:15%;"><?php echo $i;?> </td>-->
                                                <td style="width:15%;"><?php echo $group->name;?> </td>
                                                <td style="width:15%;"><a href="<?php echo base_url(); ?>Welcome2/edit_group/<?php echo $group->id;?>" class="editbut" >EDIT / </a>
                                                    <a href="#" onclick="return deletedata(<?php echo $group->id;?>);" class="editbut" >DELETE</a></td>
                                            
                                            </tr>
                                             <?php $i++;}?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                           
                        </div>

                    </div>
               
            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                             <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>




                                            var gap = 20;
                                            var boxH = $(window).height() - gap;
                                            var boxW = $(window).width() - gap * 2;
                                           


        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <script>
             window.cc = '';

            function deletedata(x) {
            
            cc = x;
                $.fallr.show({
                    buttons: {
                        button1: {text: 'Yes', danger: true, onclick: clicked},
                        button2: {text: 'Cancel'}
                    },
                    content: '<p>Are you sure you want to delete?</p>',
                    icon: 'error',
                    position: 'center'
                });

            }
            var clicked = function () {
           
           
                 var post_data = {
                    'id': cc,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/Welcome2/delete_group",
                    data: post_data,
                    success: function (data) {
                       
                        if (data.length > 0) {
                            
                                window.location = "<?php echo base_url() ?>index.php/Welcome2/groups/";
                                $.fallr('hide');
                            cc = ''; 
                             
                             
                        }
                        
                    }
                });
               


            };
            function chkgrp()
            {
              var grp =  document.getElementById('grp').value;
              if(grp == ''){
                   $.fallr.show({
                        content: '<p>Group Name is Required</p>',
                        position: 'center'
                    });
            return false;
              }
            }
            </script>
    </body>
</html>