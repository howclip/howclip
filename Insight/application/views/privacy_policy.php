
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
          <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
   
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">

                        </div>
                        <div class="col-md-2 col-xs-4">
                            <div class="dropdown" style="text-align:right;">
<!--                                <button class="btn logdrop dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> <?php echo ucwords($this->session->userdata('username')); ?>
                                    <span class="caret"></span>
                                </button>-->
<!--                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left:initial;">
                                    <li><a href="<?php echo base_url('Welcome2'); ?>/dashboard">Home</a></li>
                                    <li><a href="<?php echo base_url('Welcome2'); ?>/change_pass">Change Password</a></li>
                                    <li><a href="<?php echo base_url('Welcome2'); ?>/edit_profile/<?php echo $this->session->userdata('user_id'); ?>">Change Profile</a></li>
                                    <li><a href="<?php echo base_url('Welcome2'); ?>/groups">Groups</a></li>
                                     <li><a href="<?php echo base_url('Welcome2'); ?>/general_setting">General Setting</a></li>
                                    <li><a href="<?php echo base_url('Welcome'); ?>/logout">Logout</a></li>
                                </ul>-->
                            </div>



                        </div>
                    </div>
                </div>
                 <?php if ($this->session->flashdata('flash_message')) { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="#" 
                                           aria-hidden="true">X</a>
                                        <h4><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('flash_message');
                                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                                    </div>
                                    <?php
                                }
                                if ($this->session->flashdata('permission_message')) {
                                    ?>
                                    <div class="alert alert-block alert-warning fade in">
                                        <a class="close" data-dismiss="alert" href="#" 
                                           aria-hidden="true">X</a>
                                        <h5> <i class="fa fa-frown-o"></i> <?php
                                            echo
                                            $this->session->flashdata('permission_message');
                                            ?>  <i class="fa fa-thumbs-down"></i></h5>
                                    </div>
                                    <?php  }?>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4>PUEO BUSINESS SOLUTIONS LLC </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
               
                    <div class="container">
                        <div class="row">
                <div class="col-md-5">
                    <div id="polina1">
                       
                        <form role="form" action="" method = "post">
                            <div class="col-md-12">
                               <?php
                            $query = $this->db->get('tblgeneralsetting')->result_array();
                        $title = $query[0]['general_value'];
                         $content = $query[1]['general_value'];
                            ?>

                                <h1><?php echo ucwords($title); ?></</h1>
                            </div>
                            <div class="form-group col-md-12">
                          <label style="font-size:15px;"><?php echo ucfirst(strtolower($content)); ?></label>                         
                            </div>
                            
                
                           <button class="btn submitfrm" onclick="goBack();">Agree</button>
                            
                        </form>
                        
                    </div>
                </div>

                <div class="col-md-4"></div>
                <div class="col-md-3">
                    <img src="<?php echo base_url(); ?>images/logo100.png" class="img-responsive sidelog">
                </div>
            </div>
                      
                    </div>
               
            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: 202.681.0239, Fax: 202.869.3765, inquiries@pueobusinesssolutions.com </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>function goBack() {    window.history.back();}</script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
 <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>




                                            var gap = 20;
                                            var boxH = $(window).height() - gap;
                                            var boxW = $(window).width() - gap * 2;
                                          

        </script>
        <script type="text/javascript">
    var wordLen = 50; // Maximum word length
     function checkWordLen(){
         var obj=document.getElementById('content');
             var content=document.getElementById('content').value;
        if (content== '')
        {
           
             $.fallr.show({
                        content: '<p>Display Content is Required</p>',
                        position: 'center'
                    });
            return false;
        }
      var len = obj.value.split(/[\s]+/);
       if(len.length > wordLen ){
           //alert("You cannot put more than "+wordLen+" words in display content.");
           obj.oldValue = obj.value!=obj.oldValue?obj.value:obj.oldValue;
           obj.value = obj.oldValue?obj.oldValue:"";
            $.fallr.show({
                        content: '<p>You cannot put more than  50 words in display content.</p>',
                        position: 'center'
                    });
           return false;
       }
     return true;
   }
  </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
<script type="text/javascript">
    $(function () {
        $('.date-picker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
        });
    });
</script>
<script>
    function valid() {
      var privacy_title = document.getElementById('privacy_title').value;
        var privacy_content = document.getElementById('privacy_content').value;
         var content = document.getElementById('content').value;
          if (privacy_title == '')
        {
           
             $.fallr.show({
                        content: '<p>Privacy Heading is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (privacy_content == '')
        {
           
             $.fallr.show({
                        content: '<p>Privacy Content is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (content== '')
        {
           
             $.fallr.show({
                        content: '<p>Display Content is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        
        
      
 }
    </script>
    </body>
</html>