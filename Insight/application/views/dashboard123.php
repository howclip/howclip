<?php
$name = $this->session->userdata('user_firstname');
$id = $this->session->userdata('user_id');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 
        <!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">   
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">


                        </div>
                        <div class="col-md-2 col-xs-4">
                            <div class="dropdown" style="text-align:right;">
                                <button class="btn logdrop dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> <?php echo ucwords($this->session->userdata('username')); ?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left:initial;">
                                    <li><a href="<?php echo base_url('Dashboard'); ?>/dashboard">Home</a></li>
                                    <li><a href="<?php echo base_url('Dashboard'); ?>/change_pass">Change Password</a></li>
                                    <li><a href="<?php echo base_url('Dashboard'); ?>/edit_profile/<?php echo $this->session->userdata('user_id'); ?>">Change Profile</a></li>

<!--                                    <li><a href="<?php echo base_url('Dashboard'); ?>/groups">Groups</a></li>-->
                                    <li><a href="<?php echo base_url('Welcome'); ?>/logout">Logout</a></li>
                                </ul>
                            </div>



                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in" style="padding:5px;">
                        <a class="close" data-dismiss="alert" onclick="return closemsg();" href="#" 
                           aria-hidden="true">X</a>
                        <h4 style="margin-bottom: 0;"><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                <?php }
                ?>
                <div class="menustrip">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-12">

                                <h4>PUEO BUSINESS SOLUTIONS LLC </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
                <div class="container">
                    <div class="row">
                        <div class="col-md-2">
                            <a href="<?php echo base_url('Dashboard'); ?>/newlibrary" class="btn liberary"> + CREATE NEW LIBRARY</a>
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-6">
                            <input type="submit" value="Group Filter" class="searchbutton" />

  <!-- <input type="text"  class="form-control searchinut"/>-->
                            <select class="form-control searchinut" id="select" onchange="alertMessage();">
                                <option value="">SHOW ALL GROUPS</option>
                                <?php
                                foreach ($groups as $group) {
                                    if ($group->name != '') {
                                        ?>
                                        <option value="<?php echo $group->id; ?>"><?php echo $group->name; ?></option>

                                        <?php
                                    }
                                }
                                ?>
                            </select>

                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-2">
                            <a href="<?php echo base_url(); ?>uploads/insight.crx" class="btn chromeex"> <img src="<?php echo base_url(); ?>assets/images/google-chrome-extension-6a26cdad27e6f383174791f8648fbe4cc7627acc06e3870c588217c98d1bde91.png"> Download    </a>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <!--                            -->
                            <h4 class="mnage"><?php echo ucwords($this->session->userdata('username')); ?></h4>
                            <table style="color:white;margin-bottom: 10px;text-align: center;width:100%;background-color: #cc0000">  

                                <?php
                                //$user_id=$this->session->userdata('user_id');
                                $email = $this->session->userdata('user_email');
                                ;
                                $query = $this->db->get_where('invite_user', array('user_email' => $email, 'notification' => '1', 'email_send' => '1'))->result_array();
                                //echo "<pre>";
                                //print_r($query);
                                if (count($query) > 0) {
                                    foreach ($query as $row) {
                                        $query1 = $this->db->get_where('library', array('library_id' => $row['library_id']))->row_array();
                                        // print_r($query1);
                                        //echo $query1['name'];
                                        ?>
                                        <tr><td style="width:60%">IF YOU WANT TO ACCEPT "<?php echo ucwords($query1['name']); ?>" LIBRARY THEN CLICK ACCEPT BUTTON</td><td>
        <!--                                If you want to accept "<?php echo ucwords($query1['name']); ?>" library then click Accept Button -->
                                                <button style="padding:5px;margin-top: 5px;margin-bottom: 5px;color:black" onclick="move_library123('<?php echo $query1['library_id']; ?>')"> Accept</button>
                                                <button style="padding:5px;margin-top: 5px;margin-bottom: 5px;color:black" onclick="move_library12('<?php echo $query1['library_id']; ?>')"> Reject </button></td></tr>


                                    <?php
                                    }
                                } else {
                                    //echo "no";
                                }
                                ?>

                            </table>
                            <div class="row">
                                <div class="col-md-12">
                                    <button style="float:right;padding:3px;margin-left:10px;margin-bottom: 10px; " data-toggle="modal" data-target="#myModal" onclick="modelpopup()"> Import</button>

                                    <button style="float:right;padding:3px;margin-left:10px;margin-bottom: 10px; " onclick="change_status('active');" id="hello1"> Active</button>
                                    <button style="float:right;padding:3px;margin-left:10px; " onclick="change_status('inactive');" id="hello"> Inactive

                                    </button>
                                <button style="float:right;padding:3px;margin-left:10px; " id="save-reorder">Set Preority</button> </div>
                            </div>

                            <div class="table-responsive">
                                <table class="table table-striped mytable">
                                    <thead>
                                    <th style="width:15%;">Library </th>
                                    <th style="width:15%;">Group </th>
                                    <th style="width:15%;">Description </th>
                                    <th style="width:12%;">Date</th>
                                    <th style="width:12%;">Color </th>
                                    <th style="width:15%;">Status </th>

                                    <th style="width:12%;">Action </th>
                                    <th style="width:15%;"> </th>
                                    <th style="width:5%;"> </th>
                                    </thead>
                                    <tbody id="pri" class="tb">
<?php 

 $quer = "SELECT * FROM library where creator=".$id." ORDER BY name";
                        $query = $this->db->query($quer );
                        $data1 = $query->result_array();
foreach ($data1 as $lib) { 
    $gr= $this->db->get_where('groups',array('id'=>$lib['group_name']))->result_array();
    ?>
                                            <tr>
                                                <td style="width:15%;"><?php echo $lib['name']; ?></td>
                                                <td style="width:15%;"><?php echo $gr[0]['name']; ?></td>
                                                <td style="width:15%;"><?php echo $lib['discription']; ?> </td>
                                                <td style="width:12%;"><?php echo $lib['date']; ?></td>
                                                <td style="width:12%;"><input type="text" style="text-align:center;width:80px;color:white;background-color:#<?php echo $lib['color']; ?>; " disabled=""  value="<?php //echo $lib->color;  ?>"></td>
                                                <td style="width:15%;"><?php echo $lib['status']; ?></td>
                                                <td style="width:12%;"> <a href="<?php echo base_url(); ?>Dashboard/edit_library/<?php echo $lib['library_id']; ?>" class="editbut" >EDIT</a>/
                                                    <a class="editbut" href="#" onclick="return deletedata(<?php echo $lib['library_id']; ?>);" >DELETE</a>
                                                </td>
                                                <th style="width:15%;"><a href="<?php echo base_url(); ?>Dashboard/email/<?php echo $lib['library_id']; ?>" class="editbut">INVITE USERS</a> </th>  
                                                <td><input style="margin: 4px 4px 0;float:right" type="checkbox" id="chk" class="case" name="userchkbox[]" value="<?php echo $lib['library_id']; ?>"></td>

    <!--href="<?php echo base_url(); ?>Dashboard/delete_library/<?php echo $lib->library_id; ?>"-->
                                            </tr>
<?php } ?>

                                    </tbody>

                                </table>
                            </div>
                            <h4 class="mnage">PUBLIC LIBRARIES</h4>
                            <div class="row">
                                <div class="col-md-12" style="line-height:1;">
                                    <button style="float:right;padding:5px;margin-bottom: 10px;" onclick="move_library();"> Move To Private </button>

                                </div>
                            </div>


                            <div class="table-responsive">
                                <table class="table table-striped mytable">
                                    <thead>
                                    <th style="width:10%;">Library </th>
                                    <th style="width:10%;">Group </th>
                                    <th style="width:20%;">Description </th>


                                    </thead>
                                    <tbody id="pub">
<?php foreach ($libraries as $library) { ?>
                                            <tr>
                                                <td style="width:10%;"><?php echo $library->name; ?> </td>
                                                <td style="width:10%;"><?php echo $library->groupName; ?> </td>
                                                <td style="width:20%;"><?php echo $library->discription; ?> 

                                                    <input style="margin: 4px 4px 0;float:right" type="checkbox" id="chk2" class="case2" name="userchkbox2[]" value="<?php echo $library->library_id; ?>"></td>
                                            </tr>
<?php } ?>

                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>


                </div>


            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: 202.681.0239, Fax: 202.869.3765, inquiries@pueobusinesssolutions.com </p>
                        </div>
                    </div>
                </div>
            </div>
        </div><script src="<?php echo base_url(); ?>assets/js/jscolor.js"></script>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Import Excel File</h4>
                    </div>
                    <div class="modal-body">
            <!--            <p id="pdetail"></p>-->
                        <form  action="<?php echo site_url(); ?>Dashboard/abc" name="myfrm" method="POST" enctype="multipart/form-data">

                            <table>
                                <tr>
                                    <td style="width:180px">Choose your file: </td>
                                    <td>
                                        <input type="file" class="form-control" name="file" id="userfile"  align="center" onchange="this.form.submit();" />
                                    </td>
                                </tr>
                                <tr style="height:0px;">
                                    <td></td>
                        
                                    </td>
                                </tr>
                                <tr style="height:80px;">
                                    <td></td>
                                    <td>
                                   
                                        <a href="<?php echo base_url(); ?>uploads/test1.xlsx"><input type="button" value="Download Demo Data" class="btn btn-primary start"></a>

                                    </td>
                                </tr>
                            </table> 

                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <script>
$(document).ready(function(){
	$(document).on('click','#save-reorder',function(){
           //alert("Deff");return false;
		var list = new Array();
		$('#pri').find('.default').each(function(){
			 var id=$(this).attr('id');	
			 list.push(id);
		});
		var data=JSON.stringify(list);
		$.ajax({
        url: '<?php echo base_url(); ?>Dashboard/get_records', // server url
        type: 'POST', //POST or GET 
        data: {token:'reorder',data:data}, // data to send in ajax format or querystring format
        datatype: 'json',
        success: function(response) {
			//alert(response);
        }
 
    });
	});
	
});
        </script>
  
        <script type="text/javascript">
    $(document).ready(function(){
        $('#hello').click(function(){
            if($('.case').prop("checked") == true){
                //alert("Please select library for inactive.");
            }
            else if($('.case').prop("checked") == false){
                 alert("Please select library for inactive.");
            }
            
        });
        $('#hello1').click(function(){
            if($('.case').prop("checked") == true){
                //alert("Please select library for inactive.");
            }
            else if($('.case').prop("checked") == false){
                 alert("Please select library for active.");
            }
            
        });
    });
</script>
            <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <script>
  $(function() {
    $( "#pri" ).sortable();
    $( "#pri" ).disableSelection();
  });
  </script>
        <script>
            document.getElementById("file").onchange = function() {
            document.getElementById("form").submit();
            }
        </script>
        <script>
            function modelpopup()
            {
            //alert(id);
            return false;
            $.ajax({
            url: "<?php echo base_url(); ?>Dashboard/library_import",
                    type: "POST",
                    data: {'id': "yes"},
                    success: function (response == 'yes')
                    {
                    alert(response);
                    $("#pdetail").html(response);
                    }

            });
            }
        </script>

        <script type="text/javascript">
            $('.tb').sortable();
        </script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>

            function move_library123(id) {
            // alert(id);
            var library = [];
            library.push(id);
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/move_library_accept',
                    type: "POST",
                    data: {library: id,
                            detail: library
                    },
                    success: function (response)
                    {
//alert(response);
                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    }

            });
            }



            var gap = 20;
            var boxH = $(window).height() - gap;
            var boxW = $(window).width() - gap * 2;
        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);
            (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
            })();
        </script>
        <script>
            function alertMessage()
            {
            $('#pub').empty();
            $('#pri').empty();
            var group = document.getElementById('select').value;
            var post_data = {
            'grp': group,
            };
            $.ajax({
            type: "POST",
                    url: "<?php echo base_url() ?>index.php/Dashboard/filter_group",
                    data: post_data,
                    success: function (data) {
                    console.log(data);
                    if (data.length > 0) {
                    var res = data.split('||');
                    $('#pri').append(res[0]);
                    $('#pub').append(res[1]);
                    // $('#autoSuggestionsList2').html(data);
                    // document.getElementById("refer_site2").value = d;
                    }
                    }
            });
            }
            window.cc = '';
            function deletedata(x) {

            cc = x;
            $.fallr.show({
            buttons: {
            button1: {text: 'Yes', danger: true, onclick: clicked},
                    button2: {text: 'Cancel'}
            },
                    content: '<p>Are you sure you want to delete?</p>',
                    icon: 'error',
                    position: 'center'
            });
            }
            var clicked = function () {

            var post_data = {
            'id': cc,
            };
            $.ajax({
            type: "POST",
                    url: "<?php echo base_url() ?>index.php/Dashboard/delete_library",
                    data: post_data,
                    success: function (data) {

                    if (data.length > 0) {
                    window.location = "<?php echo base_url() ?>index.php/Dashboard/dashboard/";
                    $.fallr('hide');
                    cc = '';
                    // $('#autoSuggestionsList2').html(data);
                    // document.getElementById("refer_site2").value = d;
                    }
                    }
            });
            };
            function change_status(x) {
            var status = x;
            var data = [];
            $(".case").each(function () {
            var aa = this.checked;
            var val = this.value;
            if (aa)
            {
            var abc = val + '|' + status;
            data.push(abc
                    );
            }

            });
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/setprivacy',
                    type: "POST",
                    data: {detail: data
                    },
                    success: function (response)
                    {
                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    }

            });
            }

            function move_library() {

            var library = [];
            $(".case2").each(function () {
            var chk = this.checked;
            var val2 = this.value;
            if (chk)
            {
            var abc = val2;
            library.push(abc
                    );
            }
            });
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/move_library',
                    type: "POST",
                    data: {detail: library
                    },
                    success: function (response)
                    {

                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    }

            });
            }

            function move_library12(id) {

            // alert(id);
            var library = [];
            library.push(id);
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/move_library_reject',
                    type: "POST",
                    data: {library: id,
                            detail: library
                    },
                    success: function (response)
                    {
//alert(response);
                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    }

            });
            }
            function closemsg()
            {
            $('.alert').hide();
            }

        </script>
    </body>
</html>