<?php
$name = $this->session->userdata('user_firstname');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">


                        </div>
                        <div class="col-md-2 col-xs-4">
<div class="list-group">
            <a href="#menupos1" class="list-group-item logdrop" data-toggle="collapse" data-parent="#mainmenu"><img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> Demo <span class="menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
            <div class="collapse pos-absolute" id="menupos1">
            <?php if($hide_menu != "Home"){?>
             <a href="<?php echo base_url('Welcome2'); ?>/dashboard" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Home</a>
            <?php } ?>
             <a href="<?php echo base_url('Welcome2'); ?>/change_pass" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Password</a>
             <a href="<?php echo base_url('Welcome2'); ?>/edit_profile/<?php echo $this->session->userdata('user_id'); ?>" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Profile</a>
             <a href="<?php echo base_url('Welcome2'); ?>/general_setting" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Setting</a> 
                <a href="<?php echo base_url('Welcome2'); ?>/add_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Column Type</a>
                 <a href="<?php echo base_url('Welcome2'); ?>/all_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">List</a>
             <!--<a href="#submenu1" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Column Field<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>-->
<!--                  <div class="collapse list-group-submenu" id="submenu1">
                    <a href="<?php echo base_url('Welcome2'); ?>/library_country" class="list-group-item sub-sub-item" data-parent="#submenu1">Country</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_state" class="list-group-item sub-sub-item" data-parent="#submenu1">State</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_link" class="list-group-item sub-sub-item" data-parent="#submenu1">Link Type</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_image" class="list-group-item sub-sub-item" data-parent="#submenu1">Image Type</a>
                  </div>-->
             <!--<a href="<?php echo base_url('Welcome2'); ?>/add_sub" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Substitute</a>-->
                  <a href="<?php echo base_url('Welcome2'); ?>/all_library" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Library</a>
                  
             <a href="#submenu2" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Email<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu2">
                    <a href="<?php echo base_url('Welcome2'); ?>/email_template" class="list-group-item sub-sub-item" data-parent="#submenu1">Invite Email</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/user_invite_email" class="list-group-item sub-sub-item" data-parent="#submenu1">User Email</a>
                   
                  </div>
             <a href="#submenu3" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Add Record<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu3">
                    <a href="<?php echo base_url('Welcome2'); ?>/occupation" class="list-group-item sub-sub-item" data-parent="#submenu1">Occupation</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/groups" class="list-group-item sub-sub-item" data-parent="#submenu1">Groups</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/link" class="list-group-item sub-sub-item" data-parent="#submenu1">Links</a>
                   
                  </div>
             <a href="<?php echo base_url('Welcome2'); ?>/mail_settings" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Mail Settings</a>    
             <a href="<?php echo base_url('Welcome'); ?>/logout" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Logout</a>    
              </div>
            </div>
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <?php
                    $this->session->flashdata('flash_message');
                    ?>


                <?php }
                ?>
                <div class="menustrip">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php echo $banner[4]['general_value']?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
                <div class="container">
                    <div class="row">

                        <div class="col-md-2">

                        </div>
                        <div class="col-md-8">


                            <input type="text"  class="form-control searchinuttext" id="search"/>
                            <input type="submit" value="Search User" class="searchbutton" onclick="getuser()" />

                        </div>
                        <div class="col-md-2">

                        </div>
                        <div class="col-md-2">

                        </div>


                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <h4 class="mnage"><?php echo "USER LIST"; ?></h4>
                            
                            
                             <div class="row">
                                <div class="col-md-12">
<!--                                    <button style="float:right;padding:3px;margin-left:10px;margin-bottom: 10px; " onclick="send_mail();"> Send Mail To All Users</button>
                                   -->
                                    </button></div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped mytable">
                                    <thead>
                                  
                                    <th style="width:15%;">Username </th>
                                    <th style="width:25%;">First Name </th>
                                    <th style="width:15%;">Last Name </th>
                                    <th style="width:15%;">Email </th>
                                    
                                    <th style="width:15%;"> Action </th>
                                   
                                    </thead>
                                    <tbody id="pub">
                                        <?php $i = 1;
                                        foreach ($users as $user) { ?>
                                            <tr>
                                              
                                                <td style="width:15%;"><?php echo $user->username; ?></td>
                                                <td style="width:25%;"><?php echo $user->user_firstname; ?> </td>
                                                <td style="width:25%;"><?php echo $user->user_lastname; ?> </td>

                                                <td style="width:15%;"><?php echo $user->user_email; ?></td>
                                               
                                                <td>  <?php if($user->status == 'active') {?><button style="color: red;
    font-size: initial;width: 80px;" onclick="change_user_status('inactive',<?php echo $user->user_id; ?>);" type="button">Inactive</button>
                                                <?php } if($user->status == 'inactive') {?> <button style="color:green;
    font-size: initial;width: 80px;" onclick="change_user_status('active',<?php echo $user->user_id; ?>);" type="button">Active</button><?php }?></td>
    
                                            </tr>
    <?php $i++;
} ?>

                                    </tbody>

                                </table>
                            </div>


                        </div>
                    </div>


                </div>


            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>




                                                var gap = 20;
                                                var boxH = $(window).height() - gap;
                                                var boxW = $(window).width() - gap * 2;


        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <script>
            function getuser()
            {
                $('#pub').empty();
               
                var name = document.getElementById('search').value;
                var post_data = {
                    'name': name,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/Welcome2/filter_user",
                    data: post_data,
                    success: function (data) {
                        console.log(data);
                        if (data.length > 0) {
                            var res = data.split('||');
                            $('#pub').append(res[0]);
                            //$('#pub').append(res[1]);

                            // $('#autoSuggestionsList2').html(data);
                            // document.getElementById("refer_site2").value = d;
                        }
                    }
                });

            }
            window.cc = '';

            function changestatus(x) {

                cc = x;
                $.fallr.show({
                    buttons: {
                        button1: {text: 'Yes', danger: true, onclick: clicked},
                        button2: {text: 'Cancel'}
                    },
                    content: '<p>Are you sure you want to delete?</p>',
                    icon: 'error',
                    position: 'center'
                });

            }
            var clicked = function () {

                var post_data = {
                    'id': cc,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/Dashboard/delete_library",
                    data: post_data,
                    success: function (data) {
                        console.log(data);
                        if (data.length > 0) {
                            window.location = "<?php echo base_url() ?>index.php/Dashboard/dashboard/";
                            $.fallr('hide');
                            cc = '';

                            // $('#autoSuggestionsList2').html(data);
                            // document.getElementById("refer_site2").value = d;
                        }
                    }
                });



            };
            function change_user_status(x,y){
            var status = x;
            var user = y;
             var post_data = {
                    'status': status,
                    'id': user,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/Welcome2/change_user",
                    data: post_data,
                    success: function (data) {
                        console.log(data);
                        if (data.length > 0) {
                            window.location = "<?php echo base_url() ?>index.php/Welcome2/dashboard/";
                           
                           

                            // $('#autoSuggestionsList2').html(data);
                            // document.getElementById("refer_site2").value = d;
                        }
                    }
                });

            }
function send_mail() {
//alert("rtrtg");return false;
                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/Welcome2/send_mail_user',
                    type: "POST",
                    data: {library: 'yes'
                    },
                    success: function (response)
                    {
//alert(response);
                       // window.location.href = '<?php echo base_url(); ?>index.php/Welcome2/dashboard';
                    }

                });

            }

        </script>
    </body>
</html>