<?php
$name = $this->session->userdata('user_firstname');
$id = $this->session->userdata('user_id');

if($param1 == "edit")
{
     $detail = $this->db->get_where('mail_settings',array('id'=>$param2))->result_array();
    $formaction="edit";
}else
{
    $formaction = "create";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script> 
        <!--    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">-->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.css" rel="stylesheet">   
    </head>
    <body id="b1">
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">


                        </div>
                        <div class="col-md-2 col-xs-4">
                          <div class="list-group">
            <a href="#menupos1" class="list-group-item logdrop" data-toggle="collapse" data-parent="#mainmenu"><img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> Demo <span class="menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
            <div class="collapse pos-absolute" id="menupos1">
              
             <a href="<?php echo base_url('Welcome2'); ?>/dashboard" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Home</a>
             <a href="<?php echo base_url('Welcome2'); ?>/change_pass" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Password</a>
             <a href="<?php echo base_url('Welcome2'); ?>/edit_profile/<?php echo $this->session->userdata('user_id'); ?>" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Profile</a>
             <a href="<?php echo base_url('Welcome2'); ?>/general_setting" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Setting</a> 
              <a href="<?php echo base_url('Welcome2'); ?>/add_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Column Type</a>
               <a href="<?php echo base_url('Welcome2'); ?>/all_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">List</a>
<!--             <a href="#submenu1" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Column Field<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu1">
                    <a href="<?php echo base_url('Welcome2'); ?>/library_country" class="list-group-item sub-sub-item" data-parent="#submenu1">Country</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_state" class="list-group-item sub-sub-item" data-parent="#submenu1">State</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_link" class="list-group-item sub-sub-item" data-parent="#submenu1">Link Type</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_image" class="list-group-item sub-sub-item" data-parent="#submenu1">Image Type</a>
                  </div>-->
             <!--<a href="<?php echo base_url('Welcome2'); ?>/add_sub" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Substitute</a>-->
                  <a href="<?php echo base_url('Welcome2'); ?>/all_library" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Library</a>
             <a href="#submenu2" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Email<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu2">
                    <a href="<?php echo base_url('Welcome2'); ?>/email_template" class="list-group-item sub-sub-item" data-parent="#submenu1">Invite Email</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/user_invite_email" class="list-group-item sub-sub-item" data-parent="#submenu1">User Email</a>
                   
                  </div>
             <a href="#submenu3" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Add Record<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu3">
                    <a href="<?php echo base_url('Welcome2'); ?>/occupation" class="list-group-item sub-sub-item" data-parent="#submenu1">Occupation</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/groups" class="list-group-item sub-sub-item" data-parent="#submenu1">Groups</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/link" class="list-group-item sub-sub-item" data-parent="#submenu1">Links</a>
                   
                  </div>
             <a href="<?php echo base_url('Welcome'); ?>/logout" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Logout</a>    
              </div>
            </div>
                        </div>
                    </div>
                </div>
                <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in" style="padding:5px;">
                        <a class="close" data-dismiss="alert" onclick="return closemsg();" href="#" 
                           aria-hidden="true">X</a>
                        <h4 style="margin-bottom: 0;"><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                <?php }
                ?>
                <?php if ($this->session->flashdata('error_message')) { ?>
                    <div class="alert alert-block alert-danger fade in" style="padding:5px;">
                        <a class="close" data-dismiss="alert" onclick="return closemsg();" href="#" 
                           aria-hidden="true">X</a>
                        <h4 style="margin-bottom: 0;"><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?> </i></h4>
                    </div>
                <?php }
                ?>
                <div class="menustrip">
                    <div class="container">

                        <div class="row">
                            <div class="col-md-12">

                                <h4><?php echo $banner[4]['general_value'];?> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
                <div class="container">
                    
        <?php echo form_open('Welcome2/mail_settings/'.$formaction, array('id' => 'usersForm', 'class' => 'form-horizontal'));?>
             
                    <div class="row" style="margin-bottom:2%">
                        <div class="col-md-2">
                            <!--<a href="<?php echo base_url('Dashboard'); ?>/newlibrary" class="btn liberary"> + CREATE NEW LIST </a>-->
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-6">
                            <input type="button" value="SMTP SERVER" class="searchbutton" />

                            <input type="hidden" value="<?php echo $detail[0]['id']?>" name="mail_id">
                             <input type="text" name="smtp_server" id="grp" class="form-control addgroup"  value = "<?php echo $detail[0]['smtp_server']?>"/>

                        </div>
                        <div class="col-md-1">

                        </div>
                       

                    </div>
                    <div class="row" style="margin-bottom:2%">
                        <div class="col-md-2">
                            <!--<a href="<?php echo base_url('Dashboard'); ?>/newlibrary" class="btn liberary"> + CREATE NEW LIST </a>-->
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-6">
                            <input type="button" value="Port" class="searchbutton" />

  <!-- <input type="text"  class="form-control searchinut"/>-->
                            <input type="text" name="port" id="grp" class="form-control addgroup"  value = "<?php echo $detail[0]['port']?>"/>

                        </div>
                        <div class="col-md-2">
                          
                        </div>
                       

                    </div>
                    
                     <div class="row" style="margin-bottom:2%">
                        <div class="col-md-2">
                            <!--<a href="<?php echo base_url('Dashboard'); ?>/newlibrary" class="btn liberary"> + CREATE NEW LIST </a>-->
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-6">
                            <input type="button" value="USERNAME" class="searchbutton" />

  <!-- <input type="text"  class="form-control searchinut"/>-->
                            <input type="text" name="username" id="grp" class="form-control addgroup"  value = "<?php echo $detail[0]['username']?>"/>

                        </div>
                        <div class="col-md-2">
                          
                        </div>
                       

                    </div>
                    
                     <div class="row" style="margin-bottom:2%">
                        <div class="col-md-2">
                            <!--<a href="<?php echo base_url('Dashboard'); ?>/newlibrary" class="btn liberary"> + CREATE NEW LIST </a>-->
                        </div>
                        <div class="col-md-1">

                        </div>
                        <div class="col-md-6">
                            <input type="button" value="PASSWORD" class="searchbutton" />

  <!-- <input type="text"  class="form-control searchinut"/>-->
                            <input type="password" name="password" id="grp" class="form-control addgroup"  value = "<?php echo $detail[0]['password']?>"/>

                        </div>
                        <div class="col-md-2">
                           <button style="height:52px;" type="submit" class="btn liberary" style="height: 50px;">Submit</button>
                       
                        </div>
                       

                    </div>
                    <?php echo form_close();?>
                    
                  
                  

                    
                </div>


            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: <?php echo $banner[5]['general_value'];?>, Fax: <?php echo $banner[6]['general_value'];?>, <?php echo $banner[7]['general_value'];?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div><script src="<?php echo base_url(); ?>assets/js/jscolor.js"></script>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Import Excel File</h4>
                    </div>
                    <div class="modal-body">
            <!--            <p id="pdetail"></p>-->
                        <form  action="<?php echo site_url(); ?>Dashboard/abc" name="myfrm" method="POST" enctype="multipart/form-data">

                            <table>
                                <tr>
                                    <td style="width:180px">Choose your file: </td>
                                    <td>
                                        <input type="file" class="form-control" name="file" id="userfile"  align="center" onchange="this.form.submit();" />
                                    </td>
                                </tr>
                                <tr style="height:0px;">
                                    <td></td>

                                    </td>
                                </tr>
                                <tr style="height:80px;">
                                    <td></td>
                                    <td>

                                        <a href="<?php echo base_url(); ?>uploads/test1.xlsx"><input type="button" value="Download Demo Data" class="btn btn-primary start"></a>

                                    </td>
                                </tr>
                            </table> 

                        </form>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
 <script>
            function alertMessage()
            {
            $('#pub').empty();
            $('#pri').empty();
            var group = document.getElementById('select').value;
            var post_data = {
            'grp': group,
            };
            $.ajax({
            type: "POST",
                    url: "<?php echo base_url() ?>index.php/Dashboard/filter_group",
                    data: post_data,
                    success: function (data) {
                    console.log(data);
                    if (data.length > 0) {
                   
                    var res = data.split('||');
                         //alert(res[1]);
                         if(res[1]){
                    $('#pri').append(res[0]);
                    $('#pub').append(res[1]);
                }
                else{
                    $('#pub').append(res[0]); 
                    //alert("fgfd");
            }
                    // $('#autoSuggestionsList2').html(data);
                    // document.getElementById("refer_site2").value = d;
                    }
                    }
            });
            }
             </script>
        <script>
            $(document).ready(function(){
            $(document).on("mouseout", function(){
            //$(document).on('click','#save-reorder',function(){
            //alert("Deff");return false;
            var list = new Array();
            $('#pri').find('.default').each(function(){
            var id = $(this).attr('id');
            list.push(id);
            });
            var data = JSON.stringify(list);
            $.ajax({
            url: '<?php echo base_url(); ?>Dashboard/get_records', // server url
                    type: 'POST', //POST or GET 
                    data: {token:'reorder', data:data}, // data to send in ajax format or querystring format
                    datatype: 'json',
                    success: function(response) {
                    //alert(response);
                    }

            });
            });
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function(){
            $('#hello').click(function(){
            if ($('.case').prop("checked") == true){
            //alert("Please select library for inactive.");
            }
            else if ($('.case').prop("checked") == false){
            alert("Please select library for inactive.");
            }

            });
            $('#hello1').click(function(){
            if ($('.case').prop("checked") == true){
            //alert("Please select library for inactive.");
            }
            else if ($('.case').prop("checked") == false){
            alert("Please select library for active.");
            }

            });
            });
        </script>
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
        <script>
            $(function() {
            $("#pri").sortable();
            $("#pri").disableSelection();
            });
        </script>
       
        <script>
            function modelpopup()
            {
            //alert("hjbj");
            return false;
            $.ajax({
            url: "<?php echo base_url(); ?>Dashboard/library_import",
                    type: "POST",
                    data: {'id': "yes"},
                    success: function (response == 'yes')
                    {
                    alert(response);
                    $("#pdetail").html(response);
                    }

            });
            }
        </script>

        <script type="text/javascript">
            $('#pri').sortable();
        </script>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>

            function move_library123(id) {
            // alert(id);
            var library = [];
            library.push(id);
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/move_library_accept',
                    type: "POST",
                    data: {library: id,
                            detail: library
                    },
                    success: function (response)
                    {
//alert(response);
                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    }

            });
            }



            var gap = 20;
            var boxH = $(window).height() - gap;
            var boxW = $(window).width() - gap * 2;
        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);
            (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
            })();
        </script>
       <script>
            window.cc = '';
            function deletedata(x) {

            cc = x;
            $.fallr.show({
            buttons: {
            button1: {text: 'Yes', danger: true, onclick: clicked},
                    button2: {text: 'Cancel'}
            },
                    content: '<p>Are you sure you want to delete?</p>',
                    icon: 'error',
                    position: 'center'
            });
            }
            var clicked = function () {

            var post_data = {
            'id': cc,
            };
            $.ajax({
            type: "POST",
                    url: "<?php echo base_url() ?>index.php/Dashboard/delete_library",
                    data: post_data,
                    success: function (data) {

                    if (data.length > 0) {
                    window.location = "<?php echo base_url() ?>index.php/Dashboard/dashboard/";
                    $.fallr('hide');
                    cc = '';
                    // $('#autoSuggestionsList2').html(data);
                    // document.getElementById("refer_site2").value = d;
                    }
                    }
            });
            };
            function change_status(x) {
            var status = x;
            var data = [];
            $(".case").each(function () {
            var aa = this.checked;
            var val = this.value;
            if (aa)
            {
            var abc = val + '|' + status;
            data.push(abc
                    );
            }

            });
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/setprivacy',
                    type: "POST",
                    data: {detail: data
                    },
                    success: function (response)
                    {
//                   
                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    }

            });
            }

            function move_library() {

            var library = [];
            $(".case2").each(function () {
            var chk = this.checked;
            var val2 = this.value;
            if (chk)
            {
            var abc = val2;
            library.push(abc
                    );
            }
            });
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/move_library',
                    type: "POST",
                    data: {detail: library
                    },
                    success: function (response)
                    {

                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    alert("Libraries moved successfully");
                    }

            });
            }

            function move_library12(id) {

            // alert(id);
            var library = [];
            library.push(id);
            $.ajax({
            url: '<?php echo base_url(); ?>index.php/Dashboard/move_library_reject',
                    type: "POST",
                    data: {library: id,
                            detail: library
                    },
                    success: function (response)
                    {
//alert(response);
                    window.location.href = '<?php echo base_url(); ?>index.php/Dashboard/dashboard';
                    }

            });
            }
            function closemsg()
            {
            $('.alert').hide();
            }



            function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
        </script>
    </body>
</html>