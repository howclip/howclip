<?php
//error_reporting(~E_NOTICE);
//if ($param1) {
   // $result = $this->db->get_where('user', array('user_id' => $param1))->row_array();
   
   // $formaction = 'edit';
//} else {
   // $formaction = 'create';
//}
?>
<!DOCTYPE html>
    <html lang="en">
        <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
       
        </head>
        <body class="holder">
        <div class="">
            <div class="bordered"></div>

            <div class="lftcont">
                <div class="container-fluid">
                    <div class="row">
                        <?php if ($this->session->flashdata('flash_message')) { ?>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="#" 
                           aria-hidden="true">X</a>
                        <h4><i class="fa fa-smile-o"></i> <?php
                            echo
                            $this->session->flashdata('flash_message');
                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('permission_message')) {
                    ?>
                    <div class="alert alert-block alert-danger fade in">
                        <a class="close" data-dismiss="alert" href="#" 
                           aria-hidden="true">X</a>
                        <h5><i class="fa fa-frown-o"></i> <?php
                            echo
                            $this->session->flashdata('permission_message');
                            ?><i class="fa fa-thumbs-down"></i></h5>
                    </div>
                <?php } ?>
                        <div class="col-md-4">
                            <div class="lftspc">
                                <h4 class="experie">Experience</h4>
                                <h4 class="insired">INSIGHT</h4>
                                <h4 class="insired"><span style="color:#FFF;">by</span> Pueo</h4>
                               
                                <?php
                                  $balance = "SELECT * FROM `links` where status='active'  ORDER BY `links`.`link_id` DESC";
                                    $query = $this->db->query($balance);
                                    $data = $query->result_array();

                                    $newData = [];
                                    foreach ($data as $value) {
                                        $abc = $value['link_type'];
                                    $newData[$abc] = $value;
                                    
                                    }
                                    
         $pagedata['data1'] = $newData;
         //echo "<pre>";
     // print_r($pagedata['data1']);
                                          $query1 = $this->db->get_where('links',array('status'=>'active'))->result_array();
                                                $query = $this->db->get('tblgeneralsetting')->result_array();

                                                $content = $query[2]['general_value'];
                                                ?>

                                <p class="textbtm"><?php echo ucfirst(strtolower($content)); ?></p>
                            </div>
<!--                            <div class="lftspc" style="margin-left: 320px;margin-top: -55px;color:white">
                                <?php foreach($pagedata['data1'] as $row){
                                    if($row['link_type']=="pdf"){ ?>
                                     <p><a style="color:white;text-decoration: none" href="<?php echo base_url(); ?>uploads/<?php echo $row['link_url']; ?>"><img src="<?php echo base_url(); ?>images/img.jpg" class="img-responsive agnhead"></a></p>
     
                                   <?php } 
                                    if($row['link_type']=="youtube"){?>
                                       <p><a style="color:white;text-decoration: none" href="<?php echo $row['link_url']; ?>">YOUTUBE</a></p>
      
                                    <?php }
                                    if($row['link_type']=="website link"){?>
                                        
                                       <p><a style="color:white;text-decoration: none;width: 234px;margin-left: 68px;" href="<?php echo $row['link_url']; ?>" class="btn liberary">Website Link</a></p>
     
                                    <?php } } ?>
                            </div>-->
                        </div>
                        <div class="col-md-4">
                             
    <div class="videobox">
      <?php foreach($pagedata['data1'] as $row){?>
                               <?php     if($row['link_type']=="youtube"){ ?>  
   <iframe width="100%" height="200" src="<?php echo $row['link_url']; ?>" frameborder="0" allowfullscreen></iframe>
                                    <?php } ?>
   <div class="caption row">
 <?php 
                                    if($row['link_type']=="pdf"){?>
   <div class="col-md-12">
   <a class="btn downloadbut" href="<?php echo base_url(); ?>uploads/<?php echo $row['link_url']; ?>"  role="button">Download Overview</a>
   </div>
        <?php } 
                                    if($row['link_type']=="website link"){?>
   <div class="col-md-12">
   <p>
   <a href="<?php echo $row['link_url']; ?>" role="button">Please Click here</a></p>
   </div>
       <?php } ?>
   </div><?php } ?>
   </div> 
   </div>


                        <div class="col-md-4">
                            <div id="polina">
                                <div class="logheader">
                                <img src="<?php echo base_url(); ?>images/logos.png" class="img-responsive imglog">
                                </div>

                                <form role="form" action="<?php echo base_url(); ?>Welcome/admin_login/<?php //echo $formaction; ?>" method = "post">
                                    <h1>LOGIN</h1>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon symbolism"><i class="fa fa-user" aria-hidden="true"></i></span>
                                        <input readonly onfocus="this.removeAttribute('readonly');" type="text" name="username" class="form-control logtxt" id="Email" placeholder="User Id">
                                    </div>
                                    <div class="input-group form-group">
                                        <span class="input-group-addon symbolism"><i class="fa fa-unlock-alt" aria-hidden="true"></i></span>
                                        <input readonly onfocus="this.removeAttribute('readonly');" type="password" name="password" class="form-control logtxt" id="Password" placeholder="Password">
                                    </div>
                                    <div class="checkbox">
                                        <label>
<!--                                            <input type="checkbox"> Check me out-->
                                        </label>
                                    </div>
<!--                                    <a href="dashboard.html" class="btn submitfrm">Submit</a>-->
<button type="submit" class="btn submitfrm" onclick="return valid();">Submit</button>
<!--                                    <p class="mycon">Don't have an Account?  <a href="<?php echo base_url('Welcome'); ?>/userregistration" class="backlog">Register Now</a></p>-->
                                </form>
                            </div>
                            <div class="insightimg">
                            <img src="<?php echo base_url(); ?>images/insightnew.png" class="img-responsive">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                    <div class="col-md-12">


                    </div>

                    </div>
                </div>
            </div>


            <div class="footer1">
                <div class="container">
                <div class="row">
                <div class="col-md-12">
                 <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                </div>
                </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
       <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
       <!-- Include all compiled plugins (below), or include individual files as needed -->
       <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
       <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>
            $( document ).ready(function() {
    vdocument.getElementById('Email').value = '';
                document.getElementById('Password').value ='';
});




                                        var gap = 20;
                                        var boxH = $(window).height() - gap;
                                        var boxW = $(window).width() - gap * 2;


        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
        <script>
            function valid()
            {
                var email = document.getElementById('Email').value;
                var Password = document.getElementById('Password').value;
                if (email == '')
                {

                    $.fallr.show({
                        content: '<p>User Id is Required</p>',
                        position: 'center'
                    });
                    return false;
                }
                if (Password == '')
                {

                    $.fallr.show({
                        content: '<p>Password is Required</p>',
                        position: 'center'
                    });
                    return false;
                }


            }
        </script>
       </body>
    </html>