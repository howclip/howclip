
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
          <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
   
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">

                        </div>
                        <div class="col-md-2 col-xs-4">
<div class="list-group">
            <a href="#menupos1" class="list-group-item logdrop" data-toggle="collapse" data-parent="#mainmenu"><img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> Demo <span class="menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
            <div class="collapse pos-absolute" id="menupos1">
              
             <a href="<?php echo base_url('Welcome2'); ?>/dashboard" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Home</a>
             <a href="<?php echo base_url('Welcome2'); ?>/change_pass" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Password</a>
             <a href="<?php echo base_url('Welcome2'); ?>/edit_profile/<?php echo $this->session->userdata('user_id'); ?>" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Profile</a>
             <a href="<?php echo base_url('Welcome2'); ?>/general_setting" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Setting</a> 
                        <a href="<?php echo base_url('Welcome2'); ?>/add_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Column Type</a>
             <a href="<?php echo base_url('Welcome2'); ?>/all_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">List</a>
             <!--<a href="#submenu1" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Column Field<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>-->
<!--                  <div class="collapse list-group-submenu" id="submenu1">
                    <a href="<?php echo base_url('Welcome2'); ?>/library_country" class="list-group-item sub-sub-item" data-parent="#submenu1">Country</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_state" class="list-group-item sub-sub-item" data-parent="#submenu1">State</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_link" class="list-group-item sub-sub-item" data-parent="#submenu1">Link Type</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_image" class="list-group-item sub-sub-item" data-parent="#submenu1">Image Type</a>
                  </div>-->
             <!--<a href="<?php echo base_url('Welcome2'); ?>/add_sub" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Substitute</a>-->
                  <a href="<?php echo base_url('Welcome2'); ?>/all_library" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Library</a>
             <a href="#submenu2" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Email<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu2">
<!--                    <a href="<?php echo base_url('Welcome2'); ?>/email_template" class="list-group-item sub-sub-item" data-parent="#submenu1">Invite Email</a>-->
                    <a href="<?php echo base_url('Welcome2'); ?>/user_invite_email" class="list-group-item sub-sub-item" data-parent="#submenu1">User Email</a>
                   
                  </div>
             <a href="#submenu3" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Add Record<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu3">
                    <a href="<?php echo base_url('Welcome2'); ?>/occupation" class="list-group-item sub-sub-item" data-parent="#submenu1">Occupation</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/groups" class="list-group-item sub-sub-item" data-parent="#submenu1">Groups</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/link" class="list-group-item sub-sub-item" data-parent="#submenu1">Links</a>
                   
                  </div>
             <a href="<?php echo base_url('Welcome2'); ?>/mail_settings" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Mail Settings</a>    
             <a href="<?php echo base_url('Welcome'); ?>/logout" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Logout</a>    
              </div>
            </div>

                        </div>
                    </div>
                </div>
                 <?php if ($this->session->flashdata('flash_message')) { ?>
                                    <div class="alert alert-block alert-success fade in">
                                        <a class="close" data-dismiss="alert" href="#" 
                                           aria-hidden="true">X</a>
                                        <h4><i class="fa fa-smile-o"></i> <?php
                                            echo
                                            $this->session->flashdata('flash_message');
                                            ?>  <i class="fa fa-thumbs-up"></i></h4>
                                    </div>
                                    <?php
                                }
                                if ($this->session->flashdata('permission_message')) {
                                    ?>
                                    <div class="alert alert-block alert-warning fade in">
                                        <a class="close" data-dismiss="alert" href="#" 
                                           aria-hidden="true">X</a>
                                        <h5> <i class="fa fa-frown-o"></i> <?php
                                            echo
                                            $this->session->flashdata('permission_message');
                                            ?>  <i class="fa fa-thumbs-down"></i></h5>
                                    </div>
                                    <?php  }?>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php 
                                
                                //print_r($banner);
                                  echo $banner[4]['general_value'];    
                                ?> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
               
                    <div class="container">
                        <div class="row">
                <div class="col-md-10" style="margin-left: 80px">
                    <div id="polina1">
                       
                        <form role="form" action="<?php echo base_url(); ?>Welcome2/template_update" method = "post">
                            <div class="col-md-12">
                                <?php $data=$this->db->get('email')->result_array(); ?>
                                <h1>Email Template</h1>
                            </div>
                            <div class="form-group col-md-12">
                                <label style="font-size:15px;"><?php echo ucwords($data[0]['general_title']); ?></label>
                                <input type="text" class="form-control" name="privacy_title" id="privacy_title"  placeholder="Enter privacy policy heading" value="<?php echo $data[0]['general_value']; ?>">
		
                                                        
                            </div>
                            <div class="form-group col-md-12">
                                <label style="font-size:15px;"><?php echo ucwords($data[1]['general_title']); ?></label>
                                <input type="text" class="form-control" rows="4" cols="20" id="privacy_content" name="privacy_content" value="<?php echo $data[1]['general_value']; ?>"  >
		 
                                                        
                            </div>
<!--                             <div class="form-group col-md-12">
                                <label style="font-size:15px;"><?php echo ucwords($data[3]['general_title']); ?></label>
                                <textarea  style="resize:none; height:150px" class="form-control" rows="4" cols="20" id="privacy_content" name="privacy_content"   ><?php echo $data[3]['general_value']; ?></textarea>
		 
                                 <select class="form-control " name="status" id="status">
                                    
                                      <option value="Active" <?php
                                                                if ($data[3]['general_value'] == 'active') {
                                                                    echo "selected";
                                                                }
                                                                ?>>Active</option>
                                        <option value="Inactive" <?php
                                                                if ($data[3]['general_value']== 'inactive') {
                                                                    echo "selected";
                                                                }
                                                                ?>>Inactive</option>
                                </select>                        
                            </div>-->
                <div class="form-group col-md-12">
                    <label style="font-size:15px;"><?php echo ucwords($data[2]['general_title']); ?></label>
                          <textarea name="content" class="ckeditor form-control " id="content"><?php echo $data[2]['general_value']; ?></textarea>

<!--                    <textarea  style="resize:none; height:150px" class="form-control"  id="content" name="content" ><?php echo $data[2]['general_value']; ?></textarea>-->
		 <input type="hidden" class="form-control" 
                                                       name="name1"  value="<?php echo $data[0]['general_id']; ?>" >
                                                <input type="hidden" class="form-control" 
                                                       name="name2"  value="<?php echo $data[1]['general_id']; ?>" >
                                                 <input type="hidden" class="form-control" 
                                                       name="name3"  value="<?php echo $data[2]['general_id']; ?>" >
                                                 <input type="hidden" class="form-control" 
                                                       name="name4"  value="<?php echo $data[3]['general_id']; ?>" >
                                                        
                            </div>
                            <button type="submit" class="btn submitfrm" onclick="return valid();" style="width:100px;margin-left: 650px">Save</button>
                            
                        </form>
                        
                    </div>
                </div>

                <div class="col-md-4"></div>
               
            </div>
                      
                    </div>
               
            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                             <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
 <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>




                                            var gap = 20;
                                            var boxH = $(window).height() - gap;
                                            var boxW = $(window).width() - gap * 2;
                                          

        </script>
         <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/ckeditor/ckeditor.js"></script>
<!--	<script type="text/javascript" src="<?php echo base_url(); ?>asset/js/ckfinder/ckfinder.js"></script>-->
   <script>
CKEDITOR.replace('feturelist');
</script>
        <script type="text/javascript">
    var wordLen = 50; // Maximum word length
     function checkWordLen(){
         var obj=document.getElementById('content');
             var content=document.getElementById('content').value;
        if (content== '')
        {
           
             $.fallr.show({
                        content: '<p>Display Content is Required</p>',
                        position: 'center'
                    });
            return false;
        }
      var len = obj.value.split(/[\s]+/);
       if(len.length > wordLen ){
           //alert("You cannot put more than "+wordLen+" words in display content.");
           obj.oldValue = obj.value!=obj.oldValue?obj.value:obj.oldValue;
           obj.value = obj.oldValue?obj.oldValue:"";
            $.fallr.show({
                        content: '<p>You cannot put more than  50 words in display content.</p>',
                        position: 'center'
                    });
           return false;
       }
     return true;
   }
  </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
<script type="text/javascript">
    $(function () {
        $('.date-picker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
        });
    });
</script>
<script>
    function valid() {
      var privacy_title = document.getElementById('privacy_title').value;
        var privacy_content = document.getElementById('privacy_content').value;
         var content = document.getElementById('content').value;
        
          if (privacy_title == '')
        {
           
             $.fallr.show({
                        content: '<p>Suject is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (privacy_content == '')
        {
           
             $.fallr.show({
                        content: '<p>Sender is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (content== '')
        {
           
             $.fallr.show({
                        content: '<p>Content is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        
        
      
 }
    </script>
    </body>
</html>