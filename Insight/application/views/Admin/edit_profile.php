<?php
$name = $this->session->userdata('user_firstname');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
         <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">

                        </div>
                        <div class="col-md-2 col-xs-4">
<div class="list-group">
            <a href="#menupos1" class="list-group-item logdrop" data-toggle="collapse" data-parent="#mainmenu"><img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> Demo <span class="menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
            <div class="collapse pos-absolute" id="menupos1">
              
             <a href="<?php echo base_url('Welcome2'); ?>/dashboard" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Home</a>
             <a href="<?php echo base_url('Welcome2'); ?>/change_pass" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Password</a>
<!--             <a href="<?php echo base_url('Welcome2'); ?>/edit_profile/<?php echo $this->session->userdata('user_id'); ?>" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Change Profile</a>-->
             <a href="<?php echo base_url('Welcome2'); ?>/general_setting" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Setting</a> 
                        <a href="<?php echo base_url('Welcome2'); ?>/add_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Column Type</a>
            <a href="<?php echo base_url('Welcome2'); ?>/all_column_type" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">List</a>
             <!--<a href="#submenu1" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Column Field<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>-->
<!--                  <div class="collapse list-group-submenu" id="submenu1">
                    <a href="<?php echo base_url('Welcome2'); ?>/library_country" class="list-group-item sub-sub-item" data-parent="#submenu1">Country</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_state" class="list-group-item sub-sub-item" data-parent="#submenu1">State</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_link" class="list-group-item sub-sub-item" data-parent="#submenu1">Link Type</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/library_image" class="list-group-item sub-sub-item" data-parent="#submenu1">Image Type</a>
                  </div>-->
             <!--<a href="<?php echo base_url('Welcome2'); ?>/add_sub" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Substitute</a>-->
                  <a href="<?php echo base_url('Welcome2'); ?>/all_library" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Library</a>
             <a href="#submenu2" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Email<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu2">
                    <a href="<?php echo base_url('Welcome2'); ?>/email_template" class="list-group-item sub-sub-item" data-parent="#submenu1">Invite Email</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/user_invite_email" class="list-group-item sub-sub-item" data-parent="#submenu1">User Email</a>
                   
                  </div>
             <a href="#submenu3" class="list-group-item sub-item noborder" data-toggle="collapse" data-parent="#submenu1">Add Record<span class=" menu-ico-collapse"><i class="glyphicon glyphicon-chevron-down"></i></span></a>
                  <div class="collapse list-group-submenu" id="submenu3">
                    <a href="<?php echo base_url('Welcome2'); ?>/occupation" class="list-group-item sub-sub-item" data-parent="#submenu1">Occupation</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/groups" class="list-group-item sub-sub-item" data-parent="#submenu1">Groups</a>
                    <a href="<?php echo base_url('Welcome2'); ?>/link" class="list-group-item sub-sub-item" data-parent="#submenu1">Links</a>
                   
                  </div>
             <a href="<?php echo base_url('Welcome2'); ?>/mail_settings" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Mail Settings</a>    
             <a href="<?php echo base_url('Welcome'); ?>/logout" data-toggle="collapse" data-target="#menupos1" class="list-group-item sub-item">Logout</a>    
              </div>
            </div>
                        </div>
                    </div>
                </div>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php echo $banner[4]['general_value']?> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
               
                    <div class="container">
                        <div class="row">
                <div class="col-md-5">
                    <div id="polina1">
                        <?php 
                      //
                        foreach($userinfo as $user) {?>
                        <form role="form" action="<?php echo base_url(); ?>Welcome2/save_user/<?php echo $user->user_id;?>" method = "post">
                            <div class="col-md-12">
                                <h1>EDIT PROFILE</h1>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="username" value="<?php echo $user->username;?>" class="form-control logtxt" id="user" placeholder="Username">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="first_name" value="<?php echo $user->user_firstname;?>" class="form-control logtxt" id="first_name" placeholder="First Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="last_name" value="<?php echo $user->user_lastname;?>" class="form-control logtxt" id="last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="dob" value="<?php echo $user->user_DOB;?>" class="form-control logtxt date-picker" placeholder="DOB" />
                                    <span class="input-group-addon symbolism">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                             <div class="form-group col-md-6">
                                <input type="text" name="zip" onkeypress="return isNumber(event)" value="<?php if($user->user_zipcode != 0){echo $user->user_zipcode;}?>" class="form-control logtxt" id="exampleInputEmail1" placeholder="Zip Code">
                            </div>
                            <div class="form-group col-md-12"> <?php
                                                            //$data1=$this->db->get_where("occupation",array('status'=>'active'))->result_array();
                                                          //print_r($data1);
                                                            ?>
                              <select name="gen" id="gender" class="form-control logtxt">
                                                            <option value="">select gender</option>

                                                                <option value="Male" <?php
                                                               if ($user->user_gender == 'Male') {
                                                                   echo 'selected';
                                                               }
                                                               ?>>Male</option>
                                                                <option value="Female" <?php
                                                               if ($user->user_gender == 'Female') {
                                                                   echo 'selected';
                                                               }
                                                               ?>>Female</option>
                                                        </select>  
                            </div>
<!--                             <div class="form-group col-md-6" style="text-align: center">
                                <input type="radio" name="gen" class="" id="male" checked="" value="Male" <?php
                                                               if ($user->user_gender == 'Male') {
                                                                   echo 'checked';
                                                               }
                                                               ?>><label style="font-size:15px;margin-left:10px">Male</label>
                               
                            </div>
                            <div class="form-group col-md-6">
                               
                                <input type="radio" name="gen" class="" id="female" value="Female"  <?php
                                                               if ($user->user_gender == 'Female') {
                                                                   echo 'checked';
                                                               }
                                                               ?>><label style="font-size:15px;margin-left:10px">Female</label>
                            </div>-->
                             <div class="form-group col-md-12">
                                 <select name="occupation" id="occupation" class="form-control logtxt">
                                                            <option value="">select occupation</option>

                                                            <?php
                                                            
                                                            $data1=$this->db->get_where("occupation")->result_array();
                                                            foreach ($data1 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['occupation_id']; ?>" <?php
                                                                if ($user->user_occupation == $row['occupation_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['occupation_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
                            </div>
                           
                            <div class="form-group col-md-12">
                                <input type="email" name="email" value="<?php echo $user->user_email;?>" class="form-control logtxt" id="email" placeholder="Email">
                            </div>
                          
                            <button type="submit" class="btn submitfrm" onclick="return valid();">Submit</button>
                            
                        </form>
                        <?php }?>
                    </div>
                </div>

                <div class="col-md-4"></div>
                <div class="col-md-3">
                  <?php   $abc = $this->db->get("tblgeneralsetting")->result_array();

          // echo $file_name = $abc[2]['general_value'];
                                        if($abc[3]['general_value']){?>
                            <img src="<?php echo base_url() . "uploads/" . $abc[3]['general_value']; ?>" alt="" class="img-responsive sidelog"/>
                           <?php }else{?>
                       <img src="<?php echo base_url(); ?>images/logo100.png" class="img-responsive sidelog">
              
                             <?php } ?>
                
                </div>
            </div>
                      
                    </div>
               
            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>




                                            var gap = 20;
                                            var boxH = $(window).height() - gap;
                                            var boxW = $(window).width() - gap * 2;
                                          

        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
<script type="text/javascript">
    $(function () {
        $('.date-picker').datepicker({
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'yy-mm-dd',
            'maxDate':'0'
        });
    });
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      alert("Enter only numbers"); 
        return false;

    }
    return true;
}


function valid() {
      var user = document.getElementById('user').value;
        var first_name = document.getElementById('first_name').value;
         var last_name = document.getElementById('last_name').value;
var occupation = document.getElementById('occupation').value;
         var email = document.getElementById('email').value;
        
        
         if (user == '')
        {
           
             $.fallr.show({
                        content: '<p>Username is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (first_name == '')
        {
           
             $.fallr.show({
                        content: '<p>First Name is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (last_name == '')
        {
           
             $.fallr.show({
                        content: '<p>Last Name is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (email == '')
        {
            
             $.fallr.show({
                        content: '<p>Email Address is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (occupation == '')
        {
           
             $.fallr.show({
                        content: '<p>Occupation is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        }
</script>
    </body>
</html>