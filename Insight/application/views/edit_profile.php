<?php
$name = $this->session->userdata('user_firstname');
$id = $this->session->userdata('user_id');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
         <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-6 col-xs-4">
                            <img src="<?php echo base_url(); ?>images/logoname.png" class="img-responsive">

                        </div>
                        <div class="col-md-2 col-xs-4">
                            <div class="dropdown" style="text-align:right;">
                                <button class="btn logdrop dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"> <?php echo ucwords($this->session->userdata('username')); ?>
                                    <span class="caret"></span>
                                </button>
                                 <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left:initial;">
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/dashboard">Home</a></li>
                                       <li><a href="<?php echo base_url(); ?>index.php/Dashboard/change_pass">Change Password</a></li>
                                        <?php if($menu_value != "edit_profile"){?>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/edit_profile/<?php echo $this->session->userdata('user_id'); ?>">Change Profile</a></li>
                                        <?php }?>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/column_type">Column Type</a></li>
                                      <li><a href="<?php echo base_url(); ?>index.php/Dashboard/column_list">List</a></li>
<!--                                    <li><a href="<?php echo base_url('Dashboard'); ?>/groups">Groups</a></li>-->
                                    <li><a href="<?php echo base_url('Welcome'); ?>/logout">Logout</a></li>
                                </ul>
                            </div>



                        </div>
                    </div>
                </div>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><?php echo $banner[4]['general_value']?></h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
               
                    <div class="container">
                        <div class="row">
                <div class="col-md-5">
                    <div id="polina1">
                        
                        <?php 
                        //echo $id;
                          $subscriber = $this->db->get_where("library", array('user_id' => $id))->result_array();
                         $published = $this->db->get_where("library", array('creator' => $id))->result_array();
                          $active = $this->db->get_where("library", array('creator' => $id,'status' => 'active'))->result_array();
                          $a1=count($active);
                          $b1=count($published);
                            $c1=count($subscriber);
                          //echo "rftgr".$id;
                          //echo "<pre>";
                         // print_r($subscriber);
                        foreach($userinfo as $user) {?>
                        <form role="form" action="<?php echo base_url(); ?>index.php/Dashboard/save_user/<?php echo $user->user_id;?>" method = "post">
                            <div class="col-md-12">
                                <h1>MY PROFILE</h1>
                            </div>
                            <div class="form-group col-md-12">
                                 
                                <label style="color:white;font-size:10pt">USER SINCE : <?php echo $user->user_date;?></label> <br>
                                <label style="color:white;font-size:10pt">Library Summary :</label>
                            </div>
                            <div class="form-group col-md-4">
                                <label style="color:white;font-size:10pt">Active :  <?php echo $a1;?></label>
<!--                                <input type="text"  value="<?php echo $a1;?>" readonly="" class="form-control logtxt" id="user" placeholder="Username">-->
                            </div>
                            <div class="form-group col-md-4">
                                <label style="color:white;font-size:10pt">Subscriber :  <?php echo $c1;?></label>
<!--                                <input type="text"  value="<?php echo $c1;?>" readonly="" class="form-control logtxt" id="user" placeholder="Username">-->
                            </div>
                            <div class="form-group col-md-4">
                                <label style="color:white;font-size:10pt">Published : <?php echo $b1; ?></label>
<!--                                <input type="text"  value="<?php echo $b1; ?>" readonly="" class="form-control logtxt" id="first_name" placeholder="First Name">-->
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="username" value="<?php echo $user->username; ?>" class="form-control logtxt" id="user" placeholder="Username">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="first_name" value="<?php echo $user->user_firstname;?>" class="form-control logtxt" id="first_name" placeholder="First Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="last_name" value="<?php echo $user->user_lastname;?>" class="form-control logtxt" id="last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-6"><label style="color:white;font-size:10pt"> Date of Birth:</label>
<!--                                <div class='input-group date' id='datetimepicker1'>
                                   <input type='text' name="dob" value="<?php echo $user->user_DOB;?>" class="form-control logtxt date-picker" placeholder="DOB" />
                                    <span class="input-group-addon symbolism">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>-->
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="dob" readonly="" id="dob" class="form-control logtxt date-picker" placeholder="DOB" value="<?php echo $user->user_DOB;?>"/>
                                    <span class="input-group-addon symbolism">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                            
                            
                              
                            



<div class="form-group col-md-6">
                                 <label style="color:white;font-size:10pt"> Zip Code:</label>
                                <input type="text" name="zip" onkeypress="return isNumber(event)" value="<?php if($user->user_zipcode != 0){echo $user->user_zipcode;}?>" class="form-control logtxt" id="exampleInputEmail1" placeholder="Zip Code">
                            </div>
                            <div class="form-group col-md-6"> <?php
                                                            //$data1=$this->db->get_where("occupation",array('status'=>'active'))->result_array();
                                                          //print_r($data1);
                                                            ?>
                              <select name="gen" id="gender" class="form-control logtxt">
                                                            <option value="">select gender</option>

                                                                <option value="Male" <?php
                                                               if ($user->user_gender == 'Male') {
                                                                   echo 'selected';
                                                               }
                                                               ?>>Male</option>
                                                                <option value="Female" <?php
                                                               if ($user->user_gender == 'Female') {
                                                                   echo 'selected';
                                                               }
                                                               ?>>Female</option>
                                                        </select>  
                            </div>
<!--                             <div class="form-group col-md-6" style="text-align: center">
                                <input type="radio" name="gen" class="" id="male" checked="" value="Male" <?php
                                                               if ($user->user_gender == 'Male') {
                                                                   echo 'checked';
                                                               }
                                                               ?>><label style="font-size:15px;margin-left:10px">Male</label>
                               
                            </div>
                            <div class="form-group col-md-6">
                               
                                <input type="radio" name="gen" class="" id="female" value="Female"  <?php
                                                               if ($user->user_gender == 'Female') {
                                                                   echo 'checked';
                                                               }
                                                               ?>><label style="font-size:15px;margin-left:10px">Female</label>
                            </div>-->
                             <div class="form-group col-md-6">
                                 <select name="occupation" id="occupation" class="form-control logtxt">
                                                            <option value="">select occupation</option>

                                                            <?php
                                                            
                                                            $data1=$this->db->get_where("occupation")->result_array();
                                                            foreach ($data1 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['occupation_id']; ?>" <?php
                                                                if ($user->user_occupation == $row['occupation_id']) {
                                                                    echo "selected";
                                                                }
                                                                ?>><?php echo $row['occupation_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>
                            </div>
                           
                            <div class="form-group col-md-12">
                                <input type="email" name="email" value="<?php echo $user->user_email;?>" class="form-control logtxt" id="email" placeholder="Email">
                            </div>
                          
                            <button type="submit" class="btn submitfrm" onclick="return valid();">Submit</button>
                            
                        </form>
                        <?php }?>
                    </div>
                </div>

                <div class="col-md-4"></div>
                <div class="col-md-3">
                   <?php   $abc = $this->db->get("tblgeneralsetting")->result_array();

          // echo $file_name = $abc[2]['general_value'];
                                        if($abc[3]['general_value']){?>
                            <img src="<?php echo base_url() . "uploads/" . $abc[3]['general_value']; ?>" alt="" class="img-responsive sidelog"/>
                           <?php }else{?>
                       <img src="<?php echo base_url(); ?>images/logo100.png" class="img-responsive sidelog">
              
                             <?php } ?>
                
                </div>
            </div>
                        
<!--                        <div class="row">
                <div class="col-md-5">
                    <div id="polina1" 
   style="max-height: 594px;" >
                        <form role="form" action="<?php echo base_url(); ?>Welcome/save_user" method = "post">
                            <div class="col-md-12">
                                <h1>Registration</h1>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="username" class="form-control logtxt" id="user" placeholder="Username">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="first_name" class="form-control logtxt" id="first_name" placeholder="First Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="last_name" class="form-control logtxt" id="last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="dob" readonly="" id="dob" class="form-control logtxt date-picker" placeholder="DOB" />
                                    <span class="input-group-addon symbolism">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                             <div class="form-group col-md-6">
                                 <input type="text" name="zip" class="form-control logtxt" id="zip" onkeypress="return isNumber(event)"  placeholder="Zip Code">
                                 
                            </div>
                              <div class="form-group col-md-6"> <?php
                                                            //$data1=$this->db->get_where("occupation",array('status'=>'active'))->result_array();
                                                          //print_r($data1);
                                                            ?>
                              <select name="gen" id="gender" class="form-control logtxt">
                                                            <option value="">select gender</option>

                                                                <option value="Male">Male</option>
                                                                <option value="Female">Female</option>
                                                        </select>  
                            </div>
                            
                            <div class="form-group col-md-6" style="text-align: center">
                                <input type="radio" name="gen" class="form-control logtxt" id="male" checked="" value="Male"><label style="font-size:15px;margin-left:10px">Male</label>
                               
                            </div>
                            <div class="form-group col-md-6">
                               
                                <input type="radio" name="gen" class="" id="female" value="Female"><label style="font-size:15px;margin-left:10px">Female</label>
                            </div>
                            
                            <div class="form-group col-md-6"> <?php
                                                            //$data1=$this->db->get_where("occupation",array('status'=>'active'))->result_array();
                                                          //print_r($data1);
                                                            ?>
                              <select name="occupation" id="occupation" class="form-control logtxt">
                                                            <option value="">select occupation</option>

                                                            <?php
                                                            $data1=$this->db->get_where("occupation")->result_array();
                                                            foreach ($data1 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['occupation_id']; ?>"><?php echo $row['occupation_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>  
                            </div>
                            <div class="form-group col-md-12">
                                <input type="email" readonly onfocus="this.removeAttribute('readonly');" name="email" class="form-control logtxt" id="email" placeholder="Email">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="password" readonly onfocus="this.removeAttribute('readonly');" name="password" class="form-control logtxt" id="pass" placeholder="Password">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="checkbox" class="" id="privacy"> 
                                <label><a href="<?php echo base_url(); ?>Welcome/privacy_policy"  style="font-size:15px;margin-left:10px;color:white">Privacy Policy </a></label>
                           <div class="popup" onclick="myFunction()" style="font-size:15px;margin-left:10px;color:white">Agree to Privacy Policy
                           </div>
                            </div>

                            <button type="submit" onclick="return valid();"  class="btn submitfrm">Submit</button>
                           
                        </form>
                    </div>
                </div>

                <div class="col-md-4"></div>
                <div class="col-md-3">
                    <img src="<?php echo base_url(); ?>images/logo100.png" class="img-responsive sidelog">
                </div>
            </div>-->
                      
                    </div>
               
            </div>


            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        <script>




                                            var gap = 20;
                                            var boxH = $(window).height() - gap;
                                            var boxW = $(window).width() - gap * 2;
                                          

        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
<script type="text/javascript">
  
        
      
        //alert(date_val);
     
    
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      alert("Enter only numbers"); 
        return false;

    }
    return true;
}

   $(function () {
                                   $('.date-picker').datepicker({
                                       changeMonth: true,  
                                       changeYear: true, 
                                       showButtonPanel: true,
                                         dateFormat: 'yy-mm-dd',
                                       maxDate: '0',  
                                       yearRange: '1900:' + new Date().getFullYear()
                                    });
                                });
function valid() {
    
   // alert("vfdg");
      var user = document.getElementById('user').value;
        var first_name = document.getElementById('first_name').value;
         var last_name = document.getElementById('last_name').value;
var occupation = document.getElementById('occupation').value;
         var email = document.getElementById('email').value;
          var gender= document.getElementById('gender').value;
         if (gender == '')
        {
           
             $.fallr.show({
                        content: '<p>Gender is Required</p>',
                        position: 'center'
                    });
            return false;
        }
         if (user == '')
        {
           
             $.fallr.show({
                        content: '<p>Username is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (first_name == '')
        {
           
             $.fallr.show({
                        content: '<p>First Name is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (last_name == '')
        {
           
             $.fallr.show({
                        content: '<p>Last Name is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (email == '')
        {
            
             $.fallr.show({
                        content: '<p>Email Address is Required</p>',
                        position: 'center'
                    });
            return false;
        }
           if (occupation == '')
        {
           
             $.fallr.show({
                        content: '<p>Occupation is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        }
</script>
    </body>
</html>