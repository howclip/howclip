<?php
echo $name1 = $this->session->userdata['user_id'];
//error_reporting(~E_NOTICE);
//if ($param1) {
//    $result = $this->db->get_where('user', array('user_id' => $param1))->row_array();
//   
//    $formaction = 'edit';
//} else {
//    $formaction = 'create';
//}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
         <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
    <style>
/* Popup container - can be anything you want */
.popup {
    position: relative;
    display: inline-block;
    cursor: pointer;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}

/* The actual popup */
.popup .popuptext {
    visibility: hidden;
    width:450px;
    height:400px;
    background-color: #fff;
   color:black;
    text-align: center;
    border-radius: 6px;
    padding: 8px 0;
    position: absolute;
    z-index: 1;
    bottom: 125%;
    left: 350px;
    margin-left: -80px;
}

/* Popup arrow */
.popup .popuptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
}

/* Toggle this class - hide and show the popup */
.popup .show {
    visibility: visible;
    -webkit-animation: fadeIn 1s;
    animation: fadeIn 1s;
}

/* Add animation (fade in the popup) */
@-webkit-keyframes fadeIn {
    from {opacity: 0;} 
    to {opacity: 1;}
}

@keyframes fadeIn {
    from {opacity: 0;}
    to {opacity:1 ;}
}
</style>
    </head>
    <body class="holder1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <img src="<?php echo base_url(); ?>images/insighthead.png" class="img-responsive agnhead">
                </div>
            </div>
            <?php if ($this->session->flashdata('flash_message')) { ?>
                <div class="alert alert-block alert-success fade in">
                    <a class="close" data-dismiss="alert" onClick="return closemsg();" href="#" aria-hidden="true">X</a>
                    <h4><i class="fa fa-smile-o"></i> <?php
                        echo
                        $this->session->flashdata('flash_message');
                        ?>  <i class="fa fa-thumbs-up"></i></h4>
                </div>
                <?php
            }
            if ($this->session->flashdata('permission_message')) {
                ?>
            <div class="alert alert-block alert-danger fade in"  style="padding:5px;">
                <a class="close" onClick="closemsg()" data-dismiss="alert" href="#" 
                       aria-hidden="true">X</a>
                    <h5 style="font-size: 14px;"><i class="fa fa-frown-o"></i> <?php
                        echo
                        $this->session->flashdata('permission_message');
                        ?><i class="fa fa-thumbs-down"></i></h5>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-md-5">
                    <div id="polina1">
                        <form role="form" action="<?php echo base_url(); ?>Welcome/save_user" method = "post">
                            <div class="col-md-12">
                                <h1>Registration</h1>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="username" class="form-control logtxt" id="user" placeholder="Username">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="first_name" class="form-control logtxt" id="first_name" placeholder="First Name">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="text" name="last_name" class="form-control logtxt" id="last_name" placeholder="Last Name">
                            </div>
                            <div class="form-group col-md-6">
                                <div class='input-group date' id='datetimepicker1'>
                                    <input type='text' name="dob" readonly="" id="dob" class="form-control logtxt date-picker" placeholder="DOB" />
                                    <span class="input-group-addon symbolism">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                </div>
                            </div>
                             <div class="form-group col-md-6">
                                 <input type="text" name="zip" class="form-control logtxt" id="zip" onKeyPress="return isNumber(event)"  placeholder="Zip Code">
                                 
                            </div>
                              <div class="form-group col-md-6"> <?php
                                                            //$data1=$this->db->get_where("occupation",array('status'=>'active'))->result_array();
                                                          //print_r($data1);
                                                            ?>
                              <select name="gen" id="gender" class="form-control logtxt">
                              <option value="">select gender</option>
                             <option value="Male">Male</option>
                             <option value="Female">Female</option>
                             </select>  
                             </div>
                             <!-- <div class="form-group col-md-6" style="text-align: center">
                             <input type="radio" name="gen" class="form-control logtxt" id="male" checked="" value="Male"><label 									                             style="font-size:15px;margin-left:10px">Male</label>  
                             </div>
                             <div class="form-group col-md-6">
                             <input type="radio" name="gen" class="" id="female" value="Female"><label style="font-size:15px;margin-left:10px">Female</label>
                             </div>-->
                             <div class="form-group col-md-6"> <?php
                                                            //$data1=$this->db->get_where("occupation",array('status'=>'active'))->result_array();
                                                          //print_r($data1);
                                                            ?>
                              <select name="occupation" id="occupation" class="form-control logtxt">
                                                            <option value="">select occupation</option>

                                                            <?php
                                                            $data1=$this->db->get_where("occupation")->result_array();
                                                            foreach ($data1 as $row) {
                                                                ?>
                                                                <option value="<?php echo $row['occupation_id']; ?>"><?php echo $row['occupation_name']; ?></option>
                                                                    <?php } ?>
                                                        </select>  
                            </div>
                            <div class="form-group col-md-12">
                                <input type="email" readonly onFocus="this.removeAttribute('readonly');" name="email" class="form-control logtxt" id="email" placeholder="Email">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="password" readonly onFocus="this.removeAttribute('readonly');" name="password" class="form-control logtxt" id="pass" placeholder="Password">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="checkbox" class="" id="privacy"> 
<!--                                <label><a href="<?php echo base_url(); ?>Welcome/privacy_policy"  style="font-size:15px;margin-left:10px;color:white">Privacy Policy </a></label>-->
                           <div class="popup" onClick="myFunction()" style="font-size:15px;margin-left:10px;color:white">Agree to Privacy Policy
                            <div class="popuptext" id="myPopup"><div class="">
                                    <div class="">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                       
                                    </div>
                                    <div class="modal-body">
                                         <div class="col-md-12">
                               <?php
                            $query = $this->db->get('tblgeneralsetting')->result_array();
                        $title = $query[0]['general_value'];
                         $content = $query[1]['general_value'];
                            ?>

                                <h1 style="margin-top: 15px"><?php echo ucwords($title); ?></</h1>
                            </div>
                            <div class="form-group col-md-12">
                          <label style="font-size:15px;border:1px double black; "><?php echo ucfirst(strtolower($content)); ?></label>                         
                            </div>
                                    </div>
                                    
                                </div>
                            
                            
                            </div></div>
                            </div>

                            <button type="submit" onClick="return valid();"  class="btn submitfrm">Submit</button>
                            <p class="mycon">Already have an Account?  <a href="<?php echo base_url('Welcome'); ?>/index" class="backlog">LOGIN</a></p>
                        </form>
                    </div>
                </div>

                <div class="col-md-4"></div>
                <div class="col-md-3">
                    <img src="<?php echo base_url(); ?>images/logo100.png" class="img-responsive sidelog">
                </div>
            </div>


        </div>
        <div class="footer1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p>Phone: 202.681.0239, Fax: 202.869.3765, inquiries@pueobusinesssolutions.com </p>
                    </div>
                </div>
            </div>
        </div>


   
    </body>
</html>
<script src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">
 <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
       <script>
// When the user clicks on div, open the popup
function myFunction() {
    var popup = document.getElementById("myPopup");
    popup.classList.toggle("show");
}
</script>
 <script>




                                            var gap = 20;
                                            var boxH = $(window).height() - gap;
                                            var boxW = $(window).width() - gap * 2;
                                          

        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);

            (function () {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();

        </script>
<script type="text/javascript">
                         
                                    $('.date-picker').datepicker({
                                       changeMonth: true,  
                                       changeYear: true, 
                                       showButtonPanel: true,
                                         dateFormat: 'yy-mm-dd',
                                       maxDate: '0',  
                                       yearRange: '1900:' + new Date().getFullYear()
                                    });
                               
                                function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      alert("Enter only numbers"); 
        return false;

    }
    return true;
}
</script>
<script>
    function valid() {
        
       // alert("fghfh");
        var pass = document.getElementById('pass').value;
        var user = document.getElementById('user').value;
        var first_name = document.getElementById('first_name').value;
        var last_name = document.getElementById('last_name').value;
        var dob = document.getElementById('dob').value;
        var occupation = document.getElementById('occupation').value;
        var email = document.getElementById('email').value;
        var privacy = document.getElementById('privacy').value;
        var gender= document.getElementById('gender').value;
  
     // alert(first_name); return false;
        if (user == '')
        {
            
             $.fallr.show({
                        content: '<p>Username is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (first_name == '')
        {
            
             $.fallr.show({
                        content: '<p>First Name is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (last_name == '')
        {
            
             $.fallr.show({
                        content: '<p>Last Name is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (dob == '')
        {
           
             $.fallr.show({
                        content: '<p>DOB is Required</p>',
                        position: 'center'
                    });
            return false;
        }
       
     
        if (email == '')
        {
            
             $.fallr.show({
                        content: '<p>Email Address is Required</p>',
                        position: 'center'
                    });
            return false;
        }

        if (pass == '')
        {
          
             $.fallr.show({
                        content: '<p>Password is Required</p>',
                        position: 'center'
                    });
            return false;
        }


        if (pass.length < 8)
        {
            
             $.fallr.show({
                        content: '<p>Password must be 8 Charactes Long</p>',
                        position: 'center'
                    });
            return false;
        }
   if (occupation == '')
        {
           
             $.fallr.show({
                        content: '<p>Occupation is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if (gender == '')
        {
           
             $.fallr.show({
                        content: '<p>Gender is Required</p>',
                        position: 'center'
                    });
            return false;
        }
        if($("#privacy").is(":checked"))
        {
     
        
        }else
        {
           
             $.fallr.show({
                        content: 'Cannot complete registration without agreeing to the Privacy Policy',
                        position: 'center'
                    });
            return false;
        }
    }
     function showmodel() {
     //alert("rftg");
    $('#myModal').modal('show');
                   //  $('#first').modal('show');  
                   }
function closemsg()
{
    $('.alert').hide();
    }

</script>