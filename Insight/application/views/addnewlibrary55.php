<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PUEO</title>
        <!-- Bootstrap -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery-fallr-2.0.1.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js"></script>
        <link rel="stylesheet" type="text/css" media="screen" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/themes/base/jquery-ui.css">

    </head>
    <body>
        <div  class="wrapper">
            <div  class="header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-3 col-xs-12">
                            <img src="<?php echo base_url(); ?>images/logo.png" class="img-responsive headlog">
                        </div>
                        <div class="col-md-7 col-xs-6">
                            <img src="<?php echo base_url(); ?>images/create.png" class="img-responsive crea">

                        </div>
                        <div class="col-md-2 col-xs-6">
                            <div class="dropdown" style="text-align:right;">
                                <button class="btn logdrop dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <img src="<?php echo base_url(); ?>images/user-group-icon.png" class="img-responsive dropimg"><?php echo ucwords($this->session->userdata('username')); ?>
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="right:0;left:initial;">
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/dashboard">Home</a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/change_pass">Change Password</a></li>
                                    <li><a href="<?php echo base_url(); ?>index.php/Dashboard/edit_profile/<?php echo $this->session->userdata('user_id'); ?>">Change Profile</a></li>

<!--                                    <li><a href="<?php echo base_url('Dashboard'); ?>/groups">Groups</a></li>-->
                                    <li><a href="<?php echo base_url('Welcome'); ?>/logout">Logout</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="menustrip">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h4> </h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sheet">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <?php if ($this->session->flashdata('flash_message')) { ?>
                                <div class="alert alert-block alert-success fade in" style="padding:5px;">
                                    <a class="close" data-dismiss="alert" onClick="return closemsg();" href="#" 
                                       aria-hidden="true">X</a>
                                    <h4 style="margin-bottom: 0;"><i class="fa fa-smile-o"></i> <?php
                                        echo
                                        $this->session->flashdata('flash_message');
                                        ?>  <i class="fa fa-thumbs-up"></i></h4>
                                </div>
                            <?php }
                            ?>

                            <div class="paragraph">
                                Create or edit your own personal library with any text records you would like Insight to identify and associated data you would like to display when reading or typing in your favorite Web apps (Facebook, Email, Chat).  Make it accessible to all, distribute the Access ID to your friends, or keep it entirely to yourself – the choice is yours and we hope you enjoy.

                            </div>
                        </div> 
                    </div>
                    <form role="form" action="<?php echo base_url(); ?>index.php/Dashboard/save_library" method = "post" enctype="multipart/form-data">
                        <div>

                        </div>



                        <div class="row topspc">
                            <div class="col-md-3 spc"> 
                                <div class="label-form"> Library Name</div>
                            </div>
                            <div class="col-md-3 spc">
                                <input type="text" name="library_name" class="form-control inputform" id="lib_name"  placeholder="">
                            </div>
                            <div class="col-md-3 spc">
                                <div class="label-form"> Assign Color</div>
                            </div>
                            <div class="col-md-3 spc">
                                 <input  class="jscolor form-control inputform" value=""   name="color" id="color">
                            </div>
                        </div>
                      
                        <div class="row topspc">
                            <div class="col-md-3 spc">

                                <div class="label-form"> Description</div>

                            </div>
                            <div class="col-md-9 spc">

                                <input type="text" name="description" class="form-control inputform" id="desc" placeholder="">

                            </div>

                        </div>
                        <div class="row topspc">

                            <div class="col-md-3 spc">
                                <div class="label-form"> Access Type</div>


                            </div>
                            <div class="col-md-3 spc">

                                <select class="form-control inputform" name="access_type" id="access">
                                    <option value=""> Select Access Type </option>
                                    <option value="private"> Private </option>
                                    <option value="public"> Public </option>

                                </select>

                            </div>
                            <div class="col-md-3 spc">
                                <div class="label-form"> Library Group</div>
                            </div>
                            <div class="col-md-3 spc">
                                <select class="form-control inputform" name="group_name" id="lib_group">
                                    <option value=""> Select Group </option>
                                    <?php foreach ($groups as $group) { ?>
                                        <option value="<?php echo $group->id; ?>"> <?php echo $group->name; ?> </option>
                                    <?php } ?>
                                </select>
                            </div></div>

                        <div class="table-responsive">
                            <table class="table table-striped empbox" id="mtable">
                                <thead>
                                    <tr id="headapp">
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="red" id="redapp">
                                    </tr>
                                    <tr id="rowapp0" class="rowapp">
                                    </tr>
                                </tbody>
                            </table>

                        </div>
                        <div class="row">
                            <div class="col-md-5 col-sm-3" style="width:33%">
                                <a href="#" onClick="create_col();"> <img src="<?php echo base_url(); ?>images/buttoin.png" class="img-responsive addons">
                                    <h4 class="addhead" onClick="create_col();">Add New Column (Up to 10)</h4></a>
                            </div>
                            <div class="col-md-5 col-sm-3" style="width:36%">
                                <a href="#" onClick="create_row();"> <img src="<?php echo base_url(); ?>images/buttoin.png" class="img-responsive addons">
                                    <h4 class="addhead" onClick="create_row();">Add New Record  (Up to 1000)</h4></a>
                            </div>
                              <div class="col-md-1 col-sm-1">
                                 <button style="height:52px;width:100%" type="button" class="btn liberary" style="height: 50px;" onclick="show_invite()">Invite User</button>
                                <!--<button type ="submit" id="btnSubmit" style="padding:0px;border-radius: 10px;" onclick="return validate();"> <a href="#"> <img src="<?php echo base_url(); ?>images/submit.png" class="img-responsive subimage"></a></button>-->
                            </div>
                            <div class="col-md-2 col-sm-2">
                                 <!--<button style="height:52px;" type="button" class="btn liberary" style="height: 50px;">Invite User</button>-->
                                <button type ="submit" id="btnSubmit" style="padding:0px;border-radius: 10px;width:82%" onclick="return validate();"> <a href="#"> <img src="<?php echo base_url(); ?>images/submit.png" class="img-responsive subimage"></a></button>
                            </div>
                        </div>
                        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Record Title</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="text" name="row_title" class="form-control inputform" onKeyUp="suggestion12(this.value);" id="row_title" placeholder="Record Title">
                                    <div id="div11" style="margin-bottom: 10px">
                                            <input name="type1" class="form-control inputform " style="margin-top:20px;" id="search">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onClick="add_row();">Add Row</button>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="modal fade" id="first" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Enter Your First Record</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="text" name="col_title" class="form-control inputform " style="margin-bottom:20px;" id="first_col" placeholder="Column Title">
                                        <input type="text" name="row_title" class="form-control inputform " style="margin-bottom:20px;" id="first_row" onKeyUp="suggestion(this.value);"  placeholder="Row Title">
                                   
                                        <div id="div1" style="margin-bottom: 10px">
                                            <input name="type1" class="form-control inputform " style="margin-top:20px;" id="search">
                                        </div>
                                        <select name="type" class="form-control inputform " style="margin-bottom:20px;" id="type" onclick="suggestion1();">
                                            <option value=""> Select Field Type</option>
                                            <?php
                                            $row1 = $this->db->get_where('column_type')->result_array();
                                            foreach ($row1 as $new) {
                                                ?> 
                                                <option value="<?php echo $new['id']; ?>"><?php echo ucwords($new['name']); ?></option> 


                                            <?php } ?>
                                        </select>
                                       
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onClick="addpop();">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal fade" id="colModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Column Title</h4>
                                    </div>
                                    <div class="modal-body">
                                        <input type="text" name="row_title" class="form-control inputform" id="col_title" placeholder="Column Title">
                                        <select name="type" class="form-control inputform " style="margin-top:20px;" id="type1">
                                            <option value=""> Select Field Type</option>
                                            <?php
                                            $row1 = $this->db->get_where('column_type')->result_array();
                                            foreach ($row1 as $new) {
                                                ?> 
                                                <option value="<?php echo $new['id']; ?>"><?php echo ucwords($new['name']); ?></option> 


                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onClick="createcolumn();">Add Column</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="modal fade" id="invite_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top:100px;">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Invite User</h4>
                                    </div>
                                    <div class="modal-body"> 
                          <div class="row">
                <div class="col-md-12">
                    <div id="polina1">
                       
                        <!--<form role="form" action="<?php echo base_url(); ?>index.php/Dashboard/invite_user" method = "post">-->
                            <div class="col-md-12">
                                <?php $data=$this->db->get('email')->result_array();
                                     // $data = $this->db->get()->result_
                                ?>
                                <h1>Invite Users</h1>
                            </div>
                            <div class="col-md-12" style="margin-top: 10px">
                                <label style="font-size:15px;"> Add User Email Id</label>
                                  </div>
                                    <div class="col-md-6" style="color:red;margin-top: 10px">
                                         
                                        
                                        
                                        <input type="hidden" class="form-control" name="library"   value="<?php print_r($groups); ?>">
                                 
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 1" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="etext" class="form-control" name="mail1[]"   placeholder="Enter user email 2" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 3" value="" onblur="validateEmail(this);" >
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 4" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 5" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"  placeholder="Enter user email 6" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 7" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 8" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 9" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 10" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 11" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 12" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 13" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 14" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 15" value="" onblur="validateEmail(this);">
                                   </div>
                                <div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"  placeholder="Enter user email 16" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 17" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 18" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 19" value="" onblur="validateEmail(this);">
                                   </div><div class="col-md-6" style="color:red;margin-top: 10px">
                                <input type="text" class="form-control" name="mail1[]"   placeholder="Enter user email 20" value="" onblur="validateEmail(this);">
                                   </div>
                            <div class="form-group col-md-12">
                                <label style="font-size:15px;"><?php echo ucwords($data[0]['general_title']); ?></label>
                                <input type="text" class="form-control" name="sender" id="subject"   value="<?php echo $data[0]['general_value']; ?>">
		
                                                        
                            </div>
                            <div class="form-group col-md-12">
                                <label style="font-size:15px;"><?php echo ucwords($data[1]['general_title']); ?></label>
                                <input type="text" class="form-control" readonly="" rows="4" cols="20" id="sender" name="subject" onblur="validateEmail(this);" value="<?php echo $data[1]['general_value']; ?>"  >
		 
                                                        
                            </div>
                             <div class="form-group col-md-12">
<!--                                <label style="font-size:15px;"><?php echo ucwords($data[3]['general_title']); ?></label>
                                <textarea  style="resize:none; height:150px" class="form-control" rows="4" cols="20" id="privacy_content" name="privacy_content"   ><?php echo $data[3]['general_value']; ?></textarea>
		 
                                 <select class="form-control " name="status" id="status">
                                    
                                      <option value="Active" <?php
                                                                if ($data[3]['general_value'] == 'active') {
                                                                    echo "selected";
                                                                }
                                                                ?>>Active</option>
                                        <option value="Inactive" <?php
                                                                if ($data[3]['general_value']== 'inactive') {
                                                                    echo "selected";
                                                                }
                                                                ?>>Inactive</option>
                                </select>                        -->
                            </div>
                <div class="form-group col-md-12">
                    <label style="font-size:15px;"><?php echo ucwords($data[2]['general_title']); ?></label>
                          <textarea name="content" class="ckeditor form-control " id="content"><?php echo $data[2]['general_value']; ?></textarea>

<!--                    <textarea  style="resize:none; height:150px" class="form-control"  id="content" name="content" ><?php echo $data[2]['general_value']; ?></textarea>-->
<!--		 <input type="hidden" class="form-control" 
                                                       name="name1"  value="<?php echo $data[0]['general_id']; ?>" >
                                                <input type="hidden" class="form-control" 
                                                       name="name2"  value="<?php echo $data[1]['general_id']; ?>" >
                                                 <input type="hidden" class="form-control" 
                                                       name="name3"  value="<?php echo $data[2]['general_id']; ?>" >
                                                 <input type="hidden" class="form-control" 
                                                       name="name4"  value="<?php echo $data[3]['general_id']; ?>" >-->
                                                        
                            </div>
                            <button type="submit" class="btn submitfrm" onclick="return valid();" style="width:100px;margin-left: 650px;visibility:hidden;">Send</button>
                            
                        <!--</form>-->
                        
                    </div>
                </div>

                <div class="col-md-4"></div>
               
            </div>
                                        
                                        </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <!--<button type="button" class="btn btn-primary" onClick="createcolumn();">Add Column</button>-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <div class="footer">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <p>Phone: <?php echo $banner[5]['general_value']?>, Fax: <?php echo $banner[6]['general_value']?>, <?php echo $banner[7]['general_value']?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
        <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jscolor.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-fallr-2.0.1.js"></script>
        
<!--<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>-->
        <script>
//$(function() {
//    $( "#first_row" ).autocomplete({
//       // source: '<?php echo base_url(); ?>Dashboard/auto_sug'
//    });
//});
</script>
        <script type="text/javascript">
            
            
            function show_invite()
            {
              //  alert("fg");
              
              
              $('#invite_modal').modal('show');
            }
//function suggestion(id){
//  //alert(id);  
//  var abc111 = '';
//        $.ajax({
//            url: "<?php echo base_url(); ?>index.php/Dashboard/auto_sug",
//           
//            type: "POST",
//            data: {'id': id
//                
//            },
//            success: function (response)
//            {console.log(response);
//               if (response) {
//              
//
//                $("#div1").html(response);
//                $("#div1").show();
//                 //abc111 = jQuery.parseJSON(response);
//                
//             }
//
//            }
//
//        });
//}
//function suggestion12(id){
//  //alert(id);  
//  var abc111 = '';
//        $.ajax({
//            url: "<?php echo base_url(); ?>index.php/Dashboard/auto_sug",
//           
//            type: "POST",
//            data: {'id': id
//                
//            },
//            success: function (response)
//            {//console.log(response);
//               if (response) {
//              
//
//                $("#div11").html(response);
//                $("#div11").show();
//                 //abc111 = jQuery.parseJSON(response);
//                
//             }
//else{
//    $("#div11").hide();
//}
//            }
//
//        });
//}
//function suggestion1(){
//$("#div1").hide();
//$("#div11").hide();
//}
function changevalue(){
  var sug = $('#search').val();
document.getElementById('first_row').value=sug;
//document.getElementById('row_title').value=sug;
//$("#div1").hide();
}
function changevalue1(){
  var sug = $('#search').val();

document.getElementById('row_title').value=sug;
//$("#div1").hide();
}
        </script>
        <script>
            function checkUrl(url)
                    {
                    //regular expression for URL
                    var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
                    if (pattern.test(url)){

                    return true;
                    } else {
                       
                        //alert("enter valid url");
//                        var lib = document.getElementById('url').value;
//            f (lib == '') {
//            //alert('Enter Library Name');
//            $.fallr.show({
//            content: '<p>Enter valid URL</p>',
//                    position: 'center'
//            });
//            return false;
//            }
           $.fallr.show({
            content: '<p>Url is not valid</p>',
                    position: 'center'
            });
             return false;
                    //alert("Invalid Url");
                   // return false;gg
                    }
                    }
            $(document).ready(function() {
// Datepicker Popups calender to Choose date.
            $(function() {
            $("#datepicker").datepicker();
          //  $(".usedatepicker").datepicker();
// Pass the user selected date format.
            $("#format").change(function() {
            $("#datepicker").datepicker("option", "dateFormat", $(this).val());
            $(".usedatepicker").datepicker("option", "dateFormat", $(this).val());
            });
            });
            });
            
            
//            $('body').on('focus', ".usedatepicker", function(){
//            $(this).datepicker(); 
//          $(".usedatepicker").removeClass("hasDatepicker");
//    });
//         
//            $('.usedatepicker').each(function(){
//            $(this).datepicker();
//              $(".usedatepicker").removeClass("hasDatepicker");
//            });
    
           $('#lib_name').blur(function () {
            if ($(this).val()) {
            var chk = $(this).val();
            var post_data = {
            'lib': chk,
            };
            $.ajax({
            type: "POST",
                    url: "<?php echo base_url() ?>index.php/Dashboard/chk_lib",
                    data: post_data,
                    success: function (data) {
                    console.log(data);
                    var res = data.split("||");
                    var ss = res[0].trim();
                    if (ss == 'success') {

                    $.fallr.show({
                    content: '<p>Library Name already Exists</p>',
                            position: 'center'
                    });
                    document.getElementById('lib_name').value = '';
                    return false;
                    }
                    if (ss == 'fail') {

                    return true;
                    }

                    }
            });
            }
            });
            var gap = 20;
            var boxH = $(window).height() - gap;
            var boxW = $(window).width() - gap * 2;
        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-36251023-1']);
            _gaq.push(['_setDomainName', 'jqueryscript.net']);
            _gaq.push(['_trackPageview']);
            (function () {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
            })();
        </script>


        <script>
            function openpopup()
            {

            $('#myModal').modal('show');
            }
            window.abc = 0;
            function add_row()
            {

            var title = document.getElementById('row_title').value;
            if (title != '') {
            var search_val = document.getElementById('search').value;
            var clonehead = $("#mtable thead tr th:last").clone();
            var clonered = $("#mtable tbody tr td:last").clone();
            var clonerow = $("#mtable tbody tr td:first").clone();
            var genrow = $("#mtable tbody tr:last").clone().find('input').val('').end();
             $(".usedatepicker").removeAttr('id');
            var len = $(".rowapp").length + 1;
            if (len > 1000) {
            alert("maximum row size is 1000");
            return false
            }
            genrow.attr('id', "rowapp" + ( + abc + 1));
            $('#mtable tbody').append(genrow);
            var delid = "'rowapp" + ( + abc + 1) + "'";
            var rtitle = "<input type ='hidden' name='rowname[]' value= " + title + "> <input type ='hidden' name='search_name[]' value= "+ search_val +">";
            $("#mtable tbody tr:last td:first").html(title + rtitle);
            
            $('#rowapp' + ( + abc + 1)).find("td:last").html('<a href="#" onclick ="del(' + delid + ')">DELETE</a>');
            $('#myModal').modal('hide');
            abc++;
            var title = document.getElementById('row_title').value = '';
//              $('body').on('focus', ".usedatepicker", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker").removeClass("hasDatepicker");
//   }); 
            } else {
            alert('Enter Row Title');
            return false;
            }
            }
            function addpop()
            {
            //var val=  $("#type option:selected").text();
            //console.log(val1);
            var val1 = $('#type').val();
            //var e = document.getElementById("type");
            //var type = e.options[e.selectedIndex].value;
            var search_val = document.getElementById('search').value;
           // alert(search_val);
            var val = val1.toLowerCase();
            //alert(val);
            var rclass = '"rowapp0"';
            var cclass = '1';
            var frow = document.getElementById('first_row').value;
            var fcol = document.getElementById('first_col').value;
            if ((frow != '') && (fcol != ''))
            {
            var head = "<th></th><th class='1'><a href='#' onclick='delcol(" + cclass + ")' > DELETE</a></th><th></th>";
            var red = "<td  class='red'></td><td  class='1'>" + fcol + "<input type ='hidden' name='colname[]' value= " + fcol + "><input type ='hidden' name='coltype[]' value= " + val + "></td><td > </td>";
			 $.ajax({
    url: '<?php echo base_url();?>index.php/Dashboard/show_list_data/first',
    type:"POST",
    data:{'column_value': val,
            'frow':frow,
            'fcol':fcol,
            'rclass':rclass,
            'search_val':search_val
        },
    success: function(response)
    {
       //alert(response);
	   
            $('#headapp').append(head);
            $('#redapp').append(red);
            $('#rowapp0').append(response);
            $('#first').modal('hide');
            
//              $('body').on('focus', ".usedatepicker", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker").removeClass("hasDatepicker");
//   });
    
     $("#del_btn").css("background-color", "#333333"); 
         $("#del_btn").css("border", "1px solid #333333"); 
    }
    
  });
 
          

            } else {
            alert('Enter Row and Column Title');
            return false;
            }

            }
            function create_col()
            {

            $('#headapp').each(function () {
            if ($(this).find('th').length == 0) {
            $('#first').modal('show');
            } else
            {
            $('#colModal').modal('show');
            }
            });
            //  $(".usedatepicker").removeClass("hasDatepicker");
            }

            function createcolumn() {
            var val1 = $('#type1').val();
            //var e = document.getElementById("type");
            //var type = e.options[e.selectedIndex].value;

            var val = val1.toLowerCase();
           // alert(val);
            var col = document.getElementById('col_title').value;
            if (col != '') {
            var len = $("#mtable thead tr th");
            if (len.length > '11') {
            alert("Maximum column size is 10");
            return false;
            }
            var clonerow = $(".rowapp td:last").clone();
            var clonered = $("#redapp td:last").clone();
            var count = document.getElementById('redapp').getElementsByTagName("td").length;
            var count = count - 1;
            var clonered = "<td class= "+ count + ">" + col + "<input type ='hidden' name='colname[]' value= " + col + "><input type ='hidden' name='coltype[]' value =" + val +">";
            var clonehead = '<th class = ' + count + '><a href="#" onclick="delcol(' + count + ')" > DELETE</a></th>';
            $('#headapp').find("th:last").before(clonehead);
            $('#redapp').find("td:last").before(clonered);
            $('.rowapp').each(function () {
            var count1 = document.getElementById(this.id).getElementsByTagName("td").length;
            var row = document.getElementById(this.id);
            var x = row.insertCell(count1 - 1);
            x.className = count;
            
            //alert(val);
            
             $.ajax({
    url: '<?php echo base_url();?>index.php/Dashboard/show_list_data/column',
    type:"POST",
    data:{'column_value': val,
            'col':col
        },
    success: function(response)
    {
       //alert(response);
	 x.innerHTML = response;  
//            $('#headapp').append(head);
//            $('#redapp').append(red);
//            $('#rowapp0').append(response);
//            $('#first').modal('hide');
//      $('body').on('focus', ".usedatepicker", function(){
//           $(this).datepicker(); 
//          $(".usedatepicker").removeClass("hasDatepicker");
//    });
    }
    
  });
            
            
            
//            if (val == "date"){
//
//            x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='"+ val +"'><input type='text' name='" + col + "val[]' class='form-control usedatepicker inputform r0 c0' readonly  placeholder='select date'> ";
//            }
//            else if (val == "image"){
//            x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='"+ val +"'> <input type='file' name='" + col + "val[]' class='form-control inputform r0 c0'  onchange='ValidateSingleInput(this);'> ";
//            }
//            else if (val == "link"){
//            x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='"+ val +"'><input type='text' name='" + col + "val[]' class='form-control abcurl inputform r0 c0'  placeholder='enter Url' onblur='checkUrl(this.value);'> ";
//            }
//            else if (val == "state"){
//
//          x.innerHTML = <?php echo '"<td><input type='?><?php echo "hidden"?><?php echo ' name=';?><?php echo '"';?>+ col + "valtype[]"+<?php echo '"';?><?php echo ' value=';?><?php echo '"';?>+ val +<?php echo '"';?><?php echo '><select class=' ?><?php echo "'form-control inputform r0 c0'". 'name="'; ?> + col + 'val[]' + <?php echo '"';?>  <?php echo '>'; ?><?php foreach ($state as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
             // x.innerHTML= <?php echo '"<td>"' ?><?php echo '"<input type='?><?php echo "hidden"?> <?php echo 'name='?><?php echo '"';?>+col+"valtype[]"+<?php echo '"';?><?php echo ' value='?><?php echo '"';?>+val+<?php echo '"';?><?php echo' ></td><td><select  class=' ?><?php echo "'form-control inputform r0 c0'". 'name="'; ?> + col + 'val[]' + <?php echo '"';?>  <?php echo '>'; ?><?php foreach ($state as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
//
//            }
//            else if (val == "country"){
//
//          x.innerHTML = <?php echo '"<td><input type='?><?php echo "hidden"?><?php echo ' name=';?><?php echo '"';?>+ col + "valtype[]"+<?php echo '"';?><?php echo ' value=';?><?php echo '"';?>+ val +<?php echo '"';?><?php echo '><select class=' ?><?php echo "'form-control inputform r0 c0'". 'name="'; ?> + col + 'val[]' + <?php echo '"';?>  <?php echo '>'; ?><?php foreach ($country as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
           // x.innerHTML= <?php echo '"<td>"' ?><?php echo '"<input type='?><?php echo "hidden"?> <?php echo 'name='?><?php echo '"';?>+col+"valtype[]"+<?php echo '"';?><?php echo ' value='?><?php echo '"';?>+val+<?php echo '"';?><?php echo' ></td><td><select  class=' ?><?php echo "'form-control inputform r0 c0'". 'name="'; ?> + col + 'val[]' + <?php echo '"';?>  <?php echo '>'; ?><?php foreach ($country as $cou) { ?><?php echo '<option value='; ?><?php echo $cou[name]; ?><?php echo '>' ?><?php echo $cou[name]; ?> <?php echo' </option>'; ?><?php } ?><?php echo '</select></td>"'; ?>
//
//            }
//            else {
//
//            x.innerHTML = "<td><input type='hidden' class=' form-control inputform r0 c0 ' name='" + col + "valtype[]' value='"+ val +"'><input type='text' name='" + col + "val[]' class='form-control inputform r0 c0' id='exampleInputEmail3' placeholder=''> ";
//            }
//
//            // x.innerHTML = "<td><input type='text' name='" + col + "val[]' class='form-control inputform r0 c0' id='exampleInputEmail3' placeholder=''> ";





            });
            document.getElementById('col_title').value = '';
            $('#colModal').modal('hide');
            } else {
            alert('Enter Column Title');
            return false;
            }
            }
            function create_row()
            {

            $('#headapp').each(function () {
            if ($(this).find('th').length == 0) {
            $('#first').modal('show');
            } else
            {
            $('#myModal').modal('show');
            }
            });
            
              $(".usedatepicker").removeClass("hasDatepicker");
              $("#search").val("");
            }
            function del(x)
            {
            a = x;
            $.fallr.show({
            buttons: {
            button1: {text: 'Yes', danger: true, onclick: clicked2},
                    button2: {text: 'Cancel'}
            },
                    content: '<p>Are you sure you want to delete?</p>',
                    icon: 'error',
                    position: 'center'
            });
            }

            var clicked2 = function () {
            var re = ''
//before $('.rowapp').find('td').length
                    if ($('#mtable').find('tr').length == 3) {
            $('.red').empty();
            $('.rowapp').empty();
            $('#headapp').empty();
            re = 1;
            a = '';
            } else {
            var row = document.getElementById(a);
            row.parentNode.removeChild(row);
            re = 1;
            a = '';
            }
            if (re == 1) {
            $.fallr.hide();
            a = '';
            }

            };
            var clicked = function () {

            var re = '';
            if ($('.red').find('td').length == 3) {
            $('.red').empty();
            $('.rowapp').empty();
            $('#headapp').empty();
            re = 1;
            a = '';
            } else {
            $('.' + a).remove();
            re = 1;
            a = '';
            }


            if (re == 1) {
            $.fallr.hide();
            a = '';
            }

            };
            window.a = '';
            function delcol(y)
            {
            a = y;
            $.fallr.show({
            buttons: {
            button1: {text: 'Yes', danger: true, onclick: clicked},
                    button2: {text: 'Cancel'}
            },
                    content: '<p>Are you sure you want to delete?</p>',
                    icon: 'error',
                    position: 'center'
            });
            }
            function validate1() {
            var url = document.getElementById("url").value;
            var pattern = /(ftp|http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
            if (pattern.test(url)) {
            //alert("Url is valid");
            return true;
            }
            $.fallr.show({
            content: '<p>Url is not valid</p>',
                    position: 'center'
            });
            document.getElementById('url').value = '';
            // alert("Url is not valid!");
            return false;
            }

            function validate() {
            var lib = document.getElementById('lib_name').value;
            var color = document.getElementById('color').value;
            var lib_group = document.getElementById('lib_group').value;
            var access = document.getElementById('access').value;
            var desc = document.getElementById('desc').value;
            var count = document.getElementById('redapp').getElementsByTagName("td").length;
            if (lib == '') {
            //alert('Enter Library Name');
            $.fallr.show({
            content: '<p>Enter Library Name</p>',
                    position: 'center'
            });
            return false;
            }
            if (lib_group == '') {

            $.fallr.show({
            content: '<p>Enter Library Group Name</p>',
                    position: 'center'
            });
            return false;
            }


            if (desc == '') {

            $.fallr.show({
            content: '<p>Enter Library Description</p>',
                    position: 'center'
            });
            return false;
            }
            if (color == '') {

            $.fallr.show({
            content: '<p>Please Select Color</p>',
                    position: 'center'
            });
            return false;
            }
            if (access == '') {

            $.fallr.show({
            content: '<p>Enter Access Type</p>',
                    position: 'center'
            });
            return false;
            }
            if (count == '0') {

            $.fallr.show({
            content: '<p>Enter Row and Colunm</p>',
                    position: 'center'
            });
            return false;
            }

 var lib1 = document.getElementById('url').value;
            if (lib1 == '') {
            //alert('Enter Library Name');
            $.fallr.show({
            content: '<p>Enter valid URL</p>',
                    position: 'center'
            });
            return false;
            }
            }
            function closemsg()
            {
            $('.alert').hide();
            }





        </script>

        <script>
            function url_validate(userUrl)
            {

            var regUrl = /^(((ht|f){1}(tp:[/][/]){1})|((www.){1}))[-a-zA-Z0-9@:%_\+.~#?&//=]+$/;
            if (regUrl.test(userUrl) == false)
            {

            alert("Website URL is not valid yet.");
            }
            else
            {

            alert()"You have entered a valid website URL!");
            }
            }
            $(document).ready(function () {
            $('.abc').change(function () {
            var val = $(this).val().toLowerCase();
            var regex = new RegExp("(.*?)\.(docx|doc|pdf|xml|bmp|ppt|xls)$");
            if (!(regex.test(val))) {
            $(this).val('');
            alert('Please select correct file format');
            } }); });
        </script> 
        <script type="text/javascript">
            function Validate_url() {
            var regexp = new RegExp("^http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&amp;%\$#_]*)?$");
            var url = document.getElementById("txtUrl").value;
            if (!regexp.test(url)) {
            alert("Not valid Url!");
            } else {
            alert("Valid Url!");
            }
            }

        </script>
        <script>


            function fileValidation(){
            var fileInput = document.getElementById('file');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
            if (!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
            fileInput.value = '';
            return false;
            } else{
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
            document.getElementById('imagePreview').innerHTML = '<img src="' + e.target.result + '"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
            }
            }
            }


            var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png"];
            function ValidateSingleInput(oInput) {
            if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
            var sCurExtension = _validFileExtensions[j];
            if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
            blnValid = true;
            break;
            }
            }

            if (!blnValid) {
            alert("Sorry, File Format is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
            oInput.value = "";
            return false;
            }
            }
            }
            return true;
            }

$('#btnSubmit').click(function() {
    var exit = false;
    $('input.abcurl').each(function() {
        if ($(this).val().length == 0) {
            
             $.fallr.show({
            content: '<p>Enter URL</p>',
                    position: 'center'
            });
            exit = true;
            return false;
        }
         //regular expression for URL
            var pattern = /^(http|https)?:\/\/[a-zA-Z0-9-\.]+\.[a-z]{2,4}/;
            if (pattern.test($(this).val())){

            return true;
            } else {


            $.fallr.show({
            content: '<p>Url is not valid</p>',
                    position: 'center'
            });
            return false;
 
            }
    });

    if( exit ) {
        return false;
    }

    //alert("This alerts even when loop returns false");
});
//$('#btnSubmit').click(function() {
//    var exit = false;
//    $('input.usedatepicker').each(function() {
//        if ($(this).val().length == 0) {
//             $.fallr.show({
//            content: '<p>Enter Date</p>',
//                    position: 'center'
//            });
//            exit = true;
//            return false;
//        }
//         //regular expression for URL
//           
//    });
//
//    if( exit ) {
//        return false;
//    }
//
//    //alert("This alerts even when loop returns false");
//});
//$('#btnSubmit').click(function() {
//    var exit = false;
//    $('input.inputform').each(function() {
//        if ($(this).val().length == 0) {
//             $.fallr.show({
//            content: '<p>Enter Value</p>',
//                    position: 'center'
//            });
//            exit = true;
//            return false;
//        }
//         //regular expression for URL
//           
//    });
//
//    if( exit ) {
//        return false;
//    }
//
//    //alert("This alerts even when loop returns false");
//});
//$('body').on('focus', ".usedatepicker", function(){
//            $(this).datepicker(); });
//            $('.usedatepicker').each(function(){
//            $(this).datepicker();
//            });
        </script>


    </body>
</html>


