<?php

class Dashboard_m extends CI_Model {

    function __construct() {
        // Call the Model constructor
        parent::__construct();
       

        
        //$this->load->library('BRIFBusiness');
    }
    
    function add_data($datauser)
 {
  $this->db->insert('data',$datauser);
  return $this->db->insert_id();
 } 

    
     function getall_users() {
        $this->db->select('*');
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    
    
    
    function getall_groups() {
        $this->db->select('*');
        $query = $this->db->get('groups');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }

    function getall_groupsbyId($id) {
        $this->db->where('id', $id);
        $this->db->select('*');
        $query = $this->db->get('groups');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
function getall_occupation() {
        $this->db->select('*');
        $query = $this->db->get('occupation');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_occupationbyId($id) {
        $this->db->where('occupation_id', $id);
        $this->db->select('*');
        $query = $this->db->get('occupation');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_userbyId($id) {
        $this->db->where('user_id', $id);
        $this->db->select('*');
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }

    function getall_library() {
       
//        $this->db->where('access_type', 'public');
//        $this->db->where('status', 'active');
//        $this->db->where('move_status','');
//        $this->db->select('*');
//        $query = $this->db->get('library'); 
//        echo $this->db->last_query();
        $query = $this->db->query("SELECT * FROM `library` WHERE `access_type` = 'public' AND `status` = 'active' AND `move_status` IS NULL");
        //die;
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $key => $row) {
                $data[] = $row;
                 $row->group_name;
               
                 $this->db->where('id', $row->group_name);
                $this->db->select('*');
                $query2 = $this->db->get('groups')->result_array();
                foreach($query2 as $namedata){
                   $row->groupName = $namedata['name'];
                }
                
            }
           
            return $data;
        } else {

            return false;
        }
    }

    function get_libraryedit($id) {

        $this->db->where('library_id', $id);
        $this->db->select('*');
        $query = $this->db->get('library')->result_array();
        
        foreach ($query as $key => $row) {

            $data [] = $row;
            $this->db->where('library_id', $id);
            $this->db->where('row_id', '0');
            $this->db->select('*');
            $queryrow = $this->db->get('library_info')->result_array();
            
            foreach ($queryrow as $kyrr => $resultrow) {

                $resultrow['id'];
                $this->db->where('row_id', $resultrow['id']);
                $this->db->select('*');
                $qryrow = $this->db->get('library_info')->result_array();
                
               // echo $this->db->last_query();
                $queryrow[$kyrr]['ii'] = $qryrow;
              // echo '<pre>';print_r($queryrow[$kyrr]['ii']);
                $aa[]=$queryrow[$kyrr]['ii'];
                
            }
           
//die;
//            $this->db->where('library_id', $id);
//            $this->db->where('row_id !=', '0');
//            $this->db->group_by('row_col_name');
//            $this->db->select('*');
//            $querycol = $this->db->get('library_info')->result_array();
              $val = "SELECT * FROM `library_info` where library_id= '" . $id . "' and row_id !='0' GROUP BY `library_info`.`value`";
              $query = $this->db->query($val);
              $querycol= $query->result_array();
             
            $this->db->where('library_id', $id);
            $this->db->select('*');
            $query2 = $this->db->get('library_info')->result_array();

            $this->db->where('library_id', $id);
            $this->db->where('row_id !=', '0');
            $this->db->select('*');
            $querycolinfo = $this->db->get('library_info')->result_array();

            $data[$key]['colnum'] = $colnum;
            $data[$key]['colinfo'] = $aa[0];
//           echo '<pre>';
//            print_r($data[$key]['colinfo']);
//             die;
            
            $data[$key]['rowinfo'] = $queryrow;
            $data[$key]['info'] = $query2;
        }
        return $data;
    }

    function get_librarybyid($id) {
        $this->db->where('creator', $id);
        $this->db->select('*');
        $query = $this->db->get('library');

        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
                 $this->db->where('id', $row->group_name);
                $this->db->select('*');
                $query2 = $this->db->get('groups')->result_array();;
                foreach($query2 as $namedata){
                   $row->groupName = $namedata['name'];
                }
            }
            return $data;
        } else {

            return false;
        }
    }
     function getall_links() {
        $this->db->select('*');
        $query = $this->db->get('links');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }

    function getall_linksbyId($id) {
        $this->db->where('link_id', $id);
        $this->db->select('*');
        $query = $this->db->get('links');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_country() {
        $this->db->select('*');
        $query = $this->db->get('country');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_countrybyId($id) {
        $this->db->where('country_id', $id);
        $this->db->select('*');
        $query = $this->db->get('country');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
function getall_state() {
        $this->db->select('*');
        $query = $this->db->get('state');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_statebyId($id) {
        $this->db->where('state_id', $id);
        $this->db->select('*');
        $query = $this->db->get('state');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_linktype() {
        $this->db->select('*');
        $query = $this->db->get('link_type');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_linktypebyId($id) {
        $this->db->where('link_type_id', $id);
        $this->db->select('*');
        $query = $this->db->get('link_type');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_imagetype() {
        $this->db->select('*');
        $query = $this->db->get('image_type');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_imagetypebyId($id) {
        $this->db->where('image_type_id', $id);
        $this->db->select('*');
        $query = $this->db->get('image_type');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    
    function getall_template() {
        $this->db->select('*');
        $query = $this->db->get('emailtemplate');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }

    function getall_templatebyId($id) {
        $this->db->where('template_id', $id);
        $this->db->select('*');
        $query = $this->db->get('emailtemplate');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    
    function getall_columntype() {
        $this->db->select('*');
        $query = $this->db->get('column_type');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_columntypebyId($id) {
        $this->db->where('id', $id);
        $this->db->select('*');
        $query = $this->db->get('column_type');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_subtitute() {
        $this->db->select('*');
        $query = $this->db->get('subsitute');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
    function getall_subtitutebyId($id) {
        $this->db->where('id', $id);
        $this->db->select('*');
        $query = $this->db->get('subsitute');
        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {

            return false;
        }
    }
}
