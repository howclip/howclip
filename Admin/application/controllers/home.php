<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home extends CI_Controller {

    public function __construct() {



        parent::__construct();

        $this->load->helper('url');

        $this->load->helper('text');

        $this->load->database();

        $this->load->model('master_model');

        $this->load->library('session');
        ini_set('post_max_size', '1024M');
        ini_set('upload_max_filesize', '1024M');
        ini_set('max_execution_time', 6000);
        ini_set('max_input_time', 6000);
        ini_set('memory_limit', '128M');
    }

    public function index() {

        $this->welcome();
    }

    public function welcome() {


        $this->load->view('login');
    }

    public function login() {



        $this->form_validation->set_rules('email', 'email is', 'trim|required');

        $this->form_validation->set_rules('password', 'password is', 'trim|required');

        $pass = md5($this->input->post('password'));

        $data = array('email' => $this->input->post('email'),
            'password' => $pass
        );



        if ($this->form_validation->run() == FALSE) {



            $this->load->view('login');
        } else {

            $selectquery = $this->db->get_where('users', array('type' => 'company','email' => $this->input->post('email'), 'password' => $pass
            ));
           
            if ($selectquery->num_rows() > 0) {

                $row = $selectquery->result_array();

                $userrecord = array(
                    'name' => $row[0]['name'],
                    'email' => $row[0]['email'],
                    'id' => $row[0]['id'],
                    'role' => $row[0]['role'],
                );

                $this->session->set_userdata($userrecord);

                redirect('admin/dashboard', 'refresh');
            } else {

                $this->session->set_flashdata('err', "Invalid login");
                redirect('home/welcome', 'refresh');
            }
        }
    }

    public function logout() {

        $this->session->unset_userdata();

        $this->session->sess_destroy();

        redirect('home/welcome', 'refresh');

        exit;
    }

}

?>