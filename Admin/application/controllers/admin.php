<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class admin extends CI_Controller {

    public function __construct() {

        parent::__construct();
        $this->load->library('session');

        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->database();
        $this->load->model('master_model');
        $this->load->library('session');
        $this->load->library('Excel');
        $valid_login = $this->session->userdata('id');
        if ($valid_login == '') {
            redirect('home/welcome');
        }
    }

    public function dashboard() {
        $this->load->view('admin/header');
        $this->load->view('admin/dashboard');
    }

    public function test() {

        $this->load->view('admin/test');
    }

    public function demo() {

        if ($_FILES['image1']['name']) {



            $errors = array();
            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image1']['name'];
            $file_size = $_FILES['image1']['size'];
            $file_tmp = $_FILES['image1']['tmp_name'];
            $file_type = $_FILES['image1']['type'];

            $file_ext = strtolower(end(explode('.', $_FILES['image1']['name'])));
            $expensions = array("jpeg", "jpg", "png");
            if (in_array($file_ext, $expensions) === false) {

                $this->session->set_flashdata('permission_message', 'extension not allowed, please choose a JPEG or PNG file.');
                echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            }
            $file_name . '<br>';
            $file_tmp . '<br>';
            if (move_uploaded_file($file_tmp, "uploads/" . $file_name)) {
                echo "yes";
            } else {
                echo "no";
            }

            $data['image1'] = $file_name;
        }
    }

    public function popup($param1 = '', $param2 = '', $param3 = '') {

        $page_data['param1'] = $param1;
        $page_data['param2'] = $param2;
        $page_data['param3'] = $param3;
        $this->load->view('admin/' . $param1, $page_data);
    }

    public function client_curd($param1 = '', $param2 = '', $param3 = '') {

        $this->dashboard();
    }

    public function password() {
        $this->load->view('admin/header');
        $this->load->view('admin/password');
    }

    public function changepassword() {

        $user_id = $this->input->post('loginid');
        $curr = md5($this->input->post('password'));

        $a = $data1['users'] = $this->db->get_where('users', array('id' => $user_id))->result_array();

        foreach ($a as $value) {
            $pass = $value['password'];
        }

        if ($curr == $pass) {
            $data_pass['npassword'] = md5($this->input->post('new_pass'));
            $data_pass['cpassword'] = md5($this->input->post('conf_pass'));
            if ($data_pass['npassword'] == $data_pass['cpassword']) {
                $data2['password'] = md5($this->input->post('new_pass'));

                $this->db->where('id', $user_id);
                $this->db->update('users', $data2);
                $this->session->set_flashdata('flash_message', ' Password Update successully');
                redirect('admin/dashboard', 'refresh');
            } else {
                $this->session->set_flashdata('err', 'confirm Password did not match');
                redirect('admin/password', 'refresh');
            }
        } else {
            $this->session->set_flashdata('err', 'Current Password did not match');
            redirect('admin/password', 'refresh');
        }
    }

    public function add($param1 = '', $param2 = '', $param3 = '') {

        $this->load->view('admin/header');
        $this->load->view('admin/add');
        $this->load->view('modal');
    }

    public function ben_curd($param1 = '', $param2 = '', $param3 = '') {

        if ($param1 == 'create') {
            $row = $this->db->get_where('master_bended', array('category' => $this->input->post('cat'), 'type' => $this->input->post('type')));
            if ($row->num_rows > 0) {
                $this->session->set_flashdata('permission_message', 'email_id_already_exist_!!!_enter_another_....');
            } else {
                $data['category'] = $this->input->post('cat');
                $data['type'] = $this->input->post('type');

                $this->db->insert('master_bended', $data);

                $this->session->set_flashdata('flash_message', 'added_successfully');
            }

            redirect('admin/add', 'refresh');
        }

        if ($param1 == 'do_update') {

            $data2['category'] = $this->input->post('cat');
            $data2['type'] = $this->input->post('type');
            $this->db->where('id', $param2);
            $this->db->update('master_bended', $data2);

            $this->session->set_flashdata('flash_message', 'Updated_successfully');
            redirect('admin/add', 'refresh');
        }
        if ($param1 == 'delete_ben') {
            $this->db->where('id', $param2);
            $this->db->delete('master_bended');
            $this->session->set_flashdata('flash_message', 'deleted_successully');
            redirect('admin/add', 'refresh');
        }
    }

    public function category() {
        if (isset($_GET['page'])) {
            $start = $_GET['page'] * 10;
        } else {
            $start = 0;
        }
        $data['categories'] = $this->master_model->getcats($start);
        $data['page_title'] = "CATEGORY";
        $this->load->view('admin/header');
        $this->load->view('admin/show_category', $data);
    }

    public function feature() {
        $data['features'] = $this->master_model->getvideoFeatures();
        $data['page_title'] = "VIDE0 FEATURES";
        $this->load->view('admin/header');
        $this->load->view('admin/show_feature', $data);
    }

    public function add_feature() {

        $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');
        $this->form_validation->set_rules('name', $this->lang->line("Feature Name"), 'required|xss_clean');


        $name = $this->input->post('name');
        $status = $this->input->post('status');

        $feature = array(
            'name' => $name,
            'status' => $status,
        );

        if ($this->form_validation->run() == true && $this->master_model->add_feature($feature)) {
            $this->session->set_flashdata('msg', 'Inserted Successfully');
            redirect('admin/feature', refresh);
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));



            $meta['page_title'] = "ADD FEATURE";
            $data['page_title'] = "ADD FEATURE";
            $this->load->view('admin/header', $meta);
            $this->load->view('admin/add_feature', $data);
        }
    }

    public function update_feature($id = NULL) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');
        $this->form_validation->set_rules('name', $this->lang->line("Feature Name"), 'required|xss_clean');


        $name = $this->input->post('name');
        $status = $this->input->post('status');

        $feature = array(
            'name' => $name,
            'status' => $status,
        );

        if ($this->form_validation->run() == true && $this->master_model->update_feature($feature, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            redirect('admin/feature', refresh);
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

            $data['feature'] = $this->master_model->getvideoFeaturesById($id);

            $meta['page_title'] = "EDIT FEATURE";
            $data['page_title'] = "Edit FEATURE";
            $this->load->view('admin/header', $meta);
            $this->load->view('admin/update_feature', $data);
        }
    }

    public function delete_feature($id = NULL) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->master_model->delete_feature($id)) {
            $this->session->set_flashdata('msg', 'Daleted Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function add_category() {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');
        $this->form_validation->set_rules('cat_name', $this->lang->line("Category Name"), 'required|xss_clean');
        $this->form_validation->set_rules('sub_cat', $this->lang->line("Sub Category Name"), 'required|xss_clean');

        $cat = $this->input->post('cat_name');
        $sub_cat = $this->input->post('sub_cat');
        $status = $this->input->post('status');
        
        $q=$this->db->get_where('category',array('category_title'=>$sub_cat,'category_parent'=>$cat))->result_array();
        
        
        $category = array(
            'category_title' => $sub_cat,
            'category_parent' => $cat,
            'status' => $status,
        );

        if ($this->form_validation->run() == true && $this->master_model->add_category($category)) {
            if ($q) {
                $this->session->set_flashdata('msg', 'This category already exist.');
              // echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            }else{
            $this->session->set_flashdata('msg', 'Inserted Successfully');}
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
           } else {
 
            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));


            $data['categories'] = $this->master_model->show_categories();
            $data['categoryList'] = $this->master_model->fetchCategoryTree();
            $meta['page_title'] = "ADD CATEGORY";
            $data['page_title'] = "ADD CATEGORY";
            $this->load->view('admin/header', $meta);
            $this->load->view('admin/add_category', $data);
        }
    }

    public function update_cat() {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');

        $this->form_validation->set_rules('sub_cat', $this->lang->line("Sub Category Name"), 'required|xss_clean');

        $cat = $this->input->post('cat_name');
        $sub_cat = $this->input->post('sub_cat');
        $status = $this->input->post('status');


        $category = array(
            'category_title' => $sub_cat,
            'status' => $status,
        );

        if ($this->form_validation->run() == true && $this->master_model->update_category($category, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));



            $data['all_categories'] = $this->master_model->show_categoriesById($id);

            $meta['page_title'] = "UPDATE CATEGORY";
            $data['page_title'] = "UPDATE CATEGORY";
            $this->load->view('admin/header', $meta);
            $this->load->view('admin/update_category', $data);
        }
    }

    public function delete_cat($id = NULL) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->master_model->delete_cat($id)) {
            $this->session->set_flashdata('msg', 'Deleted Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function setting() {
        $data['setting_data'] = $this->master_model->getadmindata();

        $data['page_title'] = "SETTINGS";
        $data['id'] = $id;
        $data['page_title'] = "SETTINGS";
        $this->load->view('admin/header', $data);
        $this->load->view('admin/settings', $data);
    }

    public function update_setting() {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');

        $this->form_validation->set_rules('name', $this->lang->line("Name"), 'required|xss_clean');
        $this->form_validation->set_rules('email', $this->lang->line("email"), 'required|xss_clean');

        $this->form_validation->set_rules('company_name', $this->lang->line("Company name"), 'required|xss_clean');

        $this->form_validation->set_rules('gender', $this->lang->line("gender"), 'required|xss_clean');
        $this->form_validation->set_rules('dob', $this->lang->line("Date Of Birth"), 'required|xss_clean');
        $this->form_validation->set_rules('facebookId', $this->lang->line("Facebook Id"), 'required|xss_clean');
        $this->form_validation->set_rules('googleId', $this->lang->line("Gmail Id"), 'required|xss_clean');

        if ($_FILES['image1']['name']) {


            $errors = array();
            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image1']['name'];
            $file_size = $_FILES['image1']['size'];
            $file_tmp = $_FILES['image1']['tmp_name'];
            $file_type = $_FILES['image1']['type'];

            $file_ext = strtolower(end(explode('.', $_FILES['image1']['name'])));
            $expensions = array("jpeg", "jpg", "png");
            if (in_array($file_ext, $expensions) === false) {

                $this->session->set_flashdata('permission_message', 'extension not allowed, please choose a JPEG or PNG file.');
                echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            }

            move_uploaded_file($file_tmp, "uploads/" . $file_name);

            $data['image1'] = $file_name;
        }

        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $company = $this->input->post('company_name');
        $facebook = $this->input->post('facebookId');
        $gmail = $this->input->post('googleId');
        $steam = $this->input->post('steamId');
        $gender = $this->input->post('gender');
        $dob = $this->input->post('dob');
        $date = date("Y-m-d", strtotime($dob));
        $user_data = array(
            'username' => $name,
            'email' => $email,
            'companyName' => $company,
            'facebookId' => $facebook,
            'googleId' => $gmail,
            'gender' => $gender,
            'dob' => $date,
            'steamId' => $steam,
            'type' => 'company'
        );
        if ($data['image1'] != '') {
            $img = $data['image1'];
            $user_data['company_logo'] = $img;
        }
        //print_r($set_data);die;		
        if ($this->master_model->Update_setting($user_data)) {
            $this->session->set_flashdata('msg', 'Saved Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
        }
    }

    public function show_video() {
        // print phpinfo();
        //max upload size
        //echo 'post_max_size = ' . ini_get('post_max_size') . "\n";

        if (isset($_GET['page'])) {
            $start = $_GET['page'] * 10;
        } else {
            $start = 0;
        }
        $data['videos'] = $this->master_model->Allvideo($start);
        $data['page_title'] = "VIDEOS";
        $this->load->view('admin/header');
        $this->load->view('admin/show_video', $data);
    }

    public function add_video() {
        $data['features'] = $this->master_model->getvideoFeatures();
        $data['categories'] = $this->master_model->show_categories();
        $data['page_title'] = "ADD VIDEO";
        $this->load->view('admin/header', $data);
        $this->load->view('admin/add_video', $data);
    }

    public function sizeinput($input, $len) {
        (int) $len;
        (string) $input;
        $n = substr($input, 0, $len);
        $ret = trim($n);
        $out = htmlentities($ret, ENT_QUOTES);
        return $out;
    }

    public function upload() {

        $this->form_validation->set_rules('category', $this->lang->line("category"), 'required|xss_clean');
        $this->form_validation->set_rules('duration', $this->lang->line("duration"), 'required|xss_clean');
        $this->form_validation->set_rules('name', $this->lang->line("name"), 'required|xss_clean');
        $this->form_validation->set_rules('description', $this->lang->line("description"), 'required|xss_clean');
        $this->form_validation->set_rules('feature', $this->lang->line("feature"), 'required|xss_clean');
        //max upload size
        //echo 'post_max_size = ' . ini_get('post_max_size') . "\n";
        if ($this->form_validation->run() == true && $_POST && array_key_exists("file1", $_FILES)) {
            $uploaddir = 'uploads/videos/';
            $live_dir = 'uploads/live/';
            $live_img = 'uploads/images/';
            $clipdir = 'uploads/clips/';
            $interval = 5;
            $size = '320x240';
            $seed = rand(1, 2009) * rand(1, 10);
            $upload = $seed . basename($_FILES['file1']['name']);
            echo $upload;
            $uploadfile = $uploaddir . $upload;


            $vid_usr_ip = $_SERVER['REMOTE_ADDR'];
            $safe_file = $this->checkfile($_FILES['file1']);
            if ($safe_file['safe'] == 1) {
                if (move_uploaded_file($_FILES['file1']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.<br/>";
                    $base = basename($uploadfile, $safe_file['ext']);
                    $new_file = $base . 'mp4';
                    $new_image = $base . 'jpg';
                    $new_image_path = $live_img . $new_image;
                    $new_flv = $live_dir . $new_file;
                    $new_clip = $clipdir . $new_file;
                    //exec("ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default -an -threads 0 $new_flv");
                    exec("ffmpeg -i $uploadfile -c:v libx264 $new_flv");
                    exec("ffmpeg -ss 00:00:05 -i $uploadfile scale=320:176 $new_image_path");

                    /* 	working on server exec("ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default -an -threads 0 $new_flv");
                      exec("ffmpeg -i $uploadfile $new_image_path"); */

                    //for image on server working
                    //exec("ffmpeg -ss 00:00:05 -i $uploadfile $new_image_path");
                    //for video local
                    //exec('ffmpeg -i '.$uploadfile.' '.$new_flv.'');
                    //exec('ffmpeg -ss 00:01:00 -i '.$uploadfile.' -to 00:04:00 '.$new_clip.'');
                    //two pass method
                    // exec(" ffmpeg -i $uploadfile -vcodec libx264 -vpre medium_firstpass -b 416k -threads 0 -pass 1 -an -f mp4 -y /dev/null && ffmpeg -i $uploadfile -vcodec libx264 -vpre medium -b 416k -threads 0 -pass 2 -acodec libfaac -ab 128k $new_flv ");

                    $cat = $this->input->post('category');
                    $dur = $this->input->post('duration');
                    $name = $this->input->post('name');
                    $description = $this->input->post('description');
                    $feature = $this->input->post('feature');



                    $date = date("Y-m-d");
                    $video_data = array(
                        'name' => $new_file,
                        'short_description' => $name,
                        'description' => $description,
                        'date' => $date,
                        'video_category' => $cat,
                        'video_duration' => $dur,
                        'video_img' => $new_image,
                        'videoFeature' => $feature
                    );

                    if ($this->master_model->add_video($video_data)) {
                        $this->session->set_flashdata('msg', 'Saved Successfully');
                        echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
                    }
                } else {

                    echo "Possible file upload attack!\n";
                    // print_r($_FILES);
                }
            } else {

                echo 'Invalid File Type Please Try Again. You file must be of type 
 			 .mpg, .wma, .mov, .flv, .mp4, .avi, .qt, .wmv, .rm';
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function checkfile($input) {

        $ext = array('mp4', 'avi', 'wmv', 'mpg');
        $extfile = substr($input['name'], -4);
        $extfile = explode('.', $extfile);
        $good = array();
        $extfile = $extfile[1];
        if (in_array($extfile, $ext)) {
            $good['safe'] = true;
            $good['ext'] = $extfile;
        } else {
            $good['safe'] = false;
        }
        return $good;
    }

    public function update_videbyId($id) {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($_POST && array_key_exists("file1", $_FILES)) {
            $name = $this->input->post('name');


            $uploaddir = 'uploads/videos/';

            $live_dir = 'uploads/live/';

            $live_img = 'uploads/images/';
            $clipdir = 'uploads/clips/';
            $seed = rand(1, 2009) * rand(1, 10);
            $upload = $seed . basename($_FILES['file1']['name']);
            $uploadfile = $uploaddir . $upload;


            $vid_usr_ip = $_SERVER['REMOTE_ADDR'];
            $safe_file = $this->checkfile($_FILES['file1']);
            if ($safe_file['safe'] == 1) {
                if (move_uploaded_file($_FILES['file1']['tmp_name'], $uploadfile)) {
                    echo "File is valid, and was successfully uploaded.<br/>";
                    $base = basename($uploadfile, $safe_file['ext']);
                    $new_file = $base . 'mp4';
                    $new_image = $base . 'jpg';
                    $new_image_path = $live_img . $new_image;
                    $new_flv = $live_dir . $new_file;
                    exec("ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default -an -threads 0 $new_flv");
                    exec("ffmpeg -ss 00:00:05 -i $uploadfile $new_image_path");


                    /* working on server exec("ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default -an -threads 0 $new_flv");
                      exec("ffmpeg -i $uploadfile $new_image_path"); */
                    //exec("ffmpeg -i $uploadfile $new_image_path");
                    //for video server working
                    //exec("ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default -an -threads 0 $new_flv");
                    //for video local
                    // exec('ffmpeg -i '.$uploadfile.' '.$new_flv.'');
                    $video_data = array(
                        'name' => $new_file,
                        'video_img' => $new_image,
                    );
                } else {

                    echo 'Invalid File Type Please Try Again. You file must be of type 
 			 .mpg, .wma, .mov, .flv, .mp4, .avi, .qt, .wmv, .rm';
                }
            }
        }

        $cat = $this->input->post('category');
        $dur = $this->input->post('duration');

        $description = $this->input->post('description');
        $feature = $this->input->post('feature');
        $name = $this->input->post('name');
        $date = date("Y-m-d");
        $video_data['videoname'] = $name;
        $video_data['description'] = $description;
        $video_data['date'] = $date;
        $video_data['video_category'] = $cat;
        $video_data['video_duration'] = $dur;
        $video_data['videoFeature'] = $feature;
        /* $video_data = array(
          // 'name'    				=> $new_file ,
          'short_description' 	=> $name ,
          'description'  		=> $description,
          'date'         		=> $date,
          'video_category'       => $cat,
          'video_duration'       => $dur,
          // 'video_img'            => $new_image,
          'videoFeature'         => $feature

          ); */


        if ($this->master_model->update_video($video_data, $id)) {
            $this->session->set_flashdata('msg', 'updated Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function update_video() {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $data['features'] = $this->master_model->getvideoFeatures();
        $data['video'] = $this->master_model->getvideoById($id);
        $data['garima'] = 'garima';
        $data['categories'] = $this->master_model->show_categories();
        $data['categoryList'] = $this->master_model->fetchCategoryTree();
        $data['page_title'] = "VIDEOS";
        $this->load->view('admin/header');
        $this->load->view('admin/update_video', $data);
    }

    public function delete_video($id = NULL) {
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
            $name = $this->input->post('name');
            $image = $this->input->post('image');

            $this->master_model->del_video($id, $image);
            $path = ('uploads/live/' . $name);
            $img = ('uploads/images/' . $image);
            unlink($path);
            unlink($img);
            $this->session->set_flashdata('msg', 'Deleted Successfully');

            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
        if ($this->input->get('id')) {
            $id = $this->input->get('id');

            $this->master_model->delete_video($id);

            $this->session->set_flashdata('msg', 'Deleted Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function about() {
        $data['aboutContent'] = $this->master_model->getAbout();
        $data['page_title'] = "About Us";
        $this->load->view('admin/header');
        $this->load->view('admin/about', $data);
    }

    public function Insertabout() {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }


        $content1 = $this->input->post('content1');
        $content2 = $this->input->post('content2');
        $content3 = $this->input->post('content3');
        $content4 = $this->input->post('content4');
        $content5 = $this->input->post('content5');
        $content6 = $this->input->post('content6');
        $content7 = $this->input->post('content7');
        $contentdata = array(
            'aboutcontent' => $content1,
            'Copyrightcontent' => $content4,
            'gettingstartcontent' => $content2,
            'contactcontent' => $content3,
        );


        if ($this->master_model->insertabout($contentdata, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function terms() {
        $data['TermsContent'] = $this->master_model->getterms();
        $data['page_title'] = "Terms & Conditions";
        $this->load->view('admin/header');
        $this->load->view('admin/terms', $data);
    }

    public function InsertTerms() {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $content = $this->input->post('content');

        $data = array('terms' => $content);

        if ($this->master_model->insertTerms($data, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function gettingstart() {

        $data['getting'] = $this->master_model->getgattingstart();
        $data['page_title'] = "Getting Started";
        $this->load->view('admin/header');
        $this->load->view('admin/gettingstart', $data);
    }

    public function InsertGettingstart() {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $content = $this->input->post('content');

        $data = array('content' => $content);

        if ($this->master_model->gettingstart($data, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function Contact() {
        $data['contact'] = $this->master_model->getcontactus();
        $data['page_title'] = "Contact Us";
        $this->load->view('admin/header');
        $this->load->view('admin/contactUs', $data);
    }

    public function Insertcontact() {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $content = $this->input->post('content');

        $data = array('content' => $content);

        if ($this->master_model->insertcontact($data, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function import_category() {

        $data['page_title'] = "Contact Us";
        $this->load->view('admin/header');
        $this->load->view('admin/import_cat', $data);
    }

    public function import_Categorydata() {

        if ($_FILES['file']['name']) {

            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $_FILES['file']['name'];
            $file_size = $_FILES['file']['size'];
            $file_tmp = $_FILES['file']['tmp_name'];
            $file_type = $_FILES['file']['type'];
            $file_ext = strtolower(end(explode('.', $_FILES['file']['name'])));
            $expensions = array("xls", "xlsx", "csv");
            if (in_array($file_ext, $expensions) === false) {             //$errors[]="extension not allowed, please choose a JPEG or PNG file.";      
                $this->session->set_flashdata('per_message', 'File Format Not Suppoted');
                redirect('Dashboard/dashboard', 'refresh');
            } else {
                move_uploaded_file($file_tmp, $file_name);
                $data['file'] = $file_name;
            }
        }
        $this->demodata($file_name);
    }

    public function demodata($par1) {
        $inputfilename = $par1;
        $file_ext = strtolower(end(explode('.', $inputfilename)));
        if (($file_ext == 'xls') || ($file_ext == 'xlsx')) {
            $inputfiletype = PHPExcel_IOFactory::identify($inputfilename);
            $objReader = PHPExcel_IOFactory::createReader($inputfiletype);
            $objPHPExcel = $objReader->load($inputfilename);

            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            for ($r1 = 2; $r1 <= $highestRow; $r1++) {
                $rowData = $sheet->rangeToArray('A' . $r1 . ':' . $highestColumn . $r1, NULL, TRUE, FALSE);
       
                if (($rowData[0][0] != '')) {
                    $cat1 = $rowData[0][0];
                    //  $catqry1 = $this->db->get_where('category', array('category_title' => $cat1, 'category_parent' => 0))->row_array();
                    $catqry = $this->db->get_where('category', array('category_title' => $cat1, 'category_parent' => 0));
                    // if(count($catqry1)>0){
                    //  $rrr=1;
                    // }else{
                    if ($catqry->num_rows() > 0) {
                       
                        $result1 = $catqry->row_array();
                        
                        if ($result1['id'] > 0) {
                            $first = $rowData[0][1];
                            $second = $rowData[0][2];
                            $third = $rowData[0][3];
                            $fourth = $rowData[0][4];
                            $catqry2 = $this->db->get_where('category', array('category_title' => $first, 'category_parent' => $result1['id']));
                            if ($catqry2->num_rows() > 0) {
                                $result2 = $catqry2->row_array();
                                if ($result2['id'] > 0) {
                                    if ($second != null) { 
                                        $catqry3 = $this->db->get_where('category', array('category_title' => $second, 'category_parent' => $result2['id']));
                                        if ($catqry3->num_rows() > 0) { 
                                            $result3 = $catqry3->row_array();
                                            if ($result3['id'] > 0 && $third != null) { 
                                                $catqry4 = $this->db->get_where('category', array('category_title' => $third, 'category_parent' => $result3['id']));
                                               
                                                if ($catqry4->num_rows() > 0) { 
                                                    $result4 = $catqry4->row_array();
                                                    if ($result4['id'] > 0) {
                                                        if ($fourth != null && $result4['id'] != '') {
                                                            $dataarr = array('category_title' => $fourth,
                                                                'category_parent' => $result4['id'],'status'=>'active');
                                                            $this->db->insert('category', $dataarr);
                                                        }
                                                    }
                                                } else { 
                                                    if ($third != '' && $result3['id'] != '') { 
                                                        $abc1 = array('category_title' => $third,
                                                            'category_parent' => $result3['id'],'status'=>'active');
                                                        if ($this->db->insert('category', $abc1)) {
                                                            $third1 = $this->db->insert_id();
                                                            if($rowData[0][4] != '' && $third1 > 0 ){
                                                            $abc2 = array('category_title' => $rowData[0][4],
                                                                'category_parent' => $third1,'status'=>'active');
                                                            $this->db->insert('category', $abc2);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if ($second != '' && $result2['id'] != '') { 
                                                $ddg1 = array('category_title' => $second,
                                                    'category_parent' => $result2['id'],'status'=>'active');
                                                if ($this->db->insert('category', $ddg1)) {
                                                    $sec1 = $this->db->insert_id();
                                                    if($rowData[0][3] != '' && $sec1 > 0){
                                                    $ddg2 = array('category_title' => $rowData[0][3],
                                                        'category_parent' => $sec1,'status'=>'active');
                                                    if ($this->db->insert('category', $ddg2)) {
                                                        $sec2 = $this->db->insert_id();
                                                        if($sec2 > 0 && $rowData[0][4]!= ''){
                                                        $ddg3 = array('category_title' => $rowData[0][4],
                                                            'category_parent' => $sec2,'status'=>'active');
                                                        $this->db->insert('category', $ddg3);
                                                        }
                                                    }
                                                }
                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                if ($first != '' && $result1['id'] != '') {
                                    $ar = array('category_title' => $first,
                                        'category_parent' => $result1['id'],'status'=>'active');
                                    if ($this->db->insert('category', $ar)) {
                                        $i1 = $this->db->insert_id();
                                        if ($i1 != '' && $rowData[0][2] != '') {
                                            $ar1 = array('category_title' => $rowData[0][2],
                                                'category_parent' => $i1,'status'=>'active');
                                            if ($this->db->insert('category', $ar1)) {
                                                $i2 = $this->db->insert_id();
                                                if ($i2 != '' && $rowData[0][3] != '') {
                                                    $ar2 = array('category_title' => $rowData[0][3],
                                                        'category_parent' => $i2,'status'=>'active');
                                                    if ($this->db->insert('category', $ar2)) {
                                                        $i3 = $this->db->insert_id();
                                                        if ($i3 != '' && $rowData[0][4] != '') {
                                                            $ar3 = array('category_title' => $rowData[0][4],
                                                                'category_parent' => $i3,'status'=>'active');
                                                            $this->db->insert('category', $ar3);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        if ($rowData[0][0] != '') {
                            $insertarr = array('category_title' => $rowData[0][0],
                                'category_parent' => 0,'status'=>'active'
                            );
                            if ($this->db->insert('category', $insertarr)) {
                                $ins1 = $this->db->insert_id();
                                if ($ins1 != '' && $rowData[0][1] != '') {
                                    $child1 = $rowData[0][1];
                                    $arr2 = array('category_title' => $child1,
                                        'category_parent' => $ins1,'status'=>'active');
                                    if ($this->db->insert('category', $arr2)) {
                                        $ins2 = $this->db->insert_id();

                                        if ($ins2 != '' && $rowData[0][2] != '') {
                                            $child2 = $rowData[0][2];
                                            $arr3 = array('category_title' => $child2,
                                                'category_parent' => $ins2,'status'=>'active');
                                            if ($this->db->insert('category', $arr3)) {
                                                $ins3 = $this->db->insert_id();
                                                if ($ins3 != '' && $rowData[0][3] != '') {
                                                    $child3 = $rowData[0][3];
                                                    $arr4 = array('category_title' => $child3,
                                                        'category_parent' => $ins3,'status'=>'active');
                                                    if ($this->db->insert('category', $arr4)) {
                                                        $ins4 = $this->db->insert_id();
                                                        if ($ins4 != '' && $rowData[0][4] != '') {
                                                            $child4 = $rowData[0][4];
                                                            $arr5 = array('category_title' => $child4,
                                                                'category_parent' => $ins4,'status'=>'active');
                                                            $this->db->insert('category', $arr5);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
               // }
            } }
            //if($rrr==1){
//              $this->session->set_flashdata('permission_message', 'Duplicate record not inserted');  
           // }else{
            $this->session->set_flashdata('msg', 'Data Saved Successfully');
           // }
           // echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
           redirect('admin/category','refresh');
            
            
            
            
        }
    }

}

?>