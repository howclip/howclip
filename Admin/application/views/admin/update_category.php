<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
          <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
                <!-- STYLER -->
                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                  <li>  <a href="#"></a> </li>
                  <!--<li>
											<a href="#">Other Pages</a>
										</li>
										<li>Blank Page</li>-->
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                  <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>
                </div>
               
              </div>
            </div>
          </div>
		   <?php if($this->session->flashdata('msg'))
	 		{
			?>
						<div class="separator"></div>
						<div class="alert alert-block alert-success fade in">
											<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
												<p><h4> Successful!</h4> <?php echo $this->session->flashdata('msg'); ?></p>
										</div>
										
									
			<?php } ?>		
										<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>
												<div class="tools hidden-xs">
													
												</div>
											</div>
								
											<div class="box-body big">
											   <?php foreach($all_categories as $searchcat) { $id = $searchcat->id; ?>
											<?php echo form_open('admin/update_cat?id='.$id, array('id' => 'usersForm', 'class' => 'form-horizontal'));?>
          <div class="panel panel-default">

            <div class="panel-body">

              <div class="tabbable">
			  <?php if($searchcat->sub['child']['child2']['child3']['category_title']!= "") { ?>
			  <div class="form-group">
             
                  <label class="control-label col-md-6" style="text-align:right;">Parent Category3 Name: <span class="required">*</span></label>
                  <div class="col-md-6">
             <input type="text" class="form-control" name="cat_name" readonly required="required" value="<?php echo ucwords($searchcat->sub['child']['child2']['child3']['category_title']);?>"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>
			  <?php } ?>
			   <?php if($searchcat->sub['child']['child2']['category_title']!= "") { ?>
			  <div class="form-group">
             
                  <label class="control-label col-md-6" style="text-align:right;">Parent Category3 Name: <span class="required">*</span></label>
                  <div class="col-md-6">
             <input type="text" class="form-control" name="cat_name" readonly required="required" value="<?php echo ucwords($searchcat->sub['child']['child2']['category_title']);?>"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>
			  <?php }?>
			   <?php if($searchcat->sub['child']['category_title']!= "") { ?>
			  <div class="form-group">
             
                  <label class="control-label col-md-6" style="text-align:right;">Parent Category2 Name: <span class="required">*</span></label>
                  <div class="col-md-6">
             <input type="text" class="form-control" name="cat_name" readonly required="required" value="<?php echo ucwords($searchcat->sub['child']['category_title']);?>"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>
    	<?php } ?>
		 <?php if($searchcat->sub['category_title']!= "") { ?>
                <div class="form-group">
             
                  <label class="control-label col-md-6" style="text-align:right;">Parent Category1 Name: <span class="required">*</span></label>
                  <div class="col-md-6">
             <input type="text" class="form-control" name="cat_name" readonly required="required" value="<?php echo ucwords($searchcat->sub['category_title']);?>"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div> 
               <?php } ?>
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Sub Category / Category Name:<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="sub_cat" value="<?php echo ucwords($searchcat->category_title);?>" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
               
                <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Status:<span class="required">*</span></label>
                  <div class="col-md-6" >
				  <select name="status" class="form-control" required="required" style="width:200px;">
				  <option value="active" <?php if($searchcat->status == 'active'){ echo "selected"; }?>> Active</option>
				  <option value="inactive" <?php if($searchcat->status == 'inactive'){ echo "selected"; }?>>Inactive</option>
				  <option value="pending" <?php if($searchcat->status == 'pending'){ echo "selected"; }?>> Pending</option>
                 </select>
                 <span class="error-span"></span>
                 </div>
               </div>  
               <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >

                                           

                                         <button type="submit" class="btn btn-primary"> <?php echo 'Update Category';?> <i class="fa fa-save"></i></button>

                                        </div>

                                     </div>

                                  </div>

                               </div>
               
               
                
              </div>
              <?php echo form_close();?>
					<?php } ?>						
											
							
											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
          </section>
        </div>
      </div>
    </div>
  </div>
</section>
