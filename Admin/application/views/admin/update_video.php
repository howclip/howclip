<style>

.video {

    position: relative;

	

    ...

}

.video .close {

    position: absolute;

    top: 2px;

    right: 2px;

	margin-left:20px;

    z-index: 100;

    ...

}

</style>

<script>

/* Script written by Adam Khoury @ DevelopPHP.com */

/* Video Tutorial: http://www.youtube.com/watch?v=EraNFJiY0Eg */

function _(el){

	return document.getElementById(el);

}

function uploadFile(){

  document.getElementById('progressBar').style.display = "block";

 var name = document.getElementById('name').value;

  var description = document.getElementById('description').value;

   var video_id = document.getElementById('video_id').value;

    var category = document.getElementById('category').value;

    var duration = document.getElementById('duration').value;

	 var feature = document.getElementById('feature').value;

   

    

	var file = _("file1").files[0];

	// alert(file.name+" | "+file.size+" | "+file.type);

	var formdata = new FormData();

	formdata.append("file1", file);

	formdata.append("name", name);

	formdata.append("description", description);

	formdata.append("category", category);

	formdata.append("duration", duration);

	formdata.append("feature", feature);

	var ajax = new XMLHttpRequest();

	ajax.upload.addEventListener("progress", progressHandler, false);

	ajax.addEventListener("load", completeHandler, false);

	ajax.addEventListener("error", errorHandler, false);

	ajax.addEventListener("abort", abortHandler, false);

	ajax.open("POST", 'update_videbyId?id='+video_id);

	ajax.send(formdata);

}

function progressHandler(event){

	_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;

	var percent = (event.loaded / event.total) * 100;

	_("progressBar").value = Math.round(percent);

	_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";

}

function completeHandler(event){

alert('Saved Successfully');

//console.log(response);

 document.getElementById('progressBar').style.display = "none";

	_("status").innerHTML = event.target.responseText;

	_("progressBar").value = 0;

	_("msg").innerHTML = "Uploaded Successfully";

	 document.getElementById('progressBar').style.display = "none";

	 

	

}

function errorHandler(event){

	_("status").innerHTML = "Upload Failed";

}

function abortHandler(event){

	_("status").innerHTML = "Upload Aborted";

}

</script>

<script>

function del_video() {

var pp = $('#video_id').val();

var video = $('#video_img').val();

var img = $('#video').val();



//alert(pp);

     $.ajax({

       url:'delete_video',

       type: 'POST',

       data: {'id': pp,'name': video ,'image': img},

       success: function(response){

	  window.location.href = 'update_video?id='+pp ;

       //alert(response);

		  //console.log(response);

       },

       

   });

}

</script>



<div id="main-content">

    <div class="container">

      <div class="row">

        <div id="content" class="col-lg-12">

          <!-- PAGE HEADER-->

          <div class="row">

            <div class="col-sm-12">

         <div class="page-header">

                <!-- STYLER -->

                <!-- /STYLER -->

                <!-- BREADCRUMBS -->

                <ul class="breadcrumb">

                  <li>  <a href="#"></a> </li>

                  <!--<li>

											<a href="#">Other Pages</a>

										</li>

										<li>Blank Page</li>-->

                </ul>

                <!-- /BREADCRUMBS -->

                <div class="clearfix">

                  <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>

                </div>

               

              </div>

            </div>

          </div>

		   	

					
									

													<!-- BASIC -->

										<div class="box border blue">

											<div class="box-title">

												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>

												<div class="tools hidden-xs">

													

												</div>

											</div>

											<div class="box-body big">

											<?php  foreach($video as $vid) { ?> 

											<input type="hidden" id="video_id" name="video_id" value="<?php echo $vid->id; ?>">

											

											

										<div class="row">

										

											<div class="col-md-10">

											<form id="upload_form" class="form-horizontal" enctype="multipart/form-data" method="post">

											<div class="dropbox">

											<div class="innerbox">

											<div id="drop">

				 

			<div class="video">

			<input type="hidden" name="video_img" id="video_img" class="form-control" value="<?=$vid->name?>" >

			<input type="hidden" name="video" id="video" class="form-control" value="<?=$vid->videothumb?>" >

			<button type="submit" id="del" onclick="del_video();" class="close">&times;</button>

					

					

					<video width="240" height="240" style="float:left;margin-left:40px;" controls>

					 <source src="http://howclip.com/uploads/live/<?php echo $vid->name;?>" type="video/mp4">

					</video>

					<?php if($vid->videothumb !='') { ?>

					<img src="http://howclip.com/uploads/images/<?php echo $vid->videothumb;?>"   style="float:left;width:240px;height:132px;margin-left:10px;margin-top:55px">

					<?php } ?>

				 </div>

               </div>

			   <div class="clear"/>

			   <div class="form-group">

			    <label class="control-label col-md-4" style="text-align:right; display:none">Category Name: <span class="required">*</span></label>

                  <div class="col-md-4">

				  </div>

			   </div>

			   <div class="form-group">

                

                  <label class="control-label col-md-4" style="text-align:right;">Category Name: <span class="required">*</span></label>

                  <div class="col-md-4">

              <select name="cat_name" id="category" class="form-control" style="width:200px;" >

	  <option  value="0">--Select Category--</option>

	   <?php foreach ($categoryList as $cat) { ?>
                                                            <option value="<?php echo $cat['category_title']; ?>" <?php if($cat["category_title"] == $vid->video_category){
                                                            echo "selected";} ?>>
                                                            <?php echo $cat['category_title']; ?></option> 
                                                            <?php } ?>

	  </select>

                 <span class="error-span"></span>

                 </div>

               </div>   

				 <div class="form-group" style="display:none">

				<label class="control-label col-md-4" style="text-align:right;">Choose Video:<span class="required">*</span></label>

	<div class="col-md-6" >

				 <input type="file" name="file1" class="form-control" value="<?=$vid->name?>" id="file1" style="width:200px;">

				  <span class="error-span"></span>



				  <progress id="progressBar" value="0" max="100" style="width:200px; display:none"></progress>

  <h4 id="status"></h4>

  

			</div>  	

			

                 </div>

				 

				 

				 

				   <div class="form-group">

                  <label class="control-label col-md-4" style="text-align:right;">Video Name:<span class="required">*</span></label>

                  <div class="col-md-4" >

                 <input type="text" class="form-control" id="name" name="name" value="<?php echo $vid->videoname;?>"  required="required"  style="width:200px;"/>

                 <span class="error-span"></span>

                 </div>

               </div>  

			   <div class="form-group">

                  <label class="control-label col-md-4" style="text-align:right;">Description:<span class="required">*</span></label>

                  <div class="col-md-8" >

				  <textarea rows="4"  cols="60" id="description" class="form-control" name="description"  style="width:200px;"  required="required"><?php echo $vid->description;?>

				  </textarea>

                 

                 <span class="error-span"></span>

                 </div>

               </div>

			   <div class="form-group">

                

                  <label class="control-label col-md-4" style="text-align:right;">Video Duration: <span class="required">*</span></label>

                  <div class="col-md-4">

              <input type="text" class="form-control" value="<?php echo $vid->video_duration;?>" id="duration" name="duration"  required="required"  style="width:200px;"/>

                 <span class="error-span"></span>

                 </div>

               </div> 

			    <div class="form-group">

                

                  <label class="control-label col-md-4" style="text-align:right;">Video Feature: <span class="required">*</span></label>

                  <div class="col-md-4">

              <select name="feature" id="feature" class="form-control" style="width:200px;" >

	  <option>--Select Feature--</option>

	  <?php foreach($features as $fea) { ?><option <?php if( $fea->name == $vid->videoFeature) { echo "selected"; }?>  value="<?php echo $fea->name;?>"><?php echo $fea->name; ?></option> <?php } ?>

	  </select>

                 <span class="error-span"></span>

                 </div>

               </div> 

						

							

			   <div class="form-actions clearfix">



                                  <div class="row">



                                     <div class="col-md-12">



                                        <div class="col-md-offset-6 col-md-9" >



                                           



                                         <button type="button"  onclick="uploadFile()" class="btn btn-primary"> <?php echo 'Save';?> <i class="fa fa-save"></i></button><!--<p id="loaded_n_total"></p>-->



                                        </div>



                                     </div>



                                  </div>



                               </div> 

			</div>

									

											

											</div>

											</div>

											</div>

										

											</form>

											<?php } ?>

											</div>

										</div>

										<!-- /BASIC -->

										<!-- BASIC -->

										

          </section>

        </div>

      </div>

    </div>

  </div>

</section>













