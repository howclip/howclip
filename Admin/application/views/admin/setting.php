<div id="main-content">
  
   
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
         <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
               
                <div class="clearfix">
                  <h3 class="content-title pull-left">MANAGE SETTING</h3>
                </div>
               <? //print_r($h); ?>
              </div>
            </div>
          </div>
		  
		 <? echo $netamt = $this->master_model->message($this->session->flashdata('flash_message'),$this->session->flashdata('flash_message123'));  ?>	 
		  
					
			<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>DEFAULT SETTINGS</h4>
												<div class="tools hidden-xs">
													
												</div>
											</div>
											<div class="box-body big">
											
											<?php echo form_open('admin/system_setting/', array('id' => 'usersForm', 'class' => 'form-horizontal'));?>
          <div class="panel panel-default" style="padding: 0px;border: none;">

            <div class="panel-body" style="padding: 0px;">

              <div class="tabbable">
     <div class="row">
     <div class="col-md-6">

				  <div class="form-group">
                  <label class="control-label col-md-4" style="text-align:right;"><?php echo 'Currency';?> </label>
                  <div class="col-md-8">
                  
                  <select class="col-md-12 select2-onscreen"  name="currency" id="currency" >
                  
                  <?php $roll_a=$this->db->get('currency')->result_array();

                  foreach($roll_a as $role):?>

                  <option 
                  <?php if($role['id']==@$h['0']->currency)echo 'selected'; ?> value="<?php echo $role['id'] ?>" ><?php echo $role['name'].'('.$role['symbol'].')'; ?></option>

                  <?php endforeach;?>

                  </select>
                 
                  
                  
                <!-- <input type="text" class="form-control" name="currency" id="currency" value="<? echo @$h['0']->currency; ?>"/>-->
                 <span class="error-span"></span>
                 </div>
               </div> 
               
              
      </div>
   <div class="col-md-6">
 
  
   
   </div>
               </div>
               <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >

                                           

                                         <button type="submit" class="btn btn-primary"> <?php echo 'SAVE SETTINGS';?> <i class="fa fa-save"></i></button>

                                        </div>

                                     </div>

                                  </div>

                               </div>
               
               
                
              </div>
              <?php echo form_close();?>
											
											
											
											
											
											

											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
      
        </div>
      </div>
    </div>
  </div>
</section>

<script>
$("#currency").select2({

                placeholder: "Select Currency"

            });
</script>
