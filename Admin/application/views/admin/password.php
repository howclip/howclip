<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
          <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
                <!-- STYLER -->
                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                  <li>  <a href="#"></a> </li>
                  <!--<li>
											<a href="#">Other Pages</a>
										</li>
										<li>Blank Page</li>-->
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                  <h3 class="content-title pull-left">CHANGE PASSWORD</h3>
                </div>
               
              </div>
            </div>
          </div>
		   <?php if($this->session->flashdata('err'))
	 		{
			?>
						<div class="separator"></div>
						<div class="alert alert-block alert-danger fade in">
											<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
												<p><h4><i class="fa fa-exclamation-circle"></i> Warning</h4> <?php echo $this->session->flashdata('err'); ?></p>
										</div>
										
			<?php } ?>	
										<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>
												<div class="tools hidden-xs">
													
												</div>
											</div>
											<div class="box-body big">
											<?php echo form_open('admin/changepassword', array('id' => 'usersForm', 'class' => 'form-horizontal'));?>
          <div class="panel panel-default">

            <div class="panel-body">

              <div class="tabbable">
    
                <div class="form-group">
                <input type="hidden" id="loginid" name="loginid" value="<?php echo $this->session->userdata('id'); ?>" />
                  <label class="control-label col-md-6" style="text-align:right;"><?php echo 'Current';?> <?php echo 'Password';?> <span class="required">*</span></label>
                  <div class="col-md-6">
                 <input type="password" class="form-control" name="password" id="password1" required="required"   style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div> 
               
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;"><?php echo 'New';?> <?php echo 'Password';?> <span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="password" class="form-control" name="new_pass" id="password" onkeypress="mn()" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
               
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;"><?php echo 'Confirm';?> <?php echo 'Password';?> <span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="password" class="form-control" name="conf_pass" onkeyup="mm();" id="cnpassword"  required="required" style="width:200px;"/>
                 <span id="val"></span>
                 </div>
               </div>  
               <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >

                                           

                                         <button type="submit" class="btn btn-primary"> <?php echo 'Change Password';?> <i class="fa fa-save"></i></button>

                                        </div>

                                     </div>

                                  </div>

                               </div>
               
               
                
              </div>
              <?php echo form_close();?>
											
											
											
											
											
											

											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
          </section>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">

function mm()

{

var v=$('#password').val();

if($('#password').val()!=$('#cnpassword').val())

$('#val').html('confirm password invalid');

else

$('#val').html('valid');

}


			
	

	<!------GET STATE LIST AJAX FUNCTION------>

			


		function isNumber(evt) {

        var iKeyCode = (evt.which) ? evt.which : evt.keyCode

        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))

            return false;



        return true;

   		}



</script>
