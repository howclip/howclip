<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
          <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
                <!-- STYLER -->
                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                  <li>  <a href="#"></a> </li>
                  <!--<li>
											<a href="#">Other Pages</a>
										</li>
										<li>Blank Page</li>-->
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                  <h3 class="content-title pull-left">CHANGE PASSWORD</h3>
                </div>
               
              </div>
            </div>
          </div>
		   
										<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>
												<div class="tools hidden-xs">
													<a href="#box-config" data-toggle="modal" class="config">
														<i class="fa fa-cog"></i>
													</a>
													<a href="javascript:;" class="reload">
														<i class="fa fa-refresh"></i>
													</a>
													<a href="javascript:;" class="collapse">
														<i class="fa fa-chevron-up"></i>
													</a>
													<a href="javascript:;" class="remove">
														<i class="fa fa-times"></i>
													</a>
												</div>
											</div>
											<div class="box-body big">
											<?php echo form_open('admin/changepassword', array('id' => 'usersForm', 'class' => 'form-horizontal'));?>
												
												  
												  <div class="form-group">
													<label for="exampleInputPassword1">Old Password</label>
													<input type="password" class="form-control" id="oldpassword" placeholder="Old Password">
												  </div>
												   <div class="form-group">
													<label for="exampleInputPassword1">New Password</label>
													<input type="password" class="form-control" id="newpassword" placeholder="New Password">
												  </div>
												   <div class="form-group">
													<label for="exampleInputPassword1">Confirm Password</label>
													<input type="password" class="form-control" id="cnpassword" placeholder="Confirm Password">
												  </div>
												 
												  <button type="submit" class="btn btn-success">Submit</button>
													</div>
												  </div>
												  <?php echo form_close();?>

											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
          </section>
        </div>
      </div>
    </div>
  </div>
</section>