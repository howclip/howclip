<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
         
             
                  <div class="clearfix">
                  <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>
				  <div class="content-title pull-right">
				     <form  method="post"  >
										<button class="btn btn-xs btn-primary" onclick="window.location='add_feature'; return false;"> Add Feature </button> 
					</form>
									</div>
							
                </div>
               
           
            	<!-- SAMPLE BOX CONFIGURATION MODAL FORM-->
			
			<!-- /SAMPLE BOX CONFIGURATION MODAL FORM-->
						
					
						<!-- TABLE IN MODAL -->
						<div class="row">
							<div class="col-md-6">
								<!-- BOX -->
								
									
									
								<!-- /BOX -->
							</div>
						</div>
						<!-- /TABLE IN MODAL -->
						<!-- SAMPLE BOX CONFIGURATION MODAL FORM-->
						
						
			
			<?php if($this->session->flashdata('permission_message'))
	 		{
			?>
					
						<div class="alert alert-block alert-danger fade in">
											<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
												<p><h4><i class="fa fa-exclamation-circle"></i> Warning</h4> <?php echo $this->session->flashdata('permission_message'); ?></p>
										</div>
										
									
			<?php } ?>				
					
						<!-- /SAMPLE BOX CONFIGURATION MODAL FORM-->
						
						<!-- DATA TABLES -->
						<div class="row">
							<div class="col-md-12">
								<!-- BOX -->
								<div class="box border blue">
									<div class="box-title">
										<h4><i class="fa fa-table"></i><?php echo $page_title;?></h4>
										<div class="tools hidden-xs">
											
											</a>
											
										</div>
									</div>
									<div class="box-body">
										<table id="datatable1" cellpadding="0" cellspacing="0" border="0" class="datatable table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th>Id</th>
													<th>Video Feature Name</th>
													<th>Action</th>
												</tr>
											</thead>
											
											<tbody>
											 <?php $i=1;  foreach($features as $feature){ ?>
   
												<tr class="gradeX">
												</td>
													 <td class="con0"><?=$i?></td>
													  <td><?=$feature->name?></td>
													<td>
										
											
										<a href="update_feature?id=<?php echo $feature->id;?>" rel="tooltip" data-placement="top" data-original-title="<?php echo 'edit' ?> <?php echo 'Feature' ?>" class="fa fa-edit"> </a>
&nbsp;&nbsp;&nbsp;<a href="delete_feature?id=<?php echo $feature->id;?>"  onclick="confirmDelete()" class="fa fa-trash-o">  </a>
																	</td>
												</tr>
												 <?php $i++; }    ?>
												
												
												
											</tbody>
											
										</table>
									</div>
								</div>
								<!-- /BOX -->
									
							</div>
						</div>
						<!-- /DATA TABLES -->
						
						
						<!-- /EXPORT TABLES -->
						<div class="footer-tools">
							<span class="go-top">
								<i class="fa fa-chevron-up"></i> Top
							</span>
						</div>
					</div><!-- /CONTENT-->
				</div>
			</div>

		</div>
	</section>
	<!--/PAGE -->

		 
          <!-- page end-->
          </section>
        </div>
      </div>
    </div>
  </div>
</section>

<!--
<script type="text/javascript" language="javascript"> 
window.onload = function disableBackButton()
{
window.history.forward()
}  
disableBackButton();  
window.onload=disableBackButton();  
window.onpageshow=function(evt) { if(evt.persisted) disableBackButton() }  
window.onunload=function() { void(0) }  -->

</script>