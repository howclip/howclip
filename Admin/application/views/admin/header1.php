<!DOCTYPE html>
<html lang="en">
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta http-equiv="content-type" Content="application/javascript charset=UTF-8">
	<meta charset="utf-8">
	<title>HowClip</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">
	<meta name="description" content="">
	<meta name="author" content="">
<!--   web cam -->
<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/webcam/core.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/webcam/snap.min.css" media="all"/>
<script src="<?php echo base_url();?>/assets/webcam/snap-utils.js"></script>
<script src="<?php echo base_url();?>/assets/webcam/snap.js"></script>-->
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/cloud-admin.css" >
	<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/themes/default.css"  >
	<link rel="stylesheet" type="text/css"  href="<?php echo base_url();?>assets/css/responsive.css" >
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	<!-- ANIMATE -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animatecss/animate.min.css" />
	<!-- DATE RANGE PICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap-daterangepicker/daterangepicker-bs3.css" />
    
    <!--BOOTSTARP DATE PICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstarp-datepicker/bootstrap-datetimepicker.min.css" />
    
	<!-- TODO -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/jquery-todo/css/styles.css" />
	<!-- FULL CALENDAR -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/fullcalendar/fullcalendar.min.css" />
	<!-- GRITTER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/gritter/css/jquery.gritter.css" />
	<!-- FONTS -->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
    
    <!-- DATA TABLES -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datatables/media/css/jquery.dataTables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datatables/media/assets/css/datatables.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datatables/extras/TableTools/media/css/TableTools.min.css" />
    
    <!-- SELECT2 -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/select2/select2.min.css" />
    <!-- FULL CALENDAR -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/fullcalendar/fullcalendar.min.css" />
	<!-- UNIFORM -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/uniform/css/uniform.default.min.css" />
    
    <!-- TABLE CLOTH -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/tablecloth/css/tablecloth.min.css" />
    
    <!-- JQUERY UI-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/jquery-ui-1.10.3.custom/css/custom-theme/jquery-ui-1.10.3.custom.min.css" />
    <!-- BOOTSTRAP-SWITCH 
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap-switch/bootstrap-switch.min.css">-->
    <!-- TYPEAHEAD -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/typeahead/typeahead.css" />
    
    <!-- FILE UPLOAD -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap-fileupload/bootstrap-fileupload.min.css" />
    
    <!-- BOOTSTRAP-SWITCH -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap-switch/bootstrap-switch.min.css">
	<!-- WIZARD -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/bootstrap-wizard/wizard.css" />
    <!-- DROPZONE -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/dropzone/dropzone.min.css" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700' rel='stylesheet' type='text/css'>
  <!---------Fancy Checkbox----------------->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/fancy-checkbox.css" />
  
  <!----------------------------->
    <!-- DATE PICKER -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datepicker/themes/default.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datepicker/themes/default.date.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/js/datepicker/themes/default.time.min.css" />
    <!-- JQUERY -->
	<script src="<?php echo base_url();?>/assets/js/jquery/jquery-2.0.3.min.js"></script>
    <script src="<?php echo base_url();?>/assets/css/datatables.min.js"></script>
	<?php $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		  $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		  $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
		  $this->output->set_header('Pragma: no-cache');  ?>
		  
<script>
	 function confirmDelete(){
			var agree = confirm("Are you sure you want to delete this record?");
			  if(agree == true){
				return true;
			}
			else{
			return false;
			}
			}
	</script>
	
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/datatables.min.css">
</head>    
<body>
    
<header class="navbar clearfix" id="header">
  <div class="container">
    <div class="navbar-brand">
  
      <a href="#"> <span class="logos">HowClip</span> </a>
     
      <div id="sidebar-collapse" class="sidebar-collapse btn">
	   <i class="fa fa-bars" data-icon1="fa fa-bars" data-icon2="fa fa-bars" ></i> 
	 </div>
  
    </div>
 
    <ul class="nav navbar-nav pull-right">
      <!-- BEGIN USER LOGIN DROPDOWN -->
      <li class="dropdown user" id="header-user"> <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <span class="username"> <i class="fa fa-user"></i> <?php echo ucwords($this->session->userdata('name')); ?></span> <i class="fa fa-angle-down"></i> </a>
        <ul class="dropdown-menu">
          
          <li><a href="<?php echo base_url()?>index.php/admin/password"><i class="fa fa-eye"></i> Change Password</a></li>
          <li><a href="<?php echo base_url()?>index.php/home/logout"><i class="fa fa-power-off"></i> Log Out</a></li>
        </ul>
      </li>
      <!-- END USER LOGIN DROPDOWN -->
    </ul>
    <!-- END TOP NAVIGATION MENU -->
  </div>
</header>
<!--/HEADER -->
<!-- PAGE -->
<section id="page">
  <!-- SIDEBAR -->
  <div id="sidebar" class="sidebar">
    <div class="sidebar-menu nav-collapse">
      <div class="divide-20"></div>
      <!-- SIDEBAR MENU -->
      <ul class="sidebar-menu" id="nav-accordion">
        <li> <a  href="<?php echo base_url()?>index.php/admin/dashboard"  class="active"> <i class=" fa fa-bar-chart-o"></i><span>DASHBOARD</span> </a> </li>
           <li> <a href="<?php echo base_url()?>index.php/admin/setting"> <i class="fa fa-cogs"></i><span>SETTING</span></a> </li>
		   <li> <a href="<?php echo base_url()?>index.php/admin/category"> <i class="fa fa-check-square"></i><span>CATEGORY</span></a> </li>
		    <li> <a href="<?php echo base_url()?>index.php/admin/show_video"> <i class="fa fa-video-camera"></i><span>VIDEO</span></a> </li>
			 <li> <a href="<?php echo base_url()?>index.php/admin/feature"> <i class="fa fa-video-camera"></i><span>VIDEO FEATURE</span></a> </li>
			 <li  data-toggle="collapse" data-target="#products" class="sidebar-menu">
                  <a href="#"><i class="fa fa-pencil-square"></i> HOME PAGES <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="products">
				 <li> <a href="<?php echo base_url()?>index.php/admin/about"> <i class="fa fa-pencil-square"></i><span>ABOUT US</span></a> </li>
			   <li> <a href="<?php echo base_url()?>index.php/admin/terms"> <i class="fa fa-pencil-square"></i><span>Terms & Conditions</span></a> </li>
		 <li> <a href="<?php echo base_url()?>index.php/admin/gettingstart"> <i class="fa fa-pencil-square"></i><span>Getting Started</span></a> </li>
		  <li> <a href="<?php echo base_url()?>index.php/admin/Contact"> <i class="fa fa-pencil-square"></i><span>Contact Us</span></a> </li>
                   
                </ul>
			 
			 
			 
			  <!--<li> <a href="<?php echo base_url()?>index.php/admin/about"> <i class="fa fa-user"></i><span>ABOUT US</span></a> </li>
			   <li> <a href="<?php echo base_url()?>index.php/admin/terms"> <i class="fa fa-user"></i><span>Terms & Conditions</span></a> </li>
		 <li> <a href="<?php echo base_url()?>index.php/admin/gettingstart"> <i class="fa fa-user"></i><span>Getting Started</span></a> </li>
		  <li> <a href="<?php echo base_url()?>index.php/admin/Contact"> <i class="fa fa-user"></i><span>Contact Us</span></a> </li>-->
        </ul>
      </li>
        
        
        
        
        
      
      </ul>
      <!-- /SIDEBAR MENU -->
    </div>
  </div>
  <!-- /SIDEBAR -->
  
<!--/PAGE -->
<!-- JAVASCRIPTS -->




<!-- JAVASCRIPTS -->
	<!-- Placed at the end of the document so the pages load faster -->
	<!-- JQUERY -->
	
	<script src="<?php echo base_url();?>/assets/js/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.min.js"></script>
	<!-- BOOTSTRAP -->
	<script src="<?php echo base_url();?>/assets/bootstrap-dist/js/bootstrap.min.js"></script>
	
	<!--BOOTSTARP DATE PICKER MOMENT.JS -->
	<!--<script src="<?php echo base_url();?>/assets/js/bootstarp-datepicker/moment.js"></script>-->
     <!--BOOTSTARP DATE PICKER -->
	<script src="<?php echo base_url();?>/assets/js/bootstarp-datepicker/bootstrap-datetimepicker.js"></script>
    <!--<script src="<?php echo base_url();?>/assets/js/bootstarp-datepicker/bootstrap-datepicker.js"></script>-->
    
    	
	<!-- MOMENT.JS -->
	<script src="<?php echo base_url();?>/assets/js/bootstrap-daterangepicker/moment.min.js"></script>
    
   
	
	<script src="<?php echo base_url();?>/assets/js/bootstrap-daterangepicker/daterangepicker.min.js"></script>
	
	<!-- SLIMSCROLL -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jQuery-slimScroll-1.3.0/jquery.slimscroll.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jQuery-slimScroll-1.3.0/slimScrollHorizontal.min.js"></script>
	<!-- BLOCK UI -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jQuery-BlockUI/jquery.blockUI.min.js"></script>
    
    
    
	<!-- SPARKLINES -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/sparklines/jquery.sparkline.min.js"></script>
	<!-- EASY PIE CHART -->
	<script src="<?php echo base_url();?>/assets/js/jquery-easing/jquery.easing.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/easypiechart/jquery.easypiechart.min.js"></script>
	<!-- FLOT CHARTS -->
	<script src="<?php echo base_url();?>/assets/js/flot/jquery.flot.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/flot/jquery.flot.time.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/flot/jquery.flot.selection.min.js"></script>
	<script src="<?php echo base_url();?>/assets/js/flot/jquery.flot.resize.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/flot/jquery.flot.pie.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/flot/jquery.flot.stack.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/flot/jquery.flot.crosshair.min.js"></script>
	<!-- TODO -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jquery-todo/js/paddystodolist.js"></script>
	<!-- TIMEAGO -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/timeago/jquery.timeago.min.js"></script>
	<!-- FULL CALENDAR -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/fullcalendar/fullcalendar.min.js"></script>
	<!-- COOKIE -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/jQuery-Cookie/jquery.cookie.min.js"></script>
	<!-- GRITTER -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/gritter/js/jquery.gritter.min.js"></script>
	
    <!-- SELECT2 -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/select2/select2.min.js"></script>
	<!-- UNIFORM -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/uniform/jquery.uniform.min.js"></script>
	
	<!-- BOOTBOX -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootbox/bootbox.min.js"></script>
    <script src="<?php echo base_url();?>/assets/js/bootstrap-wizard/form-wizard.min.js"></script>
    <!-- MARKDOWN -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap-markdown/js/markdown.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap-markdown/js/to-markdown.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap-markdown/js/bootstrap-markdown.min.js"></script>
	<!-- BOOTSTRAP WYSIWYG -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap-wysiwyg/jquery.hotkeys.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap-wysiwyg/bootstrap-wysiwyg.min.js"></script>
    <!-- TABLE CLOTH -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/tablecloth/js/jquery.tablecloth.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/tablecloth/js/jquery.tablesorter.min.js"></script>
    <!-- CKEDITOR -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/ckeditor/ckeditor.js"></script>
	
    <!-- DATA TABLES -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datatables/media/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datatables/media/assets/js/datatables.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datatables/extras/TableTools/media/js/TableTools.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datatables/extras/TableTools/media/js/ZeroClipboard.min.js"></script>
    
    <!-- DATE PICKER -->
	<!--<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datepicker/picker.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datepicker/picker.date.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/datepicker/picker.time.js"></script>-->
    
    <!-- INPUT MASK -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
    <!-- BOOTSTRAP SWITCH 
    <script type="text/javascript" src="<?php echo base_url();?>/assets/js/bootstrap-switch/bootstrap-switch.min.js"></script>-->
   	<!-- DROPZONE -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/dropzone/dropzone.min.js"></script>

    	<!-- FULL CALENDAR -->
	<script type="text/javascript" src="<?php echo base_url();?>/assets/js/fullcalendar/fullcalendar.min.js"></script>

     <!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url();?>/assets/js/script.js"></script>
    <script src="<?php echo base_url();?>/assets/js/addremove.js"></script>
    <script src="<?php echo base_url();?>/assets/js/enquiry.js"></script>
	<!-- WIZARD -->
	<script src="<?php echo base_url();?>assets/js/bootstrap-wizard/jquery.bootstrap.wizard.min.js"></script>
	<!-- WIZARD -->
	<script src="<?php echo base_url();?>assets/js/jquery-validate/jquery.validate.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap-wizard/form-wizard.min.js"></script>
	<!-- CUSTOM SCRIPT -->
	<script src="<?php echo base_url();?>assets/js/script.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap-wizard/form-wizard.min.js"></script>
	 <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
 <script src="//oss.maxcdn.com/jquery.form/3.50/jquery.form.min.js"></script>
 <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/apistyle.css"/>

    <script type="text/javascript" src="//imasdk.googleapis.com/js/sdkloader/ima3.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/application.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/js/ads.js"></script>
   
	 <script type='text/javascript'>
       var googletag = googletag || {};
       googletag.cmd = googletag.cmd || [];
       (function() {
         var gads = document.createElement('script');
         gads.async = true;
         gads.type = 'text/javascript';
         gads.src = '//www.googletagservices.com/tag/js/gpt.js';
         var node = document.getElementsByTagName('script')[0];
         node.parentNode.insertBefore(gads, node);
       })();
     </script>

     <!-- Register your companion slots -->
     <script type='text/javascript'>
       googletag.cmd.push(function() {
         // Supply YOUR_NETWORK/YOUR_UNIT_PATH in place of 6062/iab_vast_samples.
         googletag.defineSlot('/6062/iab_vast_samples', [728, 90], 'companionDiv')
             .addService(googletag.companionAds())
             .addService(googletag.pubads());
         googletag.companionAds().setRefreshUnfilledSlots(true);
         googletag.pubads().enableVideoAds();
         googletag.enableServices();
       });
     </script>

