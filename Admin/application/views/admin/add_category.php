<style type="text/css">
.loading-image {
  position: absolute;
  top: 22%;
  left: -25%;
  z-index: 10;
}
.loader
{
    display: none;
    width:200px;
    height: 200px;
    position: fixed;
    top: 50%;
    left: 50%;
    text-align:center;
    margin-left: -50px;
    margin-top: -100px;
    z-index:2;
   
}
</style>

<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
          <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
                <!-- STYLER -->
                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                  <li>  <a href="#"></a> </li>
                  <!--<li>
											<a href="#">Other Pages</a>
										</li>
										<li>Blank Page</li>-->
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                  <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>
                </div>
               
              </div>
            </div>
          </div>
		    <?php if($this->session->flashdata('msg'))
	 		{
			?>
						<div class="separator"></div>
						<div class="alert alert-block alert-success fade in">
											<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
												<p><h4> Successful!</h4> <?php echo $this->session->flashdata('msg'); ?></p>
										</div>
										
									
			<?php } ?>		
										<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>
												<div class="tools hidden-xs">
													
												</div>
											</div>
											<div class="box-body big">
											<?php echo form_open('admin/add_category', array('id' => 'usersForm', 'class' => 'form-horizontal'));?>
          <div class="panel panel-default">

            <div class="panel-body">

              <div class="tabbable">
    
                <div class="form-group">
                
                  <label class="control-label col-md-6" style="text-align:right;">Parent Category Name: <span class="required">*</span></label>
                  <div class="col-md-6">
				<select name="cat_name" class="form-control" id="a1" style="width:200px;">
				<option value="0">Main category </option>
				<?php foreach($categoryList as $cl) { ?>
				<option  value="<?php echo $cl["id"] ?>"><?php echo $cl["category_title"]; ?></option>
				<?php } ?>
				</select>
			  
				  
              <!--<select name="cat_name" class="form-control" style="width:200px;" >
	  <option  value="0">--No Parent--</option>
	  <? foreach($categories as $cat) { ?><option  value="<? echo $cat->id;?>"><? echo $cat->category_title; ?></option> <? } ?>
	  </select>-->
                 <span class="error-span"></span>
                 </div>
               </div> 
               
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Sub Category Name/Main Category Name:<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="sub_cat" id="c1" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			    
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Status:<span class="required">*</span></label>
                  <div class="col-md-6" >
				  <select name="status" id="d1" class="form-control" required="required" style="width:200px;">
				  <option value="">Select Status</option>
				 <option value="active">Active</option>
				  <option value="inactive">Inactive</option>
				  <option value="pending"> Pending</option>
                 </select>
                 <span class="error-span"></span>
                 </div>
               </div>  
               
              
               <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >

                                           

                                         <button id="b1" type="submit" class="btn btn-primary"> <?php echo 'Add Category';?> <i class="fa fa-save"></i></button>

                                        </div>

                                     </div>

                                  </div>

                               </div>
              <div class="loader">
   <center>
       <img class="loading-image" src="http://myvote.resultxpress.com/image/giphy.gif" alt="loading..">
   </center>
</div>
                
              </div>
              <?php echo form_close();?>
											
											
											
											
											
											

											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
          </section>
        </div>
      </div>
    </div>
  </div>
</section>


<script>
     $("#b1").on("click", function (e) { 
         var a1=$('#a1').val();
          var c1=$('#c1').val();
           var d1=$('#d1').val();
         if(a1 && c1 && d1){
           setTimeout(function(){  $("#b1").prop('disabled', true); }, 300);
      ;
       location.reload();
   }  
            });
    </script>
    