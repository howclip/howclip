	
	<script>

function _(el){
	return document.getElementById(el);
}
function uploadFile(){
  document.getElementById('progressBar').style.display = "block";
 var name = document.getElementById('name').value;
  var description = document.getElementById('video_description').value;
  var duration = document.getElementById('duration').value;
  var category = document.getElementById('category').value; 
   var feature = document.getElementById('feature').value;
  
	var file = _("file1").files[0];
		//var name = _("name").name;
	 //alert(file.name+" | "+file.size+" | "+file.type);
	var formdata = new FormData();
	formdata.append("file1", file);
	formdata.append("name", name);
	formdata.append("description", description);
	formdata.append("category", category);
	formdata.append("duration", duration);
	formdata.append("feature", feature);
	
	var ajax = new XMLHttpRequest();
	ajax.upload.addEventListener("progress", progressHandler, false);
	ajax.addEventListener("load", completeHandler, false);
	ajax.addEventListener("error", errorHandler, false);
	ajax.addEventListener("abort", abortHandler, false);
	ajax.open("POST", "upload");
	ajax.send(formdata);
}
function progressHandler(event){
	_("loaded_n_total").innerHTML = "Uploaded "+event.loaded+" bytes of "+event.total;
	var percent = (event.loaded / event.total) * 100;
	_("progressBar").value = Math.round(percent);
	_("status").innerHTML = Math.round(percent)+"% uploaded... please wait";
}
function completeHandler(event){
	alert('Uploaded Successfully');
	_("status").innerHTML = event.target.responseText;
	_("progressBar").value = 0;
	//_("msg").innerHTML = "Uploaded Successfully";
	 document.getElementById('progressBar').style.display = "none";
	 document.getElementById('file1').value = "";
	 document.getElementById('category').value = "";
	 document.getElementById('duration').value = "";
	 document.getElementById('name').value = "";
	 document.getElementById('video_description').value = "";
	
}
function errorHandler(event){
	_("status").innerHTML = "Upload Failed";
}
function abortHandler(event){
	_("status").innerHTML = "Upload Aborted";
}
</script>
 
<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
          <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
              
                <div class="clearfix">
                  <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>
                </div>
               
              </div>
            </div>
          </div>
		   <div style="color:#FF0000">  <?= validation_errors(); ?></div>
										<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>
												<div class="tools hidden-xs">
													
												</div>
											</div>
											<div class="box-body big">
											
											
										<div class="row">
										
											<div class="col-md-10">
											<form id="upload_form" class="form-horizontal" enctype="multipart/form-data" method="post">
											<div class="dropbox">
											<div class="innerbox">
											<div id="drop">
											 <div class="form-group">
                
                  <label class="control-label col-md-4" style="text-align:right;">Category Name: <span class="required">*</span></label>
                  <div class="col-md-4">
              <select name="cat_name" id="category" class="form-control" style="width:200px;" >
	  <option>--Select Category--</option>
	  <?php foreach($categories as $cat) { ?><option  value="<?php echo $cat->category_title;?>"><?php echo $cat->category_title; ?></option> <?php } ?>
	  </select>
                 <span class="error-span"></span>
                 </div>
               </div> 
											
											
											
											
				 <div class="form-group">
				<label class="control-label col-md-4" style="text-align:right;">Choose Video:<span class="required">*</span></label>

			<!--	<a>Browse</a>-->
			<div class="col-md-8" >
				 <input type="file" name="file1" class="form-control" id="file1" style="width:200px;">
				  <progress id="progressBar" value="0" max="100" style="width:200px; display:none"></progress>
  <h3 id="status"></h3>
  
                 </div>
               </div>  
				 
				   <div class="form-group">
                  <label class="control-label col-md-4" style="text-align:right;">Video Name:<span class="required">*</span></label>
                  <div class="col-md-8" >
                 <input type="text" class="form-control" id="name" name="name"  required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			   <div class="form-group">
                  <label class="control-label col-md-4" style="text-align:right;">Description:<span class="required">*</span></label>
                  <div class="col-md-8" >
				 <!-- <textarea  name="editor1" id="video_description" rows="10" cols="80">
               
            </textarea>-->
			
				  <textarea rows="4" cols="60" id="video_description"  name="description" style="width:200px;" required="required"> </textarea>
				 
                 
                 <span class="error-span"></span>
                 </div>
               </div>
			   
			    <div class="form-group">
                
                  <label class="control-label col-md-4" style="text-align:right;">Video Duration: <span class="required">*</span></label>
                  <div class="col-md-4">
              <input type="text" class="form-control" id="duration" name="duration"  required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div> 
											
			     <div class="form-group">
                
                  <label class="control-label col-md-4" style="text-align:right;">Video Feature: <span class="required">*</span></label>
                  <div class="col-md-4">
              <select name="cat_name" id="feature" class="form-control" style="width:200px;" >
	  <option>--Select Feature--</option>
	  <?php foreach($features as $fea) { ?><option  value="<?php echo $fea->name;?>"><?php echo $fea->name; ?></option> <?php } ?>
	  </select>
                 <span class="error-span"></span>
                 </div>
               </div> 
						
			   
			   <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >

                                           

                                         <button type="button"  onclick="uploadFile()" class="btn btn-primary"> <?php echo 'Upload File';?> <i class="fa fa-save"></i></button><p id="loaded_n_total"></p>

                                        </div>

                                     </div>

                                  </div>

                               </div> 
			</div>
									
											
											</div>
											</div>
											</div>
										</div>	
											</form>
											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
          </section>
        </div>
      </div>
    </div>
  </div>
</section>
 <!--<script>
 CKEDITOR.replace( 'editor1' );
 </script>-->