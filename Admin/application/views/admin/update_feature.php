<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
          <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
                <!-- STYLER -->
                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                  <li>  <a href="#"></a> </li>
                  <!--<li>
											<a href="#">Other Pages</a>
										</li>
										<li>Blank Page</li>-->
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                  <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>
                </div>
               
              </div>
            </div>
          </div>
		    <?php if($this->session->flashdata('msg'))
	 		{
			?>
						<div class="separator"></div>
						<div class="alert alert-block alert-success fade in">
											<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
												<p><h4> Successful!</h4> <?php echo $this->session->flashdata('msg'); ?></p>
										</div>
										
									
			<?php } ?>	<div style="color:#FF0000">  <?= validation_errors(); ?></div>
										<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>
												<div class="tools hidden-xs">
													
												</div>
											</div>
											<?php foreach($feature as $feat) {?>
											<div class="box-body big">
											<?php echo form_open('admin/update_feature?id='.$feat->id, array('id' => 'usersForm', 'class' => 'form-horizontal'));?>
          <div class="panel panel-default">

            <div class="panel-body">

              <div class="tabbable">
    
               
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Video Feature:<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="name" style="width:200px;" value="<?php echo $feat->name;?>"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			      
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;"> Feature Status:<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <select name="status" class="form-control" style="width:200px;" >
				  <option>--Select--</option>
				 <option value="Active" <?php if($feat->status == 'Active'){ echo "selected"; }?> >Active</option>
				 <option value="Inactive"  <?php if($feat->status == 'Inactive'){ echo "selected"; }?> >Inactive</option>
				 </select>
                 <span class="error-span"></span>
                 </div>
               </div>  
			    
                 
               
              
               <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >

                                           

                                         <button type="submit" class="btn btn-primary"> <?php echo 'Update Feature';?> <i class="fa fa-save"></i></button>

                                        </div>

                                     </div>

                                  </div>

                               </div>
               
               
                
              </div>
              <?php echo form_close();?>
			  <?php } ?>
											
											
											
											
											
											

											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
          </section>
        </div>
      </div>
    </div>
  </div>
</section>


