<?php // error_reporting(); ?>
<div id="main-content">
    <div class="container">
        <div class="row">
            <div id="content" class="col-lg-12">
                <!-- PAGE HEADER-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="page-header">
                            <!-- STYLER -->
                            <!-- /STYLER -->
                            <!-- BREADCRUMBS -->
                            <ul class="breadcrumb">
                                <li>  <a href="#"></a> </li>
                                <!--<li>
                                                                                                      <a href="#">Other Pages</a>
                                                                                              </li>
                                                                                              <li>Blank Page</li>-->
                            </ul>
                            <!-- /BREADCRUMBS -->
                            <div class="clearfix">
                                <h3 class="content-title pull-left"><?php echo "Import Category"; ?></h3>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
                if ($this->session->flashdata('permission_message')) {  
                    ?>
                    <div class="separator"></div>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                      
                        <p style="color:red;"><h4> </h4> <span style="color:red;"><?php echo $this->session->flashdata('permission_message'); ?></span></p>
                    </div>


                <?php } ?>
                <?php
                if ($this->session->flashdata('msg')) {
                    ?>
                    <div class="separator"></div>
                    <div class="alert alert-block alert-success fade in">
                        <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                        <p><h4> Successful!</h4> <?php echo $this->session->flashdata('msg'); ?></p>
                    
                    </div>


                <?php } ?>	<div style="color:#FF0000">  <?= validation_errors(); ?></div>
                <!-- BASIC -->
                <div class="box border blue">
                    <div class="box-title">
                        <h4><i class="fa fa-bars"></i>Import Category</h4>
                        <div class="tools hidden-xs">

                        </div>
                    </div>
                    <div class="box-body big">





                        <?php echo form_open('admin/import_Categorydata', array('id' => 'usersForm', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data')); ?>
                        <div class="panel panel-default">
                              <div class="form-group">
                                  <div class="col-md-3">
                             <span id="err" style="color:red;display:none;" > Only .xlsx files are allowed</span>
                             <span id="err1" style="color:red;display:none;" > Choose File</span>
                                              </div>
                                  </div>
                            <div class="form-group">
                                <div class="col-md-3">
                                    <a href="<?php echo base_url(); ?>uploads/category.xlsx"><button type="button" style="margin-bottom:10px;" class="btn btn-primary start" 

                                                                                                     >Download Sample</button> </a> 
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="control-label col-md-3" style="text-align:right;">Choose File:<span class="required">*</span></label>
                                <div class="col-md-9" >
                                    <input type="file" class="form-control" id="file" name="file" value=""  />
                                    <span class="error-span"></span>
                                </div>
                            </div>  





                            <div class="form-actions clearfix">

                                <div class="row">

                                    <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >



                                            <button type="submit" onclick="return checkfile();" class="btn btn-primary"> <?php echo 'Import'; ?> <i class="fa fa-save"></i></button>

                                        </div>

                                    </div>

                                </div>

                            </div>




                            <?php echo form_close(); ?>








                        </div>
                        <!-- /BASIC -->
                        <!-- BASIC -->

                        </section>
                    </div>
                </div>
            </div>
        </div>
        </section>

        <script>
            function checkfile()
            {
                var file = document.getElementById("file").value;
                if (file == '')
                {

                    document.getElementById("err1").style.display = "block";
                    setTimeout(function () {
                        document.getElementById("err1").style.display = "none";
                    }, 2000);
                    return false;
                } else {
                    var ext = $('#file').val().split('.').pop().toLowerCase();
                    if ($.inArray(ext, ['xlsx']) == -1) {
                        document.getElementById("err").style.display = "block";
                        setTimeout(function () {
                            document.getElementById("err").style.display = "none";
                        }, 2000);
                        return false;
                    }

                }

            }

        </script>
