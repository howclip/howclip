
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
       $(function () {
        $("#datepicker").datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: '-100:+0'
        });
    });
  
  </script>
<div id="main-content">
    <div class="container">
      <div class="row">
        <div id="content" class="col-lg-12">
          <!-- PAGE HEADER-->
          <div class="row">
            <div class="col-sm-12">
         <div class="page-header">
                <!-- STYLER -->
                <!-- /STYLER -->
                <!-- BREADCRUMBS -->
                <ul class="breadcrumb">
                  <li>  <a href="#"></a> </li>
                  <!--<li>
											<a href="#">Other Pages</a>
										</li>
										<li>Blank Page</li>-->
                </ul>
                <!-- /BREADCRUMBS -->
                <div class="clearfix">
                  <h3 class="content-title pull-left"><?php echo $page_title; ?></h3>
                </div>
               
              </div>
            </div>
          </div>
		    <?php if($this->session->flashdata('msg'))
	 		{
			?>
						<div class="separator"></div>
						<div class="alert alert-block alert-success fade in">
											<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
												<p><h4> Successful!</h4> <?php echo $this->session->flashdata('msg'); ?></p>
										</div>
										
									
			<?php } ?>		
										<!-- BASIC -->
										<div class="box border blue">
											<div class="box-title">
												<h4><i class="fa fa-bars"></i>FILL YOUR DETAIL</h4>
												<div class="tools hidden-xs">
													
												</div>
											</div>
											<?php  //echo '<pre>'; print_r($setting_data);?>
											
											<?php foreach($setting_data as $data) { ?>
											<div class="box-body big">
											<?php echo form_open('admin/update_setting', array('id' => 'usersForm', 'class' => 'form-horizontal' , 'enctype' => 'multipart/form-data'));?>
          <div class="panel panel-default">

            <div class="panel-body">

              <div class="tabbable">
    
                <div class="form-group">
                
                  <label class="control-label col-md-6" style="text-align:right;">Name: <span class="required">*</span></label>
                  <div class="col-md-6">
				   <input type="text" class="form-control" value="<?php echo $data->username;?>" name="name" required="required"  style="width:200px;"/>
             
                 <span class="error-span"></span>
                 </div>
               </div> 
               
               <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Company Name:<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="company_name" value="<?php echo $data->companyName;?>" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			   <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Email:<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="email" value="<?php echo $data->email;?>" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			   <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Gender:<span class="required">*</span></label>
                  <div class="col-md-6" >
                  <select name="gender" class="form-control" style="width:200px;" >
                      <option <?php if($data->gender =='male'){ echo "selected"; }?>  value="male">Male</option> 
				  <option <?php if($data->gender =='female'){ echo "selected"; }?> value="female">Female</option> 
				  </select>
                 <span class="error-span"></span>
                 </div>
               </div>  
			   <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">DOB:<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" id="datepicker" name="dob" value="<?php echo $data->dob;?>" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			   <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Facebook URL <i class="fa fa-facebook-square" aria-hidden="true"></i> :<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="facebookId" value="<?php echo $data->facebookId;?>" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			     <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Twitter URL <i class="fa fa-twitter-square" aria-hidden="true"></i> :<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="googleId" value="<?php echo $data->googleId;?>" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
               
			     <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Instagram URL <i class="fa fa-instagram" aria-hidden="true"></i> :<span class="required">*</span></label>
                  <div class="col-md-6" >
                 <input type="text" class="form-control" name="steamId" value="<?php echo $data->steamId;?>" required="required"  style="width:200px;"/>
                 <span class="error-span"></span>
                 </div>
               </div>  
			    <div class="form-group">
                  <label class="control-label col-md-6" style="text-align:right;">Company Logo<span class="required">*</span></label>
                  <div class="col-md-6" >
				   <img src="<?php echo $this->config->base_url(); ?>uploads/<?php echo $data->company_logo;?>" width="150px" />
                 <input type="file" class="form-control" name="image1" value="<?php echo $data->company_logo;?>"  />
                 <span class="error-span"></span>
                 </div>
               </div>   
			    
               
               
              
               <div class="form-actions clearfix">

                                  <div class="row">

                                     <div class="col-md-12">

                                        <div class="col-md-offset-6 col-md-9" >

                                           

                                         <button type="submit" class="btn btn-primary"> <?php echo 'Save Settings';?> <i class="fa fa-save"></i></button>

                                        </div>

                                     </div>

                                  </div>

                               </div>
               
               
                
              </div>
              <?php echo form_close();?>
						<?php } ?>			
											</div>
										</div>
										<!-- /BASIC -->
										<!-- BASIC -->
										
          </section>
        </div>
      </div>
    </div>
  </div>
</section>


