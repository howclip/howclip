<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->library('session'); 
		 
	}
				
public function message($succ,$fail)
{
	if($succ)
	{
				echo'<div class="alert alert-block alert-success fade in">';
				echo '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>';
				echo'<p><h4> Successful!</h4>'.$succ.'</p>';
				echo'</div>';
	}
	if($fail)
	{
			
				echo'<div class="alert alert-block alert-danger fade in">';
				echo'<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>';
				echo'<p><h4><i class="fa fa-exclamation-circle"></i> Warning</h4>'.$fail.'</p>';
				echo'</div>';
	}							
			
				    				
}
public function fetchCategoryTree($parent = 0, $spacing = '', $user_tree_array = '') {

  if (!is_array($user_tree_array))
    $user_tree_array = array();

  $sql = "SELECT `id`, `category_title`, `category_parent` FROM `category` WHERE 1 AND `category_parent` = $parent ORDER BY id ASC";
  $query = mysql_query($sql);
  if (mysql_num_rows($query) > 0) {
    while ($row = mysql_fetch_object($query)) {
      $user_tree_array[] = array("id" => $row->id, "category_title" => $spacing . $row->category_title);
      $user_tree_array = $this->fetchCategoryTree($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);
    }
  }
  return $user_tree_array;
}

public function fetchCategoryTreeList($parent = 0, $user_tree_array = '') {

    if (!is_array($user_tree_array))
    $user_tree_array = array();

  $sql = "SELECT `id`, `category_title`, `category_parent` FROM `category` WHERE 1 AND `category_parent` = $parent ORDER BY id ASC";
  $query = mysql_query($sql);
  if (mysql_num_rows($query) > 0) {
     $user_tree_array[] = "<ul>";
    while ($row = mysql_fetch_object($query)) {
	  $user_tree_array[] = "<li>". $row->category_title."</li>";
      $user_tree_array = $this->fetchCategoryTreeList($row->cid, $user_tree_array);
    }
	$user_tree_array[] = "</ul>";
  }
  return $user_tree_array;
}	

	
public function getsetting($id)
{
	   $q = $this->db->get_where("settings" , array('created' => $id, 'company_id' =>$this->session->userdata('company_id')));
		if($q->num_rows() > 0)
		{
			foreach (($q->result_array()) as $row)
			 {
				$data = $row;
			 }
				
			return $data;
		}
}	


	function show_categories()
	{
	
		 $this->db->select('*');
		 $q = $this->db->get('category');
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function show_categoriesByIdold($id)
	{
		$this->db->where('id',$id);
		 $this->db->select('*');
		 $q = $this->db->get('category');
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
					
					 $this->db->where('id',$row->category_parent);
					 $this->db->select('category_title');
					 $qry = $this->db->get('category');
						foreach (($qry ->result()) as $r)
						{
						$par= $r->category_title;
						}
					$row->parent = $par;
					
				}
				//print_r($data);
				return $data;
			}
	}
	function show_categoriesById($id)
	{
		$sql = "select * from category where id = $id order by id desc ";
	$qry = $this->db->query($sql);
                                                                                                  				 	if($qry->num_rows() > 0)
				 {
				 	foreach(($qry->result()) as $key => $res)
					{
					 $pid = $res->category_parent;
					 $resdata[] = $res;
					 $vcat = $res->category_title;
					}	
				 }
				$catdata[] = $resdata;
			 $dataqqq = $catdata[0];
			
			 
			
			 foreach($catdata[0] as $key => $subdata)
			 {
			 	$id = $subdata->category_parent;
				$pcat = $subdata->category_title;
				$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
				$dataqqq[$key]->sub = $res;
				
				
				$id1 = $res['category_parent'];
				$pcat2 = $res['category_title'];
				
				$res1 =  $this->db->get_where('category',array('id'=>$id1))->row_array();
				$dataqqq[$key]->sub['child'] = $res1;
				
				$id2 = $res1['category_parent'];
				$pcat3 = $res1['category_title'];
				$res2 =  $this->db->get_where('category',array('id'=>$id2))->row_array();
				$dataqqq[$key]->sub['child']['child2'] = $res2;
				
				$id3 = $res2['category_parent'];
				$res3 =  $this->db->get_where('category',array('id'=>$id3))->row_array();
				$dataqqq[$key]->sub['child']['child2']['child3'] = $res3;
				
				
				
				
			 }
			 
			 return $dataqqq;
			
			
	}
	public function add_category($category)
	{
		
		$this->db->insert('category',$category); 
		return true;  
	}
	public function update_category($category,$id)
	{
	
		$this->db->where('id', $id);
		if($this->db->update('category',$category)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function delete_cat($id)
	{
	   if($this->db->delete('category', array('id' => $id)))
	   {
		return true;
	   }
		else 
	   {
		return false;
	   }
	}
	function getadmindata()
	{
		$id = $this->session->userdata('id');
		 $this->db->select('*');
		 $this->db->where('id',$id);
		 $q = $this->db->get('users');
		
			if($q->num_rows() > 0) 
			{
			
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
					
				return $data;
			}
	}
	public function Update_setting($user_data)
	{
		
		$id = $this->session->userdata('id');
		$this->db->where('id', $id);
		
		
		if($this->db->update('users',$user_data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function add_video($video_data)
	{
		
		$this->db->insert('video',$video_data); 
		return true;  
	}
	function Allvideo($start)
	{
		$cnd = "where videoname != '' order by `id` DESC LIMIT ".$start.",10 ";
		$sql = "SELECT * FROM `video` $cnd ";
		$q = $this->db->query($sql);

		/* $this->db->select('*');
		 $q = $this->db->get('video');*/
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function getvideoById($id)
	{
		 $this->db->select('*');
		  $this->db->where('id',$id);
		 $q = $this->db->get('video');
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function update_video($video_data,$id)
	{
		
		
		$this->db->where('id', $id);
		if($this->db->update('video',$video_data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function del_video($id)
	{
	$video_data= array('name' => '','video_img' => '');
	   $this->db->where('id', $id);
		if($this->db->update('video',$video_data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function delete_video($id)
	{
	   if($this->db->delete('video', array('id' => $id)))
	   {
		return true;
	   }
		else 
	   {
		return false;
	   }
	}
	function show_AllcategoriesById()
	{
		
		 $this->db->select('*');
		 $q = $this->db->get('category');
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
					if($row->category_parent != 0)
					{
					 $this->db->where('id',$row->category_parent);
					 $this->db->select('category_title');
					 $qry = $this->db->get('category');
						foreach (($qry ->result()) as $r)
						{
							$par= $r->category_title;
						}
					$row->parent = $par;
					}
					
				}
				//print_r($data);
				return $data;
			}
	}
		
function menu()
	{
		$menus = $this->db->get_where('category', array('category_parent'=>0))->result();
		$data = array();
		foreach($menus as $menu)
		{
			 $submenu = $this->db->get_where('category',array('category_parent'=>$menu->id));
			 if($submenu->num_rows()>0)
				 $menu->submenu = $submenu->result();
			 else
				 $menu->submenu = array();
	
			$data[] = $menu;
			
		}
		return $data;
	}
	public function add_feature($feature)
	{
		
		$this->db->insert('videofeature',$feature); 
		return true;  
	}
	function getvideoFeatures()
	{
		 $this->db->select('*');
		 $q = $this->db->get('videofeature');
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function getvideoFeaturesById($id)
	{
		 $this->db->select('*');
		 $this->db->where('id',$id);
		 $q = $this->db->get('videofeature');
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function delete_feature($id)
	{
	   if($this->db->delete('videofeature', array('id' => $id)))
	   {
		return true;
	   }
		else 
	   {
		return false;
	   }
	}
	public function update_feature($feature,$id)
	{
	
		$this->db->where('id', $id);
		if($this->db->update('videofeature',$feature)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function insertabout($contentdata,$id)
	{
		$this->db->where('id', $id);
		if($this->db->update('about',$contentdata)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	function getAbout()
	{
		 $this->db->select('*');
		 $q = $this->db->get('about',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function insertTerms($data,$id)
	{
		
		$this->db->where('id', $id);
		if($this->db->update('terms&conditions',$data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	function getterms()
	{
		 $this->db->select('*');
		 $q = $this->db->get('terms&conditions',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function getgattingstart()
	{
		 $this->db->select('*');
		 $q = $this->db->get('gettingstart',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function gettingstart($data,$id)
	{
		
		$this->db->where('id', $id);
		if($this->db->update('gettingstart',$data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	function getcontactus()
	{
		 $this->db->select('*');
		 $q = $this->db->get('contactus',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function insertcontact($data,$id)
	{
		
		$this->db->where('id', $id);
		if($this->db->update('contactus',$data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	function getCatsold()
	{
	//echo '<pre>';
		$this->db->select('*');
		
		 $q = $this->db->get('category');
		 
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
					
					 $this->db->where('id',$row->category_parent);
					 $this->db->select('*');
					 $qry = $this->db->get('category');
						foreach (($qry ->result()) as $r)
						{
						
							$par= $r->category_title;
							$p2 = $r->category_parent;
							
							$this->db->where('id',$p2);
							$this->db->select('*');
						 	$qry2 = $this->db->get('category');
							foreach(($qry2 ->result()) as $r2 )
							{
								 $par2 = $r2->category_title;
								 $p3 = $r2->category_parent;
								 $this->db->where('id',$p3);
								 $this->db->select('*');
								 $qry3 = $this->db->get('category');
								 foreach(($qry3 ->result()) as $r3 )
								 {
								 	$par3 = $r3->category_title;
									$p4 = $r3->category_parent;
									$this->db->where('id',$p4);
									$this->db->select('*');
									$qry4 = $this->db->get('category');
									foreach(($qry4 ->result()) as $r4 )
									 {
									 	$par4 = $r4->category_title;
									 }
								 }
								 
							}
							
						}
								$row->parent4 = $par4;
							$row->parent3 = $par3;
						$row->parent2 = $par2;
					$row->parent = $par;
					
				}
				//print_r($data);die;
				return $data;
			}
	
  
    }
	
	
	 public function getCats($start)
	{
	 $cnd = "WHERE id NOT IN (SELECT category_parent FROM category) ORDER BY id DESC LIMIT ".$start.",10 ";
	//$sql = "SELECT * FROM category WHERE id NOT IN (SELECT category_parent FROM category) ORDER BY id desc";
	$sql = "SELECT * FROM `category` $cnd ";
	$qry = $this->db->query($sql);
    
				 {
				 	foreach(($qry->result()) as $key => $res)
					{
					 $pid = $res->category_parent;
					 $resdata[] = $res;
					 $vcat = $res->category_title;
					}	
				 }
				$catdata[] = $resdata;
			 $dataqqq = $catdata[0];
			
			 
			
			 foreach($catdata[0] as $key => $subdata)
			 {
			 	$id = $subdata->category_parent;
				$pcat = $subdata->category_title;
				$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
				$dataqqq[$key]->sub = $res;
				
				
				$id1 = $res['category_parent'];
				$pcat2 = $res['category_title'];
				
				$res1 =  $this->db->get_where('category',array('id'=>$id1))->row_array();
				$dataqqq[$key]->sub['child'] = $res1;
				
				$id2 = $res1['category_parent'];
				$pcat3 = $res1['category_title'];
				$res2 =  $this->db->get_where('category',array('id'=>$id2))->row_array();
				$dataqqq[$key]->sub['child']['child2'] = $res2;
				
				$id3 = $res2['category_parent'];
				$res3 =  $this->db->get_where('category',array('id'=>$id3))->row_array();
				$dataqqq[$key]->sub['child']['child2']['child3'] = $res3;
				
				
				
				
			 }
			 
			 return $dataqqq;
			
			
	}        
		
		
		
			
}


?>