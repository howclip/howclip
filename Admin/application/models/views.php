<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class views extends CI_Model 
{
	
	function __construct()
    {
        parent::__construct();
    }
	
	public function getAcademicYearList($id='',$status='') 	
	{
	  	echo 'yes';die;
		
		if($status=='archived')
		{
			$s_cnd ='1';
		}
		elseif($status=='active' || $status=='')
		{
			$status='active';
			$s_cnd ='0';
		}
		elseif($status=='all')
		{
			$status ='';
		}
		
		if($id){ $this->db->where('id',$id);}
		if($status){$this->db->where('achive_status' , $s_cnd );}
		$this->db->order_by('sort_order','asc');
        $q = $this->db->get('academic_year');
     	
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function getStandardList($id='',$status='')
	{
		if($status=='archived')
		{
			$s_cnd ='1';
		}
		elseif($status=='active' || $status=='')
		{
			$status='active';
			$s_cnd ='0';
		}
		elseif($status=='all')
		{
			$status ='';
		}
		
		if($id){ $this->db->where('id',$id);}
		if($status){$this->db->where('status' , $s_cnd );}
		$this->db->order_by('sort_order','asc');
        $q = $this->db->get('standard');
     	
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function getEnquiryList($id='')
	{
		if($id){ $this->db->where('id',$id);}
		$this->db->order_by('id','desc');
        $q = $this->db->get('enquiry_master');
     	
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function getPaymentCategoryList($id='')
	{
		if($id){ $this->db->where('id',$id);}
		$this->db->order_by('id','desc');
        $q = $this->db->get('payment_category');
     	
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function getExpensesList($id='')
	{
		if($id){ $this->db->where('id',$id);}
		$this->db->order_by('id','desc');
        $q = $this->db->get('expenses');
     	
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function getSchoolList($id='')
	{
		if($id){ $this->db->where('id',$id);}
		$this->db->order_by('id','desc');
        $q = $this->db->get('schools');
     	
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	public function getBankList($id='')
	{
		if($id){ $this->db->where('id',$id);}
		$this->db->order_by('id','desc');
        $q = $this->db->get('banks');
     	
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
}