<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud_model extends CI_Model {
	
	function __construct()
    {
        parent::__construct();
    }
	
	function user_details($id='')
	{
	}

	function get_value_by_id($table,$type_id='',$field='')
	{
		return $this->db->get_where($table,array('id'=>$type_id))->row()->$field;	
	}
	
	function value_by_id($table,$cnd='',$type_id='',$field='')
	{
		return $this->db->get_where($table,array($cnd => $type_id))->row()->$field;	
	}
	
	
	function create_log($data)
	{
		$data['timestamp']	=	strtotime(date('Y-m-d').' '.date('H:i:s'));
		$data['ip']			=	$_SERVER["REMOTE_ADDR"];
		$location 			=	new SimpleXMLElement(file_get_contents('http://freegeoip.net/xml/'.$_SERVER["REMOTE_ADDR"]));
		$data['location']	=	$location->City.' , '.$location->CountryName;
		$this->db->insert('log' , $data);
	}
	
	
	function get_system_settings()
	{
		$query	=	$this->db->get('settings' );
		return $query->result_array();
	}
	
	////////BACKUP RESTORE/////////
	function create_backup($type)
	{
		$this->load->dbutil();
		
		
		$options = array(
                'format'      => 'txt',             // gzip, zip, txt
                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
                'newline'     => "\n"               // Newline character used in backup file
              );
		
		 
		if($type == 'all')
		{
			$tables = array('');
			$file_name	=	'system_backup';
		}
		else 
		{
			$tables = array('tables'	=>	array($type));
			$file_name	=	'backup_'.$type;
		}

		$backup =& $this->dbutil->backup(array_merge($options , $tables)); 


		$this->load->helper('download');
		force_download($file_name.'.sql', $backup);
	}
	
	
	/////////RESTORE TOTAL DB/ DB TABLE FROM UPLOADED BACKUP SQL FILE//////////
	function restore_backup()
	{
		move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/backup.sql');
		$this->load->dbutil();
		
		
		$prefs = array(
            'filepath'						=> 'uploads/backup.sql',
			'delete_after_upload'			=> TRUE,
			'delimiter'						=> ';'
        );
		$restore =& $this->dbutil->restore($prefs); 
		unlink($prefs['filepath']);
	}
	
	/////////DELETE DATA FROM TABLES///////////////
	function truncate($type)
	{
		if($type == 'all')
		{
			//give all table list here
			//$this->db->truncate('');
		}
		else
		{	
			$this->db->truncate($type);
		}
	}
	
	
	////////IMAGE URL//////////
	function get_image_url($type = '' , $id = '')
	{
		$dir='';
		if($type)
		{
			$dir = $type.'_image/';	
		}
		if(file_exists('uploads/'.$dir.$id))
			$image_url	=	base_url().'uploads/'.$dir.$id;
		else
			$image_url	=	base_url().'uploads/img_not_found.gif';
			
		return $image_url;
	}
	/*----------------RECURSIVE FOR BRANCHES----------------*/
	function fetchBranchTree($parent='0',$spacing='',$user_tree_array='') 	
	{
	  if (!is_array($user_tree_array))
		$user_tree_array = array();
	  
	  $this->db->select('id, title, parent_branch`');
	  $this->db->from('branches');
	  $this->db->where('parent_branch', $parent);
	  $this->db->order_by("id", "ASC"); 
	  $query = $this->db->get();
	   
	  if ($query->num_rows() > 0) {
		foreach  ($query->result() as $rows) {
		  $user_tree_array[] = array("id" => $rows->id, "title" => $spacing . $rows->title);
		  $user_tree_array = $this->fetchBranchTree($rows->id, $spacing . '> ', $user_tree_array);
		}
	  }
	  return $user_tree_array;
	}

	/*----------------GET COUNTRY LIST----------------*/
	function get_country_List() 	
	{
	  $query	=	$this->db->get('countries');
	  $country = $query->result_array();
	  return $country;
	}
	
	/*----------------GET STATE LIST----------------*/
	function get_State_List($param='') 	
	{
	  $query	=	$this->db->get_where('states' , array('country_id' => $param));
	  $states = $query->result_array();
	  return $states;
	}
	
	/*----------------GET BRANCH LIST----------------*/
	function getBranchtList($id='') 	
	{
	  	$this->db->select("branches.*,states.name as state,countries.name as country");      
		if($id){ $this->db->where('branches.id',$id);}
        $this->db->join('states', 'branches.Province = states.id');
		$this->db->join('countries', 'branches.country_id = countries.id');
        $q = $this->db->get('branches');
     	//  $d= $q->result();
  		 // echo $this->db->last_query();die;
        if($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data[] = $row;
            }
        }
        return $data;
	}
	
	/*----------------GET ALL ROWS LIST----------------*/
	function get_All_List($table='',$cnd='',$param='') 	
	{
	  $query   = $this->db->get_where($table , array($cnd => $param));
	  $results = $query->result_array();
	  return $results;
	}
	
	/*----------------GET ALL ROWS LIST----------------*/
	function get_All_List_orderBy($table='',$cnd='',$param='',$order='',$by='',$limit=NULL) 	
	{
	  if($cnd){$excnd = explode(',',$cnd);}
	  if($param){$exparam = explode(',',$param);}
	  foreach($excnd as $k => $c)
	  {
		$cond[$c] = $exparam[$k];  
	  }
	  if($limit!='')
	  {
	   	$this->db->limit($limit);
	  }
	  $query = $this->db->order_by($order, $by)->get_where($table , $cond);
	  $results = $query->result_array();
	  return $results;
	}
	
	/*----------------GET ALL ROWS LIST----------------*/
	function get_All_table_List($table='') 	
	{
	  $query   = $this->db->get_where($table);
	  $results = $query->result_array();
	  return $results;
	}
	
	/*----------------Count_all_records----------------*/
	function count_all_record($table='') 	
	{
	  $this->db->from($table);
	  $query = $this->db->get();
	  $results = $query->num_rows();
	  return $results;
	}
	
	/*----------------GET ALL HOLIDAY LIST----------------*/
	function get_Holiday_List($start_date='',$end_date='') 	
	{
	  $this->db->select('start_date,end_date');
	  $this->db->from('holidays');
	  $this->db->where("start_date BETWEEN '$start_date' AND '$end_date'");
	  $this->db->where("end_date BETWEEN '$start_date' AND '$end_date'");
	  $result = $this->db->get()->result_array();
	  return $result;
	}
	
	/*----------------Return all dates between two dates----------------*/
	function date_range($first, $last, $step = '+1 day', $output_format = 'Y-m-d' ) 
	{
		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);
	
		while( $current <= $last ) {
	
			$dates[] = date($output_format, $current);
			$current = strtotime($step, $current);
		}
	
		return $dates;
	}
	
	
	
	
	
	
	
	/*----------------GET MAX ID FROM TABLE----------------*/
	function get_Max_ID($table='', $param1='', $param2='') 
	{
		$this->db->select_max($param1, $param2);
		$result = $this->db->get($table)->row();  
		return $result->$param2;
	}
	
	 public function get_required_fields($table='',$list='',$cnd='',$where='')
	 {
		 $fields=implode(',',$list);
		 $this->db->select($fields);
		 $this->db->from($table);
	     $this->db->where("$cnd = '$where'");
		 $results = $this->db->get()->result_array();
		 return $results;
	 }
	 public function getCountryByName($name) 
	 {
	  $this->db->select('*')->from('countries')->like('name', $name, 'both');
	  $q = $this->db->get();
	  if($q->num_rows() > 0) {
	   return $q->row();
	   }    
	   return FALSE;
	  }
	 public function getStateByName($name) 
	 {
	  $this->db->select('*')->from('states')->like('name', $name, 'both');
	  $q = $this->db->get();
	  if($q->num_rows() > 0) {
	   return $q->row();
	   }    
	   return FALSE;
	  }
	 public function check_permission($param='')
	 {
		$perLogin=$this->db->get_where('permissions',array('id' => $this->session->userdata('permission_id')))->row()->permission;
		$login_per=explode(',',$perLogin);
		if(in_array($param,$login_per))
			 {
				return $login_per;
			 }
			 else
			 {
		    return 0;
			 }
	 }
}

