<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_model extends CI_Model 
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
        $this->load->library('session'); 
		 
	}
				
public function message($succ,$fail)
{
	if($succ)
	{
				echo'<div class="alert alert-block alert-success fade in">';
				echo '<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>';
				echo'<p><h4> Successful!</h4>'.$succ.'</p>';
				echo'</div>';
	}
	if($fail)
	{
			
				echo'<div class="alert alert-block alert-danger fade in">';
				echo'<a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>';
				echo'<p><h4><i class="fa fa-exclamation-circle"></i> Warning</h4>'.$fail.'</p>';
				echo'</div>';
	}							
			
				    				
}
public function fetchCategoryTree($parent = 0, $spacing = '', $user_tree_array = '') {

  if (!is_array($user_tree_array))
    $user_tree_array = array();

  $sql = "SELECT `id`, `category_title`, `category_parent` FROM `category` WHERE 1 AND `category_parent` = $parent ORDER BY id ASC";
  $query = mysql_query($sql);
  if (mysql_num_rows($query) > 0) {
    while ($row = mysql_fetch_object($query)) {
      $user_tree_array[] = array("id" => $row->id, "category_title" => $spacing . $row->category_title);
      $user_tree_array = $this->fetchCategoryTree($row->id, $spacing . '&nbsp;&nbsp;', $user_tree_array);
    }
  }
  return $user_tree_array;
}

public function fetchCategoryTreeList($parent = 0, $user_tree_array = '') {

    if (!is_array($user_tree_array))
    $user_tree_array = array();

  $sql = "SELECT `id`, `category_title`, `category_parent` FROM `category` WHERE 1 AND `category_parent` = $parent ORDER BY id ASC";
  $query = mysql_query($sql);
  if (mysql_num_rows($query) > 0) {
     $user_tree_array[] = "<ul>";
    while ($row = mysql_fetch_object($query)) {
	  $user_tree_array[] = "<li>". $row->category_title."</li>";
      $user_tree_array = $this->fetchCategoryTreeList($row->cid, $user_tree_array);
    }
	$user_tree_array[] = "</ul>";
  }
  return $user_tree_array;
}	

	
public function getsetting($id)
{
	   $q = $this->db->get_where("settings" , array('created' => $id, 'company_id' =>$this->session->userdata('company_id')));
		if($q->num_rows() > 0)
		{
			foreach (($q->result_array()) as $row)
			 {
				$data = $row;
			 }
				
			return $data;
		}
}	


	function show_categories()
	{
	
		 $this->db->select('*');
		 $q = $this->db->get('category');
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function show_categoriesById($id)
	{
		$this->db->where('id',$id);
		 $this->db->select('*');
		 $q = $this->db->get('category');
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
					
					 $this->db->where('id',$row->category_parent);
					 $this->db->select('category_title');
					 $qry = $this->db->get('category');
						foreach (($qry ->result()) as $r)
						{
						$par= $r->category_title;
						}
					$row->parent = $par;
					
				}
				//print_r($data);
				return $data;
			}
	}
	public function add_category($category)
	{
		
		$this->db->insert('category',$category); 
		return true;  
	}
	public function update_category($category,$id)
	{
	
		$this->db->where('id', $id);
		if($this->db->update('category',$category)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function delete_cat($id)
	{
	   if($this->db->delete('category', array('id' => $id)))
	   {
		return true;
	   }
		else 
	   {
		return false;
	   }
	}
	function getadmindata()
	{
		$id = $this->session->userdata('id');
		 $this->db->select('*');
		 $this->db->where('id',$id);
		 $q = $this->db->get('users');
		
			if($q->num_rows() > 0) 
			{
			
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
					
				return $data;
			}
	}
	public function Update_setting($user_data)
	{
		
		$id = $this->session->userdata('id');
		$this->db->where('id', $id);
		
		
		if($this->db->update('users',$user_data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function add_video($video_data)
	{
		
		$this->db->insert('video',$video_data); 
		return true;  
	}
	function Allvideo()
	{
		
		 $this->db->select('*');
		 $q = $this->db->get('video');
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function getvideoById($id)
	{
		 $this->db->select('*');
		  $this->db->where('id',$id);
		 $q = $this->db->get('video');
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function update_video($video_data,$id)
	{
		
		
		$this->db->where('id', $id);
		if($this->db->update('video',$video_data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function del_video($id)
	{
	$video_data= array('name' => '','video_img' => '');
	   $this->db->where('id', $id);
		if($this->db->update('video',$video_data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function delete_video($id)
	{
	   if($this->db->delete('video', array('id' => $id)))
	   {
		return true;
	   }
		else 
	   {
		return false;
	   }
	}
	function show_AllcategoriesById()
	{
		
		 $this->db->select('*');
		 $q = $this->db->get('category');
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
					if($row->category_parent != 0)
					{
					 $this->db->where('id',$row->category_parent);
					 $this->db->select('*');
					 $qry = $this->db->get('category');
						foreach (($qry ->result()) as $r)
						{
							$par = $r->category_title;
							 $parid = $r->id;
							 $this->db->where('id',$parid);
							 $this->db->select('*');
							 $qrty = $this->db->get('category');
							 foreach($qrty as $qq)
							 {
							 $par= $r->category_title;
							 $parid = $r->id;
							 }
							
							
							
							
							
						}
						
						
						
						
					$row->parent = $par;
					}
					
				}
				//print_r($data);die;
				return $data;
			}
	}
		
	function getCats()
	{
		$this->db->select('*');
		$this->db->where('category_parent',0);
		$q = $this->db->get('category');
		if($q->num_rows() >0)
		{
			foreach(($q->result()) as $row)
			{
			
			 $row->category_title;
			 $pid = $row->id;
			 $this->db->select('*');
			 $this->db->where('category_parent',$pid);
			 $qry = $this->db->get('category');
				 if($qry->num_rows() > 0)
				 {
				 	foreach(($qry->result()) as $res)
					{
						 $res->category_title;
						 $resdata[] = $res;
						 $subid = $res->id;
						 $this->db->select('*');
						 $this->db->where('category_parent',$subid);
						 $q1 = $this->db->get('category');
						 if($q1->num_rows() > 0)
						 {
						 	foreach(($q1->result()) as $subdata)
							{
								
								$subsubdata[] = $subdata;
								$this->db->select('*');
								$this->db->where('category_parent',$subdata->id);
								$q2 = $this->db->get('category');
								if($q2->num_rows() > 0)
								{
									foreach(($q2->result()) as $sub2data)
									{
									
										$sub2d[] = $sub2data;
										$this->db->select('*');
										$this->db->where('category_parent',$sub2data->id);
										$q3 = $this->db->get('category');
										if($q3->num_rows() > 0)
										{
											foreach(($q3->result()) as $sub3data)
											{
												$sub3data->id;
												$sub4data[] = $sub3data;
												$this->db->select('*');
												$this->db->where('category_parent',$sub3data->id);
												$q4 = $this->db->get('category');
												if($q4->num_rows() > 0)
												{
													foreach(($q4->result()) as $sub5data)
													{
														$sub6data[] = $sub5data;
													}
													$sub3data->sub5 = $sub6data;
												}
											}
											$sub2data->sub4 = $sub4data;
										}
										
									}
									$subdata->sub3 = $sub2d;
								}
							}
						 }
						 $res->sub2 = $subsubdata;
					}
					
				 }
			 $row->sub = $resdata;
			 $catdata[] = $row;
			}
			
			return $catdata;
		}
	
	
  
    }
		
		
		
		
function menu()
	{
		$menus = $this->db->get_where('category', array('category_parent'=>0))->result();
		$data = array();
		foreach($menus as $menu)
		{
			 $submenu = $this->db->get_where('category',array('category_parent'=>$menu->id));
			 if($submenu->num_rows()>0)
				 $menu->submenu = $submenu->result();
			 else
				 $menu->submenu = array();
	
			$data[] = $menu;
			
		}
		return $data;
	}
	public function add_feature($feature)
	{
		
		$this->db->insert('videofeature',$feature); 
		return true;  
	}
	function getvideoFeatures()
	{
		 $this->db->select('*');
		 $q = $this->db->get('videofeature');
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function getvideoFeaturesById($id)
	{
		 $this->db->select('*');
		 $this->db->where('id',$id);
		 $q = $this->db->get('videofeature');
		
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function delete_feature($id)
	{
	   if($this->db->delete('videofeature', array('id' => $id)))
	   {
		return true;
	   }
		else 
	   {
		return false;
	   }
	}
	public function update_feature($feature,$id)
	{
	
		$this->db->where('id', $id);
		if($this->db->update('videofeature',$feature)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	public function insertabout($contentdata,$id)
	{
		$this->db->where('id', $id);
		if($this->db->update('about',$contentdata)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	function getAbout()
	{
		 $this->db->select('*');
		 $q = $this->db->get('about',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function insertTerms($data,$id)
	{
		
		$this->db->where('id', $id);
		if($this->db->update('terms&conditions',$data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	function getterms()
	{
		 $this->db->select('*');
		 $q = $this->db->get('terms&conditions',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	function getgattingstart()
	{
		 $this->db->select('*');
		 $q = $this->db->get('gettingstart',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function gettingstart($data,$id)
	{
		
		$this->db->where('id', $id);
		if($this->db->update('gettingstart',$data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	function getcontactus()
	{
		 $this->db->select('*');
		 $q = $this->db->get('contactus',1);
			if($q->num_rows() > 0) 
			{
				foreach (($q->result()) as $row)
				{
					$data[] = $row;
				}
				return $data;
			}
	}
	public function insertcontact($data,$id)
	{
		
		$this->db->where('id', $id);
		if($this->db->update('contactus',$data)) 
		{
		
          return true;
		} 
		else 
		{
			return false;
		}
	}
	
		
			
}


?>