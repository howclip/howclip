<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class welcome extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('text');
        $this->load->database();
        $this->load->model('master_model');
        $this->load->library('session');
        $this->load->library('email');
        $this->load->library('pagination');
        ini_set('post_max_size', '1024M');
        ini_set('upload_max_filesize', '1024M');
        ini_set('max_execution_time', 18000);
        ini_set('max_input_time', 18000);
        ini_set('memory_limit', '128M');

    }

    public function index($offset = 0) {


        $this->welcome();
    }

    public function welcome() { 
        $uid = $this->session->userdata('id');
        if ($uid != "") {
            $this->db->select('*');
            $this->db->where('userId', $uid);
            $this->db->where('videoname', "");

            $q = $this->db->get('video');

            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $name = $row->name;
                    $name;
                    $path = ('uploads/live/' . $name);
                    unlink($path);
                    $this->db->delete('video', array('id' => $row->id));
                }
            }
        }
        $data['playlist'] = $this->master_model->getAllPlaylistlimited();
        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['categories'] = $this->master_model->getCategoryForParentId();
        $data['feature'] = $this->master_model->videoByFeature();
        $data['features'] = $this->master_model->getactivevideofeature();
        $data['companydetail'] = $this->master_model->getcompanydetail();
       // $this->load->view('againlogin', $data);
        $this->load->view('login', $data);
    }

    public function test() {
       
        $this->load->view('test', $data);
    }

    public function gmail() {

        $this->load->view('gmaillogin');
    }

    public function register() {

        $this->form_validation->set_rules('fname', 'First Name is', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name is', 'trim|required');
        $this->form_validation->set_rules('email', 'Email is', 'trim|required');
        $this->form_validation->set_rules('password', 'Password is', 'trim|required');

        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $username = $fname . "  " . $lname;
        $password = md5($this->input->post('password'));


        $email = $this->input->post('email');


        $this->db->where('email', $email);
        $this->db->select('*');
        $q = $this->db->get('users');

        if ($q->num_rows() > 0) {

            $this->session->set_flashdata('regerr', 'Email already exists !');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {


            $data = array(
                'Firstname' => $fname,
                'Lastname' => $lname,
                'email' => $email,
                'password' => $password,
                'username' => $username
            );
            if ($this->form_validation->run() == FALSE) {

                redirect('welcome/welcome', 'refresh');
            } else {
                if ($this->master_model->register($data)) {

                    $insert_id = $this->db->insert_id();
                    $userrecord = array(
                        'name' => $username,
                        'email' => $email,
                        'id' => $insert_id,
                    );

                    $this->session->set_userdata($userrecord);
                    $this->session->set_flashdata('smsg', 'Registered Successfully');
                    redirect('home/dash', 'refresh');
                }
            }
        }
    }

    public function dash() {

        $id = $this->session->userdata('id');
        $data['userdata'] = $this->master_model->getdataforuser();
        $data['userDetail'] = $this->master_model->getUserbyId($id);
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        //echo $this->session->userdata('name');die;
        if ($this->session->userdata('id') != "") {

            $this->load->view('dashboad', $data);
        } else {
            redirect('welcome/welcome', 'refresh');
        }
    }

    public function loginpage($par1 = "") {

        if ($par1 != "") {
            $subarray = array('url' => "showvideo/$par1");
            $this->session->set_userdata($subarray);
        }
        if ($par1 == "upload") {
            //echo "abc";die;
            $subarray = array('url' => "uploadpage");
            $this->session->set_userdata($subarray);
            redirect('home/uploadpage', 'refresh');
        }

        if ($this->session->userdata('email') == '') {
            $this->load->view('signin');
        } else {
            redirect('home/dash', 'refresh');
        }
    }

    public function uploadpage() {

//       echo exec('/usr/bin/ffmpeg -i /var/www/html/uploads/videos/19336.mp4 /var/www/html/uploads/videos/19336.avi');


        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['categories'] = $this->master_model->show_categories();
        $uid = $this->session->userdata('id');
        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $data['catmenu'] = $this->master_model->getCategoryForParentId();
        $this->load->view('homepage', $data);
    }

    public function edittest() {
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['categories'] = $this->master_model->show_categories();
        $uid = $this->session->userdata('id');
        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $data['catmenu'] = $this->master_model->getCategoryForParentId();
        $this->load->view('edittest', $data);
    }

    public function login() {

        $pass = md5($this->input->post('password'));
        $data = array(
            'email' => $this->input->post('email'),
            'password' => $pass
        );


        $selectquery = $this->db->get_where('users', array('email' => $this->input->post('email'), 'password' => $pass));


        if ($selectquery->num_rows()) {

            $row = $selectquery->result_array();

            $userrecord = array(
                'name' => $row[0]['username'],
                'email' => $row[0]['email'],
                'id' => $row[0]['id'],
                'role' => $row[0]['role'],
                'userlogo' => $row[0]['userLogo'],
            );

            $this->session->set_userdata($userrecord);
            $this->session->set_flashdata('smsg', 'Login Successfully');
            if ($this->session->userdata('url') != "") {
                $reurl = $this->session->userdata('url');

                redirect('home/' . $reurl, 'refresh');
            } else {
                echo "success";
                die;
                redirect('home/welcome', 'refresh');
            }
        } else {
            echo "Invalid Login Details";
            $this->session->set_flashdata('err', 'Invalid Login Details');
            "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function search() {
        $search_data = $this->input->post('search_data');

        $result = $this->master_model->get_autocomplete($search_data);

        if (!empty($result)) {
            foreach ($result as $row):

                echo "<div style='margin-top:15px;padding:0px 10px;'><a href='".base_url()."index.php/welcome/showvideo/$row->id' style='color:#000;font-size:13px;font-weight:bold'>" . ucwords($row->videoname) . "</a></div>";

            endforeach;
        }
        else {
            echo "<div style='margin-top:15px;padding:0px 10px;'><a href='#' style='color:#000;font-size:13px;font-weight:bold'>" . ucwords("Results Not Found!") . "</a></div>";
        }
    }

    public function searchAlllist() {

        $search_data = $this->input->post('search_data');

        $result = $this->master_model->get_autocompletedatalist($search_data);

        if (!empty($result)) {

            foreach ($result as $row):

                echo "<div style='margin-top:15px;padding:0px 10px;'><a href='".base_url()."index.php/home/searchplaylist/$row->playlist_name' style='color:#000;font-size:13px;font-weight:bold'>" . ucwords($row->playlist_name) . "</a></div>";


            endforeach;
        }
        else {
            echo "<div style='margin-top:15px;padding:0px 10px;'><a href='#' style='color:#000;font-size:13px;font-weight:bold'>" . ucwords("Results Not Found!") . "</a></div>";
        }
    }

    public function searchAll() {

        $search_data = $this->input->post('search_data');

        $result = $this->master_model->get_autocompletedata($search_data);

        if (!empty($result)) {
            foreach ($result as $row):
                if ($this->session->userdata('id') == '') {
                    echo "<div style='margin-top:15px;padding:0px 10px;'><a href='".base_url()."index.php/welcome/getsearchdata/$row->videoname' style='color:#000;font-size:13px;font-weight:bold'>" . ucwords($row->videoname) . "</a></div>";
                } else {
                    echo "<div style='margin-top:15px;padding:0px 10px;'><a href='".base_url()."index.php/welcome/getsearchdata/$row->videoname' style='color:#000;font-size:13px;font-weight:bold'>" . ucwords($row->videoname) . "</a></div>";
                }

            endforeach;
        } else {
            echo "<div style='margin-top:15px;padding:0px 10px;'><a href='#' style='color:#000;font-size:13px;font-weight:bold'>" . ucwords("Results Not Found!") . "</a></div>";
        }
    }

    public function signup() {
        $data['companydetail'] = $this->master_model->getcompanydetail();
        //echo '<pre>'; print_r( $data['companydetail']);die
        $this->load->view('signup', $data);
    }

    public function registeruser() {

        $this->form_validation->set_rules('fname', 'First Name is', 'trim|required');
        $this->form_validation->set_rules('lname', 'Last Name is', 'trim|required');
        $this->form_validation->set_rules('email', 'Email is', 'trim|required');
        $this->form_validation->set_rules('password', 'Password is', 'trim|required');
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $username = $fname . " " . $lname;
        $password = md5($this->input->post('password'));
        $email = $this->input->post('email');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
        $year = $this->input->post('year');
        $gender = $this->input->post('gender');
        $code = $this->input->post('code');
        $mobile = $this->input->post('mobile');
        $curremail = $this->input->post('curremail');
        $location = $this->input->post('location');
        $dob = $year . $month . $day;
        $phn = $code . "," . $mobile;

        $this->db->where('email', $email);
        $this->db->select('*');
        $q = $this->db->get('users');

        if ($q->num_rows() > 0) {

            $this->session->set_flashdata('regerr', 'Email already exists !');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {
            $data = array(
                'Firstname' => $fname,
                'Lastname' => $lname,
                'username' => $username,
                'email' => $email,
                'password' => $password,
                'dob' => $dob,
                'gender' => $gender,
                'mobileNo' => $mobile,
                'currentEmail' => $curremail,
                'location' => $location
            );

            if ($this->form_validation->run() == FALSE) {

                redirect('welcome/welcome', 'refresh');
            } else {
                if ($this->master_model->register($data)) {
                    $insert_id = $this->db->insert_id();
                    $userrecord = array(
                        'name' => $username,
                        'email' => $email,
                        'id' => $insert_id,
                    );

                    $this->session->set_userdata($userrecord);


                    $this->session->set_flashdata('smsg', 'Registered Successfully');
                    redirect('home/dash', 'refresh');
                }
            }
        }
    }

    public function about() {

        $data['aboutContent'] = $this->master_model->getAbout();
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('aboutus', $data);
    }

    public function sizeinput($input, $len) {
        (int) $len;
        (string) $input;
        $n = substr($input, 0, $len);
        $ret = trim($n);
        $out = htmlentities($ret, ENT_QUOTES);
        return $out;
    }

    public function upload() {


        $this->form_validation->set_rules('file1', $this->lang->line("file1"), 'required|xss_clean');
        $id = $this->session->userdata('id');

        if (array_key_exists("file1", $_FILES)) {

            $uploaddir = 'uploads/videos/';
            $live_dir = 'uploads/live/';
            $live_img = 'uploads/images/';
            $clipdir = 'uploads/clips/';
            $interval = 2;
            $size = '320x240';
            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $seed = rand(1, 2009) * rand(1, 10);
            $_FILES['file1']['name'];
            $filename = explode('.', $_FILES['file1']['name']);
            $filename[0];
            $filename[1];
            if (in_array("avi", $filename)) {
                $filename = $random . ".avi";
            } else if (in_array("mp4", $filename)) {
                $filename = $random . ".mp4";
            } else if (in_array("wmv", $filename)) {
                $filename = $random . ".wmv";
            } else {
                die;
            }
            $upload = basename($filename);

            $filename = str_replace(' ', '_', $upload);


            $uploadfile = $uploaddir . $filename;
            $deletevid = $_FILES['file1']['name'];


            $vid_usr_ip = $_SERVER['REMOTE_ADDR'];
            $safe_file = $this->checkfile($_FILES['file1']);
            if ($safe_file['safe'] == 1) {

                if (move_uploaded_file($_FILES['file1']['tmp_name'], $uploadfile)) {

                    // echo "File is valid, and was .<br/>";
                    $base = basename($uploadfile, $safe_file['ext']);

                    $new_file = $base . 'mp4';
                    $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                    $new_image = $random . '.jpg';
                    //$base2 = trim($base, ".");
                    //$new_image2 = $base2.'2'.'.jpg';
                    //$new_image3 = $base2.'3'.'.jpg';

                    $new_image_path = $live_img . $new_image;
                    //$new_image_path2 = $live_img.$new_image2;
                    //$new_image_path3 = $live_img.$new_image3;

                    $new_flv = $live_dir . $new_file;
                    $new_clip = $clipdir . $new_file;
                    $interval = 2;
                    $ab = "successfully uploaded||$new_image||$base||";
                    echo $ab;
                    exec("/usr/bin/ffmpeg -ss 00:00:02 -i $uploadfile  $new_image_path");
                    $size = '320x240';
                    //exec("/usr/bin/ffmpeg -i $uploadfile -deinterlace -an -ss 1 -t 00:00:01 -r 1 -y -vcodec mjpeg -f mjpeg $new_image_path 2>&1");
                    //exec("/usr/bin/ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default $new_flv");
                    //exec("/usr/bin/ffmpeg -i $uploadfile -vcodec libx264 -vpre lossless_slow -crf 25 -acodec libfaac -threads 0 -t 60 $new_flv");
                    //exec("ffmpeg -i $uploadfile -c:v libx264 -crf 19 -preset slow -c:a libfdk_aac -b:a 192k -ac 2 $uploadfile");
                    exec("/usr/bin/ffmpeg -i $uploadfile -c:v libx264 -crf 19 -strict experimental $new_flv");

                    //exec("/usr/bin/ffmpeg -i $uploadfile -c:v libvpx -pass 1 -an -f rawvideo -y /dev/null");
                    //exec("/usr/bin/ffmpeg -i $uploadfile -c:v libvpx -pass 2 -f webm -b:v 400K -crf 10 -an -y $new_flv");
                    ////exec('/usr/bin/ffmpeg -i ' . $uploadfile . ' ' . $new_flv . '');
                    //exec("ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default -ss 00:00:04 -t 00:00:10 $new_clip");
                    //exec("ffmpeg -i $uploadfile -ss 00:00:02 $new_image_path");
                    //exec("ffmpeg -i $uploadfile -ss 00:00:10 $new_image_path2");
                    //exec("ffmpeg -i $uploadfile -ss 00:00:15 $new_image_path3");



                    $result = shell_exec('/usr/bin/ffmpeg -i ' . escapeshellcmd($uploadfile) . ' 2>&1');
                    preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result, $match);
                    $duration = $match[1];
                    $time = date("h:i:sa");
                    $name = $this->input->post('name');
                    $privacy = $this->input->post('privacy');
                    $date = date("Y-m-d");
                    $videothumb = $new_image;
                    $video_data = array(
                        'Date' => $date,
                        'name' => $new_file,
                        'video_img' => $videothumb,
                        'privacy' => $privacy,
                        'userId' => $id,
                        'time' => $time,
                        'video_duration' => $duration,
                        'videothumb' => $videothumb
                    );
                    echo $duration . "||";
                    if ($this->db->insert('video', $video_data)) {
                        //echo $this->db->last_query();die;
                        echo $insert_id = $this->db->insert_id();
                        $this->session->set_flashdata('msg', 'Saved Successfully');
                        $vidpath = ('uploads/videos/' . $deletevid);
                        unlink($vidpath);
                    }
                } else {

                    echo "Possible file upload attack!\n";
                }
            } else {

                echo 'Invalid File Type Please Try Again. You file must be of type 
 			 .mpg,.mp4, .avi, .wmv, ';
            }
        } else {
            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function checkfile($input) {

        $ext = array('mpg', 'mp4', 'wmv', 'avi');
        $extfile = substr($input['name'], -4);
        $extfile = explode('.', $extfile);
        $good = array();
        $extfile = $extfile[1];
        if (in_array($extfile, $ext)) {
            $good['safe'] = true;
            $good['ext'] = $extfile;
        } else {
            $good['safe'] = false;
        }
        return $good;
    }

    public function createclips() {

        $live_dir = 'uploads/live/';
        $clipdir = 'uploads/live/';
        $live_img = 'uploads/images/';

        $videoname = $this->input->post('video');
        $clipno = $this->input->post('clipno');
        $clipname = $this->input->post('clipname');
        $clipdesc = $this->input->post('clipdesc');
        $monetize = $this->input->post('monetize');
        $start = $this->input->post('start');
        $end = $this->input->post('end');
        $category = $this->input->post('category');
        $addedcat = $this->input->post('addcat');
        $price = $this->input->post('amount');

        $base = explode('.', $videoname);

        $databaseTimePattern = '/^(0[0-9])|(1[0-2]):[0-5][0-9]:[0-5][0-9]$/'; //Matches times in the form hh:mm:ss

        if (preg_match($databaseTimePattern, $start)) {
            $starttime = $start;
        } else {
            $starttime = gmdate("H:i:s", $start);
        }
        if (preg_match($databaseTimePattern, $end)) {
            $endtime = $end;
        } else {
            $endtime = gmdate("H:i:s", $end);
        }



        $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
        $new_file = $base[0] . $random . ".mp4";
        $new_file;
        $new_image = $base[0] . $random . ".jpg";
        $uploadfile = $live_dir . $videoname;
        $new_clip = $clipdir . $new_file;
        $new_image_path = $live_img . $new_image;

        $this->db->where('id', $category);
        $this->db->select('category_title');
        $q = $this->db->get('category');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data = $row->category_title;
            }
            $data;
        }

        if ($addedcat != "") {
            $par = $category;
            $ccateg = $addedcat;
            $category = array('category_title' => $ccateg,
                'category_parent' => $par,
                'status' => 'pending');
            $this->db->insert('category', $category);
        } else {
            $ccateg = $data;
        }

        exec("/usr/bin/ffmpeg -i $uploadfile -an -ss 00:00:02 -r 1 -vframes 1 -y $new_image_path");

        //exec("/usr/bin/ffmpeg -i $uploadfile -f mp4 -vcodec libx264 -vpre default -ss $starttime -t $endtime $new_clip");
        exec("/usr/bin/ffmpeg  -i $uploadfile -ss $starttime -to $endtime -c:v libx264 -crf 19 -strict experimental $new_clip");
        //exec("/usr/bin/ffmpeg -i $uploadfile -ss $starttime -to $endtime -c copy $new_clip");
        $date = date("Y-m-d");


        $result = shell_exec('/usr/bin/ffmpeg -i ' . escapeshellcmd($new_clip) . ' 2>&1');
        preg_match('/(?<=Duration: )(\d{2}:\d{2}:\d{2})\.\d{2}/', $result, $match);
        echo $duration = $match[1];

        //exec("ffmpeg -ss 00:00:02 -i $uploadfile  $new_image_path");
        $uid = $this->session->userdata('id');

        $video_data = array(
            'Date' => $date,
            'name' => $new_file,
            'video_img' => $new_image,
            'privacy' => 'public',
            'userId' => $uid,
            'video_duration' => $duration,
            'videothumb' => $new_image,
            'videoname' => $clipname,
            'description' => $clipdesc,
            'monetize' => $monetize,
            'video_category' => $ccateg,
            'price' => $price
        );



        if ($this->db->insert('video', $video_data)) {

            $insert_id = $this->db->insert_id();
            $clipno;
            echo $clipno . "||" . $new_image . "||" . $clipname . "||" . $insert_id;
        }
    }

    public function uploadclips($par1 = "") {
        //$par1;
// echo '<pre>';
        $data['categories'] = $this->master_model->show_categories();
        $uid = $this->session->userdata('id');
        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $data['catmenu'] = $this->master_model->getCategoryForParentId();
        $data['videoDetail'] = $this->master_model->getvideoById($par1);
        print_r($data['videoDetail']);
        //die;

        $this->load->view('uploadclips', $data);
    }

    public function updatethumb() {
        $img = $this->input->post('img');
        $video = $this->input->post('video');

        if ($this->master_model->updatethumb($img, $video)) {
            
        }
    }

    public function videoupload() {

        $id = $this->input->post('video_id');
        $video = $this->input->post('name');
        $description = $this->input->post('description');

        $privacy = $this->input->post('privacy');

        $category = $this->input->post('cat_name');
        $addedcat = $this->input->post('addcat');

        $this->db->where('id', $category);
        $this->db->select('category_title');
        $q = $this->db->get('category');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data = $row->category_title;
            }
            $data;
        }
        if ($addedcat != "") {
            $par = $category;
            $ccateg = $addedcat;
            $category = array('category_title' => $ccateg,
                'category_parent' => $par,
                'status' => 'pending');
            $this->db->insert('category', $category);
        } else {
            $ccateg = $data;
        }
        $time = date("h:i:sa");

        $duration;
        $date = date("Y-m-d");

        $clipdata = array(
            'videoname' => $video,
            //'video_category' => $ccateg,
            'description' => $description,
            'privacy' => $privacy,
        );

        if ($ccateg != "") {
            $clipdata['video_category'] = $ccateg;
        }
        $this->db->where('id', $id);
        $this->db->update('video', $clipdata);
    }

    public function terms() {
        $data['aboutContent'] = $this->master_model->getAbout();
        $data['TermsContent'] = $this->master_model->getterms();
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['page_title'] = "Terms & Conditions";
        $this->load->view('terms', $data);
    }

    public function fblog() {

        $data['page_title'] = "facebook Login";
        $this->load->view('fblogin', $data);
    }

    public function insertfbdata() {
        $email = $this->input->post('email');
        $user = $this->input->post('user');
       
        

        $selectquery = $this->db->get_where('users', array('email' => $email));
       
        if ($selectquery->num_rows()>0) {
            $row = $selectquery->result_array();
            $userrecord = array(
                'name' => $row[0]['username'],
                'email' => $row[0]['email'],
                'id' => $row[0]['id'],
                'role' => $row[0]['role'],
            );

            $this->session->set_userdata($userrecord);
            $this->session->set_flashdata('smsg', 'Login Successfully');
         
//print_r( $this->session->all_userdata());
            //redirect('home/dash', 'refresh');
        } else {

             $pass =$this->generatePassword();
           
            $data = array('email' => $email, 'password' => $pass, 'username' => $user);
           $mail = $this->sendmail();
            if ($this->db->insert('users', $data)) {
                $insert_id = $this->db->insert_id();
                 $userrecord = array(
                'name' => $user,
                'email' => $email,
                'id' => $insert_id,
                
            );

            $this->session->set_userdata($userrecord);
          
//print_r( $this->session->all_userdata());
                //redirect('home/welcome', 'refresh');
            }
        }
    }

    public function sendmail() {
        $email ="anupama1@livesoftwaresolution.com";
        $user ="anupama";
         set_time_limit(0);
                    $type = '';
                    $to = array($email => $user);
                    $from = array('anupama1@livesoftwaresolution.com' => 'LiveSoft');
                    $subject = 'Event Assign';
                    $body = 'You assign to an event.
                    Event Name:' . $data['crm_event_name'] . '
              ';
                    $this->load->library('SWpt');
                    $mailer = new Swift_Mailer(new Swift_MailTransport());



                    $smpt = 'mail.livesoftwaresolution.com';
                    $port = '25';
                    $username = 'anupama1@livesoftwaresolution.com';
                    $password = 'anupama@123';

                    @$transport = Swift_SmtpTransport::newInstance($smpt, $port)
                            ->setUsername($username)
                            ->setPassword($password);
                    $mailer = Swift_Mailer::newInstance($transport);

                    $to = $to;
                    $from = $from;
                    $subject = $subject;
                    $body = stripslashes($body);

                    @$message = Swift_Message::newInstance()
                            ->setSubject($subject)
                            ->setFrom($from)
                            ->setTo($to)
                            ->setContentType("text/html; charset=UTF-8")
                            ->setBody($body, 'text/html');
                    $mailer->send($message);
                     if ($mailer->send($message, $failures))
          {
          echo 'Message successfully sent!';
          } else {
        echo "There was an error:\n";
 print_r($failures);
          }

//        $header = "From: info@livesoftwaresolution.info";
//        $header.= "MIME-Version: 1.0\r\n";
//        $header.= "Content-Type: text/html; charset=ISO-8859-1\r\n";
//        $header.= "X-Priority: 1\r\n";
//        $to = "garima@livesoftwaresolution.com";
//        $subject = "pass";
//        $message = "password";
//        $status = mail($to, $subject, $message, $header);
//
//        if ($status) {
//            echo '<p>Your mail has been sent!</p>';
//        } else {
//            echo '<p>Something went wrong, Please try again!</p>';
//        }
    }

    public function generatePassword($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }

    public function userupdate($par1 = "") {
        $id = $par1;
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['userDetail'] = $this->master_model->getUserbyId($id);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        //echo '<pre>';
        //print_r($data['userDetail']);die;
        $data['page_title'] = "User Details";
        $this->load->view('updateUser', $data);
    }

    public function EditUser($par1 = "") {
        if ($_FILES['image1']['name']) {

            $errors = array();
            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image1']['name'];
            $file_size = $_FILES['image1']['size'];
            $file_tmp = $_FILES['image1']['tmp_name'];
            $file_type = $_FILES['image1']['type'];

            $file_ext = strtolower(end(explode('.', $_FILES['image1']['name'])));
            $expensions = array("jpeg", "jpg", "png");
            if (in_array($file_ext, $expensions) === false) {

                $this->session->set_flashdata('permission_message', 'extension not allowed, please choose a JPEG or PNG file.');
                echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            }

            move_uploaded_file($file_tmp, "uploads/" . $file_name);
            $data['image1'] = $file_name;
        }
        $img = $data['image1'];
        $id = $par1;
        $fname = $this->input->post('fname');
        $lname = $this->input->post('lname');
        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $month = $this->input->post('month');
        $day = $this->input->post('day');
        $year = $this->input->post('year');
        $gender = $this->input->post('gender');
        $code = $this->input->post('code');
        $mobile = $this->input->post('mobile');
        $curremail = $this->input->post('curremail');
        $location = $this->input->post('location');
        $dob = $year . $month . $day;
        $phn = $code . "," . $mobile;
        $history = $this->input->post('history');
        $bio = $this->input->post('bio');
        $strategy = $this->input->post('strategy');
        $motivation = $this->input->post('motivation');
        $philosophy = $this->input->post('philosophy');



        $data = array(
            'Firstname' => $fname,
            'Lastname' => $lname,
            'username' => $username,
            'email' => $email,
            'dob' => $dob,
            'gender' => $gender,
            'mobileNo' => $phn,
            'currentEmail' => $curremail,
            'location' => $location,
            'history' => $history,
            'bio' => $bio,
            'strategy' => $strategy,
            'motivation' => $motivation,
            'philosophy' => $philosophy,
        );
        if ($img != "") {
            $data['userLogo'] = $img;
        }

        if ($this->master_model->updateUser($data, $id)) {
            $this->session->set_flashdata('msg', 'Saved Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {
            $this->session->set_flashdata('err', 'Fails');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function changePass($par1) {
        $id = $par1;
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['userDetail'] = $this->master_model->getUserbyId($id);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $this->load->view('changePass', $data);
    }

    public function changepassword($par1 = "") {
        $id = $par1;

        $curr = md5($this->input->post('password'));


        $a = $data1['users'] = $this->db->get_where('users', array('id' => $id))->result_array();

        foreach ($a as $value) {
            $pass = $value['password'];
        }

        if ($curr == $pass) {

            $data2['password'] = md5($this->input->post('new_pass'));

            $this->db->where('id', $id);
            $this->db->update('users', $data2);

            $this->session->set_flashdata('flash_message', ' Password Update successully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {

            $this->session->set_flashdata('err', 'Current Password did not match');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function videoDetail($par1 = "") {
        if (isset($_GET['page'])) {
            $start = $_GET['page'] * 10;
        } else {
            $start = 0;
        }

        $id = $this->session->userdata('id');
        $data['playlist'] = $this->master_model->getplaylist($id);
        $data['videoDetail'] = $this->master_model->getvideoByUser($id, $start);
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['userDetail'] = $this->master_model->getUserbyId($id);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $data['page_title'] = "Video Details";
        $this->load->view('videodetail', $data);
    }

    function showvideo($par1 = "", $par2 = "") {
       

        $id = $par1;

        $this->db->select('userId');
        $this->db->where('id', $id);
        $qgv = $this->db->get('video')->result_array();
        $gsuserId = $qgv[0]['userId'];

        $now = new DateTime();
        $date = $now->format('Y-m-d');
        $this->db->select('view');
        $this->db->where('video_id', $id);
        $q = $this->db->get('views');

        if ($this->session->userdata('id') == "") {
            $user = "0";
        } else {
            $user = $this->session->userdata('id');
        }

        if ($q->num_rows() > 0) {
            $viewdata = array('view' => 1,
                'video_id' => $id,
                'Date' => $date,
                'UserId' => $user,
            );
            if ($gsuserId != $this->session->userdata('id')) {
                $this->db->insert('views', $viewdata);
            }
        } else {
            $viewdata = array('view' => 1,
                'video_id' => $id,
                'Date' => $date,
                'UserId' => $user,
            );
            if ($gsuserId != $this->session->userdata('id')) {
                $this->db->insert('views', $viewdata);
            }
        }
        $data['videoDetail'] = $this->master_model->getvideoById($id);
       // echo '<pre>';print_r( $data['videoDetail']);die;
        $data['usersubscription'] = $this->master_model->getuserSubscription();

        $data['Allvideo'] = $this->master_model->Allvideo();
        $data['likes'] = $this->master_model->Alllikes($id);
        $data['Unlikes'] = $this->master_model->Alldislikes($id);
        $data['Allcomment'] = $this->master_model->Allcomments($id);
        $data['Allview'] = $this->master_model->Allviews($id);
        $data['features'] = $this->master_model->getactivevideofeature();
        $data['playlist'] = $this->master_model->getplaylist($user);
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['payment'] = $par2;
        $data['userDetail'] = $this->master_model->getUserbyId($user);


        $data['page_title'] = "Video Details";
        $this->load->view('showvideo', $data);
    }

    public function insertlike() {
        $video = $this->input->post('vid');
        $user = $this->input->post('user');
        $this->master_model->insertlike($video, $user);
    }

    public function insertlikeplay() {
        $list = $this->input->post('vid');
        $user = $this->input->post('user');
        $this->master_model->insertlikeplay($list, $user);
    }

    public function insertdislike() {
        $video = $this->input->post('vid');
        $user = $this->input->post('user');

        $this->master_model->insertdislike($video, $user);
    }

    public function insertdislikeplay() {
        $video = $this->input->post('vid');
        $user = $this->input->post('user');

        $this->master_model->insertdislikeplay($video, $user);
    }

    public function comments() {
        $video = $this->input->post('vid');
        $user = $this->input->post('user');
        $userid = $this->input->post('user_id');
        $uploader = $this->input->post('uploader');
        $list = $this->input->post('list');

        if ($list != "") {
            $play = $list;
        }
        $comment = array('comment' => $this->input->post('com'),
            'user_name' => $user,
            'video_id' => $video,
            'userId' => $userid,
        );
        if ($uploader != "") {
            $comment['uploader'] = $uploader;
        }

        if ($play != "") {
            $comment['playlist'] = $play;
        }

        if ($this->db->insert('comments', $comment)) {
            if ($list == "") {
                $this->master_model->getcomment($video, $userid);
            }
            if ($list != "") {
                $this->master_model->getcommentplay($list, $uploader);
            }
        }
    }

    public function searchpage($par1 = "", $par2 = "") {

        if ($par1 == 'short') {
            $data['videoDetail'] = $this->master_model->getsearchvideoshort($par1);
        }
        if ($par1 == '1day') {

            $data['videoDetail'] = $this->master_model->getsearchvideobeforeday($par1);
        }
        if ($par1 == 'long') {
            $data['videoDetail'] = $this->master_model->getsearchvideolong($par1);
        }

        $term = $par1;
        $short_by = $this->input->post('short_by');
        $filter = $this->input->post('filter');
        $uid = $this->session->userdata('id');

        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['catmenu'] = $this->master_model->getCategoryForParentId();
        $data['categories'] = $this->master_model->getCategoryForParentId();
        $data['returnsearch'] = $this->master_model->getadvancedata($term, $short_by, $filter);
        $data['features'] = $this->master_model->getactivevideofeature();
        $this->load->view('advancesearchresult', $data);
    }

    public function categorysearch($par1 = "") {
        $data['features'] = $this->master_model->getactivevideofeature();
        $data['videoDetail'] = $this->master_model->searchBycategory($par1);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('videoSearch', $data);
    }

    public function gettingstarted() {
        $data['page_title'] = "Getting Started";
        $data['gettingstart'] = $this->master_model->getgettingstarted();
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('gettingstarted', $data);
    }

    public function contact() {
        $data['page_title'] = "Contact Us";
        $data['contactdata'] = $this->master_model->getcontact();
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('contactUs', $data);
    }

     public function featurevideo($par1 = "", $par2 = "") {

$par11= str_replace("_"," ",$par1);
$par22= str_replace("_"," ",$par2);

        if ($par11 == " ") {
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else if ($par11 == '1day') {


            $data['videoDetail'] = $this->master_model->getvideosbyfeatureday($par11, $par22);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['features'] = $this->master_model->getactivevideofeature();
            $data['usersubscription'] = $this->master_model->getuserSubscription();
            $this->load->view('videoSearchfeature', $data);

            $user = $par11;
        } else if ($par11 == 'short') {

            $data['videoDetail'] = $this->master_model->getvideosbyfeatureshort($par11, $par22);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['features'] = $this->master_model->getactivevideofeature();
            $data['usersubscription'] = $this->master_model->getuserSubscription();
            $this->load->view('videoSearchfeature', $data);

            $user = $par11;
        } else if ($par11 == 'long') {

            $data['videoDetail'] = $this->master_model->getvideosbyfeaturelong($par11, $par22);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['features'] = $this->master_model->getactivevideofeature();
            $data['usersubscription'] = $this->master_model->getuserSubscription();
            $this->load->view('videoSearchfeature', $data);

            $user = $par11;
        } else {

            $data['features'] = $this->master_model->getactivevideofeature();
            $data['videoDetail'] = $this->master_model->getvideosbyfeature($par11);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['usersubscription'] = $this->master_model->getuserSubscription();
            $this->load->view('videoSearchfeature', $data);
        }
    }


    public function history($par1 = "", $par2 = "") {


        if ($par1 == "") {
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else if ($par1 == '1day') {


            $data['videoDetail'] = $this->master_model->getsearchvideobefore($par1);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['features'] = $this->master_model->getactivevideofeature();
            $data['usersubscription'] = $this->master_model->getuserSubscription();
            $this->load->view('videoSearch', $data);

            $user = $par1;
        } else if ($par1 == 'short') {

            $data['videoDetail'] = $this->master_model->getsearchvideobeforeshort($par1);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['features'] = $this->master_model->getactivevideofeature();
            $data['usersubscription'] = $this->master_model->getuserSubscription();
            $this->load->view('videoSearch', $data);

            $user = $par1;
        } else if ($par1 == 'long') {

            $data['videoDetail'] = $this->master_model->getsearchvideobeforelong($par1);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['features'] = $this->master_model->getactivevideofeature();
            $data['usersubscription'] = $this->master_model->getuserSubscription();
            $this->load->view('videoSearch', $data);

            $user = $par1;
        } else {




            $user = $par1;
            $data['videoDetail'] = $this->master_model->gethistory($user);
            $data['companydetail'] = $this->master_model->getcompanydetail();
            $data['features'] = $this->master_model->getactivevideofeature();
            $data['usersubscription'] = $this->master_model->getuserSubscription();

            $this->load->view('videoSearch', $data);
        }
    }

    public function subscribe($par1 = "") {
        $upload = $this->input->post('uploader');
        if ($upload != "") {
            $uploader = $upload;
        } else {
            $uploader = $par1;
        }

        if ($this->master_model->subscribe($uploader)) {
            $this->session->set_flashdata('msg', 'Subscription Added!');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {

            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function unsubscribe($par1 = "") {
        $upload = $this->input->post('uploader');
        if ($upload != "") {
            $uploader = $upload;
        } else {
            $uploader = $par1;
        }



        if ($this->master_model->unsubscribe($uploader)) {
            $this->session->set_flashdata('msg', 'Subscription Removed!');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {

            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function subscriber($par1 = "") {
        $par1;
        if (isset($_GET['page'])) {
            $start = $_GET['page'] * 10;
        } else {
            $start = 0;
        }
        $par1;
        //$id = $this->session->userdata('id');
        $data['checksubcribe'] = $this->master_model->checksubs($par1);
        $data['AllSubscription'] = $this->master_model->getAllSubscriber();
        $data['userSubscription'] = $this->master_model->getuserSubscription();
        $data['userSubs'] = $this->master_model->getuserSubscriptionforsub($par1, $start);
        $data['userdata'] = $this->master_model->getdataforuploader($par1);

        $data['companydetail'] = $this->master_model->getcompanydetail();
        $uid = $this->session->userdata('id');
        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $this->load->view('subscription', $data);
    }
public function mychannel($par1 = "") {
        if (isset($_GET['page'])) {
           $start = $_GET['page'] * 10;
          
        } else {
            $start = 0;
        }
         
        $id = $this->session->userdata('id');
        $par1;
        $data['checksubcribe'] = $this->master_model->checksubs($par1);
        $data['AllSubscription'] = $this->master_model->getAllSubscriber();
        $data['userSubscription'] = $this->master_model->getuserSubscription();
        $data['userSubs'] = $this->master_model->getuserSubscriptionforsub($par1, $start);
        $data['userSubsvideo'] = $this->master_model->getuserSubscriptionforsublimit($par1,$start);
        $data['userdata'] = $this->master_model->getdataforuploader($par1);
        $uid = $this->session->userdata('id');
        $data['userDetail'] = $this->master_model->getUserbyId($par1);
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $id = $this->session->userdata('id');
        $data['playlist'] = $this->master_model->getplaylistlimit($par1,$start);
        $data['openthetab'] = $_GET['tab'];
        //echo '<pre>';
        //print_r($data['userSubsvideo'] );
        //die;
        //$this->load->view('demo', $data);
        $this->load->view('mychannel', $data);
    }
//    public function mychannel($par1 = "") {
//        if (isset($_GET['page'])) {
//            $start = $_GET['page'] * 10;
//        } else {
//            $start = 0;
//        }
//      
//        $id = $this->session->userdata('id');
//        $par1;
//        $data['checksubcribe'] = $this->master_model->checksubs($par1);
//        $data['AllSubscription'] = $this->master_model->getAllSubscriber();
//        $data['userSubscription'] = $this->master_model->getuserSubscription();
//        $data['userSubs'] = $this->master_model->getuserSubscriptionforsub($par1, $start);
//        $data['userSubsvideo'] = $this->master_model->getuserSubscriptionforsublimit($par1);
//        $data['userdata'] = $this->master_model->getdataforuploader($par1);
//        $uid = $this->session->userdata('id');
//        $data['userDetail'] = $this->master_model->getUserbyId($par1);
//        $data['companydetail'] = $this->master_model->getcompanydetail();
//          $data['playlist'] = $this->master_model->getplaylist($par1);
//        $data['openthetab'] = $_GET['tab'];
//        //echo '<pre>';
//        //print_r($data['userSubsvideo'] );
//        //die;
//        $this->load->view('mychannel', $data);
//    }

    public function advanceSearchpage() {
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('Adsearch', $data);
    }

    public function advanceSearch() {

        $data['categories'] = $this->master_model->getCategoryForParentId();
        $data['companydetail'] = $this->master_model->getcompanydetail();
        foreach ($data['categories'] as $datacat) {
            $data['Cat'] = $datacat;
        }

        $this->load->view('AdvanceSearch', $data);
    }

    public function getsubcategories() {
        $catid = $this->input->post('cat');
        $subcats = $this->master_model->getsubcatdata($catid);

        //print_r($subcats);die;
    }

    public function advancesearchdata($par1 = "", $par2 = "") {
        if ($par1 != "") {
            $term = $par1;
        } else {
            $term = $this->input->post('search_data');
        }

        $short_by = $this->input->post('short_by');
        $filter = $this->input->post('filter');
        $uid = $this->session->userdata('id');
        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['categories'] = $this->master_model->getCategoryForParentId();
        $data['returnsearch'] = $this->master_model->getadvancedata($term, $par2);
        //echo '<pre>';
        //print_r($data['userDetail']);die;
        $data['features'] = $this->master_model->getactivevideofeature();
        $data['term'] = $term;
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('searchpage', $data);
    }
    public function chkcat____($cat) {

            $array1 = $this->db->get("category", array('id' => $cat))->result_array();
            foreach ($array1 as $res) {
                $res1[] = $res['category'];
            }
            $a1 = $this->session->userdata['user1'];
           
            if (count($a1) >= 1) {
                foreach ($a1 as $key => $sub1) {

                    foreach ($sub1 as $sub2) {
                        if ($sub2 == $id) {
                            echo $selectId;
                        }
                    }
                }
            }

            $a1['id'][$id] = $id;
              //print_r($a1);

            $this->session->set_userdata('user1', $a1);
        
    }
    
    
    
    
   
public function chkcat($cat)
{
    
     $data = array();
   // echo $cat; die;
        
        $res = $this->db->get_where('category',array('id'=>$cat))->row_array();
      // print_r($res);  die;
       // die;
        if($res['category_parent'] == '0'){
            $data[$cat] = $cat;
            $this->session->set_userdata('cat', $data);
           // print_r($data);
            //$userdata = array('cat' => $cat);
           // $this->session->set_userdata($data);
        }else{
             $dolly =  $res['category_parent'];
              $a1 = $this->session->userdata['cat'];
              unset($a1[$dolly]);
              
            
//            / $data[$dolly] = $cat;
            $this->session->set_userdata('cat', $a1);
        }
        
       
       // print_r($a1);
        
       
       // print_r($a1); die;
        
       // print_r($this->session->all_userdata());
        
        
}
    public function searchsubcat() {
      
       $cat = $this->input->post('search');
       
        $result = $this->master_model->getsubcats($cat);
//die;
        echo json_encode($result);
         $this->chkcat($cat);
//        $data = array();
//        $res = $this->db->get_where('category',array('id',$cat))->row_array();
//        if($res['category_parent'] == '0'){
//            $data['cat'][$cat] = $cat;
//            //$userdata = array('cat' => $cat);
//            $this->session->set_userdata($data);
//        }
//        if($res['category_parent'] != '0')
//        {
//            $as = $res['category_parent'];
//            
//           $catdata = $this->session->userdata('cat');
//           unset($catdata[$as]);
//           $this->session->set_userdata($catdata);
//            
//            $userdata = array('cat' => $cat);
//        }
//       
//    
        
        
        
    }
    
    function chkcatarr()
    {
        
         $catid = $this->input->post('cat');
       
        $this->db->where('id',$catid);
        $qtry = $this->db->get('category')->result_array();
        
        foreach($qtry as $rr){
            echo $rr['category_parent'];
        }
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public function searchsubcat2() {
        $cat = $this->input->post('search');
        $this->chkcat($cat);
        $result = $this->master_model->getsubcats($cat);
        echo json_encode($result);
    }

    public function searchsubcat3() {
        $cat = $this->input->post('search');
        $this->chkcat($cat);
        $result = $this->master_model->getsubcats($cat);
        echo json_encode($result);
    }

    public function searchsubcat4() {
        $cat = $this->input->post('search');
        $this->chkcat($cat);
        $result = $this->master_model->getsubcats($cat);
        echo json_encode($result);
    }

    public function createplaylist() {
        $this->db->where('user_id', $this->input->post('user'));
        $this->db->where('playlist_name', $this->input->post('vname'));
        $this->db->select('*');
        $q = $this->db->get('playlist');
        if ($q->num_rows() > 0) {
            echo "Err";
            die;
        }

        $vid = $this->input->post('video');

        $this->db->where('id', $vid);
        $this->db->select('*');
        $qry = $this->db->get('video');

        if ($qry->num_rows() > 0) {
            foreach (($qry->result()) as $row) {
                $datathumb = $row->videothumb;
            }
        }


        $date = date("Y-m-d");
        $pname = $this->input->post('vname');
        $playarray = array(
            'playlist_name' => $this->input->post('vname'),
            'privacy' => $this->input->post('vprivacy'),
            'video_id' => $this->input->post('video'),
            'user_id' => $this->input->post('user'),
            'uloader_id' => $this->input->post('uploader'),
            'playdate' => $date,
            'playthumb' => $datathumb
        );

        $this->db->insert('playlist', $playarray);
        $insert_id = $this->db->insert_id();
        if ($insert_id != "") {
            $ab = "$insert_id||$pname||";
            echo $ab;
        }
    }

    public function addplaylist() {

        $date = date("Y-m-d");
        $uid = $this->session->userdata('id');
        $addarray = array(
            'playlist_name' => $this->input->post('list'),
            'video_id' => $this->input->post('video'),
            'user_id' => $uid,
            'uloader_id' => $this->input->post('uploader'),
            'playdate' => $date,
        );

        $this->db->insert('playlist', $addarray);
    }

    public function addInplaylist() {

        $addplay = $this->input->post('add');
        $parentplay = $this->input->post('playlist');
        $creator = $this->input->post('creator');

        $this->db->where('playlist_name', $addplay);
        $this->db->where('user_id', $creator);
        $this->db->select('*');
        $qry = $this->db->get('playlist');

        if ($qry->num_rows() > 0) {
            foreach (($qry->result()) as $row) {
                $videodata = $row->video_id;
                $date = date("Y-m-d");
                $playarray = array(
                    'playlist_name' => $addplay,
                    'privacy' => 'private',
                    'video_id' => $videodata,
                    'user_id' => $creator,
                    'playdate' => $date,
                    'parent_list' => $parentplay,
                );
                $this->db->insert('playlist', $playarray);
            }
        }
    }

    public function deletefromplay() {

        $uid = $this->session->userdata('id');
        $play = $this->input->post('playlist');
        $video = $this->input->post('vname');
        if ($this->db->delete('playlist', array('id' => $play, 'video_id' => $video, 'user_id' => $uid))) {

            return true;
        } else {

            return false;
        }
    }

    public function deletefromlist() {

        $play = $this->input->post('vname');
        $list = $this->input->post('playlist');
        $creator = $this->input->post('creator');

        $this->db->where('id', $list);
        $this->db->select('*');
        $q = $this->db->get('playlist');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $name = $row->playlist_name;
            }
        }

        if ($this->db->delete('playlist', array('playlist_name' => $name, 'user_id' => $creator, 'parent_list' => $play))) {
            return true;
        } else {
            return false;
        }
    }

    public function deletefromplaylist() {
        $uid = $this->session->userdata('id');
        $play = $this->input->post('playlist');
        $video = $this->input->post('vname');
        if ($this->db->delete('playlist', array('playlist_name' => $play, 'video_id' => $video, 'user_id' => $uid))) {

            return true;
        } else {

            return false;
        }
    }

    public function deletefromplaylistvideos() {

        $playlist = $this->input->post('delete');
        $creator = $this->input->post('creator');
        $listname = $this->input->post('playlist');


        if ($this->db->delete('playlist', array('playlist_name' => $listname, 'parent_list' => $playlist, 'user_id' => $creator))) {

            return true;
        } else {

            return false;
        }
    }

    public function showplaylist($par1 = "") {
        $par1;

        $data['companydetail'] = $this->master_model->getcompanydetail();
        $id = $this->session->userdata('id');
        $data['playlist'] = $this->master_model->getplaylist($par1);
        $data['userDetail'] = $this->master_model->getUserbyId($id);
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $this->load->view('playlist', $data);
    }

    public function createplaylistdash() {

        $palylist = $_POST['data']['list'];
        $privacy = $_POST['data']['privacy'];
        $user = $this->session->userdata('id');


        $this->db->where('user_id', $user);
        $this->db->where('playlist_name', $palylist);
        $this->db->select('*');
        $q = $this->db->get('playlist');
        if ($q->num_rows() > 0) {
            echo "This playlist already Exists!";
            die;
        }

        foreach ($_POST['video'] as $data) {

            $this->db->where('id', $data);
            $this->db->select('*');
            $qry = $this->db->get('video');

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {
                    $datathumb = $row->videothumb;
                }
            }


            $date = date("Y-m-d");
            $add = array(
                'playlist_name' => $palylist,
                'privacy' => $privacy,
                'user_id' => $user,
                'uloader_id' => $user,
                'playdate' => $date,
                'video_id' => $data,
                'playthumb' => $datathumb
            );

            $this->db->insert('playlist', $add);
            $insert_id = $this->db->insert_id();
            if ($insert_id != "") {
                $ab = "$insert_id||$palylist||";
                echo $ab;
            }
        }
    }

    public function insertplaydata() {
        //print_r($_POST);die;
        $uid = $this->session->userdata('id');
        foreach ($_POST['video'] as $data) {
            $date = date("Y-m-d");
            $arr = explode('|', $data);
            $video = $arr[0];
            $playlist = $arr[1];


            $this->db->where('id', $playlist);
            $this->db->select('*');
            $q = $this->db->get('playlist');

            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {

                    $data = $row->playlist_name;
                    $privacy = $row->privacy;
                }
            }
            echo $data;

            $this->db->where('id', $video);
            $this->db->select('*');
            $qry = $this->db->get('video');

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {
                    $datathumb = $row->videothumb;
                }
            }

            $add = array(
                'playlist_name' => $data,
                'privacy' => $privacy,
                'user_id' => $uid,
                'uloader_id' => $uid,
                'video_id' => $video,
                'playdate' => $date,
                'playthumb' => $datathumb
            );

            $this->db->where('video_id', $video);
            $this->db->where('playlist_name', $data);
            $this->db->where('user_id', $uid);
            $this->db->select('*');
            $qrycheck = $this->db->get('playlist');
            //echo $this->db->last_query();die;
            if ($qrycheck->num_rows() > 0) {


                die;
            } else {
                $this->db->insert('playlist', $add);
            }
        }
    }

    public function searchplay() {
        $search_data = $this->input->post('search_data');

        $result = $this->master_model->get_autoplay($search_data);
        if (!empty($result)) {
            foreach ($result as $row):

                echo "<li><a href='".base_url()."index.php/home/showplaylist/$row->id'><input type='checkbox' style='display:inline;' name='videochk[]' class='case2' id=$row->id" . " onChange='insertplay(this.id);'>" . ucwords($row->playlist_name) . "</a></li>";

            endforeach;
        }
        else {
            echo "<li><a href='#'>" . ucwords("Results Not Found!") . "</a></li>";
        }
    }

    public function deleteplaylist() {
        $uid = $this->session->userdata('id');
        $playid = $this->input->post('playid');
        if ($this->db->delete('playlist', array('playlist_name' => $playid, 'user_id' => $uid))) {
            echo $playid;
        }
    }

    public function playvideo($par1 = "", $par2 = "") {
         $play = $par1;
          $play = str_replace("%20", " ", $play);
         $creator = $par2;
        $now = new DateTime();
        $date = $now->format('Y-m-d');
        $this->db->select('view');
        $this->db->where('playlist_name', $play);
        //$this->db->where('creator', $creator);
        $q = $this->db->get('views');
        if ($this->session->userdata('id') == "") {
            $user = "0";
        } else {
            $user = $this->session->userdata('id');
        }


        if ($q->num_rows() > 0) {
            

            $viewdata = array('view' => 1,
                'Date' => $date,
                'UserId' => $user,
                'playlist_name' => $play,
                'creator' => $creator,
            );
            $this->db->insert('views', $viewdata);
        } else {

            $viewdata = array('view' => 1,
                'UserId' => $user,
                'Date' => $date,
                'playlist_name' => $play,
                'creator' => $creator,
            );
            $this->db->insert('views', $viewdata);
        }
        $price = $this->db->get_where('playlist',array('playlist_name'=>$play,'user_id'=>$user))->result_array();
        $data['price'] = $price[0]['price'];
        $data['videoDetail'] = $this->master_model->getvideoByplayname($play);
       //echo '<pre>';
        //print_r($data['videoDetail']);
        //die;
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $data['Allvideo'] = $this->master_model->Allvideo();
        $data['likes'] = $this->master_model->Alllikes($id);
        $data['Unlikes'] = $this->master_model->Alldislikes($id);
        $data['Allcomment'] = $this->master_model->Allcommentsplay($play);
        $data['Allview'] = $this->master_model->Allviews($id);
        $data['features'] = $this->master_model->getactivevideofeature();
        $data['playlist'] = $this->master_model->getplaylist($creator);
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['userDetail'] = $this->master_model->getUserbyId($creator);
        
        $data['page_title'] = "Video Details";

        $this->load->view('showvideoplaylist', $data);
    }

    function embed($par1 = "") {
        $i = 0;
        if ($_POST) {
            $i = 1;
            $vid = $this->input->post('video');
        } else {
            $vid = $par1;
        }
        $this->db->where('id', $vid);
        $this->db->select('*');
        $q = $this->db->get('video');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {

                $video = $row->name;
            }
        }
        if ($i == 1) {
            echo "".base_url()."index.php/home/embed/$vid||";
        }
        $data['video'] = $video;
        $this->load->view('embed', $data);
    }

    function embedlist($par1 = "", $par2 = "") {
        $play = $this->input->post('video');
        $creator = $this->input->post('creator');
        $i = 0;
        if ($play) {
            $i = 1;
            $play = $play;
            $creator = $creator;
        } else {
            $play = $play;
            $creator = $creator;
        }

        $data['videoDetail'] = $this->master_model->getvideoByplayId($play, $creator);

        if ($i == 1) {
            echo "".base_url()."index.php/welcome/embedlist/$play||";
        }
        $this->load->view('embedlist', $data);
    }

    public function createvideolist() {

        $this->db->where('user_id', $this->input->post('user'));
        $this->db->where('playlist_name', $this->input->post('vname'));
        $this->db->select('*');
        $q = $this->db->get('playlist');
        if ($q->num_rows() > 0) {
            echo "Err";
            die;
        }

        $vid = $this->input->post('video');

        $this->db->where('playlist_name', $vid);
        $this->db->where('user_id', $this->input->post('user'));
        $this->db->select('*');
        $qry = $this->db->get('playlist');

        if ($qry->num_rows() > 0) {
            foreach (($qry->result()) as $row) {
                $videodata = $row->video_id;
                $date = date("Y-m-d");
                $pname = $this->input->post('vname');
                $playarray = array(
                    'playlist_name' => $this->input->post('vname'),
                    'privacy' => $this->input->post('vprivacy'),
                    'video_id' => $videodata,
                    'user_id' => $this->input->post('user'),
                    'uloader_id' => $this->input->post('uploader'),
                    'playdate' => $date,
                    'parent_list' => $vid,
                );
                $this->db->insert('playlist', $playarray);
            }
        }
        $user = $this->input->post('user');
        $play = $this->input->post('vname');
        $insert_id = $this->db->insert_id();
        if ($insert_id != "") {
            $ab = "$insert_id||$play||";
            echo $ab;
        }
    }

    public function searchplaylist($par1 = "") {

        $uid = $this->session->userdata('id');
        $data['listDetail'] = $this->master_model->getplaylistvideos($par1);
        $data['Allplaylist'] = $this->master_model->getallplaylists();
        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['feature'] = $this->master_model->videoByFeature();
        $this->load->view('searchplay', $data);
    }

    public function getsearchdata($par1 = "", $par2 = "") {
        if ($par1 == 'short') {
            $data['videoDetail'] = $this->master_model->getsearchvideoshort($par1);
        }
        if ($par1 == '1day') {

            $data['videoDetail'] = $this->master_model->getsearchvideobeforeday($par1);
        }
        if ($par1 == 'long') {
            $data['videoDetail'] = $this->master_model->getsearchvideolong($par1);
        }
        if ($par1 != "") {
            $term = $par1;
            
        }
        if ($par1 == "") {
            $term = $this->input->post('search_data');
        }
        $short_by = $this->input->post('short_by');
        $filter = $this->input->post('filter');
        $uid = $this->session->userdata('id');

        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['catmenu'] = $this->master_model->getCategoryForParentId();

        $data['returnsearch'] = $this->master_model->getadvancedata($term, $short_by, $filter);
        
         $data['playlist'] = $this->master_model->getAllPlaylistforterm($term);
         //$data['list'] = '1';
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['features'] = $this->master_model->getactivevideofeature();
        $this->load->view('advancesearchresult', $data);
    }

    public function getadvanceresult($cat1 = "", $parm = "", $parm1 = "", $parm2 = "", $parm3 = "") {
     
     $par1 = $this->input->post('short');
        $par2 = $this->input->post('filter');
        if ($par1 != "") {
            $short = $par1;
        }
        if ($par2 != "") {
            $filter = $par2;
        }
        if ($cat1 != "") {
            $cat[] = $cat1;
            $categoryids[] = $cat1;
        }
        if ($parm != "") {
            $cat[] = $parm;
            $categoryids[] = $parm;
        }
        if ($parm1 != "") {
            $cat[] = $parm1;
            $categoryids[] = $parm1;
        }
        if ($parm2 != "") {
            $cat[] = $parm2;
            $categoryids[] = $parm2;
        }
        if ($parm3 != "") {
            $cat[] = $parm3;
            $categoryids[] = $parm3;
        }

        $uid = $this->session->userdata('id');
     // $cat[] = $this->input->post('testcat');
      $cat[] = $this->input->post('categories');
      $include_check = $this->input->post('include_check');
        //$cat[] = $this->input->post('categories');
      if($include_check){
          $catid='';
      }else{
        $catid = $this->input->post('categories');
     
        $data['catids'] = $catid;
        $data['categogyids'] = $categoryids;
      
    $playlist =  $this->input->post('testplay');
     if($playlist == 'yes'){
      $data['playlist'] = $this->master_model->getAllPlaylistforsearch($catid);
    
       $data['list'] = '1';
      }}
        
        
        
//
//        foreach ($cat as $cccat) {
//
//            $exp = explode(',', $cccat);
//            $count = count($exp);
//            if ($count == '1') {
//                //$cat[] = $exp[0];
//
//                $vv = $exp[0];
//                $getcat = $this->master_model->getCategoryForParentId($vv);
//                //print_r($getcat);
//
//                foreach ((array_keys($getcat)) as $kval) {
//                    $cat[] = $kval;
//                }
//            }
//        }
       
       // if ($catid != '') {
        $data['categorydata'] = $this->master_model->getadvanceresult($cat, $short, $filter);
       
       // }

        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['catmenu'] = $this->master_model->getCategoryForParentId();

        $data['features'] = $this->master_model->getactivevideofeature();
        $data['catids'] = $catid;
        $data['categogyids'] = $categoryids;

        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('advancesearchresult', $data);
    }
    
    public function getadvanceresult2($cat1 = "", $parm = "", $parm1 = "", $parm2 = "", $parm3 = "") {


        $par1 = $this->input->post('short');
        $par2 = $this->input->post('filter');
        if ($par1 != "") {
            $short = $par1;
        }
        if ($par2 != "") {
            $filter = $par2;
        }
        if ($cat1 != "") {
            $cat[] = $cat1;
            $categoryids[] = $cat1;
        }
        if ($parm != "") {
            $cat[] = $parm;
            $categoryids[] = $parm;
        }
        if ($parm1 != "") {
            $cat[] = $parm1;
            $categoryids[] = $parm1;
        }
        if ($parm2 != "") {
            $cat[] = $parm2;
            $categoryids[] = $parm2;
        }
        if ($parm3 != "") {
            $cat[] = $parm3;
            $categoryids[] = $parm3;
        }

        $uid = $this->session->userdata('id');
        $cat[] = $this->input->post('categories');
        $catid = $this->input->post('categories');
//
//        foreach ($cat as $cccat) {
//
//            $exp = explode(',', $cccat);
//            $count = count($exp);
//            if ($count == '1') {
//                //$cat[] = $exp[0];
//
//                $vv = $exp[0];
//                $getcat = $this->master_model->getCategoryForParentId($vv);
//                //print_r($getcat);
//
//                foreach ((array_keys($getcat)) as $kval) {
//                    $cat[] = $kval;
//                }
//            }
//        }
       
        //if ($catid != '') {
        $data['categorydata'] = $this->master_model->getadvanceresult2($cat, $short, $filter);
        //}

        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['catmenu'] = $this->master_model->getCategoryForParentId();

        $data['features'] = $this->master_model->getactivevideofeature();
     
        $data['catids'] = $catid;
        $data['categogyids'] = $categoryids;

        $data['companydetail'] = $this->master_model->getcompanydetail();
        $this->load->view('advancesearchresult', $data);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    public function cliping() {
        $this->load->view('cliping');
    }

    public function logout() {


        $this->session->sess_destroy();
        redirect('home/welcome', 'refresh');
        exit;
    }

    public function payment() {
        $uid = $this->session->userdata('id');
        $videoid = $this->input->post('videoid');
        $price = $this->input->post('price');
        $uploader = $this->input->post('uploaderid');
        $order_id = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
        $date = date("Y-m-d");
        $paydata = array(
            'order_id' => $order_id,
            'videoId' => $videoid,
            'price' => $price,
            'uploderId' => $uploader,
            'userId' => $uid,
        );
        $payrecord = array(
            'videoId' => $videoid,
            'price' => $price,
            'uploderId' => $uploader,
            'userId' => $uid,
            'submit_date' => $date,
            'status' => 'pending',
            'orderId' => $order_id,
        );

        $this->db->insert('payment_info', $payrecord);
        echo $order_id;
        $this->session->set_userdata('payment', $paydata);
    }

    public function success($par1 = "") {
        $par1;

        $pay = $this->session->userdata('payment');
        $pay1 = $pay['videoId'];
        $price = $pay['price'];
        $user = $pay['userId'];
        $uploderId = $pay['uploderId'];
        $order = $pay['order_id'];
        $date = date("Y-m-d");
        $order;
        if ($par1 == $order) {
            $this->db->where('orderId', $order);
            $this->db->select('*');
            $q = $this->db->get('payment_info');
            if ($q->num_rows() > 0) {
                $data2['status'] = 'Success';

                $this->db->where('orderId', $order);
                $this->db->update('payment_info', $data2);
            }
            $this->session->unset_userdata('payment');
            redirect('home/showvideo/' . $pay1 . '/success', 'refresh');
        }
    }

    public function cancel($par1 = "") {


        $pay = $this->session->userdata('payment');
        $order = $pay['order_id'];
        $pay1 = $pay['videoId'];
        if ($par1 == $order) {
            $data2['status'] = 'Cancel';
            $this->db->where('orderId', $order);
            $this->db->update('payment_info', $data2);
            $this->session->unset_userdata('payment');
            redirect('home/showvideo/' . $pay1 . '/cancel', 'refresh');
        }
    }

    public function myorders($par1 = "") {
        $data['orderdetail'] = $this->master_model->getorderdetail($par1);
        //echo '<pre>';
        // print_r($data['orderdetail']);die;
        $id = $this->session->userdata('id');
        $data['userdata'] = $this->master_model->getdataforuser();
        $data['userDetail'] = $this->master_model->getUserbyId($id);
        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['usersubscription'] = $this->master_model->getuserSubscription();
        $this->load->view('myorder', $data);
    }
    public function open_advanceSearch(){
       $uid = $this->session->userdata('id');

        $data['userDetail'] = $this->master_model->getUserbyId($uid);
        $data['catmenu'] = $this->master_model->getCategoryForParentId();

       // $data['returnsearch'] = $this->master_model->getadvancedata($term, $short_by, $filter);

        $data['companydetail'] = $this->master_model->getcompanydetail();
        $data['features'] = $this->master_model->getactivevideofeature();
        $this->load->view('advancesearchresult', $data);
    }
    
public function checkemail(){
    $name=$this->input->post('id');
$check=$this->db->get_where('users',array('email'=>$name))->result_array();
if($check){
    echo "yes";
}
}
}
