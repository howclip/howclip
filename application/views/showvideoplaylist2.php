<!DOCTYPE HTML>
<html>
    <head>
        <title>HowClip</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>

        <style>
            .bc-player-04ac0699-ca6b-449b-a2b6-0f14b4a7893b_default .vjs-big-play-button:before{
                content:'Click to View Clip';
                font-size: 13px;
                width:auto ;
                line-height:19px;
               
            }
             .increasesize {
                width:14px;
                height:14px;
            }
            /*         .video-js{
              pointer-events: none;
            }*/
        </style>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
        <script>
            $(document).ready(function () {
                var user_id = document.getElementById('userid').value;
                var uploader = document.getElementById('uploader').value;

                if (uploader == user_id)
                {
                    $("#subhref").attr("href", '#');
                }
            });
        </script>
        <script>
            $(document).ready(function () {
                var price = document.getElementById('videoprice').value;
//                if ((price > 0))
//                {
//                    $(".video-js").css("pointer-events", "none");
//                }
                var paydone = document.getElementById('paydone').value;
                var paystatus = document.getElementById('paystate').value;
                var uploader = document.getElementById('vidupload').value;
                var user = document.getElementById('userid').value;
                if ((price > 0) && (paystatus != 'Success'))
                {

                    $(".video-js").css("pointer-events", "none");
                }
                if (paystatus == 'Success')
                {

                    $(".video-js").css("pointer-events", "all");
                }
                if (user != '' && uploader == user)
                {
                    document.getElementById('pricebefore').style.display = 'none';
                    $(".video-js").css("pointer-events", "all");
                }
                if (paydone == 'success')
                {

                    document.getElementById('pricebefore').style.display = "none";
                    $(".video-js").css("pointer-events", "all");
                    $(".ppbefore").hide();
                    $(".ppafter").show();
                    document.getElementById('playlibefore').style.display = 'none';
                    document.getElementById('playlisuccess').style.display = 'block';
                }
                if (paydone == 'cancel')
                {

                    $(".video-js").css("pointer-events", "none");
                }




            });

        </script>


        <script type="text/javascript">

            function submitthecomment() {
                
                var comment = document.getElementById('comment').value;
                var video = document.getElementById('vid').value;
                var user = document.getElementById('uid').value;
                var user_id = document.getElementById('userid').value;
                var uploader = document.getElementById('uploader').value;
                var list = document.getElementById('playgname').value;
                var userlogo = document.getElementById('userlogo').value;

                event.preventDefault();

                if (user_id == "")
                {

                    return false;
                } else
                {

                    $.ajax({
                        url: '<?php echo base_url(); ?>index.php/home/comments',
                        type: "POST",
                        data: {'com': comment, 'vid': video, 'user': user, 'user_id': user_id, 'uploader': uploader, 'list': list
                        },
                        success: function (response)
                        {

                            var jsonobj = $.parseJSON(response);
                            document.getElementById('comment').value = "";
                            document.getElementById('commentdiv').style.display = "none";
                            document.getElementById('commdiv').style.display = "block";
                            $("#commdiv").append("<li class='media'><div class='media-left'><a href='#'><img class='media-object' width='80px' height='70px' src='".base_url()."/uploads/" + userlogo + "'></a></div><div class='media-body' style='margin-top:5px;'><p>" + jsonobj.comment + "</p><p> " + jsonobj.user + "</p><p style='font-size:12px; margin:10px 0px;'><a href='#'> </a></p><ul class='media-list'></ul></div></li>");
                                    //$('#returncomment').html(jsonobj.comment);
                                    //$('#returnuser').html(jsonobj.user);

                        }

                    });
                }

            }


            function cancelcom()
            {

            }
        </script>
        <script>



            function getplay()
            {

                $("#play").css("display", "none");
                var aa = $(".playform").css("display", "block");

                if (aa.length > 0)
                {


                    setTimeout(function () {
                        $('#playli').click();
                    }, 05);
                    setTimeout(function () {
                        $('#playname').focus();
                    }, 05);

                    $('ul.nojs2').click(function (e) {

                        e.stopPropagation();
                    });



                }

            }
        </script>
        <style>
            div.scroll
            {
                background-color:#00FFFF;
                width:40%;
                height:300px;
                FLOAT: left;
                margin-left: 5%;
                padding: 1%;
                overflow:scroll;
            }


        </style>
        <style type="text/css">
            .video-js {
                float: left;
                margin: 15px;
                width: 100%;
                height: 380px;
            }
            .vjs-playlist {
                width: 100%;
                float: left;
                margin: 15px;
                height:380px;
            }
        </style>
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">

    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 

            <div class="sticky-header header-section ">
                <div class="header-left"> 

                    <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
                            <?php foreach ($features as $videofeature) { ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/featurevideo/<?php echo $videofeature->name; ?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name; ?> </a> </li>
                            <?php } ?>
                            <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"><i class="fa fa-th-large nav_icon"></i> My Channel </a> </li>
                            <li class="divider" role="seperator"></li>
                            <li> <a href="#"><i class="fa fa-th-large nav_icon"></i>SUBSCRIPTIONS </a> </li>
                            <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $uploader->username; ?> </a> </li>
                            <?php } ?>
                        </ul>
                    </div>

                    <?php
                    foreach ($companydetail as $company) {
                        if ($company->company_logo != "") {
                            $cmplogo = base_url() . "/Admin/uploads/$company->company_logo";
                        } else {
                            $cmplogo = base_url() . "/uploads/logo.png";
                        }
                        ?>      <?php } ?>
                    <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">

                            <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                        </a> </div>

                    <!--//logo--> 
                    <!--search-box-->
                    <div class="search-box">
                        <form class="input">
                            <input class="sb-search-input input__field--madoka" name="search_data" id="input-34"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off" />
                            <label class="input__label" for="input-34"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                                </svg> </label>
                        </form>

                        <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
                            <div id="autoSuggestionsList"></div>
                        </div>
                    </div>
                    <!--//end-search-box-->
                    <div class="clearfix"> </div>
                </div>
                <div class="header-right">
                    <div class="profile_details_left"><!--notifications of menu start -->
                        <ul class="nofitications-dropdown">
                            <?php
                            if ($this->session->userdata('id') != "") {
                                $ppage = "uploadpage";
                            } else {
                                $ppage = "loginpage/upload";
                            }
                            ?>
                            <?php if ($this->session->userdata('id') == "") { ?>
                                <li class="dropdown head-dpdn" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > 
                                <?php } ?>
                                <?php if ($this->session->userdata('id') != "") { ?>

                                <li class="dropdown head-dpdn"><a href="<?php echo base_url() ?>index.php/home/loginpage/upload"><img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" ></a>
                                <?php } ?>
                            </li><?php //echo base_url()index.php/home/loginpage   ?>
                            <?php if ($this->session->userdata('id') == "") { ?>

                                <li class="dropdown head-dpdn" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>assets/images/signinbut.png" class="img-responsive but"></li>
                      <!-- <li class="dropdown head-dpdn"> <a href="#"> <img src="<?php echo base_url(); ?>assets/images/signinbut.png" id="pop" data-toggle="modal" data-target="#myModal" class="img-responsive but"></a> </li>-->
                            <?php } ?>
                            <!--<li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/signup"> <img src="<?php echo base_url(); ?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>-->
                            <?php
                            foreach ($userDetail as $user) {
                                $username = $user->username;
                                $userimg = $user->userLogo;
                                if ($userimg == "") {
                                    $userimg = "user.png";
                                } else {
                                    $userimg = $user->userLogo;
                                }
                            }
                            ?>
                            <?php if ($this->session->userdata('id') != "") { ?>
                                <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="notification_header">
                                                <h3><?php echo $username; ?></h3>
                                            </div>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p><?php echo $this->session->userdata('email'); ?></p>

                                                </div>
                                                <div class="clearfix"></div>
                                            </a></li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                        </li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="clearfix"> </div>
                    </div>

                </div>


            </div>
            <div class="uploadwrap">
                <div class="col-md-12">
                    <div class="col-md-8">
                        <video id="myPlayerID"
                               data-account="3676484087001"
                               data-player="04ac0699-ca6b-449b-a2b6-0f14b4a7893b"
                               data-embed="default"
                               class="video-js">
                        </video>

                        <div class="pricetag" id="pricebefore" style="display:block;"   onclick="changeprice()" >
                           <?php if($videoDetail['price']>0.00){ ?>
                          View Playlist<br>
                           <?php }else{ ?>
                            View Playlist<br>
                            <?php } ?>
                            $ <?php echo $videoDetail['price']; ?>
                        </div>
                        <div class="agreetag" id="priceafter" style="display:none">
                            <p> <?php if($videoDetail['price']>0.00){ ?>
                             View this playlist
                           <?php }else{ ?>
                            View this Playlist for PLAYLISTS
                            <?php } ?> for $ <?php echo $videoDetail['price']; ?></p>
                            <span style="text-align:center;display:block;"><button  type="button" <?php if ($this->session->userdata('id') == "") { ?> class="pop agreesub"  data-toggle="modal" data-target="#myModal" <?php } else { ?> class="btn agreesub" onclick="makepayment('<?php echo $videoDetail['price']; ?>', '<?php echo $videoDetail['playlistname']; ?>', '<?php echo $videoDetail['userId']; ?>');"<?php } ?>  >Agree</button>
                                <button class="btn disagreesub" onclick="canpay()" type="submit">Don't Agree</button></span>
                        </div>
                        <!--<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="makepayment">-->
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="makepayment">
                            <!-- Identify your business so that you can collect the payments. -->
                            <!--<input type="hidden" name="business" value="<?php echo "logo@livesoftwaresolution.info"; ?>">-->
                             <input type="hidden" name="business" value="<?php echo "david@howclip.com"; ?>">
                            <!-- Specify a Buy Now button. -->
                            <input type="hidden" name="cmd" value="_xclick">

                            <!-- Specify details about the item that buyers will purchase. -->
                            <input type="hidden" name="item_name" value="<?php echo $videoDetail['playlistname']; ?>"> 

                            <input type="hidden" name="video_id" value="<?php echo $videoDetail['userId']; ?>">
                            <input type="hidden" name="amount" value="<?php echo $videoDetail['price']; ?>">
                            <input type="hidden" name="currency_code" value="USD">

                            <!-- Specify URLs -->
                            <input type='hidden' name='cancel_return' id="cancelpay" >
                            <input type='hidden' name='return' id="subpay" >


                            <!-- Display the payment button. -->
                    <!--        <input type="image" name="Pay" border="0">-->


                        </form>
                    </div>
                    <div class="col-md-4" style="overflow-y:scroll">
                        <ol class="vjs-playlist" style="margin-left:0px;"></ol>
                    </div>
                </div>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blankpage">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="hidden" id="videoprice" value="<?php echo $videoDetail['price']; ?>" >
                                                <input type="hidden" id="paystate" value="<?php echo $videoDetail['pay_state']; ?>" >
                                                <input type="hidden" id="paydone" value="<?php echo $payment; ?>" >
                                                <input type="hidden" id="vidupload" value="<?php echo $videoDetail['userId']; ?>" >
                                                <?php
                                                if ($videoDetail['userlogo'] == "") {
                                                    $userimg = "user.png";
                                                } else {
                                                    $userimg = $videoDetail['userlogo'];
                                                }
                                                ?>
                                                <div class="iconimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div>

                                                <div class="videosubscribe">
                                                    
                                                    <span style="font-size:16px;"><?php echo ucwords($videoDetail['playlistname']); ?></span><br>
                                                   <a href="<?php echo base_url(); ?>index.php/welcome/mychannel/<?php echo $videoDetail['userId']; ?>">    <?php echo ucwords($videoDetail['username']); ?></a>

                                                    <br>
                                                    <?php
                                                    $playname = $videoDetail['playlistname'];
                                                    $subs = $videoDetail['userId'];
                                                    if ($videoDetail['subs'] == "0") {
                                                        ?>

                                                        <a id="subhref" href="<?php
                                                        if ($this->session->userdata('id') == "") {


                                                            echo base_url() . "index.php/home/loginpage/$subs";
                                                        } else {
                                                            echo base_url()
                                                            ?>index.php/home/subscribe/<?php
                                                               echo $subs;
                                                           }
                                                           ?>"><img src="<?php echo base_url(); ?>assets/images/download.jpg" class="img-responsive"></a>


                                                    <?php } ?>
                                                    <?php
                                                    $subs = $videoDetail['userId'];
                                                    if ($videoDetail['subs'] == "1") {
                                                        ?>

                                                        <a id="subhref" href="<?php
                                                        if ($this->session->userdata('id') == "") {

                                                            echo base_url() . "index.php/home/loginpage/$subs";
                                                        } else {
                                                            echo base_url()
                                                            ?>index.php/home/unsubscribe/<?php
                                                               echo $subs;
                                                           }
                                                           ?>"><img src="<?php echo base_url(); ?>assets/images/unsubscribe.jpg" class="img-responsive"></a>

                                                    <?php } ?>


                                                </div>
                                                <p align="right" style="display:inline-block;float:right;margin-top:15px;"><?php
                                                    if ($videoDetail['view'] == "") {
                                                        echo "0";
                                                    } else {
                                                        echo $videoDetail['view'];
                                                    }
                                                    ?> Views</p>
                                            </div>

                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="likebox">
                                                    <div class="sharemen">


                                                        <ul>

                                                            <li class="dropdown play" id="playul">
                                                                <a id="playli" <?php if ($this->session->userdata('id') == "") { ?> class="pop" data-content="Please Sign In!"<?php } ?> <?php if ($this->session->userdata('id') != "") { ?> class="dropdown-toggle" data-toggle="dropdown" <?php } ?>  role="button" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus" aria-hidden="true"></i> Add to </a>
                                                                <ul class="dropdown-menu nojs2">
                                                                    <li><span id="Err"></span></li>
                                                                    <?php
                                                                    foreach ($playlist as $li) {
                                                                        $gvideo = $li->parent_list;
                                                                        $guser = $li->user_id;
                                                                        ?>
                                                                        <li id="fplay" style="display:none"><a href="#"><input type="checkbox" id="videochk" checked="checked" onChange="removevideo(this.id, '<?php echo $videoDetail['playlistname']; ?>', '<?php echo $guser; ?>');"><span id="playlistname" style="font-size:14px;"></span><span id="playlistid" style="display:none"></span></a></li>
                                                                    <?php } ?>

                                                                    <?php
                                                                    $i = 0;
                                                                    foreach ($playlist as $play) {
                                                                        $parent = $play->parent_list;
                                                                        $listname = $videoDetail['playlistname'];
                                                                        ?>
                                                                        <li><a href="#"><input type="checkbox" name="videochk[]" class="case increasesize" id="<?php echo $i; ?>" <?php if ($parent == $listname) { ?> checked= "checked" <?php } ?> onChange="addremovevideo(this.id, '<?php echo $videoDetail['playlistname']; ?>', '<?php echo $videoDetail['userId']; ?>', '<?php echo $play->playlist_name; ?>');"><?php echo $play->playlist_name; ?></a></li><?php
                                                                        $i++;
                                                                    }
                                                                    ?>

                                                                    <li role="separator" class="divider"></li>
                                                                    <li id="play" onClick="getplay();"><a href="javascript:void;">Create a new Playlist</a></li>

                                                                    <form id="myForm">
                                                                        <li class="playform" style="display:none;"><a href="javascript:void;"><input name="playname" id="playname" type="text" style="width: 100%;height:25px;"></a></li>
                                                                        <li class="playform" style="display:none;"><span style="width:55%;display:inline;padding:0px;"><select name="playprivacy" id="playprivacy" style="width:60%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;"><option value="public">Public</option><option value="unlisted">Unlisted</option><option value="private">Private</option></select></span><span style="width:40%;display:inline;padding:0px;"><input type="button" value="Create" onClick="createplay('<?php echo $this->session->userdata('id') ?>', '<?php echo $videoDetail['playlistname']; ?>', '<?php echo $videoDetail['userId']; ?>');" style="width:37%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;margin-left:5px;background-color: darkslateblue;
                                                                                                                                                                                                        color: white;"/></span></li>
                                                                    </form>
                                                                </ul>
                                                            </li>
                                                            <li class="dropdown">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share" aria-hidden="true"></i> Share </a>
                                                                <ul class="dropdown-menu" style="min-width: 152px;">

                                                                    <li style="padding:5px 15px;"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url() ?>index.php/home/showvideo/<?php echo $videoDetail['playlistname']; ?>" target="_blank" style="display:inline;padding:5px 5px;"><img src="https://simplesharebuttons.com/images/somacro/facebook.png" width="30px;" height="30px;" alt="Facebook" /> </a><a href="https://plus.google.com/share?url=<?php echo base_url() ?>index.php/home/showvideo/<?php echo $videoDetail['playlistname']; ?>" onClick="javascript:window.open(this.href,
                                                                                    '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                                                                            return false;" style="display:inline;padding:5px 5px;">

                                                                    </li>
                                                                </ul>
                                                            </li>

                                                            <li class="dropdown">
                                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onClick="generateiframe('<?php echo $videoDetail['playlistname']; ?>', '<?php echo $videoDetail['userId']; ?>');"</i> Embed</a>
                                                                <ul class="dropdown-menu iframe">
                                                                    <li class="dropdown.mega-dropdown"><span id="embedcode"></span></li>

                                                                </ul>
                                                            </li> 
                                                        </ul>
                                                    </div>
                                                    <div class="likemen">


                                                        <?php
                                                        $videoplay = $videoDetail['playlistname'];
                                                        $creator = $videoDetail['userId'];
                                                        $uid = $this->session->userdata('id');
                                                        if ($this->session->userdata('id') != "") {
                                                            $uid == "";
                                                            $subarray = array('url' => "showvideo/$videoplay");
                                                            $this->session->set_userdata($subarray);
                                                        }
                                                        ?>
                                                        <ul>

                                                            <li><a href="javascript:void(0);" <?php if ($uid == "") { ?> class="pop" data-content="Please Sign In!"<?php } ?>  onClick="Submitlike('<?php echo $videoplay; ?>', '<?php echo $creator; ?>');"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span id="likes"><?php
                                                                        if ($videoDetail['totallikes'] == "") {
                                                                            echo "0";
                                                                        } else {
                                                                            echo $videoDetail['totallikes'];
                                                                        }
                                                                        ?>  </span></a></li>
                                                            <li><a href="javascript:void(0);" <?php if ($uid == "") { ?> class="pop" data-content="Please Sign In!"<?php } ?>  onClick="Submitdislike('<?php echo $videoplay; ?>', '<?php echo $creator; ?>');"><i class="fa fa-thumbs-down" aria-hidden="true"></i> <span id="dislikes"><?php
                                                                        if ($videoDetail['Unlikes'] == "") {
                                                                            echo "0";
                                                                        } else {
                                                                            echo $videoDetail['Unlikes'];
                                                                        }
                                                                        ?>  </span> </a></li>
                                                        </ul>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blankpage">

                                        <div class="blankhead">Comments</div>
                                        <div class="commentprofile">
                                            <div class="row">
                                                <div class="col-md-2" style="padding-right:0px;">
                                                    <img src="<?php echo base_url(); ?>assets/images/37610397-good-wallpapers.jpg" class="img-responsive">
                                                </div>

                                                <div class="col-md-10" style="padding-left:10px;">

                                                    <div id="commentsubmit11">
                                                        <div class="row" style="margin:0px;">
                                                            <div class="col-md-12" style="padding-left:0px; padding-right:0px;">
                                                                <textarea class="form-control commentform" style="resize:none" id="comment" rows="2" onClick="showcomments()" name="commnets"></textarea>
                                                                <input type="hidden" name="video_id" id="vid" value="<?php echo $videoDetail['playlistname']; ?>">
                                                                <input type="hidden" name="play" id="playgname" value="<?php echo $videoDetail['playlistname']; ?>">
                                                                <input type="hidden" name="user_id" id="uid" value="<?php echo $this->session->userdata('name'); ?>"> <input type="hidden" name="user" id="userid" value="<?php echo $this->session->userdata('id'); ?>">
                                                                <input type="hidden" name="uploader" id="uploader" value="<?php echo $videoDetail['userId']; ?>">
                                                            </div>
                                                        </div>
                                                        <div class="row" id="commentdiv" style="display:none;margin:0px;">
                                                            <div class="col-md-6"></div>
                                                            <div class="col-md-6" style="padding-left:0px; padding-right:0px;">

                                                                <button <?php if ($this->session->userdata('id') == '') { ?> class="btn commenter pop" data-toggle="modal" data-target="#myModal"  <?php } else { ?> class="btn commenter" type="button" id="commentsubmithgh" onclick="abccheck();" <?php } ?> >Comment</button> <button class="btn commentercanc" type="submit" onClick="cancelcom();">Cancel</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>



                                        <div class="row">
                                            <?php
                                            foreach ($videoDetail as $vid) {
                                                $userlogo = $vid->userlogo;
                                                if ($vid->userlogo == '') {
                                                    $userlogo = "user.png";
                                                }
                                            }
                                            ?>
                                            <div class="col-md-12"><input type="hidden" id="userlogo" value="<?php echo $userlogo; ?>">

                                                <ul class="media-list" id="commdiv"></ul>

                                                <ul class="media-list">


                                                    <?php
                                                    foreach ($Allcomment as $comm) {
                                                        if ($comm->userlogo == "") {
                                                            $imguser = "user.png";
                                                        } else {
                                                            $imguser = $comm->userlogo;
                                                        }
                                                        ?>
                                                        <li class="media">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $imguser; ?>" alt="..." width="80px" height="70px">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">

                                                                <p><?php echo $comm->comment; ?></p>
                                                                <p><?php echo $comm->user; ?></p>

                                                                <p style="font-size:12px; margin:10px 0px;"><a href="#"></a></p>
                                                                <ul class="media-list">

                                                                </ul>
                                                            </div>
                                                        </li>
                                                    <?php } ?>



                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blankpage">
                                        <?php foreach ($Allvideo as $vid) { ?>
                                            <div class="upcommming">
                                                <div class="media">

                                                    <div class="media-left">
                                                        <?php
                                                        if (getimagesize(base_url() . '/uploads/images/' . $vid->video_img) !== false) {
                                                            $link = base_url() . "/uploads/images/" . $vid->video_img;
                                                        } else {
                                                            $link = base_url() . "/uploads/images/download.jpg";
                                                        }
                                                        ?>
                                                        <a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $vid->id; ?>">
                                                            <img class="media-object sideimage" src=<?php echo $link; ?> alt="...">
                                                        </a>
                                                        <?php if ($vid->price > 0) { ?>
                                                            <a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $vid->id; ?>">
                                                                <div class="dollar1" ><img src="<?php echo base_url() ?>/assets/images/usd1600.png" style="width:20px;height:25px;" class="img-responsive"></div>
                                                            </a>

                                                        <?php } ?>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $vid->id; ?>"><?php echo ucwords($vid->videoname); ?></a></h4>
                                                        <p style="font-size:12px;"><?php echo ucwords($vid->video_category); ?></p>
                                                        <p style="font-size:12px;"><?php
                                                            if ($vid->videoview == "") {
                                                                echo "0";
                                                            } else {
                                                                echo $vid->videoview;
                                                            }
                                                            ?> Views</p>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>


                                    </div>


                                </div>
                            </div>
                        </div>




                    </div> 
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="margin:20% auto;">  
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=" position: absolute;right: 10px;top: 10px;z-index: 100;"></button>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <a href="<?php echo base_url() ?>index.php/welcome"><img src="<?php echo base_url(); ?>assets/images/logo1.png" class="img-responsive logolog"></a>
                                <h4 style="text-align:center; font-size:13px;">Sign in to continue to <span style="color:#03C;">HowClip</span></h4>

                                <div style="color:#FF0000;text-align:center;" id="signformerror"></div>



                                <div class="logwrap">
                                    <div class="profiledp"> <i class="fa fa-user" aria-hidden="true"></i></div>
                                    <form class="setup" method="post"  name="signform" id="signform">
                                        <!-- action="<?php echo base_url() ?>index.php/home/login"-->
                                        <div class="form-group">

                                            <input type="email" class="form-control login" name="email" id="emailAddress" placeholder="Email">
                                        </div>
                                        <div class="form-group">

                                            <input type="password" class="form-control login" name="password" id="pass" placeholder="Password">
                                        </div>


                                        <button type="button" class="btn loginbutton" onClick="getlogin();">Submit</button>
                                    </form>

                                </div>
                                <h3><a href="<?php echo base_url() ?>index.php/welcome/signup" style="font-size:14px;margin:10px 0px; text-align:center; text-decoration:none;display:block;">Create Account</a></h3>
                                <div class="logresponsive">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img src="<?php echo base_url(); ?>assets/images/fblogin.jpg" id="loginBtn" class="img-responsive logimageres"><div id="response" style="display:none;"></div> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="margin:20% auto;">  
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=" position: absolute;right: 10px;top: 10px;z-index: 100;"></button>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                        <?php   foreach ($companydetail as $company) {
                                                    if ($company->company_logo != "") {
                                                        $cmplogo = base_url() . 'Admin/uploads/'.$company->company_logo;
                                                    } else {
                                                        $cmplogo = base_url() . "uploads/logo.png";
                                                    }
                                                    ?><?php } ?>
                                                <a href="<?php echo base_url() ?>index.php/welcome"><img src="<?php echo $cmplogo; ?>" class="img-responsive logolog"></a>
                                 <h4 style="text-align:center; font-size:13px;">Sign in to continue to <span style="color:#03C;">HowClip</span></h4>


                                <div style="color:#FF0000;text-align:center;" id="signformerror"></div>



                                <div class="logwrap">
                                    <div class="profiledp"> <i class="fa fa-user" aria-hidden="true"></i></div>
                                    <form class="setup" method="post"  name="signform" id="signform">
                                        <!-- action="<?php echo base_url() ?>index.php/home/login"-->
                                        <div class="form-group">

                                            <input type="email" class="form-control login" name="email" id="emailAddress" placeholder="Email">
                                        </div>
                                        <div class="form-group">

                                            <input type="password" class="form-control login" name="password" id="pass" placeholder="Password">
                                        </div>


                                        <button type="button" class="btn loginbutton" onClick="getlogin();">Submit</button>
                                    </form>

                                </div>
                                <h3><a href="<?php echo base_url() ?>index.php/welcome/signup" style="font-size:14px;margin:10px 0px; text-align:center; text-decoration:none;display:block;">Create Account</a></h3>
                                <div class="logresponsive">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img src="<?php echo base_url(); ?>assets/images/fblogin.jpg" id="loginBtn" class="img-responsive logimageres"><div id="response" style="display:none;"></div> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <div class="footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="footmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                            <li><a href="javascript:void(0);"> Press </a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                            <li><a href="javascript:void(0);"> Creators</a></li>
                            <li><a href="javascript:void(0);"> Advertise</a></li>
                            <li><a href="javascript:void(0);"> Developers</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="minfootmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                            <li><a href="javascript:void(0);"> Privacy</a></li>
                            <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                            <li><a href="javascript:void(0);"> Send feedback</a></li>
                            <li><a href="javascript:void(0);"> Test new features</a></li>
                            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--//footer--> 
    </div>
    <!-- Classie --> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
    <script>
                                            var menuLeft = document.getElementById('cbp-spmenu-s1'),
                                                    showLeftPush = document.getElementById('showLeftPush'),
                                                    body = document.body;

                                            showLeftPush.onclick = function () {
                                                classie.toggle(this, 'active');
                                                classie.toggle(body, 'cbp-spmenu-push-toright');
                                                classie.toggle(menuLeft, 'cbp-spmenu-open');
                                                disableOther('showLeftPush');
                                            };

                                            function disableOther(button) {
                                                if (button !== 'showLeftPush') {
                                                    classie.toggle(showLeftPush, 'disabled');
                                                }
                                            }
    </script> 
    <!--scrolling js--> 
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fileupload.js"></script>
    <!--//scrolling js--> 
    <!-- Bootstrap Core JavaScript --> 

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 
    <script>
                                            $(function () {
                                                $('#menu').metisMenu({
                                                    toggle: false // disable the auto collapse. Default: true.
                                                });
                                            });
    </script>
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
    </script>
    <script type="text/javascript">

        function ajaxSearch()
        {
            var input_data = $('#input-31').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchAlllist",
                    data: post_data,
                    success: function (data) {
                        console.log(data);
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
    </script>
    <script src="<?php echo base_url(); ?>assets/js/playlist.js"></script>
    <!--<script src="//players.brightcove.net/3676484087001/04ac0699-ca6b-449b-a2b6-0f14b4a7893b_default/index.min.js"></script>-->
    <script type="text/javascript">
        var videodata = '<?php echo json_encode($videoDetail['url']); ?>';
        var JSONObject = JSON.parse(videodata);
        var abc = [];
        var i = 0;

        var count = 0;
        videojs('myPlayerID').ready(function () {
            var myPlayer = this;

            myPlayer.playlist([
<?php
$result = count($videoDetail['url']);

for ($i = 0; $i < $result; $i++) {

    $dd = $videoDetail['url'][$i];
    $img = $videoDetail['thumb'][$i];
    $name = $videoDetail['videoname'][$i];

    echo '
                            { 
                            "sources": [{
                              "src": "' . base_url() . '/uploads/live/' . $dd . '", "type": "video/mp4"
                            }],
                            "name": "' . $name . '",
                            "thumbnail": "' . base_url() . '/uploads/images/' . $img . '",
                            "poster": "' . base_url() . '/uploads/images/' . $img . '"
		 
		 
                      }
		  
                    ,';
}
?>



            ]);

        });
    </script>
    <script>
        function showcomments()
        {

            document.getElementById('commentdiv').style.display = "block";
        }
    </script>
    <script>
        function Submitlike(video, uid) {

            if (uid == "")
            {
            } else {
                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/home/insertlikeplay',
                    type: "POST",
                    data: {'vid': video, 'user': uid
                    },
                    success: function (response)
                    {

                        var jsonobj = $.parseJSON(response);

                        $('#likes').html(jsonobj.totallike);
                        $('#dislikes').html(jsonobj.Unlike);
                    }

                });
            }
        }
    </script>

    <script>
        function Submitdislike(video, uid) {
            if (uid == "")
            {

            } else {
                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/home/insertdislikeplay',
                    type: "POST",
                    data: {'vid': video, 'user': uid
                    },
                    success: function (response)
                    {
                        console.log(response);
                        var jsonobj = $.parseJSON(response);

                        $('#likes').html(jsonobj.totallike);
                        $('#dislikes').html(jsonobj.Unlike);


                    }

                });
            }
        }
    </script>

    <script>
        $('.pop').popover().click(function () {
            setTimeout(function () {
                $('.pop').popover('hide');
            }, 2000);
        });

    </script>
    <script>
        function generateiframe(x, y)
        {
            

            var post_data = {
                'video': x,
                'creator': y
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/welcome/embedlist",
                data: post_data,
                success: function (data) {
                    console.log(data);
                    if (data.length > 0) {
                        var res = data.split('||');
                        var ab = '<iframe width="560" height="315" src="' + res[0] + '" frameborder="0" allowfullscreen></iframe>';
                        $('#embedcode').text(ab);
                        $('li.dropdown.mega-dropdown').removeClass('close');
                        $('ul.iframe *').click(function (e) {
                            e.stopPropagation();
                        });
                    }
                }
            });
        }

    </script>


    <script>
        function createplay(x, y, z)
        {
            var uname = x;
            var uvideo = y;
            var uploader = z;

            var name = document.getElementById('playname').value;
            var privacy = document.getElementById('playprivacy').value;
            var post_data = {
                'vname': name,
                'vprivacy': privacy,
                'user': x,
                'video': uvideo,
                'uploader': uploader

            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/createvideolist",
                data: post_data,
                success: function (data) {
                    console.log(data);
                    if (data.length > 0) {
                        var res = data.split('||');
                        if (res[1] == undefined)
                        {
                            $("#videochk").css("display", "none");
                        } else
                        {
                            $("#videochk").css("display", "inline");
                        }
                        $("#fplay").css("display", "block");


                        $('#playlistname').html(res[1]);
                        $('#playlistid').html(res[0]);
                        if (res[0] == 'Err')
                        {
                            $('#Err').html(res[0]);
                        }
                        setTimeout(function () {
                            $('#Err').fadeOut('fast');
                        }, 1000);

                    }
                }
            });

        }

        function removevideo(id, v, w)
        {

            var i = id;
            var video = v;
            var uploader = w;

            var abc = $("#" + i).prop("checked");
            if (abc)
            {

                var post_data = {
                    'vname': name,
                    'user': x,
                    'video': uvideo,
                    'uploader': uploader

                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/createplaylist",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {
                            var res = data.split('||');
                            $("#fplay").css("display", "block");
                            $('#playlistname').html(res[1]);
                            $('#playlistid').html(res[0]);



                        }
                    }
                });
            } else
            {

                var name = document.getElementById('playlistid').innerText;
                var post_data = {
                    'vname': video,
                    'playlist': name,
                    'creator': uploader
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/deletefromlist",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {

                        }
                    }
                });

            }

        }

    </script>
    <script>
        function addremovevideo(id, v, w, x)
        {

            var abf = $("#" + id).prop("checked");

            var i = id;
            var add = v;
            var creator = w;
            var parent = x;

            if (abf)
            {

                var post_data = {
                    'add': add,
                    'playlist': parent,
                    'creator': creator,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/addInplaylist",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {


                        }
                    }
                });
            } else
            {

                var post_data = {
                    'delete': add,
                    'playlist': parent,
                    'creator': creator,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/deletefromplaylistvideos",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {

                        }
                    }
                });

            }

        }
    </script>
    <script type="text/javascript">

        function ajaxSearch()
        {

            var input_data = $('#input-34').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchAll",
                    data: post_data,
                    success: function (data) {
                        //console.log(data);
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
    </script>
    <script>
        $(document).on("click", function (e) {
            if (!$("#suggestions").is(e.target)) {
                $("#suggestions").hide();
            }
        });

        function changeprice()
        {
            document.getElementById('pricebefore').style.display = "none";
            document.getElementById('priceafter').style.display = "block";
        }
    </script>
    <script>

        function makepayment(a, b, c)
        {

            var price = a;
            var video = b;
            var uploader = c;
            var post_data = {
                'price': price,
                'videoid': video,
                'uploaderid': uploader,
            };

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/payplaylist",
                data: post_data,
                success: function (data) {
                    //console.log(data);
                    //$('#makepayment').submit();
                    if (data.length > 0) {
                        var aa = data;
                        document.getElementById('subpay').value = "<?php echo base_url() ?>index.php/home/success_play/" + aa;
                        document.getElementById('cancelpay').value = "<?php echo base_url() ?>index.php/home/cancel/" + aa;
                        $('#makepayment').submit();
                        //vvalue='<?php echo base_url() . 'index.php/home/cancel/' . $ad['order_id'] ?>' '			 
                    }
                }
            });

            //  $('#makepayment').submit();

        }


    </script>
    <script>
        function canpay()
        {
            document.getElementById('pricebefore').style.display = "block";
            document.getElementById('priceafter').style.display = "none";
        }

    </script>

    <script>

        function getlogin()
        {

            var email = document.getElementById("emailAddress").value;
            var password = document.getElementById("pass").value;

            if (email == "")
            {
                document.getElementById("signformerror").innerHTML = "Enter the Email Address";
                return false;
            } else if (password == "")
            {
                document.getElementById("signformerror").innerHTML = "Enter the Password";
                return false;
            } else
            {

                var sign_data = {
                    'email': email,
                    'password': password,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/welcome/login",
                    data: sign_data,
                    success: function (data) {
                        console.log(data);
                        if (data.length > 0) {
                            if (data == 'success')
                            {
                                var videoid = document.getElementById('vid').value;
                                var uploader = document.getElementById('uploader').value;
                                window.location = "<?php echo base_url() ?>index.php/home/playvideo/" + videoid + "/" + uploader;
                            } else
                            {
                                document.getElementById("signformerror").innerHTML = data;
                            }

                        }
                    }
                });


            }


        }
        $('#pass').keypress(function (e) {
            if (e.which == '13') {
                getlogin();
            }
        });
        
        function abccheck()
        {

            var comment = document.getElementById('comment').value;
            var video = document.getElementById('vid').value;
            var user = document.getElementById('uid').value;
            var user_id = document.getElementById('userid').value;
            var uploader = document.getElementById('uploader').value;
            var list = document.getElementById('playgname').value;
            var userlogo = document.getElementById('userlogo').value;
           
            if (user_id == "")
                    {
                        return false;
                    } 
                    else
                    {
                        alert();
                          $.ajax({
                            url: '<?php echo base_url(); ?>index.php/home/comments',
                            type: "POST",
                            data: {'com': comment, 'vid': video, 'user': user, 'user_id': user_id, 'uploader': uploader, 'list': list
                            },
                            success: function (response)
                            {

                                var jsonobj = $.parseJSON(response);
                                console.log(response);
                                //return false;
                                 document.getElementById('comment').value = "";
                                  document.getElementById('commentdiv').style.display = "none";
                                   document.getElementById('commdiv').style.display = "block";
                                  
                            $("#commdiv").append("<li class='media'><div class='media-left'><a href='#'><img class='media-object' width='80px' height='70px' src='<?php echo base_url();?>/uploads/" + userlogo + "'></a></div><div class='media-body' style='margin-top:5px;'><p>" + jsonobj.comment + "</p><p> " + jsonobj.user + "</p><p style='font-size:12px; margin:10px 0px;'><a href='#'> </a></p><ul class='media-list'></ul></div></li>");
                                                        //$('#returncomment').html(jsonobj.comment);
                                        //$('#returnuser').html(jsonobj.user);

                            }

                        });
                    }
            return false;
        }
    </script>
     <script type="text/javascript">
            function aaa(x,y){
                var email = x;
                 var name = y;
                 $.ajax({
                    url: '<?php echo base_url(); ?>index.php/welcome/insertfbdata',
                    type: "POST",
                    data: {'email': email,'user':name
                    },
                    success: function (response)
                    {
                        window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                    }

                });
            }
              function SubmitUserData(email) {
                alert(email);
                 return false;
                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/home/insertfbdata',
                    type: "POST",
                    data: {'email': email
                    },
                    success: function (response)
                    {
                        window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                    }

                });
            }

            function getUserData() {
                FB.api('/me?fields=id,first_name,last_name,email,link,gender,locale,picture', function (response) {
                    document.getElementById('response').innerHTML = 'Hello ' + response.id + '<br>' + response.email + '<br>' + response.first_name + ' ' + response.last_name + '<br>' + response.gender;
                   var uname = response.first_name;
        var email = response.email;
      
       
                    aaa(email,uname);
                });
            }
          

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '284987908652218',
                    xfbml: true,
                    version: 'v2.6'
                });
            };

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));

            document.getElementById('loginBtn').addEventListener('click', function () {

                FB.login(function (response) {
                    if (response.authResponse) {
                        //user just authorized your app
                        document.getElementById('loginBtn').style.display = 'none';
                        getUserData();

                    }
                }, {scope: 'email,public_profile', return_scopes: true});
            }, false);
           

        </script>
</body>
</html>