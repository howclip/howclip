<!DOCTYPE HTML>
<html>
<head>
<title>Pupilclip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-menu.css">
 
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
.widthdrop {
  min-width:auto;
  width:100%;
}

</style>

<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">

<div class="main-content"> 
  <!--left-fixed -navigation-->
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
   <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->
      
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
<li> <a href="#" class="active"><i class="fa fa-home nav_icon"></i> Home </a> </li>
		  <?php foreach($features as $videofeature) { ?>
          <li> <a href="<?php echo base_url()?>index.php/home/featurevideo/<?php echo $videofeature->name;?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name;?> </a> </li>
		  <?php } ?>
          <li> <a href="<?php echo base_url()?>index.php/home/history/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
		    <li> <a href="<?php echo base_url()?>index.php/home/advanceSearch"><i class="fa fa-th-large nav_icon"></i> Advanced Search </a> </li>  </ul>
</div>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"  > <a href="http://livesoftwaresolution.info/PUPILCLIP/">
        <!--<h1>Pupilclip</h1>
        <span>Website</span> </a>--> 
        <img src="<?php echo base_url();?>assets/images/logo.png" width="90" class="img-responsive">
       </a> </div>
      <!--//logo--> 
      <!--search-box-->
     <div class="search-box">
         <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off"  />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"  style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
           
          </li>
		   <?php if($this->session->userdata('id') == "") {?>
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"> <img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive but"></a> </li>
            <?php } ?>
		   <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/signup"> <img src="<?php echo base_url();?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>
		      <?php foreach($userDetail as $user) { $username = $user->username;$userimg = $user->userLogo; if($userimg== "") { $userimg = "user.png"; }}?>
		   <?php if($this->session->userdata('id')!="") {?>
		   <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url();?>uploads/<?php echo $userimg;?>" class="img-responsive"></div> </a>
            <ul class="dropdown-menu" style="left:initial;right:0;color:#000;">
              <li>
                <div class="notification_header">
                  <h3><?php echo $username;?></h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p><?php echo $this->session->userdata('email');?></p>
                  
                </div>
                <div class="clearfix"></div>
                </a></li>
				<li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/dash">My Account</a> </div>
              </li>
              <li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/logout">Log Out</a> </div>
              </li>
            </ul>
           </li>
		   <?php }?>
        </ul>
        <div class="clearfix"> </div>
      </div>
	 
	 
    </div>
    
    
  </div>
  
  
 <div class="uploadwrap">
 
  <div class="container-fluid" style="margin:30px 0px;" >
  
 <div class="row">
 
 <div class="col-md-2">
 
 
<div class="vertimen dropdown" >
<?php 
$i=0;foreach($categories as $parent) {

?>
 <ul>
 <li><a href="#"> <?php echo $parent['title'];?>  <input type="checkbox" id="adsearch" onChange="adsearch('<?php echo $parent['id'];?>');" value="<?php echo $parent['id'];?>" class="adsearch" style="float:right">  </a></li>
 
 <!--<li><a href="#">  Check me out  menu 2  <input type="checkbox" style="float:right">  </a></li>
 
 <li><a href="#"> Check me out menu 3   <input type="checkbox" style="float:right"> </a></li>
 <li><a href="#"> Check me out   menu 4  <input type="checkbox" style="float:right"></a></li>
 <li><a href="#"> Check me out  menu 5   <input type="checkbox" style="float:right"></a></li>-->
 </ul>
 <?php $i++;} ?>
 </div>
 </div>
 <div id="returndiv">
 <div class="col-md-2">
 <div class="vertimen dropdown-content">
 <ul>
 <li><a href="#"> Check me out menu 1  <input type="checkbox"  style="float:right"></a></li>
 
 <li><a href="#">  Check me out  menu 2  <input type="checkbox" style="float:right"></a></li>
 
 <li><a href="#"> Check me out menu 3   <input type="checkbox" style="float:right"> </a></li>
 <li><a href="#"> Check me out   menu 4  <input type="checkbox" style="float:right"></a></li>
 <li><a href="#"> Check me out  menu 5   <input type="checkbox" style="float:right"></a></li>
 </ul>
 </div>
 </div>
 <div class="col-md-2">
 <div class="vertimen">
 <ul>
 <li><a href="#"> Check me out menu 1  <input type="checkbox" style="float:right">  </a></li>
 
 <li><a href="#">  Check me out  menu 2  <input type="checkbox" style="float:right">  </a></li>
 
 <li><a href="#"> Check me out menu 3   <input type="checkbox" style="float:right"> </a></li>
 <li><a href="#"> Check me out   menu 4  <input type="checkbox" style="float:right"></a></li>
 <li><a href="#"> Check me out  menu 5   <input type="checkbox" style="float:right"></a></li>
 </ul>
 </div>
 </div>
 <div class="col-md-2">
 <div class="vertimen">
 <ul>
 <li><a href="#"> Check me out menu 1  <input type="checkbox" style="float:right">  </a></li>
 
 <li><a href="#">  Check me out  menu 2  <input type="checkbox" style="float:right">  </a></li>
 
 <li><a href="#"> Check me out menu 3   <input type="checkbox" style="float:right"> </a></li>
 <li><a href="#"> Check me out   menu 4  <input type="checkbox" style="float:right"></a></li>
 <li><a href="#"> Check me out  menu 5   <input type="checkbox" style="float:right"></a></li>
 </ul>
 </div>
 </div>
 <div class="col-md-2">
 <div class="vertimen">
 <ul>
 <li><a href="#"> Check me out menu 1  <input type="checkbox" style="float:right">  </a></li>
 
 <li><a href="#">  Check me out  menu 2  <input type="checkbox" style="float:right">  </a></li>
 
 <li><a href="#"> Check me out menu 3   <input type="checkbox" style="float:right"> </a></li>
 <li><a href="#"> Check me out   menu 4  <input type="checkbox" style="float:right"></a></li>
 <li><a href="#"> Check me out  menu 5   <input type="checkbox" style="float:right"></a></li>
 </ul>
 </div>
 </div>
 </div>
 
 </div>
 
 <div class="row">
 
 
 <form class="form-inline">
 <div class="col-md-5">
 <button type="submit" class="btn menubutpage">Send invitation</button>
 </div>
 <div class="col-md-1">
  <div class="form-group">
   <select class="form-control login" style="margin:10px 0px;width:100%;">
  <option>1</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
  <option>5</option>
</select>
  </div>
  </div>
  <div class="col-md-1">
  <div class="form-group">
    <select class="form-control login" style="margin:10px 0px;width:100%;">
  <option>1</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
  <option>5</option>
</select>
  </div>
  </div>
</form>
 
 

 </div>
 
 
  </div>
  </div>
  <div class="footer">
    <div class="row">
      <div class="col-md-12">
        <div class="footmen">
          <ul>
            <li><a href="#"> About </a></li>
            <li><a href="#"> Press </a></li>
            <li><a href="#"> Copyright </a></li>
            <li><a href="#"> Creators</a></li>
            <li><a href="#"> Advertise</a></li>
            <li><a href="#"> Developers</a></li>
            <li><a href="#">Pupilclipe</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="minfootmen">
          <ul>
            <li><a href="#"> Terms</a></li>
            <li><a href="#"> Privacy</a></li>
            <li><a href="#"> Policy & Safety</a></li>
            <li><a href="#"> Send feedback</a></li>
            <li><a href="#"> Test new features</a></li>
            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<script src="<?php echo base_url();?>assets/js/fileupload.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 

<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
 <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/sidebar-menu.js"></script>
  <script>
    $.sidebarMenu($('.sidebar-menu'))
  </script>
  <script type="text/javascript">

function ajaxSearch()
{
    var input_data = $('#input-31').val();

    if (input_data.length === 0)
    {
        $('#suggestions').hide();
    }
    else
    {

        var post_data = {
            'search_data': input_data,
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchAll",
            data: post_data,
            success: function (data) {
                // return success
                if (data.length > 0) {
				//window.location.href = '<?php echo base_url();?>index.php/home/searchpage';
                    $('#suggestions').show();
                    $('#autoSuggestionsList').addClass('auto_list');
                    $('#autoSuggestionsList').html(data);
                }
            }
         });
		 
		
     }
	 
 }
</script>
<script>
function adsearch(x)
{


var pri = x;
     
$.ajax({
      url: '<?php echo base_url();?>index.php/home/getsubcategories',
      type:"POST",
      data:{cat : pri
                 },
    success: function(response)
    {
	//alert(response);
	console.log(response);
	
	
	document.getElementById('returndiv').innerHTML = response;
		//window.location.href = '<?php echo base_url();?>index.php/home/dash';
     }
    
  });

}
</script>

</body>
</html>