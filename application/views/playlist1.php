<!DOCTYPE HTML>
<html>
    <head>
        <title>HowClip</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sidebar-menu.css">
        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>

        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <style>

            input[type="checkbox"] {
                display:none;
            }

            input[type="checkbox"] + label {
                color:#f2f2f2;
                font-family:Arial, sans-serif;
                font-size:14px;
                float:left;
            }

            input[type="checkbox"] + label span {
                display:inline-block;
                width:19px;
                height:19px;
                margin:-1px 4px 0 0;
                vertical-align:middle;
                background:url(images/check_radio_sheet.png) left top no-repeat;
                cursor:pointer;
            }

            input[type="checkbox"]:checked + label span {
                background:url(images/check_radio_sheet.png) -19px top no-repeat;
            }
.butonon
{
    display: block;
    margin:5px 0;
    padding: 2px 15px;
    color: #fff;
    font-size: 13px;
    background-color: #06C;
    border-radius: 2px;
    width: 55px;
}
        </style>
        <style>
            input[type=text] {
                width: 130px;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 10px;
                font-size: 14px;
                background-color: white;
                background-image: url('searchicon.png');
                background-position: 10px 10px; 
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            input[type=text]:focus {
                width: 100%;
                border-radius: 10px;
                outline:0 !important; 
            }

            .search-box input[type=text] {
                width: 50%;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 25px;
                font-size: 14px;
                background-color: white;
                background-image: url(https://www.w3schools.com/howto/searchicon.png);
                background-position: 10px 10px;
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            .search-box input[type=text]:focus {
                outline:0;
            }

            .searchbut
            {
                padding: 5px 15px;
                background: #ddd;
                color:#000;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
            }
            .searchbut:hover ,.searchbut:focus,.searchbut:active:focus{
                background: #ccc;
                color:#000;
                outline:0;
            }
            .advance{
                background: #ddd;
                color:#d60808;
                padding: 5px 15px;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
                font-weight:bold;
               
            }
            a.advance:visited
            {
                color:#d60808;
            }






        </style>

        <!--//Metis Menu -->
    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 

            <div class="sticky-header header-section ">
                <div class="header-left"> 
                    <!--toggle button start-->
                 <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->

                    <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu8">
                            <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id') ?>">My Videos</a></li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>"> History </a> </li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"> My Channel </a> </li>
                            <li class="divider" role="seperator"></li>
                            <li> <a href="#">SUBSCRIPTIONS </a> </li>
                            <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"> <?php echo $uploader->username; ?> </a> </li>
                            <?php } ?>

                        </ul>
                    </div>
                    <!--toggle button end--> 
                    <!--logo -->
                    <?php foreach ($companydetail as $company) {
                        if ($company->company_logo != "") {
                            $cmplogo = base_url() . "Admin/uploads/$company->company_logo";
                        } else {
                            $cmplogo = base_url() . "uploads/logo.png";
                        } ?><?php } ?>
                        <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">

                                <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                            </a> </div>

                    
                
                   <form class="input" method="post" id="my_form" action="<?php echo base_url() ?>index.php/welcome/getsearchdata">
                        <div class="search-box">
                            <input type="text" style="width:55%;" id="input-31" onKeyUp="ajaxSearch();" name="search_data" placeholder="Search..">

                            <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:55%;z-index:10000;">
                                <div id="autoSuggestionsList"></div>
                            </div>

                            <a href="javascript:void(0)" id="chkval"  class="btn searchbut"> Search  </a>
                            <a href="<?php echo base_url() ?>index.php/welcome/open_advanceSearch" class="btn advance"> Advanced Topic Search  </a>
                        </div>
                    <!--//end-search-box-->
                    <div class="clearfix"> </div>
                </div>
                <div class="header-right">
                    <div class="profile_details_left"><!--notifications of menu start -->
                        <ul class="nofitications-dropdown">
                            <li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/uploadpage" style="color:#333;" > <img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > </a>

                            </li>
<?php foreach ($userDetail as $user) {
    $username = $user->username;
    $userimg = $user->userLogo;
    if ($userimg == "") {
        $userimg = "user.png";
    }
} ?>
                        <!--<li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url(); ?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
                            <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                <ul class="dropdown-menu" style="left:initial; right:0;">
                                    <li>
                                        <div class="notification_header">
                                            <h3><?php echo $username; ?></h3>
                                        </div>
                                    </li>
                                    <li><a href="#">
                                            <div class="notification_desc">
                                                <p><?php echo $this->session->userdata('email'); ?></p>

                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                    </li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"> </div>
                    </div>


                </div>


            </div>
            <div class="uploadwrap">
                <div class="container-fluid">
                    <div class="row" style="padding:0px;">
                        <div class="col-md-2" style="padding-right:0px;">
                            <ul class="sidebar-menu">
                                <li class="sidebar-header">HowClip</li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/dash">
                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-files-o"></i>
                                        <span>Video Manager</span>
                                    </a>
                                    <ul class="sidebar-submenu" >
                                        <li><a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Videos</a></li>
                                        <li><a href="<?php echo base_url() ?>index.php/home/showplaylist/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Playlists</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>My Channel</span>

                                    </a>

                                </li>



                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/userupdate/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>User Profile</span>

                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/changePass/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>Change Password</span>

                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/myorders/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>My Orders</span>

                                    </a>

                                </li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="blankpage">
                                <div>
                                    <h4>Playlist &nbsp;&nbsp; <span class="badge"></span></h4>

                                </div>
                                <div class="searchedit">
                                    <!--<button type="button" >Create Playlist</button>-->



                                </div>


                            </div>
<?php // foreach($playlist as $play) { ?>

                            <!--<div class="blankpage">
                             <input type="checkbox" id="c2" name="cc" />
                                      <label for="c2"><span></span>  </label>
                                     <div class="imageedit"> <img src="<?php echo base_url(); ?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                             
                                             
                                  <div class="edithead"><?php // $play->playlist_name; ?></div>
                                  <div class="edittime"><?php // $play->date; ?></div>
                                  
                                  </div>
                                    
                                     <div class="viewmen">
                                
                                     <button class="btn publishedit" style="background-color:#FF0000" type="button">Delete</button>
                                     
                                 
                          
                            </div>
                            <br>-->
<?php foreach ($playlist as $play) { ?>
                                <div class="blankpage" id="<?php echo $play->playlist_name; ?>">
    <?php if (getimagesize('uploads/images/' . $play->playthumb) !== false) {
        $link = base_url() . "uploads/images/" . $play->playthumb;
    } else {
        $link = base_url() . "uploads/images/download.jpg";
    } ?>

                                    <label for="c3"><span></span>  </label>
                                    <div class="imageedit"> <a href="<?php echo base_url() ?>index.php/home/playvideo/<?php echo $play->playlist_name; ?>/<?php echo $play->user_id; ?>"><img src="<?php echo $link; ?>" class="img-responsive videdit"></a>
                                        <div class="edithead"><a href="<?php echo base_url() ?>index.php/home/playvideo/<?php echo $play->playlist_name; ?>/<?php echo $play->user_id; ?>"><?php echo ucwords($play->playlist_name); ?></a>
                                        <a href="<?php echo base_url() ?>index.php/home/edit_playlist/<?php echo $play->playlist_name; ?>" class="btn butonon"> Edit </a>
                                        
                                        </div>
<!--                                        <div class="edittime"><?php echo $date; ?></div>-->
                                        
                                    </div>

                                    <div class="viewmen">
                                        <button class="btn publishedit" style="background-color:#FF0000" type="button" onClick="deleteplay('<?php echo $play->playlist_name; ?>');">Delete</button>
                                    </div>

                                </div>
<?php } ?>
                            <!-- <div class="blankpage">
                           
                              <input type="checkbox" id="c4" name="cc" />
                                       <label for="c4"><span></span>  </label>
                                      <div class="imageedit"> <img src="<?php echo base_url(); ?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                   <div class="edithead">Video Name</div>
                                   <div class="edittime">14/08/2017</div>
                                   <div class="dropdown">
                             <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Edit
                               <span class="caret"></span>
                             </button>
                             <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                               <li><a href="#">Action</a></li>
                               <li><a href="#">Another action</a></li>
                               <li><a href="#">Something else here</a></li>
                               <li role="separator" class="divider"></li>
                               <li><a href="#">Separated link</a></li>
                             </ul>
                           </div>
                                   </div>
                                      
                                      <div class="likolay">
                                 <div class="view" data-toggle="tooltip" data-placement="bottom" title="Public"><a href="#"><i class="fa fa-globe" aria-hidden="true"></i></a></div>
                                      <div class="editlike">
                                      <ul>
                                      <li data-toggle="tooltip" data-placement="bottom" title="All Comments"><a href="#"><i class="fa fa-comment" aria-hidden="true"></i> </a> 0</li>
                                      <li data-toggle="tooltip" data-placement="bottom" title="Likes"><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> </a> 0</li>
                                      <li data-toggle="tooltip" data-placement="bottom" title="Dislikes"><a href="#"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a> 0</li>
                                      </ul>
                                      </div>
                                      
                                      </div>
                           
                             </div>
                             <div class="blankpage">
                           
                              <input type="checkbox" id="c5" name="cc" />
                                       <label for="c5"><span></span>  </label>
                                      <div class="imageedit"> <img src="<?php echo base_url(); ?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                   <div class="edithead">Video Name</div>
                                   <div class="edittime">14/08/2017</div>
                                   <div class="dropdown">
                             <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Edit
                               <span class="caret"></span>
                             </button>
                             <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                               <li><a href="#">Action</a></li>
                               <li><a href="#">Another action</a></li>
                               <li><a href="#">Something else here</a></li>
                               <li role="separator" class="divider"></li>
                               <li><a href="#">Separated link</a></li>
                             </ul>
                           </div>
                                   </div>
                                      
                                      <div class="viewmen">
                                 <div class="draft">Draft</div>
                                      <button class="btn publishedit" type="submit">Publish</button>
                                      
                                      </div>
                           
                             </div>
                             <div class="blankpage">
                           
                              <input type="checkbox" id="c6" name="cc" />
                                       <label for="c6"><span></span>  </label>
                                      <div class="imageedit"> <img src="<?php echo base_url(); ?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                   <div class="edithead">Video Name</div>
                                   <div class="edittime">14/08/2017</div>
                                   <div class="dropdown">
                             <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Edit
                               <span class="caret"></span>
                             </button>
                             <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                               <li><a href="#">Action</a></li>
                               <li><a href="#">Another action</a></li>
                               <li><a href="#">Something else here</a></li>
                               <li role="separator" class="divider"></li>
                               <li><a href="#">Separated link</a></li>
                             </ul>
                           </div>
                                   </div>
                                      
                                      <div class="viewmen">
                                 <div class="draft">Draft</div>
                                      <button class="btn publishedit" type="submit">Publish</button>
                                      
                                      </div>
                           
                             </div>
                             <div class="blankpage">
                           
                              <input type="checkbox" id="c7" name="cc" />
                                       <label for="c7"><span></span>  </label>
                                      <div class="imageedit"> <img src="<?php echo base_url(); ?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                   <div class="edithead">Video Name</div>
                                   <div class="edittime">14/08/2017</div>
                                   <div class="dropdown">
                             <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                              Edit
                               <span class="caret"></span>
                             </button>
                             <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                               <li><a href="#">Action</a></li>
                               <li><a href="#">Another action</a></li>
                               <li><a href="#">Something else here</a></li>
                               <li role="separator" class="divider"></li>
                               <li><a href="#">Separated link</a></li>
                             </ul>
                           </div>
                                   </div>
                                      
                                      <div class="likolay">
                                 <div class="view" data-toggle="tooltip" data-placement="bottom" title="Public"><a href="#"><i class="fa fa-globe" aria-hidden="true"></i></a></div>
                                      <div class="editlike">
                                      <ul>
                                      <li data-toggle="tooltip" data-placement="bottom" title="All Comments"><a href="#"><i class="fa fa-comment" aria-hidden="true"></i> </a> 0</li>
                                      <li data-toggle="tooltip" data-placement="bottom" title="Likes"><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> </a> 0</li>
                                      <li data-toggle="tooltip" data-placement="bottom" title="Dislikes"><a href="#"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a> 0</li>
                                      </ul>
                                      </div>
                                      
                                      </div>
                           
                             </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//footer--> 
        </div>
        <!-- Classie --> 
        <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
        <script>
                                        var menuLeft = document.getElementById('cbp-spmenu-s1'),
                                                showLeftPush = document.getElementById('showLeftPush'),
                                                body = document.body;

                                        showLeftPush.onclick = function () {
                                            classie.toggle(this, 'active');
                                            classie.toggle(body, 'cbp-spmenu-push-toright');
                                            classie.toggle(menuLeft, 'cbp-spmenu-open');
                                            disableOther('showLeftPush');
                                        };

                                        function disableOther(button) {
                                            if (button !== 'showLeftPush') {
                                                classie.toggle(showLeftPush, 'disabled');
                                            }
                                        }
        </script> 
        <!--scrolling js--> 
        <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/fileupload.js"></script>
        <!--//scrolling js--> 
        <!-- Bootstrap Core JavaScript --> 
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 
        <script>
                                        $(function () {
                                            $('#menu').metisMenu({
                                                toggle: false // disable the auto collapse. Default: true.
                                            });
                                        });
        </script>
        <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/sidebar-menu.js"></script>
        <script>
                                        $.sidebarMenu($('.sidebar-menu'))
        </script>
        <script>

            $(function () {
                $('[data-toggle="tooltip"]').tooltip()
            })
        </script>
        <script>
            function deleteplay(x)
            {

                var post_data = {
                    'playid': x,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/deleteplaylist",
                    data: post_data,
                    success: function (data) {
                        
                        if (data.length > 0) {

                            $("#" + data).css("display", "none");
                        }
                    }
                });

            }
        </script>

        <script type="text/javascript">
  function ajaxSearch()
        {
            var input_data = $('#input-31').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
        $('#chkval').click(function (e) {
                e.preventDefault();
                var input = document.getElementById("input-31").value;
                if (input == '') {
                    alert('Please Type Text For Search!');
                     e.preventDefault();
                }
                else{
                    document.getElementById('my_form').submit();
                }
            });
        </script>
        <script>
            $(document).on("click", function (e) {
                if (!$("#suggestions").is(e.target)) {
                    $("#suggestions").hide();
                }
            });
        </script>
    </body>
</html>