<!DOCTYPE HTML>
<html>
<head>
<title>Pupilclip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-menu.css">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
.widthdrop {
	min-width: auto;
	width: 100%;
}
</style>
<style>
.widthdrop {
	min-width: auto;
	width: 100%;
}
</style>
<!--<script>
$("document").ready(function() {
    setTimeout(function() {
        $("#259").trigger('click');
    },200);
});
</script>-->
<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation--> 
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
   
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="http://livesoftwaresolution.info/PUPILCLIP/">Home</a></li>
    <?php foreach($features as $videofeature) { ?>
          <li> <a href="<?php echo base_url()?>index.php/home/featurevideo/<?php echo $videofeature->name;?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name;?> </a> </li>
		  <?php } ?>
          <li> <a href="<?php echo base_url()?>index.php/home/history/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
		<li> <a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> My Channel </a> </li>
	<li class="divider" role="seperator"></li>
		  <li> <a href="#"><i class="fa fa-th-large nav_icon"></i>SUBSCRIPTIONS </a> </li>
		  <?php foreach(($usersubscription->uploaderdetail) as $uploader) { ?>
		   <li> <a href="<?php echo base_url()?>index.php/home/subscriber/<?php echo $uploader->id;?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $uploader->username; ?> </a> </li>
		   <?php } ?>
  </ul>
</div>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"  > <a href="http://livesoftwaresolution.info/PUPILCLIP">
        <!--<h1>Pupilclip</h1>
        <span>Website</span> </a>--> 
        <img src="<?php echo base_url();?>assets/images/logo.png" width="90" class="img-responsive">
       </a> </div>
      <!--//logo--> 
      <!--search-box-->
     <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off" />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"  style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
           
          </li>
		   <?php if($this->session->userdata('id') == "") {?>
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"> <img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive but"></a> </li>
            <?php } ?>
		   <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/signup"> <img src="<?php echo base_url();?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>
		      <?php foreach($userDetail as $user) { $username = $user->username;$userimg = $user->userLogo; if($userimg== "") { $userimg = "user.png"; }}?>
		   <?php if($this->session->userdata('id')!="") {?>
		   <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url();?>uploads/<?php echo $userimg;?>" class="img-responsive"></div> </a>
            <ul class="dropdown-menu" style="left:initial;right:0;color:#000;">
              <li>
                <div class="notification_header">
                  <h3><?php echo $username;?></h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p><?php echo $this->session->userdata('email');?></p>
                  
                </div>
                <div class="clearfix"></div>
                </a></li>
				<li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/dash">My Account</a> </div>
              </li>
              <li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/logout">Log Out</a> </div>
              </li>
            </ul>
           </li>
		   <?php }?>
        </ul>
        <div class="clearfix"> </div>
      </div>
  
      <div class="profile_details"> 
       
      </div>
      <div class="clearfix"> </div>
    </div>
  </div>
  <div class="uploadwrap">
    <div class="container-fluid" style="margin:30px 0px;">
    <div class="row">
    <div class="col-md-12">
	   <form class="form-inline" id="advancesearch" method="post" action="<?php echo base_url()?>index.php/home/getadvanceresult">
	 
    <div class="blankpage">
    <div class="row">
        <div class="col-md-1"></div>
          <div class="col-md-2" style="padding:0px 5px">
          <div class="selectmenu1advance">
		  <?php
		  if($catids != "")
		   {
		   $people = $catids;
		   }
		   else
		   {
		   $people = $categogyids;
		   }
		  //$people = array("260","259","334");
		 $people2 = explode(",",$people);
		  //print_r($people2);
		  ?>
          <ul>
		   <?php foreach($catmenu as $cat) {?>
		 <li  style="margin-top:5px;class:active;" > 
			<a href="#"><?php echo $cat[title];?><input type="checkbox" class="case" name="clippee" id="<?php echo $cat[id];?>" <?php if (in_array($cat[id], $people2)) {?> checked="checked"<?php }?> style="float:right;" value="<?php echo $cat[id];?>" onChange="getsubcat('<?php echo $cat[id];?>',this.id)"></a>
		  </li>
		  <?php } ?>
		
          </ul>
		  
          </div>
          </div>
           
		   <div class="col-md-2" style="padding:0px 5px">
          <div class="selectmenu1advance">
          <div id="autoresp" class="autoresp">
		  <ul class="fisrt">
        <li><a href="#">--Sub Category--</a></li>
		</ul>
	 </div>
          </div>
          </div>
		   
           <div class="col-md-2" style="padding:0px 5px">
          <div class="selectmenu1advance">
          <div id="autoresp2" class="autoresp2">
		   <ul class="second">
        <li><a href="#">--Sub Category--</a></li>
		</ul>
	 </div>
          </div>
          </div>
           <div class="col-md-2" style="padding:0px 5px">
           <div class="selectmenu1advance">
           <div id="autoresp3" class="autoresp3">
		   <ul class="third">
       		 <li><a href="#">--Sub Category--</a></li>
		   </ul>
	        </div>
            </div>
            </div> 
          <div class="col-md-2" style="padding:0px 5px">
          <div class="selectmenu1advance">
           <div id="autoresp4" class="autoresp4">
		    <ul class="fourth">
       		 <li><a href="#">--Sub Category--</a></li>
			</ul>
	       </div>
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
	  
	  
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <form class="form-inline">
         <div class="col-md-4"></div>
            <div class="col-md-2" style="padding:0 5px;">
              <div class="form-group" style="width:100%;" >
                
                  <select name ="short" class="form-control login" style="margin:10px 0px;width:100%;" >
                  <option value="">Short By</option>
                  <option value="newest">Newest</option>
                  <option value="week">Last Week</option>
                  <option value="month">This Month</option>
                  <option value="year">This Year</option>
                </select>
              </div>
            </div>
            <div class="col-md-2" style="padding:0 5px;">
              <div class="form-group" style="width:100%;" >
                
                  <select class="form-control login" name="filter" style="margin:10px 0px;width:100%;" >
                  <option value="">Filter</option>
                  <option value="today">Today</option>
                  <option value="short">Short</option>
                  <option value="long">Long</option>
                </select>
              </div>
            </div>
             <div class="col-md-4" style="padding-right:0; padding-left:5px;">
             <button type="submit" class="btn menubutpage" onClick="getdata()">Search</button>
            </div>
          </form>
        </div>
      </div>
    </div>
	 
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <div class="blankpage">
    <div class="row">
    <div class="col-md-2">
    <div class="advancemenu">
    <h3>Show results for</h3>
	<?php 
	$catdata = $categorydata['category'];
	if($catdata != "") {
	?>
	<ul style="margin-left:5px;list-style:none; margin-top:10px;">
	<?php foreach($catdata as $value) { ?>
	 <li><a href="#"><?php echo $value;?></a></li>
	<?php }?>
	 	
	</ul>
	<?php } ?>
	 <?php foreach($returnsearch as $searchcat) {?>
	 
    <ul style="margin-left:5px;list-style:none;">
    <?php if($searchcat->sub['child']['child2']['child3']['category_title'] !="") { $ss7 = $searchcat->sub['child']['child2']['child3']['category_title'];$ss8 = preg_replace('/\s+/', '', $ss7);$newssz3 = explode('/',$ss8);$ssss4 = $newssz3[0].$newssz3[1];?>     
 <li><a href="#"><?php echo ucwords($searchcat->sub['child']['child2']['child3']['category_title']);?> </a>
 
 <?php } ?>
   <ul style="margin-left:5px;list-style:none;">
  <?php if($searchcat->sub['child']['child2']['category_title'] !="") { $ss5 = $searchcat->sub['child']['child2']['category_title']; $ss6 = preg_replace('/\s+/', '', $ss5); $newssz2 = explode('/',$ss6);$ssss3 = $newssz2[0].$newssz2[1]; ?>
 <li><a href="#"><?php echo ucwords($searchcat->sub['child']['child2']['category_title']); ?></a>
 
 <?php }?>
  
   <ul style="margin-left:5px;list-style:none;">
   <?php if($searchcat->sub['child']['category_title'] != "") { $ss3 = $searchcat->sub['child']['category_title'];$ss4 = preg_replace('/\s+/', '', $ss3);$newssz1 = explode('/',$string2);$ssss2 = $newssz1[0].$newssz1[1]; ?>
  <li><a href="#"><?php echo ucwords($searchcat->sub['child']['category_title']);?></a>
  
  <?php }?>
  <ul style="margin-left:5px;list-style:none;">
  <?php if($searchcat->sub['category_title'] != "") { $ss = $searchcat->sub['category_title'];$ss2 = preg_replace('/\s+/', '', $ss);$ssz = explode('/',$ss2);$ssss = $ssz[0].$ssz[1];?>

   <li><a href="#"><?php echo ucwords($ssss);?></a>
     <?php } ?>
  <ul style="margin-left:5px;list-style:none;">
  
  <?php $i=1; if($searchcat->category_title !="") { $string1 = $searchcat->category_title; $string2 = preg_replace('/\s+/', '', $string1);$newssz = explode('/',$string2);$ssss1 = $newssz[0].$newssz[1];  ?>
   <li><a href="#"><?php echo ucwords($searchcat->category_title);?></a></li>
   <?php $i++;} ?>
   </ul>
  </li>
  
  </ul>
  </li>
 </ul>
 </li>
 </ul>
 </li>
 </ul>
 <?php } ?>
    </div>
    </div>
    <div class="col-md-10">
    
    <div class="row" style="margin-top:20px;">
          <div class="col-md-10 col-md-offset-1">
            <h3 class="title1"><strong>Result:</strong></h3>
			
          </div>
        </div>
		
		
		
		
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <div class="row">
		
		
		 <?php if(($categorydata['video']) !=""){ foreach(($categorydata['video']) as $key => $vid) {  ?><?php $string = $vid->video_category;$string1 = preg_replace('/\s+/', '', $string); $nnn = explode('/',$string1);  $nh = $nnn[0].$nnn[1];?>
		<div class="<?php echo $nh;?>">
		  
		  
          <div class="col-md-3 col-xs-6">
		  
		  <?php if (getimagesize('http://livesoftwaresolution.info/PUPILCLIP/uploads/images/'.$vid->videothumb) !== false) { $link ="http://livesoftwaresolution.info/PUPILCLIP/uploads/images/".$vid->videothumb;}else{$link = "http://livesoftwaresolution.info/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vid->id;?>"><img src=<?php echo $link; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
             
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"> <?php $vv = $vid->videoname;$length3 = strlen($vid->videoname); if($length3 < 5){
			  $vvname = $vv;}else { $vvname = substr($vv,0,5); $vvname = $vvname."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vid->id;?>"><?php  echo ucwords($vvname);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($vid->videoview == 0) { echo "0"; } else { echo $vid->videoview; }?> Views</a> 
              </p>
			  <?php $vcat = $vid->video_category;$length5 = strlen($vid->video_category); if($length5 < 10){
			  $vvcategory = $vcat;}else { $vvcategory = substr($vcat,0,10); $vvcategory = $vvcategory."..."; }?>
			 	  
			  
			     <p><a href="#"><?php echo ucwords($vvcategory); ?></a></p>
 
 
 <!--<div class="advancepara">
  <?php $vname = $vid->description;$length = strlen($vid->description); if($length < 50){
			  $v = $vname;}else { $v = substr($vname,0,50); $v = $v."..."; }?>
 <?php echo ucwords($v);?>
 </div>-->
 
 </div>
            </div>
          </div>
		  	</div>
			  <?php } } ?>
		
		
		  <?php if($returnsearch !="") { foreach($returnsearch as $key1 => $searchvideo) { 
		  ?>
		  <?php if(($searchvideo->videodata) !=""){ foreach(($searchvideo->videodata) as $key => $vid) {  ?><?php $string = $vid['video_category'];$string1 = preg_replace('/\s+/', '', $string); $nnn = explode('/',$string1); $nh = $nnn[0].$nnn[1];?>
		<div class="<?php echo $nh;?>">
		  
		  
          <div class="col-md-3 col-xs-6">
		  
		  <?php if (getimagesize('http://livesoftwaresolution.info/PUPILCLIP/uploads/images/'.$vid['videothumb']) !== false) { $link ="http://livesoftwaresolution.info/PUPILCLIP/uploads/images/".$vid['videothumb'];}else{$link = "http://livesoftwaresolution.info/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vid['id'];?>"><img src=<?php echo $link; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
             
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"> <?php $vv = $vid['videoname'];$length3 = strlen($vid['videoname']); if($length3 < 5){
			  $vvname = $vv;}else { $vvname = substr($vv,0,5); $vvname = $vvname."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vid['id'];?>"><?php  echo ucwords($vvname);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($vid['totalv'] == 0) { echo "0"; } else { echo $vid['totalv']; }?> Views</a> 
              </p>
			  
			   <?php $vidcat = $vid['video_category'];$lengthvid = strlen($vid['video_category']); if($lengthvid < 10){
			  $vidcat2 = $vidcat;}else { $vidcat3 = substr($vidcat,0,10); $vidcat2 = $vidcat3."..."; }?>
			  
			  
			   <p><a href="#"><?php echo ucwords($vidcat2); ?></a></p>

 
 <!--<div class="advancepara">
  <?php $vname = $vid['description'];$length = strlen($vid['description']); if($length < 50){
			  $v = $vname;}else { $v = substr($vname,0,50); $v = $v."..."; }?>
 <?php echo ucwords($v);?>
 </div>-->
 
 </div>
            </div>
          </div>
		  	</div>
			  <?php } } ?>
			  
		  
		<?php if(($searchvideo->sub['videos']) !=""){ foreach($searchvideo->sub['videos'] as $vvdata) {  ?><?php $stringc = $vvdata['video_category'];$stringcc = preg_replace('/\s+/', '', $stringc);
		$ssza = explode('/',$stringcc);$ssssa = $ssza[0].$ssza[1];
		?>
		<div class="<?php echo $ssssa;?>">
		  <div class="col-md-3 col-xs-6">
		 
		  <?php if (getimagesize('http://livesoftwaresolution.info/PUPILCLIP/uploads/images/'.$vvdata['videothumb']) !== false) { $link2 ="http://livesoftwaresolution.info/PUPILCLIP/uploads/images/".$vvdata['videothumb'];}else{$link2 = "http://livesoftwaresolution.info/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvdata['id'];?>"><img src=<?php echo $link2; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2v = $vvdata['videoname'];$lengthg = strlen($vvdata['videoname']); if($lengthg < 5){
			  $vvname2 = $v2v;}else { $vvname2 = substr($v2v,0,5); $vvname2 = $vvname2."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvdata['id'];?>"><?php echo ucwords($vvname2);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($vvdata['totalsubview'] == 0) { echo "0"; } else { echo $vvdata['totalsubview']; }?> Views</a> 
              </p>
			  
			  
			    <?php $vvdatacat = $vvdata['video_category'];$lengthcow = strlen($vvdata['video_category']); if($lengthcow < 8){
			  $vvdatacat2 = $vvdatacat;}else { $vvdatacat3 = substr($vvdatacat,0,8); $vvdatacat2 = $vvdatacat3."..."; }?>
			  
			   <?php $vvdatacat = $vvdata['video_category'];$lengthvvdatacat = strlen($vvdata['video_category']); if($lengthvvdatacat < 10){
			  $vvdatacat2 = $vvdatacat;}else { $vvdatacat3 = substr($vvdatacat,0,10); $vvdatacat2 = $vvdatacat3."..."; }?>
			   <p><a href="#"><?php echo ucwords($vvdatacat2); ?></a></p>

 
 <!--<div class="advancepara">
 <?php $vname2 = $vvdata['description'];$lengtho = strlen($vvdata['description']); if($lengtho < 50){
			  $v8 = $vname2;}else { $v8 = substr($vname2,0,50); $v8 = $v8."..."; }?>
 <?php echo ucwords($v8);?>
 </div>-->
 
 </div>
            </div>
          </div>
		  </div>
		  
		<?php } }?>	  
			  
			  
			<?php if(($searchvideo->sub['child']['videos']) != "") { foreach(($searchvideo->sub['child']['videos']) as $vvideos) { ?>
		 <?php $str = $vvideos['video_category'];$str2 = preg_replace('/\s+/', '', $str);
		$str3 = explode('/',$str2);$str33 = $str3[0].$str3[1];
		?>
		 <div class="<?php echo $str33;?>">
		  <div class="col-md-3 col-xs-6" >
		 
		  <?php if (getimagesize('http://livesoftwaresolution.info/PUPILCLIP/uploads/images/'.$vvideos['videothumb']) !== false) { $link3 ="http://livesoftwaresolution.info/PUPILCLIP/uploads/images/".$vvideos['videothumb'];}else{$link3 = "http://livesoftwaresolution.info/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvideos['id'];?>"><img src=<?php echo $link3; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2name = $vvideos['videoname'];$length6 = strlen($vvideos['videoname']); if($length6 < 5){
			  $vidname = $v2name;}else { $vidname = substr($v2name,0,5); $vidname = $vidname."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvideos['id'];?>"><?php echo ucwords($vidname);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($vvideos['totalchildview'] == 0) { echo "0"; } else { echo $vvideos['totalchildview']; }?> Views</a> 
              </p>
			  
			    <?php $vvideoscat = $vvideos['video_category'];$lengthgoat = strlen($vvideos['video_category']); if($lengthgoat < 10){
			  $vvideoscat2 = $vvideoscat;}else { $vvideoscat3 = substr($vvideoscat,0,10); $vvideoscat2 = $vvideoscat3."..."; }?>
			  
			  
			   <p><a href="#"><?php echo ucwords($vvideoscat2); ?></a></p>
 
 
 <!--<div class="advancepara">
 <?php $vnamedesc = $vvideos['description'];$length7 = strlen($vvideos['description']); if($length7 < 50){
			  $v9 = $vnamedesc;}else { $v8 = substr($vnamedesc,0,50); $v9 = $v9."..."; }?>
 <?php echo ucwords($v9);?>
 </div>-->
 
 </div>
            </div>
          </div>
		  </div>
		  
		  
		<?php } } ?>
		
			  
		 <?php if(($searchvideo->sub['child']['child2']['vvv']) != "") { foreach(($searchvideo->sub['child']['child2']['vvv']) as $final) { ?>
		  <?php $strgs = $final['video_category'];$strv = preg_replace('/\s+/', '', $strgs);
		$strvs = explode('/',$strv);$str333 = $strvs[0].$strvs[1];
		?>
		 <div class="<?php echo $str333;?>">
		  <div class="col-md-3 col-xs-6" >
		
		  <?php if (getimagesize('http://livesoftwaresolution.info/PUPILCLIP/uploads/images/'.$final['videothumb']) !== false) { $linkfinal ="http://livesoftwaresolution.info/PUPILCLIP/uploads/images/".$final['videothumb'];}else{$linkfinal = "http://livesoftwaresolution.info/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $final['id'];?>"><img src=<?php echo $linkfinal; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2namefinal = $final['videoname'];$lengthfinal = strlen($final['videoname']); if($lengthfinal < 5){
			  $vidnamefinal = $v2namefinal;}else { $vidnamefinal = substr($v2namefinal,0,5); $vidnamefinal = $vidnamefinal."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $final['id'];?>"><?php echo ucwords($vidnamefinal);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($final['totalchild2view'] == 0) { echo "0"; } else { echo $final['totalchild2view']; }?> Views</a> 
              </p>
			  
			    <?php $finalcategory = $final['video_category'];$lengthfinalcat = strlen($final['video_category']); if($lengthfinalcat < 10){
			  $finalcatname = $finalcategory;}else { $finalcatname = substr($finalcatname,0,10); $finalcatname = $finalcatname."..."; }?>
			  
			  
			   <p><a href="#"><?php echo ucwords($finalcatname); ?></a></p>
 
 
 <!--<div class="advancepara">
 <?php $vnamedescfinal = $final['description'];$length7final = strlen($final['description']); if($length7final < 50){
			  $vfinal = $vnamedescfinal;}else { $v8 = substr($vnamedescfinal,0,50); $v9 = $v9."..."; }?>
 <?php echo ucwords($vfinal);?>
 </div>-->
 
 </div>
            </div>
          </div>
		  </div>
		  
		<?php } } ?>		    
		<?php if(($searchvideo->sub['child']['child2']['child3']['video']) != "") { foreach(($searchvideo->sub['child']['child2']['child3']['video']) as $last) { ?>
		<?php $lstr = $last['video_category'];$lstr1 = preg_replace('/\s+/', '', $lstr);
		$lstr2 = explode('/',$lstr1);$lstr3 = $lstr2[0].$lstr2[1];
		?>
		<div class="<?php echo $lstr3;?>">
		  <div class="col-md-3 col-xs-6" >
		
		  <?php if (getimagesize('http://livesoftwaresolution.info/PUPILCLIP/uploads/images/'.$last['videothumb']) !== false) { $linklast ="http://livesoftwaresolution.info/PUPILCLIP/uploads/images/".$last['videothumb'];}else{$linklast = "http://livesoftwaresolution.info/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $last['id'];?>"><img src=<?php echo $linklast; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2namelast = $last['videoname'];$lengthlast = strlen($last['videoname']); if($lengthlast < 5){
			  $vidnamelast = $v2namelast;}else { $vidnamelast = substr($v2namelast,0,5); $vidnamelast = $vidnamelast."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $last['id'];?>"><?php echo ucwords($vidnamelast);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($last['totalchildviewlast'] == 0) { echo "0"; } else { print_r($last['totalchildviewlast']); }?> Views</a> 
              </p>
			  
			  <?php $lastvideocategory = $last['video_category'];$lengthlastcategory = strlen($last['video_category']); if($lengthlastcategory < 10){
			  $vidcategorylast = $lastvideocategory;}else { $vidcategorylast = substr($lastvideocategory,0,10); $vidcategorylast = $vidcategorylast."..."; }?>
			  
			  
			  
			   <p><a href="#"><?php echo ucwords($vidcategorylast); ?></a></p>

 
 <!--<div class="advancepara">
 <?php $vnamedesclast = $last['description'];$length7last = strlen($last['description']); if($length7last < 50){
			  $vlast = $vnamedesclast;}else { $v8last = substr($vnamedesclast,0,50); $v9last = $v9last."..."; }?>
 <?php echo ucwords($vlast);?>
 </div>-->
 
 </div>
            </div>
          </div>
		  </div>
		  
		<?php } }?>		  		    
			  
			  
			  
			  
			  
			  
			  
			  
		 <?php } }?>	
			
			
    
          </div>
        </div>
        </div>
        </div>
        </div>
     </div>
    </div>
    </div> 
  </div>
 
</div>
</div>
<div class="footer">
  <div class="row">
    <div class="col-md-12">
      <div class="footmen">
        <ul>
          <li><a href="#"> About </a></li>
          <li><a href="#"> Press </a></li>
          <li><a href="#"> Copyright </a></li>
          <li><a href="#"> Creators</a></li>
          <li><a href="#"> Advertise</a></li>
          <li><a href="#"> Developers</a></li>
          <li><a href="#">Pupilclipe</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="minfootmen">
        <ul>
          <li><a href="#"> Terms</a></li>
          <li><a href="#"> Privacy</a></li>
          <li><a href="#"> Policy & Safety</a></li>
          <li><a href="#"> Send feedback</a></li>
          <li><a href="#"> Test new features</a></li>
          <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--//footer-->
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script> 
<script src="<?php echo base_url();?>assets/js/fileupload.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 

<script src="<?php echo base_url();?>assets/js/custom.js"></script> 
<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script> 
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/sidebar-menu.js"></script> 
<script>
    $.sidebarMenu($('.sidebar-menu'))
  </script> 
<script>

function getsubcat(x,e)
{
var abc = $("#" +e).prop("checked");
if(abc)
{

var x = x;
 var post_data = {
            'search': x,
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchsubcat",
            data: post_data,
            success: function (data) {
			
				//$( ".fisrt" ).empty();
				//$( ".second" ).empty();
				//$( ".third" ).empty();
				//$( ".fourth" ).empty();
                 if (data.length > 0) {
				
				  var json = $.parseJSON(data);
			
                $.each(json,function(k,v){
				
                    $("#autoresp").append("<ul style='list-style:none;' class='fisrt'><li><a href='#'>"+ v.category_title +"<input name='soneka' class='case' id='"+ v.id +"' type='checkbox' onChange='getsubcat2("+v.id +","+"this.id"+")'"  +"value=" +v.id + " >"+"</a></li></ul>");
                   
                }); 
				 
				
                
                }
                
            }
         });
}
else
{

}
}
function getsubcat2(y,e)
{

var a1 = $("#" +e).prop("checked");

if(a1)
{
var post_data = {
            'search': y,
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchsubcat2",
            data: post_data,
            success: function (response) {
				//$( ".second" ).empty();
				//$( ".third" ).empty();
				//$( ".fourth" ).empty();
				//$( "#addedadded" ).empty();
			
                 if (response.length > 0) {
				 
				  var json = $.parseJSON(response);
                $.each(json,function(k,v){
                    $("#autoresp2").append("<ul style='list-style:none;' class='second'><li><a href='#'>"+ v.category_title +"<input id='"+ v.id +"' type='checkbox' class='case' name='clip1sub2' onChange='getsubcat3("+v.id +","+"this.id"+")'"  +"value=" +v.id + " >"+"</a></li></ul>");
                   
                }); 
				
                }
                
            }
         });
}
else
{
 
}

}

function getsubcat3(z,e)
{

var a2 = $("#" +e).prop("checked");
if(a2)
{
var post_data = {
            'search': z,
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchsubcat3",
            data: post_data,
            success: function (response) {
				//$( ".third" ).empty();
				
				//$( ".fourth" ).empty();
				//$( "#addedcat2" ).empty();
			
                 if (response.length > 0) {
				 var json = $.parseJSON(response);
                $.each(json,function(k,v){
                    $("#autoresp3").append("<ul style='list-style:none;' class='third'><li><a href='#'>"+ v.category_title +"<input id='"+ v.id +"' class='case' type='checkbox' name='clip1sub3' onChange='getsubcat4("+v.id +","+"this.id"+")'"  +"value=" +v.id + " >"+"</a></li></ul>");
                   
                }); 
				 
				
                }
                
            }
         });
}
}

function getsubcat4(z2,e)
{
var a4 = $("#" +e).prop("checked");
if(a4)
{
var post_data = {
            'search': z2,
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchsubcat4",
            data: post_data,
            success: function (response) {
			//$( ".fourth" ).empty();
			//$( "#addedcat3" ).empty();
                 if (response.length > 0) {
				 
				  var json = $.parseJSON(response);
                $.each(json,function(k,v){
                    $("#autoresp4").append("<ul style='list-style:none;' class='fourth'><li><a href='#'>"+ v.category_title +"<input id='"+ v.id +"' type='checkbox' class='case' name='clip1sub4' onChange='getsubcat5("+v.id +","+"this.id"+")'"  +"value=" +v.id + " >"+"</a></li></ul>");
                   
                }); 
				
				
                }
                
            }
         });
}
}
function getsubcat5()
{
//$( "#addedcat4" ).empty();
}

function getsubcatcat(p,e)
{

var gs = $("#myclip2").prop("checked");
if(gs)
{

 var post_data = {
            'search': p,
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchsubcat",
            data: post_data,
            success: function (data) {
			
			//$( ".2fisrt" ).empty();
                 if (data.length > 0) {
			
				  var json = $.parseJSON(data);
			
                $.each(json,function(k,v){
				
                    $("#autorespclip2").append("<ul style='list-style:none;' class='2fisrt'><li><a href='#'>"+ v.category_title +"<input name='clip2sub1' id='"+ v.id +"' class='case' type='checkbox' onChange='getsubcatsecond("+v.id +","+"this.id"+")'"  +"value=" +v.id + " >"+"</a></li></ul>");
                   
                }); 
				 
			
                }
                
            }
         });

}
}
</script>
<script>
function getdata()
{
var cats = [];
 $(".case").each(function() {
            var aa = this.checked;
			var vid = this.value;
			
			if(aa) 
			{
			cats.push(vid);
			 }
		console.log(cats);
		
		document.getElementById('categories').value =  cats;
		
		
		
		
		
});
}
</script>








</body>
</html>