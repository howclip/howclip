<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>HowClip</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>
  </head>
  <body>
<div class="wrapper">
<div class="abouttop">
<div class="container">
<div class="row">
<div class="col-md-3">
 <?php foreach($companydetail as $company) { if($company->company_logo != ""){ $cmplogo = "http://104.168.142.137/PUPILCLIP/Admin/uploads/$company->company_logo"; } else {$cmplogo = "http://104.168.142.137/PUPILCLIP/uploads/logo.png";  } ?>
      <div class="logo"  > <a href="http://104.168.142.137/PUPILCLIP/index.php/welcome">
      
        <img src="<?php echo $cmplogo;?>" class="img-responsive">
       </a> </div>
    <?php } ?>
</div>
<div class="col-md-9">
  <!--search-box-->
     <div class="search-box">
       <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off"  />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>
</div>
</div>
</div>
</div>

<div class="aboutmen">
<div class="container">
<ul>
<li><a href="<?php echo base_url()?>index.php/home/about"> About </a></li>
<li><a href="#"> Press </a></li>
<li><a href="<?php echo base_url()?>index.php/home/terms"> Copyright </a></li>
<li><a href="#"> Safety  </a></li>
<li><a href="#"> Creators </a></li>
<li><a href="#"> Advertise </a></li>
<li><a href="#"> Developers  </a></li>
<li><a href="#"> Help  </a></li>
</ul>
</div>
</div>
<div class="aboutwhole">
<div class="container">
<div class="row">
<div class="col-md-2">
<div class="abouttab">

<h4> About HowClip</h4>

<ul>
<li><a href="<?php echo base_url()?>index.php/home/about"> About HowClip </a></li>
<!--<li><a href="<?php echo base_url()?>index.php/home/gettingstarted"> Getting Started</a></li>-->
<li><a href="#"> Community Guidelines </a></li>
<!--<li><a href="#"> Brand Guidelines  </a></li>
<li><a href="#"> Contact Us </a></li>-->
<li><a href="#"> Careers</a></li>
<li><a href="#">Merchandise </a></li>
</ul>

</div>
</div>
<?php foreach($contactdata as $content) {?>
<div class="col-md-10">
<div class="contentabt">
<div class="row">
<div class="col-md-12">
<h2><?php echo $page_title;?></h2>
<p><?php echo $content->content;?></p>

</div>
</div>




</div>



</div>
</div>
<?php } ?>
</div>
</div>
</div>


</div>
<div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="http://104.168.142.137/PUPILCLIP/index.php/welcome">How Cip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/custom.js"></script>
<script type="text/javascript">

function ajaxSearch()
{
    var input_data = $('#input-31').val();

    if (input_data.length === 0)
    {
        $('#suggestions').hide();
    }
    else
    {

        var post_data = {
            'search_data': input_data,
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchAll",
            data: post_data,
            success: function (data) {
                // return success
                if (data.length > 0) {
				//window.location.href = '<?php echo base_url();?>index.php/home/searchpage';
                    $('#suggestions').show();
                    $('#autoSuggestionsList').addClass('auto_list');
                    $('#autoSuggestionsList').html(data);
                }
            }
         });
		 
		
     }
	 
 }
</script>
 <script>
  $(document).on("click", function(e){
    if( !$("#suggestions").is(e.target) ){ 
        $("#suggestions").hide();
    }
});
  </script>
  </body>
</html>