<!DOCTYPE HTML>
<html>
<head>
<title>Pupilclip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">


<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation-->
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
   <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->
      
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"  > <a href="index.html">
        <!--<h1>Pupilclip</h1>
        <span>Website</span> </a>--> 
        <img src="<?php echo base_url();?>assets/images/logo.png" width="90" class="img-responsive">
       </a> </div>
      <!--//logo--> 
      <!--search-box-->
     <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>Uploading Files</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
           </li>
          <li class="dropdown head-dpdn"> <a href="login.html"> <img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive but"></a> </li>
          <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url();?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
      <!--notification menu end -->
      <div class="profile_details"> 
        
      
      </div>
   
    </div>
    
    
  </div>
  
  <div class="uploadwrap">
  <div class="container">
  <div class="row">
  <div class="col-md-12">
  <div class="blankpage">
  <div class="row">
  <div class="col-md-3">
  <h4>Uploaded Video</h4>
  </div>
  <div class="col-md-7">
  <div class="row">
  <div class="col-md-12">
  <div id="myProgress">
  <div id="myBar"></div>
</div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
<div class="terms"><p>*</p></div>
<div class="hint">Your Videos are still uploading.Please keep this page open until they are done.</div>
</div>
</div>
  </div>
  <div class="col-md-2">
  <a class="btn publish" href="#" role="button">Link</a>
 <a class="btn privacy" href="#" role="button">Link</a>
  </div>
  </div>
  </div>
  </div>
   </div>
  
  <div class="row">
  <div class="col-md-12">
  <div class="blankpage">
  <div class="row">
  <div class="col-md-3">
  <img src="<?php echo base_url();?>assets/images/summer-wallpaper-hd-2.jpg" class="img-responsive">
  <div class="blankhead">Upload Status</div>
  <p style="font-size:11px; color:#999;">vflgbfb</p>
   <p style="font-size:11px; color:#999;">vflgbfbfbkf flkjgblfbm <a href="#">dgv@kjf.com</a></p>
    <div class="blankhead">Upload Status</div>
   
<div class="terms"><p>*</p></div>
<div class="hint" style="width:70%;">Your Videos are still uploading.Please keep this page open until they are done.</div>

  </div>
  <div class="col-md-9">
  <div class="row">
  <div class="col-md-10">
  <div id="myProgress">
  <div id="myBar"></div>
</div>
  </div>
  <div class="col-md-2">
   <a class="btn publish2" href="#" role="button">Link</a>
  
  </div>
  </div>
  <div class="row">
  <div class="col-md-12">
   <ul class="nav nav-tabs uploadtab" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Profile</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Messages</a></li>
    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Settings</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content uplaodtabontent">
    <div role="tabpanel" class="tab-pane active" id="home">
    <div class="row">
    <div class="col-md-7">
    <form>
  <div class="form-group">
 
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="kdvjkvjdfvj">
  </div>
  <div class="form-group">
   
    <textarea class="form-control" rows="2"></textarea>
  </div>
  <div class="form-group">
 
    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="kdvjkvjdfvj">
  </div>
   <h4> Video Thumbnail</h4>
<p>fgdufgduvjhfiy dfj jdfjgvk jdkjvgk</p> 
</form>
    
    
    
    </div>
    <div class="col-md-5">
    <form>
     <div class="row">
    <div class="col-md-12">
    <div class="form-group" >
    <select class="form-control selectuplo">
  <option>1</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
  <option>5</option>
</select>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <p  style="float:left;">Also share on</p>
    <div class="checkbox" style="float:right;margin-top: 0px;">
    <label>
      <input type="checkbox"> <span style="color:#900;"><i class="fa fa-google-plus-square" aria-hidden="true"></i> </span>
    </label>
     <label>
      <input type="checkbox"> <span style="color:#09C;"><i class="fa fa-twitter-square" aria-hidden="true"></i> </span>
    </label>
  </div>
  </div>
  </div>
    <div class="row">
    <div class="col-md-4">
    <img src="<?php echo base_url();?>assets/images/yt-about-index-getting-started.png" class="img-responsive">
    </div>
    <div class="col-md-8">
    <textarea class="form-control" rows="1"></textarea>
    </div>
    </div>
     <div class="row">
    <div class="col-md-12">
    <a class="btn addplay" href="#" role="button">+ Add to playlist</a>
    </div>
    </div>
    </form>
    
    </div>
    </div>
    </div>
    <div role="tabpanel" class="tab-pane" id="profile">hello</div>
    <div role="tabpanel" class="tab-pane" id="messages">heyyyy</div>
    <div role="tabpanel" class="tab-pane" id="settings">hellllloooo</div>
  </div>

</div>
  
  
  
  
  
  </div>
  </div>
  
  
  </div>
  </div>
  </div>
  </div>
  
  
  <div class="row">
  <div class="col-md-12">
  <div class="blankpage">
  <div class="row">
  <div class="col-md-3">
  <img src="<?php echo base_url();?>assets/images/summer-wallpaper-hd-2.jpg" class="img-responsive">
  </div>
  <div class="col-md-7">
  <div class="row">
  <div class="col-md-12">
  <div id="myProgress">
  <div id="myBar"></div>
</div>
</div>
</div>
<div class="row">
  <div class="col-md-12">
<div class="stronghead">Your Videos are still uploading.Please keep this page open .</div>
</div>
</div>

  </div>
  <div class="col-md-2">
  <a class="btn publish" href="#" role="button">Publish</a>

  </div>
  </div>
  <div class="row">
<div class="col-md-12">
<div class="showbox">Show More</div>
<div class="showboxhr"></div>
</div>
</div>
  </div>
  </div>
   </div>
  </div>
  
  
  
 
  </div>
  </div>
  
  
   <!--<div class="footer">
    <div class="row">
      <div class="col-md-12">
        <div class="footmen">
          <ul>
            <li><a href="#"> About </a></li>
            <li><a href="#"> Press </a></li>
            <li><a href="#"> Copyright </a></li>
            <li><a href="#"> Creators</a></li>
            <li><a href="#"> Advertise</a></li>
            <li><a href="#"> Developers</a></li>
            <li><a href="#">Pupilclipe</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="minfootmen">
          <ul>
            <li><a href="#"> Terms</a></li>
            <li><a href="#"> Privacy</a></li>
            <li><a href="#"> Policy & Safety</a></li>
            <li><a href="#"> Send feedback</a></li>
            <li><a href="#"> Test new features</a></li>
            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>-->
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
</body>
</html>