<!DOCTYPE HTML>
<html>
    <head>
        <title>HowClip</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sidebar-menu.css">
        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
        <script>function fbs_click() {
                u = location.href;
                t = document.title;
                window.open('https://www.facebook.com/sharer/sharer.php?u=http%3A//13.59.106.5/index.php/welcome&p[video][0]=<?php echo base_url() ?>/index.php/home/showvideo?id=227');
                return false;
            }</script>
        <style>
            div.scroll
            {
                background-color:#00FFFF;
                width:40%;
                height:300px;
                FLOAT: left;
                margin-left: 5%;
                padding: 1%;
                overflow:scroll;
            }
			.imageuploadtable
			{
			width:23%;
                        padding:10px;
			}
			.headingtable
			{
			    width: 55%;
    vertical-align: top;
    padding: 10px 0px;
}
			.headingtable p
			{
			font-weight:bold;
			font-size:14px;
			}
			.nxthead
			{
			font-size:13px !important;
			}
			.labelformnxt
			{    text-align: center;
    display: block;
    margin: 10px 0;
	font-size:12px;
}
.spconce
{
margin:15px 0 0px 0 !important;
}

.sidevideo
{

}

        </style>
        <style type="text/css">
            .video-js {
                float: left;
                margin: 15px;
                width: 100%;
                height: 380px;
            }
            .vjs-playlist {
                width: 100%;
                float: left;
                margin: 15px;
                height:380px;
            }
        </style>
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">


        <!--//Metis Menu -->
    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 
            <!--left-fixed -navigation-->

            <!--left-fixed -navigation--> 
            <!-- header-starts -->
            <div class="sticky-header header-section ">
                <div class="header-left"> 
                    <!--toggle button start-->
                 <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->

                    <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id') ?>">My Videos</a></li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>">History </a> </li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"> My Channel </a> </li>
                            <li class="divider" role="seperator"></li>
                            <li> <a href="#">SUBSCRIPTIONS </a> </li>
                            <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"> <?php echo $uploader->username; ?> </a> </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php
                    foreach ($companydetail as $company) {
                        if ($company->company_logo != "") {
                            $cmplogo = base_url() . "Admin/uploads/$company->company_logo";
                        } else {
                            $cmplogo = base_url() . "uploads/logo.png";
                        }
                        ?><?php } ?>
                    <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">

                            <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                        </a> </div>

                    <div class="search-box">
                        <form class="input">
                            <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off" />
                            <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                                </svg> </label>
                        </form>

                        <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
                            <div id="autoSuggestionsList"></div>
                        </div>
                    </div>

                    <div class="clearfix"> </div>
                </div>
                <div class="header-right">
                    <div class="profile_details_left"><!--notifications of menu start -->
                        <ul class="nofitications-dropdown">
                            <li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/uploadpage" style="color:#333;" > <img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > </a>

                            </li>
                            <?php
                            foreach ($userDetail as $user) {
                                $username = $user->username;
                                $userimg = $user->userLogo;
                                if ($userimg == "") {
                                    $userimg = "user.png";
                                }
                            }
                            ?>
                       <!-- <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url(); ?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
                            <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                <ul class="dropdown-menu" style="left:initial; right:0;">
                                    <li>
                                        <div class="notification_header">
                                            <h3><?php echo $username; ?></h3>
                                        </div>
                                    </li>
                                    <li><a href="#">
                                            <div class="notification_desc">
                                                <p><?php echo $this->session->userdata('email'); ?></p>

                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                    </li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"> </div>
                    </div>
                </div>
            </div>
            <div class="uploadwrap">
                <div class="container-fluid">
                    <div class="row" style="padding:0px;">
                        <div class="col-md-2" style="padding-right:0px;">
                            <ul class="sidebar-menu">
                                <li class="sidebar-header">HowClip</li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-files-o"></i>
                                        <span>Video Manager</span>
                                    </a>
                                    <ul class="sidebar-submenu" >
                                        <li><a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Videos</a></li>
                                        <li><a href="<?php echo base_url() ?>index.php/home/showplaylist/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Playlists</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>My Channel</span>
                                    </a>

                                </li>

                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/userupdate/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>User Profile</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/changePass/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>Change Password</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/myorders/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>My Orders</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="blankpage">
                                <div>
                                    <ul class="nav nav-tabs myeditab" role="tablist">
                                        <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab"><i class="fa fa-pencil" aria-hidden="true"></i> Info & Setting</a></li>
                                    </ul>

                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="info">

                                            <div class="tabheadmen">
                                                <div class="row">

                                                    <div class="col-md-8">
                                                        <h4><?php echo ucwords($videoDetail['playlistname']); ?></h4>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <!--<a class="btn cancel" href="#" role="button">Cancel</a><a class="btn publish1" href="#" role="button">Publish</a>-->
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-8">
                                                        <video id="myPlayerID"
                                                               data-account="3676484087001"
                                                               data-player="04ac0699-ca6b-449b-a2b6-0f14b4a7893b"
                                                               data-embed="default"
                                                               class="video-js" controls>
                                                        </video>
                                                    </div>
                                                    <div class="col-md-4" style="overflow:hidden">
                                                        <ol class="vjs-playlist sort" style="margin-left:0px;" ></ol>
                                                    </div>
                                                </div>
                                            </div>

                                            <br>
                                            <div class="row">
                                                
                                                <div class="col-md-12">
                                                    
                                                    <ul class="nav nav-tabs uploadtab" role="tablist">
                     <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>

                                                    </ul><br>
                                                    <h4>Edit Playlist</h4>
                                                     <h5>Drag & Drop videos to rearrange!</h5>
                     <div class="tab-content uplaodtabontent">
                                                        <div role="tabpanel" class="tab-pane active" id="home">

                                                            <div class="row blankpage" style="margin:20px 0;">
                                                                <table>
                                                                    <tbody class="trsort">
                                                                <?php $i = 0;
                                                                foreach ($playlist['video'] as $pp) { ?>
                                                                    <!--<div class="row" id="<?php echo $pp['id']; ?>" style="margin:10px 0;">
                                                                        <div class="form-group">
                                                                            <div class="col-md-3">
                        <img src="<?php echo base_url(); ?>/uploads/images/<?php echo $pp['video_img']; ?>" class="img-responsive sidevideo">
                                                                            </div >
                                                                            <div class="col-md-6">
                                                                                <p><?php echo ucwords($pp['videoname']); ?></p>
<?php $result = $this->db->get_where('users', array('id' => $pp['userId']))->result_array(); ?>
                                                                                <p><?php echo ucwords($result[0]['username']); ?>
                                                                            </div>
                                                                            <div class="col-md-3">
 <button class="btn publishedit" style="background-color:#FF0000" type="button" onClick="deletevid('<?php echo $videoDetail['playlistname']; ?>', '<?php echo $pp['id']; ?>');">Delete</button>
                                                                            </div>
                                                                        </div>
                                                                    </div>-->
                                                                
														<div class="col-md-12 container text-center">
                                                                                                                    
                                                                                                                    <tr id="<?php echo $pp['id']; ?>" class="default row blankpage" style="padding-top: 100px;">
															<td class="imageuploadtable">
															
															<img src="<?php echo base_url(); ?>/uploads/images/<?php echo $pp['video_img']; ?>" class="img-responsive sidevideo">
															</td>
															<td class="headingtable">
															<p><?php echo ucwords($pp['videoname']); ?></p>
<?php $result = $this->db->get_where('users', array('id' => $pp['userId']))->result_array(); ?>
                                                                                <div class="nxthead"><?php echo ucwords($result[0]['username']); ?></div>
															</td>
															<td class="buttonstable">
															
															<button class="btn publishedit" style="background-color:#FF0000" type="button" onClick="deletevid('<?php echo $videoDetail['playlistname']; ?>', '<?php echo $pp['id']; ?>');">Delete</button>
															</td>		
															</tr>		
																	
																
																	
														<!--</div>-->	
																	
                                                                                    <?php $i++;
                                                                                } ?>
                                                                    </tbody>		
                                                                </table>
                                                            </div>
                                                                <div class="col-md-12" style="padding:0;">

                                                                    <div style="padding: 0 5px;">
                                                                        <div style="color:#FF0000;text-align:center;" id="formerror"></div>
                                                                        <div style="color:#FF0000;text-align:center;display:none;" id="playlistnameerror"> Enter Playlist Name</div>
                                                                        <div style="color:#FF0000;text-align:center;display:none;" id="clipdescrror">Enter Playlist Privacy</div>
                                                                    </div>

                                                                    <div class="form-group row spconce">
                                                                        <div id="formerror"></div>
                                                                        <div class="col-md-2">
                                                                            <label class="labelformnxt">Name
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-md-10">
  <input type="text" id="name" class="form-control " name="name" placeholder="Name" value="<?php echo ucwords($videoDetail['playlistname']); ?>">
                                                                        </div>
                                                                    </div>
<?php $uid = $this->session->userdata('id');
$pr = $this->db->get_where('playlist',array('playlist_name'=>$videoDetail['playlistname'],'user_id' => $uid))->result_array();

?>
                                                                    <div class="form-group row spconce">
                                                                        <div class="col-md-2">
                                                                            <label class="labelformnxt">Privacy
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-md-10">
                                                                            <select class="form-control selectuplo" name="privacy" id="privacy"> 
                                                                                <option <?php
                                                                                if ($pr[0]['privacy'] == 'public') {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> value="public">Public</option>
                                                                                <option <?php
                                                                                if ($pr[0]['privacy'] == 'unlisted') {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> value="unlisted">Unlisted</option>
                                                                                <option <?php
                                                                                if ($pr[0]['privacy'] == 'private') {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> value="private">Private</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>

                                                                    <div class="form-group row spconce">
                                                                        <div class="col-md-2">
                                                                            <label class="labelformnxt">Price
                                                                            </label>
                                                                        </div>
                                                                        <div class="col-md-10">
                                                                            <input type="text" id="price" class="form-control " name="price" placeholder="price" value="<?php echo $pr[0]['price'];?>">
                                                                        </div>
                                                                    </div>
																	
																	
																	
                                                                    <a class="btn cancelform" href="#" role="button" >Cancel</a>
                                                                        <?php $name= str_replace(" ","_",$videoDetail['playlistname']); ?>
                                                                    <input type="button" name="submit" value="Publish" onClick="editplay('<?php echo $name; ?>')" class="btn publishform"/>

                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>

                                                </div>

                                            </div>


                                        </div>

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="enhancement">

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="audio">

                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="screen">

                                    </div>
                                </div>

                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="footmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                            <li><a href="javascript:void(0);"> Press </a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                            <li><a href="javascript:void(0);"> Creators</a></li>
                            <li><a href="javascript:void(0);"> Advertise</a></li>
                            <li><a href="javascript:void(0);"> Developers</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="minfootmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                            <li><a href="javascript:void(0);"> Privacy</a></li>
                            <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                            <li><a href="javascript:void(0);"> Send feedback</a></li>
                            <li><a href="javascript:void(0);"> Test new features</a></li>
                            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--//footer--> 
    </div>
    <!-- Classie --> 
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
    <script>
                                                                        var menuLeft = document.getElementById('cbp-spmenu-s1'),
                                                                                showLeftPush = document.getElementById('showLeftPush'),
                                                                                body = document.body;

                                                                        showLeftPush.onclick = function () {
                                                                            classie.toggle(this, 'active');
                                                                            classie.toggle(body, 'cbp-spmenu-push-toright');
                                                                            classie.toggle(menuLeft, 'cbp-spmenu-open');
                                                                            disableOther('showLeftPush');
                                                                        };

                                                                        function disableOther(button) {
                                                                            if (button !== 'showLeftPush') {
                                                                                classie.toggle(showLeftPush, 'disabled');
                                                                            }
                                                                        }
    </script> 
    <!--scrolling js--> 
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fileupload.js"></script>
    <!--//scrolling js--> 
    <!-- Bootstrap Core JavaScript --> 
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 
    <script>
                                                                        $(function () {
                                                                            $('#menu').metisMenu({
                                                                                toggle: false // disable the auto collapse. Default: true.
                                                                            });
                                                                        });
    </script>
    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sidebar-menu.js"></script>
    <script>
                                                                        $.sidebarMenu($('.sidebar-menu'))
    </script>
    <script src="<?php echo base_url(); ?>assets/js/playlist.js"></script>
      <!--<script src="//players.brightcove.net/3676484087001/04ac0699-ca6b-449b-a2b6-0f14b4a7893b_default/index.min.js"></script>-->
     
        
    <script type="text/javascript">
                                                                        var videodata = '<?php echo json_encode($videoDetail['url']); ?>';

                                                                        var JSONObject = JSON.parse(videodata);
                                                                        var abc = [];
                                                                        var i = 0;

                                                                        var count = 0;
                                                                        videojs('myPlayerID').ready(function () {
                                                                            var myPlayer = this;

                                                                            myPlayer.playlist([
<?php
$result = count($videoDetail['url']);

for ($i = 0; $i < $result; $i++) {

    $dd = $videoDetail['url'][$i];
    $img = $videoDetail['thumb'][$i];
    $name = $videoDetail['videoname'][$i];

    echo '
                            { 
                            "sources": [{
                              "src": "' . base_url() . '/uploads/live/' . $dd . '", "type": "video/mp4"
                            }],
                            "name": "' . $name . '",
                            "thumbnail": "' . base_url() . '/uploads/images/' . $img . '",
                            "poster": "' . base_url() . '/uploads/images/' . $img . '"
		 
		 
                      }
		  
                    ,';
}
?>



                                                                            ]);

                                                                        });
    </script>
       
        
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script> 
    <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">   
        
        
    <script type="text/javascript">
  
    $(document).ready(function () {
            $('tbody.trsort').sortable();
           // $('ol.sort').sortable();
           // $('tbody.trsort').disableSelection();
        });
        </script>
      <script>
 //For Table Order Save In Database       
  
            $(document).ready(function(){
            $(document).on("mouseout", function()
            {
            //$(document).on('click','#save-reorder',function(){
            //alert("Deff");return false;
            var list = new Array();
            $('.trsort').find('.default').each(function(){
            var id = $(this).attr('id');
            //alert(id);
            list.push(id);
            });
            
            var data = JSON.stringify(list);
               var name = document.getElementById('name').value;
            $.ajax({
            url: "<?php echo base_url() ?>index.php/home/drag_row",
                    type: 'POST', //POST or GET 
                    data: {data:list},// data to send in ajax format or querystring format
                    datatype: 'json',
                    success: function(response) 
                    {
window.location.href = '<?php echo base_url(); ?>index.php/home/edit_playlist/' + name;
                        //console.log(response);
                    //alert(response);
                    }

            });
            });
            });
        </script>
        
    

    <script>

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <script type="text/javascript">

        function ajaxSearch()
        {
            var input_data = $('#input-31').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/search",
                    data: post_data,
                    success: function (data) {
                        // return success
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
    </script>
    <script>
        function updatethumb(x)
        {

            var vid = document.getElementById('video_id').value;

            var post_data = {
                'img': x,
                'video': vid,
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/updatethumb",
                data: post_data,
                success: function (response) {
                    console.log(response);

                }
            });
        }
    </script>
    <script>

        $(document).ready(function () {
            var ee = $('#cat').val();
            $('#category').val(ee);
        });
    </script>
    <script>
        $(document).on("click", function (e) {
            if (!$("#suggestions").is(e.target)) {
                $("#suggestions").hide();
            }
        });
    </script>
    <script>
        function getsubcat(x, e)
        {
            var abc = $("#" + e).prop("checked");
            if (abc)
            {

                var x = x;
                var post_data = {
                    'search': x,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat",
                    data: post_data,
                    success: function (data) {
                        console.log(data);
                        $(".fisrt").empty();
                        $(".second").empty();
                        $(".third").empty();
                        $(".fourth").empty();
                        if (data.length > 0) {

                            var json = $.parseJSON(data);

                            $.each(json, function (k, v) {

                                $("#autoresp").append("<ul style='list-style:none;' class='fisrt'><li><a href='javascript:void(0)'>" + v.category_title + "<input name='category' id='" + v.id + "' type='radio' onChange='getsubcat2(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp").append("<ul style='list-style:none;' class='fisrt' id='addedadded'><li><a href='javascript:void(0)'><input type='text' style='width:100%;position:relative;margin-left:10px;' name='addedcat' id='adcat' placeholder='Add Subcategory'></a></ul>");


                        }

                    }
                });
            } else
            {

            }
        }
        function getsubcat2(y, e)
        {

            var a1 = $("#" + e).prop("checked");

            if (a1)
            {
                var post_data = {
                    'search': y,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat2",
                    data: post_data,
                    success: function (response) {
                        $(".second").empty();
                        $(".third").empty();
                        $(".fourth").empty();
                        $("#addedadded").empty();

                        if (response.length > 0) {

                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp2").append("<ul style='list-style:none;' class='second'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat3(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp2").append("<ul style='list-style:none;' class='second' id='addedcat2'><li><a href='javascript:void(0)'><input type='text' style='width:100%;position:relative;margin-left:10px;' name='addedcat' id='adcat'  placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            } else
            {

            }

        }

        function getsubcat3(z, e)
        {

            var a2 = $("#" + e).prop("checked");
            if (a2)
            {
                var post_data = {
                    'search': z,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat3",
                    data: post_data,
                    success: function (response) {
                        $(".third").empty();

                        $(".fourth").empty();
                        $("#addedcat2").empty();

                        if (response.length > 0) {
                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp3").append("<ul style='list-style:none;' class='third'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat4(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp3").append("<ul style='list-style:none;' class='third' id='addedcat3'><li><a href='javascript:void(0)'><input type='text' name='addedcat' id='adcat' style='width:100%;position:relative;margin-left:10px;' placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            }
        }

        function getsubcat4(z2, e)
        {
            var a4 = $("#" + e).prop("checked");
            if (a4)
            {
                var post_data = {
                    'search': z2,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat4",
                    data: post_data,
                    success: function (response) {
                        $(".fourth").empty();
                        $("#addedcat3").empty();
                        if (response.length > 0) {

                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp4").append("<ul style='list-style:none;' class='fourth'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat5(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp4").append("<ul style='list-style:none;' class='fourth' id='addedcat4'><li><a href='javascript:void(0)'><input type='text' name='addedcat' id='adcat' style='width:100%;position:relative;margin-left:10px;' placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            }
        }
        function getsubcat5()
        {
            $("#addedcat4").empty();
        }
    </script>

    <script>
        function editvideo()
        {
            var addcat = "";
            var video_id = document.getElementById('video_id').value;
            var name = document.getElementById('name').value;
            var description = document.getElementById('desc').value;
            var privacy = document.getElementById('privacy').value;


            var uid = document.getElementById('user_id').value;
            if (name == "")
            {

                document.getElementById('clipnameerror').style.display = "block";
                setTimeout(function () {
                    $('#clipnameerror').fadeOut('fast');
                }, 1000);
                return false;
            } else if (description == "")
            {
                document.getElementById('clipdescrror').style.display = "block";
                setTimeout(function () {
                    $('#clipdescrror').fadeOut('fast');
                }, 1000);
                return false;
            } else {

                var cat_name = $("input[name='category']:checked").val();
                element = document.getElementById('adcat');
                if (element != null) {
                    addcat = element.value;

                } else {

                    addcat = null;
                }
                var post_data = {
                    'video_id': video_id,
                    'name': name,
                    'description': description,
                    'privacy': privacy,
                    'cat_name': cat_name,
                    'addcat': addcat,
                };


                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/videoupload",
                    data: post_data,
                    success: function (data) {
                        window.location.href = '<?php echo base_url(); ?>index.php/home/videoDetail/' + uid;

                    }
                });


            }
        }
    </script>
    <script>
        function deletevid(x, y) {

            var post_data = {
                'video_id': y,
                'play': x,
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/deletevideofromPlaylist",
                data: post_data,
                success: function (data) {
                    if (data.length > 0) {

                        $("#" + data).css("display", "none");
                    }


                }
            });

        }
        function editplay(x) {
            var privacy = document.getElementById('privacy').value;
            var name = document.getElementById('name').value;
            var price = document.getElementById('price').value;
            var post_data = {
                'play': x,
                'privacy': privacy,
                'name': name,
                'price': price

            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/edit_list",
                data: post_data,
                success: function (data) {
                    //console.log(data);return false;
                    if (data.length > 0) {
                     window.location.href = '<?php echo base_url(); ?>index.php/home/edit_playlist/' + name;

                    }

                }
            });

        }
    </script>

   

</body>
</html>