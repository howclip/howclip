<!DOCTYPE HTML>
<html>
<head>
<title>HowClip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-menu.css">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
.widthdrop {
  min-width:auto;
  width:100%;
}

</style>

<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation-->
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
   <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->
      
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
<ul class="dropdown-menu" aria-labelledby="dropdownMenu1" id="mainmenu">
    <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
    <li><a href="<?php echo base_url()?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id')?>">My Videos</a></li>
	<li> <a href="<?php echo base_url()?>index.php/home/history/<?php echo $this->session->userdata('id');?>"> History </a> </li>
	<li> <a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>"> My Channel </a> </li>
	 <li class="divider" role="seperator"></li>
		  <li> <a href="#">SUBSCRIPTIONS </a> </li>
		  <?php foreach(($userSubscription->uploaderdetail) as $uploader) { ?>
		   <li> <a href="<?php echo base_url()?>index.php/home/subscriber/<?php echo $uploader->id;?>"> <?php echo $uploader->username; ?> </a> </li>
		   <?php } ?>
  
  
  </ul>
</div>
      <!--toggle button end--> 
      <!--logo -->
      <?php foreach($companydetail as $company) { if($company->company_logo != ""){ $cmplogo = base_url()."/Admin/uploads/$company->company_logo"; } else {$cmplogo = base_url()."/uploads/logo.png";  } ?>
      <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">
      
        <img src="<?php echo $cmplogo;?>" class="img-responsive">
       </a> </div>
    <?php } ?>
      <!--//logo--> 
      <!--search-box-->
     <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off" />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/uploadpage" style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
            
           </li>
        <?php foreach($userDetail as $user) { $username = $user->username;  $userimg = $user->userLogo;  if($userimg== "") { $userimg = "user.png"; } }?>
      <!-- <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url();?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
       <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url();?>uploads/<?php echo $userimg;?>" class="img-responsive"></div> </a>
           <ul class="dropdown-menu" style="left:initial; right:0;">
              <li>
                <div class="notification_header">
                  <h3><?php echo $username;?></h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p><?php echo $this->session->userdata('email');?></p>
                 
                </div>
                <div class="clearfix"></div>
                </a></li>
				<li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/dash">My Account</a> </div>
              </li>
              <li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/logout">Log Out</a> </div>
              </li>
            </ul>
           </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
    
   
    </div>
    
    
  </div>
  
 <div class="uploadwrap">
  <div class="container-fluid">
  <div class="row" style="padding:0px;">
  <div class="col-md-10 col-md-offset-1">
   <?php $total = 0; foreach($userdata as $user) {
    $totalview = $user->views;
  $total +=$totalview; } ?>
  <div class="blankpage">
<!--<div class="subscribermenu">
  <ul>
   <?php foreach($userdata['subscriber'] as $sub) {?><?php }?>
  <li><a href="#"><i><?php if($sub == "") { echo "0"; } else { echo $sub;}?></i> Subscriber </a></li>
  <li><a href="#"><i class="fa fa-bar-chart" aria-hidden="true"></i>
 <?php if($total == "") { echo "0" ; } else { echo $total;}  ?> View</a></li>
  <li><a href="<?php echo base_url()?>index.php/home/videoDetail/<?php echo $this->session->userdata('id')?>"><i class="fa fa-tasks" aria-hidden="true"></i>
 Video Manager </a></li>
  </ul> 
  </div>-->
  
  <?php foreach(($userSubs->uploaderdetail) as $detail ){
  if (getimagesize(base_url().'/assets/images/'.$detail->banner) !== false) { $banner =base_url()."/assets/images/".$detail->banner;}else{$banner = base_url()."/assets/images/IMG_118883.jpg";}
  
  $uploaderId = $detail->id;
   $uploader1 = $detail->username; if($detail->userLogo == "") { $userlog = "user.png"; } else { $userlog =  $detail->userLogo; } } ?>
  
  <div class="wallpaerban">
  <img src=<?php echo $banner;?> class="img-responsive">
   <div class="wallparerofile">
  <img src="<?php echo base_url();?>uploads/<?php echo $userlog;?>" class="img-responsive">
  </div>
  </div>
  
 <div class="subscribehaed">
 <div class="row">
 <div class="col-md-8">
 <h4><?php echo ucwords($uploader1);?></h4>
 </div>
 <!--<?php foreach($checksubcribe as $g) { ;?>
 
 <div class="col-md-4">
 <div class="btn-group" role="group" aria-label="..." style="float:right;">
 <div id="subscribe" style="display:none">
  <button onClick="updatesubscription('<?php echo $g->subId; ?>');"><img src="<?php echo base_url();?>assets/images/download.jpg" class="img-responsive subscribeornot"></button>
  
 
  </div>
  <div id="unsubscribe">
 
  <button onClick="updateunsubscription('<?php echo $g->subId; ?>');"><img src="<?php echo base_url();?>assets/images/unsubscribe.jpg" class="img-responsive subscribeornot"></button>
  </div>
  <?php } ?>
  <button type="button" class="btn subscribeornot"><?php if($sub == "") { echo "0"; } else { echo $sub;}?></button>
</div>
 </div>-->
 <?php foreach($checksubcribe as $g) { ;?>
 <div class="col-md-4">
 <div class="btn-group" role="group" aria-label="..." style="float:right;">
 <div id="subscribe" style="display:none">
 <a href="#" onClick="updatesubscription('<?php echo $g->subId; ?>');" style="float:none;"><img src="<?php echo base_url();?>assets/images/download.jpg" class="img-responsive subscribeornot"></a>
  <button type="button" class="btn subscribeornot"><?php if($sub == "") { echo "0"; } else { echo $sub;}?></button>
  </div>
  <div id="unsubscribe">
   <a href="#" onClick="updateunsubscription('<?php echo $g->subId; ?>');" style="float:none;"><img src="<?php echo base_url();?>assets/images/unsubscribe.jpg" class="img-responsive subscribeornot"></a>
  <button type="button" class="btn subscribeornot"><?php if($sub == "") { echo "0"; } else { echo $sub;}?></button>
 </div>
 
</div>
 </div>
 
 <?php } ?>
 
 
 </div>
 <div class="row">
 <div class="col-md-12">
 <ul class="nav nav-tabs uploadtab" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Videos</a></li>
<li role="presentation"><a href="#about" aria-controls="about" role="tab" data-toggle="tab">About</a></li>
  </ul>
 
 
 </div>
 </div>
 </div>
 
  </div>
 
   <div class="tab-content">
   <div role="tabpanel" class="tab-pane active" id="home">
   <div class="row">
   <div class="col-md-9" style="padding: 0px 5px 0 0;">
    <div class="blankpage">
  <!--<div class="dropdown">
  <button class="btn allactivity dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    All Activities
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
  <hr>-->
  <?php foreach(($userSubs->uploaderdetail) as $up) { if($up->userLogo == ""){  $uimg = "user.png";}else { $uimg = $up->userLogo; } } foreach(($userSubs->uploader) as $videos) { ?>
  <div class="media">
  <div class="media-left">
    <a href="#">
      <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $uimg;?>" width="60" height="60" alt="...">
    </a>
  </div>
  <div class="media-body">
  <h4 class="media-heading subscriberheading"><?php echo ucwords($uploader1);?> <span style="font-size:12px; color:#999;"> Uploaded a Video</span><span style="color:#ddd;margin-left:10px;font-size:12px;"><?php  $date = explode('-',$videos->Date);
   if($date[1] == '01')
   { $month = 'Jan'; }  if($date[1] == '02'){ $month = 'Feb'; } if($date[1] == '03'){ $month = 'Mar'; } if($date[1] == '04'){ $month = 'Apr'; }if($date[1] == '05'){ $month = 'May'; } if($date[1] == '06'){ $month = 'Jun'; } if($date[1] == '07'){ $month = 'Jul'; } if($date[1] == '08'){ $month = 'Aug'; } if($date[1] == '09'){ $month = 'Sep'; } if($date[1] == '10'){ $month = 'Oct'; } if($date[1] == '11'){ $month = 'Nov'; }  if($date[1] == '12'){ $month = 'Dec'; }  

 echo $date[2]. " " .$month. " ".$date[0]; ?></span></h4>
    <div class="row filtercontent">
	<?php if (getimagesize(base_url().'/uploads/images/'.$videos->video_img) !== false) { $link =base_url()."/uploads/images/".$videos->video_img;}else{$link = base_url()."/uploads/images/download.jpg";} ?>
  <div class="col-md-4">
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $videos->id;?>"> <img src="<?php echo $link;?>" width="400px" class="img-responsive">
 </a>
  </div>
  <div class="col-md-8">
  <h4><a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $videos->id;?>"><?php echo ucwords($videos->videoname);?></a></h4>
  <ul>
  <li><a href="#"><?php  $date = explode('-',$videos->Date);
   if($date[1] == '01')
   { $month = 'Jan'; }  if($date[1] == '02'){ $month = 'Feb'; } if($date[1] == '03'){ $month = 'Mar'; } if($date[1] == '04'){ $month = 'Apr'; }if($date[1] == '05'){ $month = 'May'; } if($date[1] == '06'){ $month = 'Jun'; } if($date[1] == '07'){ $month = 'Jul'; } if($date[1] == '08'){ $month = 'Aug'; } if($date[1] == '09'){ $month = 'Sep'; } if($date[1] == '10'){ $month = 'Oct'; } if($date[1] == '11'){ $month = 'Nov'; }  if($date[1] == '12'){ $month = 'Dec'; }  

 echo $date[2]. " " .$month. " ".$date[0];?> </a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> <?php if($videos->videoview == "") {echo "0" ; } else { echo $videos->videoview;}  ?> View</a></li>
  </ul>
 <!-- <p><a href="#"> Video paragraph will be here...</a></p>-->
  <p><a href="#"><?php $vname = $videos->description; $length = strlen($videos->description); if($length < 100){
			  $v = $vname; }else { $v = substr($vname,0,100); $v = $v."..."; } echo $v;?></a></p>
  </div>
  
  
</div>  
    
    
  </div>
</div>
  
    <?php } ?>
 
  
  
  <!--<div class="media">
  <div class="media-left">
    <a href="#">
      <img class="media-object" src="<?php echo base_url();?>assets/images/37610397-good-wallpapers.jpg" width="60" height="60" alt="...">
    </a>
  </div>
  <div class="media-body">
  <h4 class="media-heading subscriberheading">Media heading <span style="font-size:12px; color:#999;">Uploaded a Video</span><span style="color:#ddd;margin-left:10px;font-size:12px;">10 months ago</span></h4>
    <div class="row filtercontent">
  <div class="col-md-4">
  <img src="<?php echo base_url();?>assets/images/36569259-good-wallpapers.jpg" width="400px"class="img-responsive">
  </div>
  <div class="col-md-8">
  <h4><a href="#">Video Name </a></h4>
  <ul>
  <li><a href="#">4 Years ago </a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> 477 views</a></li>
  </ul>
  <p><a href="#"> Video paragraph will be here...</a></p>
  <p><a href="#">PHP is one of the most useful languages to know and is used everywhere you look online. In this tutorial, I start from the beginning ...</a></p>
  </div>
  
  
</div>  
    
    
  </div>
</div>-->

<!--<div class="media">
  <div class="media-left">
    <a href="#">
      <img class="media-object" src="<?php echo base_url();?>assets/images/37610397-good-wallpapers.jpg" width="60" height="60" alt="...">
    </a>
  </div>
  <div class="media-body">
  <h4 class="media-heading subscriberheading">Media heading <span style="font-size:12px; color:#999;">Uploaded a Video</span><span style="color:#ddd;margin-left:10px;font-size:12px;">10 months ago</span></h4>
    <div class="row filtercontent">
  <div class="col-md-4">
  <img src="<?php echo base_url();?>assets/images/36569259-good-wallpapers.jpg"width="400px" class="img-responsive">
  </div>
  <div class="col-md-8">
  <h4><a href="#">Video Name </a></h4>
  <ul>
  <li><a href="#">4 Years ago </a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> 477 views</a></li>
  </ul>
  <p><a href="#"> Video paragraph will be here...</a></p>
  <p><a href="#">PHP is one of the most useful languages to know and is used everywhere you look online. In this tutorial, I start from the beginning ...</a></p>
  </div>
  
  
</div>  
    
    
  </div>
</div>-->
<!--<div class="media">
  <div class="media-left">
    <a href="#">
      <img class="media-object" src="<?php echo base_url();?>assets/images/37610397-good-wallpapers.jpg" width="60" height="60" alt="...">
    </a>
  </div>
  <div class="media-body">
  <h4 class="media-heading subscriberheading">Media heading <span style="font-size:12px; color:#999;">Uploaded a Video</span><span style="color:#ddd;margin-left:10px;font-size:12px;">10 months ago</span></h4>
    <div class="row filtercontent">
  <div class="col-md-4">
  <img src="<?php echo base_url();?>assets/images/36569259-good-wallpapers.jpg"width="400px" class="img-responsive">
  </div>
  <div class="col-md-8">
  <h4><a href="#">Video Name </a></h4>
  <ul>
  <li><a href="#">4 Years ago </a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> 477 views</a></li>
  </ul>
  <p><a href="#"> Video paragraph will be here...</a></p>
  <p><a href="#">PHP is one of the most useful languages to know and is used everywhere you look online. In this tutorial, I start from the beginning ...</a></p>
  </div>
  
  
</div>  
    
    
  </div>
</div>-->
</div>
</div>

<div class="col-md-3" style="padding: 0 0 0 5px;">
 <div class="blankpage">
 <?php  foreach($AllSubscription as $allsub) { if($allsub['userLogo'] == ""){$llogo = "user.png"; }else { $llogo = $allsub['userLogo'];}?>
 <div class="media" style="margin:0px;">
  
  <div class="media-left">
    <a href="#">
     <img class="media-object" src="<?php echo base_url();?>uploads/<?php echo $llogo;?>" width="50" height="50" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading subscriberheading"><?php echo ucwords($allsub['username']);?></h4>
	<div id="subscribe">
	<div class="sub" id="<?php echo "sub".$allsub['id'];?>">
   <a class="btn allactivitysubs" id="<?php echo "sub".$allsub['id'];?>" href="#" onClick="updatesubscription1('<?php echo $allsub['id'];?>','subscribe','<?php echo $allsub['username'];?>');" role="button">Subscribe</a>
   </div>
   <div class="unsub" id="<?php echo "unsub".$allsub['id'];?>" style="display:none;">
   <a class="btn allactivitysubs" id="<?php echo "unsub".$allsub['id'];?>" href="#" onClick="updatesubscription1('<?php echo $allsub['id'];?>','unsubscribe','<?php echo $allsub['username'];?>');" role="button">UnSubscribe</a>
   </div>
   </div>
  </div>
</div>
<?php } ?>

<!--<div class="media" style="margin:0px;">
  <div class="media-left">
    <a href="#">
     <img class="media-object" src="<?php echo base_url();?>assets/images/37610397-good-wallpapers.jpg" width="50" height="50" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading subscriberheading">Media heading</h4>
   <a class="btn allactivitysubs" href="#" role="button">Subscribe</a>
  </div>
</div>-->

<!--<div class="media" style="margin:0px;">
  <div class="media-left">
    <a href="#">
     <img class="media-object" src="<?php echo base_url();?>assets/images/37610397-good-wallpapers.jpg" width="50" height="50" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading subscriberheading">Media heading</h4>
   <a class="btn allactivitysubs" href="#" role="button">Subscribe</a>
  </div>
</div>-->
<!--<div class="media" style="margin:0px;">
  <div class="media-left">
    <a href="#">
     <img class="media-object" src="<?php echo base_url();?>assets/images/37610397-good-wallpapers.jpg" width="50" height="50" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading subscriberheading">Media heading</h4>
   <a class="btn allactivitysubs" href="#" role="button">Subscribe</a>
  </div>
</div>-->



 </div>
</div>

</div>


</div>





<div role="tabpanel" class="tab-pane" id="profile">
 <div class="blankpage">

<!--<div class="row" style="border-bottom:1px solid #ddd;padding-bottom:10px;">
<div class="col-md-4">
<h4><a class="btn btn-default" href="#" role="button"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i>
</a> Uploads</h4>
</div>
<div class="col-md-5">

</div>
<div class="col-md-2">
<div class="dropdown" style="float:right;">
  <button class="btn allactivity dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
  Date added (newest)
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
</div>
<div  class="col-md-1">
<div class="dropdown" style="float:right;">
  <button class="btn allactivity dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
   Grid
    <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="left:initial;right:0;">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
</div>

</div>-->


<div class="row">
<?php foreach(($userSubs->uploader) as $video) {  if (getimagesize(base_url().'/uploads/images/'.$video->video_img) !== false) { $link =base_url()."/uploads/images/".$video->video_img;}else{$link = base_url()."/uploads/images/download.jpg";} ?>
 <?php $vname = $video->videoname;$length = strlen($video->videoname); if($length < 10){
			  $v = $vname;}else { $v = substr($vname,0,10); $v = $v."..."; }?>
          <div class="col-md-3 col-xs-6">
            <div class="thumbnail"> <a href="#"><img src=<?php echo $link;?> class="img-responsive"></a>
              <div class="victo"><?php $duration = explode(':',$video->video_duration); if($duration[0] == '0') { echo $videolong = $duration[1].":".$duration[2];}else { echo $videolong = $duration[0].":".$duration[1].":".$duration[2]; }?></div>
              <div class="caption catch">
                <h3><a href="#"><?php echo $v;?></a></h3>
				
                <p><a href="#"><?php echo ucwords($video->video_category); ?></a></p>
                <p><a href="#" class="btn btm" role="button"><?php if($video->videoview == "") { echo "0";} else { echo ucwords($video->videoview); }?> Views</a> 
                <a href="#" class="btn ltm" role="button"><?php  $date = explode('-',$video->Date);
   if($date[1] == '01')
   { $month = 'Jan'; }  if($date[1] == '02'){ $month = 'Feb'; } if($date[1] == '03'){ $month = 'Mar'; } if($date[1] == '04'){ $month = 'Apr'; }if($date[1] == '05'){ $month = 'May'; } if($date[1] == '06'){ $month = 'Jun'; } if($date[1] == '07'){ $month = 'Jul'; } if($date[1] == '08'){ $month = 'Aug'; } if($date[1] == '09'){ $month = 'Sep'; } if($date[1] == '10'){ $month = 'Oct'; } if($date[1] == '11'){ $month = 'Nov'; }  if($date[1] == '12'){ $month = 'Dec'; }  

 echo $date[2]. " " .$month. " ".$date[0];; ?></a></p>
              </div>
            </div>
          </div>
		  <?php } ?>
        
          <div style="width:auto; float: right; padding:10px; background-color:#eee" align="bottom">
 <b>Page : 
  <?php  $uploaderId;  
    $cnd = "WHERE `userId` = $uploaderId order by `id` DESC" ;
        $sql = mysql_query("select * FROM video $cnd  ");
        $numrecords=mysql_num_rows($sql);
        for($i=0;$i<=($numrecords/10);$i++)
        {
          echo "<a href='?page=".($i)."'>".($i+1)." </a>";
        }
        ?>
    </b>

</div>

        </div>
		
</div>
		
</div>
<div role="tabpanel" class="tab-pane" id="about">
<?php foreach($userSubs->uploaderdetail as $userdata) {?>
<div class="blankpage">
<div class="row">
<label>History:</label>
<p><span>
<?php echo $userdata->history;?>
<span>
</p>
</div>

<div class="row" style="margin-top:10px;">
<label>Bio:</label>
<p><span>
<?php echo $userdata->bio;?>
<span>
</p>
</div>

<div class="row" style="margin-top:10px;">
<label>Strategy:</label>
<p><span>
<?php echo $userdata->strategy;?>
<span>
</p>
</div>

<div class="row" style="margin-top:10px;">
<label>Motivation:</label>
<p><span>
<?php echo $userdata->motivation;?>
<span>
</p>
</div>

<div class="row" style="margin-top:10px;">
<label>Philosophy:</label>
<p><span>
<?php echo $userdata->philosophy;?>
<span>
</p>
</div>

</div>
<?php }?>
</div>












</div>










 
  </div>
  </div>
  </div>
  </div>
  <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<script src="<?php echo base_url();?>assets/js/fileupload.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
 <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/sidebar-menu.js"></script>
  <script>
    $.sidebarMenu($('.sidebar-menu'))
  </script>
  <script>
function updatesubscription(x)
{

var post_data = {
            'uploader': x,
			
            };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/subscribe",
            data: post_data,
            success: function (response) {
              
					 $('#subscribe').hide();
					 $('#unsubscribe').show();
					  //console.log(response);
                
            }
         });
}
function updateunsubscription(x)
{
//alert(x);return false;
var post_data = {
            'uploader': x,
			
            };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/unsubscribe",
            data: post_data,
            success: function (response) {
               console.log(response);
			    if (response.length > 0) {
                 window.location.href = '<?php echo base_url();?>index.php/home/dash';
					 $('#subscribe').show();
					 $('#unsubscribe').hide();
					 
                }
                
            }
         });
}

function updatesubscription1(x,y,z)
{
//alert(x);
var condition = y;
var url = "<?php echo base_url()?>index.php/home/"+y;
//alert(url);return false;
var post_data = {
            'uploader': x,
            };
        $.ajax({
            type: "POST",
            url: url,
            data: post_data,
            success: function (response) {
               console.log(response);
			   if (response.length > 0) {
                   // alert('yes');
					if(condition == "subscribe")
					{
					
					$("#mainmenu").append("<li id='" + x +"'> <a href='<?php echo base_url()?>index.php/home/subscriber/" +x + "'> " + z + " </a></li>");
					$('#sub'+x).hide();
					 $('#unsub'+x).show();
					}
					else
					{
					 $("#" + x ).remove();
					 $('#sub'+x).show();
					 $('#unsub'+x).hide();
					}
					
                }
                
            }
         });
}
function updateunsubscription1(x)
{
alert(x);return false;
var post_data = {
            'uploader': x,
            };
        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/unsubscribe",
            data: post_data,
            success: function (response) {
               console.log(response);
			    if (response.length > 0) {
                   //window.location.href = '<?php echo base_url();?>index.php/home/dash';
					 $('#sub').show();
					 $('#unsub').hide();
                }
                
            }
         });
}


</script>
 <script type="text/javascript">

function ajaxSearch()
{
    var input_data = $('#input-31').val();

    if (input_data.length === 0)
    {
        $('#suggestions').hide();
    }
    else
    {

        var post_data = {
            'search_data': input_data,
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/search",
            data: post_data,
            success: function (data) {
                // return success
                if (data.length > 0) {
                    $('#suggestions').show();
                    $('#autoSuggestionsList').addClass('auto_list');
                    $('#autoSuggestionsList').html(data);
                }
            }
         });
		 
		
     }
	 
 }
</script>


</body>
</html>