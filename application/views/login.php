<!DOCTYPE HTML>
<html>
   <head>
      <title>HowClip</title>
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
         SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
      <style>
         .panel {
         background-color: #444;
         height: 34px;
         padding: 10px;
         }
         .panel a#login_pop, .panel a#join_pop {
         border: 2px solid #aaa;
         color: #fff;
         display: block;
         float: right;
         margin-right: 10px;
         padding: 5px 10px;
         text-decoration: none;
         text-shadow: 1px 1px #000;
         -webkit-border-radius: 10px;
         -moz-border-radius: 10px;
         -ms-border-radius: 10px;
         -o-border-radius: 10px;
         border-radius: 10px;
         }
         a#login_pop:hover, a#join_pop:hover {
         border-color: #eee;
         }
         .overlay {
         background-color: rgba(0, 0, 0, 0.6);
         bottom: 0;
         cursor: default;
         left: 0;
         opacity: 0;
         position: fixed;
         right: 0;
         top: 0;
         visibility: hidden;
         z-index: 1;
         -webkit-transition: opacity .5s;
         -moz-transition: opacity .5s;
         -ms-transition: opacity .5s;
         -o-transition: opacity .5s;
         transition: opacity .5s;
         }
         .overlay:target {
         visibility: visible;
         opacity: 1;
         }
         .popup {
         min-width:450px;
         background-color: #fff;
         border: 3px solid #fff;
         display: inline-block;
         left: 50%;
         opacity: 0;
         padding: 15px;
         position: fixed;
         text-align: justify;
         top: 20%;
         visibility: hidden;
         z-index: 10;
         -webkit-transform: translate(-50%, -50%);
         -moz-transform: translate(-50%, -50%);
         -ms-transform: translate(-50%, -50%);
         -o-transform: translate(-50%, -50%);
         transform: translate(-50%, -50%);
         -webkit-border-radius: 10px;
         -moz-border-radius: 10px;
         -ms-border-radius: 10px;
         -o-border-radius: 10px;
         border-radius: 10px;
         -webkit-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
         -moz-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
         -ms-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
         -o-box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
         box-shadow: 0 1px 1px 2px rgba(0, 0, 0, 0.4) inset;
         -webkit-transition: opacity .5s, top .5s;
         -moz-transition: opacity .5s, top .5s;
         -ms-transition: opacity .5s, top .5s;
         -o-transition: opacity .5s, top .5s;
         transition: opacity .5s, top .5s;
         }
         .overlay:target+.popup {
         top: 50%;
         opacity: 1;
         visibility: visible;
         }
         .close {
         background-color: rgba(0, 0, 0, 0.8);
         height: 30px;
         line-height: 30px;
         position: absolute;
         right: 0;
         text-align: center;
         text-decoration: none;
         top: -15px;
         width: 30px;
         -webkit-border-radius: 15px;
         -moz-border-radius: 15px;
         -ms-border-radius: 15px;
         -o-border-radius: 15px;
         border-radius: 15px;
         }
         .close:before {
         color: rgba(255, 255, 255, 0.9);
         content: "X";
         font-size: 24px;
         text-shadow: 0 -1px rgba(0, 0, 0, 0.9);
         }
         .close:hover {
         background-color: rgba(64, 128, 128, 0.8);
         }
         .popup p, .popup div {
         margin-bottom: 10px;
         }
         .popup label {
         display: inline-block;
         text-align: left;
         width: 120px;
         }
         .popup input[type="text"], .popup input[type="password"] {
         border: 1px solid;
         border-color: #999 #ccc #ccc;
         margin: 0;
         padding: 2px;
         -webkit-border-radius: 2px;
         -moz-border-radius: 2px;
         -ms-border-radius: 2px;
         -o-border-radius: 2px;
         border-radius: 2px;
         }
         .popup input[type="text"]:hover, .popup input[type="password"]:hover {
         border-color: #555 #888 #888;
         }
      </style>
      <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
      <!-- Bootstrap Core CSS -->
      <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
      <!-- Custom CSS -->
      <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
      <!-- font CSS -->
      <!-- font-awesome icons -->
      <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
      <!-- //font-awesome icons -->
      <!-- js-->
      <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
      <!--webfonts-->
      <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
      <!--//webfonts-->
      <!--animate-->
      <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
      <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
      <script>
         new WOW().init();
      </script>
      <!--//end-animate-->
      <!-- Metis Menu -->
      <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
      <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
      <style>
         input[type=text] {
         width: 130px;
         box-sizing: border-box;
         border: 2px solid #ccc;
         border-radius: 10px;
         font-size: 14px;
         background-color: white;
         background-image: url('searchicon.png');
         background-position: 10px 10px; 
         background-repeat: no-repeat;
         padding: 8px 20px 8px 40px;
         -webkit-transition: width 0.4s ease-in-out;
         transition: width 0.4s ease-in-out;
         }
         input[type=text]:focus {
         width: 100%;
         border-radius: 10px;
         outline:0 !important; 
         }
         .search-box input[type=text] {
         width: 50%;
         float:left;
         box-sizing: border-box;
         border: 2px solid #ccc;
         border-radius: 25px;
         font-size: 14px;
         background-color: white;
         background-image: url(https://www.w3schools.com/howto/searchicon.png);
         background-position: 10px 10px;
         background-repeat: no-repeat;
         padding: 8px 20px 8px 40px;
         -webkit-transition: width 0.4s ease-in-out;
         transition: width 0.4s ease-in-out;
         }
         .search-box input[type=text]:focus {
         outline:0;
         }
         .searchbut
         {
         padding: 5px 15px;
         background: #ddd;
         color:#000;
         float:left;
         margin: 0 4px;
         margin-top: 5px;
         border-radius:8px;
         display:inline-block;
         font-size: 13px;
         }
         .searchbut:hover ,.searchbut:focus,.searchbut:active:focus{
         background: #ccc;
         color:#000;
         outline:0;
         }
         .advance{
         background: #ddd;
         color:#d60808;
         margin-top: 5px;
         padding: 5px 15px;
         border-radius:8px;
         display:inline-block;
         font-size: 13px;
         font-weight:bold;
         }
      </style>
      <style>
      </style>
      <!--//Metis Menu -->
   </head>
   <body class="cbp-spmenu-push">
      <div class="main-content">
         <!--left-fixed -navigation-->
         <div class=" sidebar" role="navigation">
            <div class="navbar-collapse">
               <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
                  <ul class="nav showone" id="side-menu">
                     <li> <a href="#" class="active"><i class="fa fa-home nav_icon"></i> Home </a> </li>
                    
                     <?php foreach ($features as $videofeature) { 

                     $name= str_replace(" ","_",$videofeature->name); ?>
                     
                     <li> 
                        <a href="<?php echo base_url() ?>index.php/welcome/featurevideo/<?php echo $name; ?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name; ?> </a> 
                     </li>
                     <?php
                      }
                       ?>
                     
                     <?php if ($this->session->userdata('id') != '') { ?>   
                     <li> <a href="<?php echo base_url() ?>index.php/welcome/history/<?php echo $this->session->userdata('id'); ?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
                     <?php } ?>

                     <?php if ($this->session->userdata('id') == '') { ?>   
                     
                     <li> <a href="javascript:void(0);"><i class="fa fa-th-large nav_icon"  data-content="Please Sign In!"></i> History </a> </li>
                     <?php } ?>
                     <!--<li> <a href="<?php echo base_url() ?>index.php/home/advanceSearch"><i class="fa fa-th-large nav_icon"></i> Advanced Search </a> </li>-->
                  </ul>
                  <!-- <a href="<?php echo base_url() ?>index.php/home/loginpage">  <img src="<?php echo base_url(); ?>assets/images/signinbutsec.png" class="img-responsive butside"></a>
                     <a href="#">  <img src="<?php echo base_url(); ?>assets/images/monetize.png" class="img-responsive butsidemonet"></a>-->
                  <!--<a class="btn monetize" href="#" role="button">Monetize</a>-->
                  <?php if ($this->session->userdata('id') == "") { ?>
                  <?php
                     if ($this->session->flashdata('regerr')) {
                         ?>
                  <div style="color:#FF0000;text-align:center;" > <?php echo $this->session->flashdata('regerr'); ?></div>
                  <?php } ?>    
                  <div style="color:#FF0000;text-align:center;" id="formerror"></div>
                  <div style="color:#FF0000;text-align:center;display:none;" id="fnameerror">Enter First Name</div>
                  <div style="color:#FF0000;text-align:center;display:none;" id="lnameerror">Enter Last Name</div>
                  <div style="color:#FF0000;text-align:center;display:none;" id="mailerror">Enter Email Address</div>
                  <div style="color:#FF0000;text-align:center;display:none;" id="passerror">Enter Password</div>
                  <p style="color:#C00329;font-size:16px;margin:0;padding:0 0 0 15px; text-align:center;">Sign up with </p>
                  <a class="btn signup" id="loginBtn" href="#" role="button"> <i class="fa fa-facebook" aria-hidden="true"></i>Facebook</a>
                  <form class="logform" onSubmit="return validation()" name="form" method="post" action="<?php echo base_url() ?>index.php/welcome/register">
                     <div class="row" style="margin:5px 0px;">
                        <div class="col-md-6" style="padding-right:5px;padding-left:0px;">
                           <div class="form-group">
                              <input type="name" class="form-control" value="<?php echo set_value('fname'); ?>" name="fname" id="exampleInputFName" placeholder="First">
                           </div>
                        </div>
                        <div class="col-md-6" style="padding-left:5px;padding-right:0px;">
                           <div class="form-group">
                              <input type="name" class="form-control" name="lname" id="exampleInputLName" placeholder="Last">
                           </div>
                        </div>
                     </div>
                     <div class="form-group">
                        <input type="name" class="form-control" name="email" id="email_address" placeholder="Email">
                        <p id="error" style="display:none;color:red;">Wrong email</p>
                     </div>
                     <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password"  placeholder="Password">
                     </div>
                     <!--onChange="validate_pass(this)"-->
                     <p style=" color: #999; font-size: 8px;margin: 0;padding: 0;">By logging in,you agree to our <a href="<?php echo base_url() ?>index.php/welcome/terms">Terms of Service</a> and <a href="<?php echo base_url() ?>index.php/welcome/terms">Privacy Policy</a></p>
                     <div id="lengtherr" style="display:none;color:#FF0000;">
                        <p style="color:#FF0000">Your password must be at least 8 characters!</p>
                     </div>
                     <button type="submit" class="btn join">Sign Up</button>
                  </form>
                  <hr>
                  <p>Sign in now to see your Channel & recommendation </p>
                  <?php } ?>
                  <div class="clearfix"> </div>
                  <ul id="menu" class="dropmen">
                     <?php
                        foreach ($categories as $menu) {
                            ?>
                     <li>
                        <a href="#"><?php echo $menu['title']; ?> <span class="fa arrow"></span></a>
                        <ul class="scrolldiv">
                           <?php
                              if ($menu['submenu'] != '') {
                                  foreach ($menu['submenu'] as $sub) {
                                      ?>
                           <li><a href="<?php echo base_url() ?>index.php/welcome/categorysearch/<?php echo $sub['title']; ?>"><?php echo $sub['title']; ?></a></li>
                           <?php
                              }
                              }
                              ?>
                        </ul>
                     </li>
                     <?php } ?>
                  </ul>
               </nav>
            </div>
         </div>
         <div class="sticky-header header-section ">
            <div class="header-left">
               <button id="showLeftPush"><i class="fa fa-bars"></i></button>
               <?php
                  foreach ($companydetail as $company) {
                      if ($company->company_logo != "") {
                          $cmplogo1 = base_url() . "Admin/uploads/$company->company_logo";
                      } else {
                          $cmplogo1 = base_url() . "uploads/logo.png";
                      }
                      ?>
               <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">
                  <img src="<?php echo $cmplogo1; ?>" class="img-responsive">
                  </a> 
               </div>
               <?php } ?>
              
               <form class="input" method="post" id="my_form" action="<?php echo base_url() ?>index.php/welcome/getsearchdata">
                  <div class="search-box">
                     <input type="text" style="width:55%;" id="input-31" onKeyUp="ajaxSearch();" name="search_data" placeholder="Search..">
                     <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:55%;z-index:10000;">
                        <div id="autoSuggestionsList"></div>
                     </div>
                     <a href="javascript:void(0)" id="chkval"  class="btn searchbut"> Search  </a>
                     <a href="<?php echo base_url() ?>index.php/welcome/open_advanceSearch" class="btn advance"> Advanced Topic Search  </a>
                  </div>
               </form>
               
               <!--//end-search-box-->
               <div class="clearfix"> </div>
            </div>
            <div class="header-right">
               <div class="profile_details_left">
                  <!--notifications of menu start -->
                  <ul class="nofitications-dropdown">
                     <?php
                        if ($this->session->userdata('id') != "") {
                            $ppage = "uploadpage";
                        } else {
                            $ppage = "loginpage/upload";
                        }
                        ?>
                     <?php if ($this->session->userdata('id') == "") { ?>
                     <li class="dropdown head-dpdn" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > 
                        <?php } ?>

                        <?php if ($this->session->userdata('id') != "") { ?>
                     <li class="dropdown head-dpdn"><a href="<?php echo base_url() ?>index.php/home/loginpage/upload"><img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" ></a>
                        <?php } ?>
                     </li>
                     <?php //echo base_url()index.php/home/loginpage    ?>
                     <?php if ($this->session->userdata('id') == "") { ?>
                     <li class="dropdown head-dpdn" id="check" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>assets/images/signinbut.png" class="img-responsive but"></li>
                     <!-- <li class="dropdown head-dpdn"> <a href="#"> <img src="<?php echo base_url(); ?>assets/images/signinbut.png" id="pop" data-toggle="modal" data-target="#myModal" class="img-responsive but"></a> </li>-->
                     <?php } ?>
                     <?php if ($this->session->userdata('id') == "") { ?>
                     <!--                                <li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/signup"> <img src="<?php echo base_url(); ?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>-->
                     <?php } ?>
                     <?php if ($this->session->userdata('id') != "") { ?>   
                     <?php
                        foreach ($userDetail as $user) {
                            $username = $user->username;
                            $userimg = $user->userLogo;
                            if ($user->userLogo == '') {
                                $userimg = "user.png";
                            }
                        }
                        ?>
                     <li class="dropdown head-dpdn">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" >
                           <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div>
                        </a>
                        <ul class="dropdown-menu">
                           <li>
                              <div class="notification_header">
                                 <h3><?php echo $username; ?></h3>
                              </div>
                           </li>
                           <li>
                              <a href="#">
                                 <div class="notification_desc">
                                    <p><?php echo $this->session->userdata('email'); ?></p>
                                 </div>
                                 <div class="clearfix"></div>
                              </a>
                           </li>
                           <li>
                              <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                           </li>
                           <li>
                              <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                           </li>
                        </ul>
                     </li>
                     <?php } ?>
                  </ul>

                  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                     <div class="modal-dialog" role="document">
                        <div class="modal-content" style="margin:20% auto;">
                           <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=" position: absolute;right: 10px;top: 10px;z-index: 100;"></button>
                           <div class="modal-body">
                              <div class="row">
                                 <div class="col-md-6 col-md-offset-3">
                                    <?php
                                       foreach ($companydetail as $company) {
                                           if ($company->company_logo != "") {
                                               $cmplogo = base_url() . 'Admin/uploads/'.$company->company_logo;
                                           } else {
                                               $cmplogo = base_url() . "uploads/logo.png";
                                           }
                                           ?><?php } ?>
                                    <a href="<?php echo base_url() ?>index.php/welcome"><img src="<?php echo $cmplogo; ?>" class="img-responsive logolog"></a>
                                    <h4 style="text-align:center; font-size:13px;">Sign in to continue to <span style="color:#03C;">Howclip</span></h4>
                                    <div style="color:#FF0000;text-align:center;" id="signformerror"></div>
                                    <div class="logwrap">
                                       <div class="profiledp"> <i class="fa fa-user" aria-hidden="true"></i></div>
                                       <form class="setup" method="post"  name="signform" id="signform">
                                          <!-- action="<?php echo base_url() ?>index.php/home/login"-->
                                          <div class="form-group">
                                             <input type="email" class="form-control login" name="email" id="email" placeholder="Email">
                                          </div>
                                          <div class="form-group">
                                             <input type="password" class="form-control login" name="password" id="pass" placeholder="Password">
                                          </div>
                                          <button type="button" id="log" class="btn loginbutton" onClick="return getlogin();">Submit</button>
                                       </form>
                                    </div>
                                    <h3><a href="<?php echo base_url() ?>index.php/welcome/signup" style="font-size:14px;margin:10px 0px; text-align:center; text-decoration:none;display:block;">Create Account</a></h3>
                                    <div class="logresponsive">
                                       <ul>
                                          <li>
                                             <a href="#">
                                             <img src="<?php echo base_url(); ?>assets/images/fblogin.jpg" id="loginBtn1" class="img-responsive logimageres "> 
                                             </a>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>

                  
                  <div class="clearfix"> </div>
               </div>
            </div>
            <div class="clearfix"> </div>
            <div class="header-center">
               <div id="cssmenu">
                  <ul>
                     <?php
                        foreach ($categories as $menu) {
                            ?>
                     <li>
                        <a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>'><i class="fa fa-cogs nav_icon"></i><?php echo $menu['title']; ?> </a>
                        <ul>
                           <?php
                              if ($menu['submenu'] != '') {
                                  foreach ($menu['submenu'] as $sub) {
                                      ?>
                           <li>
                              <a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>/<?php echo $sub['id']; ?>' ><?php echo $sub['title']; ?></a>
                              <ul>
                                 <?php
                                    if ($sub['submenu'] != '') {
                                        foreach ($sub['submenu'] as $sub2) {
                                            ?>
                                 <li>
                                    <a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>/<?php echo $sub['id']; ?>/<?php echo $sub2['id']; ?>' ><?php echo $sub2['title']; ?></a>
                        <!--
                           <ul>
                           <?php
                              if ($sub2['submenu'] != '') {
                                  foreach ($sub2['submenu'] as $sub3) {
                                      ?>
                                                               <li><a href='<?php echo base_url() ?>index.php/Welcome/getadvanceresult/<?php echo $menu['id']; ?>/<?php echo $sub['id']; ?>/<?php echo $sub2['id']; ?>/<?php echo $sub3['id']; ?>' ><?php echo $sub3['title']; ?></a>
                           
                                                                   <ul>
                           <?php
                              if ($sub3['submenu'] != '') {
                                  foreach ($sub3['submenu'] as $sub4) {
                                      ?>
                                                                                                    
                           <?php
                              }
                              }
                              ?>
                                                                   </ul>
                           
                           
                           <?php
                              }
                              }
                              ?>
                           </ul>-->
                                    <?php
                                       }
                                       }
                                       ?>
                              </ul>
                           </li>
                           <?php
                              }
                              }
                              ?>
                        </ul>
                     </li>
                     <?php } ?>
                  </ul>
               </div>
            </div>
         </div>
         <div id="page-wrapper">
            <div class="main-page">
               <div class="blank-page widget-shadow scroll" id="style-2 div1">
                  <div class="row">
                     <div class="col-md-12">
                        <div id="response" style="display:none;"></div>
                        <h3 class="title1"><?php echo "Playlist"; ?></h3>
                     </div>
                  </div>
                  <div class="row">
                     <?php foreach ($playlist as $play) { //echo '<pre>'; print_r($play); ?>
                     <?php
                        if (getimagesize(base_url() . 'uploads/images/' . $play['video'][0]['video_img'])) {
                            
                             $link = base_url() . "uploads/images/" . $play['video'][0]['video_img'];
                        } else {
                            $link = base_url() . "uploads/images/download.jpg";
                        }
                        ?>
                     <div class="col-md-3 col-xs-6">
                        <div class="thumbnail">
                           <a href="<?php echo base_url() ?>index.php/welcome/playvideo/<?php echo $play['play'] . '/' . $play['user']['id']; ?>">
                           <img src="<?php echo $link; ?>" class="img-responsive" width="200px" height="100" alt="No Image Available">
                           <?php if ($play['price'] > 0) { ?>
                           <a href="<?php echo base_url() ?>index.php/welcome/playvideo/<?php echo $play['play'] . '/' . $play['user']['id']; ?>">
                              <div class="dollar" ><img src="<?php echo base_url() ?>assets/images/usd1600.png" class="img-responsive"></div>
                           </a>
                           <?php } ?>
                           </a>
                           <div class="victo">
                              <?php
                                 $duration = explode(':', $play['duration']);
                                 if ($duration[0] == '0') {
                                     echo $videolong = $duration[1] . ":" . $duration[2];
                                 } else {
                                     echo $videolong = $duration[0] . ":" . $duration[1] . ":" . $duration[2];
                                 }
                                 ?>
                           </div>
                           <div class="caption catch">
                              <?php
                                 $vname = $play['play'];
                                 $length = strlen($play['play']);
                                 if ($length < 10) {
                                     $v = $vname;
                                 } else {
                                     $v = substr($vname, 0, 10);
                                     $v = $v . "...";
                                 }
                                 ?>
                              <h3><a href="<?php echo base_url() ?>index.php/welcome/playvideo/<?php echo $play['play']; ?>" title="<?php echo ucwords($play['play']); ?>"><?php echo ucwords($v); ?></a></h3>
                              <p><a href="#"><?php echo ucwords("playlist"); ?></a></p>
                              <p><a href="#" class="btn btm" role="button"><?php
                                 if ($play['views'] == "") {
                                     echo "0";
                                 } else {
                                     echo ucwords($play['views']);
                                 }
                                 ?> Views</a> 
                                 <a href="#" class="btn ltm" role="button"><?php
                                    $date = explode('-', $play['date']);
                                    if ($date[1] == '01') {
                                        $month = 'Jan';
                                    } if ($date[1] == '02') {
                                        $month = 'Feb';
                                    } if ($date[1] == '03') {
                                        $month = 'Mar';
                                    } if ($date[1] == '04') {
                                        $month = 'Apr';
                                    }if ($date[1] == '05') {
                                        $month = 'May';
                                    } if ($date[1] == '06') {
                                        $month = 'Jun';
                                    } if ($date[1] == '07') {
                                        $month = 'Jul';
                                    } if ($date[1] == '08') {
                                        $month = 'Aug';
                                    } if ($date[1] == '09') {
                                        $month = 'Sep';
                                    } if ($date[1] == '10') {
                                        $month = 'Oct';
                                    } if ($date[1] == '11') {
                                        $month = 'Nov';
                                    } if ($date[1] == '12') {
                                        $month = 'Dec';
                                    }
                                    
                                    echo $date[2] . " " . $month . " " . $date[0];
                                    ;
                                    ?></a>
                              </p>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>
               <?php foreach ($feature as $key => $feat) { //echo '<pre>'; print_r($feat);   ?>
               <div class="blank-page widget-shadow scroll" id="style-2 div1">
                  <div class="row">
                     <div class="col-md-12">
                        <h3 class="title1"><?php echo $key; ?></h3>
                     </div>
                  </div>
                  <div class="row">
                     <?php foreach ($feat as $video) { ?>
                     <div class="col-md-3 col-xs-6">
                        <?php
                           //if (getimagesize('uploads/images/' . $video->video_img) !== false) {
                            if (getimagesize('uploads/images/' . $video->video_img)) {
                                $link = base_url() . "uploads/images/" . $video->video_img;
                            } else {
                                $link = base_url() . "uploads/images/download.jpg";
                            }
                            ?>
                        <div class="thumbnail">
                           <a href="<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $video->id; ?>">
                           <img src="<?php echo $link; ?>" class="img-responsive" width="200px" height="100" alt="No Image Available">
                           <?php if ($video->price > 0) { ?>
                           <a href="<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $video->id; ?>">
                              <div class="dollar" ><img src="<?php echo base_url() ?>assets/images/usd1600.png" class="img-responsive"></div>
                           </a>
                           <?php } ?>
                           </a>
                           <div class="victo"><?php
                              $duration = explode(':', $video->video_duration);
                              if ($duration[0] == '0') {
                                  echo $videolong = $duration[1] . ":" . $duration[2];
                              } else {
                                  echo $videolong = $duration[0] . ":" . $duration[1] . ":" . $duration[2];
                              }
                              ?></div>
                           <div class="caption catch">
                              <?php
                                 $vname = $video->videoname;
                                 $length = strlen($video->videoname);
                                 if ($length < 10) {
                                     $v = $vname;
                                 } else {
                                     $v = substr($vname, 0, 10);
                                     $v = $v . "...";
                                 }
                                 ?>
                              <h3><a href="<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $video->id; ?>" title="<?php echo ucwords($video->videoname); ?>"><?php echo ucwords($v); ?></a></h3>
                              <p><a href="#"><?php echo ucwords($video->video_category); ?></a></p>
                              <p><a href="#" class="btn btm" role="button"><?php
                                 if ($video->views == "") {
                                     echo "0";
                                 } else {
                                     echo ucwords($video->views);
                                 }
                                 ?> Views</a> 
                                 <a href="#" class="btn ltm" role="button"><?php
                                    $date = explode('-', $video->Date);
                                    if ($date[1] == '01') {
                                        $month = 'Jan';
                                    } if ($date[1] == '02') {
                                        $month = 'Feb';
                                    } if ($date[1] == '03') {
                                        $month = 'Mar';
                                    } if ($date[1] == '04') {
                                        $month = 'Apr';
                                    }if ($date[1] == '05') {
                                        $month = 'May';
                                    } if ($date[1] == '06') {
                                        $month = 'Jun';
                                    } if ($date[1] == '07') {
                                        $month = 'Jul';
                                    } if ($date[1] == '08') {
                                        $month = 'Aug';
                                    } if ($date[1] == '09') {
                                        $month = 'Sep';
                                    } if ($date[1] == '10') {
                                        $month = 'Oct';
                                    } if ($date[1] == '11') {
                                        $month = 'Nov';
                                    } if ($date[1] == '12') {
                                        $month = 'Dec';
                                    }
                                    
                                    echo $date[2] . " " . $month . " " . $date[0];
                                    ;
                                    ?></a>
                              </p>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                  </div>
               </div>
               <?php } ?>
            </div>
         </div>
         <div class="footer">
            <div class="row">
               <div class="col-md-12">
                  <div class="footmen">
                     <ul>
                        <li><a href="<?php echo base_url() ?>index.php/welcome/about"> About </a></li>
                        <li><a href="javascript:void(0);"> Press </a></li>
                        <li><a href="javascript:void(0);"> Copyright </a></li>
                        <li><a href="javascript:void(0);"> Creators</a></li>
                        <li><a href="javascript:void(0);"> Advertise</a></li>
                        <li><a href="javascript:void(0);"> Developers</a></li>
                        <li><a href="javascript:void(0);">HowClip</a></li>
                     </ul>
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-12">
                  <div class="minfootmen">
                     <ul>
                        <li><a href="<?php echo base_url() ?>index.php/welcome/terms"> Terms</a></li>
                        <li><a href="javascript:void(0);"> Privacy</a></li>
                        <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                        <li><a href="javascript:void(0);"> Send feedback</a></li>
                        <li><a href="javascript:void(0);"> Test new features</a></li>
                        <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
         <!--//footer--> 
      </div>
      <!-- Classie --> 
      <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
      <script>
         var menuLeft = document.getElementById('cbp-spmenu-s1'),
                 showLeftPush = document.getElementById('showLeftPush'),
                 body = document.body;
         
         showLeftPush.onclick = function () {
             classie.toggle(this, 'active');
             classie.toggle(body, 'cbp-spmenu-push-toright');
             classie.toggle(menuLeft, 'cbp-spmenu-open');
             disableOther('showLeftPush');
         };
         
         function disableOther(button) {
             if (button !== 'showLeftPush') {
                 classie.toggle(showLeftPush, 'disabled');
             }
         }
      </script> 
      <!--scrolling js--> 
      <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
      <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
      <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
      <!--//scrolling js--> 
      <!-- Bootstrap Core JavaScript --> 
      <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script> 
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
      <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 
      <script>
         $(function () {
             $('#menu').metisMenu({
                 toggle: false // disable the auto collapse. Default: true.
             });
         });
      </script>
      <script>
         $('.pop').popover().click(function () {
             setTimeout(function () {
                 $('.pop').popover('hide');
             }, 2000);
         });
         
      </script>
      <script type="text/javascript">
         function ajaxSearch()
         {
         
             var input_data = $('#input-31').val();
         
             if (input_data.length === 0)
             {
                 $('#suggestions').hide();
             } else
             {
         
                 var post_data = {
                     'search_data': input_data,
                     '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                 };
         
                 $.ajax({
                     type: "POST",
                     url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                     data: post_data,
                     success: function (data) {
                         //console.log(data);
                         if (data.length > 0) {
                             $('#suggestions').show();
                             $('#autoSuggestionsList').addClass('auto_list');
                             $('#autoSuggestionsList').html(data);
                         }
                     }
                 });
         
         
             }
         
         }
      </script>
      <script>
         function checkbox()
         {
             if (!this.form.checkbox.checked)
             {
                 alert('You must agree to the terms first.');
                 return false;
             }
         }
      </script>
      <script>
         function validate_pass(that)
         {
             var pass = ($('#password').val());
             var cpass = ($('#confirm_password').val());
             if (pass.length < 8)
             {
                 $("#lengtherr").show();
                 return false;
             }
             if (pass != cpass)
             {
         
                 $("#selectPeriodRangePanel").show();
         
                 return false;
             }
         }
      </script>
      <script>
         function validation() {
         
             var fname = document.form.fname.value;
             var lname = document.form.lname.value;
             var femail = document.form.email.value;
             var fpassword = document.form.password.value;
             var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{1,2}/.test(femail);
         
             if (fname == "")
             {
         
                 document.getElementById('fnameerror').style.display = 'block';
                 setTimeout(function () {
                     $('#fnameerror').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (lname == "")
             {
         
                 document.getElementById('lnameerror').style.display = 'block';
                 setTimeout(function () {
                     $('#lnameerror').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (femail == "")
             {
                 document.getElementById('mailerror').style.display = 'block';
                 setTimeout(function () {
                     $('#mailerror').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (fpassword == "")
             {
         
                 document.getElementById('passerror').style.display = 'block';
                 setTimeout(function () {
                     $('#passerror').fadeOut('fast');
                 }, 1000);
                 return false;
         
             }
             if (fpassword.length < 8)
             {
                 $("#lengtherr").show();
                 setTimeout(function () {
                     $('#lengtherr').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (!re)
             {
                 document.getElementById("formerror").innerHTML = "Enter valid Email";
                 setTimeout(function () {
                     $('#formerror').fadeOut('fast');
                 }, 1000);
         
                 return false;
             }
         
         }
      </script>
      <script>
         $(document).on("click", function (e) {
             if (!$("#suggestions").is(e.target)) {
                 $("#suggestions").hide();
             }
         });
          $("#check").on("click", function (e) {
            $('#emailAdd').val('');
            $('#pass').val('');
         });
      </script>
      <script>
         // $('#email_address').on('keypress', function() {
         //    var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
         //    if(!re) {
         //        $('#error').show();
         //    } else {
         //        $('#error').hide();
         //    }
         //})
      </script>
      <script>
         function getlogin()
         {
         
         
             var email = document.getElementById("email").value;
             var password = document.getElementById("pass").value;
         
             if (email == "")
             {
                 document.getElementById("signformerror").innerHTML = "Enter the Email Address";
                 return false;
             } else if (password == "")
             {
         
                 document.getElementById("signformerror").innerHTML = "Enter the Password";
                 return false;
             } else
             {
         
                 var sign_data = {
                     'email': email,
                     'password': password,
                 };
         
                 $.ajax({
                     type: "POST",
                     url: "<?php echo base_url() ?>index.php/welcome/login",
                     data: sign_data,
                     success: function (data) {
                         console.log(data);
                         if (data.length > 0) {
                             if (data == 'success')
                             {
                                 window.location = "<?php echo base_url() ?>index.php/home/welcome";
                             } else
                             {
                                 document.getElementById("signformerror").innerHTML = data;
                             }
         
                         }
                     }
                 });
         
         
         
             }
         
         
         }
         $('#pass').keypress(function (e) {
             if (e.which == '13') {
                 getlogin();
             }
         });
      </script>
      <script type="text/javascript">
         function aaa(x,y){
             var email = x;
              var name = y;
             // alert();
              $.ajax({
              url: '<?php echo base_url(); ?>index.php/welcome/insertfbdata',
                 type: "POST",
                 data: {'email': email,'user':name
                 },
                 success: function (response)
                 {
                    // alert(response);return false;
                    //alert('<?php echo base_url(); ?>index.php/home/dash');
         window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                 }
         
             });
         }
           function SubmitUserData(email) {
             alert(email);
              return false;
             $.ajax({
                 url: '<?php echo base_url(); ?>index.php/home/insertfbdata',
                 type: "POST",
                 data: {'email': email
                 },
                 success: function (response)
                 {
                     window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                 }
         
             });
         }
         
         function getUserData() {
             FB.api('/me?fields=id,first_name,last_name,email,link,gender,locale,picture', function (response) {
                 document.getElementById('response').innerHTML = 'Hello ' + response.id + '<br>' + response.email + '<br>' + response.first_name + ' ' + response.last_name + '<br>' + response.gender;
                var uname = response.first_name;
         var email = response.email;
         
         
                 aaa(email,uname);
             });
         }
         
         
         window.fbAsyncInit = function () {
             FB.init({
                 appId: '1884735515112965',
                 xfbml: true,
                 version: 'v2.6'
             });
         };
         
         (function (d, s, id) {
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {
                 return;
             }
             js = d.createElement(s);
             js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
         
         document.getElementById('loginBtn').addEventListener('click', function () {
         
             FB.login(function (response) {
                 if (response.authResponse) {
                     //user just authorized your app
                     document.getElementById('loginBtn').style.display = 'none';
                     getUserData();
         
                 }
             }, {scope: 'email,public_profile', return_scopes: true});
         }, false);
         document.getElementById('loginBtn1').addEventListener('click', function () {
         
             FB.login(function (response) {
                 if (response.authResponse) {
                     //user just authorized your app
                    // document.getElementById('loginBtn1').style.display = 'none';
                     getUserData();
         
                 }
             }, {scope: 'email,public_profile', return_scopes: true});
         }, false);
         
      </script>
      <script>
         function SubmitUserData(email) {
             $.ajax({
                 url: '<?php echo base_url(); ?>index.php/home/insertfbdata',
                 type: "POST",
                 data: {'email': email
                 },
                 success: function (response)
                 {
                     //alert(response);
                     //console.log(response);
                     window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                 }
         
             });
         }
         function onSignIn(googleUser) {
         
             var profile = googleUser.getBasicProfile();
             console.log("ID: " + profile.getId());
             document.getElementById('gid').innerHTML = profile.getId();
             console.log('Full Name: ' + profile.getName());
             document.getElementById('gname').innerHTML = profile.getName();
             document.getElementById('gmail').innerHTML = profile.getEmail();
             console.log('Given Name: ' + profile.getGivenName());
             console.log('Family Name: ' + profile.getFamilyName());
             console.log("Image URL: " + profile.getImageUrl());
             console.log("Email: " + profile.getEmail());
         
             var id_token = googleUser.getAuthResponse().id_token;
             console.log("ID Token: " + id_token);
             var email = profile.getEmail();
             //alert(email);
             SubmitUserData(email);
         
         }
         
         $('#chkval').click(function (e) {
             e.preventDefault();
             var input = document.getElementById("input-31").value;
             if (input == '') {
                 alert('Please Type Text For Search!');
                  e.preventDefault();
             }
             else{
                 document.getElementById('my_form').submit();
             }
         });
      </script>
   </body>
</html>