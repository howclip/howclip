<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>HowClip</title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet">
	
	
  </head>
  <body>

  <div class="headtop">
  <div class="container-fluid">
  <div class="row">
  <div class="col-md-12 col-xs-12">
      
       <?php foreach($companydetail as $company) { if($company->company_logo != ""){ $cmplogo = base_url()."/Admin/uploads/$company->company_logo"; } else {$cmplogo = base_url()."/uploads/logo.png";  } ?>
      <div class="logoside"  > <a href="<?php echo base_url() ?>index.php/welcome">
      
        <img src="<?php echo $cmplogo;?>" class="img-responsive">
       </a> </div>
    <?php } ?>
<!--  <div class="logoside"><a href="http://livesoftwaresolution.info/PUPILCLIP/"><img src="<?php echo base_url();?>assets/images/logo1.png" class="img-responsive"></a></div>
  <a href="<?php echo base_url()?>index.php/home/loginpage"> <img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive signin" width="105"></a>
  </div>-->
  </div>
  </div>
  </div>
  <div class="contentform">
  <div class="container">
  <div class="row">
  <div class="col-md-12">
      <h4>Create Your <a href="<?php echo base_url() ?>index.php/welcome"> <img src="<?php echo $cmplogo;?>" class="img-responsive logohead"> </a> Account</h4>
  </div>
  </div>
  <div class="row">
       <?php if($this->session->flashdata('regerr'))
	 		{
			?>
					
	<div style="color:#FF0000;text-align:center;" > <?php echo $this->session->flashdata('regerr'); ?></div>
										
			<?php } ?>
  <div style="color:#FF0000;text-align:center;" id="formerror"></div>
  <div class="col-md-4 col-md-offset-4">
  <div class="formclas">

  
  
  
  
  <form class="setform" method="post" name="myform" action="<?php echo base_url()?>index.php/welcome/registeruser">
  <div class="row">
  <div class="form-group col-md-6 col-xs-6" style="padding-right:5px;">
    <input type="text" class="form-control login" name="fname" id="fname" placeholder="First Name">
  </div>
  <div class="form-group col-md-6 col-xs-6" style="padding-left:5px;">

    <input type="text" class="form-control login" name="lname" id="lname" placeholder="Last Name">
  </div>
  </div>
  
  <!--<div class="row">
  <div class="form-group col-md-12">
    <input type="text" class="form-control login" id="exampleInputEmail1" name="username" placeholder="Choose your Username">
  </div>
  </div>-->
  
  
  <div class="row">
  <div class="form-group col-md-12">
    <input type="email" class="form-control login" name="email" id="email_address" onchange="checkmail(this.value);" placeholder="Choose your Email">
	<p id="error" style="display:none;color:red;"></p>
  </div>
  </div>
  <div class="row">
  <div class="form-group col-md-12">
    <input type="password" class="form-control login" id="password" name="password" placeholder="Create a Password">
  </div>
  </div>
  <div class="row">
<div class="form-group col-md-12">
    <input type="password" class="form-control login" id="confirm_password" name="confirm_password" placeholder="Confirm your Password">
  </div>
  </div>

   <div id="selectPeriodRangePanel" style="display:none;color:#FF0000;margin-left:15px;">
    <p style="color:#FF0000">Password and Confirm Password Does Not match!</p>
</div>

<div id="lengtherr" style="display:none;color:#FF0000;margin-left:15px;">
    <p style="color:#FF0000">Your password must be at least 8 characters!</p>
</div>
<div id="selectPeriodRangePanel" style="color:#FFFFFF;margin-left:15px;">
    <p style="color:#FFFFFF;">Optional: </p>
</div>
  <div class="row">
<div class="form-group col-md-4 col-xs-4" style="padding-right:5px;">
    <select class="form-control selcto" name="month">
  		 <option value="">Month</option>
  		 <option value="01">Jan</option>
  		 <option value="02">Feb</option>
  		 <option value="03">March</option>
         <option value="04">April</option>
         <option value="05">May</option>
         <option value="06">June</option> 
         <option value="07">July</option>
         <option value="08">Aug</option>  
         <option value="09">Sep</option>
         <option value="10">Oct</option>  
         <option value="11">Nov</option>
         <option value="12">Dec</option>
           
</select>
  </div>
  <div class="form-group col-md-4 col-xs-4" style="padding:0px 5px;">
    <input type="text" name="day" class="form-control login" id="exampleInputEmail1" maxlength="2" placeholder="Day">
  </div>
  <div class="form-group col-md-4 col-xs-4" style="padding-left:5px;">
    <input type="text" name="year" class="form-control login" id="exampleInputEmail1" maxlength="4" placeholder="Year">
  </div>
  </div>
  <div class="row">
<div class="form-group  col-md-12" >
    <select class="form-control selcto" name="gender">
  		 <option value="">Gender</option>
  		 <option value="female">Female</option>
  		 <option value="male">Male</option>
  		 <option value="other">Other</option>
         <option value="undefine">Rather not say</option>   
</select>
  </div>
  </div>
  <div class="row">
  <!-- <div class="form-group col-md-3 col-xs-3" style="padding-right:0px;">
   
  <select class="form-control selcto" name="code">
  		<option value="+93">+93</option>
											<option value="+355">+355</option>
											<option value="+213">+213</option>
											<option value="+1-684">+1-684</option>
											<option value="+376">+376</option>
											<option value="+244">+244</option>
											<option value="+1-264">+1-264</option>
											<option value="+672">+672</option>
											<option value="+1-268">+1-268</option>
											<option value="+54">+54</option>
											<option value="+374">+374</option>
											<option value="+297">+297</option>
											<option value="+61">+61</option>
											<option value="+43">+43</option>
											<option value="+994">+994</option>
											<option value="+1-242">+1-242</option>
											<option value="+973">+973</option>
											<option value="+880">+880</option>
											<option value="+1-246">+1-246</option>
											<option value="+375">+375</option>
											<option value="+32">+32</option>
											<option value="+501">+501</option>
											<option value="+229">+229</option>
											<option value="+1-441">+1-441</option>
											<option value="+975">+975</option>
											<option value="+591">+591</option>
											<option value="+387">+387</option>
											<option value="+267">+267</option>
											<option value="+55">+55</option>
											<option value="+246">+246</option>
											<option value="+1-284">+1-284</option>
											<option value="+673">+673</option>
											<option value="+359">+359</option>
											<option value="+226">+226</option>
											<option value="+257">+257</option>
											<option value="+855">+855</option>
											<option value="+237">+237</option>
											<option value="+1">+1</option>
											<option value="+238">+238</option>
											<option value="+1-345">+1-345</option>
											<option value="+236">+236</option>
											<option value="+235">+235</option>
											<option value="+86">+86</option>
											<option value="+61">+61</option>
											<option value="+57">+57</option>
											<option value="+269">+269</option>
											<option value="+682">+682</option>
											<option value="+506">+506</option>
											<option value="+385">+385</option>
											<option value="+53">+53</option>
											<option value="+599">+599</option>
											<option value="+357">+357</option>
											<option value="+420">+420</option>
											<option value="+243">+243</option>
											<option value="+45">+45</option>
											<option value="+253">+253</option>
											<option value="+1-767">+1-767</option>
											<option value="+1-809">+1-809</option>
											<option value="+1-829">+1-829</option>
											<option value="+1-849">+1-849</option>
											<option value="+670">+670</option>
											<option value="+593">+593</option>
											<option value="+20">+20</option>
											<option value="+503">+503</option>
											<option value="+240">+240</option>
											<option value="+291">+291</option>
											<option value="+372">+372</option>
											<option value="+251">+251</option>
											<option value="+500">+500</option>
											<option value="+298">+298</option>
											<option value="+679">+679</option>
											<option value="+358">+358</option>
											<option value="+33">+33</option>
											<option value="+689">+689</option>
											<option value="+241">+241</option>
											<option value="+220">+220</option>
											<option value="+995">+995</option>
											<option value="+49">+49</option>
											<option value="+233">+233</option>
											<option value="+350">+350</option>
											<option value="+30">+30</option>
											<option value="+299">+299</option>
											<option value="+1-473">+1-473</option>
											<option value="+1-671">+1-671</option>
											<option value="+502">+502</option>
											<option value="+44-1481">+44-1481</option>
											<option value="+224">+224</option>
											<option value="+245">+245</option>
											<option value="+592">+592</option>
											<option value="+509">+509</option>
											<option value="+504">+504</option>
											<option value="+852">+852</option>
											<option value="+36">+36</option>
											<option value="+354">+354</option>
											<option value="+91">+91</option>
											<option value="+62">+62</option>
											<option value="+98">+98</option>
											<option value="+964">+964</option>
											<option value="+353">+353</option>
											<option value="+44-1624">+44-1624</option>
											<option value="+972">+972</option>
											<option value="+39">+39</option>
											<option value="+225">+225</option>
											<option value="+1-876">+1-876</option>
											<option value="+81">+81</option>
											<option value="+44-1534">+44-1534</option>
											<option value="+962">+962</option>
											<option value="+7">+7</option>
											<option value="+254">+254</option>
											<option value="+686">+686</option>
											<option value="+383">+383</option>
											<option value="+965">+965</option>
											<option value="+996">+996</option>
											<option value="+856">+856</option>
											<option value="+371">+371</option>
											<option value="+961">+961</option>
											<option value="+266">+266</option>
											<option value="+231">+231</option>
											<option value="+218">+218</option>
											<option value="+423">+423</option>
											<option value="+370">+370</option>
											<option value="+352">+352</option>
											<option value="+853">+853</option>
											<option value="+389">+389</option>
											<option value="+261">+261</option>
											<option value="+265">+265</option>
											<option value="+60">+60</option>
											<option value="+960">+960</option>
											<option value="+223">+223</option>
											<option value="+356">+356</option>
											<option value="+692">+692</option>
											<option value="+222">+222</option>
											<option value="+230">+230</option>
											<option value="+262">+262</option>
											<option value="+52">+52</option>
											<option value="+691">+691</option>
											<option value="+373">+373</option>
											<option value="+377">+377</option>
											<option value="+976">+976</option>
											<option value="+382">+382</option>
											<option value="+1-664">+1-664</option>
											<option value="+212">+212</option>
											<option value="+258">+258</option>
											<option value="+95">+95</option>
											<option value="+674">+674</option>
											<option value="+977">+977</option>
											<option value="+31">+31</option>
											<option value="+599">+599</option>
											<option value="+687">+687</option>
											<option value="+64">+64</option>
											<option value="+505">+505</option>
											<option value="+227">+227</option>
											<option value="+234">+234</option>
											<option value="+683">+683</option>
											<option value="+850">+850</option>
											<option value="+1-670">+1-670</option>
											<option value="+47">+47</option>
											<option value="+968">+968</option>
											<option value="+92">+92</option>
											<option value="+680">+680</option>
											<option value="+970">+970</option>
											<option value="+507">+507</option>
											<option value="+675">+675</option>
											<option value="+595">+595</option>
											<option value="+51">+51</option>
											<option value="+63">+63</option>
											<option value="+64">+64</option>
											<option value="+48">+48</option>
											<option value="+351">+351</option>
											<option value="+1-787">+1-787</option>
											<option value="+974">+974</option>
											<option value="+242">+242</option>
											<option value="+262">+262</option>
											<option value="+40">+40</option>
											<option value="+250">+250</option>
											<option value="+590">+590</option>
											<option value="+290">+290</option>
											<option value="+1-869">+1-869</option>
											<option value="+1-758">+1-758</option>
											<option value="+508">+508</option>
											<option value="+1-784">+1-784</option>
											<option value="+685">+685</option>
											<option value="+378">+378</option>
											<option value="+239">+239</option>
											<option value="+966">+966</option>
											<option value="+221">+221</option>
											<option value="+381">+381</option>
											<option value="+248">+248</option>
											<option value="+232">+232</option>
											<option value="+65">+65</option>
											<option value="+1-721">+1-721</option>
											<option value="+421">+421</option>
											<option value="+386">+386</option>
											<option value="+677">+677</option>
											<option value="+252">+252</option>
											<option value="+27">+27</option>
											<option value="+82">+82</option>
											<option value="+211">+211</option>
											<option value="+34">+34</option>
											<option value="+94">+94</option>
											<option value="+249">+249</option>
											<option value="+597">+597</option>
											<option value="+47">+47</option>
											<option value="+268">+268</option>
											<option value="+46">+46</option>
											<option value="+41">+41</option>
											<option value="+963">+963</option>
											<option value="+886">+886</option>
											<option value="+992">+992</option>
											<option value="+255">+255</option>
											<option value="+66">+66</option>
											<option value="+228">+228</option>
											<option value="+690">+690</option>
											<option value="+676">+676</option>
											<option value="+1-868">+1-868</option>
											<option value="+216">+216</option>
											<option value="+90">+90</option>
											<option value="+993">+993</option>
											<option value="+1-649">+1-649</option>
											<option value="+688">+688</option>
											<option value="+1-340">+1-340</option>
											<option value="+256">+256</option>
											<option value="+380">+380</option>
											<option value="+971">+971</option>
											<option value="+44">+44</option>
											<option value="+1">+1</option>
											<option value="+598">+598</option>
											<option value="+998">+998</option>
											<option value="+678">+678</option>
											<option value="+379">+379</option>
											<option value="+58">+58</option>
											<option value="+84">+84</option>
											<option value="+681">+681</option>
											<option value="+212">+212</option>
											<option value="+967">+967</option>
											<option value="+260">+260</option>
											<option value="+263">+263</option>
         
</select>
  </div>-->
  
   <div class="form-group col-md-12" >
    <input type="text" class="form-control login" id="exampleInputEmail1" name="mobile" placeholder="Enter your mobile no">
  </div>
  </div>
 
  <div class="row">
   <div class="form-group col-md-12">
    <input type="text" class="form-control login" id="curremail" name="curremail" placeholder="Enter your current Email Address">
	<p id="currerror" style="display:none;color:red;">Wrong email</p>
  </div>
  </div>
  <div class="row">
   <div class="form-group col-md-12">
   <select class="form-control selcto" name="location">
   
		<option value="United States">United States</option>
		<option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
		<option value="United Kingdom">United Kingdom</option>
 		<option value="">Location</option>
  		<option value="Aaland Islands">Aaland Islands</option>
		<option value="Afghanistan">Afghanistan</option>
		<option value="Albania">Albania</option>
		<option value="Algeria">Algeria</option>
		<option value="American Samoa">American Samoa</option>
		<option value="Andorra">Andorra</option>
		<option value="Angola">Angola</option>
		<option value="Anguilla">Anguilla</option>
		<option value="Antarctica">Antarctica</option>
		<option value="Antigua and Barbuda">Antigua and Barbuda</option>
		<option value="Argentina">Argentina</option>
		<option value="Armenia">Armenia</option>
		<option value="Aruba">Aruba</option>
		<option value="Australia">Australia</option>
		<option value="Austria">Austria</option>
		<option value="Azerbaijan">Azerbaijan</option>
		<option value="Bahamas">Bahamas</option>
		<option value="Bahrain">Bahrain</option>
		<option value="Bangladesh">Bangladesh</option>
		<option value="Barbados">Barbados</option>
		<option value="Belarus">Belarus</option>
		<option value="Belgium">Belgium</option>
		<option value="Belize">Belize</option>
		<option value="Benin">Benin</option>
		<option value="Bermuda">Bermuda</option>
		<option value="Bhutan">Bhutan</option>
		<option value="Bolivia">Bolivia</option>
		<option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
		<option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
		<option value="Botswana">Botswana</option>
		<option value="Bouvet Island">Bouvet Island</option>
		<option value="Brazil">Brazil</option>
		<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
		<option value="Brunei Darussalam">Brunei Darussalam</option>
		<option value="Bulgaria">Bulgaria</option>
		<option value="Burkina Faso">Burkina Faso</option>
		<option value="Burundi">Burundi</option>
		<option value="Cambodia">Cambodia</option>
		<option value="Cameroon">Cameroon</option>
		<option value="Canada">Canada</option>
		<option value="Canary Islands">Canary Islands</option>
		<option value="Cape Verde">Cape Verde</option>
		<option value="Cayman Islands">Cayman Islands</option>
		<option value="Central African Republic">Central African Republic</option>
		<option value="Chad">Chad</option>
		<option value="Chile">Chile</option>
		<option value="China">China</option>
		<option value="Christmas Island">Christmas Island</option>
		<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
		<option value="Colombia">Colombia</option>
		<option value="Comoros">Comoros</option>
		<option value="Congo">Congo</option>
		<option value="Cook Islands">Cook Islands</option>
		<option value="Costa Rica">Costa Rica</option>
		<option value="Cote D'Ivoire">Cote D'Ivoire</option>
		<option value="Croatia">Croatia</option>
		<option value="Cuba">Cuba</option>
		<option value="Curacao">Curacao</option>
		<option value="Cyprus">Cyprus</option>
		<option value="Czech Republic">Czech Republic</option>
		<option value="Democratic Republic of Congo">Democratic Republic of Congo</option>
		<option value="Denmark">Denmark</option>
		<option value="Djibouti">Djibouti</option>
		<option value="Dominica">Dominica</option>
		<option value="Dominican Republic">Dominican Republic</option>
		<option value="East Timor">East Timor</option>
		<option value="Ecuador">Ecuador</option>
		<option value="Egypt">Egypt</option>
		<option value="El Salvador">El Salvador</option>
		<option value="Equatorial Guinea">Equatorial Guinea</option>
		<option value="Eritrea">Eritrea</option>
		<option value="Estonia">Estonia</option>
		<option value="Ethiopia">Ethiopia</option>
		<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
		<option value="Faroe Islands">Faroe Islands</option>
		<option value="Fiji">Fiji</option>
		<option value="Finland">Finland</option>
		<option value="France, skypolitan">France, skypolitan</option>
		<option value="French Guiana">French Guiana</option>
		<option value="French Polynesia">French Polynesia</option>
		<option value="French Southern Territories">French Southern Territories</option>
		<option value="FYROM">FYROM</option>
		<option value="Gabon">Gabon</option>
		<option value="Gambia">Gambia</option>
		<option value="Georgia">Georgia</option>
		<option value="Germany">Germany</option>
		<option value="Ghana">Ghana</option>
		<option value="Gibraltar">Gibraltar</option>
		<option value="Greece">Greece</option>
		<option value="Greenland">Greenland</option>
		<option value="Grenada">Grenada</option>
		<option value="Guadeloupe">Guadeloupe</option>
		<option value="Guam">Guam</option>
		<option value="Guatemala">Guatemala</option>
		<option value="Guernsey">Guernsey</option>
		<option value="Guinea">Guinea</option>
		<option value="Guinea-Bissau">Guinea-Bissau</option>
		<option value="Guyana">Guyana</option>
		<option value="Haiti">Haiti</option>
		<option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
		<option value="Honduras">Honduras</option>
		<option value="Hong Kong">Hong Kong</option>
		<option value="Hungary">Hungary</option>
		<option value="Iceland">Iceland</option>
		<option value="India">India</option>
		<option value="Indonesia">Indonesia</option>
		<option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
		<option value="Iraq">Iraq</option>
		<option value="Ireland">Ireland</option>
		<option value="Israel">Israel</option>
		<option value="Italy">Italy</option>
		<option value="Jamaica">Jamaica</option>
		<option value="Japan">Japan</option>
		<option value="Jersey">Jersey</option>
		<option value="Jordan">Jordan</option>
		<option value="Kazakhstan">Kazakhstan</option>
		<option value="Kenya">Kenya</option>
		<option value="Kiribati">Kiribati</option>
		<option value="Korea, Republic of">Korea, Republic of</option>
		<option value="Kuwait">Kuwait</option>
		<option value="Kyrgyzstan">Kyrgyzstan</option>
		<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
		<option value="Latvia">Latvia</option>
		<option value="Lebanon">Lebanon</option>
		<option value="Lesotho">Lesotho</option>
		<option value="Liberia">Liberia</option>
		<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
		<option value="Liechtenstein">Liechtenstein</option>
		<option value="Lithuania">Lithuania</option>
		<option value="Luxembourg">Luxembourg</option>
		<option value="Macau">Macau</option>
		<option value="Madagascar">Madagascar</option>
		<option value="Malawi">Malawi</option>
		<option value="Malaysia">Malaysia</option>
		<option value="Maldives">Maldives</option>
		<option value="Mali">Mali</option>
		<option value="Malta">Malta</option>
		<option value="Marshall Islands">Marshall Islands</option>
		<option value="Martinique">Martinique</option>
		<option value="Mauritania">Mauritania</option>
		<option value="Mauritius">Mauritius</option>
		<option value="Mayotte">Mayotte</option>
		<option value="Mexico">Mexico</option>
		<option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
		<option value="Moldova, Republic of">Moldova, Republic of</option>
		<option value="Monaco">Monaco</option>
		<option value="Mongolia">Mongolia</option>
		<option value="Montenegro">Montenegro</option>
		<option value="Montserrat">Montserrat</option>
		<option value="Morocco">Morocco</option>
		<option value="Mozambique">Mozambique</option>
		<option value="Myanmar">Myanmar</option>
		<option value="Namibia">Namibia</option>
		<option value="Nauru">Nauru</option>
		<option value="Nepal">Nepal</option>
		<option value="Netherlands">Netherlands</option>
		<option value="Netherlands Antilles">Netherlands Antilles</option>
		<option value="New Caledonia">New Caledonia</option>
		<option value="New Zealand">New Zealand</option>
		<option value="Nicaragua">Nicaragua</option>
		<option value="Niger">Niger</option>
		<option value="Nigeria">Nigeria</option>
		<option value="Niue">Niue</option>
		<option value="Norfolk Island">Norfolk Island</option>
		<option value="North Korea">North Korea</option>
		<option value="Northern Mariana Islands">Northern Mariana Islands</option>
		<option value="Norway">Norway</option>
		<option value="Oman">Oman</option>
		<option value="Pakistan">Pakistan</option>
		<option value="Palau">Palau</option>
		<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
		<option value="Panama">Panama</option>
		<option value="Papua New Guinea">Papua New Guinea</option>
		<option value="Paraguay">Paraguay</option>
		<option value="Peru">Peru</option>
		<option value="Philippines">Philippines</option>
		<option value="Pitcairn">Pitcairn</option>
		<option value="Poland">Poland</option>
		<option value="Portugal">Portugal</option>
		<option value="Puerto Rico">Puerto Rico</option>
		<option value="Qatar">Qatar</option>
		<option value="Reunion">Reunion</option>
		<option value="Romania">Romania</option>
		<option value="Russian Federation">Russian Federation</option>
		<option value="Rwanda">Rwanda</option>
		<option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
		<option value="Saint Lucia">Saint Lucia</option>
		<option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
		<option value="Samoa">Samoa</option>
		<option value="San Marino">San Marino</option>
		<option value="Sao Tome and Principe">Sao Tome and Principe</option>
		<option value="Saudi Arabia">Saudi Arabia</option>
		<option value="Senegal">Senegal</option>
		<option value="Serbia">Serbia</option>
		<option value="Seychelles">Seychelles</option>
		<option value="Sierra Leone">Sierra Leone</option>
		<option value="Singapore">Singapore</option>
		<option value="Slovak Republic">Slovak Republic</option>
		<option value="Slovenia">Slovenia</option>
		<option value="Solomon Islands">Solomon Islands</option>
		<option value="Somalia">Somalia</option>
		<option value="South Africa">South Africa</option>
		<option value="South Georgia South Sandwich Islands">South Georgia &amp; South Sandwich Islands</option>
		<option value="South Sudan">South Sudan</option>
		<option value="Spain">Spain</option>
		<option value="Sri Lanka">Sri Lanka</option>
		<option value="St. Barthelemy">St. Barthelemy</option>
		<option value="St. Helena">St. Helena</option>
		<option value="St. Martin (French part)">St. Martin (French part)</option>
		<option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
		<option value="Sudan">Sudan</option>
		<option value="Suriname">Suriname</option>
		<option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
		<option value="Swaziland">Swaziland</option>
		<option value="Sweden">Sweden</option>
		<option value="Switzerland">Switzerland</option>
		<option value="Syrian Arab Republic">Syrian Arab Republic</option>
		<option value="Taiwan">Taiwan</option>
		<option value="Tajikistan">Tajikistan</option>
		<option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
		<option value="Thailand">Thailand</option>
		<option value="Togo">Togo</option>
		<option value="Tokelau">Tokelau</option>
		<option value="Tonga">Tonga</option>
		<option value="Trinidad and Tobago">Trinidad and Tobago</option>
		<option value="Tunisia">Tunisia</option>
		<option value="Turkey">Turkey</option>
		<option value="Turkmenistan">Turkmenistan</option>
		<option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
		<option value="Tuvalu">Tuvalu</option>
		<option value="Uganda">Uganda</option>
		<option value="Ukraine">Ukraine</option>
		<option value="United Arab Emirates">United Arab Emirates</option>
		
		<option value="Uruguay">Uruguay</option>
		<option value="Uzbekistan">Uzbekistan</option>
		<option value="Vanuatu">Vanuatu</option>
		<option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
		<option value="Venezuela">Venezuela</option>
		<option value="Viet Nam">Viet Nam</option>
		<option value="Virgin Islands (British)">Virgin Islands (British)</option>
		<option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
		<option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
		<option value="Western Sahara">Western Sahara</option>
		<option value="Yemen">Yemen</option>
		<option value="Zambia">Zambia</option>
		<option value="Zimbabwe">Zimbabwe</option>
           
</select>
  </div>
  </div>
 
  
  
  <button type="submit" class="btn submitbut" onClick="return val()">Submit</button>
</form>
  </div>
  </div>
  </div>
  </div>
</div>
 <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="javascript:void(0);"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="javascript:void(0);">Pupilclipe</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script>
function validate_pass(that)
 {
 var pass = ($('#password').val());
   var cpass = ($('#confirm_password').val());
  if(pass.length < 8)
  { 
 	$("#lengtherr").show();return false;}
  else if(pass != cpass)
  {	
  
  	 
  	$("#selectPeriodRangePanel").show();
	
	return false;
  }
  else
  {
  return true;
  }
  }
  </script>
	<!-- <script>
	function val(){

    var uname = document.getElementById('fname').value;
	var lname =  document.getElementById('lname').value;
 	var femail = document.getElementById('email_address').value;
  	var fpassword = document.getElementById('password').value;
  	var confirm_password =  document.getElementById('confirm_password').value;
	
    if(!uname){
        document.getElementById("formerror").innerHTML = "Enter First Name";
        return false;
    }
    
	else if(!lname){
        document.getElementById("formerror").innerHTML = "Enter Last Name";
        return false;
    }
	else if(!femail){
        document.getElementById("formerror").innerHTML = "Enter Email Address";
        return false;
    }
	else if(!fpassword){
        document.getElementById("formerror").innerHTML = "Enter First Password";
        return false;
    }
	else if(!confirm_password){
        document.getElementById("formerror").innerHTML = "Enter Confirm Password";
        return false;
    }
	else if(fpassword != confirm_password){
       $("#selectPeriodRangePanel").show();
	setTimeout(function() {
    $('#selectPeriodRangePanel').fadeOut('fast');}, 1000);
        return false;
    }
	else if(fpassword.length < 8){
      $("#lengtherr").show();
	setTimeout(function() {
    $('#lengtherr').fadeOut('fast');}, 1000);
        return false;
    }
	
    else{
        return true;
    }
	
}
	
  </script> -->
  <script>
    function checkmail(id){
      var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(id);
   
     $.ajax({
                    url: "<?php echo base_url(); ?>index.php/welcome/checkemail",
                    //url  : "http://livesoftwaresolution.net/lms/Adminlogin/subjectselect",
                    type: "POST",
                    data: {'id': id},
                    
                    success: function (response)
                    {
                     
                        if (response=="yes") {
                                $('#error').show();
                          $('#error').html("Username already exists. ");
                          

                          
                        }
                        else{
                             if(!re ) {
        $('#error').show();
        $('#error').html(" Wrong email.");
    } else {
        $('#error').hide();
    }
                        }

                    }

                });   
      }
 $('#email_address').on('change', function() {
    var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
    
    if(!re ) {
        $('#error').show();
    } else {
        $('#error').hide();
    }
})
  </script>
   <script>
 $('#curremail').on('change', function() {
    var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
    if(!re) {
        $('#currerror').show();
    } else {
        $('#currerror').hide();
    }
})
  </script>
  </body>
</html>