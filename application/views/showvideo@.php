<!DOCTYPE HTML>
<html>
    <head>

        <title>HowClip</title>

        <meta property="og:title" content="European Travel Destinations">
        <meta property="og:description" content="Offering tour packages for individuals or groups.">
        <meta property="og:image" content="<?php echo base_url() . '/uploads/images/' . $videoDetail[0]->video_img; ?>">
        <meta property="og:url" content="<?php echo $videoDetail[0]->videoname; ?>">
        <meta name="twitter:card" content="summary_large_image">

        <meta property="og:site_name" content="European Travel, Inc.">
        <meta name="twitter:image:alt" content="Alt text for image">
        <meta property="fb:app_id" content="your_app_id" />
        <meta name="twitter:site" content="@website-username">
        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>front/images/favicons.png"/>
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>

        <script>
            $(document).ready(function () {

                var price = document.getElementById('videoprice').value;
                var paydone = document.getElementById('paydone').value;
                var paystatus = document.getElementById('paystate').value;
                var uploader = document.getElementById('vidupload').value;
                var user = document.getElementById('userid').value;


                if ((price > 0) && (paystatus != 'Success'))
                {

                    document.getElementById("myvideo").controls = false;
                }
                if (paystatus == 'Success')
                {

                    document.getElementById("myvideo").controls = true;
                }
                if (user != '' && uploader == user)
                {
                    document.getElementById('pricebefore').style.display = 'none';
                    document.getElementById("myvideo").controls = true;
                }


                if (paydone == 'success')
                {

                    document.getElementById('pricebefore').style.display = "none";
                    document.getElementById("myvideo").controls = true;
                    $(".ppbefore").hide();
                    $(".ppafter").show();
                    document.getElementById('playlibefore').style.display = 'none';
                    document.getElementById('playlisuccess').style.display = 'block';
                }
                if (paydone == 'cancel')
                {


                    document.getElementById("myvideo").controls = false;
                }
                if (uploader == user)
                {
                    $("#subhref").attr("href", 'javascript:void(0)');
                }



            });
        </script>

        <style>
            a {
                color: #0254EB
            }
            a:visited {
                color: #0254EB
            }
            a.morelink {
                text-decoration:none;
                outline: none;

            }
            .morecontent span {
                display: none;
            }
            .increasesize {
                width:14px;
                height:14px;
            }
        </style>
         <style>
            input[type=text] {
                width: 130px;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 10px;
                font-size: 14px;
                background-color: white;
                background-image: url('searchicon.png');
                background-position: 10px 10px; 
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            input[type=text]:focus {
                width: 100%;
                border-radius: 10px;
                outline:0 !important; 
            }

            .search-box input[type=text] {
                width: 50%;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 25px;
                font-size: 14px;
                background-color: white;
                background-image: url(https://www.w3schools.com/howto/searchicon.png);
                background-position: 10px 10px;
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            .search-box input[type=text]:focus {
                outline:0;
            }

            .searchbut
            {
                padding: 5px 15px;
                background: #ddd;
                color:#000;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
            }
            .searchbut:hover ,.searchbut:focus,.searchbut:active:focus{
                background: #ccc;
                color:#000;
                outline:0;
            }
            .advance{
                background: #ddd;
                color:#d60808;
                padding: 5px 15px;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
                font-weight:bold;
               
            }
            a.advance:visited
            {
                color:#d60808;
            }






        </style>

        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">

        <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>front/images/favicons.png"/>
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
        <script type="text/javascript" src="simple_social_share.js"></script>
        <script>
            new WOW().init();
        </script>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#commentsubmit").submit(function () {
                    var comment = document.getElementById('comment').value;
                    var video = document.getElementById('vid').value;
                    var user = document.getElementById('uid').value;
                    var user_id = document.getElementById('userid').value;
                    var userlogo = document.getElementById('userlogo').value;

                    event.preventDefault();
                    if (user_id == "")
                    {
                        return false;
                    } else
                    {
                        $.ajax({
                            url: '<?php echo base_url(); ?>index.php/home/comments',
                            type: "POST",
                            data: {'com': comment, 'vid': video, 'user': user, 'user_id': user_id
                            },
                            success: function (response)
                            {
                                var jsonobj = $.parseJSON(response);
                                console.log(jsonobj.comment);
                                document.getElementById('comment').value = "";
                                document.getElementById('commentdiv').style.display = "none";
                                document.getElementById('commdiv').style.display = "block";
                                //$('#returncomment').html(jsonobj.comment);
                                //$('#returnuser').html(jsonobj.user);
                                $("#commdiv").append("<li class='media'><div class='media-left'><a href='#'><img class='media-object' width='80px' height='70px' src='<?php echo base_url(); ?>/uploads/" + userlogo + "'></a></div><div class='media-body' style='margin-top:5px;'><p>" + jsonobj.comment + "</p><p> " + jsonobj.user + "</p><p style='font-size:12px; margin:10px 0px;'><a href='#'> </a></p><ul class='media-list'></ul></div></li>");
                            }

                        });
                    }

                });
            });

            function cancelcom()
            {
                //alert('abc');
            }
        </script>
        <script>
            $(document).ready(function () {
                var showChar = 100;
                var ellipsestext = "...";
                var moretext = "more";
                var lesstext = "less";
                $('.more').each(function () {
                    var content = $(this).html();

                    if (content.length > showChar) {

                        var c = content.substr(0, showChar);
                        var h = content.substr(showChar - 1, content.length - showChar);

                        var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                        $(this).html(html);
                    }

                });

                $(".morelink").click(function () {
                    if ($(this).hasClass("less")) {
                        $(this).removeClass("less");
                        $(this).html(moretext);
                    } else {
                        $(this).addClass("less");
                        $(this).html(lesstext);
                    }
                    $(this).parent().prev().toggle();
                    $(this).prev().toggle();
                    return false;
                });
            });
        </script>
        <script>
            function getplay(){
          
                $("#play").css("display", "none");
                var aa = $(".playform").css("display", "block");

                if (aa.length > 0)
                {

                    setTimeout(function () {
                        $('#playli').click();
                    }, 01);
                    setTimeout(function () {
                        $('#playname').focus();
                    }, 02);

                    $('ul.nojs *').click(function (e) {
                        e.stopPropagation();
                    });



                }

            }
        </script>

        <script>function fbs_click() {
                u = location.href;
                t = <?php echo $videoDetail[0]->videoname; ?>
                window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url() ?>index.php/home/showvideo/368');
                return false;
            }</script>

        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">

    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 

            <div class="sticky-header header-section ">
                <div class="header-left"> 
<input type="hidden" id="link1" value="<?php echo $_GET['link'];?>">
                    <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bars"></i>
                        </button>
                        <?php  if ($this->session->userdata('id') != '') { ?>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
                                <?php foreach ($features as $videofeature) { ?>
                                    <li> <a href="<?php echo base_url() ?>index.php/welcome/featurevideo/<?php echo $videofeature->name; ?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name; ?> </a> </li>
                                <?php } ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
                                <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"><i class="fa fa-th-large nav_icon"></i> My Channel </a> </li>
                                <li class="divider" role="seperator"></li>
                                <li> <a href="#"><i class="fa fa-th-large nav_icon"></i>SUBSCRIPTIONS </a> </li>
                                <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                    <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $uploader->username; ?> </a> </li>
                                <?php } ?>
                            </ul>
                        <?php } ?>
                    </div>
                    <!--toggle button end--> 
                    <!--logo -->
                    <?php
                    foreach ($companydetail as $company) {
                        if ($company->company_logo != "") {
                            $cmplogo = base_url() . "/Admin/uploads/$company->company_logo";
                        } else {
                            $cmplogo = base_url() . "/uploads/logo.png";
                        }
                        ?>
                        <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">

                                <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                            </a> </div>
                    <?php } ?>

                    <!--//logo--> 
                    <!--search-box-->
                   <form class="input" method="post" id="my_form" action="<?php echo base_url() ?>index.php/welcome/getsearchdata">
                        <div class="search-box">
                            <input type="text" style="width:55%;" id="input-31" onKeyUp="ajaxSearch();" name="search_data" placeholder="Search..">

                            <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:55%;z-index:10000;">
                                <div id="autoSuggestionsList"></div>
                            </div>

                            <a href="javascript:void(0)" id="chkval"  class="btn searchbut"> Search  </a>
                            <a href="<?php echo base_url() ?>index.php/welcome/open_advanceSearch" class="btn advance"> Advanced Topic Search  </a>
                        </div>




                    </form>
                    <!--//end-search-box-->
                    <div class="clearfix"> </div>
                </div>
                <div class="header-right">
                    <div class="profile_details_left"><!--notifications of menu start -->
                        <ul class="nofitications-dropdown">
                            <?php
                            if ($this->session->userdata('id') != "") {
                                $ppage = "uploadpage";
                            } else {
                                $ppage = "loginpage/upload";
                            }
                            ?>
                            <?php if ($this->session->userdata('id') == "") { ?>
                                <li class="dropdown head-dpdn" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > 
                                <?php } ?>
                                <?php if ($this->session->userdata('id') != "") { ?>

                                <li class="dropdown head-dpdn"><a href="<?php echo base_url() ?>index.php/home/loginpage/upload"><img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" ></a>
                                <?php } ?>
                            </li><?php //echo base_url()index.php/home/loginpage    ?>
                            <?php if ($this->session->userdata('id') == "") { ?>

                                <li class="dropdown head-dpdn" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url(); ?>assets/images/signinbut.png" class="img-responsive but"></li>
                      <!-- <li class="dropdown head-dpdn"> <a href="#"> <img src="<?php echo base_url(); ?>assets/images/signinbut.png" id="pop" data-toggle="modal" data-target="#myModal" class="img-responsive but"></a> </li>-->
                            <?php } ?>
<!--                            <li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/signup"> <img src="<?php echo base_url(); ?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>-->
                            <?php
                            foreach ($userDetail as $user) {
                                $username = $user->username;
                                $userimg = $user->userLogo;
                                if ($userimg == "") {
                                    $userimg = "user.png";
                                } else {
                                    $userimg = $user->userLogo;
                                }
                            }
                            ?>
                            <?php if ($this->session->userdata('id') != "") { ?>
                                <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="notification_header">
                                                <h3><?php echo $username; ?></h3>
                                            </div>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p><?php echo $this->session->userdata('email'); ?></p>

                                                </div>
                                                <div class="clearfix"></div>
                                            </a></li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                        </li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                        <div class="clearfix"> </div>
                    </div>


                </div>


            </div>

            <div class="uploadwrap">
                <?php foreach ($videoDetail as $video) { //echo '<pre>';print_r($video) ;die;  ?>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div id="vvideo" style="">
                                    <video controls id="myvideo" style="width: 75%;
                                           height: 400px;
                                           margin: auto;
                                           display: block;background-color:black;">
                                        <source src="<?php echo base_url(); ?>uploads/live/<?php echo $video->name; ?>" type="video/mp4">

                                    </video>

                                    <input type="hidden" id="videoprice" value="<?php echo $video->price; ?>" >
                                    <input type="hidden" id="paystate" value="<?php echo $video->Paystate; ?>" >
                                    <input type="hidden" id="paydone" value="<?php echo $payment; ?>" >
                                    <input type="hidden" id="vidupload" value="<?php echo $video->userId; ?>" >
                                    <?php if (($video->price > 0) && ($video->Paystate != 'Success')) { ?>
                                  
                                        <div class="pricetag" id="pricebefore" style="display:block;" onclick="changeprice()" >
                                           <?php if($video->price>0.00){ ?>
                              View Clip<br>  <input type="hidden" value="price" id="auto">
                           <?php }else{ ?>
                            View Clip<br>
                            <?php } ?>
                                            $ <?php echo $video->price; ?>
                                        </div>
                                        <?php // ($video->userId) != ($this->session->userdata('id'))  ?>
                                        <div class="agreetag" id="priceafter" style="display:none">
                                            <p> <?php if($video->price>0.00){ ?>
                             View this Clip
                           <?php }else{ ?>
                            View this clip
                            <?php } ?> for $ <?php echo $video->price; ?></p>
                                            <span style="text-align:center;display:block;"><button  type="button" <?php if ($this->session->userdata('id') == "") { ?> class="pop agreesub"  data-toggle="modal" data-target="#myModal" <?php } else { ?>class="btn agreesub" onclick="makepayment('<?php echo $video->price; ?>', '<?php echo $video->id; ?>', '<?php echo $video->userId; ?>');"<?php } ?>  >Agree</button>
                                                <button class="btn disagreesub" onclick="canpay()" type="submit">Don't Agree</button></span>
                                        </div>
                                        <!--   -->
<!--                                        <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="makepayment">-->
                                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="makepayment">
                                            <!-- Identify your business so that you can collect the payments. -->
<!--                                            <input type="hidden" name="business" value="<?php echo "logo@livesoftwaresolution.info"; ?>">-->
                                                <input type="hidden" name="business" value="<?php echo "david@howclip.com"; ?>">
                                            <!-- Specify a Buy Now button. -->
                                            <input type="hidden" name="cmd" value="_xclick">

                                            <!-- Specify details about the item that buyers will purchase. -->
                                            <input type="hidden" name="item_name" value="<?php echo $video->videoname; ?>"> 

                                            <input type="hidden" name="video_id" value="<?php echo $video->id; ?>">
                                            <input type="hidden" name="amount" value="<?php echo $video->price; ?>">
                                            <input type="hidden" name="currency_code" value="USD">

                                            <!-- Specify URLs -->
                                            <input type='hidden' name='cancel_return' id="cancelpay" >
                                            <input type='hidden' name='return' id="subpay" >


                                            <!-- Display the payment button. -->
                                    <!--        <input type="image" name="Pay" border="0">-->


                                        </form>



                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8" >
                                <div class="row">
                                    <div class="col-md-12">
    
    
                                        <div class="blankpage">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <br>

                                                <?php } ?>
                                            </div>
                                            <h4 id="videoplaysuccess" style="font-weight: 400;color:green;display:none;">Video Added Successfully Into the Playlist  <span class="badge"></span></h4>
                                            <h4 id="videoplayfail" style="font-weight: 400;color:green;display:none;">Video Deleted Successfully from the Playlist  <span class="badge"></span></h4>
                                            <?php 
                                            
                                            if ($video->userimg == "") {
                                                $userimg = "user.png";
                                            } else {
                                                $userimg = $video->userimg;
                                            }
                                            ?>
                                            <div class="iconimg"><a href="<?php echo base_url(); ?>index.php/welcome/mychannel/<?php echo $video->userId; ?>"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></a></div>

                                            <div class="videosubscribe">
                                                <?php foreach ($videoDetail as $g) { ?>
                                                    <span style="font-size:16px;"><?php echo ucwords($g->videoname); ?></span><br>
                                                    <a href="<?php echo base_url(); ?>index.php/welcome/mychannel/<?php echo $video->userId; ?>">   <?php echo ucwords($g->username); ?></a>
                                                    <br>
                                                    <?php
                                                    if ($this->session->all_userdata('url') != "") {
                                                        $this->session->unset_userdata('url');
                                                    }
                                                    ?>

                                                    <?php if ($g->subscriber == "0") { ?>
                                                    <a id="subhref1" <?php if ($this->session->userdata('id') == '') { ?> data-toggle="modal" data-target="#myModal" <?php } else { ?> onclick="dynamicsubscribe('<?php echo $video->userId; ?>');"  <?php } ?>><img src="<?php echo base_url(); ?>assets/images/download.jpg" class="img-responsive"></a>

                                                    <?php } ?>
                                                    <a id="subhref4" style="display:none" <?php if ($this->session->userdata('id') == '') { ?> data-toggle="modal" data-target="#myModal" <?php } else { ?> onclick="dynamicsubscribe('<?php echo $video->userId; ?>');"  <?php } ?>><img src="<?php echo base_url(); ?>assets/images/download.jpg" class="img-responsive"></a>
                                                <?php } ?>
                                                <?php if ($g->subscriber == "1") { ?>
                                                    <a id="subhref2" <?php if ($this->session->userdata('id') == '') { ?> data-toggle="modal" data-target="#myModal" <?php } else { ?> onclick="dynamicunsubscribe('<?php echo $video->userId; ?>');"  <?php }?> > 
                                                        
                                                       <img src="<?php echo base_url(); ?>assets/images/unsubscribe.jpg" class="img-responsive"></a><?php } ?>
                                                        
                                                       <a id="subhref3" style="display:none;" <?php if ($this->session->userdata('id') == '') { ?> data-toggle="modal" data-target="#myModal" <?php } else { ?> onclick="dynamicunsubscribe('<?php echo $video->userId; ?>');"  <?php }?> > 
                                                        
                                                       <img src="<?php echo base_url(); ?>assets/images/unsubscribe.jpg" class="img-responsive"></a> 
                                                      

                                            </div>
                                            <?php foreach ($Allview as $v) { ?>
                                                <p align="right" style="display:inline-block;float:right;margin-top:15px;"><?php
                                                    if ($v->view == "") {
                                                        echo "0";
                                                    } else {
                                                        echo $v->view;
                                                    }
                                                    ?> Views</p><?php } ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="likebox">
                                                    <div class="sharemen">

                                                        <ul>
                                                            <?php
                                                            foreach ($videoDetail as $li) {
                                                                $liprice = $li->price;
                                                            } if (($this->session->userdata('id') == "")) {
                                                                ?>
                                                                <li class="dropdown play" id="playul" <?php if ($this->session->userdata('id') != "") { ?> style="display:none;" <?php } ?>>

                                                                    <?php if ($liprice > 0) { ?>
                                                                        <span class="ppbefore"><a id="playli"   class="pop" data-toggle="modal" data-target="#myModal" role="button" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus" aria-hidden="true"></i> Add to </a></span>
                                                                    <?php } ?>
                                                                </li>

                                                            <?php } ?> 

                                                            <?php if (($this->session->userdata('id') != "")) { ?>
                                                                <?php
                                                                foreach ($videoDetail as $li) {
                                                                    $gvideo = $li->id;
                                                                    $guser = $li->userId;
                                                                    if ($li->price > 0) {
                                                                        ?>
                                                                        <li class="dropdown play" id="playul">


                                                <!--   <span class="ppbefore"><a id="playli"  <?php if (($li->price > 0) && ($li->Paystate != 'Success')) { ?> onclick="makepayment('<?php echo $li->price; ?>','<?php echo $li->id; ?>','<?php echo $li->userId; ?>');"  <?php } if (($li->price < 0) || ($li->Paystate == 'Success')) { ?> class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" <?php } ?> ><i class="fa fa-plus" aria-hidden="true"></i> Add to </a></span>-->

                                                                            <a id="playli"   class="dropdown-toggle ppafter" <?php if (($li->price > 0) && ($li->Paystate != 'Success') && ($li->userId != $this->session->userdata('id'))) { ?> onclick="makepayment('<?php echo $li->price; ?>', '<?php echo $li->id; ?>', '<?php echo $li->userId; ?>');"  <?php } if (($li->price < 0) || ($li->Paystate == 'Success') || ($li->userId == $this->session->userdata('id'))) { ?>  data-toggle="dropdown"   role="button" aria-haspopup="true" aria-expanded="false" <?php } ?> > <i class="fa fa-plus" aria-hidden="true"></i> Add to </a>   
                                                                            <ul class="dropdown-menu nojs">
                                                                                <li><span id="Err"></span></li>

                                                                                <li id="fplay" style="display:none"><a href="javascript:void;"><input type="checkbox" id="videochk" checked="checked" onChange="removevideo(this.id, '<?php echo $li->id; ?>', '<?php echo $li->userId; ?>');"><span id="playlistname" style="font-size:14px;"></span><span id="playlistid" style="display:none"></span></a></li>
                                                                                <?php
                                                                                $i = 0;
                                                                                foreach ($playlist as $play) {
                                                                                    ?>
                                                                                    <li><?php $playarr = $play->videos; ?><a href="javascript:void;"><input type="checkbox" name="videochk[]" class="case increasesize" id="<?php echo $i; ?>" <?php if (in_array($gvideo, $playarr)) { ?> checked="checked" <?php } ?> onChange="addremovevideo(this.id, '<?php echo $gvideo; ?>', '<?php echo $guser; ?>', '<?php echo $play->id; ?>', '<?php echo $play->playlist_name; ?>');"><?php echo $play->playlist_name; ?></a></li><?php
                                                                                    $i++;
                                                                                }
                                                                                ?>

                                                                                <li role="separator" class="divider"></li>
                                                                                <li id="play" onClick="getplay();"><a href="javascript:void;">Create a new Playlist</a></li>
                                                                                <?php foreach ($videoDetail as $li) { ?>
                                                                                    <form id="myForm">
                                                                                        <li class="playform" style="display:none;"><a href="javascript:void;"><input name="playname" id="playname" type="text" style="width: 100%;height:25px;"></a></li>
                                                                                        <li class="playform" style="display:none;"><span style="width:55%;display:inline;padding:0px;"><select name="playprivacy" id="playprivacy" style="width:60%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;"><option value="public">Public</option><option value="unlisted">Unlisted</option><option value="private">Private</option></select></span><span style="width:40%;display:inline;padding:0px;"><input type="button" value="Create" onClick="createplay('<?php echo $this->session->userdata('id') ?>', '<?php echo $li->id; ?>', '<?php echo $li->userId; ?>');" style="width:37%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;margin-left:5px;background-color: darkslateblue;
                                                                                                                                                                                                                        color: white;"/></span></li>
                                                                                    </form>
                                                                                </ul>
                                                                            </li>
                                                                            <?php
                                                                        }
                                                                    }
                                                                }
                                                                ?>
                                                            <?php } ?>  






















<!--                                                            <a href="http://www.facebook.com/sharer.php?u=https://simplesharebuttons.com" target="_blank">
                                                                 <img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />
                                                             </a>
                                                           <ul>
                                                            <?php
                                                            foreach ($videoDetail as $li) {
                                                                $gvideo = $li->id;
                                                                $guser = $li->userId;
                                                                if ($li->price > 0) {
                                                                    ?>
                                                                                           <li class="dropdown play" id="playul">
                                                                                          
                                                                                         
                                                                                            <span class="ppbefore"><a id="playli"  <?php if ($this->session->userdata('id') == "") { ?> class="pop" data-content="Please Sign In!" <?php } else if ($this->session->userdata('id' != "") || ($li->price > 0)) { ?> onclick="makepayment('<?php echo $li->price; ?>','<?php echo $li->id; ?>','<?php echo $li->userId; ?>');"  <?php } else { ?>class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" <?php } ?> ><i class="fa fa-plus" aria-hidden="true"></i> Add to </a></span>
                                                                                            <a id="playli"   class="dropdown-toggle ppafter" style="display:none" data-toggle="dropdown"   role="button" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus" aria-hidden="true"></i> Add to </a>   
                                                                                               <ul class="dropdown-menu nojs">
                                                                                                           <li><span id="Err"></span></li>
                                                                                                            
                                                                                                     <li id="fplay" style="display:none"><a href="#"><input type="checkbox" id="videochk" checked="checked" onChange="removevideo(this.id,'<?php echo $li->id; ?>','<?php echo $li->userId; ?>');"><span id="playlistname" style="font-size:14px;"></span><span id="playlistid" style="display:none"></span></a></li>
                                                                    <?php
                                                                    $i = 0;
                                                                    foreach ($playlist as $play) {
                                                                        ?>
                                                                                                                     <li><?php $playarr = $play->videos; ?><a href="#"><input type="checkbox" name="videochk[]" class="case" id="<?php echo $i; ?>" <?php if (in_array($gvideo, $playarr)) { ?> checked="checked" <?php } ?> onChange="addremovevideo(this.id,'<?php echo $gvideo; ?>','<?php echo $guser; ?>','<?php echo $play->id; ?>','<?php echo $play->playlist_name; ?>');"><?php echo $play->playlist_name; ?></a></li><?php
                                                                        $i++;
                                                                    }
                                                                    ?>
                                                                                                   
                                                                                                     <li role="separator" class="divider"></li>
                                                                                                     <li id="play" onClick="getplay();"><a href="#">Create a new Playlist</a></li>
                                                                    <?php foreach ($videoDetail as $li) { ?>
                                                                                                                    <form id="myForm">
                                                                                                                     <li class="playform" style="display:none;"><a href="#"><input name="playname" id="playname" type="text" style="width: 100%;height:25px;"></a></li>
                                                                                                                                  <li class="playform" style="display:none;"><span style="width:55%;display:inline;padding:0px;"><select name="playprivacy" id="playprivacy" style="width:60%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;"><option value="public">Public</option><option value="unlisted">Unlisted</option><option value="private">Private</option></select></span><span style="width:40%;display:inline;padding:0px;"><input type="button" value="Create" onClick="createplay('<?php echo $this->session->userdata('id') ?>','<?php echo $li->id; ?>','<?php echo $li->userId; ?>');" style="width:37%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;margin-left:5px;background-color: darkslateblue;
                                                                                                             color: white;"/></span></li>
                                                                                                                 </form>
                                                                                                                   </ul>
                                                                                                                 </li>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>-->

                                                            <?php
                                                            foreach ($videoDetail as $li) {
                                                                $gvideo = $li->id;
                                                                $guser = $li->userId;
                                                                if ($li->price <= 0) {
                                                                    ?>
                                                                    <li class="dropdown play" id="playul">

                                <!--<a id="playli"   class="dropdown-toggle" data-toggle="dropdown"   role="button" aria-haspopup="true" aria-expanded="false" ><i class="fa fa-plus" aria-hidden="true"></i> Add to </a>-->


                                                                        <a id="playli"  <?php if ($this->session->userdata('id') == "") { ?> class="pop" data-toggle="modal" data-target="#myModal" <?php } else { ?>class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" <?php } ?> ><i class="fa fa-plus" aria-hidden="true"></i> Add to </a>

                                                                        <ul class="dropdown-menu nojs">
                                                                            <li><span id="Err"></span></li>

                                                                            <li id="fplay" style="display:none"><a href="javascript:void;"><input type="checkbox" id="videochk" checked="checked" onChange="removevideo(this.id, '<?php echo $li->id; ?>', '<?php echo $li->userId; ?>');"><span id="playlistname" style="font-size:14px;"></span><span id="playlistid" style="display:none"></span></a></li>
                                                                            <?php
                                                                            $i = 0;
                                                                            foreach ($playlist as $play) {
                                                                                ?>
                                                                                <li><?php $playarr = $play->videos; ?><a href="javascript:void;"><input type="checkbox" name="videochk[]" class="case increasesize" id="<?php echo $i; ?>" <?php if (in_array($gvideo, $playarr)) { ?> checked="checked" <?php } ?> onChange="addremovevideo(this.id, '<?php echo $gvideo; ?>', '<?php echo $guser; ?>', '<?php echo $play->id; ?>', '<?php echo $play->playlist_name; ?>');"><?php echo $play->playlist_name; ?></a></li><?php
                                                                                $i++;
                                                                            }
                                                                            ?>

                                                                            <li role="separator" class="divider"></li>
                                                                            <li id="play" onClick="getplay();"><a href="javascript:void;">Create a new Playlist</a></li>
                                                                            <?php foreach ($videoDetail as $li) { ?>
                                                                                <form id="myForm">
                                                                                    <li class="playform" style="display:none;"><a href="javascript:void;"><input name="playname" id="playname" type="text" style="width: 100%;height:25px;"></a></li>
                                                                                    <li class="playform" style="display:none;"><span style="width:55%;display:inline;padding:0px;"><select name="playprivacy" id="playprivacy" style="width:60%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;"><option value="public">Public</option><option value="unlisted">Unlisted</option><option value="private">Private</option></select></span><span style="width:40%;display:inline;padding:0px;"><input type="button" value="Create" onClick="createplay('<?php echo $this->session->userdata('id') ?>', '<?php echo $li->id; ?>', '<?php echo $li->userId; ?>');" style="width:37%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;margin-left:5px;background-color: darkslateblue;
                                                                                                                                                                                                                    color: white;"/></span></li>
                                                                                </form>
                                                                            </ul>
                                                                        </li>
                                                                        <?php
                                                                    }
                                                                }
                                                            }
                                                            ?>




                                                            <?php foreach ($videoDetail as $li) { ?>
                                                                <li class="dropdown">
                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-share" aria-hidden="true"></i> Share </a>
                                                                    <ul class="dropdown-menu" style="min-width: 125px;">

                                                                        <li style="padding:5px 15px;"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $li->id; ?>" target="_blank" style="display:inline;padding:5px 5px;"><img src="https://simplesharebuttons.com/images/somacro/facebook.png" width="30px;" height="30px;" alt="Facebook" /> </a><a href="https://plus.google.com/share?url=<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $li->id; ?>" onClick="javascript:window.open(this.href,
                                                                                        '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');
                                                                                return false;" style="display:inline;padding:5px 5px;">
    <!--                                                                                <img src="https://simplesharebuttons.com/images/somacro/google.png" width="30px;" height="30px;" alt="Google" />-->
                                                                                                                                                                                                            </a>
                                                                                                                                                                                                            <a href="https://twitter.com/share?url=<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $li->id; ?>&amp;text=<?php echo $li->videoname; ?>&amp;hashtags=simplesharebuttons" target="_blank">
                                                                                                                                                                                                            <img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
                                                                                                                                                                                                            </a>
                                                                                                                                                                                                            <!--                                                                            <a class="twitter-share-button"
                                                                                                                                                                                                            href="https://twitter.com/intent/tweet?text=<?php echo $li->videoname; ?>&url=<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $li->id; ?>&widget_type=video" style="display:inline;padding:5px 5px;">
                                                                                                                                                                                                            <img src="https://simplesharebuttons.com/images/somacro/twitter.png" width="30px;" height="30px;" alt="Twitter" />
                                                                                                                                                                                                            </a>-->
                                                                                                                                                                                                            <!--                                                                            <a href="https://twitter.com/share?url=<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $li->id; ?>" &amp;text="Simple%20Share%&amp;hashtags=simpleshare&quot;" target="_blank"><img src="http://livesoftwaresolution.in/popdini/front/images/botones redes soc twitter.png" class="img-circle img-responsive" alt="Twitter"></a>
                                                                                                                                                                                                            -->

                                                                        </li>
                                                                    </ul>
                                                                </li>

                                                                <li class="dropdown">
                                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" onClick="generateiframe('<?php echo $li->id; ?>');"</i> Embed</a>
                                                                    <ul class="dropdown-menu iframe">
                                                                        <li class="dropdown.mega-dropdown"><span id="embedcode"></span></li>

                                                                    </ul>
                                                                </li>      

                                                            </ul>

                                                        <?php } ?>
                                                    </div>

                                                    <div class="likemen">


                                                        <?php
                                                        foreach ($videoDetail as $v) {
                                                            $video = $v->id;
                                                            $uid = $this->session->userdata('id');
                                                        }
                                                        if ($this->session->userdata('id') != "") {
                                                            $uid == "";
                                                            $subarray = array('url' => "showvideo/$video->id");
                                                            $this->session->set_userdata($subarray);
                                                        }
                                                        ?>
                                                        <ul>

                                                            <li><a href="javascript:void(0)" <?php if ($uid == "") { ?> class="pop" data-toggle="modal" data-target="#myModal"<?php } ?>  onClick="Submitlike('<?php echo $video; ?>', '<?php echo $uid; ?>');"> <i class="fa fa-thumbs-up" aria-hidden="true"></i> <span id="likes"><?php
                                                                        if ($likes == "") {
                                                                            echo "0";
                                                                        } else {
                                                                            echo $likes;
                                                                        }
                                                                        ?>  </span></a></li>
                                                            <li><a href="javascript:void(0)" <?php if ($uid == "") { ?> class="pop" data-toggle="modal" data-target="#myModal"<?php } ?>  onClick="Submitdislike('<?php echo $video; ?>', '<?php echo $uid; ?>');"><i class="fa fa-thumbs-down" aria-hidden="true"></i> <span id="dislikes"><?php
                                                                        if ($Unlikes == "") {
                                                                            echo "0";
                                                                        } else {
                                                                            echo $Unlikes;
                                                                        }
                                                                        ?>  </span> </a></li>
                                                        </ul>
                                                    </div>

                                                </div> 

                                            </div>
                                        </div>
                                    </div>
                                    <div id="message" style="display:none;float:right;margin-right:25px;"><span style="font-size:13px;color:#990000;padding-bottom:5px;">Please Login...</span></div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blankpage">

                                        <?php
                                        foreach ($videoDetail as $vid) {
                                            $date = explode('-', $vid->Date);
                                            if ($date[1] == '01') {
                                                $month = 'Jan';
                                            } if ($date[1] == '02') {
                                                $month = 'Feb';
                                            } if ($date[1] == '03') {
                                                $month = 'Mar';
                                            } if ($date[1] == '04') {
                                                $month = 'Apr';
                                            }if ($date[1] == '05') {
                                                $month = 'May';
                                            } if ($date[1] == '06') {
                                                $month = 'Jun';
                                            } if ($date[1] == '07') {
                                                $month = 'Jul';
                                            } if ($date[1] == '08') {
                                                $month = 'Aug';
                                            } if ($date[1] == '09') {
                                                $month = 'Sep';
                                            } if ($date[1] == '10') {
                                                $month = 'Oct';
                                            } if ($date[1] == '11') {
                                                $month = 'Nov';
                                            } if ($date[1] == '12') {
                                                $month = 'Dec';
                                            }
                                            $desc = $vid->description;
                                            $cat = $vid->video_category;
                                        }
                                        ?>
                                        <p style="padding: 5px 0px 5px 0px;"><span>Published on <?php echo $date[2] . " " . $month . " " . $date[0]; ?> </span></p>
                                        <div class="comment more">
                                            <?php echo $desc; ?>
                                        </div>


                                        <p style="padding: 5px 0px 5px 0px;"><span>Category:</span><span style="margin-left:15px;"><?php if ($vid->par4title != "") { ?><a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult/<?php echo $vid->par4Id; ?>'><?php echo $vid->par4title; ?></a> > <?php } ?><?php if ($vid->par3title != "") { ?><a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult/<?php if ($vid->par4Id != '') {
                                                    echo $vid->par4Id . '/';
                                                } echo $vid->par3Id; ?>'><?php echo $vid->par3title; ?></a> > <?php } ?><?php if ($vid->par2title != "") { ?> <a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult/<?php if ($vid->par4Id != '') {
                                                    echo $vid->par4Id . '/';
                                                } if ($vid->par3Id != '') {
                                                    echo $vid->par3Id . '/';
                                                } echo $vid->par2Id; ?>'><?php echo $vid->par2title; ?></a> > <?php } ?><?php if ($vid->par1title != "") { ?><a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult/<?php if ($vid->par4Id != '') {
                                                    echo $vid->par4Id . '/';
                                                }if ($vid->par3Id != '') {
                                                    echo $vid->par3Id . '/';
                                                }if ($vid->par2Id != '') {
                                                    echo $vid->par2Id . '/';
                                                }echo $vid->par1Id; ?>'><?php echo $vid->par1title; ?></a> ><?php } ?> <a href='<?php echo base_url() ?>index.php/welcome/getadvanceresult/<?php if ($vid->par4Id != '') {
                                                echo $vid->par4Id . '/';
                                            } if ($vid->par3Id != '') {
                                                echo $vid->par3Id . '/';
                                            }if ($vid->par2Id != '') {
                                                echo $vid->par2Id . '/';
                                            } if ($vid->par1Id != '') {
                                                echo $vid->par1Id . '/';
                                            } echo $vid->categoryId; ?>'><?php echo $vid->video_category; ?></a></span></p>


                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blankpage">
                                        <div class="blankhead">Comments</div>
                                        <div class="commentprofile">
                                            <div class="row">
                                                <div class="col-md-2" style="padding-right:0px;">
                                                    <img src="<?php echo base_url(); ?>assets/images/37610397-good-wallpapers.jpg" class="img-responsive">
                                                </div>
                                                <?php foreach ($videoDetail as $vid) { ?>
                                                    <div class="col-md-10" style="padding-left:10px;">
                                                        <form method="post" action="#" id="commentsubmit">
                                                            <div class="row" style="margin:0px;">
                                                                <div class="col-md-12" style="padding-left:0px; padding-right:0px;">
                                                                    <textarea class="form-control commentform" style="resize:none" id="comment" rows="2" onClick="showcomments()" name="commnets"></textarea>
                                                                    <input type="hidden" name="video_id" id="vid" value="<?php echo $vid->id; ?>">

                                                                    <input type="hidden" name="user_id" id="uid" value="<?php echo $this->session->userdata('name'); ?>"> <input type="hidden" name="user" id="userid" value="<?php echo $this->session->userdata('id'); ?>">
                                                                </div>
                                                            </div>
                                                            <div class="row" id="commentdiv" style="display:none;margin:0px;">
                                                                <div class="col-md-6"></div>
                                                                <div class="col-md-6" style="padding-left:0px; padding-right:0px;">

                                                                    <button <?php if ($this->session->userdata('id') == "") { ?> class="btn commenter pop" onclick="return oooplog();"  <?php } else { ?> class="btn commenter" type="submit" id="commentsubmit" <?php } ?> >Comment</button> <button class="btn commentercanc" type="submit" onClick="cancelcom();">Cancel</button>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
<?php } ?>
                                            </div>
                                        </div>

                                        <div class="row">
<?php
foreach ($videoDetail as $vid) {
    $userlogo = $vid->userlogo;
    if ($vid->userlogo == '') {
        $userlogo = "user.png";
    }
}
?>
                                            <div class="col-md-12"><input type="hidden" id="userlogo" value="<?php echo $userlogo; ?>">

                                                <ul class="media-list" id="commdiv"></ul>

                                                <ul class="media-list">

                                                    <!--  <li class="media"  style="display:none;">
                                                      
                                                        <div class="media-left">
                                                          <a href="#">
                                                            <img class="media-object"  width="80px" height="70px" src="<?php echo base_url(); ?>uploads/<?php echo $userlogo; ?>" alt="...">
                                                          </a>
                                                        </div>
                                                          <div class="media-body" id="gcom" style="margin-top:5px;" >
                                                        
                                                         <p id="returncomment" ></p>
                                                                <p id="returnuser"></p>
                                                     
                                                         <p style="font-size:12px; margin:10px 0px;"><a href="#"> </a></p>
                                                         <ul class="media-list">
                                                      
                                                    </ul>
                                                        </div>
                                                      </li>
                                                    -->
<?php
foreach ($Allcomment as $comm) {
    if ($comm->userlogo == "") {
        $imguser = "user.png";
    } else {
        $imguser = $comm->userlogo;
    }
    ?>
                                                        <li class="media">
                                                            <div class="media-left">
                                                                <a href="#">
                                                                    <img class="media-object" src="<?php echo base_url(); ?>uploads/<?php echo $imguser; ?>" alt="..." width="80px" height="70px">
                                                                </a>
                                                            </div>
                                                            <div class="media-body">

                                                                <p><?php echo $comm->comment; ?></p>
                                                                <p><?php echo $comm->user; ?></p>

                                                                <p style="font-size:12px; margin:10px 0px;"><a href="#"></a></p>
                                                                <ul class="media-list">

                                                                </ul>
                                                            </div>
                                                        </li>
<?php } ?>



                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                          <div class="col-md-4">
                          <div class="row">
                          <div class="col-md-12">
                          <div class="blankpage">
                             
<?php foreach ($Allvideo as $ggvid) { ?>
                                          <div class="upcommming">
                                          <div class="media">
                                          
                                          <div class="media-left">
                            <?php
                           //if (getimagesize('uploads/images/' . $ggvid->video_img !==false))
                             if (getimagesize('uploads/images/' . $ggvid->videothumb))
                            {
                                $link = base_url() . "/uploads/images/" . $ggvid->videothumb;
                            } else {
                                $link = base_url() . "/uploads/images/download.jpg";
                            }
                            ?>
                                            <a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $ggvid->id; ?>">
                                              <img class="media-object sideimage" src=<?php echo $link; ?> alt="...">
                                            <div class="dollar1">
                                                <?php if ($ggvid->price > 0) { ?>
                                                
                              <img src="<?php echo base_url() ?>assets/images/usd1600.png" class="img-responsive">
                                            <?php } ?>
<!--                                                <img src="<?php echo base_url() ?>/assets/images/usd1600.png" class="img-responsive">-->
                                            </div>
                                            </a>
                                                 
                                          </div>
                                          <div class="media-body">
                                            <h4 class="media-heading"><a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $ggvid->id; ?>"><?php echo ucwords($ggvid->videoname); ?></a></h4>
                                           <p style="font-size:12px;"><?php echo ucwords($ggvid->video_category); ?></p>
                                            <p style="font-size:12px;"><?php
                        if ($ggvid->videoview == "") {
                            echo "0";
                        } else {
                            echo $ggvid->videoview;
                        }
                        ?> Views</p>
                                          </div>
                                        </div>
                                        </div>
                                                    <?php } ?>
                        
                        
                          </div>
                          
                          
                          </div>
                          </div>
                          </div>
<!--                        <div class="col-md-4">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="blankpage">
<?php foreach ($Allvideo as $ggvid) { ?> 
                                            <div class="upcommming">
                                                <div class="media">
                                                    <div class="media-left">
                                                            <?php
                                                             if (getimagesize('uploads/images/' . $ggvid->video_img !== false))
                                                             {
                                                                $link = base_url() . "/uploads/images/" . $ggvid->video_img;
                                                            } else {
                                                                $link = base_url() . "/uploads/images/download.jpg";
                                                            }
                                                            ?>
                                                        <a href="<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $ggvid->id; ?>">
                                                            <img class="media-object sideimage" src=<?php echo $link; ?> alt="...">
    <?php if ($ggvid->price > 0) { ?>
                                                                <div class="dollar1"><img src="<?php echo base_url() ?>/assets/images/usd1600.png" class="img-responsive"></div>
    <?php } ?>
                                                        </a>
                                                    </div>
                                                    <div class="media-body">
                                                        <h4 class="media-heading"><a href="<?php echo base_url() ?>index.php/welcome/showvideo/<?php echo $ggvid->id; ?>"><?php echo ucwords($ggvid->videoname); ?></a></h4>
                                                        <p style="font-size:12px;"><?php echo ucwords($ggvid->video_category); ?></p>
                                                        <p style="font-size:12px;"><?php
    if ($ggvid->videoview == "") {
        echo "0";
    } else {
        echo $ggvid->videoview;
    }
    ?> Views</p>
                                                    </div>
                                                </div>
                                            </div>
<?php } ?>    



                                    </div>


                                </div>
                            </div>
                        </div>-->

                    </div> 
                </div>
            </div>
        </div>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content" style="margin:20% auto;">  
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style=" position: absolute;right: 10px;top: 10px;z-index: 100;"></button>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <?php
                                                foreach ($companydetail as $company) {
                                                    if ($company->company_logo != "") {
                                                        $cmplogo = base_url() . 'Admin/uploads/'.$company->company_logo;
                                                    } else {
                                                        $cmplogo = base_url() . "uploads/logo.png";
                                                    }
                                                    ?><?php } ?>
                                                <a href="<?php echo base_url() ?>index.php/welcome"><img src="<?php echo $cmplogo; ?>" class="img-responsive logolog"></a>
                                <h4 style="text-align:center; font-size:13px;">Sign in to continue to <span style="color:#03C;">HowClip</span></h4>

                                <div style="color:#FF0000;text-align:center;" id="signformerror"></div>



                                <div class="logwrap">
                                    <div class="profiledp"> <i class="fa fa-user" aria-hidden="true"></i></div>
                                    <form class="setup" method="post"  name="signform" id="signform">
                                        <!-- action="<?php echo base_url() ?>index.php/home/login"-->
                                        <div class="form-group">

                                            <input type="email" class="form-control login" name="email" id="emailAddress" placeholder="Email">
                                        </div>
                                        <div class="form-group">

                                            <input type="password" class="form-control login" name="password" id="pass" placeholder="Password">
                                        </div>


                                        <button type="button" class="btn loginbutton" onClick="getlogin();">Submit</button>
                                    </form>

                                </div>
                                <h3><a href="<?php echo base_url() ?>index.php/welcome/signup" style="font-size:14px;margin:10px 0px; text-align:center; text-decoration:none;display:block;">Create Account</a></h3>
                                <div class="logresponsive">
                                    <ul>
                                        <li>
                                            <a href="#">
                                                <img src="<?php echo base_url(); ?>assets/images/fblogin.jpg" id="loginBtn" class="img-responsive logimageres"><div id="response" style="display:none;"></div> </a>
                                        </li>
                                    </ul>
                                </div>
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="footmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                            <li><a href="javascript:void(0);"> Press </a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                            <li><a href="javascript:void(0);"> Creators</a></li>
                            <li><a href="javascript:void(0);"> Advertise</a></li>
                            <li><a href="javascript:void(0);"> Developers</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="minfootmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                            <li><a href="javascript:void(0);"> Privacy</a></li>
                            <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                            <li><a href="javascript:void(0);"> Send feedback</a></li>
                            <li><a href="javascript:void(0);"> Test new features</a></li>
                            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--//footer--> 
    </div>
    <!-- Classie --> 
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
    <script>
                                            var menuLeft = document.getElementById('cbp-spmenu-s1'),
                                                    showLeftPush = document.getElementById('showLeftPush'),
                                                    body = document.body;

                                            showLeftPush.onclick = function () {
                                                classie.toggle(this, 'active');
                                                classie.toggle(body, 'cbp-spmenu-push-toright');
                                                classie.toggle(menuLeft, 'cbp-spmenu-open');
                                                disableOther('showLeftPush');
                                            };

                                            function disableOther(button) {
                                                if (button !== 'showLeftPush') {
                                                    classie.toggle(showLeftPush, 'disabled');
                                                }
                                            }
    </script> 
    <!--scrolling js--> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fileupload.js"></script>
    <!--//scrolling js--> 
    <!-- Bootstrap Core JavaScript --> 


    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 

    <script>
                                            $(function () {
                                                $('#menu').metisMenu({
                                                    toggle: false // disable the auto collapse. Default: true.
                                                });
                                            });
    </script>
    <script>
$(window).load(function() {
     var vid = document.getElementById("myvideo");
     var pr = $('#auto').val();
      var ee = $('#link1').val();
            if(ee=='1'){
                if(pr=='price'){
                    
                }else{
          vid.autoplay = true;
    vid.load();
    }  
        }
    
   
});
        $(document).ready(function () {
            var ee = $('#link1').val();
            if(ee=='1'){
            alert(ee);
        }
        });
    </script>
    <script>
        function Submitlike(video, uid) {
            if (uid == "")
            {

            } else {
                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/home/insertlike',
                    type: "POST",
                    data: {'vid': video, 'user': uid
                    },
                    success: function (response)
                    {
                        var jsonobj = $.parseJSON(response);
                        $('#likes').html(jsonobj.totallike);
                        $('#dislikes').html(jsonobj.Unlike);
                    }

                });
            }
        }
    </script>

    <script>
        function Submitdislike(video, uid) {
            if (uid == "")
            {

            } else {
                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/home/insertdislike',
                    type: "POST",
                    data: {'vid': video, 'user': uid
                    },
                    success: function (response)
                    {
                        var jsonobj = $.parseJSON(response);

                        $('#likes').html(jsonobj.totallike);
                        $('#dislikes').html(jsonobj.Unlike);

                    }

                });
            }
        }
    </script>


    <script>
        function showcomments()
        {
            document.getElementById('commentdiv').style.display = "block";
        }
    </script>
    <script type="text/javascript">

        function ajaxSearch()
        {
            var input_data = $('#input-31').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
    </script>
    <script>
        $(document).on("click", function (e) {
            if (!$("#suggestions").is(e.target)) {
                $("#suggestions").hide();
            }
        });
    </script>
    <script>
        $(".show-more a").on("click", function () {
            var $this = $(this);
            var $content = $this.parent().prev("div.content");
            var linkText = $this.text().toUpperCase();

            if (linkText === "SHOW MORE") {
                linkText = "Show less";
                $content.switchClass("hideContent", "showContent", 400);
            } else {
                linkText = "Show more";
                $content.switchClass("showContent", "hideContent", 400);
            }
            ;

            $this.text(linkText);
        });
    </script>
    <script>
        $('.pop').popover().click(function () {
            setTimeout(function () {
                $('.pop').popover('hide');
            }, 2000);
        });

    </script>
    <script>
        function createplay(x, y, z)
        {
            var uname = x;
            var uvideo = y;
            var uploader = z;
            var name = document.getElementById('playname').value;
            var privacy = document.getElementById('playprivacy').value;
            var post_data = {
                'vname': name,
                'vprivacy': privacy,
                'user': x,
                'video': uvideo,
                'uploader': uploader

            };

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/createplaylist",
                data: post_data,
                success: function (data) {
                    if (data.length > 0) {
                        var res = data.split('||');
                        if (res[1] == undefined)
                        {
                            $("#videochk").css("display", "none");
                        } else
                        {
                            $("#videochk").css("display", "inline");
                            document.getElementById('videoplaysuccess').style.display = "block";
                            setTimeout(function () {
                                document.getElementById('videoplaysuccess').style.display = "none";
                            }, 2000);
                        }
                        $("#fplay").css("display", "block");


                        $('#playlistname').html(res[1]);
                        $('#playlistid').html(res[0]);
                        if (res[0] == 'Err')
                        {
                            $('#Err').html(res[0]);
                        }
                        setTimeout(function () {
                            $('#Err').fadeOut('fast');
                        }, 1000);

                    }
                }
            });

        }
        function removevideo(id, v, w)
        {

            var i = id;
            var video = v;
            var uploader = w;


            var abc = $("#" + i).prop("checked");
            if (abc)
            {
                var post_data = {
                    'vname': name,
                    'user': x,
                    'video': uvideo,
                    'uploader': uploader

                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/createplaylist",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {
                            var res = data.split('||');
                            $("#fplay").css("display", "block");
                            $('#playlistname').html(res[1]);
                            $('#playlistid').html(res[0]);
                            document.getElementById('videoplaysuccess').style.display = "block";
                            setTimeout(function () {
                                document.getElementById('videoplaysuccess').style.display = "none";
                            }, 2000);


                        }
                    }
                });
            } else
            {
                var name = document.getElementById('playlistid').innerText;
                var post_data = {
                    'vname': video,
                    'playlist': name,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/deletefromplay",
                    data: post_data,
                    success: function (data) {
                        document.getElementById('videoplayfail').style.display = "block";
                        setTimeout(function () {
                            document.getElementById('videoplayfail').style.display = "none";
                        }, 2000);

                        if (data.length > 0) {


                        }
                    }
                });

            }

        }
    </script>
    <script>
        function addremovevideo(id, v, w, x, g)
        {

            var abf = $("#" + id).prop("checked");

            var i = id;
            var video = v;
            var uploader = w;
            var playlist = x;
            var listname = g;

            if (abf)
            {

                var post_data = {
                    'playid': playlist,
                    'video': video,
                    'uploader': uploader,
                    'list': listname,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/addplaylist",
                    data: post_data,
                    success: function (data) {


                        document.getElementById('videoplaysuccess').style.display = "block";
                        setTimeout(function () {
                            document.getElementById('videoplaysuccess').style.display = "none";
                        }, 2000);


                    }
                });
            } else
            {

                var post_data = {
                    'vname': video,
                    'playlist': listname,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/deletefromplaylist",
                    data: post_data,
                    success: function (data) {
                        document.getElementById('videoplayfail').style.display = "block";
                        setTimeout(function () {
                            document.getElementById('videoplayfail').style.display = "none";
                        }, 2000);
                        if (data.length > 0) {

                        }
                    }
                });

            }



            console.log(i);


        }
    </script>
    <script>
        function generateiframe(x)
        {
            var post_data = {
                'video': x,
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/welcome/embed",
                data: post_data,
                success: function (data) {
                    if (data.length > 0) {
                        var res = data.split('||');
                        var ab = '<iframe width="560" height="315" src="' + res[0] + '" frameborder="0" allowfullscreen></iframe>';
                        $('#embedcode').text(ab);
                        $('li.dropdown.mega-dropdown').removeClass('close');
                        $('ul.iframe *').click(function (e) {
                            e.stopPropagation();
                        });
                    }
                }
            });
        }

    </script>

    <script>

        function getlogin()
        {

            var email = document.getElementById("emailAddress").value;
            var password = document.getElementById("pass").value;

            if (email == "")
            {
                document.getElementById("signformerror").innerHTML = "Enter the Email Address";
                return false;
            } else if (password == "")
            {
                document.getElementById("signformerror").innerHTML = "Enter the Password";
                return false;
            } else
            {

                var sign_data = {
                    'email': email,
                    'password': password,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/welcome/login",
                    data: sign_data,
                    success: function (data) {
                        console.log(data);
                        if (data.length > 0) {
                            if (data == 'success')
                            {
                                var videoid = document.getElementById('vid').value;
                                window.location = "<?php echo base_url() ?>index.php/welcome/showvideo/" + videoid;
                            } else
                            {
                                document.getElementById("signformerror").innerHTML = data;
                            }

                        }
                    }
                });


            }


        }
        $('#pass').keypress(function (e) {
            if (e.which == '13') {
                getlogin();
            }
        });
    </script>
    <script>
        function changeprice()
        {
            document.getElementById('pricebefore').style.display = "none";
            document.getElementById('priceafter').style.display = "block";
        }

        function makepayment(a, b, c)
        {

            var price = a;
            var video = b;
            var uploader = c;
            var post_data = {
                'price': price,
                'videoid': video,
                'uploaderid': uploader,
            };

            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/payment",
                data: post_data,
                success: function (data) {
                    //console.log(data);
                    //$('#makepayment').submit();
                    if (data.length > 0) {
                        var aa = data;
                        document.getElementById('subpay').value = "<?php echo base_url() ?>index.php/home/success/" + aa;
                        document.getElementById('cancelpay').value = "<?php echo base_url() ?>index.php/home/cancel/" + aa;
                        $('#makepayment').submit();
                        //vvalue='<?php echo base_url() . 'index.php/home/cancel/' . $ad['order_id'] ?>' '			 
                    }
                }
            });

            //  $('#makepayment').submit();

        }


    </script>
    <script>
function dynamicsubscribe(x){
    
    var aa = x;
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/home/subscribe',
                type: "POST",
                data: {'uploader': aa, 
                },
                success: function (response)
                {
                  
                  $("#subhref1").css("display", "none");
                  $("#subhref4").css("display", "none");
                  $('#subhref3').css("display", "block");
        // window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                }

            });
    
}
function dynamicunsubscribe(x){
    var aa = x;
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/home/unsubscribe',
                type: "POST",
                data: {'uploader': aa, 
                },
                success: function (response)
                {
                    $("#subhref2").css("display", "none");
                    $("#subhref3").css("display", "none");
                    
                  $('#subhref4').css("display", "block");
                   // window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                }

            });
    
}
        function canpay()
        {
            document.getElementById('pricebefore').style.display = "block";
            document.getElementById('priceafter').style.display = "none";
        }
        function oooplog()
        {
            $('#myModal').modal('show');
            return false;
        }
    </script>
    <script type="text/javascript">
        function aaa(x, y) {
            var email = x;
            var name = y;
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/welcome/insertfbdata',
                type: "POST",
                data: {'email': email, 'user': name
                },
                success: function (response)
                {
                    window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                }

            });
        }
        function SubmitUserData(email) {
            alert(email);
            return false;
            $.ajax({
                url: '<?php echo base_url(); ?>index.php/home/insertfbdata',
                type: "POST",
                data: {'email': email
                },
                success: function (response)
                {
                    window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                }

            });
        }

        function getUserData() {
            FB.api('/me?fields=id,first_name,last_name,email,link,gender,locale,picture', function (response) {
                document.getElementById('response').innerHTML = 'Hello ' + response.id + '<br>' + response.email + '<br>' + response.first_name + ' ' + response.last_name + '<br>' + response.gender;
                var uname = response.first_name;
                var email = response.email;


                aaa(email, uname);
            });
        }


        window.fbAsyncInit = function () {
            FB.init({
                appId: '1884735515112965',
                xfbml: true,
                version: 'v2.6'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        document.getElementById('loginBtn').addEventListener('click', function () {

            FB.login(function (response) {
                if (response.authResponse) {
                    //user just authorized your app
                    document.getElementById('loginBtn').style.display = 'none';
                    getUserData();

                }
            }, {scope: 'email,public_profile', return_scopes: true});
        }, false);

  $('#chkval').click(function (e) {
                e.preventDefault();
                var input = document.getElementById("input-31").value;
                if (input == '') {
                    alert('Please Type Text For Search!');
                     e.preventDefault();
                }
                else{
                    document.getElementById('my_form').submit();
                }
            });
    </script>

</body>
</html>