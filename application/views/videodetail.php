<!DOCTYPE HTML>
<html>
    <head>
        <title>HowClip</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sidebar-menu.css">
        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>

        <script>
            new WOW().init();
        </script>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.min.js"></script>
        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <script type="text/javascript">
            $(document).ready(function () {
                document.getElementById("dropdownMenu10").disabled = true;
            });
        </script>

        <script>
            function getplay()
            {

                $("#play").css("display", "none");
                var aa = $(".playform").css("display", "block");

                if (aa.length > 0)
                {

                    setTimeout(function () {
                        $('#dropdownMenu10').click();
                    }, 01);
                    setTimeout(function () {
                        $('#playname').focus();
                    }, 02);

                    $('ul.nojs *').click(function (e) {

                        e.stopPropagation();
                    });



                }
            }
        </script>
        <style>

            input[type="checkbox"] {
                display:none;
            }

            input[type="checkbox"] + label {
                color:#f2f2f2;
                font-family:Arial, sans-serif;
                font-size:14px;
                float:left;
            }

            input[type="checkbox"] + label span {
                display:inline-block;
                width:19px;
                height:19px;
                margin:-1px 4px 0 0;
                vertical-align:middle;
                background:url(<?php echo base_url(); ?>assets/images/check_radio_sheet.png) left top no-repeat;
                cursor:pointer;
            }

            input[type="checkbox"]:checked + label span {
                background:url(<?php echo base_url(); ?>assets/images/check_radio_sheet.png) -19px top no-repeat;
            }
            #easyPaginate {width:300px;}
            #easyPaginate img {display:block;margin-bottom:10px;}
            .easyPaginateNav a {padding:5px;}
            .easyPaginateNav a.current {font-weight:bold;text-decoration:underline;}
        </style>

        <!--//Metis Menu -->
    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 
            <!--left-fixed -navigation-->

            <!--left-fixed -navigation--> 
            <!-- header-starts -->
            <div class="sticky-header header-section ">
                <div class="header-left"> 
                    <!--toggle button start-->
                 <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->

                    <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu8" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu8">
                            <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id') ?>">My Videos</a></li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>"> History </a> </li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"> My Channel </a> </li>
                            <li class="divider" role="seperator"></li>
                            <li> <a href="#">SUBSCRIPTIONS </a> </li>
                            <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"> <?php echo $uploader->username; ?> </a> </li>
                            <?php } ?>

                        </ul>
                    </div>
                    <!--toggle button end--> 
                    <!--logo -->
                    <?php
                    foreach ($companydetail as $company) {
                        if ($company->company_logo != "") {
                            $cmplogo = base_url()."Admin/uploads/$company->company_logo";
                        } else {
                            $cmplogo = base_url()."uploads/logo.png";
                        }
                        ?>
                        <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">

                                <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                            </a> </div>
                    <?php } ?>
                    <!--//logo--> 
                    <!--search-box-->

                    <div class="search-box">
                        <form class="input" method="post">
                            <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearchdata();" placeholder="Search..." type="search" autocomplete="off" />
                            <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                                </svg> </label>
                        </form>

                        <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
                            <div id="autoSuggestionsList"></div>
                        </div>
                    </div>
                    <!--//end-search-box-->
                    <div class="clearfix"> </div>
                </div>
                <div class="header-right">
                    <div class="profile_details_left"><!--notifications of menu start -->
                        <ul class="nofitications-dropdown">
                            <li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/uploadpage" style="color:#333;" > <img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > </a>

                            </li>
                            <?php
                            foreach ($userDetail as $user) {
                                $username = $user->username;
                                $userimg = $user->userLogo;
                                if ($userimg == "") {
                                    $userimg = "user.png";
                                }
                            }
                            ?>
                        <!--<li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url(); ?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
                            <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                <ul class="dropdown-menu" style="left:initial; right:0;">
                                    <li>
                                        <div class="notification_header">
                                            <h3><?php echo $username; ?></h3>
                                        </div>
                                    </li>
                                    <li><a href="#">
                                            <div class="notification_desc">
                                                <p><?php echo $this->session->userdata('email'); ?></p>

                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                    </li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"> </div>
                    </div>


                </div>


            </div>
            <div class="uploadwrap">
                <div class="container-fluid">
                    <div class="row" style="padding:0px;">
                        <div class="col-md-2" style="padding-right:0px;">
                            <ul class="sidebar-menu">
                                <li class="sidebar-header">HowClip</li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/dash">
                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-files-o"></i>
                                        <span>Video Manager</span>
                                    </a>
                                    <ul class="sidebar-submenu" >
                                        <li><a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Videos</a></li>
                                        <li><a href="<?php echo base_url() ?>index.php/home/showplaylist/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Playlists</a></li>

                                    </ul>
                                </li>
                                  <li>
        <a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>">
          <i class="fa fa-pie-chart"></i>
          <span>My Channel</span>
         
        </a>
     
      </li>
      
     

                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/userupdate/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>User Profile</span>

                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/changePass/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>Change Password</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/myorders/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>My Orders</span>

                                    </a>

                                </li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="blankpage">
                                <div>
                                    <h4>Video &nbsp;&nbsp; <span class="badge"></span></h4>
                                    <div id="selectvideo" style="color:#FF0000;display:none;">Please Select Video &nbsp;&nbsp; <span class="badge"></span></div>
                                    <h4 id="videoplaysuccess" style="color:green;display:none;">Video Added Successfully Into the Playlist  <span class="badge"></span></h4>
                                
                                </div>
                                <div class="searchedit">
                                    <form>
                                        <input class="sb-search-input input__field--madoka" name="search_data" id="input-32"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off"  />
                                        <label for="input-32"> </label>
                                    </form>
                                    <div id="suggestionsbox" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
                                        <div id="autoSuggestionsListbox"></div>
                                    </div>  
                                </div> 
                                <div style="margin-top:20px;">
                                    <input type="checkbox" id="c1" name="cc" />
                                    <label for="c1"><span></span>  </label>
                                    <div style="display:inline-block;"> 


                                        <div class="dropdown" style="display:inline-block;">
                                            <button class="btn myedit dropdown-toggle" type="button" id="select_all" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                Action
                                                <span class="caret"></span>
                                            </button>
                                            <input type="hidden" id="userid" value="<?php echo $this->session->userdata('id'); ?>">  
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1" style="min-width: 106px !important;">
                                                <li><p style="padding:3px;"> Privacy</p></li>

                                                <li role="separator" class="divider"></li>
                                                <li onClick="privacy('public');"><a href="#">Public</a></li>

                                                <li onClick="privacy('unlisted');"><a href="#">Unlisted</a></li>
                                                <li onClick="privacy('private');"><a href="#">Private</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li onClick="privacy('delete');"><a href="#">Delete</a></li>
                                            </ul>



                                        </div>
                                        <div class="dropdown" style="display:inline-block;">
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu10" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="padding:2px 5px;">
                                                Add to
                                            </button>
                                            <ul class="dropdown-menu nojs" aria-labelledby="dropdownMenu10">
                                                <li class="search-box">
                                                    <form class="input">
                                                        <input class="sb-search-input input__field--madoka" name="search_data" id="searchplay"  onkeyup="ajaxSearchdataplay();" placeholder="Search..." type="search" autocomplete="off" />
                                                        <label class="input__label" for="searchplay"> 
                                                        </label>
                                                    </form>

                                                    <div id="suggestionsplay">
                                                        <div id="autoSuggestionsListplay"></div>
                                                    </div>
                                                </li>

                                                <li class="pp"><span id="Err"></span></li>

                                                <li class="pp" id="fplay" style="display:none"><a><input type="checkbox"  id="videochk" ><span id="playlistname" style="font-size:14px;"></span><span id="playlistid" style="display:none"></span></a></li>
                                                <?php
                                                $i = 0;
                                                foreach ($playlist as $play) {
                                                    ?>
                                                    <li class="pp"><?php $playarr[] = $play->video_id; ?><a href="<?php echo base_url() ?>index.php/home/playvideo/<?php echo $play->playlist_name; ?>/<?php echo $this->session->userdata('id') ?>"><input type="checkbox" style="display:inline;" name="videochk[]" class="case2"  id="<?php echo $play->id; ?>" onChange="insertplay(this.id);"><?php echo $play->playlist_name; ?></a></li><?php
                                                    $i++;
                                                }
                                                ?>

                                                <li role="separator" class="divider pp"></li>
                                                <li class="pp" id="play" onClick="getplay();"><a href="javascript:void;">Create a new Playlist</a></li>

                                                <li class="playform pp" style="display:none;"><a href="javascript:void;"><form id="myForm"><input name="playname" id="playname" type="text" style="width: 90%;height:25px;margin-left:10px;"></a></li>
                                                            <li class="playform pp" style="display:none;"><span style="width:55%;display:inline;padding:0px;"><select name="playprivacy" id="playprivacy" style="width:50%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;margin-left:11px;"><option value="public">Public</option><option value="unlisted">Unlisted</option><option value="private">Private</option></span><span style="width:40%;display:inline;padding:0px;"><input type="button" value="Create" onClick="createplay();"  style="width:37%;display:inline;height:25px;margin-top:10px;margin-bottom:10px;margin-left:5px;background-color: darkslateblue;
                                                                                                                                                                                                        color: white;"/></form></span></li> 
                                            </ul>
                                        </div>


                                    </div>

                                    <div class="viewmen">
                                        <div class="editlike">
                                            <?php
                                            foreach ($videoDetail as $video) {
                                                $vid = $video->id;
                                            }
                                            ?>
                                            <ul>
                                                <li>View </li>
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Newest <span class="caret"></span></a>
                                                    <ul class="dropdown-menu" style="left:initial; right:0; ">
                                                        <li><a href="<?php echo base_url() ?>index.php/home/videoByPrivacy/public">Public</a></li>
                                                        <li><a href="<?php echo base_url() ?>index.php/home/videoByPrivacy/unlisted">Unlisted</a></li>
                                                        <li><a href="<?php echo base_url() ?>index.php/home/videoByPrivacy/private">Private</a></li>

                                                    </ul>
                                                </li>

                                            </ul>

                                        </div>

                                    </div>

                                </div>

                            </div>





                            <?php
                            $i = 0;
                            if ($videoDetail != "")
                                foreach ($videoDetail as $video) {
                                    if ($video->noresult == "") {
                                        ?>     
                                        <div class="blankpage">
                                            <div class="row">

                                                <div class="col-md-8" >

                                                    <input type="checkbox" id="c2.<?php echo $i; ?>" name="videochk[]" class="case" value="<?php echo $video->id; ?>"/>
                                                    <label for="c2.<?php echo $i; ?>"><span></span>  </label>
                                                    <?php
                                                    if (getimagesize('uploads/images/' . $video->videothumb) !== false) {
                                                        $link = base_url()."uploads/images/" . $video->videothumb;
                                                    } else {
                                                        $link = base_url()."uploads/images/download.jpg";
                                                    }
                                                    ?>
                                                    <div class="imageedit"><a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $video->id; ?>"> <img src="<?php echo $link; ?>" class="img-responsive videdit"></a>
                                                        <?php if ($video->price > 0) { ?>
                                                            <a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $video->id; ?>"><div class="dollar1" ><img src="<?php echo base_url() ?>/assets/images/usd1600.png" class="img-responsive"></div></a>

            <?php } ?>
                                                    </div>
                                                    <div style="display:inline-block;width: 55%;
                                                         max-height: 200px;">
                                                         <?php
                                                         $vname = $video->videoname;
                                                         $length = strlen($video->videoname);
                                                         if ($length < 20) {
                                                             $v = $vname;
                                                         } else {
                                                             $v = substr($vname, 0, 20);
                                                             $v = $v . "...";
                                                         }
                                                         ?>

                                                        <div class="edithead"><a href="<?php echo base_url() ?>index.php/home/showvideo/<?php echo $video->id; ?>"><?php echo ucwords($v); ?></a></div><br>
                                                        <div class="edittime" ><?php echo $video->Date; ?></div>
                                                        <div class="dropdown">
                                                            <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                                Edit
                                                                <span class="caret"></span>
                                                            </button>
                                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                                <li><a href="<?php echo base_url() ?>index.php/home/EditvideoByUser/<?php echo $video->id; ?>">Info & Settings</a></li>
                                                                <li><a href="<?php echo base_url() ?>index.php/home/Editvideocreateclips/<?php echo $video->id; ?>">Create Clips</a></li>
                                                               <!-- <li><a href="<?php echo base_url() ?>index.php/home/downloadvideo?id=<?php echo $video->id; ?>">Download mp4</a></li>-->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="likolay">
                                                        <div class="view" data-toggle="tooltip" data-placement="bottom" title="Public"><a href="#"><i class="fa fa-globe" aria-hidden="true"></i></a><?php
                                                            if ($video->view == "") {
                                                                echo "0";
                                                            } else {
                                                                echo $video->view;
                                                            }
                                                            ?></div>
                                                        <div class="editlike">
                                                            <ul>
                                                                <li data-toggle="tooltip" data-placement="bottom" title="All Comments"><a href="#"><i class="fa fa-comment" aria-hidden="true"></i> </a> <?php
                                                                    if ($video->comment == "") {
                                                                        echo "0";
                                                                    } else {
                                                                        echo $video->comment;
                                                                    }
                                                                    ?></li>
                                                                <li data-toggle="tooltip" data-placement="bottom" title="Likes"><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> </a><?php
                                                                    if ($video->like == "") {
                                                                        echo "0";
                                                                    } else {
                                                                        echo $video->like;
                                                                    }
                                                                    ?></li>
                                                                <li data-toggle="tooltip" data-placement="bottom" title="Dislikes"><a href="#"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a> <?php
                                                                    if ($video->Unlike == "") {
                                                                        echo "0";
                                                                    } else {
                                                                        echo $video->Unlike;
                                                                    }
                                                                    ?></li>
                                                            </ul>
                                                        </div>

                                                    </div>
                                                    <div class="viewmen">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                    }
                                }
                            ?>

                            <div style="width:auto; float: right; padding:10px; background-color:#eee" align="bottom">
                                <b>Page : 
                                    <?php
                                    $userid = $this->session->userdata('id');
                                    $cnd = "WHERE `userId` = $userid order by `id` DESC";
                                    $sql = mysql_query("select * FROM video $cnd  ");
                                    $numrecords = mysql_num_rows($sql);
                                    for ($i = 0; $i <= ($numrecords / 10); $i++) {
                                        echo "<a href='?page=" . ($i) . "'>" . ($i + 1) . " </a>";
                                    }
                                    ?>
                                </b>

                            </div>



                            <!--<div class="blankpage">
                          
                             <input type="checkbox" id="c3" name="cc" />
                                      <label for="c3"><span></span>  </label>
                                     <div class="imageedit"> <img src="images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                  <div class="edithead">Video Name</div>
                                  <div class="edittime">14/08/2017</div>
                                  <div class="dropdown">
                            <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                             Edit
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#">Separated link</a></li>
                            </ul>
                          </div>
                                  </div>
                                     
                                     <div class="viewmen">
                                <div class="draft">Draft</div>
                                     <button class="btn publishedit" type="submit">Publish</button>
                                     
                                     </div>
                          
                            </div>-->
                            <!--<div class="blankpage">
                          
                             <input type="checkbox" id="c4" name="cc" />
                                      <label for="c4"><span></span>  </label>
                                     <div class="imageedit"> <img src="images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                  <div class="edithead">Video Name</div>
                                  <div class="edittime">14/08/2017</div>
                                  <div class="dropdown">
                            <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                             Edit
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#">Separated link</a></li>
                            </ul>
                          </div>
                                  </div>
                                     
                                     <div class="likolay">
                                <div class="view" data-toggle="tooltip" data-placement="bottom" title="Public"><a href="#"><i class="fa fa-globe" aria-hidden="true"></i></a></div>
                                     <div class="editlike">
                                     <ul>
                                     <li data-toggle="tooltip" data-placement="bottom" title="All Comments"><a href="#"><i class="fa fa-comment" aria-hidden="true"></i> </a> 0</li>
                                     <li data-toggle="tooltip" data-placement="bottom" title="Likes"><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> </a> 0</li>
                                     <li data-toggle="tooltip" data-placement="bottom" title="Dislikes"><a href="#"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a> 0</li>
                                     </ul>
                                     </div>
                                     
                                     </div>
                          
                            </div>-->
                            <!--<div class="blankpage">
                          
                             <input type="checkbox" id="c5" name="cc" />
                                      <label for="c5"><span></span>  </label>
                                     <div class="imageedit"> <img src="images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                  <div class="edithead">Video Name</div>
                                  <div class="edittime">14/08/2017</div>
                                  <div class="dropdown">
                            <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                             Edit
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#">Separated link</a></li>
                            </ul>
                          </div>
                                  </div>
                                     
                                     <div class="viewmen">
                                <div class="draft">Draft</div>
                                     <button class="btn publishedit" type="submit">Publish</button>
                                     
                                     </div>
                          
                            </div>-->
                            <!--<div class="blankpage">
                          
                             <input type="checkbox" id="c6" name="cc" />
                                      <label for="c6"><span></span>  </label>
                                     <div class="imageedit"> <img src="images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                  <div class="edithead">Video Name</div>
                                  <div class="edittime">14/08/2017</div>
                                  <div class="dropdown">
                            <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                             Edit
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#">Separated link</a></li>
                            </ul>
                          </div>
                                  </div>
                                     
                                     <div class="viewmen">
                                <div class="draft">Draft</div>
                                     <button class="btn publishedit" type="submit">Publish</button>
                                     
                                     </div>
                          
                            </div>-->
                            <!--<div class="blankpage">
                          
                             <input type="checkbox" id="c7" name="cc" />
                                      <label for="c7"><span></span>  </label>
                                     <div class="imageedit"> <img src="images/36569259-good-wallpapers.jpg" class="img-responsive videdit">
                                  <div class="edithead">Video Name</div>
                                  <div class="edittime">14/08/2017</div>
                                  <div class="dropdown">
                            <button class="btn myedit dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                             Edit
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                              <li><a href="#">Action</a></li>
                              <li><a href="#">Another action</a></li>
                              <li><a href="#">Something else here</a></li>
                              <li role="separator" class="divider"></li>
                              <li><a href="#">Separated link</a></li>
                            </ul>
                          </div>
                                  </div>
                                     
                                     <div class="likolay">
                                <div class="view" data-toggle="tooltip" data-placement="bottom" title="Public"><a href="#"><i class="fa fa-globe" aria-hidden="true"></i></a></div>
                                     <div class="editlike">
                                     <ul>
                                     <li data-toggle="tooltip" data-placement="bottom" title="All Comments"><a href="#"><i class="fa fa-comment" aria-hidden="true"></i> </a> 0</li>
                                     <li data-toggle="tooltip" data-placement="bottom" title="Likes"><a href="#"><i class="fa fa-thumbs-up" aria-hidden="true"></i> </a> 0</li>
                                     <li data-toggle="tooltip" data-placement="bottom" title="Dislikes"><a href="#"><i class="fa fa-thumbs-down" aria-hidden="true"></i></a> 0</li>
                                     </ul>
                                     </div>
                                     
                                     </div>
                          
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//footer--> 
        </div>
        <!-- Classie --> 

        <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 

        <!--scrolling js--> 
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/fileupload.js"></script>
        <!--//scrolling js--> 
        <!-- Bootstrap Core JavaScript --> 
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 

        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

        <script type="text/javascript">

                                                                var _gaq = _gaq || [];
                                                                _gaq.push(['_setAccount', 'UA-36251023-1']);
                                                                _gaq.push(['_setDomainName', 'jqueryscript.net']);
                                                                _gaq.push(['_trackPageview']);

                                                                (function () {
                                                                    var ga = document.createElement('script');
                                                                    ga.type = 'text/javascript';
                                                                    ga.async = true;
                                                                    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                                                                    var s = document.getElementsByTagName('script')[0];
                                                                    s.parentNode.insertBefore(ga, s);
                                                                })();

        </script>

        <script>
            var menuLeft = document.getElementById('cbp-spmenu-s1'),
                    showLeftPush = document.getElementById('showLeftPush'),
                    body = document.body;

            showLeftPush.onclick = function () {
                classie.toggle(this, 'active');
                classie.toggle(body, 'cbp-spmenu-push-toright');
                classie.toggle(menuLeft, 'cbp-spmenu-open');
                disableOther('showLeftPush');
            };

            function disableOther(button) {
                if (button !== 'showLeftPush') {
                    classie.toggle(showLeftPush, 'disabled');
                }
            }
        </script> 
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 
        <script>
            $(function () {
                $('#menu').metisMenu({
                    toggle: false // disable the auto collapse. Default: true.
                });
            });
        </script>
        <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/sidebar-menu.js"></script>
        <script>
            $.sidebarMenu($('.sidebar-menu'))
        </script>


        <script>
            $("#c1").change(function () {

                $("input:checkbox").prop('checked', $(this).prop("checked"));
            });





            function privacy(x)
            {
                var atLeastOneIsChecked = '';
                $(".case").each(function () {
                    if ($(".case").is(':checked')) {
                        //  alert('yes');
                        atLeastOneIsChecked = true;

                        return false;
                    }

                });



                if (($("#c1").prop("checked") == true) || (atLeastOneIsChecked == true))
                {

                    var videodata = [];
                    var uid = document.getElementById('userid').value;

                    var pri = x;
                    $(".case").each(function () {
                        var aa = this.checked;
                        var vid = this.value;

                        if (aa)
                        {
                            var abc = vid + '|' + pri;
                            videodata.push(abc
                                    );
                        }

                    });
                    $.ajax({
                        url: '<?php echo base_url(); ?>index.php/home/setprivacy',
                        type: "POST",
                        data: {video: videodata
                        },
                        success: function (response)
                        {

                            window.location.href = '<?php echo base_url(); ?>index.php/home/videoDetail/' + uid;
                        }

                    });
                } else
                {

                    document.getElementById('selectvideo').style.display = 'block';
                    setTimeout(function () {
                        $('#selectvideo').fadeOut('fast');
                    }, 3000);
                }
                if (atLeastOneIsChecked == "undefined")
                {
                    document.getElementById('selectvideo').style.display = 'block';
                    setTimeout(function () {
                        $('#selectvideo').fadeOut('fast');
                    }, 3000);
                }


            }
        </script>
        <script type="text/javascript">
            function ajaxSearchdata()
            {

                var input_data = $('#input-31').val();

                if (input_data.length === 0)
                {
                    $('#suggestions').hide();
                } else
                {

                    var post_data = {
                        'search_data': input_data,
                        '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                    };

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/home/search",
                        data: post_data,
                        success: function (data) {
                            // return success
                            if (data.length > 0) {
                                $('#suggestions').show();
                                $('#autoSuggestionsList').addClass('auto_list');
                                $('#autoSuggestionsList').html(data);
                            }
                        }
                    });


                }

            }
        </script>
        <script type="text/javascript">

            function ajaxSearch()
            {

                var input_data = $('#input-32').val();

                if (input_data.length === 0)
                {
                    $('#suggestionsbox').hide();
                } else
                {

                    var post_data = {
                        'search_data': input_data,
                        '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                    };

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/home/search",
                        data: post_data,
                        success: function (data) {
                            // return success
                            if (data.length > 0) {
                                $('#suggestionsbox').show();
                                $('#autoSuggestionsListbox').addClass('auto_list');
                                $('#autoSuggestionsListbox').html(data);
                            }
                        }
                    });


                }

            }
        </script>
        <script>
            $(document).on("click", function (e) {
                if (!$("#suggestions").is(e.target)) {
                    $("#suggestions").hide();
                }
            });
        </script>
        <script>
            $(document).on("click", function (e) {
                if (!$("#suggestionsbox").is(e.target)) {
                    $("#suggestionsbox").hide();
                }
            });
        </script>

        <script>
            $(".case").change(function () {

                var atLeastOneIsChecked = false;
                $('.case').each(function () {
                    if ($(this).is(':checked')) {
                        atLeastOneIsChecked = true;
                        // Stop .each from processing any more items
                        return false;
                    }
                });

                if (atLeastOneIsChecked == true)
                {
                    document.getElementById("dropdownMenu10").disabled = false;
                }
                if (atLeastOneIsChecked == false)
                {
                    document.getElementById("dropdownMenu10").disabled = true;
                }

            });

            function createplay()
            {

                var playlist = document.getElementById('playname').value;
                var privacy = document.getElementById('playprivacy').value;
                var videodata = [];
                var post_data = {
                    'list': playlist,
                    'privacy': privacy,
                };
                $(".case").each(function () {
                    var aa = this.checked;
                    var vid = this.value;

                    if (aa == true)
                    {

                        var video = vid;

                        videodata.push(video);

                    } else
                    {
                        document.getElementById("dropdownMenu10").disabled = true;
                    }


                });

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/createplaylistdash",
                    data: {video: videodata,
                        data: post_data,
                    },
                    success: function (data) {
                       

                        if (data.length > 0) {
                            var res = data.split('||');
                            //console.log(res[0]);
                            
                            if (res[1] == undefined)
                            {
                                $("#videochk").css("display", "none");
                            } else
                            {
                                //alert('Videos Added Successfully');
                                document.getElementById('videoplaysuccess').style.display = "block";
                                setTimeout(function () {
                                    document.getElementById('videoplaysuccess').style.display = "none";
                                }, 2000);
                                $("#videochk").css("display", "inline");
                            }
                            $("#fplay").css("display", "block");


                            $('#playlistname').html(res[1]);
                            $('#playlistid').html(res[0]);
                            if (res[1] == '')
                            {
                                $('#Err').html(res[0]);

                                setTimeout(function () {
                                    $('#Err').fadeOut('fast');
                                }, 1000);
                            }

                        }
                    }
                });


            }

            function insertplay(x)
            {

                var videodata = [];

                var pri = x;
                $(".case").each(function () {
                    var aa = this.checked;
                    var vid = this.value;
                    //alert(aa);
                    if (aa)
                    {
                        var abc = vid + '|' + pri;
                        videodata.push(abc
                                );
                    }

                });

                $.ajax({
                    url: '<?php echo base_url(); ?>index.php/home/insertplaydata',
                    type: "POST",
                    data: {video: videodata
                    },
                    success: function (response)
                    {
                         document.getElementById('videoplaysuccess').style.display = "block";
                                setTimeout(function () {
                                    document.getElementById('videoplaysuccess').style.display = "none";
                                }, 2000);
                        //alert(response);
                        //console.log(response);return false;
                        //window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                    }

                });

            }
        </script>
        <script type="text/javascript">
            function ajaxSearchdataplay()
            {

                var input_data = $('#searchplay').val();

                if (input_data.length === 0)
                {
                    $('#suggestionsplay').hide();
                } else
                {

                    var post_data = {
                        'search_data': input_data,
                    };

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/home/searchplay",
                        data: post_data,
                        success: function (data) {
                           
                            if (data.length > 0) {
                                $(".pp").empty();
                                $('#suggestionsplay').show();
                                $('#autoSuggestionsListplay').addClass('auto_list');
                                $('#autoSuggestionsListplay').html(data);
                            }
                        }
                    });


                }

            }
        </script>
        <script>
            $("#c1").change(function () {

                if ($("#c1").prop("checked") == true)
                {
                    document.getElementById("dropdownMenu10").disabled = false;
                    $('.case2:checkbox:enabled').prop('checked', false);

                } else
                {
                    document.getElementById("dropdownMenu10").disabled = true;

                }

            });
        </script>
        


    </body>
</html>