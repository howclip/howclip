<script type="text/javascript">





	function showAjaxModal(url,param2)
	{
		
		if(param2)
		{
			$('.hdmt').html(param2);	
		}
		// SHOWING AJAX PRELOADER IMAGE
		$('#modal_ajax .modal-body').html('<div style="text-align:center;"><img src="<?php echo base_url();?>assets/img/loaders/3.gif" /></div>');
		// LOADING THE AJAX MODAL
		$('#modal_ajax').modal('show', {backdrop: 'static'});
		
		// SHOW AJAX RESPONSE ON REQUEST SUCCESS 
		$.ajax({
			url: url,
			success: function(response)
			{
				jQuery('#modal_ajax .modal-body').html(response);
				if($('#fixtime').prop( "checked" ))
				{
					$('input[name="time"]:radio:first').click();
				}
				if($('#varytime').prop( "checked" ))
				{
					$( 'input[name="time"]' ).click();	
				}
			}
		});
	}
	
	function add_masters(url,param2)
	{
		if(param2)
		{
			$('.hdmt').html(param2);	
		}
		// SHOWING AJAX PRELOADER IMAGE
		$('#modal_ajax .modal-body').html('<div style="text-align:center;"><img src="<?php echo base_url();?>assets/img/loaders/3.gif" /></div>');
		// LOADING THE AJAX MODAL
		$('#modal_ajax').modal('show', {backdrop: 'static'});
		// SHOW AJAX RESPONSE ON REQUEST SUCCESS 
		$.ajax({
			url: url,
			success: function(response)
			{
				jQuery('#modal_ajax .modal-body').html(response);
				var URL= $('#modal_ajax').find('form').attr('action');
				var ID= $('#modal_ajax').find('form').attr('id');
				$('#modal_ajax').find('input[type="submit"]:not(.cancel), button').removeAttr('type').attr('type','button').on('click',function(){
				//alert(ID);
				var formData=jQuery('#'+ID).serializeArray();
				var arr=[];
				console.log(formData);
				$.each(formData, function( index, value ) {
  					$.each(value, function( index1, value1 ) {
  					 arr.push(value1 + ":" + $('input[name='+value1+']').val());
					});
				});
				//console.log(arr);
				$.ajax({
				url: URL,
				type: 'post',
				data: {arr},
				success: function(data)
				{
					//bootbox.alert('Added Field');
				}
				});
				});
			    
				

			}
		});
	}

</script>			
            <!-- SAMPLE BOX CONFIGURATION MODAL FORM-->
			<div class="modal fade" id="modal_ajax" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h4 class="modal-title hdmt">Modal Box</h4>
					</div>
					<div class="modal-body">
					  
                      
					</div>
					<div class="modal-footer">
					  <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
					  <!--<button type="button" class="btn btn-primary">Save changes</button>-->
					</div>
				  </div>
				</div>
			  </div>
			<!-- /SAMPLE BOX CONFIGURATION MODAL FORM-->
<script type="text/javascript">
	function confirm_modal(delete_url)
	{			
		$('#modal_delete').modal('show', {backdrop: 'static'});
		document.getElementById('delete_link').setAttribute('href' , delete_url);		
	}
	

	<!-- bootbox_confirm -->
	function confirm_delete(url,id)
	{
		
		
		bootbox.confirm("Are you sure You want to delete", function(result) {
 			if(result)
			{
				
				
				$.ajax({
					type : 'post',
					 url : url,
					
				       
					success: function()
					 { 
					  module_title =  $('#'+id).attr('module_title');
						$('#'+id).animate({
								'padding'    : "0px",
								'margin-left':'-10px',
								'font-size': "0px"
							  }, 500, function() {									
								  $('#'+id).remove();
								  setTimeout(function () {
									 
								var unique_id = $.gritter.add({
									title: module_title+' Successfully Deleted',
									image: '<?php echo base_url();?>assets/img/gritter/cloud.png',
									sticky: true,
									time: '',
									class_name: 'my-sticky-class'
								});
								setTimeout(function () {
									$.gritter.remove(unique_id, {
										fade: true,
										speed: 'slow'
									});
								}, 6000);
							}, 300);
															  

							  });
						
					 }
					
					});
			}
		}); 
	}
	
	function confirm_status(url,id)
	{
		var aa = $('#cc').val();
		$('#modal_changestatus').modal('show', {backdrop: 'static'});
		
		
		if(id == 'multi' ) { 
		  
			document.getElementById('update_link').setAttribute('href' , url.concat(aa));
			
		}
		
		else {
				
			document.getElementById('update_link').setAttribute('href' , url);
		}
	}
	
</script>
			
            <!-- delete modal-->
       		<div class="modal fade" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h4 class="modal-title">Are you sure about this ?</h4>
					</div>
					<!--<div class="modal-body">
					  
                      
					</div>-->
					<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
					  <button type="button" class="btn btn-info" data-dismiss="modal"><?php echo 'close';?></button>
					  <a href="javascript:;" class="btn btn-danger" id="delete_link"><?php echo 'delete';?></a>
					</div>
				  </div>
				</div>
			  </div>
            
            <!-- change status-->  
            <div class="modal fade" id="modal_changestatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h4 class="modal-title">Are you sure about this ?</h4>
					</div>
					<!--<div class="modal-body">
					  
                      
					</div>-->
					<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
					  <button type="button" class="btn btn-info" data-dismiss="modal"><?php echo 'close';?></button>
					  <a href="javascript:;" class="btn btn-danger" id="update_link"><?php echo 'update';?></a>
					</div>
				  </div>
				</div>
			  </div>  
              
              
<!-- SAMPLE BOX CONFIRM MODAL FORM-->
<script type="text/javascript">
	function confirm_bootbox(url)
	{
		$('#modal_bootbox').modal('show', {backdrop: 'static'});
		document.getElementById('link').setAttribute('href' , url);
	}
</script>
			
       		<div class="modal fade" id="modal_bootbox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
				  <div class="modal-content">
					<div class="modal-header">
					  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					  <h4 class="modal-title">Are you sure about this ?</h4>
					</div>
					<!--<div class="modal-body">
					  
                      
					</div>-->
					<div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
					  <button type="button" class="btn btn-info" data-dismiss="modal"><?php echo 'close';?></button>
					  <a href="javascript:;" class="btn btn-success" id="link"><?php echo 'ok';?></a>
					</div>
				  </div>
				</div>
			  </div>
               </body>
    </html>