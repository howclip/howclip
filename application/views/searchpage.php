<!DOCTYPE HTML>
<html>
<head>
<title>HowClip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
input[type=text] {
    width: 130px;
    box-sizing: border-box;
    border: 2px solid #ccc;
    border-radius: 10px;
    font-size: 14px;
    background-color: white;
    background-image: url('searchicon.png');
    background-position: 10px 10px; 
    background-repeat: no-repeat;
    padding: 8px 20px 8px 40px;
    -webkit-transition: width 0.4s ease-in-out;
    transition: width 0.4s ease-in-out;
}
input[type=text]:focus {
    width: 100%;
	border-radius: 10px;
	outline:0 !important; 
}

</style>
<style>

</style>
<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">



<div class="main-content"> 

  <!--left-fixed -navigation-->
  <div class="sidebar" role="navigation">
    <div class="navbar-collapse">
	
	
      <nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
	  
        <ul class="nav showone" id="side-menu">
          <li> <a href="#" class="active"><i class="fa fa-home nav_icon"></i> Home </a> </li>
		  <?php foreach($features as $videofeature) { ?>
          <li> <a href="<?php echo base_url()?>index.php/home/featurevideo/<?php echo $videofeature->name;?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name;?> </a> </li>
		  <?php } ?>
          <li> <a href="<?php echo base_url()?>index.php/home/history/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
  <li> <span style="font-size:18px;padding:15px;color:grey;">Show Results For</span> </li>
  
  <?php foreach($returnsearch as $searchcat) {?>
   <ul style="margin-left:5px;list-style:none;">
    <?php if($searchcat->sub['child']['child2']['child3']['category_title'] !="") { $ss7 = $searchcat->sub['child']['child2']['child3']['category_title'];$ss8 = preg_replace('/\s+/', '', $ss7);$newssz3 = explode('/',$ss8);$ssss4 = $newssz3[0].$newssz3[1];?>     
 <li><a href="#"><input  type="checkbox" id="<?php echo $ssss4;?>" class="chkboxl" checked style="margin-right:5px;" onChange="valueChangedlast('<?php echo $ssss4;?>')"><?php echo ucwords($searchcat->sub['child']['child2']['child3']['category_title']);?> </a>
 
 <?php } ?>
   <ul style="margin-left:5px;list-style:none;">
  <?php if($searchcat->sub['child']['child2']['category_title'] !="") { $ss5 = $searchcat->sub['child']['child2']['category_title']; $ss6 = preg_replace('/\s+/', '', $ss5); $newssz2 = explode('/',$ss6);$ssss3 = $newssz2[0].$newssz2[1]; ?>
 <li><a href="#"><input  type="checkbox" id="<?php echo $ssss3; ?>" class="chkbox" checked style="margin-right:5px;"  onChange="valueChangedfirst('<?php echo $ssss3;?>')"><?php echo ucwords($searchcat->sub['child']['child2']['category_title']); ?></a>
 
 <?php }?>
  
   <ul style="margin-left:5px;list-style:none;">
   <?php if($searchcat->sub['child']['category_title'] != "") { $ss3 = $searchcat->sub['child']['category_title'];$ss4 = preg_replace('/\s+/', '', $ss3);$newssz1 = explode('/',$string2);$ssss2 = $newssz1[0].$newssz1[1]; ?>
  <li><a href="#"><input  type="checkbox" id="<?php echo $ssss2;?>" class="chkbox1" checked style="margin-right:5px;" onChange="valueChanged1('<?php echo $ssss2;?>')"><?php echo ucwords($searchcat->sub['child']['category_title']);?></a>
  
  <?php }?>
  <ul style="margin-left:5px;list-style:none;">
  <?php if($searchcat->sub['category_title'] != "") { $ss = $searchcat->sub['category_title'];$ss2 = preg_replace('/\s+/', '', $ss);$ssz = explode('/',$ss2);$ssss = $ssz[0].$ssz[1];?>

   <li><a href="#"><input type="checkbox" id="<?php echo $ssss;?>" class="chkbox2" checked style="margin-right:5px;" onChange="valueChanged2('<?php echo $ssss;?>')"><?php echo ucwords($ssss);?></a>
     <?php } ?>
  <ul style="margin-left:5px;list-style:none;">
  
  <?php $i=1; if($searchcat->category_title !="") { $string1 = $searchcat->category_title; $string2 = preg_replace('/\s+/', '', $string1);$newssz = explode('/',$string2);$ssss1 = $newssz[0].$newssz[1];  ?>
   <li><a href="#"><input type="checkbox" id="<?php echo $ssss1;?>" class="chkbox3" checked style="margin-right:5px;"  onChange="valueChanged3('<?php echo $ssss1;?>')"><?php echo ucwords($searchcat->category_title);?></a></li>
   <?php $i++;} ?>
   </ul>
  </li>
  
  </ul>
  </li>
 </ul>
 </li>
 </ul>
 </li>
 </ul>
   <?php if($searchcat->sub['child']['child2']['child3']['category_title'] !="") { ?>     
 <!--<li><a href="#"><input  type="checkbox" id="chkboxl" class="chkboxl" checked style="margin-right:5px;" onChange="valueChangedlast('<?php echo $searchcat->sub['child']['child2']['child3']['category_title'];?>')"><?php echo ucwords($searchcat->sub['child']['child2']['child3']['category_title']);?> ee</a>-->
 
 <?php } ?>
 
 <!--<ul style="margin-left:5px; list-style:none;">
 <?php if($searchcat->sub['child']['child2']['category_title'] !="") { ?>
 <li><a href="#"><input  type="checkbox" id="chkbox" class="chkbox" checked style="margin-right:5px;"  onChange="valueChangedfirst('<?php echo $searchcat->sub['child']['child2']['category_title'];?>')"><?php echo ucwords($searchcat->sub['child']['child2']['category_title']); ?>dd</a>
 
 <?php }?>
 <ul style="margin-left:5px;list-style:none;">
 <?php if($searchcat->sub['child']['category_title'] != "") { ?>
  <li><a href="#"><input  type="checkbox" id="chkbox1" class="chkbox1" checked style="margin-right:5px;" onChange="valueChanged1('<?php echo $searchcat->sub['child']['category_title'];?>')"><?php echo ucwords($searchcat->sub['child']['category_title']);?>cc</a>
  
  <?php }?>
  <ul style="margin-left:5px;list-style:none;">
   <?php if($searchcat->sub['category_title'] != "") { ?>

   <li><a href="#"><input type="checkbox" id="chkbox2" class="chkbox2" checked style="margin-right:5px;" onChange="valueChanged2('<?php echo $searchcat->sub['category_title'];?>')"><?php echo ucwords($searchcat->sub['category_title']);?>bb</a>
  
   <?php } ?>
   <ul style="margin-left:5px; list-style:none;">
   <?php if($searchcat->category_title !="") {  ?>
   <li><a href="#"><input type="checkbox" id="chkbox3" class="chkbox3" checked style="margin-right:5px;"  onChange="valueChanged3('<?php echo $searchcat->category_title;?>')"><?php echo ucwords($searchcat->category_title);?>aa</a>
    
   
   <li>
   <?php  } ?>
   </ul>
   
   </li>
  
 
  </ul>
 
  </li>
 
  
  </ul>
 
 </li>

 </ul>-->
 
 </li>

 <?php } ?>
 
 </ul>

<ul class="dropdown">
	<li><a href="#">Short By:</a></li>
<!--	<li><a href="#">Newest</a></li>-->
	<li><a href="<?php echo base_url()?>index.php/home/advancesearchdata/<?php echo $term;?>/week">Last Week</a></li>
	<li><a href="<?php echo base_url()?>index.php/home/advancesearchdata/<?php echo $term;?>/month">This Month</a></li>
	<li><a href="<?php echo base_url()?>index.php/home/advancesearchdata/<?php echo $term;?>/year">This Year</a></li>
	</ul>
	
	<ul class="dropdown">
	<li><a href="#">Filter:</a></li>
	<li><a href="<?php echo base_url()?>index.php/home/advancesearchdata/<?php echo $term;?>/day">Today</a></li>
	<li><a href="<?php echo base_url()?>index.php/home/advancesearchdata/<?php echo $term;?>/short">Short</a></li>
	<li><a href="<?php echo base_url()?>index.php/home/advancesearchdata/<?php echo $term;?>/long">Long</a></li>
	</ul>
		
	


        <div class="clearfix"> </div>
      
        
        
        <ul id="menu" class="dropmen">
		 <?php 
	  foreach($categories as $menu)
{  ?>
<li> <a href="#"><?php echo $menu['title'];?> <span class="fa arrow"></span></a>
<ul class="scrolldiv">
<?php if($menu['submenu'] != '') { foreach($menu['submenu'] as $sub) { ?>
<li><a href="<?php echo base_url()?>index.php/home/categorysearch/<?php echo $sub['title']; ?>"><?php echo $sub['title']; ?></a></li>

<?php } } ?>
</ul>

</li>
 <?php  } ?>
</ul>
  
      </nav>
    </div>
  </div>
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
      <button id="showLeftPush"><i class="fa fa-bars"></i></button>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"  > <a href="index.html">
        <!--<h1></h1>
        <span>Website</span> </a>--> 
        <img src="<?php echo base_url();?>assets/images/logo.png" width="90" class="img-responsive">
       </a> </div>
      <!--//logo--> 
      <!--search-box-->
     <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off"  />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"  style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
           
          </li>
		   <?php if($this->session->userdata('id') == "") {?>
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"> <img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive but"></a> </li>
            <?php } ?>
		   <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/signup"> <img src="<?php echo base_url();?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>
		      <?php foreach($userDetail as $user) { $username = $user->username;$userimg = $user->userLogo; if($userimg== "") { $userimg = "user.png"; }}?>
		   <?php if($this->session->userdata('id')!="") {?>
		   <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url();?>uploads/<?php echo $userimg;?>" class="img-responsive"></div> </a>
            <ul class="dropdown-menu" style="left:initial;right:0;color:#000;">
              <li>
                <div class="notification_header">
                  <h3><?php echo $username;?></h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p><?php echo $this->session->userdata('email');?></p>
                  
                </div>
                <div class="clearfix"></div>
                </a></li>
				<li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/dash">My Account</a> </div>
              </li>
              <li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/logout">Log Out</a> </div>
              </li>
            </ul>
           </li>
		   <?php }?>
        </ul>
        <div class="clearfix"> </div>
      </div>
  
      <div class="profile_details"> 
       
      </div>
      <div class="clearfix"> </div>
    </div>
    
    <div class="clearfix"> </div>
    <div class="header-center">
    <div id="cssmenu">
            <ul>
			 <?php foreach($categories as $menu){  ?>
              <li> <a href="#"><i class="fa fa-cogs nav_icon"></i><?php echo $menu['title'];?> </a>
                <ul>
          <?php if($menu['submenu'] != '') { foreach($menu['submenu'] as $sub) { ?>
               
                  <li><a href='<?php echo base_url()?>index.php/home/categorysearch/<?php echo $sub['title']; ?>' ><?php echo $sub['title']; ?></a>
                   <ul>
				     <?php if($sub['submenu'] != '') { foreach($sub['submenu'] as $sub2) { ?>
                 <li><a href='<?php echo base_url()?>index.php/home/categorysearch/<?php echo $sub2['title']; ?>' ><?php echo $sub2['title']; ?></a>
				 
				 
				 <ul>
				     <?php if($sub2['submenu'] != '') { foreach($sub2['submenu'] as $sub3) { ?>
                 <li><a href='<?php echo base_url()?>index.php/home/categorysearch/<?php echo $sub3['title']; ?>' ><?php echo $sub3['title']; ?></a>
				 
				 <ul>
				     <?php if($sub3['submenu'] != '') { foreach($sub3['submenu'] as $sub4) { ?>
                 <li><a href='<?php echo base_url()?>index.php/home/categorysearch/<?php echo $sub4['title']; ?>' ><?php echo $sub4['title']; ?></a>
						   <?php } } ?>
                  </ul>
				 
				 
						   <?php } } ?>
                  </ul>
						   <?php } } ?>
						   
						   
						   
						   
                  </ul>
				 
                   </li>
				   
                 <?php } } ?>

                </ul>
              
               </li>
			      <?php  } ?>
            </ul>
          </div>
          </div>
  </div>
 
   
  <div id="page-wrapper">
  
  
    <div class="main-page">
	<?php //echo '<pre>';print_r($returnsearch);die;//foreach($returnsearch as $getvideo) {  }?>
	  <?php //foreach($returnsearch as $key1 => $searchvideo) { //echo '<pre>';
	// echo '<pre>'; print_r($searchvideo->videodata);die;}
	// print_r($searchvideo->sub['child']['child2']['child3']['video']); die;
	 // echo $searchvideo->videoname; 
	 //die;
	 //foreach($searchvideo->sub['videos'] as $vvdata) { print_r($vvdata); echo $vvdata['videoname']; $die; }
	   ?>
      <div class="blank-page widget-shadow scroll" id="style-2 div1">
	  <?php if($returnsearch == "") {?>
	  
	   
	  
	  <span> <?php echo "No Results found";?></span>
	  
	  
	   <?php } ?>
      <?php foreach($returnsearch as $key1 => $searchvideo) { ?>
        <div class="row">
		
		<?php if(($searchvideo->videodata) !=""){ foreach(($searchvideo->videodata) as $key => $vid) {  ?><?php $string = $vid['video_category'];$string1 = preg_replace('/\s+/', '', $string); $nnn = explode('/',$string1); $nh = $nnn[0].$nnn[1];?>
		<div class="<?php echo $nh;?>">
          <div class="col-md-3 col-xs-6" >
		
		
		  <?php if (getimagesize('http://104.168.142.137/PUPILCLIP/uploads/images/'.$vid['videothumb']) !== false) { $link ="http://104.168.142.137/PUPILCLIP/uploads/images/".$vid['videothumb'];}else{$link = "http://104.168.142.137/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vid['id'];?>"><img src=<?php echo $link; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
             
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"> <?php $vv = $vid['videoname'];$length3 = strlen($vid['videoname']); if($length3 < 5){
			  $vvname = $vv;}else { $vvname = substr($vv,0,5); $vvname = $vvname."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vid['id'];?>"><?php  echo ucwords($vvname);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($vid['totalv'] == 0) { echo "0"; } else { echo $vid['totalv']; }?> Views</a> 
              </p>
 <div class="rating">
 <ul>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 </ul>
 </div>
 
 <div class="advancepara">
  <?php $vname = $vid['description'];$length = strlen($vid['description']); if($length < 50){
			  $v = $vname;}else { $v = substr($vname,0,50); $v = $v."..."; }?>
 <?php echo ucwords($v);?>
 </div>
 
 </div>
            </div>
			
          </div>
		  </div>
		  <?php } } ?>
		  
		  
		  
		  
		  
		  
		<?php if(($searchvideo->sub['videos']) !=""){ foreach($searchvideo->sub['videos'] as $vvdata) {  ?><?php $stringc = $vvdata['video_category'];$stringcc = preg_replace('/\s+/', '', $stringc);
		$ssza = explode('/',$stringcc);$ssssa = $ssza[0].$ssza[1];
		?>
		<div class="<?php echo $ssssa;?>">
		  <div class="col-md-3 col-xs-6">
		 
		  <?php if (getimagesize('http://104.168.142.137/PUPILCLIP/uploads/images/'.$vvdata['videothumb']) !== false) { $link2 ="http://104.168.142.137/PUPILCLIP/uploads/images/".$vvdata['videothumb'];}else{$link2 = "http://104.168.142.137/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvdata['id'];?>"><img src=<?php echo $link2; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2v = $vvdata['videoname'];$lengthg = strlen($vvdata['videoname']); if($lengthg < 5){
			  $vvname2 = $v2v;}else { $vvname2 = substr($v2v,0,5); $vvname2 = $vvname2."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvdata['id'];?>"><?php echo ucwords($vvname2);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($vvdata['totalsubview'] == 0) { echo "0"; } else { echo $vvdata['totalsubview']; }?> Views</a> 
              </p>
 <div class="rating">
 <ul>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 </ul>
 </div>
 
 <div class="advancepara">
 <?php $vname2 = $vvdata['description'];$lengtho = strlen($vvdata['description']); if($lengtho < 50){
			  $v8 = $vname2;}else { $v8 = substr($vname2,0,50); $v8 = $v8."..."; }?>
 <?php echo ucwords($v8);?>
 </div>
 
 </div>
            </div>
          </div>
		  </div>
		  
		<?php } }?>

		 <?php if(($searchvideo->sub['child']['videos']) != "") { foreach(($searchvideo->sub['child']['videos']) as $vvideos) { ?>
		 <?php $str = $vvideos['video_category'];$str2 = preg_replace('/\s+/', '', $str);
		$str3 = explode('/',$str2);$str33 = $str3[0].$str3[1];
		?>
		 <div class="<?php echo $str33;?>">
		  <div class="col-md-3 col-xs-6" >
		 
		  <?php if (getimagesize('http://104.168.142.137/PUPILCLIP/uploads/images/'.$vvideos['videothumb']) !== false) { $link3 ="http://104.168.142.137/PUPILCLIP/uploads/images/".$vvideos['videothumb'];}else{$link3 = "http://104.168.142.137/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvideos['id'];?>"><img src=<?php echo $link3; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2name = $vvideos['videoname'];$length6 = strlen($vvideos['videoname']); if($length6 < 5){
			  $vidname = $v2name;}else { $vidname = substr($v2name,0,5); $vidname = $vidname."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $vvideos['id'];?>"><?php echo ucwords($vidname);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($vvideos['totalchildview'] == 0) { echo "0"; } else { echo $vvideos['totalchildview']; }?> Views</a> 
              </p>
 <div class="rating">
 <ul>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 </ul>
 </div>
 
 <div class="advancepara">
 <?php $vnamedesc = $vvideos['description'];$length7 = strlen($vvideos['description']); if($length7 < 50){
			  $v9 = $vnamedesc;}else { $v8 = substr($vnamedesc,0,50); $v9 = $v9."..."; }?>
 <?php echo ucwords($v9);?>
 </div>
 
 </div>
            </div>
          </div>
		  </div>
		  
		  
		<?php } } ?>	
		
		
		
		
			  
		 <?php if(($searchvideo->sub['child']['child2']['vvv']) != "") { foreach(($searchvideo->sub['child']['child2']['vvv']) as $final) { ?>
		  <?php $strgs = $final['video_category'];$strv = preg_replace('/\s+/', '', $strgs);
		$strvs = explode('/',$strv);$str333 = $strvs[0].$strvs[1];
		?>
		 <div class="<?php echo $str333;?>">
		  <div class="col-md-3 col-xs-6" >
		
		  <?php if (getimagesize('http://104.168.142.137/PUPILCLIP/uploads/images/'.$final['videothumb']) !== false) { $linkfinal ="http://104.168.142.137/PUPILCLIP/uploads/images/".$final['videothumb'];}else{$linkfinal = "http://104.168.142.137/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $final['id'];?>"><img src=<?php echo $linkfinal; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2namefinal = $final['videoname'];$lengthfinal = strlen($final['videoname']); if($lengthfinal < 5){
			  $vidnamefinal = $v2namefinal;}else { $vidnamefinal = substr($v2namefinal,0,5); $vidnamefinal = $vidnamefinal."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $final['id'];?>"><?php echo ucwords($vidnamefinal);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($final['totalchild2view'] == 0) { echo "0"; } else { echo $final['totalchild2view']; }?> Views</a> 
              </p>
 <div class="rating">
 <ul>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 </ul>
 </div>
 
 <div class="advancepara">
 <?php $vnamedescfinal = $final['description'];$length7final = strlen($final['description']); if($length7final < 50){
			  $vfinal = $vnamedescfinal;}else { $v8 = substr($vnamedescfinal,0,50); $v9 = $v9."..."; }?>
 <?php echo ucwords($vfinal);?>
 </div>
 
 </div>
            </div>
          </div>
		  </div>
		  
		<?php } } ?>		    
       
	   
	    <?php if(($searchvideo->sub['child']['child2']['child3']['video']) != "") { foreach(($searchvideo->sub['child']['child2']['child3']['video']) as $last) { ?>
		<?php $lstr = $last['video_category'];$lstr1 = preg_replace('/\s+/', '', $lstr);
		$lstr2 = explode('/',$lstr1);$lstr3 = $lstr2[0].$lstr2[1];
		?>
		<div class="<?php echo $lstr3;?>">
		  <div class="col-md-3 col-xs-6" >
		
		  <?php if (getimagesize('http://104.168.142.137/PUPILCLIP/uploads/images/'.$last['videothumb']) !== false) { $linklast ="http://104.168.142.137/PUPILCLIP/uploads/images/".$last['videothumb'];}else{$linklast = "http://104.168.142.137/PUPILCLIP/uploads/images/download.jpg";} ?>
		  
            <div class="thumbnail"> <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $last['id'];?>"><img src=<?php echo $linklast; ?> class="img-responsive" style="position: relative;width: 400px;height: 143px;"></a>
            
              <div class="caption catch">
 <div class="advancehead" style="margin:0px;"><?php $v2namelast = $last['videoname'];$lengthlast = strlen($last['videoname']); if($lengthlast < 5){
			  $vidnamelast = $v2namelast;}else { $vidnamelast = substr($v2namelast,0,5); $vidnamelast = $vidnamelast."..."; }?>
 <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $last['id'];?>"><?php echo ucwords($vidnamelast);?></a></div>
 <p style="float:right; display:inline-block;"class="advanceview"><a href="#" ><?php if($last['totalchildviewlast'] == 0) { echo "0"; } else { print_r($last['totalchildviewlast']); }?> Views</a> 
              </p>
 <div class="rating">
 <ul>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 <li><a hrfe="#"> <i class="fa fa-heart-o" aria-hidden="true"></i>
 </a></li>
 </ul>
 </div>
 
 <div class="advancepara">
 <?php $vnamedesclast = $last['description'];$length7last = strlen($last['description']); if($length7last < 50){
			  $vlast = $vnamedesclast;}else { $v8last = substr($vnamedesclast,0,50); $v9last = $v9last."..."; }?>
 <?php echo ucwords($vlast);?>
 </div>
 
 </div>
            </div>
          </div>
		  
		  
		<?php } }?>		    
	   
	   
	   
        </div>
		 <?php } ?>
        </div>
   
	
      </div>
    </div>
  </div>
  <!--footer-->
  
  <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="http://104.168.142.137/PUPILCLIP/index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 

<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
<script>
function valueChanged3(x)
{

var siv = x;
var str = siv.replace(/\s/g, '');
var abc = $("#"+siv).prop("checked");

if(abc)
{
$("."+ str).css("display", "block");
}
else
{
  $("."+ str).css("display", "none");
}
}
</script>



<script type="text/javascript">

function valueChangedfirst(x)
{
var siv = x;
var str = siv.replace(/\s/g, '');
var abc = $("#"+siv).prop("checked");
if(abc)
{
$("."+ siv).css("display", "block");
}
else
{
$("."+ siv).css("display", "none");
}
}
function valueChanged1(x)
{
var siv = x;
var str = siv.replace(/\s/g, '');
var abc = $("#"+siv).prop("checked");
if(abc)
{
$("."+ siv).css("display", "block");
}
else
{
$("."+ siv).css("display", "none");
}
}
function valueChanged2(x)
{
var siv = x;
var str = siv.replace(/\s/g, '');

var abc = $("#"+siv).prop("checked");
if(abc)
{
$("."+ siv).css("display", "block");
}
else
{
$("."+ siv).css("display", "none");
}

}


function valueChangedlast(x)
{
var siv = x;
var str = siv.replace(/\s/g, '');
var abc = $("#"+siv).prop("checked");
if(abc)
{
$("."+ siv).css("display", "block");
}
else
{
$("."+ siv).css("display", "none");
}
}

</script>
</body>
</html>