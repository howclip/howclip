<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
  
    <title>Pupilclip</title>

    <link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
	<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
	<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>
 	<meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="424111079839-01ao42qik8ltrgfvf5f1ibuh6ntrb629.apps.googleusercontent.com">
    <script src="https://apis.google.com/js/platform.js" async defer></script>
	<script src="<?php echo base_url();?>assets/js/jquery.min"></script>
  </head>
  <body>
   <div class="holder">
   <div class="container">
   <div class="row">
   <div class="col-md-4 col-md-offset-4">
   <a href="http://livesoftwaresolution.info/PUPILCLIP"><img src="<?php echo base_url();?>assets/images/logo1.png" class="img-responsive logo"></a>
   <h4>Sign in to continue to <span style="color:#03C;">Pupilclip</span></h4>
   <?php if($this->session->flashdata('err'))
	 		{
			?>
	<div style="color: #D8000C;">			
	<p style="text-align:center;"><?php echo $this->session->flashdata('err'); ?></p>
	</div>
    <?php } ?>		
	<div style="color:#FF0000"><?=validation_errors(); ?></div>
   <div class="logwrap">
   <div class="profiledp"> <i class="fa fa-user" aria-hidden="true"></i>  </div>
   
   <form class="setup" method="post" action="<?php echo base_url()?>index.php/home/login">
  <div class="form-group">

    <input type="email" class="form-control login" name="email" id="exampleInputEmail1" placeholder="Email">
  </div>
  <div class="form-group">
   
    <input type="password" class="form-control login" name="password" id="exampleInputPassword1" placeholder="Password">
  </div>
  
 
  <button type="submit" class="btn loginbutton">Submit</button>
</form>
   
   </div>
   <div class="row">
   <div class="col-md-10 col-md-offset-1">
   <h3>OR Sign in With...</h3>
   <div class="row">
   <div class="col-md-6">
   <div style="margin-left:10px;"><img src="<?php echo base_url();?>assets/images/fblogin.jpg" id="loginBtn" class="img-responsive" style="height:30px; float:left; display:inline-block; margin-right:10px;">
 </div>
 <div class="col-md-6">
   <div id="response" style="display:none;"></div>
   <div class="g-signin2" data-onsuccess="onSignIn" data-theme="dark" style="height:30px;display:inline-block;"></div>
   </div>
   </div>
   </div>
   </div>
	<div style="display:none">
	User Id: <span id="gid"></span><br>
	User Name: <span id="gname"></span><br>
	User Email: <span id="gmail"></span><br>
	</div>
   <h3><a href="<?php echo base_url()?>index.php/home/signup">Create Account</a></h3>
   </div>
   </div>
   </div>
   <!--<div class="row">
   <div class="col-md-4 col-md-offset-4">
   <div class="logresponsive">
   <ul>
   <li>
   <a href="#">
   <img src="<?php echo base_url();?>assets/images/fblogin.jpg" id="loginBtn" class="img-responsive" > </a>
   </li>
   <li>
 <span class="g-signin2" data-onsuccess="onSignIn" data-theme="dark"></span> </li>
   </ul>
   </div>
   </div>
   </div>-->
   

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript">
function getUserData() {
	FB.api('/me?fields=id,first_name,last_name,email,link,gender,locale,picture', function(response) {
document.getElementById('response').innerHTML = 'Hello ' + response.id+ '<br>'+ response.email+ '<br>' + response.first_name+ ' ' + response.last_name +  '<br>' + response.gender ;
var email = response.email;
SubmitUserData(email);
	});
}
 function SubmitUserData(email) {
	 $.ajax({
      url: '<?php echo base_url();?>index.php/home/insertfbdata',
      type:"POST",
      data:{'email':email
                 },
    success: function(response)
    {
	window.location.href = '<?php echo base_url();?>index.php/home/dash';
     }
    
  });
}


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '384556705248537',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

document.getElementById('loginBtn').addEventListener('click', function() {

	FB.login(function(response) {
		if (response.authResponse) {
			//user just authorized your app
			document.getElementById('loginBtn').style.display = 'none';
			getUserData();
			
		}
	}, {scope: 'email,public_profile', return_scopes: true});
}, false);
 
 </script>
  <script>
  function SubmitUserData(email) {
	 $.ajax({
      url: '<?php echo base_url();?>index.php/home/insertfbdata',
      type:"POST",
      data:{'email':email
                 },
    success: function(response)
    {
	//alert(response);
	//console.log(response);
		window.location.href = '<?php echo base_url();?>index.php/home/dash';
     }
    
  });
}
      function onSignIn(googleUser) {
	  
        var profile = googleUser.getBasicProfile();
        console.log("ID: " + profile.getId()); 
		document.getElementById('gid').innerHTML = profile.getId();
        console.log('Full Name: ' + profile.getName());
		document.getElementById('gname').innerHTML =  profile.getName();
		document.getElementById('gmail').innerHTML =  profile.getEmail();
        console.log('Given Name: ' + profile.getGivenName());
        console.log('Family Name: ' + profile.getFamilyName());
        console.log("Image URL: " + profile.getImageUrl());
        console.log("Email: " + profile.getEmail());

        var id_token = googleUser.getAuthResponse().id_token;
        console.log("ID Token: " + id_token);
		var email = profile.getEmail();
		//alert(email);
		SubmitUserData(email);
		
      };
	  
    </script>
  </body>
</html>