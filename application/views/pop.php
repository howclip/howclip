<!DOCTYPE html>
<html>
<head>
  <title>Twitter Bootstrap : Bootstrap Methods using Javascript </title>
<link rel="stylesheet"
      href="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css">

<script src="http://code.jquery.com/jquery.min.js"></script>
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
</head>
<script>
$(document).ready(function(){
    $(".pop-show").click(function(){
   		$(".pop-target a").popover('show');
    });
    $(".pop-hide").click(function(){
   		$(".pop-target a").popover('hide');
    });
    $(".pop-toggle").click(function(){
   		$(".pop-target a").popover('toggle');
    });
    $(".pop-destroy").click(function(){
   		$(".pop-target a").popover('destroy'); //destroy
    });
  
   $(".DemoBS2 a").popover({
        placement : 'top'
    });
});
 
</script>
<script>
    $(function(){
                 // function from the jquery form plugin
                 $('#upload_form').ajaxForm({
                    beforeSend:function(){
                         $(".progress").show();
                    },
                    uploadProgress:function(event,position,total,percentComplete){
                        $(".progress-bar").width(percentComplete+'%'); //dynamicaly change the progress bar width
                        $(".sr-only").html(percentComplete+'%'); // show the percentage number
                    },
                    success:function(){
                        $(".progress").hide(); //hide progress bar on success of upload
                    },
                    complete:function(response){

                        if(response.responseText == '0') {
                            $(".response").html("<div class='alert alert-dismissable alert-success'>Success! You are being redirected...</div>");
                            window.location = "<?php echo base_url('admin/videos'); ?>";
                        }

                        else if (response.responseText == '1') {
                            $(".response").html("<div class='alert alert-dismissable alert-danger'>Please select a file!</div>");
                            //window.location = "<?php echo base_url('admin/videos'); ?>";
                        }

                        else if (response.responseText == '2') {
                            $(".response").html("<div class='alert alert-dismissable alert-danger'>Please insert appopriate data!</div>");
                        }

                        else if (response.responseText == '3') {
                            $(".response").html("<div class='alert alert-dismissable alert-danger'>Upload error... Please try later!</div>");
                        }
                    }
                 });

                 //set the progress bar to be hidden on loading
                 $(".progress").hide();
            });
            </script>
<style>
.DemoBS2{
      margin:140px 20px 30px 50px;
}

</style>
<body>
<div class="DemoBS2">
      <p class="pop-target">
        <a href="#" title="Wisdom Quotes" 
        class="btn btn-lg btn-default" data-toggle="popover"
        data-content="You must be the change you 
                     wish to see in the world">Popover Target</a>
    </p>
 <div class="bs-example-tooltips">
      <button type="button" class="btn btn-primary pop-show">
        Show Popover 
      </button>
   
      <button type="button" class="btn btn-info pop-hide" >
        Hide Popover 
      </button>
   
      <button type="button" class="btn btn-warning pop-toggle" >
        Toggle Popover 
      </button>
   
      <button type="button" class="btn btn-danger pop-destroy" >
        Destroy Popover 
      </button>
    </div></div>
    
    <form id="upload_form" role="form" method="POST" action="<?php echo base_url()?>index.php/home/test" enctype="multipart/form-data">

    <div class="form-group">
        <label class="control-label">Video Title</label>
        <input type="text" class="form-control" name="video_title" id="video_title" value="<?php echo set_value('video_title');?>" required="required" />
        <?php echo form_error('video_title'); ?>
    </div>

    <div class="form-group">
        <label class="control-label">Select File</label>
        <input type="file" name="video" id="video" required="required" onchange="uploadOnChange(this.value)" />
        <?php echo form_error('video'); ?>
        <div class="alert alert-danger" id="filename_error" style="display:none"></div>
    </div>

<div class="form-group">
    <button id="upload_button" type="submit" class="btn btn-large btn-success" name="createvenuebtn" value="New Video">Upload</button>
    <a href="<?php echo base_url('admin/videos'); ?>" class="btn btn-large btn-danger">Cancel</a>
</div>

<div class="progress progress-striped active">
    <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
    <span class="sr-only">0% Complete</span>
    </div>
</div>

<div class="response"></div>

</form>
    
    
    
    
</body>
</html>








