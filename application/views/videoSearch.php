<!DOCTYPE HTML>
<html>
<head>
<title>HowClip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-menu.css">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
.widthdrop {
  min-width:auto;
  width:100%;
}
.searchimage
{
height:150px;
width:100%;
position:relative;
}
.timebox
{
position:absolute;
padding:5px;
right:15px;
bottom:0px;
background:#000;
color:#FFF;
font-size:11px;
}
</style>

<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation-->
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
   <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->
      
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
          <?php if($this->session->userdata('id')!= '') {?>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
     <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
    <?php foreach($features as $videofeature) { ?>
          <li> <a href="<?php echo base_url()?>index.php/home/featurevideo/<?php  echo $videofeature->name;?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name;?> </a> </li>
		  <?php } ?>
          <li> <a href="<?php echo base_url()?>index.php/home/history/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
		  <li> <a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> My Channel </a> </li>
	 <li class="divider" role="seperator"></li>
		  <li> <a href="#"><i class="fa fa-th-large nav_icon"></i>SUBSCRIPTIONS </a> </li>
		  <?php foreach(($usersubscription->uploaderdetail) as $uploader) { ?>
		   <li> <a href="<?php echo base_url()?>index.php/home/subscriber/<?php echo $uploader->id;?>"> <i class="fa fa-th-large nav_icon"></i><?php echo $uploader->username; ?> </a> </li>
		   <?php } ?>
  </ul>
          <?php } ?>
</div>
      <!--toggle button end--> 
      <!--logo -->
       <?php foreach($companydetail as $company) { if($company->company_logo != ""){ $cmplogo = base_url()."/Admin/uploads/$company->company_logo"; } else {$cmplogo = base_url()."/uploads/logo.png";  } ?>
      <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">
      
        <img src="<?php echo $cmplogo;?>" class="img-responsive">
       </a> </div>
    <?php } ?>
      <!--//logo--> 
      <!--search-box-->
     <!--<div class="search-box">
       <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off"/>
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>-->
	  <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off" />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>
      </div>
      </div>
	  
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"  style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
           
          </li>
		   <?php if($this->session->userdata('id') == "") {?>
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/loginpage"> <img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive but"></a> </li>
            <?php } ?>
		   <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/signup"> <img src="<?php echo base_url();?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>
		  
		   <?php if($this->session->userdata('id')!="") {?>
		   <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url();?>assets/images/photos-import-vflI4uOxj.png" class="img-responsive"></div> </a>
            <ul class="dropdown-menu" style="left:initial;right:0;color:#000;">
              <li>
                <div class="notification_header">
                  <h3><?php echo $this->session->userdata('name');?></h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p><?php echo $this->session->userdata('email');?></p>
                  
                </div>
                <div class="clearfix"></div>
                </a></li>
				<li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/dash">My Account</a> </div>
              </li>
              <li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/logout">Log Out</a> </div>
              </li>
            </ul>
           </li>
		   <?php }?>
        </ul>
        <div class="clearfix"> </div>
      </div>
	 
	 
    </div>
    
    
  </div>
 <div class="uploadwrap">
  <div class="container-fluid">
  <div class="row" style="padding:0px;">
  <div class="col-md-8 col-md-offset-2">
  <div class="blankpage">
  <div class="filtertop">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default" style="border:none;">
    <div class="panel-heading filter" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="border:1px solid #999999; padding:3px 10px; border-radius:4px;" aria-expanded="true" aria-controls="collapseOne">
         Filter <span class="caret"></span>
        </a>
        
        <div class="result"></div>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      <div class="row">
      <div class="col-md-6">
      <div class="table-responsive">
       <table class="table table-condensed filtertable">
  <tr>
  <th> Upload Date</th>
 
  <th> Duration </th>

  <!--<th>Sort by</th>-->
  </tr>
  <tr>
  <td><a href="<?php echo base_url()?>index.php/home/history/1day">Before 28 Hours </a></td>

   <td><a href="<?php echo base_url()?>index.php/home/history/short">Short (< 2min)</a><br>
   <a href="<?php echo base_url()?>index.php/home/history/long">long (< 20min)</a></td>
   

    <!-- <td>Relevance</td>-->
  </tr>
</table>
</div>
</div>
</div>
      </div>
    </div>
  </div>
  
  
</div>
  
  </div>
	<?php foreach($videoDetail as $video) {

   if($video->noresult) { ?>
  <div class="row filtercontent">
  <div class="col-md-4">
  
  </div>
  <div class="col-md-8">
  <h4><a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $video->id;?>"><?php echo ucwords($video->noresult);?> </a></h4>
  <ul>
  
  </ul>

  </div>
  
</div>  
<?php } else { ?>
 
  <div class="row filtercontent">
  <div class="col-md-6">
 
  <?php 
 if (getimagesize(base_url().'/uploads/images/'.$video->video_img) !== false) { $link =base_url()."/uploads/images/".$video->video_img;}else{$link = base_url()."/uploads/images/download.jpg"; } ?>
 
  <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $video->id;?>"><img src=<?php echo $link;?> class="img-responsive searchimage">
  </a>
      <?php if($video->price > 0) { ?>
      <a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $video->id;?>"><div class="dollar" style="margin-top: 16px;
    margin-left: 5px;"><img src="<?php echo base_url()?>/assets/images/usd1600.png" class="img-responsive"></div></a>
              
              <?php  } ?>
  <div class="timebox"><?php echo $video->video_duration;?></div>
  </div>
  <div class="col-md-6">
  <h4><a href="<?php echo base_url()?>index.php/home/showvideo/<?php echo $video->id;?>"><?php echo ucwords($video->videoname);?> </a></h4>
   
  
  <ul>
  <li><?php echo $video->username;?> </li><br>
  <li><?php echo $video->Date;?> </li>
  <li> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li> <?php if($video->view == "") { echo "0"; } else { echo $video->view; }?> Views</li>
  </ul>
 <?php $vname = $video->description;$length = strlen($video->description); if($length < 70){
			  $v = $vname;}else { $v = substr($vname,0,70); $v = $v."..."; }?>
  <p><?php echo ucwords($v);?></p>
  </div>
  
  
</div>  
<?php } } ?>
<!--<div class="row filtercontent">
  <div class="col-md-4">
  <img src="<?php echo base_url();?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive">
  </div>
  <div class="col-md-8">
  <h4><a href="#">Video Name</a></h4>
  <ul>
  <li><a href="#">4 Years ago </a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> 477 views</a></li>
  </ul>
  
  <p><a href="#">PHP is one of the most useful languages to know and is used everywhere you look online. In this tutorial, I start from the beginning ...</a></p>
  </div>
  
  
</div>-->
<!--<div class="row filtercontent">
  <div class="col-md-4">
  <img src="<?php echo base_url();?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive">
  </div>
  <div class="col-md-8">
  <h4><a href="#">Video Name</a></h4>
  <ul>
  <li><a href="#">4 Years ago </a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> 477 views</a></li>
  </ul>
 
  <p><a href="#">PHP is one of the most useful languages to know and is used everywhere you look online. In this tutorial, I start from the beginning ...</a></p>
  </div>
  
  
</div>-->

  </div>
  </div>
  </div>
  </div>
  </div>
 <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<script src="<?php echo base_url();?>assets/js/fileupload.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
<script type="text/javascript">

function ajaxSearch()
{

    var input_data = $('#input-31').val();

    if (input_data.length === 0)
    {
        $('#suggestions').hide();
    }
    else
    {

        var post_data = {
            'search_data': input_data,
            '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            };

        $.ajax({
            type: "POST",
            url: "<?php echo base_url()?>index.php/home/searchAll",
            data: post_data,
            success: function (data) {
                // return success
                if (data.length > 0) {
				//window.location.href = '<?php echo base_url();?>index.php/home/searchpage';
                    $('#suggestions').show();
                    $('#autoSuggestionsList').addClass('auto_list');
                    $('#autoSuggestionsList').html(data);
                }
            }
         });
		 
		
     }
	 
 }
</script>
 <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/sidebar-menu.js"></script>
  <script>
    $.sidebarMenu($('.sidebar-menu'))
  </script>
  <script>
  $(document).on("click", function(e){
    if( !$("#suggestions").is(e.target) ){ 
        $("#suggestions").hide();
    }
});
  </script>


</body>
</html>