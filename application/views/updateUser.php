<!DOCTYPE HTML>
<html>
    <head>
        <title>HowClip</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sidebar-menu.css">
        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>


        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
        <style>
            input[type=text] {
                width: 130px;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 10px;
                font-size: 14px;
                background-color: white;
                background-image: url('searchicon.png');
                background-position: 10px 10px; 
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            input[type=text]:focus {
                width: 100%;
                border-radius: 10px;
                outline:0 !important; 
            }

            .search-box input[type=text] {
                width: 50%;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 25px;
                font-size: 14px;
                background-color: white;
                background-image: url(https://www.w3schools.com/howto/searchicon.png);
                background-position: 10px 10px;
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            .search-box input[type=text]:focus {
                outline:0;
            }

            .searchbut
            {
                padding: 5px 15px;
                background: #ddd;
                color:#000;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
            }
            .searchbut:hover ,.searchbut:focus,.searchbut:active:focus{
                background: #ccc;
                color:#000;
                outline:0;
            }
            .advance{
                background: #ddd;
                color:#d60808;
                padding: 5px 15px;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
                font-weight:bold;
               
            }
            a.advance:visited
            {
                color:#d60808;
            }






        </style>

    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 

            <div class="sticky-header header-section ">
                <div class="header-left"> 

                    <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id') ?>">My Videos</a></li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>">History </a> </li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"> My Channel </a> </li>
                            <li class="divider" role="seperator"></li>
                            <li> <a href="#">SUBSCRIPTIONS </a> </li>
                            <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"> <?php echo $uploader->username; ?> </a> </li>
                            <?php } ?>

                            <!--<li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>-->
                        </ul>
                    </div>
                    <?php foreach ($companydetail as $company) {
                        if ($company->company_logo != "") {
                            $cmplogo = base_url() . "/Admin/uploads/$company->company_logo";
                        } else {
                            $cmplogo = base_url() . "/uploads/logo.png";
                        } ?>
                        <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">

                                <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                            </a> </div>
<?php } ?>

                   <form class="input" method="post" id="my_form" action="<?php echo base_url() ?>index.php/welcome/getsearchdata">
                        <div class="search-box">
                            <input type="text" style="width:55%;" id="input-31" onKeyUp="ajaxSearch();" name="search_data" placeholder="Search..">

                            <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:55%;z-index:10000;">
                                <div id="autoSuggestionsList"></div>
                            </div>

                            <a href="javascript:void(0)" id="chkval"  class="btn searchbut"> Search  </a>
                            <a href="<?php echo base_url() ?>index.php/welcome/open_advanceSearch" class="btn advance"> Advanced Topic Search  </a>
                        </div>




                    </form>
                    <!--//end-search-box-->
                    <div class="clearfix"> </div>
                </div>
                <div class="header-right">
                    <div class="profile_details_left"><!--notifications of menu start -->
                        <ul class="nofitications-dropdown">
                            <li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/uploadpage" style="color:#333;" > <img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > </a>

                            </li>
<?php foreach ($userDetail as $user) {
    $username = $user->username;
    $userimg = $user->userLogo;
    if ($userimg == "") {
        $userimg = "user.png";
    } ?>
                                <!--   
                                  <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url(); ?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
                                <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                    <ul class="dropdown-menu" style="left:initial; right:0;">
                                        <li>
                                            <div class="notification_header">
                                                <h3><?php echo $username; ?></h3>
                                            </div>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p><?php echo $this->session->userdata('email'); ?></p>

                                                </div>
                                                <div class="clearfix"></div>
                                            </a></li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                        </li>
                                        <li>

                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                        </li>
                                    </ul>
                                </li>
<?php } ?>
                        </ul>
                        <div class="clearfix"> </div>
                    </div>


                </div>


            </div>
            <div class="uploadwrap">
                <div class="container-fluid">
                    <div class="row" style="padding:0px;">
                        <div class="col-md-2" style="padding-right:0px;">
                            <ul class="sidebar-menu">
                                <li class="sidebar-header">HowClip</li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/dash">
                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-files-o"></i>
                                        <span>Video Manager</span>
                                    </a>
                                    <ul class="sidebar-submenu" >
                                        <li><a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Videos</a></li>
                                        <li><a href="<?php echo base_url() ?>index.php/home/showplaylist/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Playlists</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>My Channel</span>

                                    </a>

                                </li>



                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/userupdate/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>User Profile</span>

                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/changePass/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>Change Password</span>

                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/myorders/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>My Orders</span>

                                    </a>

                                </li>
                            </ul>
                        </div>
                        <div class="col-md-10" style="padding:0px;">
                            <div class="row">
                                <div class="col-md-12">
<?php foreach ($userDetail as $user) {
    $date = $user->dob; ?>
                                        <div class="blankpage">
                                            <div class="profileimg">
                                                <img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive">
                                            </div>
                                            <div class="profilestatus">
    <?php echo $this->session->userdata('name') ?>
                                                <!--<a href="#">View Channel</a>-->
                                            </div>
                                            <!--<div class="dashmen">
                                            <ul>
                                            <li><a href="#"> Views </a></li>
                                              <li><a href="#">Subscriber </a></li>
                                                <li><a href="#"> Add widget </a></li>
                                            </ul>
                                            </div>-->
                                        </div>

                                    </div>
                                </div>

                                            <?php
                                            $classes = explode("-", $date);
                                            // $classes[0]; echo $classes[1]; echo $classes[2];
                                            ?>
                                <div class="row">

                                    <div class="col-md-12">
                                        <div style="color:#FF0000;text-align:center;" id="formerror"></div>
                                        <div class="blankpage">
                                            <?php
                                            $mobile = $user->mobileNo;
                                            if (strlen($mobile) > 10) {

                                                $phn = explode(",", $mobile);
                                                $ph = $phn[1];
                                            } else {

                                                $ph = $mobile;
                                            }
                                            ?>
                                            <form class="setform" method="post" name="form" onSubmit="return validation()" action="<?php echo base_url() ?>index.php/home/EditUser/<?php echo $user->id; ?>" enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="form-group col-md-6 col-xs-6" style="padding-right:5px;">
                                                        <input type="text" class="form-control login" id="exampleInputEmail1" name="fname" placeholder="First Name" value="<?php echo $user->Firstname; ?>">
                                                        <div style="color:#FF0000;display:none;" id="fname" > Enter First Name</div>
                                                    </div>
                                                    <div class="form-group col-md-6 col-xs-6" style="padding-left:5px;">

                                                        <input type="text" class="form-control login" id="exampleInputPassword1" name="lname" placeholder="Last Name" value="<?php echo $user->Lastname; ?>">
                                                        <div style="color:#FF0000;display:none;" id="lname"> Enter Last name</div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <input type="text" class="form-control login" id="exampleInputEmail1" name="username" placeholder="Choose your Username" value="<?php echo $user->username; ?>">
                                                        <div style="color:#FF0000;display:none;" id="username" >Enter Username</div>
                                                    </div>
                                                </div>


                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <input type="email" class="form-control login" name="email" id="email_address" placeholder="Choose your Email" value="<?php echo $user->email; ?>">
                                                        <p id="error" style="display:none;color:red;">Wrong email</p>
                                                        <div style="color:#FF0000;display:none;" id="email" > Enter Email Address</div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-4 col-xs-4" style="padding-right:5px;">

                                                        <select class="form-control selcto" name="month">

                                                            <option <?php if ($classes[1] == '') {
                                                echo "selected";
                                            } ?> value="">Month</option>
                                                            <option <?php if ($classes[1] == '01') {
                                                echo "selected";
                                            } ?> value="01">Jan</option>
                                                            <option <?php if ($classes[1] == '02') {
                                                echo "selected";
                                            } ?> value="02">Feb</option>
                                                            <option <?php if ($classes[1] == '03') {
                                                echo "selected";
                                            } ?> value="03">March</option>
                                                            <option <?php if ($classes[1] == '04') {
                                                echo "selected";
                                            } ?> value="04">April</option>
                                                            <option <?php if ($classes[1] == '05') {
                                                echo "selected";
                                            } ?> value="05">May</option>
                                                            <option <?php if ($classes[1] == '06') {
                                                echo "selected";
                                            } ?> value="06">June</option> 
                                                            <option <?php if ($classes[1] == '07') {
                                                echo "selected";
                                            } ?> value="07">July</option>
                                                            <option <?php if ($classes[1] == '08') {
                                                echo "selected";
                                            } ?> value="08">Aug</option>  
                                                            <option <?php if ($classes[1] == '09') {
                                                echo "selected";
                                            } ?> value="09">Sep</option>
                                                            <option <?php if ($classes[1] == '10') {
                                                echo "selected";
                                            } ?> value="10">Oct</option>  
                                                            <option <?php if ($classes[1] == '11') {
                                                echo "selected";
                                            } ?> value="11">Nov</option>
                                                            <option <?php if ($classes[1] == '12') {
                                                echo "selected";
                                            } ?> value="12">Dec</option>

                                                        </select>
                                                        <div style="color:#FF0000;display:none;" id="month" ><?php if ($classes[1] == '') {
                                                echo "Enter Month";
                                            } else {
                                                echo $classes[1];
                                            } ?></div>
                                                    </div>
                                                    <div class="form-group col-md-4 col-xs-4" style="padding:0px 5px;">
                                                        <input type="text" name="day" class="form-control login" id="exampleInputEmail1" placeholder="Day" maxlength="2" value="<?php if ($classes[2] == 0) {
                                                echo "Day";
                                            } else {
                                                echo $classes[2];
                                            } ?> ">
                                                        <div style="color:#FF0000;display:none;" id="day" >Enter Day</div>
                                                    </div>
                                                    <div class="form-group col-md-4 col-xs-4" style="padding-left:5px;">
                                                        <input type="text" name="year" class="form-control login" id="exampleInputEmail1" placeholder="Year" maxlength="4" value="<?php if ($classes[0] == 0) {
                                                echo "Year";
                                            } else {
                                                echo $classes[0];
                                            } ?>">
                                                        <div style="color:#FF0000;display:none;" id="year" >Enter Year</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group  col-md-12" >
                                                        <select class="form-control selcto" name="gender">
                                                            <option value="">Gender</option>
                                                            <option  <?php if ($user->gender == 'female') {
                                                echo "selected";
                                            } ?> value="female">Female</option>
                                                            <option <?php if ($user->gender == 'male') {
                                                echo "selected";
                                            } ?> value="male">Male</option>
                                                            <option <?php if ($user->gender == 'other') {
                                                echo "selected";
                                            } ?> value="other">Other</option>
                                                            <option <?php if ($user->gender == 'undefine') {
                                                echo "selected";
                                            } ?> value="undefine">Rather not say</option>   
                                                        </select>
                                                        <div style="color:#FF0000;display:none;" id="gender" >Enter Gender</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group col-md-3 col-xs-3" style="padding-right:10px;">
                                                        <input type="text" class="form-control login" id="exampleInputEmail1" name="code" placeholder="Area code" onkeypress="return isNumber(event)" value="<?php echo $phn[0]; ?>">
                                                        <div style="color:#FF0000;display:none;" id="code" >Enter Area Code</div>
                                                    </div>

                                                    <div class="form-group col-md-9 col-xs-9" style="padding-left:0px;">
                                                        <input type="text" class="form-control login" id="exampleInputEmail1" name="mobile" maxlength="10" placeholder="Enter your mobile no" onkeypress="return isNumber(event)" value="<?php echo $ph; ?>">
                                                        <div style="color:#FF0000;display:none;" id="mobile" >Enter mobile </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group  col-md-12" >
                                                        <label style="font-size:14px;font-weight: normal;">How did you get interested or involved with these skill sets?</label>

                                                        <textarea onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your answer..'" style="resize:none" placeholder="Your answer.." class="form-control" cols="80" rows="4" name="history"><?php if ($user->history != '') {
                                                echo $user->history;
                                            } ?></textarea>


                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group  col-md-12" >
                                                        <label style="font-size:14px;font-weight: normal;">Tell us more about yourself!</label>
                                                        <textarea onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your answer..'" style="resize:none" placeholder="Your answer.." class="form-control" cols="80" rows="4" name="bio"><?php if ($user->bio != '') {
                                                echo $user->bio;
                                            } ?></textarea>


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group  col-md-12" >
                                                        <label style="font-size:14px;font-weight: normal;">What is your personal approach to your craft, how do you do it differently than other experts</label>
                                                        <textarea onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your answer..'" style="resize:none" placeholder="Your answer.." class="form-control" cols="80" rows="4" name="strategy"><?php if ($user->strategy != '') {
                                                echo $user->strategy;
                                            } ?></textarea>


                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group  col-md-12" >
                                                        <label style="font-size:14px;font-weight: normal;">What motivates you to be great at what you do, and to teach others?</label>
                                                        <textarea onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your answer..'" style="resize:none" placeholder="Your answer.." class="form-control" cols="80" rows="4" name="motivation"><?php if ($user->motivation != '') {
                                                echo $user->motivation;
                                            } ?></textarea>
                                                    </div>


                                                </div>
                                                <div class="row">
                                                    <div class="form-group  col-md-12" >
                                                        <label style="font-size:14px;font-weight: normal;">What do you want your legacy to be?</label>
                                                        <textarea onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your answer..'" style="resize:none" placeholder="Your answer.." class="form-control" cols="80" rows="4" name="philosophy"><?php if ($user->philosophy != '') {
                                                echo $user->philosophy;
                                            } ?></textarea>


                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="form-group col-md-12">
                                                        <select class="form-control selcto" name="location" id="country" placeholder="Select Country">
                                                            <option value="">Country</option>
                                                            <option value="Aaland Islands">Aaland Islands</option>
                                                            <option value="Afghanistan">Afghanistan</option>
                                                            <option value="Albania">Albania</option>
                                                            <option value="Algeria">Algeria</option>
                                                            <option value="American Samoa">American Samoa</option>
                                                            <option value="Andorra">Andorra</option>
                                                            <option value="Angola">Angola</option>
                                                            <option value="Anguilla">Anguilla</option>
                                                            <option value="Antarctica">Antarctica</option>
                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                            <option value="Argentina">Argentina</option>
                                                            <option value="Armenia">Armenia</option>
                                                            <option value="Aruba">Aruba</option>
                                                            <option value="Australia">Australia</option>
                                                            <option value="Austria">Austria</option>
                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                            <option value="Bahamas">Bahamas</option>
                                                            <option value="Bahrain">Bahrain</option>
                                                            <option value="Bangladesh">Bangladesh</option>
                                                            <option value="Barbados">Barbados</option>
                                                            <option value="Belarus">Belarus</option>
                                                            <option value="Belgium">Belgium</option>
                                                            <option value="Belize">Belize</option>
                                                            <option value="Benin">Benin</option>
                                                            <option value="Bermuda">Bermuda</option>
                                                            <option value="Bhutan">Bhutan</option>
                                                            <option value="Bolivia">Bolivia</option>
                                                            <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                            <option value="Botswana">Botswana</option>
                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                            <option value="Brazil">Brazil</option>
                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                            <option value="Bulgaria">Bulgaria</option>
                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                            <option value="Burundi">Burundi</option>
                                                            <option value="Cambodia">Cambodia</option>
                                                            <option value="Cameroon">Cameroon</option>
                                                            <option value="Canada">Canada</option>
                                                            <option value="Canary Islands">Canary Islands</option>
                                                            <option value="Cape Verde">Cape Verde</option>
                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                            <option value="Central African Republic">Central African Republic</option>
                                                            <option value="Chad">Chad</option>
                                                            <option value="Chile">Chile</option>
                                                            <option value="China">China</option>
                                                            <option value="Christmas Island">Christmas Island</option>
                                                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                            <option value="Colombia">Colombia</option>
                                                            <option value="Comoros">Comoros</option>
                                                            <option value="Congo">Congo</option>
                                                            <option value="Cook Islands">Cook Islands</option>
                                                            <option value="Costa Rica">Costa Rica</option>
                                                            <option value="Cote D'Ivoire">Cote D'Ivoire</option>
                                                            <option value="Croatia">Croatia</option>
                                                            <option value="Cuba">Cuba</option>
                                                            <option value="Curacao">Curacao</option>
                                                            <option value="Cyprus">Cyprus</option>
                                                            <option value="Czech Republic">Czech Republic</option>
                                                            <option value="Democratic Republic of Congo">Democratic Republic of Congo</option>
                                                            <option value="Denmark">Denmark</option>
                                                            <option value="Djibouti">Djibouti</option>
                                                            <option value="Dominica">Dominica</option>
                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                            <option value="East Timor">East Timor</option>
                                                            <option value="Ecuador">Ecuador</option>
                                                            <option value="Egypt">Egypt</option>
                                                            <option value="El Salvador">El Salvador</option>
                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                            <option value="Eritrea">Eritrea</option>
                                                            <option value="Estonia">Estonia</option>
                                                            <option value="Ethiopia">Ethiopia</option>
                                                            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                            <option value="Fiji">Fiji</option>
                                                            <option value="Finland">Finland</option>
                                                            <option value="France, skypolitan">France, skypolitan</option>
                                                            <option value="French Guiana">French Guiana</option>
                                                            <option value="French Polynesia">French Polynesia</option>
                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                            <option value="FYROM">FYROM</option>
                                                            <option value="Gabon">Gabon</option>
                                                            <option value="Gambia">Gambia</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Germany">Germany</option>
                                                            <option value="Ghana">Ghana</option>
                                                            <option value="Gibraltar">Gibraltar</option>
                                                            <option value="Greece">Greece</option>
                                                            <option value="Greenland">Greenland</option>
                                                            <option value="Grenada">Grenada</option>
                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                            <option value="Guam">Guam</option>
                                                            <option value="Guatemala">Guatemala</option>
                                                            <option value="Guernsey">Guernsey</option>
                                                            <option value="Guinea">Guinea</option>
                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                            <option value="Guyana">Guyana</option>
                                                            <option value="Haiti">Haiti</option>
                                                            <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                                                            <option value="Honduras">Honduras</option>
                                                            <option value="Hong Kong">Hong Kong</option>
                                                            <option value="Hungary">Hungary</option>
                                                            <option value="Iceland">Iceland</option>
                                                            <option value="India">India</option>
                                                            <option value="Indonesia">Indonesia</option>
                                                            <option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
                                                            <option value="Iraq">Iraq</option>
                                                            <option value="Ireland">Ireland</option>
                                                            <option value="Israel">Israel</option>
                                                            <option value="Italy">Italy</option>
                                                            <option value="Jamaica">Jamaica</option>
                                                            <option value="Japan">Japan</option>
                                                            <option value="Jersey">Jersey</option>
                                                            <option value="Jordan">Jordan</option>
                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                            <option value="Kenya">Kenya</option>
                                                            <option value="Kiribati">Kiribati</option>
                                                            <option value="Korea, Republic of">Korea, Republic of</option>
                                                            <option value="Kuwait">Kuwait</option>
                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                            <option value="Latvia">Latvia</option>
                                                            <option value="Lebanon">Lebanon</option>
                                                            <option value="Lesotho">Lesotho</option>
                                                            <option value="Liberia">Liberia</option>
                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                            <option value="Lithuania">Lithuania</option>
                                                            <option value="Luxembourg">Luxembourg</option>
                                                            <option value="Macau">Macau</option>
                                                            <option value="Madagascar">Madagascar</option>
                                                            <option value="Malawi">Malawi</option>
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option value="Maldives">Maldives</option>
                                                            <option value="Mali">Mali</option>
                                                            <option value="Malta">Malta</option>
                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                            <option value="Martinique">Martinique</option>
                                                            <option value="Mauritania">Mauritania</option>
                                                            <option value="Mauritius">Mauritius</option>
                                                            <option value="Mayotte">Mayotte</option>
                                                            <option value="Mexico">Mexico</option>
                                                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                            <option value="Monaco">Monaco</option>
                                                            <option value="Mongolia">Mongolia</option>
                                                            <option value="Montenegro">Montenegro</option>
                                                            <option value="Montserrat">Montserrat</option>
                                                            <option value="Morocco">Morocco</option>
                                                            <option value="Mozambique">Mozambique</option>
                                                            <option value="Myanmar">Myanmar</option>
                                                            <option value="Namibia">Namibia</option>
                                                            <option value="Nauru">Nauru</option>
                                                            <option value="Nepal">Nepal</option>
                                                            <option value="Netherlands">Netherlands</option>
                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                            <option value="New Caledonia">New Caledonia</option>
                                                            <option value="New Zealand">New Zealand</option>
                                                            <option value="Nicaragua">Nicaragua</option>
                                                            <option value="Niger">Niger</option>
                                                            <option value="Nigeria">Nigeria</option>
                                                            <option value="Niue">Niue</option>
                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                            <option value="North Korea">North Korea</option>
                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                            <option value="Norway">Norway</option>
                                                            <option value="Oman">Oman</option>
                                                            <option value="Pakistan">Pakistan</option>
                                                            <option value="Palau">Palau</option>
                                                            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                            <option value="Panama">Panama</option>
                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                            <option value="Paraguay">Paraguay</option>
                                                            <option value="Peru">Peru</option>
                                                            <option value="Philippines">Philippines</option>
                                                            <option value="Pitcairn">Pitcairn</option>
                                                            <option value="Poland">Poland</option>
                                                            <option value="Portugal">Portugal</option>
                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                            <option value="Qatar">Qatar</option>
                                                            <option value="Reunion">Reunion</option>
                                                            <option value="Romania">Romania</option>
                                                            <option value="Russian Federation">Russian Federation</option>
                                                            <option value="Rwanda">Rwanda</option>
                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                            <option value="Saint Lucia">Saint Lucia</option>
                                                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                                            <option value="Samoa">Samoa</option>
                                                            <option value="San Marino">San Marino</option>
                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                            <option value="Senegal">Senegal</option>
                                                            <option value="Serbia">Serbia</option>
                                                            <option value="Seychelles">Seychelles</option>
                                                            <option value="Sierra Leone">Sierra Leone</option>
                                                            <option value="Singapore">Singapore</option>
                                                            <option value="Slovak Republic">Slovak Republic</option>
                                                            <option value="Slovenia">Slovenia</option>
                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                            <option value="Somalia">Somalia</option>
                                                            <option value="South Africa">South Africa</option>
                                                            <option value="South Georgia South Sandwich Islands">South Georgia &amp; South Sandwich Islands</option>
                                                            <option value="South Sudan">South Sudan</option>
                                                            <option value="Spain">Spain</option>
                                                            <option value="Sri Lanka">Sri Lanka</option>
                                                            <option value="St. Barthelemy">St. Barthelemy</option>
                                                            <option value="St. Helena">St. Helena</option>
                                                            <option value="St. Martin (French part)">St. Martin (French part)</option>
                                                            <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                                                            <option value="Sudan">Sudan</option>
                                                            <option value="Suriname">Suriname</option>
                                                            <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                                                            <option value="Swaziland">Swaziland</option>
                                                            <option value="Sweden">Sweden</option>
                                                            <option value="Switzerland">Switzerland</option>
                                                            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                            <option value="Taiwan">Taiwan</option>
                                                            <option value="Tajikistan">Tajikistan</option>
                                                            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                            <option value="Thailand">Thailand</option>
                                                            <option value="Togo">Togo</option>
                                                            <option value="Tokelau">Tokelau</option>
                                                            <option value="Tonga">Tonga</option>
                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                            <option value="Tunisia">Tunisia</option>
                                                            <option value="Turkey">Turkey</option>
                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                            <option value="Tuvalu">Tuvalu</option>
                                                            <option value="Uganda">Uganda</option>
                                                            <option value="Ukraine">Ukraine</option>
                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                            <option value="United Kingdom">United Kingdom</option>
                                                            <option value="United States">United States</option>
                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                            <option value="Uruguay">Uruguay</option>
                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                            <option value="Vanuatu">Vanuatu</option>
                                                            <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                                                            <option value="Venezuela">Venezuela</option>
                                                            <option value="Viet Nam">Viet Nam</option>
                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                            <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                                            <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                                            <option value="Western Sahara">Western Sahara</option>
                                                            <option value="Yemen">Yemen</option>
                                                            <option value="Zambia">Zambia</option>
                                                            <option value="Zimbabwe">Zimbabwe</option>

                                                        </select>
                                                        <div style="color:#FF0000;display:none;" id="location" >Select Location</div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="form-group  col-md-12">
                                                        <div class="form-group col-md-3">
                                                            <img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" width="80px;" height="80px;">
                                                        </div>
                                                        <div class="form-group col-md-9">
                                                            <input type="file" name="image1" >
                                                        </div>
                                                    </div>

                                                </div>
                                                <input type="hidden" name="cntry" id="cntry" value="<?php echo $user->location; ?>">


                                                <p style="text-align:right"><button type="submit" class="btn dashbut">Save</button></p>
                                            </form>


                                        </div>

                                    </div>

                                </div>
<?php } ?>

                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <!--//footer--> 
        </div>
        <!-- Classie --> 
        <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
        <script>
                   var menuLeft = document.getElementById('cbp-spmenu-s1'),
                           showLeftPush = document.getElementById('showLeftPush'),
                           body = document.body;

                   showLeftPush.onclick = function () {
                       classie.toggle(this, 'active');
                       classie.toggle(body, 'cbp-spmenu-push-toright');
                       classie.toggle(menuLeft, 'cbp-spmenu-open');
                       disableOther('showLeftPush');
                   };

                   function disableOther(button) {
                       if (button !== 'showLeftPush') {
                           classie.toggle(showLeftPush, 'disabled');
                       }
                   }
        </script> 
        <!--scrolling js--> 
        <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
        <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/fileupload.js"></script>
        <!--//scrolling js--> 
        <!-- Bootstrap Core JavaScript --> 
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 
        <script>
                   $(function () {
                       $('#menu').metisMenu({
                           toggle: false // disable the auto collapse. Default: true.
                       });
                   });
        </script>
        <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/sidebar-menu.js"></script>
        <script>
                 $.sidebarMenu($('.sidebar-menu'))
        </script>

        <!-- Include all compiled plugins (below), or include individual files as needed -->


        <script>
            function validation() {

                var fname = document.form.fname.value;
                var lname = document.form.lname.value;
                var femail = document.form.email.value;

                var username = document.form.username.value;
                var day = document.form.day.value;
                var month = document.form.month.value;
                var year = document.form.year.value;
                var location = document.form.location.value;
                var mobile = document.form.mobile.value;
                var gender = document.form.gender.value;

                if (fname == "")
                {

                    document.getElementById('fname').style.display = 'block';
                    setTimeout(function () {
                        $('#fname').fadeOut('fast');
                    }, 3000);
                    return false;
                }

                if (lname == "")
                {
                    document.getElementById('lname').style.display = 'block';
                    setTimeout(function () {
                        $('#lname').fadeOut('fast');
                    }, 3000);
                    return false;
                }
                if (username == "")
                {
                    document.getElementById('username').style.display = 'block';
                    setTimeout(function () {
                        $('#username').fadeOut('fast');
                    }, 3000);
                    return false;
                }
                if (femail == "")
                {
                    document.getElementById('email').style.display = 'block';
                    setTimeout(function () {
                        $('#email').fadeOut('fast');
                    }, 3000);
                    return false;
                }

//                if (month == "")
//                {
//                    document.getElementById('month').style.display = 'block';
//                    setTimeout(function () {
//                        $('#month').fadeOut('fast');
//                    }, 3000);
//                    return false;
//                }
//                if (day == "")
//                {
//                    document.getElementById('day').style.display = 'block';
//                    setTimeout(function () {
//                        $('#day').fadeOut('fast');
//                    }, 3000);
//                    return false;
//                }
//                if (year == "")
//                {
//                    document.getElementById('year').style.display = 'block';
//                    setTimeout(function () {
//                        $('#year').fadeOut('fast');
//                    }, 1000);
//                    return false;
//                }
                if (gender == "")
                {
                    document.getElementById('gender').style.display = 'block';
                    setTimeout(function () {
                        $('#month').fadeOut('fast');
                    }, 3000);
                    return false;
                }
                if (mobile == "")
                {
                    document.getElementById('mobile').style.display = 'block';
                    setTimeout(function () {
                        $('#mobile').fadeOut('fast');
                    }, 3000);
                    return false;
                }
//                if (location == "")
//                {
//                    document.getElementById('location').style.display = 'block';
//                    setTimeout(function () {
//                        $('#location').fadeOut('fast');
//                    }, 3000);
//
//                    return false;
//                }



            }
        </script>
        <script>
            $('#email_address').on('change', function () {
                var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
                if (!re) {
                    $('#error').show();
                } else {
                    $('#error').hide();
                }
            })
        </script>

        <script>

            $(document).ready(function () {
                var ee = $('#cntry').val();
                $('#country').val(ee);
            });
        </script>
        <script type="text/javascript">
                                             function ajaxSearch()
        {
            var input_data = $('#input-31').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
        $('#chkval').click(function (e) {
                e.preventDefault();
                var input = document.getElementById("input-31").value;
                if (input == '') {
                    alert('Please Type Text For Search!');
                     e.preventDefault();
                }
                else{
                    document.getElementById('my_form').submit();
                }
            });
                              
        </script>
        <script>
            $(document).on("click", function (e) {
                if (!$("#suggestions").is(e.target)) {
                    $("#suggestions").hide();
                }
            });
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    document.getElementById("formerror").innerHTML = "Enter Only Numbers ";
                    return false;


                }
                return true;
            }


        </script>



    </body>
</html>