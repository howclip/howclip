<!DOCTYPE HTML>
<html>
<head>
<title>HowClip </title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-menu.css">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
            input[type=text] {
                width: 130px;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 10px;
                font-size: 14px;
                background-color: white;
                background-image: url('searchicon.png');
                background-position: 10px 10px; 
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            input[type=text]:focus {
                width: 100%;
                border-radius: 10px;
                outline:0 !important; 
            }

            .search-box input[type=text] {
                width: 50%;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 25px;
                font-size: 14px;
                background-color: white;
                background-image: url(https://www.w3schools.com/howto/searchicon.png);
                background-position: 10px 10px;
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            .search-box input[type=text]:focus {
                outline:0;
            }

            .searchbut
            {
                padding: 5px 15px;
                background: #ddd;
                color:#000;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
            }
            .searchbut:hover ,.searchbut:focus,.searchbut:active:focus{
                background: #ccc;
                color:#000;
                outline:0;
            }
            .advance{
                background: #ddd;
                color:#d60808;
                padding: 5px 15px;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
                font-weight:bold;
               
            }
            a.advance:visited
            {
                color:#d60808;
            }






        </style>
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
 
  <div class="sticky-header header-section ">
    <div class="header-left"> 
 
  <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
    <li><a href="<?php echo base_url()?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id')?>">My Videos</a></li>
	<li> <a href="<?php echo base_url()?>index.php/home/history/<?php echo $this->session->userdata('id');?>"> History </a> </li>
	<li> <a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>"> My Channel </a> </li>
	 
        <li class="divider" role="seperator"></li>
		  <li> <a href="#">SUBSCRIPTIONS </a> </li>
		  <?php foreach(($usersubscription->uploaderdetail) as $uploader) { ?>
		   <li> <a href="<?php echo base_url()?>index.php/home/subscriber/<?php echo $uploader->id;?>"> <?php echo $uploader->username; ?> </a> </li>
		   <?php } ?>
       
  </ul>
</div>
     <?php foreach($companydetail as $company) { if($company->company_logo != ""){ $cmplogo = base_url()."/Admin/uploads/$company->company_logo"; } else {$cmplogo = base_url()."/uploads/logo.png";  } ?>
      <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">
      
        <img src="<?php echo $cmplogo;?>" class="img-responsive">
       </a> </div>
    <?php } ?>
     
    <form class="input" method="post" id="my_form" action="<?php echo base_url() ?>index.php/welcome/getsearchdata">
                        <div class="search-box">
                            <input type="text" style="width:55%;" id="input-31" onKeyUp="ajaxSearch();" name="search_data" placeholder="Search..">

                            <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:55%;z-index:10000;">
                                <div id="autoSuggestionsList"></div>
                            </div>

                            <a href="javascript:void(0)" id="chkval"  class="btn searchbut"> Search  </a>
                            <a href="<?php echo base_url() ?>index.php/welcome/open_advanceSearch" class="btn advance"> Advanced Topic Search  </a>
                        </div>




                    </form>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
	
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/uploadpage" style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
            
           </li>
        <?php foreach($userDetail as $user) { $username = $user->username;  $userimg = $user->userLogo;  if($userimg== "") { $userimg = "user.png"; } }?>
      <!-- <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url();?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
       <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url();?>uploads/<?php echo $userimg;?>" class="img-responsive"></div> </a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3><?php echo ucwords($username);?></h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p><?php echo ucwords($this->session->userdata('email'));?></p>
                 
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/logout">Log Out</a> </div>
              </li>
            </ul>
           </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
    
   
    </div>
    
    
  </div>
 <div class="uploadwrap">
  <div class="container-fluid">
  <div class="row" style="padding:0px;">
  <div class="col-md-2" style="padding-right:0px;">
  <ul class="sidebar-menu">
      <li class="sidebar-header">HowClip</li>
      <li>
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
        </a>
       
      </li>
      <li>
        <a href="<?php echo base_url()?>index.php/home/videoDetail/<?php echo $this->session->userdata('id')?>">
          <i class="fa fa-files-o"></i>
          <span>Video Manager</span>
        </a>
        <ul class="sidebar-submenu" >
          <li><a href="<?php echo base_url()?>index.php/home/videoDetail/<?php echo $this->session->userdata('id')?>"><i class="fa fa-circle-o"></i> Videos</a></li>
          <li><a href="<?php echo base_url()?>index.php/home/showplaylist/<?php echo $this->session->userdata('id')?>"><i class="fa fa-circle-o"></i> Playlists</a></li>
         
        </ul>
      </li>
      
      <li>
        <a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>">
          <i class="fa fa-pie-chart"></i>
          <span>My Channel</span>
         
        </a>
     
      </li>
      
     
      <li>
        <a href="<?php echo base_url()?>index.php/home/userupdate/<?php echo $this->session->userdata('id')?>">
          <i class="fa fa-pie-chart"></i>
          <span>User Profile</span>
         
        </a>
     
      </li>
      <li>
        <a href="<?php echo base_url()?>index.php/home/changePass/<?php echo $this->session->userdata('id')?>">
          <i class="fa fa-laptop"></i>
          <span>Change Password</span>
       
        </a>
      
      </li>
      <li>
        <a href="<?php echo base_url()?>index.php/home/myorders/<?php echo $this->session->userdata('id')?>">
          <i class="fa fa-laptop"></i>
          <span>My Orders</span>
       
        </a>
      
      </li>
    </ul>
  </div>
  <?php 
  $total = 0;
  $totalssub = 0;
  foreach($userdata as $user) { //print_r($user); 
   $totalview = $user->views;
  $total +=$totalview;
  
 $currentvew = $user->latestview;$currentsub = $user->currsub;
  $comm = $user->comments; }?>
  <div class="col-md-10" style="padding:0px;">
  <div class="row">
  <div class="col-md-12">
  <div class="row">
  <div class="col-md-12">
  <div class="blankpage">
 
 
  <div class="row">
  <div class="col-md-8 col-xs-6 ">
  <div class="media">
  <div class="media-left">
    <a href="#">
	
      <img class="media-object profileimg" src="<?php echo base_url();?>uploads/<?php echo $userimg;?>" width="15px;" height="60px;" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading"><?php echo ucwords($this->session->userdata('name'));?></h4>
  </div>
</div>
</div>

<div class="col-md-4 col-xs-6">

  <div class="dashmen">
   <?php foreach($userdata['subscriber'] as $sub) {?><?php }?>
  <ul>
  <li><a href="#"> 
 <i><?php if($total == "") { echo "0"; } else { echo $total; }?> </i>
  Views </a></li>
    <li><a href="#"><i><?php if($sub == "") { echo "0"; } else { echo $sub;}?></i> Subscriber </a></li>
    
    <li> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <i class="fa fa-plus" aria-hidden="true"></i> Add widget</a>
            <ul class="dropdown-menu" style="left:initial; right:0;">
              <li><a href="#"> Add widgets to your dashboard    </a></li>
			  <li role="separator" class="divider"></li>
              <li><a href="#er" id="com">Comments </a></li>
              <li><a href="#er2" id="abc">Analytics</a></li>
            </ul>
           </li>
            </ul>
           </li>
      
      
      
  </ul>
  </div>
</div>
</div>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="row">
  <div class="col-md-6">
 
  
  <div class="row">
   
  <div class="col-md-12 anchorrel" id="er">
  <div class="blankpage" id="Commentdiv">

  <div class="row" >
  
  <div class="col-md-12">
  <h5>Comments</h5>
  <div class="dashmove">
  <ul>
 
  <li><a href="#" onClick="hidedivcom()">  <i class="fa fa-times" aria-hidden="true"></i></a> </li>
  </ul>
  </div><br>
  <?php  foreach($userdata['comment'] as $comm) {?>

<div style="margin:10px;"><div class="coomentterms"><p><i class="fa fa-user" aria-hidden="true"></i></p></div>
<div class="hint"> <?php echo $comm->user_name; ?><br>
    
    <?php $vname = $comm->comment;$length = strlen($comm->comment); if($length < 25){
			  $v = $vname;}else { $v = substr($vname,0,25); $v = $v."..."; }?>
    
    
<?php echo $v; ?><br>

On 
     <?php $vname2 = $comm->videoname;$length2 = strlen($comm->videoname); if($length2 < 20){
			  $v2 = $vname2;}else { $v2 = substr($vname2,0,20); $v2 = $v2."..."; }?>
    <?php echo $v2; ?><br>
 </div></div>
<?php }?>

</div>
</div>
  
 <div id="erp" style="display:none;" class="anchorrel"></div>
  
  </div>
  </div>
  </div>
  </div>
  <div class="col-md-6 eer2" style="padding-left:0px;" id="eer2">
  <div class="row">
  <div class="col-md-12">
    <div class="blankpage" id="analytics">
    <h5>Analytics Last 28 Days</h5>
    <div class="dashmove">
  <ul>
  <li><a href="#" id="erp2" onClick="hidediv()">  <i class="fa fa-times" aria-hidden="true"></i></a> </li>
  </ul>
  </div><br>
  <?php 
  $currsub = 0; 
   foreach($userdata as $user) { 
  $currentvew = $user->latestview;
  $currsub +=$currentvew;
  
 $currentsub = $user->currsub;
  $comm = $user->comments; }?>
  <?php  foreach($userdata['Currsub'] as $subsub) {  }?>
  <div class="analyticmen">
  <ul>
  <li>Watch time
  <?php if($userdata['watchtime'] == "") { echo "0"; } else { echo $userdata['watchtime']; } ?>  
  </li>
  <li>Views  <?php if( $currsub == "") { echo "0"; } else {  echo $currsub; }?> </li>
  <li> Subscriber  <?php if( $subsub == "") { echo "0"; }else { echo $subsub; }?> </li>
  </ul>
  </div>
  
    </div>
    </div>
    
    
  </div>
 
 
  </div>
  </div>
  
  
  
  </div>
  </div>
  </div>
  </div>
  <div class="footer">
    <div class="row">
      <div class="col-md-12">
        <div class="footmen">
          <ul>
            <li><a href="<?php echo base_url()?>index.php/home/about"> About </a></li>
            <li><a href="#"> Press </a></li>
            <li><a href="<?php echo base_url()?>index.php/home/terms"> Copyright </a></li>
            <li><a href="#"> Creators</a></li>
            <li><a href="#"> Advertise</a></li>
            <li><a href="#"> Developers</a></li>
            <li><a href="<?php echo base_url()?>">HowClip</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="minfootmen">
          <ul>
            <li><a href="<?php echo base_url()?>index.php/home/terms"> Terms</a></li>
            <li><a href="#"> Privacy</a></li>
            <li><a href="#"> Policy & Safety</a></li>
            <li><a href="#"> Send feedback</a></li>
            <li><a href="#"> Test new features</a></li>
            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<script src="<?php echo base_url();?>assets/js/fileupload.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
 <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/sidebar-menu.js"></script>
  <script>
    $.sidebarMenu($('.sidebar-menu'))
  </script>
  <script type="text/javascript">
    function ajaxSearch()
        {
            var input_data = $('#input-31').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
        $('#chkval').click(function (e) {
                e.preventDefault();
                var input = document.getElementById("input-31").value;
                if (input == '') {
                    alert('Please Type Text For Search!');
                     e.preventDefault();
                }
                else{
                    document.getElementById('my_form').submit();
                }
            });
</script>

<script>
    $(document).ready(function(e) {
        $("#com").click(function(e) {
        
            $("#Commentdiv").show();

        });
    });
    </script>
	<script>
    $(document).ready(function(e) {
        $('#abc').click(function(e) {
       
            $("#analytics").show();
           

        });
    });
    </script>
	<script>
   function hidedivcom()
   {
    $("#Commentdiv").hide();
   }
    </script>
	<script>
   function hidediv()
   {
    $("#analytics").hide();
   }
    </script>
<script>
  $(document).on("click", function(e){
    if( !$("#suggestions").is(e.target) ){ 
        $("#suggestions").hide();
    }
});
  </script>
</body>
</html>