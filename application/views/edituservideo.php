<!DOCTYPE HTML>
<html>
    <head>
        <title>HowClip</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/sidebar-menu.css">
        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
        <script>function fbs_click() {
                u = location.href;
                t = document.title;
                window.open('https://www.facebook.com/sharer/sharer.php?u=http%3A//13.59.106.5/index.php/welcome&p[video][0]=<?php echo base_url() ?>/index.php/home/showvideo?id=227');
                return false;
            }</script>

        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
 <style>
            input[type=text] {
                width: 130px;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 10px;
                font-size: 14px;
                background-color: white;
                background-image: url('searchicon.png');
                background-position: 10px 10px; 
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            input[type=text]:focus {
                width: 100%;
                border-radius: 10px;
                outline:0 !important; 
            }

            .search-box input[type=text] {
                width: 50%;
                box-sizing: border-box;
                border: 2px solid #ccc;
                border-radius: 25px;
                font-size: 14px;
                background-color: white;
                background-image: url(https://www.w3schools.com/howto/searchicon.png);
                background-position: 10px 10px;
                background-repeat: no-repeat;
                padding: 8px 20px 8px 40px;
                -webkit-transition: width 0.4s ease-in-out;
                transition: width 0.4s ease-in-out;
            }
            .search-box input[type=text]:focus {
                outline:0;
            }

            .searchbut
            {
                padding: 5px 15px;
                background: #ddd;
                color:#000;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
            }
            .searchbut:hover ,.searchbut:focus,.searchbut:active:focus{
                background: #ccc;
                color:#000;
                outline:0;
            }
            .advance{
                background: #ddd;
                color:#d60808;
                padding: 5px 15px;
                border-radius:8px;
                display:inline-block;
                font-size: 13px;
                font-weight:bold;
               
            }
            a.advance:visited
            {
                color:#d60808;
            }






        </style>

        <!--//Metis Menu -->
    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 
            <!--left-fixed -navigation-->

            <!--left-fixed -navigation--> 
            <!-- header-starts -->
            <div class="sticky-header header-section ">
                <div class="header-left"> 
                    <!--toggle button start-->
                 <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->

                    <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id') ?>">My Videos</a></li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>">History </a> </li>
                            <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"> My Channel </a> </li>
                            <li class="divider" role="seperator"></li>
                            <li> <a href="#">SUBSCRIPTIONS </a> </li>
                            <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"> <?php echo $uploader->username; ?> </a> </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php
                    foreach ($companydetail as $company) {
                        if ($company->company_logo != "") {
                            $cmplogo = base_url()."Admin/uploads/$company->company_logo";
                        } else {
                            $cmplogo = base_url()."uploads/logo.png";
                        }
                        ?><?php } ?>
                        <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">

                                <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                            </a> </div>
                    
                   <form class="input" method="post" id="my_form" action="<?php echo base_url() ?>index.php/welcome/getsearchdata">
                        <div class="search-box">
                            <input type="text" style="width:55%;" id="input-31" onKeyUp="ajaxSearch();" name="search_data" placeholder="Search..">

                            <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:55%;z-index:10000;">
                                <div id="autoSuggestionsList"></div>
                            </div>

                            <a href="javascript:void(0)" id="chkval"  class="btn searchbut"> Search  </a>
                            <a href="<?php echo base_url() ?>index.php/welcome/open_advanceSearch" class="btn advance"> Advanced Topic Search  </a>
                        </div>




                    </form>

                    <div class="clearfix"> </div>
                </div>
                <div class="header-right">
                    <div class="profile_details_left"><!--notifications of menu start -->
                        <ul class="nofitications-dropdown">
                            <li class="dropdown head-dpdn"> <a href="<?php echo base_url() ?>index.php/home/uploadpage" style="color:#333;" > <img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > </a>

                            </li>
                            <?php
                            foreach ($userDetail as $user) {
                                $username = $user->username;
                                $userimg = $user->userLogo;
                                if ($userimg == "") {
                                    $userimg = "user.png";
                                }
                            }
                            ?>
                       <!-- <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url(); ?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
                            <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                <ul class="dropdown-menu" style="left:initial; right:0;">
                                    <li>
                                        <div class="notification_header">
                                            <h3><?php echo $username; ?></h3>
                                        </div>
                                    </li>
                                    <li><a href="#">
                                            <div class="notification_desc">
                                                <p><?php echo $this->session->userdata('email'); ?></p>

                                            </div>
                                            <div class="clearfix"></div>
                                        </a></li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                    </li>
                                    <li>
                                        <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <div class="clearfix"> </div>
                    </div>


                </div>


            </div>
            <div class="uploadwrap">
                <div class="container-fluid">
                    <div class="row" style="padding:0px;">
                        <div class="col-md-2" style="padding-right:0px;">
                            <ul class="sidebar-menu">
                                <li class="sidebar-header">HowClip</li>
                                <li>
                                    <a href="#">
                                        <i class="fa fa-dashboard"></i> <span>Dashboard</span> 
                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-files-o"></i>
                                        <span>Video Manager</span>
                                    </a>
                                    <ul class="sidebar-submenu" >
                                        <li><a href="<?php echo base_url() ?>index.php/home/videoDetail/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Videos</a></li>
                                        <li><a href="<?php echo base_url() ?>index.php/home/showplaylist/<?php echo $this->session->userdata('id') ?>"><i class="fa fa-circle-o"></i> Playlists</a></li>

                                    </ul>
                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>My Channel</span>

                                    </a>

                                </li>



                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/userupdate/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-pie-chart"></i>
                                        <span>User Profile</span>

                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/changePass/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>Change Password</span>

                                    </a>

                                </li>
                                <li>
                                    <a href="<?php echo base_url() ?>index.php/home/myorders/<?php echo $this->session->userdata('id') ?>">
                                        <i class="fa fa-laptop"></i>
                                        <span>My Orders</span>

                                    </a>

                                </li>
                            </ul>
                        </div>
                        <div class="col-md-10">
                            <div class="blankpage">
                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs myeditab" role="tablist">
                                        <li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab"><i class="fa fa-pencil" aria-hidden="true"></i> Info & Setting</a></li>
                                       <!-- <li role="presentation"><a href="#enhancement" aria-controls="enhancement" role="tab" data-toggle="tab"><i class="fa fa-magic" aria-hidden="true"></i>
                                     Enhancement</a></li>
                                        <li role="presentation"><a href="#audio" aria-controls="audio" role="tab" data-toggle="tab"><i class="fa fa-music" aria-hidden="true"></i>
                                     Audio</a></li>
                                        <li role="presentation"><a href="#screen" aria-controls="screen" role="tab" data-toggle="tab">End Screen & Annotation</a></li>-->
                                    </ul>

                                    <!-- Tab panes -->
                                    <?php foreach ($videodata as $video) { ?>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="info">
                                                <div class="tabheadmen">
                                                    <div class="row">
                                                        <div class="col-md-8">
                                                            <h4><?php echo $video->videoname; ?></h4>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <!--<a class="btn cancel" href="#" role="button">Cancel</a><a class="btn publish1" href="#" role="button">Publish</a>-->
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <video width="100%" height="400" controls>
                                                            <source src="<?php echo base_url(); ?>uploads/live/<?php echo $video->name; ?>" type="video/mp4">

                                                        </video>


                                                    </div>

                                                </div>


                                                <!--<div class="row">
                                                <div class="col-md-8">
                                               <video width="100%" height="300" controls>
                                              <source src="<?php echo base_url(); ?>uploads/live/<?php echo $video->name; ?>" type="video/mp4">
                                             
                                               </video>
                                                
                                                
                                                </div>
                                                <div class="col-md-4">
                                                <div class="videoinstruc">
                                                <div class="row">
                                                <div class="col-md-6">
                                               <h6> Channel</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php echo $this->session->userdata('name'); ?></p>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                               <h6> Time</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php echo $video->time; ?></p>
                                                </div>
                                                </div>
                                                     <div class="row">
                                                <div class="col-md-6">
                                               <h6> Date</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php echo $video->Date; ?></p>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                               <h6> Duration</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php echo $video->video_duration; ?></p>
                                                </div>
                                                </div>
                                               
                                                <div class="row">
                                                <div class="col-md-6">
                                               <h6> View</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php
                                                if ($view == "") {
                                                    echo "0";
                                                } else {
                                                    echo $view;
                                                }
                                                ?></p>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                               <h6> Likes</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php
                                                if ($likes == "") {
                                                    echo "0";
                                                } else {
                                                    echo $likes;
                                                }
                                                ?></p>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                               <h6>Unlikes</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php
                                                if ($Unlikes == "") {
                                                    echo "0";
                                                } else {
                                                    echo $Unlikes;
                                                }
                                                ?></p>
                                                </div>
                                                </div>
                                                <div class="row">
                                                <div class="col-md-6">
                                               <h6>Comments</h6>
                                                </div>
                                                <div class="col-md-6">
                                                <p><?php
                                                if ($comm == "") {
                                                    echo "0";
                                                } else {
                                                    echo $comm;
                                                }
                                                ?></p>
                                                </div>
                                                </div>
                                                </div>
                                                </div>
                                                
                                               
                                                </div>-->
                                                <br>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <ul class="nav nav-tabs uploadtab" role="tablist">
                                                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>

                                                        </ul>

                                                        <div class="tab-content uplaodtabontent">
                                                            <div role="tabpanel" class="tab-pane active" id="home">
                                                                <div class="row">
                                                                    <div class="col-md-6" style="padding:0px;">
                                                                        <form class="logform" name="search-form" method="post" id="search-form" >
                                                                            <div style="padding: 0 5px;">
                                                                                <div style="color:#FF0000;text-align:center;" id="formerror"></div>

                                                                                <div style="color:#FF0000;text-align:center;display:none;" id="clipnameerror"> Enter Clip Name</div>
                                                                                <div style="color:#FF0000;text-align:center;display:none;" id="clipdescrror">Enter Clip Description</div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <div id="formerror"></div>
                                                                                <input type="text" id="name" class="form-control" name="name" placeholder="Name" value="<?php echo $video->videoname; ?>">
                                                                                <input type="hidden" class="form-control"  value="<?php echo $video->id; ?>" id="video_id" name="video_id" />
                                                                                <input type="hidden" class="form-control"  value="<?php echo $this->session->userdata('id'); ?>" id="user_id"  />
                                                                            </div>

                                                                    </div>

                                                                    <div class="col-md-6" style="padding:0px;" >
                                                                        <div class="form-group">

                                                                            <textarea class="form-control" style="width:87%;resize:none" rows="2" name="description" id="desc"  placeholder="Description" ><?php echo $video->description; ?></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <label>Video Category:</label> <?php echo $video->video_category; ?> (<?php echo ucwords($video->categoryStatus); ?> )
                                                                    </div>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">

                                                                        <div class="menuselecttabone">
                                                                            <p style="font-size: 12px;font-weight:600;">Choose Category</p>
                                                                            <div class="selectmenu">

                                                                                <ul>
                                                                                    <?php foreach ($catmenu as $cat) { ?>
                                                                                        <li  style="margin-top:5px;class:active;" > 
                                                                                            <a href="javascript:void(0)"><?php echo $cat[title]; ?><input type="radio" class="chkradio" name="category" id="<?php echo $cat[id]; ?>" style="float:right;" value="<?php echo $cat[id]; ?>" onChange="getsubcat('<?php echo $cat[id]; ?>', this.id)"></a>
                                                                                        </li>
                                                                                    <?php } ?>

                                                                                </ul>


                                                                            </div>
                                                                        </div>
                                                                        <div class="menuselecttabone">
                                                                            <p style="font-size: 12px;font-weight:600;" >Choose Subcategory</p>
                                                                            <div class="selectmenu">

                                                                                <div id="autoresp" class="autoresp">
                                                                                    <ul class="fisrt">

                                                                                    </ul>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="menuselecttabone">
                                                                            <p style="font-size: 12px;font-weight:600;">Choose Subcategory</p>
                                                                            <div class="selectmenu">

                                                                                <div id="autoresp2" class="autoresp2">
                                                                                    <ul class="second">

                                                                                    </ul>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                        <div class="menuselecttabone">
                                                                            <p style="font-size: 12px;font-weight:600;">Choose Subcategory</p>
                                                                            <div class="selectmenu">

                                                                                <div id="autoresp3" class="autoresp3">
                                                                                    <ul class="third">

                                                                                    </ul>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                        <div class="menuselecttabone">
                                                                            <p style="font-size: 12px;font-weight:600;">Choose Subcategory</p>
                                                                            <div class="selectmenu">

                                                                                <div id="autoresp4" class="autoresp4">
                                                                                    <ul class="fourth">

                                                                                    </ul>
                                                                                </div>

                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <input type="hidden" name="cat" id="cat" value="<?php echo $video->video_category; ?>">




                                                                <div class="row" style="margin-top:20px;">
                                                                    <div class="col-md-12" style="width:97%;">
                                                                        <div class="form-group"  >
                                                                            <select class="form-control selectuplo" name="privacy" id="privacy"> 
                                                                                <option <?php
                                                                                if ($video->privacy == 'public') {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> value="public">Public</option>
                                                                                <option <?php
                                                                                if ($video->privacy == 'unlisted') {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> value="unlisted">Unlisted</option>
                                                                                <option <?php
                                                                                if ($video->privacy == 'private') {
                                                                                    echo "selected";
                                                                                }
                                                                                ?> value="private">Private</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-md-12">

                                                                        <div class="checkbox" style="float:right;margin-top: 0px;">


                                                                        </div>
                                                                    </div>
                                                                </div>



                                                                <a class="btn cancelform" href="#" role="button" >Cancel</a>

                                                                <input type="button" name="submit" value="Post" onclick="editvideo()" class="btn publishform"/>

                                                                </form>


                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>





                                            </div>

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="enhancement">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="audio">

                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="screen">

                                        </div>
                                    </div>
                                <?php } ?>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
            <div class="row">
                <div class="col-md-12">
                    <div class="footmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                            <li><a href="javascript:void(0);"> Press </a></li>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                            <li><a href="javascript:void(0);"> Creators</a></li>
                            <li><a href="javascript:void(0);"> Advertise</a></li>
                            <li><a href="javascript:void(0);"> Developers</a></li>
                            <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="minfootmen">
                        <ul>
                            <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                            <li><a href="javascript:void(0);"> Privacy</a></li>
                            <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                            <li><a href="javascript:void(0);"> Send feedback</a></li>
                            <li><a href="javascript:void(0);"> Test new features</a></li>
                            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--//footer--> 
    </div>
    <!-- Classie --> 
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
    <script>
                                                                    var menuLeft = document.getElementById('cbp-spmenu-s1'),
                                                                            showLeftPush = document.getElementById('showLeftPush'),
                                                                            body = document.body;

                                                                    showLeftPush.onclick = function () {
                                                                        classie.toggle(this, 'active');
                                                                        classie.toggle(body, 'cbp-spmenu-push-toright');
                                                                        classie.toggle(menuLeft, 'cbp-spmenu-open');
                                                                        disableOther('showLeftPush');
                                                                    };

                                                                    function disableOther(button) {
                                                                        if (button !== 'showLeftPush') {
                                                                            classie.toggle(showLeftPush, 'disabled');
                                                                        }
                                                                    }
    </script> 
    <!--scrolling js--> 
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/fileupload.js"></script>
    <!--//scrolling js--> 
    <!-- Bootstrap Core JavaScript --> 
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/metisMenu.js"></script> 
    <script>
                                                                    $(function () {
                                                                        $('#menu').metisMenu({
                                                                            toggle: false // disable the auto collapse. Default: true.
                                                                        });
                                                                    });
    </script>
    <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/sidebar-menu.js"></script>
    <script>
                                                                    $.sidebarMenu($('.sidebar-menu'))
    </script>
    <script>

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
    <script type="text/javascript">

        function ajaxSearch()
        {
            var input_data = $('#input-31').val();

            if (input_data.length === 0)
            {
                $('#suggestions').hide();
            } else
            {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                    data: post_data,
                    success: function (data) {
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });


            }

        }
        $('#chkval').click(function (e) {
                e.preventDefault();
                var input = document.getElementById("input-31").value;
                if (input == '') {
                    alert('Please Type Text For Search!');
                     e.preventDefault();
                }
                else{
                    document.getElementById('my_form').submit();
                }
            });
    </script>
    <script>
        function updatethumb(x)
        {

            var vid = document.getElementById('video_id').value;

            var post_data = {
                'img': x,
                'video': vid,
            };
            $.ajax({
                type: "POST",
                url: "<?php echo base_url() ?>index.php/home/updatethumb",
                data: post_data,
                success: function (response) {
                    console.log(response);

                }
            });
        }
    </script>
    <script>

        $(document).ready(function () {
            var ee = $('#cat').val();
            $('#category').val(ee);
        });
    </script>
    <script>
        $(document).on("click", function (e) {
            if (!$("#suggestions").is(e.target)) {
                $("#suggestions").hide();
            }
        });
    </script>
    <script>
        function getsubcat(x, e)
        {
            var abc = $("#" + e).prop("checked");
            if (abc)
            {

                var x = x;
                var post_data = {
                    'search': x,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat",
                    data: post_data,
                    success: function (data) {
                        console.log(data);
                        $(".fisrt").empty();
                        $(".second").empty();
                        $(".third").empty();
                        $(".fourth").empty();
                        if (data.length > 0) {

                            var json = $.parseJSON(data);

                            $.each(json, function (k, v) {

                                $("#autoresp").append("<ul style='list-style:none;' class='fisrt'><li><a href='javascript:void(0)'>" + v.category_title + "<input name='category' id='" + v.id + "' type='radio' onChange='getsubcat2(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp").append("<ul style='list-style:none;' class='fisrt' id='addedadded'><li><a href='javascript:void(0)'><input type='text' style='width:100%;position:relative;margin-left:10px;' name='addedcat' id='adcat' placeholder='Add Subcategory'></a></ul>");


                        }

                    }
                });
            } else
            {

            }
        }
        function getsubcat2(y, e)
        {

            var a1 = $("#" + e).prop("checked");

            if (a1)
            {
                var post_data = {
                    'search': y,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat2",
                    data: post_data,
                    success: function (response) {
                        $(".second").empty();
                        $(".third").empty();
                        $(".fourth").empty();
                        $("#addedadded").empty();

                        if (response.length > 0) {

                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp2").append("<ul style='list-style:none;' class='second'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat3(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp2").append("<ul style='list-style:none;' class='second' id='addedcat2'><li><a href='javascript:void(0)'><input type='text' style='width:100%;position:relative;margin-left:10px;' name='addedcat' id='adcat'  placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            } else
            {

            }

        }

        function getsubcat3(z, e)
        {

            var a2 = $("#" + e).prop("checked");
            if (a2)
            {
                var post_data = {
                    'search': z,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat3",
                    data: post_data,
                    success: function (response) {
                        $(".third").empty();

                        $(".fourth").empty();
                        $("#addedcat2").empty();

                        if (response.length > 0) {
                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp3").append("<ul style='list-style:none;' class='third'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat4(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp3").append("<ul style='list-style:none;' class='third' id='addedcat3'><li><a href='javascript:void(0)'><input type='text' name='addedcat' id='adcat' style='width:100%;position:relative;margin-left:10px;' placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            }
        }

        function getsubcat4(z2, e)
        {
            var a4 = $("#" + e).prop("checked");
            if (a4)
            {
                var post_data = {
                    'search': z2,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat4",
                    data: post_data,
                    success: function (response) {
                        $(".fourth").empty();
                        $("#addedcat3").empty();
                        if (response.length > 0) {

                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp4").append("<ul style='list-style:none;' class='fourth'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat5(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp4").append("<ul style='list-style:none;' class='fourth' id='addedcat4'><li><a href='javascript:void(0)'><input type='text' name='addedcat' id='adcat' style='width:100%;position:relative;margin-left:10px;' placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            }
        }
        function getsubcat5()
        {
            $("#addedcat4").empty();
        }
    </script>

    <script>
        function editvideo()
        {
            var addcat = "";
            var video_id = document.getElementById('video_id').value;
            var name = document.getElementById('name').value;
            var description = document.getElementById('desc').value;
            var privacy = document.getElementById('privacy').value;


            var uid = document.getElementById('user_id').value;
            if (name == "")
            {

                document.getElementById('clipnameerror').style.display = "block";
                setTimeout(function () {
                    $('#clipnameerror').fadeOut('fast');
                }, 1000);
                return false;
            } else if (description == "")
            {
                document.getElementById('clipdescrror').style.display = "block";
                setTimeout(function () {
                    $('#clipdescrror').fadeOut('fast');
                }, 1000);
                return false;
            } else {

                var cat_name = $("input[name='category']:checked").val();
                element = document.getElementById('adcat');
                if (element != null) {
                    addcat = element.value;

                } else {

                    addcat = null;
                }
                var post_data = {
                    'video_id': video_id,
                    'name': name,
                    'description': description,
                    'privacy': privacy,
                    'cat_name': cat_name,
                    'addcat': addcat,
                };


                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/videoupload",
                    data: post_data,
                    success: function (data) {
                        window.location.href = '<?php echo base_url(); ?>index.php/home/videoDetail/' + uid;

                    }
                });


            }
        }
    </script>



</body>
</html>