<!DOCTYPE HTML>
<html>
<head>
<title>HowClip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-menu.css">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
.widthdrop {
  min-width:auto;
  width:100%;
}

</style>

<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation-->
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
   <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->
      
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="<?php echo base_url() ?>index.php/welcome">Home</a></li>
    <?php foreach($features as $videofeature) { ?>
          <li> <a href="<?php echo base_url()?>index.php/home/featurevideo/<?php echo $videofeature->name;?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $videofeature->name;?> </a> </li>
		  <?php } ?>
          <li> <a href="<?php echo base_url()?>index.php/home/history/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> History </a> </li>
		<li> <a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>"><i class="fa fa-th-large nav_icon"></i> My Channel </a> </li>
	<li class="divider" role="seperator"></li>
		  <li> <a href="#"><i class="fa fa-th-large nav_icon"></i>SUBSCRIPTIONS </a> </li>
		  <?php foreach(($usersubscription->uploaderdetail) as $uploader) { ?>
		   <li> <a href="<?php echo base_url()?>index.php/home/subscriber/<?php echo $uploader->id;?>"><i class="fa fa-th-large nav_icon"></i> <?php echo $uploader->username; ?> </a> </li>
		   <?php } ?>
  </ul>
</div>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"  > <a href="<?php echo base_url() ?>index.php/welcome">
        <!--<h1>Pupilclip</h1>
        <span>Website</span> </a>--> 
        <img src="<?php echo base_url();?>assets/images/logo.png" width="90" class="img-responsive">
       </a> </div>
      <!--//logo--> 
      <!--search-box-->
     <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" name="search_data" id="input-31"  onkeyup="ajaxSearch();" placeholder="Search..." type="search" autocomplete="off" />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
		
		<div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
         <div id="autoSuggestionsList"></div>
     </div>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
		<?php if($this->session->userdata('id') !=""){ $ppage = "uploadpage";} else {$ppage = "loginpage/upload"; }?>
		
          <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/<?php echo $ppage;?>"  style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
           
          </li><?php //echo base_url()index.php/home/loginpage?>
		   <?php if($this->session->userdata('id') == "") {?>
		   
		   <li class="dropdown head-dpdn" data-toggle="modal" data-target="#myModal"><img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive but"></li>
         <!-- <li class="dropdown head-dpdn"> <a href="#"> <img src="<?php echo base_url();?>assets/images/signinbut.png" id="pop" data-toggle="modal" data-target="#myModal" class="img-responsive but"></a> </li>-->
            <?php } ?>
		   <li class="dropdown head-dpdn"> <a href="<?php echo base_url()?>index.php/home/signup"> <img src="<?php echo base_url();?>assets/images/signinup.png"  class="img-responsive but hiderespo"></a> </li>
		      <?php foreach($userDetail as $user) { $username = $user->username;$userimg = $user->userLogo; if($userimg== "") { $userimg = "user.png"; }}?>
		   <?php if($this->session->userdata('id')!="") {?>
		   <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url();?>uploads/<?php echo $userimg;?>" class="img-responsive"></div> </a>
            <ul class="dropdown-menu" style="left:initial;right:0;color:#000;">
              <li>
                <div class="notification_header">
                  <h3><?php echo $username;?></h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p><?php echo $this->session->userdata('email');?></p>
                  
                </div>
                <div class="clearfix"></div>
                </a></li>
				<li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/dash">My Account</a> </div>
              </li>
              <li>
                <div class="notification_bottom"> <a href="<?php echo base_url()?>index.php/home/logout">Log Out</a> </div>
              </li>
            </ul>
           </li>
		   <?php }?>
        </ul>
        <div class="clearfix"> </div>
      </div>
	 
	 
    </div>
    
    
  </div>
 <div class="uploadwrap">
  <div class="container-fluid">
  <div class="row" style="padding:0px;">
  <div class="col-md-8 col-md-offset-2">
  <div class="blankpage">
  <div class="filtertop">
  <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
  <div class="panel panel-default" style="border:none;">
    <div class="panel-heading filter" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" style="border:1px solid #333; padding:5px 10px;" aria-expanded="true" aria-controls="collapseOne">
         Filter <span class="caret"></span>
        </a>
        <?php foreach($Allplaylist as $video) { $results = $video->results+1 ; }?>
        <div class="result">About <?php if($results > "0") { echo $results; } else {echo "0";}?> results</div>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
      <div class="panel-body">
      <div class="row">
      <div class="col-md-6">
      <div class="table-responsive">
       <table class="table table-condensed filtertable">
  <tr>
  <th> Upload Date</th>
 
  <th> Duration </th>

  <th>Sort by</th>
  </tr>
  <tr>
  <td>Last hour</td>

   <td>Short(< 2min)</td>

     <td>Relevance</td>
  </tr>
</table>
</div>
</div>
</div>
      </div>
    </div>
  </div>
  
  
</div>
  
  </div>
  
    
<?php foreach($listDetail as $list) {?>
 <?php if (getimagesize(base_url().'/uploads/images/'.$list->videothumb) !== false) { $link = base_url()."/uploads/images/".$list->videothumb;}else{$link = base_url()."/uploads/images/download.jpg";} ?>
<div class="row filtercontent">
  <div class="col-md-4">
  <a href="<?php echo base_url()?>index.php/home/playvideo/<?php echo $list->playlist_name;?>/<?php echo $list->user_id;?>">
  <img src="<?php echo $link?>" class="img-responsive">
  </a>
  </div>
  <div class="col-md-8">
  <h4><a href="<?php echo base_url()?>index.php/home/playvideo/<?php echo $list->playlist_name;?>/<?php echo $list->user_id;?>"><?php echo ucwords($list->playlist_name);?></a></h4>
  <ul>
  <li><a href="#"><?php echo ucwords($list->username);?></a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> <?php if($list->videoview == "") { echo "0"; }else { echo $list->videoview; }?> views</a></li>
  </ul>
  <p> <a href="#"><?php echo $list->playdate;?></a></p>
 <p> <a href="#"><?php if($video->totalvideos == ""){ echo "0";} else { echo $video->totalvideos; }?> Videos</a></p>
  <!--<p><a href="#">PHP is one of the most useful languages to know and is used everywhere you look online. In this tutorial, I start from the beginning ...</a></p>-->
  </div>
  
  
</div>
<?php } ?>

    
<?php foreach($Allplaylist as $video) { ?>
 <?php if (getimagesize(base_url().'/uploads/images/'.$video->videothumb) !== false) { $img =base_url()."/uploads/images/".$video->videothumb;}else{$img = base_url()."/uploads/images/download.jpg";}  ?>
<div class="row filtercontent">
  <div class="col-md-4">
  <a href="<?php echo base_url()?>index.php/home/playvideo/<?php echo $video->playlist_name;?>/<?php echo $video->user_id;?>">
  <img src=<?php echo $img;?> class="img-responsive">
  </a>
  </div>
  <div class="col-md-8">
  <h4><a href="<?php echo base_url()?>index.php/home/playvideo/<?php echo $video->playlist_name;?>/<?php echo $video->user_id;?>"><?php echo ucwords($video->playlist_name);?></a></h4>
  <ul>
  <li><a href="#"><?php echo ucwords($video->username);?></a></li>
  <li><a href="#"> <i class="fa fa-circle" aria-hidden="true"></i></a></li>
  <li><a href="#"> <?php if($video->videoview == "") { echo "0"; }else { echo $video->videoview; }?> views</a></li>
  </ul>
  <p> <a href="#"><?php echo $video->playdate;?></a></p>
  <p> <a href="#"><?php if($video->totalvideos == ""){ echo "0";} else { echo $video->totalvideos; }?> Videos</a></p>
  <!--<p><a href="#">PHP is one of the most useful languages to know and is used everywhere you look online. In this tutorial, I start from the beginning ...</a></p>-->
  </div>
  
  
</div>
<?php } ?>
  </div>
  </div>
  </div>
  </div>
  </div>
 <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<script src="<?php echo base_url();?>assets/js/fileupload.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
 <script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/sidebar-menu.js"></script>
  <script>
    $.sidebarMenu($('.sidebar-menu'))
  </script>

</body>
</html>