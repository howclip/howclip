<!DOCTYPE HTML>
<html>
    <head>
        <title>HowClip</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
              SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Core CSS -->
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />
        <!-- font CSS -->
        <!-- font-awesome icons -->
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

        <!-- //font-awesome icons -->
        <!-- js-->
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
        <!--webfonts-->
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
        <!--//webfonts-->
        <!--animate-->
        <link href="<?php echo base_url(); ?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
        <style>
            .imgsize

            {
                height:300px;
            }
        </style>
        <link href="<?php echo base_url(); ?>assets/css/build/demo.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/wow.min.js"></script>
        <script>
            new WOW().init();
        </script>
        <link href="<?php echo base_url(); ?>assets/css/build/video-js.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>assets/js/video-js/video.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/moment-2.2.1.js"></script>
        <!--RangeSlider Pluging-->
        <script src="<?php echo base_url(); ?>assets/js/video-js/rangeslider.js"></script>
        <link href="<?php echo base_url(); ?>assets/css/build/rangeslider.min.css" rel="stylesheet">

        <script type="text/javascript" src="simple_social_share.js"></script>
       

        <script>

            function _(el) {
                return document.getElementById(el);
            }
            function uploadFile() { 
                   var file = _("fileup").files[0];
                   
                document.getElementById('before').style.display = "none";
                document.getElementById('uploading').style.display = "block";
                document.getElementById('progressBar').style.display = "block";
                var privacy = document.getElementById('privacy').value;
             

                var formdata = new FormData();
                formdata.append("file1", file);
                formdata.append("privacy", privacy);

                var ajax = new XMLHttpRequest();
                ajax.upload.addEventListener("progress", progressHandler, false);
                ajax.addEventListener("load", completeHandler, false);
                ajax.addEventListener("error", errorHandler, false);
                ajax.addEventListener("abort", abortHandler, false);
                ajax.open("POST", "upload");
                ajax.send(formdata);
            }
            function progressHandler(event) {

                _("loaded_n_total").innerHTML = "";
                var percent = (event.loaded / event.total) * 100;
                _("progressBar").value = Math.round(percent);
                _("status").innerHTML = Math.round(percent) + "% Please Wait Uploading Video";
                if(Math.round(percent) == '100'){
                   _("status").innerHTML = "Please Wait Processing Video";
                }
                
            }
            function completeHandler(event) {

                console.log(event.target.responseText);

                var abc = event.target.responseText;
                var res = abc.split('||');
                var one = res[0];
                var two = res[1];
                var three = res[2];
                var four = res[3];
                var five = res[4];
                var six = res[5];
               

                var videonamec = three +"mp4";

                $("#vid1_html5_api").html("<source src='<?php echo base_url();?>/uploads/live/" + videonamec + "' type='video/mp4'></source>");
                if(two !=''){
                   
                $("#my_image").attr("src", '<?php echo base_url();?>/uploads/images/' + two);
                 }
                 if(two ==''){
                    
                      $("#my_image").attr("src", '<?php echo base_url();?>/uploads/images/download.jpg');
                  }
                 vid1_html5_api.play(); 
                document.getElementById('videoid').value = videonamec;
                $("#name").val(three);
                $("#video_id").val(four);
                $("#videothumb").val(two);
                $("#videoclip").val(three);
                _("vid").innerHTML = "Upload complete!";
               $('#status').css('color', 'Green');
                _("status").innerHTML = "PROCESSING DONE";
               
                _("progressBar").value = 0;
               
                 ('.vjs-timepanel-right-RS').innerHTML='';
                _("progressBar").value = 0;
                var sspanapend = "<span>"+four+"</span>";
                $( ".vjs-timepanel-right-RS" ).append( sspanapend );
                document.getElementById('start1').value = "00:00:00";
               
                
                document.getElementById('end1').value = four;
                document.getElementById('process_status').style.display = "none";
                document.getElementById('success_status').style.display = "block";
                document.getElementById('progressBar').style.display = "none";
                //document.getElementById('videoth').style.display = "none";
                //document.getElementById('th2').value = five;
                //document.getElementById('th3').value = six;
                document.getElementById('fileup').value = "";


            }
            function errorHandler(event) {
                _("status").innerHTML = "Upload Failed";
            }
            function abortHandler(event) {
                _("status").innerHTML = "Upload Aborted";
            }

        </script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <!--//end-animate-->
        <!-- Metis Menu -->
        <script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>

        <link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">


        <!--//Metis Menu -->


        <script type="text/javascript">
            $(document).ready(function () {
                $(".dropdown1 img.flag").addClass("flagvisibility");

                $(".dropdown1 dt a").click(function () {
                    $(".dropdown1 dd > ul").toggle();
                });

                $(".dropdown1 dd ul li a").click(function () {
                    var text = $(this).html();
                    $(".dropdown1 dt a").html(text);
                    $(".dropdown1 dd > ul").hide();
                    $("#result").html("Selected value is: " + getSelectedValue("sample"));
                    //creatpop1(getSelectedValue("sample"));
                });

                function getSelectedValue(id) {
                    return $("#" + id).find("dt a").html();
                }

                $(document).bind('click', function (e) {
                    var $clicked = $(e.target);
                    if (!$clicked.parents().hasClass("dropdown1"))
                        $(".dropdown1 dd > ul").hide();
                });


                $("#flagSwitcher").click(function () {
                    $(".dropdown1 img.flag").toggleClass("flagvisibility");
                });
            });
        </script>


        <!--//Metis Menu -->
    </head>
    <body class="cbp-spmenu-push">
        <div class="main-content"> 





            <div id="before">

                <!--left-fixed -navigation-->

                <!--left-fixed -navigation--> 
                <!-- header-starts -->
                <div class="sticky-header header-section ">
                    <div class="header-left"> 

                        <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="<?php echo base_url();?>index.php/welcome">Home</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id') ?>">My Videos</a></li>
                                <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>"> History </a> </li>
                                <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"> My Channel </a> </li>
                                <li class="divider" role="seperator"></li>
                                <li> <a href="#">SUBSCRIPTIONS </a> </li>
                                <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                    <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"> <?php echo $uploader->username; ?> </a> </li>
                                <?php } ?>
                                <!--<li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>-->
                            </ul>
                        </div>
                        <?php
                        foreach ($companydetail as $company) {
                            if ($company->company_logo != "") {
                                $cmplogo = base_url()."/Admin/uploads/$company->company_logo";
                            } else {
                                $cmplogo = base_url()."/uploads/logo.png";
                            }
                            ?>
                            <div class="logo"  > <a href="<?php echo base_url();?>index.php/welcome">

                                    <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                                </a> </div>
                        <?php } ?>

                        <div class="search-box">
                            <form class="input">
                                <input class="sb-search-input input__field--madoka" name="search_data" id="input-34"  onkeyup="ajaxSearchdata();" placeholder="Search..." type="search" autocomplete="off" />
                                <label class="input__label" for="input-34"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                    <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                                    </svg> </label>
                            </form>

                            <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
                                <div id="autoSuggestionsList"></div>
                            </div>
                        </div>
                        <!--//end-search-box-->
                        <div class="clearfix"> </div>
                    </div>
                    <div class="header-right">
                        <div class="profile_details_left"><!--notifications of menu start -->
                            <ul class="nofitications-dropdown">
                                <li class="dropdown head-dpdn"> <a href="#"style="color:#333;" > <img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > </a>

                                </li>
                                <?php
                                foreach ($userDetail as $user) {
                                    $username = $user->username;
                                    $userimg = $user->userLogo;
                                    if ($userimg == "") {
                                        $userimg = "user.png";
                                    }
                                }
                                ?>
                              <!-- <li class="dropdown head-dpdn"> <a href="login.html"> <img src="images/signinbut.png" class="img-responsive but"></a> </li>-->
                           <!-- <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url(); ?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>-->
                                <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <div class="notification_header">
                                                <h3><?php echo $this->session->userdata('name'); ?></h3>
                                            </div>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p><?php echo $this->session->userdata('email'); ?></p>

                                                </div>
                                                <div class="clearfix"></div>
                                            </a></li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                        </li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="clearfix"> </div>
                        </div>


                    </div>


                </div>

                <div class="uploadwrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12" style="padding-right:0px;">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="blankpage">

                                            <form>
                                                <div class="row">
                                                    <div class="col-md-6 col-md-offset-3 center">
                                                        <div class="btn-container">
                                                            <!--the three icons: default, ok file (img), error file (not an img)-->
                                                            <h1 class="imgupload"><!--<i class="fa fa-file-image-o"></i>--><span class="glyphicon glyphicon-folder-open"></span></h1>
                                                            <h1 class="imgupload ok"><i class="fa fa-thumbs-o-up" aria-hidden="true"></i></h1>
                                                            <h1 class="imgupload stop"><i class="fa fa-times"></i></h1>
                                                            <!--this field changes dinamically displaying the filename we are trying to upload-->
                                                            <p id="namefile">Only format allowed! (avi,mpg,wmv,mp4)</p>
                                                            <!--our custom btn which which stays under the actual one-->
                                                            <button type="button" id="btnup" class="btn btn-primary btn-lg" style="font-size:14px;">Browse for your video!</button>

                                                            <input type="file" name="file1" id="fileup" onChange="fileValidation();">
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--additional fields-->
                                                <div class="row" style="margin-top:20px;">			
                                                    <div class="col-md-12">
                                                        <!--the defauld disabled btn and the actual one shown only if the three fields are valid-->
                                                        <p style="text-align:center;"><input type="submit" value="Submit!" class="btn btn-primary" id="submitbtn"></p>
                                                        <p style="text-align:center;"><select name="privacy" id="privacy"><option value="public">Public</option>
                                                                <option value="unlisted">Unlisted</option>
                                                                <option value="private">Private</option></select> </p>
                                                    </div>
                                                </div>
                                            </form>



                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="blankpage">
                                            <div class="blankhead">HELP AND SUGGESTIONS</div>
                                            <div class="selectpara">Want to upload videos longer than 15 minutes? <a href="<?php echo base_url() ?>index.php/home/gettingstarted">Increase your limit</a></div>
                                            <div class="selectpara">By submitting your videos to YouTube, you acknowledge that you agree to YouTube's <a href="<?php echo base_url() ?>index.php/home/terms">Terms of Service</a> and <a href="<?php echo base_url() ?>index.php/home/terms">Community Guidelines</a></div>
                                            <div class="selectpara">Please be sure not to violate others' copyright or privacy rights. <a href="<?php echo base_url() ?>index.php/home/terms"> Learn more</a>    </div>
                                            <!-- <div class="selectpara">
                                             <ul>
                                             <li><a href="#">Upload Instruction</a></li>
                                             <li><a href="#">Troubleshooting</a></li>
                                             <li><a href="#">Mobile Uploads</a></li>
                                             </ul>
                                             </div>-->

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--<div class="col-md-3" style="padding-left:0px;">
                            <div class="row">
                            <div class="col-md-12">
                            <div class="blankpage">
                            <div class="rightblank">IMPORT VIDEOS</div>
                            <div class="row">
                            <div class="col-md-4" style="padding:0px;">
                            <img src="<?php echo base_url(); ?>assets/images/photos-import-vflI4uOxj.png" class="img-responsive">
                            </div>
                            <div class="col-md-8" style="padding:0px;">
                            <div class="righthead">Import your videos form Google photos</div>
                            <a class="btn importbut" href="#" role="button">Import</a>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                           <div class="row">
                            <div class="col-md-12">
                            <div class="blankpage">
                            <div class="rightblank">IMPORT VIDEOS</div>
                            <div class="row">
                            <div class="col-md-4" style="padding:0px;">
                            <img src="<?php echo base_url(); ?>assets/images/photos-import-vflI4uOxj.png" class="img-responsive">
                            </div>
                            <div class="col-md-8" style="padding:0px;">
                            <div class="righthead">Import your videos form Google photos</div>
                            <a class="btn importbut" href="#" role="button">Import</a>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            <div class="row">
                            <div class="col-md-12">
                            <div class="blankpage">
                            <div class="rightblank">IMPORT VIDEOS</div>
                            <div class="row">
                            <div class="col-md-4" style="padding:0px;">
                            <img src="<?php echo base_url(); ?>assets/images/photos-import-vflI4uOxj.png" class="img-responsive">
                            </div>
                            <div class="col-md-8" style="padding:0px;">
                            <div class="righthead">Import your videos form Google photos</div>
                            <a class="btn importbut" href="#" role="button">Import</a>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            
                            </div>-->
                        </div>
                    </div>
                </div>
                <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url();?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
                <!--//footer--> 

            </div>




            <div id="uploading" style="display:none;">


                <!--left-fixed -navigation-->

                <!--left-fixed -navigation--> 
                <!-- header-starts -->
                <div class="sticky-header header-section ">
                    <div class="header-left"> 
                        <!--toggle button start-->
                     <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->

                        <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <i class="fa fa-bars"></i>
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                <li><a href="<?php echo base_url();?>index.php/welcome">Home</a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/videoDetail?id=<?php echo $this->session->userdata('id') ?>">My Videos</a></li>
                                <li> <a href="<?php echo base_url() ?>index.php/home/history/<?php echo $this->session->userdata('id'); ?>"> History </a> </li>
                                <li> <a href="<?php echo base_url() ?>index.php/home/mychannel/<?php echo $this->session->userdata('id'); ?>"> My Channel </a> </li>
                                <li class="divider" role="seperator"></li>
                                <li> <a href="#">SUBSCRIPTIONS </a> </li>
                                <?php foreach (($usersubscription->uploaderdetail) as $uploader) { ?>
                                    <li> <a href="<?php echo base_url() ?>index.php/home/subscriber/<?php echo $uploader->id; ?>"> <?php echo $uploader->username; ?> </a> </li>
                                <?php } ?>
                                <!--<li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>-->
                            </ul>
                        </div>
                        <!--toggle button end--> 
                        <!--logo -->
                        <?php
                        foreach ($companydetail as $company) {
                            if ($company->company_logo != "") {
                                $cmplogo = base_url()."/Admin/uploads/$company->company_logo";
                            } else {
                                $cmplogo = base_url()."/uploads/logo.png";
                            }
                            ?>
                            <div class="logo"  > <a href="<?php echo base_url();?>index.php/welcome">

                                    <img src="<?php echo $cmplogo; ?>" class="img-responsive">
                                </a> </div>
                        <?php } ?>
                        <!--//logo--> 
                        <!--search-box-->
                        <div class="search-box">
                            <form class="input">
                                <input class="sb-search-input input__field--madoka" name="search_data" id="input-32"  onkeyup="ajaxSearch();" placeholder="Search..." autocomplete="off" type="search" />
                                <label class="input__label" for="input-32"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
                                    <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
                                    </svg> </label>
                            </form>

                            <div id="suggestionsbox" style="background-color:#FFF;position:absolute;top:40px;left:0px;width:100%;z-index:10000;">
                                <div id="autoSuggestionsListbox"></div>
                            </div>
                        </div>
                        <!--//end-search-box-->
                        <div class="clearfix"> </div>
                    </div>
                    <div class="header-right">
                        <div class="profile_details_left"><!--notifications of menu start -->
                            <ul class="nofitications-dropdown">
                                <li class="dropdown head-dpdn"> 
                                    <a href="<?php echo base_url() ?>index.php/home/uploadpage" style="color:#333;" style="color:#333;" > <img src="<?php echo base_url(); ?>assets/images/upload.png" class="img-responsive" width="105" > </a>

                                </li>
                                <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <div class="proimg"><img src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" class="img-responsive"></div> </a>
                                    <ul class="dropdown-menu" style="left:initial;right:0;color:#000;">
                                        <li>
                                            <div class="notification_header">
                                                <h3><?php echo ucwords($username); ?></h3>
                                            </div>
                                        </li>
                                        <li><a href="#">
                                                <div class="notification_desc">
                                                    <p><?php echo $this->session->userdata('email'); ?></p>

                                                </div>
                                                <div class="clearfix"></div>
                                            </a></li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/dash">My Account</a> </div>
                                        </li>
                                        <li>
                                            <div class="notification_bottom"> <a href="<?php echo base_url() ?>index.php/home/logout">Log Out</a> </div>
                                        </li>
                                    </ul>
                                </li>


                            </ul>
                            <div class="clearfix"> </div>
                        </div>
                        <!--notification menu end -->
                        <div class="profile_details"> 


                        </div>

                    </div> 
                </div>
                <div class="uploadwrap">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="blankpage">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img id="my_image" style="max-width:70%" src="<?php echo base_url(); ?>uploads/images/uploading.gif" class="img-responsive">
                                            <div class="blankhead">Upload Status</div>


                                            <div id="vid" style="font-size:11px; color:#999;"></div>
                                             <!--<p style="font-size:11px; color:#999;">vflgbfbfbkf flkjgblfbm <a href="#">dgv@kjf.com</a></p>-->
                                            <!--<div class="blankhead">Upload Status</div>-->

                                            <div class="terms"><p>*</p></div>
                                            <div class="hint" id="process_status" style="width:70%;">Your Videos are still uploading. Please keep this page open until they are done.</div>
                                            <div class="hint" id="success_status" style="width:70%;display:none;">Click "Post" to make your video live.</div>
                                            <div class="blankhead">Clips</div>
                                            <div class="blankhead" id="under" style="display:none;color:red"> Please Wait Uploading Clip</div>

                                            <div class="blankhead" id="complete" style="display:none">Clip is Ready and Available to View</div>



                                            <ul class="media-list" style="margin-top:20px;" id="retimg">


                                            </ul>

                                        </div>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div id="myProgress">

                                                        <progress id="progressBar" value="0" max="100" style="width:100%;height:100%;display:none"></progress>
                                                        <p id="status" style="color:red;"></p>
                                                        <h4 id="clips" style="color:red;"></h4>
                                                        <p id="loaded_n_total" style="margin-top:10px;"></p>
                                                    </div>
                                                </div>
                                                <!--<div class="col-md-2">
                                                <a class="btn publish2" href="#" role="button">Link</a>
                                                </div>-->
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                               <!--<img src="<?php echo base_url(); ?>assets/images/uploading.gif" class="img-responsive imgsize">-->
                                                    <form style="margin-top:30px;">


                                                        <video id="vid1" class="video-js vjs-default-skin" controls preload="none" width="100%" height="350"
                                                               
                                                               data-setup=''>



                                                            <track kind="captions" src="demo.captions.vtt" srclang="en" label="English"></track>
                                                            <!-- Tracks need an ending tag thanks to IE9 -->
                                                        </video>



                                                        <div class="col-md-5" style="padding: 0 5px;">
                                                            <div style="color:#FF0000;text-align:center;" id="formerror"></div>
                                                            <div style="color:#FF0000;text-align:center;display:none;" id="clipnoerror">Please Input Clip No.</div>
                                                            <div style="color:#FF0000;text-align:center;display:none;" id="clipnameerror"> Please Input Clip Name</div>
                                                            <div style="color:#FF0000;text-align:center;display:none;" id="clipdescrror">Please Input Clip Description</div>
                                                            <div style="color:#FF0000;text-align:center;display:none;" id="monetizeerror">Please Input monetize Option</div>
                                                            <div style="color:#FF0000;text-align:center;display:none;" id="formcaterror">Please Input Clip Category</div>
                                                            <div style="color:#FF0000;text-align:center;display:none;" id="costerror">Cost should be greater than 0 </div>
                                                            <div class="dropdown" style="display:inline-block;width:30%; margin-top:5px; margin-right:9px;">
                                                                <!-- <button class="btn btn-secondary cliping dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                 Select Clip <span class="caret"></span>
                                                                  </button>
                                                                 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                 <li><a href="#">Clip #1</a></li>
                                                                 <li><a href="#">Clip #2</a></li>
                                                                 <li><a href="#">Clip #3</a></li>
                                                                 <li><a href="#">Clip #4</a></li>
                                                                 <li><a href="#">Clip #5</a></li>
                                                                 </div>-->
                                                                <select class="cliping dropdown-toggle" name="clipno" id="clipno">
                                                                    <option id="clip1" value="clip1">Clip 1</option>
                                                                    <option id="clip2" value="clip2">Clip 2</option>
                                                                    <option id="clip3" value="clip3">Clip 3</option>
                                                                    <option id="clip4" value="clip4">Clip 4</option>
                                                                    <option id="clip5" value="clip5">Clip 5</option>
                                                                    <option id="clip6" value="clip6">Clip 6</option>
                                                                    <option id="clip7" value="clip7">Clip 7</option>
                                                                    <option id="clip8" value="clip8">Clip 8</option>
                                                                    <option id="clip9" value="clip9">Clip 9</option>
                                                                    <option id="clip10" value="clip10">Clip 10</option>
                                                                </select>


                                                            </div>
                                                            <input type="hidden" id="videoid" value="">
                                                            <textarea  class="form-control pageinfo clipinfo" rows="1"  id="clipname" name="clipname" placeholder="Clip Name"></textarea>
                                                            <textarea class="form-control" rows="1"  name="clipdesc" placeholder="Clip Description" id="clipdesc" style="height:45px !important; resize:none"></textarea>
                                                        </div>
                                                        <div class="col-md-3"style="padding: 0 5px;" >
                                                            <div class="form-group">
                                                                <label for="exampleInputFile" class="labeltxt">Define Clip Time</label>
                                                                <div style="display:inline-block;">
                                                                    <label class="form-control pageinfo" id="exampleInputEmail1"  style="display:inline-block; width:30%; line-height:1;">Start</label>
                                                                    <input type="text" class="form-control pageinfo" id="start1" placeholder="00:00:00" style="display:inline-block;width:60%;">
                                                                </div>
                                                                <div style="display:inline-block;">
                                                                    <label class="form-control pageinfo" id="exampleInputEmail1"  style="display:inline-block; width:30%; line-height:1;">End</label>
                                                                    <input type="text" class="form-control pageinfo" id="end1" placeholder="00:00:00" style="display:inline-block;width:60%;">
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <!--<div class="col-md-3" style="padding: 0 5px;">
                                                        <div class="form-group">
                                                        <label for="exampleInputFile" class="labeltxt">Define Categories</label>
                                                        <div class="uploader">
                                                        <input type="text" name="search" placeholder="Search..">
                                                        </div>
                                                        </div>
                                                        </div>-->
                                                        <div class="col-md-4" style="padding:0 5px;">
                                                            <div class="form-group" style="width:65%;display:inline-block; margin-bottom:5px;">
                                                                <div style="vertical-align:middle;margin-top:20px;">
                                                                    <dl id="sample" class="dropdown1">
                                                                        <dt><a href="javascript:void()" onclick="hideamount();"><span>Monetize
                                                                                    <input type="hidden" id="monetize" value="monetize"> 
                                                                                    <span style="display:inline-block;width:auto; background-color:#990000; padding:0px 10px; float:right;">
                                                                                        <i class="fa fa-caret-down" aria-hidden="true" style="color:#FFF;"></i></span></span> </a></dt>
                                                                        <dd>
                                                                            <ul  >
                                                                                <li><a href="javascript:void()" class="test" tabindex="-1" onclick="hideamount();"><span >Free<input type="hidden" id="monetize" value="free"> <i class="fa fa-check-circle" aria-hidden="true"></i></span></a></li>             
                                                                                <li ><a tabindex="-1" href="javascript:void()" onclick="hideamount();"><span>In Video Ads <input type="hidden" id="monetize" value="videoads"><i class="fa fa-check-circle" aria-hidden="true"></i></span> </a>
                                                                                    <ul  class="displayno" >
                                                                                        <li ><a href="javascript:void(0)" onclick="hideamount();"><span>Banner on Page <input type="hidden" id="monetize" value="pagebanner"><i class="fa fa-check-circle" aria-hidden="true"></i></span></a></li>
                                                                                        <li ><a href="javascript:void(0)" onclick="hideamount();"><span >Banner on video <input type="hidden" id="monetize" value="videobanner"><i class="fa fa-check-circle" aria-hidden="true"></i></span> </a></li>
                                                                                        <li ><a href="javascript:void(0)" onclick="hideamount();"><span >Pre-roll video <input type="hidden" id="monetize" value="prerollvideo"><i class="fa fa-check-circle" aria-hidden="true"></i></span> </a></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <li><a tabindex="-1" href="javascript:void(0)" onclick="displayamount();"><span>PPV <input type="hidden" id="monetize" value="ppv"> <i class="fa fa-check-circle" aria-hidden="true"></i></span></a>
                                                                                    <!--	<ul  class="displayno" >
                                                                                             <li><a href="javascript:void(0)" onclick="displayamount();"><span><span style="display:block;font-size:11px;"> Cost </span>
                                                                                             $  <span id="costbefore"><input type="text" class="form-control pageinfo" id="cost" placeholder="0000" style="display:inline-block; width:45%;"><button type="button" class="btn accept" style="width:30%;display:inline-block;">Accept</button></span>
                                                                                              </span><i class="fa fa-check-circle" aria-hidden="true"></i></a></li>
                                                                                             </ul>-->
                                                                                </li>
                                                                            </ul>
                                                                        </dd>
                                                                    </dl>
                                                                    <span id="costafter" style="display:none;margin-left: 5px;"><span style="display:block;font-size:11px;"> Cost($.05-$25.00) </span>
                                                                        $  <span ><input type="text" class="form-control pageinfo" id="cost" placeholder="Ex:1.00" onChange="return validate_val(this)" style="display:inline-block; width:45%;"><button type="button" class="btn accept" style="width:30%;display:inline-block;">Accept</button></span>
                                                                    </span></a>
                                                                </div>
                                                            </div>
                                                            <button id="abcd" type="button" class="btn postupload" onclick="createclips();"  style="width:30%;display:inline-block;    float: right;
                                                                    margin-top: 15px;">Post</button>
                                                            <div class="sharemenu">
                                                                <ul>
                                                                    <li><a href="#"> <img src="<?php echo base_url(); ?>assets/images/share-512.png"  class="img-responsive"> </a></li>

                                                                    <li><a style="display:inline;float: left" id="sharea" href="#" target="_blank" ><img src="<?php echo base_url(); ?>assets/images/fb-art.png" alt="Facebook" class="img-responsive"/> </a>
                                                                        <a style="display:inline;float:left;margin-left:5px;" href="https://twitter.com/share?url=<?php echo base_url();?>index.php/home/showvideo/<?php echo $li->id; ?>" target="_blank" style="display:inline;padding:5px 5px;">
                                                                                <img src="https://simplesharebuttons.com/images/somacro/twitter.png" width="35px;" height="35px;" alt="Twitter" />
                                                                            </a>
                                                                        
                                                                    
                                                                    </li>
                                                                    <!--<li><a href="#"> <img src="<?php echo base_url(); ?>assets/images/twitter-icon--flat-gradient-social-iconset--limav-2.png" class="img-responsive">  </a></li>
                                                                    <li><a href="#"> <img src="<?php echo base_url(); ?>assets/images/pinterest-icon-png-3.png"  class="img-responsive"> </a></li>
                                                                    <li><a href="#"> <img src="<?php echo base_url(); ?>assets/images/Instagram-icon.png" class="img-responsive">  </a></li>-->
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">

                                                    <div class="menuselecttabone">
                                                        <p style="font-size: 12px;font-weight:600;">Choose Category</p>
                                                        <div class="selectmenu">

                                                            <ul>
                                                                <?php foreach ($catmenu as $cat) { ?>
                                                                    <li  style="margin-top:5px;class:active;" > 
                                                                        <a href="javascript:void(0)"><?php echo $cat[title]; ?><input type="radio" class="chkradio" name="category" id="<?php echo $cat[id]; ?>" style="float:right;" value="<?php echo $cat[id]; ?>" onChange="getsubcat('<?php echo $cat[id]; ?>', this.id)"></a>
                                                                    </li>
                                                                <?php } ?>

                                                            </ul>


                                                        </div>
                                                    </div>
                                                    <div class="menuselecttabone">
                                                        <p style="font-size: 12px;font-weight:600;" >Choose Subcategory</p>
                                                        <div class="selectmenu">

                                                            <div id="autoresp" class="autoresp">
                                                                <ul class="fisrt">
                                                                   
                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="menuselecttabone">
                                                        <p style="font-size: 12px;font-weight:600;">Choose Subcategory</p>
                                                        <div class="selectmenu">

                                                            <div id="autoresp2" class="autoresp2">
                                                                <ul class="second">
                                                                    
                                                                </ul>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="menuselecttabone">
                                                        <p style="font-size: 12px;font-weight:600;">Choose Subcategory</p>
                                                        <div class="selectmenu">

                                                            <div id="autoresp3" class="autoresp3">
                                                                <ul class="third">
                                                                   
                                                                </ul>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="menuselecttabone">
                                                        <p style="font-size: 12px;font-weight:600;">Choose Subcategory</p>
                                                        <div class="selectmenu">

                                                            <div id="autoresp4" class="autoresp4">
                                                                <ul class="fourth">
                                                                   
                                                                </ul>
                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer">
                <div class="row">
                    <div class="col-md-12">
                        <div class="footmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/about"> About </a></li>
                                <li><a href="javascript:void(0);"> Press </a></li>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Copyright </a></li>
                                <li><a href="javascript:void(0);"> Creators</a></li>
                                <li><a href="javascript:void(0);"> Advertise</a></li>
                                <li><a href="javascript:void(0);"> Developers</a></li>
                                <li><a href="<?php echo base_url();?>index.php/welcome">HowClip</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="minfootmen">
                            <ul>
                                <li><a href="<?php echo base_url() ?>index.php/home/terms"> Terms</a></li>
                                <li><a href="javascript:void(0);"> Privacy</a></li>
                                <li><a href="javascript:void(0);"> Policy & Safety</a></li>
                                <li><a href="javascript:void(0);"> Send feedback</a></li>
                                <li><a href="javascript:void(0);"> Test new features</a></li>
                                <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <!--//footer--> 
    </div>
    <!-- Classie --> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/classie.js"></script> 
    <script>
                                                                        var menuLeft = document.getElementById('cbp-spmenu-s1'),
                                                                                showLeftPush = document.getElementById('showLeftPush'),
                                                                                body = document.body;

                                                                        showLeftPush.onclick = function () {
                                                                            classie.toggle(this, 'active');
                                                                            classie.toggle(body, 'cbp-spmenu-push-toright');
                                                                            classie.toggle(menuLeft, 'cbp-spmenu-open');
                                                                            disableOther('showLeftPush');
                                                                        };

                                                                        function disableOther(button) {
                                                                            if (button !== 'showLeftPush') {
                                                                                classie.toggle(showLeftPush, 'disabled');
                                                                            }
                                                                        }
    </script> 
    <!--scrolling js--> 
    <script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/scripts.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/cssmenujs.js"></script>
    <!--//scrolling js--> 
    <!-- Bootstrap Core JavaScript --> 
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script> 

    <script src="<?php echo base_url(); ?>assets/js/bootstrap-dropdownhover.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>

<!--<script src="js/metisMenu.js"></script>--> 
    <script>
         function fileValidation(){
            var fileInput = document.getElementById('fileup');
            var filePath = fileInput.value;
            var allowedExtensions = /(\.avi|\.mpg|\.wmv|\.mp4)$/i;
            if (!allowedExtensions.exec(filePath)){
            alert('Please upload file having extensions .avi/.wmv/.mpg/.mp4 only.');
            fileInput.value = '';
            return false;
            } 
            else{
                uploadFile();
            }
            }
                                                                        $(function () {
                                                                            $('#menu').metisMenu({
                                                                                toggle: false // disable the auto collapse. Default: true.
                                                                            });
                                                                        });
    </script>
    <script>
        function getsubcat(x, e)
        {
            var abc = $("#" + e).prop("checked");
            if (abc)
            {

                var x = x;
                var post_data = {
                    'search': x,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat",
                    data: post_data,
                    success: function (data) {
                        console.log(data);
                        $(".fisrt").empty();
                        $(".second").empty();
                        $(".third").empty();
                        $(".fourth").empty();
                        if (data.length > 0) {

                            var json = $.parseJSON(data);

                            $.each(json, function (k, v) {

                                $("#autoresp").append("<ul style='list-style:none;' class='fisrt'><li><a href='javascript:void(0)'>" + v.category_title + "<input name='category' id='" + v.id + "' type='radio' onChange='getsubcat2(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp").append("<ul style='list-style:none;' class='fisrt' id='addedadded'><li><a href='javascript:void(0)'><input type='text' style='width:100%;position:relative;margin-left:10px;' name='addedcat' id='adcat' placeholder='Add Subcategory'></a></ul>");


                        }

                    }
                });
            } else
            {

            }
        }
        function getsubcat2(y, e)
        {

            var a1 = $("#" + e).prop("checked");

            if (a1)
            {
                var post_data = {
                    'search': y,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat2",
                    data: post_data,
                    success: function (response) {
                        $(".second").empty();
                        $(".third").empty();
                        $(".fourth").empty();
                        $("#addedadded").empty();

                        if (response.length > 0) {

                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp2").append("<ul style='list-style:none;' class='second'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat3(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp2").append("<ul style='list-style:none;' class='second' id='addedcat2'><li><a href='javascript:void(0)'><input type='text' style='width:100%;position:relative;margin-left:10px;' name='addedcat' id='adcat'  placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            } else
            {

            }

        }

        function getsubcat3(z, e)
        {

            var a2 = $("#" + e).prop("checked");
            if (a2)
            {
                var post_data = {
                    'search': z,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat3",
                    data: post_data,
                    success: function (response) {
                        $(".third").empty();

                        $(".fourth").empty();
                        $("#addedcat2").empty();

                        if (response.length > 0) {
                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp3").append("<ul style='list-style:none;' class='third'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat4(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp3").append("<ul style='list-style:none;' class='third' id='addedcat3'><li><a href='javascript:void(0)'><input type='text' name='addedcat' id='adcat' style='width:100%;position:relative;margin-left:10px;' placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            }
        }

        function getsubcat4(z2, e)
        {
            var a4 = $("#" + e).prop("checked");
            if (a4)
            {
                var post_data = {
                    'search': z2,
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/searchsubcat4",
                    data: post_data,
                    success: function (response) {
                        $(".fourth").empty();
                        $("#addedcat3").empty();
                        if (response.length > 0) {

                            var json = $.parseJSON(response);
                            $.each(json, function (k, v) {
                                $("#autoresp4").append("<ul style='list-style:none;' class='fourth'><li><a href='javascript:void(0)'>" + v.category_title + "<input id='" + v.id + "' type='radio' name='category' onChange='getsubcat5(" + v.id + "," + "this.id" + ")'" + "value=" + v.id + " >" + "</a></li></ul>");

                            });
                            $("#autoresp4").append("<ul style='list-style:none;' class='fourth' id='addedcat4'><li><a href='javascript:void(0)'><input type='text' name='addedcat' id='adcat' style='width:100%;position:relative;margin-left:10px;' placeholder='Add Subcategory'></a></li></ul>");

                        }

                    }
                });
            }
        }
        function getsubcat5()
        {
            $("#addedcat4").empty();
        }
    </script>

    <script>

        //Example of options ={hidden:false,locked:true,panel:false}
        var options = {hidden: false},
        mplayer = videojs("vid1");
        mplayer.rangeslider(options);



        function playBetween() {
            var start, end;
            start = document.getElementById('Start1').value;
            end = document.getElementById('End1').value;
            mplayer.playBetween(start, end);
        }
        function loopBetween() {
            var start = document.getElementById('Start1').value;
            var end = document.getElementById('End1').value;
            mplayer.loopBetween(start, end);
        }
        function getValues() {
            var values = mplayer.getValueSlider();
           
            document.getElementById('Start1').value = videojs.round(values.start, 2);
            document.getElementById('End1').value = videojs.round(values.end, 2);
        }

        function showhide() {
            var plugin = mplayer.rangeslider.options;
            if (plugin.hidden)
                mplayer.showSlider();
            else
                mplayer.hideSlider();
        }
        function lockunlock() {
            var plugin = mplayer.rangeslider.options;
            if (plugin.locked)
                mplayer.unlockSlider();
            else
                mplayer.lockSlider();
        }
        function showhidePanel() {
            var plugin = mplayer.rangeslider.options;
            if (!plugin.panel)
                mplayer.showSliderPanel();
            else
                mplayer.hideSliderPanel();
        }
        function showhideControlTime() {
            var plugin = mplayer.rangeslider.options;
            if (!plugin.controlTime)
                mplayer.showControlTime();
            else
                mplayer.hideControlTime();
        }
    </script>
    <script>


        $(".vjs-selectionbar-arrow-RS").mouseup(function () {
            var values = mplayer.getValueSlider();
            var stat = values.start;
            var end = values.end;

            var st = getminute(stat);
            var en = getminute(end);
           
            document.getElementById('start1').value = st;
            document.getElementById('end1').value = en;
        });

        $(".vjs-selectionbar-line-RS").mouseup(function () {
            var values = mplayer.getValueSlider();
           var stat = values.start;
            var end = values.end;

            var st = getminute(stat);
            var en = getminute(end);
           
            document.getElementById('start1').value = st;
            document.getElementById('end1').value = en;
        });

        $(".vjs-seek-handle vjs-slider-handle").mouseup(function () {
            var values = mplayer.getValueSlider();
           var stat = values.start;
            var end = values.end;

            var st = getminute(stat);
            var en = getminute(end);
           
            document.getElementById('start1').value = st;
            document.getElementById('end1').value = en;
        });

        $(".vjs-rangeslider-holder").mouseup(function () {
            var values = mplayer.getValueSlider();
           var stat = values.start;
            var end = values.end;

            var st = getminute(stat);
            var en = getminute(end);
           
            document.getElementById('start1').value = st;
            document.getElementById('end1').value = en;
        });

        $(".vjs-rangeslider-handle vjs-selectionbar-left-RS").mouseup(function () {
            var values = mplayer.getValueSlider();
            var stat = values.start;
            var end = values.end;

            var st = getminute(stat);
            var en = getminute(end);
           
            document.getElementById('start1').value = st;
            document.getElementById('end1').value = en;
        });
        $(".vjs-rangeslider-handle vjs-selectionbar-right-RS").mouseup(function () {
            var values = mplayer.getValueSlider();
            var stat = values.start;
            var end = values.end;

            var st = getminute(stat);
            var en = getminute(end);
           
            document.getElementById('start1').value = st;
            document.getElementById('end1').value = en;
        });
        function getminute(x) {
            
            var h = Math.floor(x / 3600);
            var m = Math.floor(x % 3600 / 60);
            var s = Math.floor(x % 3600 % 60);

            return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
            

        }


    </script>
    <script>
     function dateDiff(time1, time2) {
      
    var t1 = new Date();
    var parts = time1.split(":");
    t1.setHours(parts[0], parts[1], parts[2], 0);
    var t2 = new Date();
    parts = time2.split(":");
    t2.setHours(parts[0], parts[1], parts[2], 0);

    var rti=parseInt(Math.abs(t1.getTime() - t2.getTime()) / 1000);
   return rti;
    
}
        function createclips()
        {

            //document.getElementById('under').style.display = "block";
            //document.getElementById('complete').style.display = "none";
            var videoname = document.getElementById('videoid').value;
            var clipno = document.getElementById('clipno').value;
            var clipname = document.getElementById('clipname').value;
            var clipdesc = document.getElementById('clipdesc').value;
            var monetize = document.getElementById('monetize').value;
            var start = document.getElementById('start1').value;
            var end = document.getElementById('end1').value;

            var amount = document.getElementById('cost').value;
            var radioValue = $("input[name='category']:checked").val();
            var dur =  dateDiff(start,end);

            if (clipno == "")
            {
                //document.getElementById("formerror").innerHTML = "Enter the Clip Number ";
                document.getElementById('clipnoerror').style.display = "block";
                setTimeout(function () {
                    $('#clipnoerror').fadeOut('fast');
                }, 1000);
                return false;
            } else if (clipname == "")
            {

                //document.getElementById("formerror").innerHTML = "Enter the Clip Name ";

                document.getElementById('clipnameerror').style.display = "block";
                setTimeout(function () {
                    $('#clipnameerror').fadeOut('fast');
                }, 1000);
                return false;
            } else if (clipdesc == "")
            {
                //document.getElementById("formerror").innerHTML = "Enter the Clip Description";
                document.getElementById('clipdescrror').style.display = "block";
                setTimeout(function () {
                    $('#clipdescrror').fadeOut('fast');
                }, 1000);
                return false;
            } else if (monetize == "monetize")
            {
                //document.getElementById("formerror").innerHTML = "Select the Clip monetize";
                document.getElementById('monetizeerror').style.display = "block";
                setTimeout(function () {
                    $('#monetizeerror').fadeOut('fast');
                }, 1000);
                return false;
            } else if (typeof (radioValue) == 'undefined')
            {

                document.getElementById('formcaterror').style.display = 'block';
                setTimeout(function () {
                    $('#formcaterror').fadeOut('fast');
                }, 1000);
                return false;
            }
            else if(dur > 160){
              alert('Clip should be under 2:40 minutes. Please adjust time length');
          }
            
        
        else
            {
                document.getElementById('under').style.display = "block";
            document.getElementById('complete').style.display = "none";
                var addcat = document.getElementById('adcat').value;
                
                setTimeout(function () {
                    $('#formerror').fadeOut('fast');
                }, 1000);
                document.getElementById('formcaterror').style.display = 'none';
                document.getElementById('costerror').style.display = 'none';
                if(amount == ''){
                    amount = 0;
                }
                
                
                
                var post_data = {
                    'video': videoname,
                    'clipno': clipno,
                    'clipname': clipname,
                    'clipdesc': clipdesc,
                    'monetize': monetize,
                    'start': start,
                    'end': end,
                    'category': radioValue,
                    'addcat': addcat,
                    'amount': amount
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url() ?>index.php/home/createclips",
                    data: post_data,
                    success: function (response) {
                         console.log(response);
                        var abc = response;
                        $(".fisrt").empty();
                        $(".second").empty();
                        $(".third").empty();
                        $(".fourth").empty();
                        $('input[name=category]').attr('checked', false);
                        setTimeout(function () {
                            $('.test').click();
                        }, 5000);

                        var res = abc.split('||');
                        var clip = res[0];
                        var clipimg = res[1];
                        var clipname = res[2];
                        var link = res[3];
                        document.getElementById('under').style.display = "none";
                        document.getElementById('complete').style.display = "block";

                        $("#retimg").append("<li class='media clipmeadia'> <div class='media-left'><a href='#'><img class='media-object clipimg' src='<?php echo base_url();?>/uploads/images/" + clipimg + "' alt='...'></a></div><div class='media-body clipbody'><h4 class='media-heading cliphead'>" + clipname + "</h4></div></li>");
                        document.getElementById('clipname').value = "";
                        document.getElementById('clipdesc').value = "";
                        document.getElementById('monetize').value = 'monetize';
                        document.getElementById('start1').value = "";
                        document.getElementById('end1').value = "";
                        $("#sharea").prop("href", "https://www.facebook.com/sharer/sharer.php?u=<?php echo base_url();?>index.php/home/showvideo/" + link)

                        if (clip == 'clip1')
                        {

                            document.getElementById('clipno').value = "clip2";
                        } else if (clip == 'clip2')
                        {

                            document.getElementById('clipno').value = "clip3";
                        } else if (clip == 'clip3')
                        {

                            document.getElementById('clipno').value = "clip4";
                        } else if (clip == 'clip4')
                        {

                            document.getElementById('clipno').value = "clip5";
                        } else if (clip == 'clip5')
                        {

                            document.getElementById('clipno').value = "clip6";
                        } else if (clip == 'clip6')
                        {

                            document.getElementById('clipno').value = "clip7";
                        } else if (clip == 'clip7')
                        {

                            document.getElementById('clipno').value = "clip8";
                        } else if (clip == 'clip8')
                        {

                            document.getElementById('clipno').value = "clip9";
                        } else if (clip == 'clip9')
                        {

                            document.getElementById('clipno').value = "clip10";
                        } else if (clip == 'clip10')
                        {

                            document.getElementById('abcd').style.display = "none";
                        } else
                        {
                            return true;
                        }
                    }
                });




            }



        }
    </script>
    <script>
        function displayamount()
        {

            document.getElementById('costbefore').style.display = "none";
            document.getElementById('costafter').style.display = "block";
        }
    </script>

    <script>
        function displayamount()
        {

            document.getElementById('costafter').style.display = "block";
        }
        function hideamount()
        {

            document.getElementById('costafter').style.display = "none";
        }
    </script> 

    <script>
        function validate_val(that)
        {
            var cost = ($('#cost').val());
            if (cost <= 0)
            {
                document.getElementById('costerror').style.display = "block";
                return false;
            }

        }
    </script>

        
        <script type="text/javascript">

            function ajaxSearchdata()
            {

                var input_data = $('#input-34').val();

                if (input_data.length === 0)
                {
                    $('#suggestions').hide();
                } else
                {

                    var post_data = {
                        'search_data': input_data,
                        '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                    };

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/home/searchAll",
                        data: post_data,
                        success: function (data) {
                            //console.log(data);
                            if (data.length > 0) {
                                $('#suggestions').show();
                                $('#autoSuggestionsList').addClass('auto_list');
                                $('#autoSuggestionsList').html(data);
                            }
                        }
                    });


                }

            }
        </script>

        <script type="text/javascript">

            function ajaxSearch()
            {

                var input_data = $('#input-32').val();

                if (input_data.length === 0)
                {
                    $('#suggestions').hide();
                } else
                {

                    var post_data = {
                        'search_data': input_data,
                        '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                    };

                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url() ?>index.php/home/searchAll",
                        data: post_data,
                        success: function (data) {
                            //console.log(data);
                            if (data.length > 0) {
                                $('#suggestionsbox').show();
                                $('#autoSuggestionsListbox').addClass('auto_list');
                                $('#autoSuggestionsListbox').html(data);
                            }
                        }
                    });


                }

            }
        </script>
<script>
            $(document).on("click", function (e) {
                if (!$("#suggestions").is(e.target)) {
                    $("#suggestions").hide();
                }
                if (!$("#suggestionsbox").is(e.target)) {
                    $("#suggestionsbox").hide();
                }
            });
        </script>
</body>
</html>