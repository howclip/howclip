<!doctype html>
<html lang="en">
   <head>
      <title>Press Single Page </title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
   </head>
   <body>
   	
      <?php include_once('header.php'); ?>
      <section class="banner">
         <div class="container">
           <div class="banner-heading">
               <h2>Press </h2>
               <p><a href="index.html">Home</a>  /  <a href="index.html">Press</a>  /   Star Wars Ut enim ad minim </p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="trending-section press-single-page">
				<div class="press-profile">
					<div class="press-img">
						<a href="#">
							<img src="<?php echo base_url();?>my-assets/images/press-img.png" alt="" />
						</a>	
					</div>
					<div class="press-detail">
						<ul>
							<li><span><i class="icon-full-name"></i></span>Username</li>
							<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
						</ul>
						<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
					</div>
				</div>
				<div class="about-channel">	 
					<div class="choose-our-channel">
						<p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem 	aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
						<ul>
							<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>

							<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>

							<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>

							<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>

							<li><span>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga </span></li>

						</ul>
					</div>
				</div>
				<div class="comment-section">
					<h3><i class="icon-comment-alt"></i>50 comments</h3>
					<div class="card content">
						<div class="card-body">
							<div class="row">
								<div class="col-md-2">
									<div class="user-pic">
										<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
									</div>	
								</div>
								<div class="col-md-10">
									<p>
										<a  href="#"><strong>Rishab Agarwal</strong></a>
										<span><i>1 day ago</i></span>
										<span class="float-right"><i class="icon-comment-dots"></i></span>

								   </p>
								   <div class="clearfix"></div>
									<p>Lorem Ipsum is simply dummy text of the pr make. </p>
									<p class="reply-section">
										<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
										<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
										<a class="float-right "> Reply</a>
								   </p>
								</div>
							</div>
						</div>
					</div>
					<div class="card content">
						<div class="card-body">
							<div class="row">
								<div class="col-md-2">
									<div class="user-pic">
										<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
									</div>	
								</div>
								<div class="col-md-10">
									<p>
										<a  href="#"><strong>Rishab Agarwal</strong></a>
										<span><i>1 day ago</i></span>
										<span class="float-right"><i class="icon-comment-dots"></i></span>

								   </p>
								   <div class="clearfix"></div>
									<p>Lorem Ipsum is simply dummy text of the pr make. </p>
									<p class="reply-section">
										<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
										<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
										<a class="float-right "> Reply</a>
								   </p>
								</div>
							</div>
						</div>
					</div>
					<div class="card content">
						<div class="card-body">
							<div class="row">
								<div class="col-md-2">
									<div class="user-pic">
										<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
									</div>	
								</div>
								<div class="col-md-10">
									<p>
										<a  href="#"><strong>Rishab Agarwal</strong></a>
										<span><i>1 day ago</i></span>
										<span class="float-right"><i class="icon-comment-dots"></i></span>

								   </p>
								   <div class="clearfix"></div>
									<p>Lorem Ipsum is simply dummy text of the pr make. </p>
									<p class="reply-section">
										<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
										<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
										<a class="float-right "> Reply</a>
								   </p>
								</div>
							</div>
						</div>
					</div>
					<a href="#" id="loadMore"><i>Load previous comments</i><span class="icon-angle-down"></span></a>
				</div>
				<div class="add-comment-section">
					<h3>Add a new comment</h3>
					<form>
						<textarea type="add" placeholder="Type Your Comment Here …..." rows="5"></textarea>
						<button type="submit" class="theme-btn">POST</button>
					</form>
				</div>
				
			 </div>	
		  </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
     
	<?php include_once('footer.php'); ?>
   </body>
</html>



