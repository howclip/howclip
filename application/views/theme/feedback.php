<!doctype html>
<html lang="en">
   <head>
      <title>Feedback</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
   </head>
   <body>
      <?php include_once('header.php'); ?>
   
      <section class="banner">
         <div class="container">
            <div class="banner-heading">
               <h2>Send feedback</h2>
               <p><a href="index.html">Home</a>  /  Send feedback</p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="uploading-vdo-section trending-section">
				<div class="heading">
                  <div class="heading-outer">
                     <h3>SEND YOUR  <span> VALUBLE FEEDBACK  </span></h3>
                  </div>
               	</div>
				<div class="profile-edit-form feed-back">
					<form>
						<div class="row">
						  <div class="col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail1">Your Name</label>
							  <input type="text" class="form-control" id="exampleInputEmail1" placeholder="First Name">
							</div>
						  </div>

						 <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail2">Your Email Id</label>
							  <input type="email" class="form-control" id="exampleInputEmail2" placeholder="mateusz.maaria@gmail.com">
							</div>
						  </div>
							
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label for="exampleInputEmail3">Your Mobile Number</label>
							  <input type="number" class="form-control" id="exampleInputEmail3" placeholder="XXXXXXXX">
							</div>
						  </div>

						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail4">Your Subject</label>
							  <input type="text" class="form-control" id="exampleInputEmail4" placeholder="Type here">
							</div>
						  </div>
						
						  <div class="col-lg-offset-0 col-lg-12 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label for="exampleInputEmail5">Write Your Feedback</label>
							  <textarea rows="5" class="form-control" id="exampleInputEmail5" placeholder="Type here"></textarea>
							</div>
						  </div>
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							
							<div class="vdo-post-btn-section">
								<button class="theme-btn" type="submit">Save Changes</button>
							</div>	

							<div class="vdo-post-btn-section">
								<button class="gray-theme-btn" type="submit">Cancel</button>
							</div>	

						 </div> 	
					  </div>
					</form>
			 	</div> 
			</div>
         </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
<?php include_once('footer.php'); ?>   
</body>
</html>


