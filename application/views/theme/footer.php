<footer>
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-6 col-sm-12">
                  <div class="about-section">
                     <div class="about-us">
                        <div class="footer-logo">
							<a href="index.php">
                           		<img src="<?php echo base_url();?>my-assets/images/logo.png" alt="" />
							</a>	
                        </div>
                        <div class="about-heading">
                           <h6><a href="about-us.html">ABOUT US</a></h6>
                        </div>
                     </div>
                     <p class="foooter-para">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown <a href="#" class="footer-more"><em>More...</em></a></p>
                  </div>
               </div>
               <div class="col-lg-2 col-md-6 col-sm-6">
                  <div class="contact-section address">
                     <h5>CONTACT US</h5>
                     <div class="contact-address">
                        <div class="icn">
                           <i class="icon-call"></i>
                        </div>
                        <div class="content">
                           <span>+44 345 678 903</span>
                        </div>
                     </div>
                     <div class="contact-address">
                        <div class="icn">
                           <i class="icon-email"></i>
                        </div>
                        <div class="content">
                           <a href="#"><span>adobexd@mail.com</span></a>
                        </div>
                     </div>
                     <div class="contact-address">
                        <div class="icn">
                           <i class="icon-address"></i>
                        </div>
                        <div class="content">
                          <span>497 Evergreen Rd. Roseville, CA 95673c  44 345 678 903</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-2 col-md-4 col-sm-6">
                  <div class="customer-section address">
                     <h5>CUSTOMER SERVICE</h5>
                     <ul>
                        <li><a href="#javascript:;">Contact Us</a></li>
                        <li><a href="<?php echo base_url('index.php/welcome/terms') ?>">Terms</a></li>
                        <li><a href="<?php echo base_url('index.php/welcome/privacy') ?>">Privacy</a></li>
                        <li><a href="<?php echo base_url('index.php/welcome/policy') ?>">Policy & Safety</a></li>
                        <li><a href="<?php echo base_url('index.php/welcome/feedback'); ?>">Send feedback</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-2 col-md-3 col-sm-6">
                  <div class="information-section address">
                     <h5>INFORMATION</h5>
                     <ul>
                        <li><a href="<?php echo base_url('index.php/welcome/about') ?>">About</a></li>
                        <li><a href="<?php echo base_url('index.php/welcome/press') ?>">Press</a></li>
                        <li><a href="#javascript:;">Copyright </a></li>
                        <li><a href="<?php echo base_url('index.php/welcome/creator') ?>">Creators</a></li>
                        <li><a href="<?php echo base_url('index.php/welcome/advertise') ?>">Advertise</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-3 col-md-5 col-sm-6">
                  <div class="subscribe-section">
                     <h5>SUBSCRIBE NEWSLETTER</h5>
                     <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                     <form>
                        <input type="email" placeholder="Email Address">
                        <button type="submit" class="subscribe-btn">Subscribe</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="mini-footer">
            <div class="container">
               <div class="copy-right">
                  <p>© 2019 howclip.com. All Rights Reserved</p>
               </div>
               <div class="arrow">
                  <i class="icon-arrow-up"></i>
               </div>
            </div>
         </div>
      </footer>

      <div class="modal fade" id="Modal-1" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/oUzs4ZuFyiI" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>

      <div class="modal fade form-sign-in popup-form" id="Modal-2" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content form-content">
               <div class="modal-header">
                  <p class="form-heading"><span> <img src="<?php echo base_url();?>my-assets/images/man(1).png" alt="" /></span>Please login to your account</p>
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
               </div>
               <div class="modal-body">
                  <div id="first">
                     <div class="myform form ">
                        
                           <div class="facebook-link">
                              <p>
                                 <a href="#" id="loginBtn1" class="signin-facebook btn mybtn"><i class="icon-facebook"></i>
                                  <span>Sign In  with Facebook</span>
                                 </a>
                              </p>
                           </div>
                           <div class="google-link">
                              <p>
                                 <a href="javascript:void();" class="google btn mybtn"><i class="icon-google"></i><span>Continue with Google
                                 </span> 
                                 </a>
                              </p>
                           </div>
                            <div style="color:#FF0000;text-align:center;" id="signformerror"></div>
                           
                           <form   method="post" name="signform" id="signform">
                           <div class="form-group email-img">
                              <input type="email" name="email" id="email"  class="form-control" placeholder="Email" >
                           </div>
                           <div class="form-group ps-img">
                              <input type="password" name="password" id="pass" class="form-control" placeholder="Password">
                           </div>
                           <div class=" text-center ">
                              <button type="button" id="log" class="pop-form-btn btn btn-block mybtn btn-primary tx-tfm" onClick="return getlogin();">Sign In</button>
                           </div>
                           </form>

                           <div class="form-group">
                              <p class="text-center forget-ps"><a href="#" data-toggle="modal" data-target="#Modal-4" >Forgot Password?</a></p>
                           </div>
                           <div class="col-md-12 ">
                              <div class="login-or">
                                 <hr class="hr-or">
                                 <span class="span-or">or</span>
                              </div>
                           </div>
                           <div class="form-group">
                              <p class="text-center any-account">Don't have account? <a href="#javascript:;" data-toggle="modal" data-target="#Modal-3" class="signup-link">Sign Up</a></p>
                           </div>
                        
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
     <div class="pop-up-serch search-section">
       <div class="container">
         <div class="search-form">
           <form>
             <div class="input-group">
               <div class="search-panel">
                 <select class="select2" >
                    <option value="" selected class="search-cat"> Category</option> 
                    <option value="co"> Comedy </option>
                    <option value="da"> Dance </option>
                    <option value="si"> Singing </option>
                 </select>
               </div>
               <input type="text" class="form-control" name="x" placeholder="Search here...">
               <span class="input-group-btn search-btn">
               <button class="btn btn-default" type="submit"><span><i class="icon-search"></i></span></button>
               </span> 
             </div>
           </form>
            <div class="remove-search">
                  <i class="icon-close"></i>
            </div>
         </div>
       </div>
     </div>
      <div class="modal fade popup-form" id="Modal-3" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content form-content">
               <div class="modal-header">
                  <p class="form-heading"><span> <img src="<?php echo base_url();?>my-assets/images/man(1).png" alt="" /></span>Please create your account</p>
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
               </div>
               <div class="modal-body">
                  <div id="first-1">
                     <div class="myform form ">
                     <?php if($this->session->flashdata('regerr'))
                              {
                              ?>
                                    
                        <div style="color:#FF0000;text-align:center;" > <?php echo $this->session->flashdata('regerr'); ?></div>
                                                   
                              <?php } ?>
                       <div style="color:#FF0000;text-align:center;" id="formerrors"></div>
                        <form method="post" name="myform" id="myformss" action="<?php echo base_url()?>index.php/welcome/registeruser">
                           
                           <div class="form-group user-img">
                              <input type="text" name="fname" id="fname" value=""  class="form-control"  placeholder="Full Name">
                           
                           </div>
                           <div style="color:#FF0000;text-align:center;display:none;" id="fnameerror">Enter Full Name</div>

                           <div class="form-group email-img">
                              <input type="email" name="email" id="email_addresss"  class="form-control" placeholder="Email">
                          <p id="errors" style="display:none;color:red;">Wrong email</p>
                           </div>
                           <div style="color:#FF0000;text-align:center;display:none;" id="mailerror">Enter Email Address</div>

                           <div class="form-group ps-img">
                              <input type="password" id="passwords" name="password" class="form-control"   placeholder="Password">
                           </div>
                           <div style="color:#FF0000;text-align:center;display:none;" id="passerror">Enter Password</div>
                           <div id="lengtherrs" style="display:none;color:#FF0000;margin-left:15px;">
                           <p style="color:#FF0000">Your password must be at least 8 characters!</p>
                          </div>
                           <div class="form-group">
                              <label for="remember-me" class="text-info remember"><span><input id="remember-me" name="remember-me" type="checkbox" required="required"></span><span>I’m in for emails with exciting discounts and personalized recommendations</span> </label><br>
                           </div>
                           
                           <div class=" text-center ">
                              <button type="button" onClick="return val()" class="pop-form-btn btn btn-block mybtn btn-primary tx-tfm">Sign Up</button>
                           </div>
                          
                           </form>

                           <div class="form-group">
                              <p class="text-center term">By logging in, you agree to our    <a href="<?php echo base_url() ?>index.php/welcome/terms ">Terms of Service</a> and <a href="<?php echo base_url() ?>index.php/welcome/policy">Privacy Policy</a></p>
                           </div>
                           <div class="col-md-12 ">
                              <div class="login-or">
                                 <hr class="hr-or">
                                 <span class="span-or">or</span>
                              </div>
                           </div>
                           <div class="form-group">
                              <p class="text-center any-account">Already have an account? <a href="javascript:void(0)" onclick="signs();" class="signin-link">Sign In</a></p>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      
      <div class="modal fade popup-form" id="Modal-4" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content form-content">
               <div class="modal-header">
                  <p class="form-heading"><span> <img src="<?php echo base_url();?>my-assets/images/man(1).png" alt="" /></span>Forgot Password</p>
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
               </div>
               <div class="modal-body">
                  <div id="first-2">
                     <div class="myform form ">
                        <form   method="post" name="login">
                           <div class="form-group email-img">
                              <input type="email" name="email"  class="form-control"   placeholder="Email" >
                           </div>
                           <div class=" text-center ">
                              <button type="submit" class="pop-form-btn btn btn-block mybtn btn-primary tx-tfm">Reset Password</button>
                           </div>
                           <div class="col-md-12 ">
                              <div class="login-or">
                                 <hr class="hr-or">
                                 <span class="span-or">or</span>
                              </div>
                           </div>
                           <div class="form-group">
                              <p class="text-center any-account">Already have an account? <a href="#" class="signin-link">Sign In</a></p>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <script   src="<?php echo base_url();?>my-assets/js/jquery.js"></script> 
      <script   src="<?php echo base_url();?>my-assets/js/owl.carousel.min.js"></script>
      <script    src="<?php echo base_url();?>my-assets/js/bootstrap.min.js"></script>
      <script   src="<?php echo base_url();?>my-assets/js/util.js"></script>
       <script   src="<?php echo base_url();?>my-assets/js/menu-aim.js"></script>
       <script   src="<?php echo base_url();?>my-assets/js/main.js"></script>
      <script   src="<?php echo base_url();?>my-assets/js/tab.js"></script>
	   <script   src="<?php echo base_url();?>my-assets/js/scrollbar.min.js"></script>
     
     
    
     
     


	<script>
				(function($){
					$(window).on("load",function(){


						$("#side-bar").mCustomScrollbar({
							scrollButtons:{enable:true,scrollType:"stepped"},
							keyboard:{scrollType:"stepped"},
							mouseWheel:{scrollAmount:188},
							theme:"rounded-dark",
							autoExpandScrollbar:true,
							snapAmount:188,
							snapOffset:65
						});

					});
				})(jQuery);
			</script>

      <script type="text/javascript">
        function signs(){
          
          $("#Modal-3").modal('hide');
          $("#Modal-2").modal('show');
        }
      </script>
      <script>	  
         $(".bar").click(function(){
         	$(".side-bar").toggleClass("show");
         })
         $(".white-close").click(function(){
         	$(".ad").addClass("display");
         	$("header").addClass("fix");
         })
         $(".arrow").click(function(){
         	$("body,html").animate({scrollTop : 0}, 500);
         })
         $(window).scroll(function(){
         	if($(document).scrollTop() > 48){
         		$("header").addClass("sticky");
         	}
         	else{
         		$("header").removeClass("sticky");
         	}
         })
         
         
      </script>
      
      <script>

         (function($) {
         $.fn.menumaker = function(options) {  
         var cssmenu = $(this), settings = $.extend({
          format: "dropdown",
          sticky: false
         }, options);
         return this.each(function() {
          $(this).find(".button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.slideToggle().removeClass('open');
            }
            else {
              mainmenu.slideToggle().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }
            }
          });

          cssmenu.find('li ul').parent().addClass('has-sub');
         multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideToggle();
              }
              else {
                $(this).siblings('ul').addClass('open').slideToggle();
              }
            });
          };

          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
          if (settings.sticky === true) cssmenu.css('position', 'fixed');
          $("ul").each(
        function() {
        var elem = $(this);
       if (elem.children().length == 0) {
         elem.remove();
         }
        }
        );
         resizeFix = function() {
         var mediasize = 1000;
            if ($( window ).width() > mediasize) {
              cssmenu.find('ul').show();
            }
            if ($(window).width() <= mediasize) {
              cssmenu.find('ul').hide().removeClass('open');
            }
          };
          resizeFix();
          return $(window).on('resize', resizeFix);
         });
         };
         })(jQuery);
         
         (function($){
         $(document).ready(function(){
         $("#cssmenu").menumaker({
          format: "multitoggle"
         });
         });
         })(jQuery);
         
      </script>
	
   <script>
		$('.select2').select2({
});
		

	</script>

	   <script>
         $(".forget-ps a").click(function(){
			 
			$("#Modal-2").css({"display":"none"});
			$(".modal-backdrop").css({"display":"none"});
         	$("#Modal-2").removeClass("in");
         })
		
      </script>

       <script>
         $('.pop').popover().click(function () {
             setTimeout(function () {
                 $('.pop').popover('hide');
             }, 2000);
         });
         
      </script>
      <script>
    function checkmail(id){

      var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(id);
   
     $.ajax({
                    url: "<?php echo base_url(); ?>index.php/welcome/checkemail",
                    //url  : "http://livesoftwaresolution.net/lms/Adminlogin/subjectselect",
                    type: "POST",
                    data: {'id': id},
                    
                    success: function (response)
                    {
                     
                        if (response=="yes") {
                                $('#error').show();
                          $('#error').html("Username already exists. ");
                          

                          
                        }
                        else{
                             if(!re ) {
        $('#error').show();
        $('#error').html(" Wrong email.");
    } else {
        $('#error').hide();
    }
                        }

                    }

                });   
      }

 $('#email_addresss').on('change', function() {
   //alert(this.value);
  
    var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{2,4}/.test(this.value);
    //alert(re);
    if(!re ) {
        $('#errors').show();
    } else {
        $('#errors').hide();
    }
})
  </script>
      <script type="text/javascript">
         function ajaxSearch()
         {
         
             var input_data = $('#input-31').val();
         
             if (input_data.length === 0)
             {
                 $('#suggestions').hide();
             } else
             {
         
                 var post_data = {
                     'search_data': input_data,
                     '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                 };
         
                 $.ajax({
                     type: "POST",
                     url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                     data: post_data,
                     success: function (data) {
                         //console.log(data);
                         if (data.length > 0) {
                             $('#suggestions').show();
                             $('#autoSuggestionsList').addClass('auto_list');
                             $('#autoSuggestionsList').html(data);
                         }
                     }
                 });
         
         
             }
         
         }
      </script>
      <script>
         function checkbox()
         {
             if (!this.form.checkbox.checked)
             {
                 alert('You must agree to the terms first.');
                 return false;
             }
         }
      </script>
      <script>
         function validate_pass(that)
         {
             var pass = ($('#passwords').val());
             var cpass = ($('#confirm_password').val());
             if (pass.length < 8)
             {
                 $("#lengtherr").show();
                 return false;
             }
             if (pass != cpass)
             {
         
                 $("#selectPeriodRangePanel").show();
         
                 return false;
             }
         }
      </script>
      <script>
   function val(){

    var uname = document.getElementById('fname').value;
   
   var femail = document.getElementById('email_addresss').value;

   var fpassword = document.getElementById('passwords').value;
   
    if(!uname){
     
        document.getElementById("formerrors").innerHTML = "Enter Full Name";
        return false;
    }
    
   else if(!femail){
     
        document.getElementById("formerrors").innerHTML = "Enter Email Address";
        return false;
    }
   else if(!fpassword){
        document.getElementById("formerrors").innerHTML = "Enter Password";
        return false;
    }
   

   else if(fpassword.length < 8){
      //alert(fpassword);
      $("#lengtherrs").show();
   setTimeout(function() {
    $('#lengtherrs').fadeOut('fast');}, 1000);
        return false;
    }
   
    else{
        
        //return true;
        $("#myformss").submit(); // Submit the form
    }
   
}
   
  </script>
     <!--  <script>
         function validation() {
         
             var fname = document.form.fname.value;
             var lname = document.form.lname.value;
             var femail = document.form.email.value;
             var fpassword = document.form.password.value;
             var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{1,2}/.test(femail);
         
             if (fname == "")
             {
         
                 document.getElementById('fnameerror').style.display = 'block';
                 setTimeout(function () {
                     $('#fnameerror').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (lname == "")
             {
         
                 document.getElementById('lnameerror').style.display = 'block';
                 setTimeout(function () {
                     $('#lnameerror').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (femail == "")
             {
                 document.getElementById('mailerror').style.display = 'block';
                 setTimeout(function () {
                     $('#mailerror').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (fpassword == "")
             {
         
                 document.getElementById('passerror').style.display = 'block';
                 setTimeout(function () {
                     $('#passerror').fadeOut('fast');
                 }, 1000);
                 return false;
         
             }
             if (fpassword.length < 8)
             {
                 $("#lengtherr").show();
                 setTimeout(function () {
                     $('#lengtherr').fadeOut('fast');
                 }, 1000);
                 return false;
             }
             if (!re)
             {
                 document.getElementById("formerror").innerHTML = "Enter valid Email";
                 setTimeout(function () {
                     $('#formerror').fadeOut('fast');
                 }, 1000);
         
                 return false;
             }
         
         }
      </script> -->
      <script>
         $(document).on("click", function (e) {
             if (!$("#suggestions").is(e.target)) {
                 $("#suggestions").hide();
             }
         });
          $("#check").on("click", function (e) {
            $('#emailAdd').val('');
            $('#pass').val('');
         });
      </script>
     
      <script>
         function getlogin()
         {
         
         
             var email = document.getElementById("email").value;
             var password = document.getElementById("pass").value;
         
             if (email == "")
             {
                 document.getElementById("signformerror").innerHTML = "Enter the Email Address";
                 return false;
             } else if (password == "")
             {
         
                 document.getElementById("signformerror").innerHTML = "Enter the Password";
                 return false;
             } else
             {
         
                 var sign_data = {
                     'email': email,
                     'password': password,
                 };

         
                 $.ajax({
                     type: "POST",
                     url: "<?php echo base_url() ?>index.php/welcome/login",
                     data: sign_data,
                     success: function (data) {
                         console.log(data);
                         
                         if (data.length > 0) {
                             if (data == 'success')
                             {
                                 window.location = "<?php echo base_url() ?>index.php/home/welcome";
                             } else
                             {
                              //alert("hey");
                                 document.getElementById("signformerror").innerHTML = data;
                                 
                             return false;
                             }
         
                         }
                     }
                 });
         
         
         
             }
         
         
         }
         $('#pass').keypress(function (e) {
             if (e.which == '13') {
                 getlogin();
             }
         });
      </script>
      <script type="text/javascript">
         function aaa(x,y){
             var email = x;
              var name = y;
             // alert();
              $.ajax({
              url: '<?php echo base_url(); ?>index.php/welcome/insertfbdata',
                 type: "POST",
                 data: {'email': email,'user':name
                 },
                 success: function (response)
                 {
                    // alert(response);return false;
                    //alert('<?php echo base_url(); ?>index.php/home/dash');
         window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                 }
         
             });
         }
           function SubmitUserData(email) {
             alert(email);
              return false;
             $.ajax({
                 url: '<?php echo base_url(); ?>index.php/home/insertfbdata',
                 type: "POST",
                 data: {'email': email
                 },
                 success: function (response)
                 {
                     window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                 }
         
             });
         }
         
         function getUserData() {
             FB.api('/me?fields=id,first_name,last_name,email,link,gender,locale,picture', function (response) {
                 document.getElementById('response').innerHTML = 'Hello ' + response.id + '<br>' + response.email + '<br>' + response.first_name + ' ' + response.last_name + '<br>' + response.gender;
                var uname = response.first_name;
         var email = response.email;
         
         
                 aaa(email,uname);
             });
         }
         
         
         window.fbAsyncInit = function () {
             FB.init({
                 appId: '1884735515112965',
                 xfbml: true,
                 version: 'v2.6'
             });
         };
         
         (function (d, s, id) {
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {
                 return;
             }
             js = d.createElement(s);
             js.id = id;
             js.src = "//connect.facebook.net/en_US/sdk.js";
             fjs.parentNode.insertBefore(js, fjs);
         }(document, 'script', 'facebook-jssdk'));
         
         document.getElementById('loginBtn').addEventListener('click', function () {
         
             FB.login(function (response) {
                 if (response.authResponse) {
                     //user just authorized your app
                     document.getElementById('loginBtn').style.display = 'none';
                     getUserData();
         
                 }
             }, {scope: 'email,public_profile', return_scopes: true});
         }, false);
         document.getElementById('loginBtn1').addEventListener('click', function () {
         
             FB.login(function (response) {
                 if (response.authResponse) {
                     //user just authorized your app
                    // document.getElementById('loginBtn1').style.display = 'none';
                     getUserData();
         
                 }
             }, {scope: 'email,public_profile', return_scopes: true});
         }, false);
         
      </script>
      <script>
         function SubmitUserData(email) {
             $.ajax({
                 url: '<?php echo base_url(); ?>index.php/home/insertfbdata',
                 type: "POST",
                 data: {'email': email
                 },
                 success: function (response)
                 {
                     //alert(response);
                     //console.log(response);
                     window.location.href = '<?php echo base_url(); ?>index.php/home/dash';
                 }
         
             });
         }
         function onSignIn(googleUser) {
         
             var profile = googleUser.getBasicProfile();
             console.log("ID: " + profile.getId());
             document.getElementById('gid').innerHTML = profile.getId();
             console.log('Full Name: ' + profile.getName());
             document.getElementById('gname').innerHTML = profile.getName();
             document.getElementById('gmail').innerHTML = profile.getEmail();
             console.log('Given Name: ' + profile.getGivenName());
             console.log('Family Name: ' + profile.getFamilyName());
             console.log("Image URL: " + profile.getImageUrl());
             console.log("Email: " + profile.getEmail());
         
             var id_token = googleUser.getAuthResponse().id_token;
             console.log("ID Token: " + id_token);
             var email = profile.getEmail();
             //alert(email);
             SubmitUserData(email);
         
         }
         
         $('#chkval').click(function (e) {
             e.preventDefault();
             var input = document.getElementById("input-31").value;
             if (input == '') {
                 alert('Please Type Text For Search!');
                  e.preventDefault();
             }
             else{
                 document.getElementById('my_form').submit();
             }
         });
      </script>
      <script>
      
         $(document).ready(function(){
        $(".comment-section .content").slice(0, 1).show();
        $("#loadMore").on("click", function(e){
         e.preventDefault();
         $(".content:hidden").slice(0, 4).slideDown();
         if($(".content:hidden").length == 0) {
           $("#loadMore").text("No Content").addClass("noContent");
         }
        });

      })



        <?php  if($this->session->flashdata('regerr')){ ?>

$(document).ready(function(){

$('#Modal-3').modal('show');
    
});

        <?php } ?>
      </script> 
