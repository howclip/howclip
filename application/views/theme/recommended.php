<!doctype html>
<html lang="en">
   <head>
      <title>Recommended Clips</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
     
   </head>
   <body>
      <?php include_once('header.php'); ?>
      <section class="banner">
         <div class="container">
            <div class="banner-heading">
               <h2>Recommended Clips</h2>
               <p><a href="index.html">Home </a> /  Recommended Clips</p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="recommanded wrapper">
            <section class="trending-filter-section">
				
               <div class="grid-item-section">
				   <div class="filter-section">
					<div class="well-1 well-sm">
                     <p class="filter-heading">Filter By</p>
						<a href="" class="refresh"><span><i class="icon-refresh"></i></span></a>
                     
                  	</div>
				</div>
                  <div class="well well-sm">
					  
                     <span class="result">Showing all 350 results</span>
					     	
					  <div class="right-side-grid">
						
						  <label>Date Wise : </label>
						  <span class="select-cat">
							  <select class="select2" >
								<optgroup label=" this week">
								  <option value="co" selected> This Week </option>
								  <option value="da"> This Month </option>
								  <option value="si"> This 	Year </option>
								</optgroup>
							  </select>
						  </span>	  
						 <div class="btn-group">
							<a href="#" id="grid" class="btn btn-default btn-lg active"><i class="icon-th"></i></a><a href="#" id="list" class="btn btn-default btn-lg"><i class="icon-list-ul"></i></a> 
						 </div>
					  </div>
                  </div>
                  <div id="products" class="row list-group all-videos">
                     <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div> 
                     <div class="item   col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item  col-lg-3 col-md-4 col-sm-6">
						 <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                        
                     </div>
                     <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
					 <div class="item  col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div> 
                  </div>
					<div class="pagination">
					  
						
							<a href="#" class="prev-anchor"><i class="icon-angle-left"></i> </a>
							
							  <a href="#">1</a><a href="#" class="active">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a>
							
							<a href="#" class="next-anchor"><i class="icon-angle-right"></i> </a>
							
					 
					</div>
				   </div>
              
            </section>
         </div>
      </div>
      <div class="passage-section">
         <div class="container">
            <div class="recent-vdo-section">
               <div class="heading">
                  <div class="heading-outer">
                     <h3>RECENT UPLOAD <span> CLIP</span></h3>
                  </div>
               </div>
               <div class="all-videos">
                  <div id="owl-demo" class="owl-carousel owl-theme">
                     <div class="item">
                        <div class="video-section">
                           <div class="video-img">
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <div class="payble">
                                 <a href="javascript:;">
                                    <h4><span>$</span>2.00</h4>
                                    <span>View</span><i class="icon-play"></i><span>Clip</span>
                                 </a>
                              </div>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="video-section">
                           <div class="video-img">
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <div class="payble">
                                 <a href="javascript:;">
                                    <h4><span>$</span>2.00</h4>
                                    <span>View</span><i class="icon-play"></i><span>Clip</span>
                                 </a>
                              </div>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="item">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
				
            </div>
         </div>
      </div>
     
	
      <script>
         $(".forget-ps a").click(function(){
         	$(".form-sign-in").toggleClass("show");
         	$(".modal-backdrop").removeClass("show");
         })
      </script>
      <script>
         $(document).ready(function() {
           var owl = $("#owl-demo");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 4
         	  }
         	}
           });
         
         });
      </script> 
      <script>
         (function($) {
         $.fn.menumaker = function(options) {  
         var cssmenu = $(this), settings = $.extend({
          format: "dropdown",
          sticky: false
         }, options);
         return this.each(function() {
          $(this).find(".button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.slideToggle().removeClass('open');
            }
            else {
              mainmenu.slideToggle().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }
            }
          });
          cssmenu.find('li ul').parent().addClass('has-sub');
         multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideToggle();
              }
              else {
                $(this).siblings('ul').addClass('open').slideToggle();
              }
            });
          };
          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
          if (settings.sticky === true) cssmenu.css('position', 'fixed');
         resizeFix = function() {
         var mediasize = 1000;
            if ($( window ).width() > mediasize) {
              cssmenu.find('ul').show();
            }
            if ($(window).width() <= mediasize) {
              cssmenu.find('ul').hide().removeClass('open');
            }
          };
          resizeFix();
          return $(window).on('resize', resizeFix);
         });
         };
         })(jQuery);
         
         (function($){
         $(document).ready(function(){
         $("#cssmenu").menumaker({
          format: "multitoggle"
         });
         });
         })(jQuery);
         
      </script>
      <script  >$(document).ready(function() {
         $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
         $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
         });
		  $('.select2').select2({
		  });
		  
      </script>
	<script  >
		 $(".filter-heading .icon-list").click(function(){
			  $(".filter-heading .icon-remove").show();
			  $(".filter-heading .icon-list").hide();
			 $(".category-wrapper").addClass("display");
		  })
		   $(".filter-heading .icon-remove").click(function(){
			   $(".filter-heading .icon-list").show();
			  $(".filter-heading .icon-remove").hide();
			   
			 $(".category-wrapper").removeClass("display");
			   
		  })
		
		
		$("#grid").click(function(){
			  $("#grid").addClass("active");
			  $("#list").removeClass("active");
		  })
		   $("#list").click(function(){
			   $("#grid").removeClass("active");
			  $("#list").addClass("active");
		  })
	</script>
	
<?php include_once('footer.php'); ?>
   </body>
</html>

