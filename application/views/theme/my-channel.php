<!doctype html>
<html lang="en">
   <head>
      <title>HowClip</title>
   </head>
   <body>
      <?php include_once('header.php');?>
      <div class="mychannel-banner" style="background-image: url(<?php echo base_url();?>my-assets/images/mychannelbg.png);">
         <div class="container">
           
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </div>
      <div class="container">
         <div class="wrapper">
            <div class="uploading-vdo-section trending-section my-channel-des">
				<div class="description">
					<div class="vdo-description">
						<div class="vdo-title">

                            <?php
                            foreach ($userDetail as $user) {
                                $username = $user->username;
                                $userimg = $user->userLogo;
                                if ($userimg == "") {
                                    $userimg = "user.png";
                                } else {
                                    $userimg = $user->userLogo;
                                }
                            }
                            ?>
							<div class="row">
								 <?php
                            $total = 0;
                            foreach ($userdata as $user) {
                                $totalview = $user->views;
                                $total +=$totalview;
                            }
                            ?>
								<div class="col-lg-2 col-md-2 col-sm-6">
									<div class="chennel-profile">
										<img width="240" height="200" src="<?php echo base_url(); ?>uploads/<?php echo $userimg; ?>" alt="" />
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<h3><?php echo ucwords($username); ?></h3>
									<ul>
										<?php foreach ($userdata['subscriber'] as $sub) { 

											?><?php } ?>
										<li><a href="#"><span class="invite"><i class="icon-subscribe"></i><?php
                                                    if ($sub == "") {
                                                        echo "0";
                                                    } else {
                                                        echo $sub;
                                                    }
                                                    ?></span><strong>Subscriber</strong></a></li>
										<li><a href="#"><span><i class="icon-playlist"></i></span><strong>Video Manager</strong> </a></li>
									</ul>
								</div>	
								<div class="col-lg-4 col-md-4">
									<div class="vdo-category">
										<p>Category : <a href="#">Creative Professions</a></p>
									</div>	
									<div class="subscribe">
										<a href="#"><i class="icon-eye-open"></i><span><?php
                                                if ($total == "") {
                                                    echo "0";
                                                } else {
                                                    echo $total;
                                                }
                                                ?> Views</span></a>
										<a href="#" class="theme-btn">Subscribe</a>

									</div>
								</div>
							</div>	
						</div>
						
						
						
						
						 <div id="parentHorizontalTab">
							<ul class="resp-tabs-list hor_1">
								<li>HOME</li>
								<li>VIDEOS</li>
								<li>PLAYLIST</li>
								<li>CHANNELS</li>
								<li>COMMUNITY</li>
								<li>ABOUT</li>
							</ul>
							<div class="resp-tabs-container hor_1">
								<div>
									<div class="row">
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="all-category-vdo-section">
												<div class="category-videos">
													<div class="heading">
													  <div class="heading-outer">
														 <h3>TOP <span> TRENDING VIDEO</span></h3>
													  </div>
													  <a href="#javascript:;" class="view-all">View All <span><img src="<?php echo base_url();?>my-assets/images/red-arrow.png" alt="" /></span></a> 
												   	</div>
													<div class="all-videos">
													  <div id="owl-demo1" class="owl-carousel owl-theme">
														 <div class="item">
															<div class="video-section">
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
													  </div>
												   </div>	
												</div>
												<div class="category-videos">
													<div class="heading">
													  <div class="heading-outer">
														 <h3>POPULAR <span>  VIDEO CLIP</span></h3>
													  </div>
													  <a href="#javascript:;" class="view-all">View All <span><img src="<?php echo base_url();?>my-assets/images/red-arrow.png" alt="" /></span></a> 
												   	</div>
													<div class="all-videos">
													  <div id="owl-demo2" class="owl-carousel owl-theme">
														 <div class="item">
															<div class="video-section">
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
													  </div>
												   </div>	
												</div>
												<div class="category-videos">
													<div class="heading">
													  <div class="heading-outer">
														 <h3>INSPIRATIONAL <span> VIDEOS </span></h3>
													  </div>
													  <a href="#javascript:;" class="view-all">View All <span><img src="<?php echo base_url();?>my-assets/images/red-arrow.png" alt="" /></span></a> 
												   	</div>
													<div class="all-videos">
													  <div id="owl-demo3" class="owl-carousel owl-theme">
														 <div class="item">
															<div class="video-section">
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
														 <div class="item">
															<div class="video-section">
															   <div class="video-img"> 
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
															   </div>
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
															   <div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																  </ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
													  </div>
												   </div>	
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12">
											<div class="related-vdo">
												<div class="other-video-clip">	
													<div class="any-videos">
														<div class="auto-mode">
															<span class="up-next">Featured Channels</span>

														</div>	
														<?php

                                                foreach ($AllSubscription as $allsub) {


                                                    if ($allsub['userLogo'] == "") {
                                                        $llogo = "user.png";
                                                    } else {
                                                        $llogo = $allsub['userLogo'];
                                                    }
                                                    ?>	
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img width="70" height="70" alt="..." src="<?php echo base_url(); ?>uploads/<?php echo $llogo; ?>" alt="" />
																		</div>
																	</a>
																</div>

																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title"><?php echo ucwords($allsub['username']); ?></p>

																		<button id="<?php echo "sub" . $allsub['id']; ?>" href="javascrit:void;" onClick="updatesubscription1('<?php echo $allsub['id']; ?>', 'subscribe', '<?php echo $allsub['username']; ?>');" class="bdr-theme-btn">Subscribe</button>
																		<button id="<?php echo "unsub" . $allsub['id']; ?>" style="display:none;"  href="javascrit:void;" onClick="updatesubscription1('<?php echo $allsub['id']; ?>', 'unsubscribe');" class="bdr-theme-btn">UnSubscribe</button>
																	
																	</div>

																</div>

															</div>
														</div>
												
														 <?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div>
									<div class="recommanded">
										<section class="trending-filter-section">

										   <div class="grid-item-section">
											   <div class="filter-section">
												<div class="well-1 well-sm">
												 <p class="filter-heading">Filter By</p>
													<a href="" class="refresh"><span><i class="icon-refresh"></i></span></a>

												</div>
											</div>
											  <div class="well well-sm">

												 <span class="result">Showing all 350 results</span>
													 	
												  <div class="right-side-grid">
													<label>Date Wise : </label>
													  <span class="select-cat">
														  <select class="select2" >
															<optgroup label=" this week">
															  <option value="co" selected> This Week </option>
															  <option value="da"> This Month </option>
															  <option value="si"> This 	Year </option>
															</optgroup>
														  </select>
													  </span>  
													 <div class="btn-group">
														<a href="#" id="grid" class="btn btn-default btn-lg active"><i class="icon-th"></i></a><a href="#" id="list" class="btn btn-default btn-lg"><i class="icon-list-ul"></i></a> 
													 </div>
												  </div>
											  </div>
											  <div id="products" class="row list-group all-videos">
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div> 
												 <div class="item   col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													 <div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>

												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="item  col-lg-3 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div> 
											  </div>
												<div class="pagination">
														<a href="#" class="prev-anchor"><i class="icon-angle-left"></i> </a>
														  <a href="#">1</a><a href="#" class="active">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a>
														<a href="#" class="next-anchor"><i class="icon-angle-right"></i> </a>
												</div>
											   </div>

										</section>
									 </div>

								</div>

								<div>
									
									<div class="all-videos playlist-channel">
									  <div class="row">
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <div class="payble">
													 <a href="javascript:;">
														<h4><span>$</span>2.00</h4>
														<span>View</span><i class="icon-play"></i><span>Clip</span>
													 </a>
												  </div>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
									  </div>
									<div class="row">
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <div class="payble">
													 <a href="javascript:;">
														<h4><span>$</span>2.00</h4>
														<span>View</span><i class="icon-play"></i><span>Clip</span>
													 </a>
												  </div>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
										 <div class="col-lg-3 col-md-4 col-sm-6">
											<div class="video-section">
											   <div class="video-img"> 
												  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
												  <span class="duration">10 Videos</span>
												  <a href="javascript:;"><i class="icon-play drk-play-btn"><span>Play all</span></i></a>
											   </div>
											   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
											   <div class="about-video">
												  <ul>
													 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
													 <li class="date">9 Apr, 2019</li>
													 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
												  </ul>
											   </div>
											   <div class="video-btn">
												  <ul>
													 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
													 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
												  </ul>
											   </div>
											</div>
										 </div>
									  </div> 
								   </div>
									<div class="pagination">
											<a href="#" class="prev-anchor"><i class="icon-angle-left"></i> </a>
											  <a href="#">1</a><a href="#" class="active">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a>
											<a href="#" class="next-anchor"><i class="icon-angle-right"></i> </a>
									</div>
									
								</div>
								 
								<div>
									<div class="row">
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="channel-subscribe-page">
												<div class="row">
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													<div class="col-lg-3 col-md-4 col-sm-6">
														<div class="subscribe-list">
															<div class="subscribe-user">
																<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
															</div>
															<h4>Chennel Name</h4>
															<a href="#"><span class="invite"><i class="icon-subscribe"></i>54789</span><strong>Subscribe</strong></a>
															<button type="button" class="subscribe-cnl-btn">Subscribe</button>
														</div>
													</div>
													
														
												</div>
												
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12">
											<div class="related-vdo">
												<div class="other-video-clip">
													
													<div class="any-videos">
														<div class="auto-mode">
															<span class="up-next">Featured Channels</span>

														</div>		
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
												
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="any-videos">
														<div class="auto-mode">
															<span class="up-next">Related Channels</span>
														</div>		
														
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
													
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div>
									<div class="row">
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="channel-comment-page">
												<div class="community">
												 <div class="all-videos">	 
													<div class="video-section">
														<div class="row">
															<div class="col-lg-4 col-md-4 col-sm-4">	
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															</div>	
															<div class="col-lg-8 col-md-8 col-sm-8">
																<div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 
																  </ul>
															   
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit Duis aute irure dolor in adipsicing elit</a></h6>
																<ul>	
																	<li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																</ul>	
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																	  <li class="view-all-cmt"><a href="#javascript:;"><i class="icon-angle-down"></i><span>View all 97 Comments</span></a></li> 
																  </ul>
															   </div>
															</div>
														 </div>
													 </div>
												</div>	 
												<div class="comment-section">
													<h3><i class="icon-comment-alt"></i>70 comments</h3>
													<div class="card content">
															<div class="card-body">
																<div class="row">
																	<div class="col-md-2">
																		<div class="user-pic">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																		</div>	
																	</div>
																	<div class="col-md-10">
																		<p>
																			<a  href="#"><strong>Rishab Agarwal</strong></a>
																			<span><i>1 day ago</i></span>
																			<span class="float-right"><i class=" icon-comment-dots"></i></span>

																	   </p>
																	   <div class="clearfix"></div>
																		<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																		<p class="reply-section">
																			<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																			<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																			<a class="float-right "> Reply</a>
																	   </p>
																	</div>
																</div>
															</div>
														</div>
														<div class="card content">
															<div class="card-body">
																<div class="row">
																	<div class="col-md-2">
																		<div class="user-pic">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																		</div>	
																	</div>
																	<div class="col-md-10">
																		<p>
																			<a  href="#"><strong>Jugnun Agarwal</strong></a>
																			<span><i>1 day ago</i></span>
																			<span class="float-right"><i class=" icon-comment-dots"></i></span>

																	   </p>
																	   <div class="clearfix"></div>
																		<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																		<p class="reply-section">
																			<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																			<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																			<a class="float-right "> Reply</a>
																	   </p>
																	</div>
																</div>
															</div>
														</div>
														<div class="card content">
															<div class="card-body">
																<div class="row">
																	<div class="col-md-2">
																		<div class="user-pic">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																		</div>	
																	</div>
																	<div class="col-md-10">
																		<p>
																			<a  href="#"><strong>Abhay Agarwal</strong></a>
																			<span><i>1 day ago</i></span>
																			<span class="float-right"><i class=" icon-comment-dots"></i></span>

																	   </p>
																	   <div class="clearfix"></div>
																		<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																		<p class="reply-section">
																			<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																			<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																			<a class="float-right "> Reply</a>
																	   </p>
																	</div>
																</div>
															</div>
														</div>
														<div class="card content">
															<div class="card-body">
																<div class="row">
																	<div class="col-md-2">
																		<div class="user-pic">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																		</div>	
																	</div>
																	<div class="col-md-10">
																		<p>
																			<a  href="#"><strong>Avinash</strong></a>
																			<span><i>1 day ago</i></span>
																			<span class="float-right"><i class=" icon-comment-dots"></i></span>

																	   </p>
																	   <div class="clearfix"></div>
																		<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																		<p class="reply-section">
																			<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																			<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																			<a class="float-right "> Reply</a>
																	   </p>
																	</div>
																</div>
															</div>
														</div>
														<div class="card content">
															<div class="card-body">
																<div class="row">
																	<div class="col-md-2">
																		<div class="user-pic">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																		</div>	
																	</div>
																	<div class="col-md-10">
																		<p>
																			<a  href="#"><strong>Mukul</strong></a>
																			<span><i>1 day ago</i></span>
																			<span class="float-right"><i class=" icon-comment-dots"></i></span>

																	   </p>
																	   <div class="clearfix"></div>
																		<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																		<p class="reply-section">
																			<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																			<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																			<a class="float-right "> Reply</a>
																	   </p>
																	</div>
																</div>
															</div>
														</div>
													<a href="#" id="loadMore"><i>Load previous comments</i><span class="icon-angle-down"></span></a>
												</div>
												<div class="add-comment-section">
													<h3>Add a new comment</h3>
													<form>
														<textarea  placeholder="Type Your Comment Here …..." rows="5"></textarea>
														<button type="submit" class="theme-btn">POST</button>
													</form>
												</div>
												</div>	
												<div class="all-videos">	 
													<div class="video-section">
														<div class="row">
															<div class="col-lg-4 col-md-4 col-sm-4">	
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															</div>	
															<div class="col-lg-8 col-md-8 col-sm-8">
																<div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 
																  </ul>
															   
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit Duis aute irure dolor in adipsicing elit</a></h6>
																<ul>	
																	<li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																</ul>	
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																	  <li class="view-all-cmt"><a href="#javascript:;"><i class="icon-angle-down"></i><span>View all 97 Comments</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
													 </div>
												</div>
												<div class="all-videos">	 
													<div class="video-section">
														<div class="row">
															<div class="col-lg-4 col-md-4 col-sm-4">	
															   <div class="video-img">
																  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
																  <span class="duration">11:15</span>
																  <div class="payble">
																	 <a href="javascript:;">
																		<h4><span>$</span>2.00</h4>
																		<span>View</span><i class="icon-play"></i><span>Clip</span>
																	 </a>
																  </div>
															   </div>
															</div>	
															<div class="col-lg-8 col-md-8 col-sm-8">
																<div class="about-video">
																  <ul>
																	 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
																	 <li class="date">9 Apr, 2019</li>
																	 
																  </ul>
															   
															   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit Duis aute irure dolor in adipsicing elit</a></h6>
																<ul>
																	<li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
																</ul>
															   </div>
															   <div class="video-btn">
																  <ul>
																	 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
																	 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
																	  <li class="view-all-cmt"><a href="#javascript:;"><i class="icon-angle-down"></i><span>View all 97 Comments</span></a></li>
																  </ul>
															   </div>
															</div>
														 </div>
													 </div>
												</div>
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12">
											<div class="related-vdo">
												<div class="other-video-clip">
													
													<div class="any-videos">
														<div class="auto-mode">
															<span class="up-next">Featured Channels</span>

														</div>		
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
												
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="any-videos">
														<div class="auto-mode">
															<span class="up-next">Related Channels</span>
														</div>		
														
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
													
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div>
									<div class="row">
										<div class="col-lg-9 col-md-9 col-sm-12">
											<div class="about-channel">
												 <h3>ABOUT  <span> CHENNALS</span></h3>
												 <p>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting 	change billionaire philanthropy revita research. Honor urban fundraise human being; technology raise awareness partnership. Political global growth cross-agency coordnation the  global financial system. Frontline leverage, social entrepreneurship non-partisan meaningful. </p> 
												<div class="social-info">
													<h5>Published on <i class="icon-clock"></i> May 21, 2019</h5>
													<h5>Our Website <i class="icon-website-link"></i><a href="#"> www.yourdoaminname.com</a></h5>
													<h5>Channel Link <i class="icon-video-camera"></i> <a href="#">https://howclip.com/youchannelname</a></h5>
													<h5 class="share-icn">Social Share <i class="icon-social-share"></i></h5> 
													<div class="login-with">
														<ul class="social-share">
															<li class="google"><a href="#" ><i class="icon icon-google-plus"></i></a></li>
															<li class="envelope"><a href="#" ><i class="icon  icon-social-mail"></i></a></li>
															<li class="facebook"><a href="#" ><i class="icon icon-facebook"></i></a></li>
															<li class="twitter"><a href="#" ><i class="icon icon-Twitter"></i></a></li>
														</ul>
													</div>
												</div>
												<div class="choose-our-channel">
													<h5>Why watch our Channels?</h5>
													<ul>
														<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>
														<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King. </span></li>
														<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>
													</ul>
												</div>			
												
											</div>
										</div>
										<div class="col-lg-3 col-md-3 col-sm-12">
											<div class="related-vdo">
												<div class="other-video-clip">
													
													<div class="any-videos">
														<div class="auto-mode">
															<span class="up-next">Featured Channels</span>

														</div>		
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
												
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div class="any-videos">
														<div class="auto-mode">
															<span class="up-next">Related Channels</span>
														</div>		
														
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
														<div class="up-nxt-vdo">
															<div class="row">
																<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
																	<a href="#">
																		<div class="subscribe-img">
																			<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																		</div>
																	</a>
																</div>
																<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
																	<div class="content">
																		<p class="subscribe-title">Channels name here</p>
																		<button class="bdr-theme-btn">Subscribe</button>
																	</div>
																</div>
															</div>
														</div>
													
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>			

					</div>
			 			
						
				</div>
			 </div>	
		  </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      <script >
      (function($){
         $(window).on("load",function(){


            $("#side-bar").mCustomScrollbar({
               scrollButtons:{enable:true,scrollType:"stepped"},
               keyboard:{scrollType:"stepped"},
               mouseWheel:{scrollAmount:188},
               theme:"rounded-dark",
               autoExpandScrollbar:true,
               snapAmount:188,
               snapOffset:65
            });

         });
      })(jQuery);
   </script>
      
        
     
      <script  >
         $(".forget-ps a").click(function(){
            $(".form-sign-in").toggleClass("show");
            $(".modal-backdrop").removeClass("show");
         })
      </script>
      <script  >
         $(document).ready(function() {
           var owl = $("#owl-demo1");
           owl.owlCarousel({
         
            nav: true,
            margin: 20,
            responsive: {
              0: {
               items: 1
              },
              600: {
               items: 2
              },
              960: {
               items: 2
              },
              1200: {
               items: 3
              }
            }
           });
         
         });
         $(document).ready(function() {
           var owl = $("#owl-demo2");
           owl.owlCarousel({
         
            nav: true,
            margin: 20,
            responsive: {
              0: {
               items: 1
              },
              600: {
               items: 2
              },
              960: {
               items: 2
              },
              1200: {
               items: 3
              }
            }
           });
         
         });
         $(document).ready(function() {
           var owl = $("#owl-demo3");
           owl.owlCarousel({
         
            nav: true,
            margin: 20,
            responsive: {
              0: {
               items: 1
              },
              600: {
               items: 2
              },
              960: {
               items: 2
              },
              1200: {
               items: 3
              }
            }
           });
         
         });
      </script> 
      <script >
         (function($) {
         $.fn.menumaker = function(options) {  
         var cssmenu = $(this), settings = $.extend({
          format: "dropdown",
          sticky: false
         }, options);
         return this.each(function() {
          $(this).find(".button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.slideToggle().removeClass('open');
            }
            else {
              mainmenu.slideToggle().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }
            }
          });
          cssmenu.find('li ul').parent().addClass('has-sub');
         multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideToggle();
              }
              else {
                $(this).siblings('ul').addClass('open').slideToggle();
              }
            });
          };
          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
          if (settings.sticky === true) cssmenu.css('position', 'fixed');
         resizeFix = function() {
         var mediasize = 1000;
            if ($( window ).width() > mediasize) {
              cssmenu.find('ul').show();
            }
            if ($(window).width() <= mediasize) {
              cssmenu.find('ul').hide().removeClass('open');
            }
          };
          resizeFix();
          return $(window).on('resize', resizeFix);
         });
         };
         })(jQuery);
         
         (function($){
         $(document).ready(function(){
         $("#cssmenu").menumaker({
          format: "multitoggle"
         });
         });
         })(jQuery);
         
      </script>
      <script  >
        $(document).ready(function() {
         $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
         $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
         });
        $('.select2').select2({
        });
        
      </script>
   <script  >
      jQuery(document).ready(function($){
       $(".filter-heading .icon-list").click(function(){
           $(".filter-heading .icon-remove").show();
           $(".filter-heading .icon-list").hide();
          $(".category-wrapper").addClass("display");
        })
         $(".filter-heading .icon-remove").click(function(){
            $(".filter-heading .icon-list").show();
           $(".filter-heading .icon-remove").hide();
            
          $(".category-wrapper").removeClass("display");
        })
         });
      
      
      
      $("#grid").click(function(){
           $("#grid").addClass("active");
           $("#list").removeClass("active");
        })
         $("#list").click(function(){
            $("#grid").removeClass("active");
           $("#list").addClass("active");
        })
   </script>
   <script >
      $(document).bind('dragover', function (event) {
         event.preventDefault();
         var dropZone = $('.zone'),
            timeout = window.dropZoneTimeout;
         if (!timeout) {
            dropZone.addClass('in');
         } else {
            clearTimeout(timeout);
         }
         var found = false,
            node = e.target;
         do {
            if (node === dropZone[0]) {
               found = true;
               break;
            }

            node = node.parentNode;
         } while (node != null);
         if (found) {
            dropZone.addClass('hover');
         } else {
            dropZone.removeClass('hover');
         }
         window.dropZoneTimeout = setTimeout(function () {
            window.dropZoneTimeout = null;
            dropZone.removeClass('in hover');
         }, 100);
      });   
   </script>   
      <script >
            
            $(document).ready(function() {
               
        //Horizontal Tab
        $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        // Child Tab
        $('#ChildVerticalTab_1').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true,
            tabidentify: 'ver_1', // The tab groups identifier
            activetab_bg: '#fff', // background color for active tabs in this group
            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
        });

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
            event.preventDefault();
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
            
            
            </script>   
        <script >
      
         $(document).ready(function(){
            
           $(".comment-section .content").slice(0, 3).show();
           $("#loadMore").on("click", function(e){
            e.preventDefault();
            $(".content:hidden").slice(0, 4).slideDown();
            if($(".content:hidden").length == 0) {
              $("#loadMore").text("No Content").addClass("noContent");
            }
           });

         });
          $("#loadMore").on("click", function(e){
            e.preventDefault();
            $(".content:hidden").slideDown(); 
             
          })
         
      </script>
      <script>
              function updatesubscription(x)
              {
                var post_data = {
                  'uploader': x,
                };
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>index.php/home/subscribe",
                  data: post_data,
                  success: function (response) {
                    //console.log(response);
                  }
                }
                      );
              }
              function updateunsubscription(x)
              {
                //alert(x);return false;
                var post_data = {
                  'uploader': x,
                };
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>index.php/home/unsubscribe",
                  data: post_data,
                  success: function (response) {
                    console.log(response);
                    if (response.length > 0) {
                      $('#subscribe').show();
                      $('#unsubscribe').hide();
                    }
                  }
                }
                      );
              }
              function updatesubscription1(x, y, z)
              {
                var condition = y;
                var url = "<?php echo base_url(); ?>index.php/home/" + y;
                //alert(url);return false;
                var post_data = {
                  'uploader': x,
                };
                $.ajax({
                  type: "POST",
                  url: url,
                  data: post_data,
                  success: function (response) {
                    console.log(response);
                    if (response.length > 0) {
                      // alert('yes');
                      if (condition == "subscribe")
                      {
                        $("#mainmenu").append("<li id='" + x + "'> <a href='<?php echo base_url(); ?>index.php/home/subscriber/" + x + "'> " + z + " </a></li>");
                        $('#sub' + x).hide();
                        $('#unsub' + x).show();
                      }
                      else
                      {
                        $("#" + x).remove();
                        $('#sub' + x).show();
                        $('#unsub' + x).hide();
                      }
                    }
                  }
                }
                      );
              }
              function updateunsubscription1(x)
              {
                alert(x);
                return false;
                var post_data = {
                  'uploader': x,
                };
                $.ajax({
                  type: "POST",
                  url: "<?php echo base_url(); ?>index.php/home/unsubscribe",
                  data: post_data,
                  success: function (response) {
                    console.log(response);
                    if (response.length > 0) {
                      $('#sub').show();
                      $('#unsub').hide();
                    }
                  }
                }
                      );
              }
            </script>
            <script type="text/javascript">
              function ajaxSearch()
              {
                var input_data = $('#input-31').val();
                if (input_data.length === 0)
                {
                  $('#suggestions').hide();
                }
                else
                {
                  var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                  };
                  $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>index.php/home/search",
                    data: post_data,
                    success: function (data) {
                      // return success
                      if (data.length > 0) {
                        $('#suggestions').show();
                        $('#autoSuggestionsList').addClass('auto_list');
                        $('#autoSuggestionsList').html(data);
                      }
                    }
                  }
                        );
                }
              }
            </script>
<?php include_once('footer.php'); ?>

   </body>
</html>


