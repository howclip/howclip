<!doctype html>
<html lang="en">
   <head>
      <title>HowClip</title>
   </head>
   <body>
      <?php include_once('header.php'); ?>
      <section class="banner">
         <div class="container">
            <div class="banner-heading">
               <h2>My Orders</h2>
               <p><a href="index.html">Home</a>  /  <a href="#">My Account</a>  /  My Orders</p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="uploading-vdo-section trending-section">
				<div class="heading">
                  <div class="heading-outer">
                     <h3>MY  <span> ORDER LIST</span></h3>
                  </div>
               	</div>
			    <div class="table-responsive">
					<table id="sailorTable" class="table order-list">

						<thead>
							<tr>
								<th>S.No.</th>
								<th>Video Name</th>
								<th>Purchaser Name</th>
								<th>Uploader Name</th>
								<th>Date</th>
                        <th>Status</th>
								<th>Price</th>
							</tr>
						</thead>
						<tbody>
                     <?php 
                     if(isset($orderdetail) && !empty($orderdetail)){
      $i =1; foreach($orderdetail as $order) {
          $purchaser=$this->db->get_where('users',array('id'=>$order->userId))->row_array();
   
          ?>
							<tr>
								<td><?php echo $i;?></td>
								<td><h5><?php echo ucwords($order->video);?></h5></td>
								<td><?php echo ucwords($purchaser['username']);?></td>
								<td><?php echo ucwords($order->username);?></td>
								<td><?php echo $order->submit_date;?></td>
								<td><?php echo ucwords($order->status);?></td>
                        <td>$ <?php echo $order->price;?></td>
							</tr>
							 <?php $i++;} 
                     }else{
                        ?>
                        <td colspan="7" style="color: red">No Record Found</td>
                      <?php
                     }
                      ?>
                     
						</tbody>
					</table>
				</div>
			</div>
			<div class="pagination">
				<a href="#" class="prev-anchor"><i class="icon-angle-left"></i> </a>
				  <a href="#">1</a><a href="#" class="active">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a>
				<a href="#" class="next-anchor"><i class="icon-angle-right"></i> </a>
			</div>
         </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      
	
	
<?php include_once('footer.php'); ?>
   </body>
</html>


