<!doctype html>
<html lang="en">
   <head>
      <title>Terms & Conditions</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
      
   </head>
   <body>
     <?php include_once('header.php'); ?>
      <section class="banner">
         <div class="container">
           <div class="banner-heading">
               <h2>Terms & Conditions</h2>
               <p><a href="index.html">Home</a>  /  Terms & Conditions </p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="trending-section terms-condition">
				<div class="description">
					<div class="term-heading">
						<h4>HowClip Terms of Use and Agreement </h4>
						<p>Pursue your career in a growing and rewarding environment while working along side a great group of professionals.</p>
					</div>	
					<div class="condition">
						<div class="number">1</div> 
						<div class="term-content">
							<h4>Your Acceptance</h4>
							<p>BY USING AND/OR VISITING THIS WEBSITE (collectively, including all Content available through the HowClip.com domain name, the "HowClip Website", or "Website"), YOU SIGNIFY YOUR ASSENT TO BOTH THESE TERMS AND CONDITIONS (the "Terms of Service") AND THE TERMS AND CONDITIONS OF HOWCLIP'S PRIVACY NOTICE, WHICH ARE PUBLISHED AT http://www.HowClip.com/privacy.php, AND WHICH ARE INCORPORATED HEREIN BY REFERENCE. If you do not agree to any of these terms, then please do not use the HowClip Website.</p>
						</div>	
					</div>
					<div class="condition">
						<div class="number">2</div> 
						<div class="term-content">
							<h4>HowClip Website</h4>
							<p>These Terms of Service apply to all users of the HowClip Website, including users who are also contributors of video content, information, and other materials or services on the Website. The HowClip Website may contain links to third party websites that are not owned or controlled by HowClip. HowClip has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third party websites. In addition, HowClip will not and cannot censor or edit the content of any third-party site. By using the Website, you expressly relieve HowClip from any and all liability arising from your use of any third-party website. Accordingly, we encourage you to be aware when you leave the HowClip Website and to read the terms and conditions and privacy policy of each other website that you visit.</p>
						</div>	
					</div>
					<div class="condition">
						<div class="number">3</div> 
						<div class="term-content">
							<h4>Website Access</h4>
							<ul class="access">
								<li>
									<span>HowClip hereby grants you permission to use the Website as set forth in this Terms of Service, provided that: (i) your use of the Website as permitted is solely for your personal, noncommercial use; (ii) you will not copy or distribute any part of the Website in any medium without HowClip's prior written authorization; (iii) you will not alter or modify any part of the Website other than as may be reasonably necessary to use the Website for its intended purpose; and (iv) you will otherwise comply with the terms and conditions of these Terms of Service.</span>
								</li>
								<li>
									<span>HowClip hereby grants you permission to use the Website as set forth in this Terms of Service, provided that: (i) your use of the Website as permitted is solely for your personal, noncommercial use; (ii) you will not copy or distribute any part of the Website in any medium without HowClip's prior written authorization; (iii) you will not alter or modify any part of the Website other than as may be reasonably necessary to use the Website for its intended purpose; and (iv) you will otherwise comply with the terms and conditions of these Terms of Service.</span>
								</li>
							</ul>
						</div>	
					</div>
					<div class="condition about-channel">
						<div class="number">4</div> 
						<div class="term-content">
							<h4>Responsibility of Contributors</h4>
							<div class="choose-our-channel">
								<p>If you operate a blog, comment on a blog, post material to the Website, post links on the Website, or otherwise make (or allow any third party to make) material available by means of the Website (any such material, “Content”), You are entirely responsible for the content of, and any harm resulting from, that Content. That is the case regardless of whether the Content in question constitutes text, graphics, an audio file, or computer software. By making Content available, you represent and warrant that:</p>
								<ul>
									<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>

									<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>

									<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>

									<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>

									<li><span>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga </span></li>


								</ul>
							</div>
						</div>	
					</div>
					<div class="condition">
						<div class="number">5</div> 
						<div class="term-content">
							<h4>Limitation of Liability</h4>
							<p>IN NO EVENT SHALL HOWCLIP, ITS OFFICERS, DIRECTORS, EMPLOYEES, OR AGENTS, BE LIABLE TO YOU FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, OR CONSEQUENTIAL DAMAGES WHATSOEVER RESULTING FROM ANY (I) ERRORS, MISTAKES, OR INACCURACIES OF CONTENT, (II) PERSONAL INJURY OR PROPERTY DAMAGE, OF ANY NATURE WHATSOEVER, RESULTING FROM YOUR ACCESS TO AND USE OF OUR WEBSITE, (III) ANY UNAUTHORIZED ACCESS TO OR USE OF OUR SECURE SERVERS AND/OR ANY AND ALL PERSONAL INFORMATION AND/OR FINANCIAL INFORMATION STORED THEREIN, (IV) ANY INTERRUPTION OR CESSATION OF TRANSMISSION TO OR FROM OUR WEBSITE, (IV) ANY BUGS, VIRUSES, TROJAN HORSES, OR THE LIKE, WHICH MAY BE TRANSMITTED TO OR THROUGH OUR WEBSITE BY ANY THIRD PARTY, AND/OR (V) ANY ERRORS OR OMISSIONS IN ANY CONTENT OR FOR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF YOUR USE OF ANY CONTENT POSTED, EMAILED, TRANSMITTED, OR OTHERWISE MADE AVAILABLE VIA THE HOWCLIP WEBSITE, WHETHER BASED ON WARRANTY, CONTRACT, TORT, OR ANY OTHER LEGAL THEORY, AND WHETHER OR NOT THE COMPANY IS ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. THE FOREGOING LIMITATION OF LIABILITY SHALL APPLY TO THE FULLEST EXTENT PERMITTED BY LAW IN THE APPLICABLE JURISDICTION.</p>
							<p>YOU SPECIFICALLY ACKNOWLEDGE THAT HOWCLIP SHALL NOT BE LIABLE FOR USER SUBMISSIONS OR THE DEFAMATORY, OFFENSIVE, OR ILLEGAL CONDUCT OF ANY THIRD PARTY AND THAT THE RISK OF HARM OR DAMAGE FROM THE FOREGOING RESTS ENTIRELY WITH YOU.</p>
						</div>	
					</div>
					<div class="condition">
						<div class="number">6</div> 
						<div class="term-content">
							<h4> Indemnity</h4>
							<p>You agree to defend, indemnify and hold harmless HowClip, its parent corporation, officers, directors, employees and agents, from and against any and all claims, damages, obligations, losses, liabilities, costs or debt, and expenses (including but not limited to attorney's fees) arising from: (i) your use of and access to the HowClip Website; (ii) your violation of any term of these Terms of Service; (iii) your violation of any third party right, including without limitation any copyright, property, or privacy right; or (iv) any claim that one of your User Submissions caused damage to a third party. This defense and indemnification obligation will survive these Terms of Service and your use of the HowClip Website.</p>
						</div>	
					</div>
			 			
						
				</div>
			 </div>	
		  </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      <?php include_once('footer.php') ?>
	

   </body>
</html>



