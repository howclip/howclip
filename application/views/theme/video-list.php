<!doctype html>
<html lang="en">
   <head>
      <title>video Playlist</title>
      
   </head>
   <body>
      <?php include_once('header.php');?>
      <section class="banner">
         <div class="container">
            <div class="banner-heading">
               <h2>Videos</h2>
               <p><a href="index.html">Home</a> / <a href="#">My account</a> / Videos</p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="uploading-vdo-section trending-section video-list-page">
				<div class="vdo-upload-search">
					<div class="row">
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="input-search">
								<input type="search" class="search form-control" placeholder="Search Video here">
							</div>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-12">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="upload-video">
										<label>Short By : </label>
										<span class="select-cat">
											<select class="select2" >
												<optgroup label="All Videos">
												  <option value="" selected>All Videos </option>	
												  <option value="co" > Funny Videos </option>
												  <option value="da"> Dance Videos </option>
												  <option value="si"> Filmy Videos</option>
												</optgroup>
											</select>
										</span>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="selectFile">       
									  <label for="file">Upload New Video</label>                   
									  <input type="file" name="files[]" id="file">
									</div>
								</div>	
							</div>
						</div>	
					</div>	
				</div>
			    <div class="table-responsive">
					<table id="sailorTable" class="table" >

						<thead>
							<tr>
								<th>Mark</th>
								<th>Video Titile</th>
								<th>Select Privacy</th>
								<th>Add to Playlist</th>
								<th>Action</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox1" type="checkbox"><label for="checkbox1"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox2" type="checkbox"><label for="checkbox2"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox3" type="checkbox"><label for="checkbox3"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox4" type="checkbox"><label for="checkbox4"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox5" type="checkbox"><label for="checkbox5"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox6" type="checkbox"><label for="checkbox6"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox7" type="checkbox"><label for="checkbox7"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox8" type="checkbox"><label for="checkbox8"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox9" type="checkbox"><label for="checkbox9"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
							<tr>
								<td><span class="checkbox checkbox-primary"><input id="checkbox10" type="checkbox"><label for="checkbox10"></label> </span></td>
								<td><div class="vdo-img"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" /></div><p class="title">Reference site about Lorem Ipsum</p></td>
								<td>
									<span class="select-cat">
										<select class="select2" >
											<optgroup label="Public">
											  <option value="" selected> Public </option>	
											  <option value="co" > Private </option>
											  <option value="da"> Personal </option>
											  <option value="si"> Friends </option>
											</optgroup>
										</select>
									</span>
								</td>
								<td><a href="#" class="theme-btn">Create</a></td>
								<td><div class="remove"><a href="#"><i class="icon-trash"></i><span>Remove</span></a></div></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<div class="pagination">
				<a href="#" class="prev-anchor"><i class="icon-angle-left"></i></a>
				<a href="#">1</a><a href="#" class="active">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a>
				<a href="#" class="next-anchor"><i class="icon-angle-right"></i></a>
			</div>
         </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      <?php include_once('footer.php');?>

   </body>
</html>


