<!doctype html>
<html lang="en">
   <head>
      <title>Upload Video Clip</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
      
   </head>
   <body>
       <?php include_once('header.php'); ?>
     
      <section class="banner">
         <div class="container">
            <div class="banner-heading">
               <h2>Upload Video Clip</h2>
               <h1> Post Video Clip with Easy Way</h1>
               <p><a href="index.html">Home</a>  /  Upload Video </p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="upload-vdo-section trending-section">
			 	<div class="zone">

				  <div id="dropZ">
					<span><i class="icon-upload"></i></span>
					<h3>Drag and drop your file here</h3>                    
					<span class="or">OR</span>
					<div class="file-select-btn">
					<ul>	
						<li>
						<div class="selectFile">       
						  <label for="file">Browse Your Video</label>                   
						  <input type="file" name="files[]" id="file">
						</div>
							</li>
						<li>
						<div class="selectFile select-cat">                        
						   <select class="select2" >
								<optgroup label=" Public">
								  <option value="" selected> Public </option>	
								  <option value="co"> Private </option>
								  <option value="da"> others </option>
								</optgroup>

							</select>
						</div>
						</li>
					</ul>	
					 </div> 
					<p>Only format allowed! (avi,mpg,wmv,mp4)</p>
				  </div>

				</div>
			 </div>
         </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            
         </div>
      </div>
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-lg-3 col-md-6 col-sm-12">
                  <div class="about-section">
                     <div class="about-us">
                        <div class="footer-logo">
							<a href="index.html">
                           		<img src="<?php echo base_url();?>my-assets/images/logo.png" alt="" />
							</a>	
                        </div>
                        <div class="about-heading">
                           <h6><a href="about-us.html">ABOUT US</a></h6>
                        </div>
                     </div>
                     <p class="foooter-para">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown <a href="#" class="footer-more"><em>More...</em></a></p>
                  </div>
               </div>
               <div class="col-lg-2 col-md-6 col-sm-6">
                  <div class="contact-section address">
                     <h5>CONTACT US</h5>
                     <div class="contact-address">
                        <div class="icn">
                           <i class="icon-call"></i>
                        </div>
                        <div class="content">
                           <span>+44 345 678 903</span>
                        </div>
                     </div>
                     <div class="contact-address">
                        <div class="icn">
                           <i class="icon-email"></i>
                        </div>
                        <div class="content">
                           <a href="#"><span>adobexd@mail.com</span></a>
                        </div>
                     </div>
                     <div class="contact-address">
                        <div class="icn">
                           <i class="icon-address"></i>
                        </div>
                        <div class="content">
                          <span>497 Evergreen Rd. Roseville, CA 95673c  44 345 678 903</span>
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-lg-2 col-md-4 col-sm-6">
                  <div class="customer-section address">
                     <h5>CUSTOMER SERVICE</h5>
                     <ul>
                        <li><a href="#javascript:;">Contact Us</a></li>
                        <li><a href="term-and-condition-page.html">Terms</a></li>
                        <li><a href="privacy.html">Privacy</a></li>
                        <li><a href="policy-safety.html">Policy & Safety</a></li>
                        <li><a href="feedback.html">Send feedback</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-2 col-md-3 col-sm-6">
                  <div class="information-section address">
                     <h5>INFORMATION</h5>
                     <ul>
                        <li><a href="about-us.html">About</a></li>
                        <li><a href="press.html">Press</a></li>
                        <li><a href="#javascript:;">Copyright </a></li>
                        <li><a href="creator.html">Creators</a></li>
                        <li><a href="advertise.html">Advertise</a></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-3 col-md-5 col-sm-6">
                  <div class="subscribe-section">
                     <h5>SUBSCRIBE NEWSLETTER</h5>
                     <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia</p>
                     <form>
                        <input type="email" placeholder="Email Address">
                        <button type="submit" class="subscribe-btn">Subscribe</button>
                     </form>
                  </div>
               </div>
            </div>
         </div>
         <div class="mini-footer">
            <div class="container">
               <div class="copy-right">
                  <p>© 2019 howclip.com. All Rights Reserved</p>
               </div>
               <div class="arrow">
                  <i class="icon-arrow-up"></i>
               </div>
            </div>
         </div>
      </footer>
      <div class="modal fade" id="Modal-1" role="dialog">
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
               </div>
               <div class="modal-body">
                  <iframe width="560" height="315" src="https://www.youtube.com/embed/oUzs4ZuFyiI" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade form-sign-in popup-form" id="Modal-2" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content form-content">
               <div class="modal-header">
                  <p class="form-heading"><span> <img src="<?php echo base_url();?>my-assets/images/man(1).png" alt="" /></span>Please login to your account</p>
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
               </div>
               <div class="modal-body">
                  <div id="first">
                     <div class="myform form ">
                        <form   method="post" name="login">
                           <div class="facebook-link">
                              <p>
                                 <a href="javascript:void();" class="signin-facebook btn mybtn"><i class="icon-facebook"></i>
                                  <span>Sign In  with Facebook</span>
                                 </a>
                              </p>
                           </div>
                           <div class="google-link">
                              <p>
                                 <a href="javascript:void();" class="google btn mybtn"><i class="icon-google"></i><span>Continue with Google
                                 </span> 
                                 </a>
                              </p>
                           </div>
                           <div class="form-group email-img">
                              <input type="email" name="email"  class="form-control" id="email-1"   placeholder="Email" >
                           </div>
                           <div class="form-group ps-img">
                              <input type="password" name="password" id="password"  class="form-control"   placeholder="Password">
                           </div>
                           <div class=" text-center ">
                              <button type="submit" class="pop-form-btn btn btn-block mybtn btn-primary tx-tfm">Sign In</button>
                           </div>
                           <div class="form-group">
                              <p class="text-center forget-ps"><a href="#" data-toggle="modal" data-target="#Modal-4" >Forgot Password?</a></p>
                           </div>
                           <div class="col-md-12 ">
                              <div class="login-or">
                                 <hr class="hr-or">
                                 <span class="span-or">or</span>
                              </div>
                           </div>
                           <div class="form-group">
                              <p class="text-center any-account">Don't have account? <a href="#" class="signup-link">Sign Up</a></p>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade popup-form" id="Modal-3" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content form-content">
               <div class="modal-header">
                  <p class="form-heading"><span> <img src="<?php echo base_url();?>my-assets/images/man(1).png" alt="" /></span>Please create your account</p>
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
               </div>
               <div class="modal-body">
                  <div id="first-1">
                     <div class="myform form ">
                        <form   method="post" name="login">
                           <div class="form-group user-img">
                              <input type="text" name="email"  class="form-control"    placeholder="Full Name" >
                           </div>
                           <div class="form-group email-img">
                              <input type="email" name="email"  class="form-control"    placeholder="Email" >
                           </div>
                           <div class="form-group ps-img">
                              <input type="password" name="password"  class="form-control"   placeholder="Password">
                           </div>
                           <div class="form-group">
                              <label for="remember-me" class="text-info remember"><span><input id="remember-me" name="remember-me" type="checkbox"></span><span>I’m in for emails with exciting discounts and personalized recommendations</span> </label><br>
                           </div>
                           <div class=" text-center ">
                              <button type="submit" class="pop-form-btn btn btn-block mybtn btn-primary tx-tfm">Sign Up</button>
                           </div>
                           <div class="form-group">
                              <p class="text-center term">By logging in, you agree to our    <a href="#">Terms of Service</a> and <a href="#">Privacy Policy</a></p>
                           </div>
                           <div class="col-md-12 ">
                              <div class="login-or">
                                 <hr class="hr-or">
                                 <span class="span-or">or</span>
                              </div>
                           </div>
                           <div class="form-group">
                              <p class="text-center any-account">Already have an account? <a href="#" class="signin-link">Sign In</a></p>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div class="modal fade popup-form" id="Modal-4" role="dialog">
         <div class="modal-dialog">
            <div class="modal-content form-content">
               <div class="modal-header">
                  <p class="form-heading"><span> <img src="<?php echo base_url();?>my-assets/images/man(1).png" alt="" /></span>Forgot Password</p>
                  <button type="button" class="close" data-dismiss="modal"><i class="icon-close"></i></button>
               </div>
               <div class="modal-body">
                  <div id="first-2">
                     <div class="myform form ">
                        <form   method="post" name="login">
                           <div class="form-group email-img">
                              <input type="email" name="email"  class="form-control"   placeholder="Email" >
                           </div>
                           <div class=" text-center ">
                              <button type="submit" class="pop-form-btn btn btn-block mybtn btn-primary tx-tfm">Reset Password</button>
                           </div>
                           <div class="col-md-12 ">
                              <div class="login-or">
                                 <hr class="hr-or">
                                 <span class="span-or">or</span>
                              </div>
                           </div>
                           <div class="form-group">
                              <p class="text-center any-account">Already have an account? <a href="#" class="signin-link">Sign In</a></p>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	
	<div class="pop-up-serch search-section">
		 <div class="container">
		   <div class="search-form">
			  <form>
				 <div class="input-group">
					<div class="search-panel">
					  <select class="select2" >
						  <option value="" selected class="search-cat"> Category</option>	
						  <option value="co"> Comedy </option>
						  <option value="da"> Dance </option>
						  <option value="si"> Singing </option>
					  </select>
					</div>
					<input type="text" class="form-control" name="x" placeholder="Search here...">
					<span class="input-group-btn search-btn">
					<button class="btn btn-default" type="submit"><span><i class="icon-search"></i></span></button>
					</span> 
				 </div>
			  </form>
			   <div class="remove-search">
			   		<i class="icon-close"></i>
			   </div>
		   </div>
		 </div>
	  </div>


      <script src="<?php echo base_url();?>my-assets/js/jquery.js"></script>
      <script src="<?php echo base_url();?>my-assets/js/owl.carousel.min.js"></script>
      <script src="<?php echo base_url();?>my-assets/js/bootstrap.min.js"></script> 
	<script src="<?php echo base_url();?>my-assets/js/util.js"></script> <!-- util functions included in the CodyHouse framework -->
  <script src="<?php echo base_url();?>my-assets/js/menu-aim.js"></script>
  <script src="<?php echo base_url();?>my-assets/js/main.js"></script>
  <script  src="<?php echo base_url();?>my-assets/js/scrollbar.min.js"></script>
	<script >
		(function($){
			$(window).on("load",function(){


				$("#side-bar").mCustomScrollbar({
					scrollButtons:{enable:true,scrollType:"stepped"},
					keyboard:{scrollType:"stepped"},
					mouseWheel:{scrollAmount:188},
					theme:"rounded-dark",
					autoExpandScrollbar:true,
					snapAmount:188,
					snapOffset:65
				});

			});
		})(jQuery);
	</script>
      <script  >
         $(".bar").click(function(){
         	$(".side-bar").toggleClass("show");
         })
         $(".white-close").click(function(){
         	$(".ad").addClass("display");
         	$("header").addClass("fix");
         })
         $(".arrow").click(function(){
         	$("body,html").animate({scrollTop : 0}, 500);
         })
         $(window).scroll(function(){
         	if($(document).scrollTop() > 48){
         		$("header").addClass("sticky");
         	}
         	else{
         		$("header").removeClass("sticky");
         	}
         })
		   $(".nav-search-icon").click(function(){
         	$(".pop-up-serch.search-section").addClass("visit");
         })
		  $(".remove-search .icon-close").click(function(){
         	$(".pop-up-serch.search-section").removeClass("visit");
         })
         
         
      </script>
      <script  >
         $(".forget-ps a").click(function(){
         	$(".form-sign-in").toggleClass("show");
         	$(".modal-backdrop").removeClass("show");
         })
      </script>
      <script  >
         $(document).ready(function() {
           var owl = $("#owl-demo");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 4
         	  }
         	}
           });
         
         });
      </script> 
      <script >
         (function($) {
         $.fn.menumaker = function(options) {  
         var cssmenu = $(this), settings = $.extend({
          format: "dropdown",
          sticky: false
         }, options);
         return this.each(function() {
          $(this).find(".button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.slideToggle().removeClass('open');
            }
            else {
              mainmenu.slideToggle().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }
            }
          });
          cssmenu.find('li ul').parent().addClass('has-sub');
         multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideToggle();
              }
              else {
                $(this).siblings('ul').addClass('open').slideToggle();
              }
            });
          };
          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
          if (settings.sticky === true) cssmenu.css('position', 'fixed');
         resizeFix = function() {
         var mediasize = 1000;
            if ($( window ).width() > mediasize) {
              cssmenu.find('ul').show();
            }
            if ($(window).width() <= mediasize) {
              cssmenu.find('ul').hide().removeClass('open');
            }
          };
          resizeFix();
          return $(window).on('resize', resizeFix);
         });
         };
         })(jQuery);
         
         (function($){
         $(document).ready(function(){
         $("#cssmenu").menumaker({
          format: "multitoggle"
         });
         });
         })(jQuery);
         
      </script>
      <script  >$(document).ready(function() {
         $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
         $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
         });
		  $('.select2').select2({
		  });
		  $("#grid").click(function(){
			  $("#grid").addClass("active");
			  $("#list").removeClass("active");
		  })
		   $("#list").click(function(){
			   $("#grid").removeClass("active");
			  $("#list").addClass("active");
		  })
      </script>
	<script  >
		 $(".filter-heading .icon-list").click(function(){
			  $(".filter-heading .icon-remove").show();
			  $(".filter-heading .icon-list").hide();
			 $(".category-wrapper").addClass("display");
		  })
		   $(".filter-heading .icon-remove").click(function(){
			   $(".filter-heading .icon-list").show();
			  $(".filter-heading .icon-remove").hide();
			   
			 $(".category-wrapper").removeClass("display");
		  })
	</script>
	<script >
		$(document).bind('dragover', function (e) {
			var dropZone = $('.zone'),
				timeout = window.dropZoneTimeout;
			if (!timeout) {
				dropZone.addClass('in');
			} else {
				clearTimeout(timeout);
			}
			var found = false,
				node = e.target;
			do {
				if (node === dropZone[0]) {
					found = true;
					break;
				}
				node = node.parentNode;
			} while (node != null);
			if (found) {
				dropZone.addClass('hover');
			} else {
				dropZone.removeClass('hover');
			}
			window.dropZoneTimeout = setTimeout(function () {
				window.dropZoneTimeout = null;
				dropZone.removeClass('in hover');
			}, 100);
		});   
	</script>   
	

   </body>
</html>

