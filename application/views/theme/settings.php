<!doctype html>
<html lang="en">
   <head>
      <title>HowClip</title>
      
   </head>
   <body>
      <?php include_once('header.php'); ?>

      <section class="banner">
         <div class="container">
            <div class="banner-heading">
               <h2>Setting</h2>
               <p><a href="index.html">Home</a>  /  <a href="#">My Account </a> /  Setting</p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="uploading-vdo-section trending-section">
				<div class="heading">
                  <div class="heading-outer">
                     <h3>PROFILE <span> EDIT</span></h3>
                  </div>
               	</div>
				<div class="profile-edit-form">
					
						<div class="row">
							<form method="post" name="form" onSubmit="return validation()" action="<?php echo base_url() ?>index.php/home/EditUser/<?php echo $user->id; ?>" enctype="multipart/form-data">
						  <div class="col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label>First Name</label>
							  <input type="text" class="form-control"  id="exampleInputEmail1" name="fname" placeholder="First Name" value="<?php echo $user->Firstname; ?>">
							  <div style="color:#FF0000;display:none;" id="fname" > Enter First Name</div>
							</div>
						  </div>

						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label >Last Name</label>
							  <input type="text" class="form-control"  id="exampleInputPassword1" name="lname" placeholder="Last Name" value="<?php echo $user->Lastname; ?>">
							  <div style="color:#FF0000;display:none;" id="lname"> Enter Last name</div>
							</div>
						  </div>
						 <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label>Email id</label>
							  <input type="email" class="form-control"  placeholder="mateusz.maaria@gmail.com" name="email" id="email_address" value="<?php echo $user->email; ?>">
							  <p id="error" style="display:none;color:red;">Wrong email</p>
                              <div style="color:#FF0000;display:none;" id="email" > Enter Email Address</div>
							</div>
						  </div>
							
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label>Profile Name</label>
							  <input type="text" class="form-control"  id="exampleInputEmail1" name="username" placeholder="Choose your Username" value="<?php echo $user->username; ?>">
							</div>
						  </div>
						 <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							   <span class="date-picker">
							    <label for="dateofbirth">DOB : </label>
								<input type="date" class="form-control"  id="dateofbirth">
						 	   </span>
							</div>
							
						  </div>

						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
							<div class="form-group">
							  <label>Gender</label>
								<div class="form-control gender">
									<ul>
										<li>
											<span class="radio radio-primary"><input type="radio" name="radio1" id="radio1" value="option1"><label for="radio1"><span>Female</span></label></span>
										</li>
										<li>
											<span class="radio radio-primary"><input type="radio" name="radio1" id="radio2" value="option1"><label for="radio2"><span>Male</span></label></span>
										</li>
										<li>
											<span class="radio radio-primary"><input type="radio" name="radio1" id="radio3" value="option1"><label for="radio3"><span>Others</span></label></span>
										</li>
										<li>
											<span class="radio radio-primary"><input type="radio" name="radio1" id="radio4" value="option1"><label for="radio4"><span>Rather not say</span></label></span>
										</li>	
									</ul>	
								</div>	
							</div>
						  </div>

						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label class="select">Country</label>
							  <div class="select-cat">	
								  <select class="form-control">
									 <option value="">Country</option>
                                                            <option value="Aaland Islands">Aaland Islands</option>
                                                            <option value="Afghanistan">Afghanistan</option>
                                                            <option value="Albania">Albania</option>
                                                            <option value="Algeria">Algeria</option>
                                                            <option value="American Samoa">American Samoa</option>
                                                            <option value="Andorra">Andorra</option>
                                                            <option value="Angola">Angola</option>
                                                            <option value="Anguilla">Anguilla</option>
                                                            <option value="Antarctica">Antarctica</option>
                                                            <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                                                            <option value="Argentina">Argentina</option>
                                                            <option value="Armenia">Armenia</option>
                                                            <option value="Aruba">Aruba</option>
                                                            <option value="Australia">Australia</option>
                                                            <option value="Austria">Austria</option>
                                                            <option value="Azerbaijan">Azerbaijan</option>
                                                            <option value="Bahamas">Bahamas</option>
                                                            <option value="Bahrain">Bahrain</option>
                                                            <option value="Bangladesh">Bangladesh</option>
                                                            <option value="Barbados">Barbados</option>
                                                            <option value="Belarus">Belarus</option>
                                                            <option value="Belgium">Belgium</option>
                                                            <option value="Belize">Belize</option>
                                                            <option value="Benin">Benin</option>
                                                            <option value="Bermuda">Bermuda</option>
                                                            <option value="Bhutan">Bhutan</option>
                                                            <option value="Bolivia">Bolivia</option>
                                                            <option value="Bonaire, Sint Eustatius and Saba">Bonaire, Sint Eustatius and Saba</option>
                                                            <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                                                            <option value="Botswana">Botswana</option>
                                                            <option value="Bouvet Island">Bouvet Island</option>
                                                            <option value="Brazil">Brazil</option>
                                                            <option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
                                                            <option value="Brunei Darussalam">Brunei Darussalam</option>
                                                            <option value="Bulgaria">Bulgaria</option>
                                                            <option value="Burkina Faso">Burkina Faso</option>
                                                            <option value="Burundi">Burundi</option>
                                                            <option value="Cambodia">Cambodia</option>
                                                            <option value="Cameroon">Cameroon</option>
                                                            <option value="Canada">Canada</option>
                                                            <option value="Canary Islands">Canary Islands</option>
                                                            <option value="Cape Verde">Cape Verde</option>
                                                            <option value="Cayman Islands">Cayman Islands</option>
                                                            <option value="Central African Republic">Central African Republic</option>
                                                            <option value="Chad">Chad</option>
                                                            <option value="Chile">Chile</option>
                                                            <option value="China">China</option>
                                                            <option value="Christmas Island">Christmas Island</option>
                                                            <option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
                                                            <option value="Colombia">Colombia</option>
                                                            <option value="Comoros">Comoros</option>
                                                            <option value="Congo">Congo</option>
                                                            <option value="Cook Islands">Cook Islands</option>
                                                            <option value="Costa Rica">Costa Rica</option>
                                                            <option value="Cote D'Ivoire">Cote D'Ivoire</option>
                                                            <option value="Croatia">Croatia</option>
                                                            <option value="Cuba">Cuba</option>
                                                            <option value="Curacao">Curacao</option>
                                                            <option value="Cyprus">Cyprus</option>
                                                            <option value="Czech Republic">Czech Republic</option>
                                                            <option value="Democratic Republic of Congo">Democratic Republic of Congo</option>
                                                            <option value="Denmark">Denmark</option>
                                                            <option value="Djibouti">Djibouti</option>
                                                            <option value="Dominica">Dominica</option>
                                                            <option value="Dominican Republic">Dominican Republic</option>
                                                            <option value="East Timor">East Timor</option>
                                                            <option value="Ecuador">Ecuador</option>
                                                            <option value="Egypt">Egypt</option>
                                                            <option value="El Salvador">El Salvador</option>
                                                            <option value="Equatorial Guinea">Equatorial Guinea</option>
                                                            <option value="Eritrea">Eritrea</option>
                                                            <option value="Estonia">Estonia</option>
                                                            <option value="Ethiopia">Ethiopia</option>
                                                            <option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
                                                            <option value="Faroe Islands">Faroe Islands</option>
                                                            <option value="Fiji">Fiji</option>
                                                            <option value="Finland">Finland</option>
                                                            <option value="France, skypolitan">France, skypolitan</option>
                                                            <option value="French Guiana">French Guiana</option>
                                                            <option value="French Polynesia">French Polynesia</option>
                                                            <option value="French Southern Territories">French Southern Territories</option>
                                                            <option value="FYROM">FYROM</option>
                                                            <option value="Gabon">Gabon</option>
                                                            <option value="Gambia">Gambia</option>
                                                            <option value="Georgia">Georgia</option>
                                                            <option value="Germany">Germany</option>
                                                            <option value="Ghana">Ghana</option>
                                                            <option value="Gibraltar">Gibraltar</option>
                                                            <option value="Greece">Greece</option>
                                                            <option value="Greenland">Greenland</option>
                                                            <option value="Grenada">Grenada</option>
                                                            <option value="Guadeloupe">Guadeloupe</option>
                                                            <option value="Guam">Guam</option>
                                                            <option value="Guatemala">Guatemala</option>
                                                            <option value="Guernsey">Guernsey</option>
                                                            <option value="Guinea">Guinea</option>
                                                            <option value="Guinea-Bissau">Guinea-Bissau</option>
                                                            <option value="Guyana">Guyana</option>
                                                            <option value="Haiti">Haiti</option>
                                                            <option value="Heard and Mc Donald Islands">Heard and Mc Donald Islands</option>
                                                            <option value="Honduras">Honduras</option>
                                                            <option value="Hong Kong">Hong Kong</option>
                                                            <option value="Hungary">Hungary</option>
                                                            <option value="Iceland">Iceland</option>
                                                            <option value="India">India</option>
                                                            <option value="Indonesia">Indonesia</option>
                                                            <option value="Iran (Islamic Republic of)">Iran (Islamic Republic of)</option>
                                                            <option value="Iraq">Iraq</option>
                                                            <option value="Ireland">Ireland</option>
                                                            <option value="Israel">Israel</option>
                                                            <option value="Italy">Italy</option>
                                                            <option value="Jamaica">Jamaica</option>
                                                            <option value="Japan">Japan</option>
                                                            <option value="Jersey">Jersey</option>
                                                            <option value="Jordan">Jordan</option>
                                                            <option value="Kazakhstan">Kazakhstan</option>
                                                            <option value="Kenya">Kenya</option>
                                                            <option value="Kiribati">Kiribati</option>
                                                            <option value="Korea, Republic of">Korea, Republic of</option>
                                                            <option value="Kuwait">Kuwait</option>
                                                            <option value="Kyrgyzstan">Kyrgyzstan</option>
                                                            <option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
                                                            <option value="Latvia">Latvia</option>
                                                            <option value="Lebanon">Lebanon</option>
                                                            <option value="Lesotho">Lesotho</option>
                                                            <option value="Liberia">Liberia</option>
                                                            <option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
                                                            <option value="Liechtenstein">Liechtenstein</option>
                                                            <option value="Lithuania">Lithuania</option>
                                                            <option value="Luxembourg">Luxembourg</option>
                                                            <option value="Macau">Macau</option>
                                                            <option value="Madagascar">Madagascar</option>
                                                            <option value="Malawi">Malawi</option>
                                                            <option value="Malaysia">Malaysia</option>
                                                            <option value="Maldives">Maldives</option>
                                                            <option value="Mali">Mali</option>
                                                            <option value="Malta">Malta</option>
                                                            <option value="Marshall Islands">Marshall Islands</option>
                                                            <option value="Martinique">Martinique</option>
                                                            <option value="Mauritania">Mauritania</option>
                                                            <option value="Mauritius">Mauritius</option>
                                                            <option value="Mayotte">Mayotte</option>
                                                            <option value="Mexico">Mexico</option>
                                                            <option value="Micronesia, Federated States of">Micronesia, Federated States of</option>
                                                            <option value="Moldova, Republic of">Moldova, Republic of</option>
                                                            <option value="Monaco">Monaco</option>
                                                            <option value="Mongolia">Mongolia</option>
                                                            <option value="Montenegro">Montenegro</option>
                                                            <option value="Montserrat">Montserrat</option>
                                                            <option value="Morocco">Morocco</option>
                                                            <option value="Mozambique">Mozambique</option>
                                                            <option value="Myanmar">Myanmar</option>
                                                            <option value="Namibia">Namibia</option>
                                                            <option value="Nauru">Nauru</option>
                                                            <option value="Nepal">Nepal</option>
                                                            <option value="Netherlands">Netherlands</option>
                                                            <option value="Netherlands Antilles">Netherlands Antilles</option>
                                                            <option value="New Caledonia">New Caledonia</option>
                                                            <option value="New Zealand">New Zealand</option>
                                                            <option value="Nicaragua">Nicaragua</option>
                                                            <option value="Niger">Niger</option>
                                                            <option value="Nigeria">Nigeria</option>
                                                            <option value="Niue">Niue</option>
                                                            <option value="Norfolk Island">Norfolk Island</option>
                                                            <option value="North Korea">North Korea</option>
                                                            <option value="Northern Mariana Islands">Northern Mariana Islands</option>
                                                            <option value="Norway">Norway</option>
                                                            <option value="Oman">Oman</option>
                                                            <option value="Pakistan">Pakistan</option>
                                                            <option value="Palau">Palau</option>
                                                            <option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
                                                            <option value="Panama">Panama</option>
                                                            <option value="Papua New Guinea">Papua New Guinea</option>
                                                            <option value="Paraguay">Paraguay</option>
                                                            <option value="Peru">Peru</option>
                                                            <option value="Philippines">Philippines</option>
                                                            <option value="Pitcairn">Pitcairn</option>
                                                            <option value="Poland">Poland</option>
                                                            <option value="Portugal">Portugal</option>
                                                            <option value="Puerto Rico">Puerto Rico</option>
                                                            <option value="Qatar">Qatar</option>
                                                            <option value="Reunion">Reunion</option>
                                                            <option value="Romania">Romania</option>
                                                            <option value="Russian Federation">Russian Federation</option>
                                                            <option value="Rwanda">Rwanda</option>
                                                            <option value="Saint Kitts and Nevis">Saint Kitts and Nevis</option>
                                                            <option value="Saint Lucia">Saint Lucia</option>
                                                            <option value="Saint Vincent and the Grenadines">Saint Vincent and the Grenadines</option>
                                                            <option value="Samoa">Samoa</option>
                                                            <option value="San Marino">San Marino</option>
                                                            <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                                                            <option value="Saudi Arabia">Saudi Arabia</option>
                                                            <option value="Senegal">Senegal</option>
                                                            <option value="Serbia">Serbia</option>
                                                            <option value="Seychelles">Seychelles</option>
                                                            <option value="Sierra Leone">Sierra Leone</option>
                                                            <option value="Singapore">Singapore</option>
                                                            <option value="Slovak Republic">Slovak Republic</option>
                                                            <option value="Slovenia">Slovenia</option>
                                                            <option value="Solomon Islands">Solomon Islands</option>
                                                            <option value="Somalia">Somalia</option>
                                                            <option value="South Africa">South Africa</option>
                                                            <option value="South Georgia South Sandwich Islands">South Georgia &amp; South Sandwich Islands</option>
                                                            <option value="South Sudan">South Sudan</option>
                                                            <option value="Spain">Spain</option>
                                                            <option value="Sri Lanka">Sri Lanka</option>
                                                            <option value="St. Barthelemy">St. Barthelemy</option>
                                                            <option value="St. Helena">St. Helena</option>
                                                            <option value="St. Martin (French part)">St. Martin (French part)</option>
                                                            <option value="St. Pierre and Miquelon">St. Pierre and Miquelon</option>
                                                            <option value="Sudan">Sudan</option>
                                                            <option value="Suriname">Suriname</option>
                                                            <option value="Svalbard and Jan Mayen Islands">Svalbard and Jan Mayen Islands</option>
                                                            <option value="Swaziland">Swaziland</option>
                                                            <option value="Sweden">Sweden</option>
                                                            <option value="Switzerland">Switzerland</option>
                                                            <option value="Syrian Arab Republic">Syrian Arab Republic</option>
                                                            <option value="Taiwan">Taiwan</option>
                                                            <option value="Tajikistan">Tajikistan</option>
                                                            <option value="Tanzania, United Republic of">Tanzania, United Republic of</option>
                                                            <option value="Thailand">Thailand</option>
                                                            <option value="Togo">Togo</option>
                                                            <option value="Tokelau">Tokelau</option>
                                                            <option value="Tonga">Tonga</option>
                                                            <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                                                            <option value="Tunisia">Tunisia</option>
                                                            <option value="Turkey">Turkey</option>
                                                            <option value="Turkmenistan">Turkmenistan</option>
                                                            <option value="Turks and Caicos Islands">Turks and Caicos Islands</option>
                                                            <option value="Tuvalu">Tuvalu</option>
                                                            <option value="Uganda">Uganda</option>
                                                            <option value="Ukraine">Ukraine</option>
                                                            <option value="United Arab Emirates">United Arab Emirates</option>
                                                            <option value="United Kingdom">United Kingdom</option>
                                                            <option value="United States">United States</option>
                                                            <option value="United States Minor Outlying Islands">United States Minor Outlying Islands</option>
                                                            <option value="Uruguay">Uruguay</option>
                                                            <option value="Uzbekistan">Uzbekistan</option>
                                                            <option value="Vanuatu">Vanuatu</option>
                                                            <option value="Vatican City State (Holy See)">Vatican City State (Holy See)</option>
                                                            <option value="Venezuela">Venezuela</option>
                                                            <option value="Viet Nam">Viet Nam</option>
                                                            <option value="Virgin Islands (British)">Virgin Islands (British)</option>
                                                            <option value="Virgin Islands (U.S.)">Virgin Islands (U.S.)</option>
                                                            <option value="Wallis and Futuna Islands">Wallis and Futuna Islands</option>
                                                            <option value="Western Sahara">Western Sahara</option>
                                                            <option value="Yemen">Yemen</option>
                                                            <option value="Zambia">Zambia</option>
                                                            <option value="Zimbabwe">Zimbabwe</option>
									</select>
								</div>  
							</div>
						  </div>
							
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label class="select">State</label>
								<div class="select-cat">
								  <select class="form-control">
									  <option>Rajasthan</option>
									  <option>Gujarat</option> 
									  <option>Madhya pradesh</option> 
									</select>
								</div>	
							</div>
						  </div>
							
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12">
							<div class="form-group">
							  <label>Address One</label>
							  <input type="text" class="form-control"  placeholder="Office 532, Manglam Mall">
							</div>
						  </div>

						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label>Address Two</label>
							  <input type="text" class="form-control"  placeholder="Ridhi, Sidhi circle, Goplapura, Jaipur">
							</div>
						  </div>
						
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label>How did you get interested or involved with these skill sets?</label>
							  <textarea rows="5" class="form-control"  placeholder="Your answer here"></textarea>
							</div>
						  </div>
							
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label>Tell us more about yourself!</label>
							  <textarea rows="5" class="form-control"  placeholder="Your answer here"></textarea>
							</div>
						  </div>
							
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label>What is your personal approach to your craft?</label>
							  <textarea  rows="5" class="form-control"  placeholder="Your answer here"></textarea>
							</div>
						  </div>	
						  
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">
							<div class="form-group">
							  <label>What motivates you to be great at what you do, and to teach others?</label>
							  <textarea rows="5" class="form-control"  placeholder="Your answer here"></textarea>
							</div>
						  </div>
						
						  <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">	
							  <div class="channel-subscribe-page">
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-3">
										<div class="subscribe-list">
											<div class="subscribe-user">
												<img src="<?php echo base_url();?>my-assets/images/cute-girl.jpg" alt="" />
											</div>
										</div>
									</div>
									<div class="col-lg-9 col-md-9 col-sm-9">
										<label>Company Logo</label>
										<div class="vdo-upload-search">				
											<div class="selectFile">       
											  <label for="file">Upload Your Logo Image</label>                   
											  <input type="file" name="files[]" id="file">
											</div>
										</div>
									</div>
								  </div>
								</div>
							</div>
							 <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-12">	
								<div class="vdo-post-btn-section">
									<button class="gray-theme-btn" type="button">Cancel</button>
								</div>	
								<div class="vdo-post-btn-section">
									<button class=" theme-btn" type="button">Save Changes</button>
								</div>		
							</div> 
	
						  
							</form>
						</div><!--/ Row-->
					 
						<hr>

						<form method="post" name="form" onSubmit="return validation()" action="<?php echo base_url()?>index.php/home/changepassword/<?php echo $this->session->userdata('id'); ?>">
						<div class="change-ps">
							<div class="heading">
							  <div class="heading-outer">
								 <h3>CHANGE <span> PASSWORD</span></h3>
							  </div>
							</div>
							<div class="row">
								<div style="color:#FF0000;text-align:center;" id="formerror"></div>
						  <?php if($this->session->flashdata('err')) { ?><p style="color:#FF0000">
						  <?php echo $this->session->flashdata('err'); }?></p>
						  <?php if($this->session->flashdata('flash_message')) { ?>
						  <p style="color:#006633"> <?php echo $this->session->flashdata('flash_message'); } ?> </p>
								<div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
									<div class="form-group">
									  <label>Old Password</label>
									  <input type="password" class="form-control"  id="password" name="password" placeholder="Enter Current Password">
									</div>
								</div>
								<div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
									<div class="form-group">
									  <label>New Password</label>
									  <input type="password" class="form-control"  id="new_pass" name="new_pass" placeholder="Enter a Password">
									</div>
								</div>
								<div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
									<div class="form-group">
									  <label>New Password</label>
									  <input type="password" class="form-control"  onChange="validate_pass(this)" id="confirm_password" name="confirm_password" placeholder="Confirm your Password">
									</div>
								</div>
								 <div id="selectPeriodRangePanel" style="display:none;color:#FF0000;margin-left:20px;">
								    <p style="color:#FF0000">Password and Confirm Password Does Not match!</p>
								</div>

								<div id="lengtherr" style="display:none;color:#FF0000;margin-left:20px;">
								    <p style="color:#FF0000">Your password must be at least 8 characters!</p>
								</div>
								<div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">	
									<div class="vdo-post-btn-section">
										<button class="gray-theme-btn" type="submit">Cancel</button>
									</div>	
									<div class="vdo-post-btn-section">
										<button class="theme-btn" type="submit">Save Changes</button>
									</div>		
								</div> 
							</div>
						</div>		
					  </form>
			 		</div> 
			</div>
         </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      
	
      
<?php include_once('footer.php');?>
   </body>
</html>


<script>
  function validation() { 

  var fpassword = document.form.password.value;
  var npassword = document.form.new_pass.value;
  var cpassword = document.form.confirm_password.value;
 alert(fpassword);
    if( fpassword == "" )
		{
	   document.getElementById("formerror").innerHTML = "Enter the Current Password";
		return false; 
		}
		
		if( npassword == "" )
		{
	   document.getElementById("formerror").innerHTML = "Enter the New Password";
		return false; 
		}
		if( cpassword == "" )
		{
	   document.getElementById("formerror").innerHTML = "Enter Confirm Password";
		return false; 
		}
		
		
		
	}
  </script>
 <script>
function validate_pass(that)
 {
 var pass = ($('#new_pass').val());
   var cpass = ($('#confirm_password').val());
  if(pass.length < 8)
  { 
 	$("#lengtherr").show();return false;}
  if(pass != cpass)
  {	
  
  	 
  	$("#selectPeriodRangePanel").show();
	
	return false;
  }
  }
  </script>