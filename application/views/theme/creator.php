<!doctype html>
<html lang="en">
   <head>
      <title>Creators</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
   </head>
   <body>
     <?php include_once('header.php'); ?>
      <section class="banner">
         <div class="container">
           <div class="banner-heading">
               <h2>Creators </h2>
               <p><a href="index.html">Home</a>  /  Creators </p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="trending-section creator">
				<div class="heading">
                  <div class="heading-outer">
                     <h3>HOW TO  <span> CREATE VIDEO CLIP</span></h3>
                  </div>
                </div>
				<div class="guidelines merchandise">
					<div class="about-channel">
						<div class="safety-tools">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<i class="icon-blog"></i>
										<h4>Open your own Account</h4>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
										<div class="point">
											<h2>1</h2>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<i class="icon-blog"></i>
										<h4>Open your own Account</h4>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
										<div class="point">
											<h2>2</h2>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<i class="icon-blog"></i>
										<h4>Open your own Account</h4>
										<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
										<div class="point">
											<h2>3</h2>
										</div>
									</div>
								</div>

							</div>
							<div class="choose-our-channel">
								<h5>Instruction for make Video clip upload</h5>
								<ul>
									<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>

									<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>

									<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>

									<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>

									<li><span>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga </span></li>
									
								</ul>
							</div>
							<div class="heading">
							  <div class="heading-outer">
								 <h3>HOWCLIP  <span> CREATER TIPS </span></h3>
								</div>
							</div>	
								  <div class="all-videos">
									  <div id="owl-demo4" class="owl-carousel owl-theme">
										 <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/2.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <div class="payble">
													 <a href="javascript:;">
														<h4><span>$</span>2.00</h4>
														<span>View</span><i class="icon-play icon-play"></i><span>Clip</span>
													 </a>
												  </div>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										  <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/3.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <a href="javascript:;"><i class="icon-play play-btn-lg"></i></a>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										 <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/4.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <a href="javascript:;"><i class="icon-play play-btn-lg"></i></a>	
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										 <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/2.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <a href="javascript:;"><i class="icon-play play-btn-lg"></i></a>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										  <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/3.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <div class="payble">
													 <a href="javascript:;">
														<h4><span>$</span>2.00</h4>
														<span>View</span><i class="icon-play icon-play-lg"></i><span>Clip</span>
													 </a>
												  </div>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										 <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/4.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <a href="javascript:;"><i class="icon-play play-btn-lg"></i></a>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										 <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/2.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <a href="javascript:;"><i class="icon-play play-btn-lg"></i></a>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										  <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/3.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <div class="payble">
													 <a href="javascript:;">
														<h4><span>$</span>2.00</h4>
														<span>View</span><i class="icon-play icon-play-lg"></i><span>Clip</span>
													 </a>
												  </div>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div>
										 <div class="item">
											<div class="video-section press-profile">
											   <div class="video-img">
												  <img src="<?php echo base_url();?>my-assets/images/4.png" alt="" /> 
												  <span class="duration">11:15</span>
												  <a href="javascript:;"><i class="icon-play play-btn-lg"></i></a>
											   </div>
											   <div class="press-detail">
													<ul>
														<li><span><i class="icon-full-name"></i></span>Username</li>
														<li><span><i class="icon-clock"></i></span>February 3, 2016</li>
													</ul>
													<h4>Star Wars Ut enim ad minim veniam, quis nostrud exercitrs Ut enim ad minim veniam.</h4>
												   <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
												</div>
											</div>
										 </div> 
									  </div>
								  </div> 
						</div>
					</div>
				</div>
			 </div>	
		  </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      
	<script >
		(function($){
			$(window).on("load",function(){


				$("#side-bar").mCustomScrollbar({
					scrollButtons:{enable:true,scrollType:"stepped"},
					keyboard:{scrollType:"stepped"},
					mouseWheel:{scrollAmount:188},
					theme:"rounded-dark",
					autoExpandScrollbar:true,
					snapAmount:188,
					snapOffset:65
				});

			});
		})(jQuery);
	</script>
	   
      <script  >
         $(".bar").click(function(){
         	$(".side-bar").toggleClass("show");
         })
         $(".white-close").click(function(){
         	$(".ad").addClass("display");
         	$("header").addClass("fix");
         })
         $(".arrow").click(function(){
         	$("body,html").animate({scrollTop : 0}, 500);
         })
         $(window).scroll(function(){
         	if($(document).scrollTop() > 48){
         		$("header").addClass("sticky");
         	}
         	else{
         		$("header").removeClass("sticky");
         	}
         })
		   $(".nav-search-icon").click(function(){
         	$(".pop-up-serch.search-section").addClass("visit");
         })
		  $(".remove-search .icon-close").click(function(){
         	$(".pop-up-serch.search-section").removeClass("visit");
         })
         
         
      </script>
      <script  >
         $(".forget-ps a").click(function(){
         	$(".form-sign-in").toggleClass("show");
         	$(".modal-backdrop").removeClass("show");
         })
      </script>
      <script  >
         $(document).ready(function() {
           var owl = $("#owl-demo1");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
		   $(document).ready(function() {
           var owl = $("#owl-demo2");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
		   $(document).ready(function() {
           var owl = $("#owl-demo3");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
		   $(document).ready(function() {
           var owl = $("#owl-demo4");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
      </script> 
      <script >
         (function($) {
         $.fn.menumaker = function(options) {  
         var cssmenu = $(this), settings = $.extend({
          format: "dropdown",
          sticky: false
         }, options);
         return this.each(function() {
          $(this).find(".button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.slideToggle().removeClass('open');
            }
            else {
              mainmenu.slideToggle().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }

            }
          });
          cssmenu.find('li ul').parent().addClass('has-sub');
         multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideToggle();
              }
              else {
                $(this).siblings('ul').addClass('open').slideToggle();
              }
            });
          };
          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
          if (settings.sticky === true) cssmenu.css('position', 'fixed');
         resizeFix = function() {
         var mediasize = 1000;
            if ($( window ).width() > mediasize) {
              cssmenu.find('ul').show();
            }
            if ($(window).width() <= mediasize) {
              cssmenu.find('ul').hide().removeClass('open');
            }
          };
          resizeFix();
          return $(window).on('resize', resizeFix);
         });
         };
         })(jQuery);
         
         (function($){
         $(document).ready(function(){
         $("#cssmenu").menumaker({
          format: "multitoggle"
         });
         });
         })(jQuery);
         
      </script>
      <script  >$(document).ready(function() {
         $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
         $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
         });
		  $('.select2').select2({
		  });
		  $("#grid").click(function(){
			  $("#grid").addClass("active");
			  $("#list").removeClass("active");
		  })
		   $("#list").click(function(){
			   $("#grid").removeClass("active");
			  $("#list").addClass("active");
		  })
      </script>
	<script  >
		 $(".filter-heading .icon-list").click(function(){
			  $(".filter-heading .icon-remove").show();
			  $(".filter-heading .icon-list").hide();
			 $(".category-wrapper").addClass("display");
		  })
		   $(".filter-heading .icon-remove").click(function(){
			   $(".filter-heading .icon-list").show();
			  $(".filter-heading .icon-remove").hide();
			   
			 $(".category-wrapper").removeClass("display");
		  })
	</script>
	<script >
		$(document).bind('dragover', function (e) {
			var dropZone = $('.zone'),
				timeout = window.dropZoneTimeout;
			if (!timeout) {
				dropZone.addClass('in');
			} else {
				clearTimeout(timeout);
			}
			var found = false,
				node = e.target;
			do {
				if (node === dropZone[0]) {
					found = true;
					break;
				}

				node = node.parentNode;
			} while (node != null);
			if (found) {
				dropZone.addClass('hover');
			} else {
				dropZone.removeClass('hover');
			}
			window.dropZoneTimeout = setTimeout(function () {
				window.dropZoneTimeout = null;
				dropZone.removeClass('in hover');
			}, 100);
		});   
	</script>   
		<script >
				
				$(document).ready(function() {
        //Horizontal Tab
        $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        // Child Tab
        $('#ChildVerticalTab_1').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true,
            tabidentify: 'ver_1', // The tab groups identifier
            activetab_bg: '#fff', // background color for active tabs in this group
            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
        });

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
				
				
				</script>	
	   <script >
		(function($){
			$(window).on("load",function(){
				
				
				$("#content-9").mCustomScrollbar({
					scrollButtons:{enable:true,scrollType:"stepped"},
					keyboard:{scrollType:"stepped"},
					mouseWheel:{scrollAmount:188},
					theme:"rounded-dark",
					autoExpandScrollbar:true,
					snapAmount:188,
					snapOffset:65
				});
				
			});
		})(jQuery);
	</script>
	   <script >
	   
	   	$(document).ready(function(){
		  $(".comment-section .content").slice(0, 1).show();
		  $("#loadMore").on("click", function(e){
			e.preventDefault();
			$(".content:hidden").slice(0, 4).slideDown();
			if($(".content:hidden").length == 0) {
			  $("#loadMore").text("No Content").addClass("noContent");
			}
		  });

		})
	   </script> 
	
<?php include_once('footer.php') ?>
   </body>
</html>




