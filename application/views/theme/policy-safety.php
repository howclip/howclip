<!doctype html>
<html lang="en">
   <head>
      <title>Policy & Safety</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
   </head>
   <body>
   <?php include_once('header.php') ?>
      <section class="banner">
         <div class="container">
           <div class="banner-heading">
               <h2>Policy & Safety</h2>
               <p><a href="index.html">Home</a>  /  Policy & Safety </p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="trending-section policy-safety">
				<div class="guidelines">
					<div class="about-channel">
						 <p>On the other hand, we denounce with righteous indignation and dislike men who are so beguiled and demoralized by the charms of pleasure of the moment, so blinded by desire, that they cannot foresee the pain and trouble that are bound to ensue; and equal blame belongs to those who fail in their duty through weakness of will, which is the same as saying through shrinking from toil and pain. These cases are perfectly simple and easy to distinguish. </p>
						<p>In a free hour, when our power of choice is untrammelled and when nothing prevents our being able to do what we like best, every pleasure is to be welcomed and every pain avoided. But in certain circumstances and owing to the claims of duty or the obligations of business it will frequently occur that pleasures have to be repudiated and annoyances accepted. The wise man therefore always holds in these matters to this principle of selection: he rejects pleasures to secure other greater pleasures, or else he endures pains to avoid worse pains. </p>
	
						<div class="safety-tools">
							<h3>Safety Tools & Resources </h3>
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<h4>Harmful or dangerous content</h4>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<h4>Harmful or dangerous content</h4>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<h4>Harmful or dangerous content</h4>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<h4>Harmful or dangerous content</h4>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<h4>Harmful or dangerous content</h4>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-6">
									<div class="resource-content">
										<h4>Harmful or dangerous content</h4>
										<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
									</div>
								</div>
							</div>
						</div>
						<div class="choose-our-channel">
							<h5>Section 1.10.33 of "de Finibus Bonorum et Malorum", written by Cicero in 45 BC</h5>
							<ul>
								<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>

								<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>

								<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>

								<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>
								
								<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>
								
								<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>


							</ul>
						</div>	
					</div>
				</div>	
			 </div>	
		  </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
<?php include_once('footer.php') ?>
   </body>
</html>




