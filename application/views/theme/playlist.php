<!doctype html>
<html lang="en">
   <head>
      <title>Playlist</title>
     
   </head>
   <body>
      <?php include_once('header.php');?>
      <section class="banner">
         <div class="container">
            <div class="banner-heading">
               <h2>Playlist</h2>
               <p><a href="index.html">Home</a> / <a href="#">My account </a>/ Playlist </p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="uploading-vdo-section trending-section">
				<div class="description">
					<div class="row">

						<div class="col-lg-7 col-md-7 col-sm-12">
							<div class="video-uploading">
								<div class="video-img">
									<a href="#"><img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger-2.png" alt="" />
										<i class="icon-play play-btn-lg"></i>
									</a>
								</div>	
							</div>
							<div class="vdo-description">
									<div class="hashtags">
										<ul>
											<li><a href="#">#Petlover</a></li>
											<li><a href="#">#animal</a></li>
											<li><a href="#">#Petlover</a></li>
										</ul>
									</div>
									<div class="vdo-title">
										<div class="row">
											<div class="col-lg-7 col-md-7">
												<h3>Automating Content Creation</h3>
												<ul>
													<li ><button class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
														<i class="icon-plus"></i> Add to
													  </button>
														<ul class="dropdown-menu" role="menu">
															<li>
																<form>
																	<p><span class="add-list">ADD TO PLAYLIST</span> <span class="icon-remove"></span></p>
																	<input type="checkbox" value="a" name="add"><span class="check-para">Lorem Ipsum is simply dummy text</span><br>
																	<input type="checkbox" value="b" name="add"><span class="check-para">There are many variations passage</span>
																	<input type="text" class="form-control" placeholder="Create a new playlist">
																	<span class="select-cat">
																		<select class="select2" >
																			<optgroup label="Public">
																			  <option value="" selected> Public </option>	
																			  <option value="co" > Public </option>
																			  <option value="da"> Private </option>
																			  <option value="si"> Clip4 </option>
																			</optgroup>
																		  </select>
																	</span>
																	<a href="#" class="theme-btn">Create</a>
																</form>
															</li>	
														  </ul>
														
													</li>
													
													<li><a href="#"><i class="icon-thumbs-up-alt"></i>2.5k</a></li>
													<li><a href="#"><i class="icon-thumbs-down-alt"></i>0.5k</a></li>
													<li><a href="#"><i class="icon-share"></i>5 Shares</a></li>
													<li><a href="#"><i class="icon-code"></i>Embed</a></li>
												</ul>
											</div>	
											<div class="col-lg-5 col-md-5">
												<div class="vdo-category">
													<p>Category : <a href="#">Creative Professions</a></p>
												</div>	
												<div class="subscribe">
													<a href="#"><i class="icon-eye-open"></i><span>4.4M Views</span></a>
													<a href="#" class="theme-btn">Subscribe</a>

												</div>
											</div>
										</div>	
									</div>
								</div>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-12">
								<div class="other-video-clip">
									<div class="auto-mode">
										<span class="up-next">Your Playlist</span>

									</div>
									<div class="any-videos" id="content-9">
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	
										<div class="up-nxt-vdo">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
													<a href="#">
													<div class="vdo-img">
														<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
														<span class="duration">11:15</span>
													</div>
														</a>
												</div>
												<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
													<div class="content">
														<p class="title"><a href="#">Playlist name here</a> </p>
														<p>Jane Doe</p>
														<a href="#"><span class="view">Privacy  : </span> Public</a>
														<div class="edit-delete">
															<a href="#"><span class="icon-edit"></span></a>
															<a href="#"><span class="icon-trash"></span></a>
														</div>
													</div>
												</div>
											</div>
										</div>	


									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-7 col-md-7">
								<div class="vdo-description">
										<div class="vdo-detail">
												<div class="card">
													<div class="card-body">
														<div class="row">
															<div class="col-md-2">
																<div class="user-pic">
																	<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																</div>	
															</div>
															<div class="col-md-10">
																<p>
																	<a  href="#"><strong>	Jane Doe</strong></a>
																	<span class="float-right">Published on <i class=" icon-time"></i> May 21, 2019</span>

															   </p>
															   <div class="clearfix"></div>
																<p>Lorem Ipsum is simply dummy text of the pr make  but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. <a href="#" class="footer-more"> Read More</a></p>

															</div>
														</div>
													</div>
												</div>
										</div>
										<div class="comment-section">
											<h3><i class="icon-comment-alt"></i>50 comments</h3>
											<div class="card content">
													<div class="card-body">
														<div class="row">
															<div class="col-md-2">
																<div class="user-pic">
																	<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																</div>	
															</div>
															<div class="col-md-10">
																<p>
																	<a  href="#"><strong>Rishab Agarwal</strong></a>
																	<span><i>1 day ago</i></span>
																	<span class="float-right"><i class=" icon-comment-dots"></i></span>

															   </p>
															   <div class="clearfix"></div>
																<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																<p class="reply-section">
																	<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																	<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																	<a class="float-right "> Reply</a>
															   </p>
															</div>
														</div>
													</div>
												</div>
												<div class="card content">
													<div class="card-body">
														<div class="row">
															<div class="col-md-2">
																<div class="user-pic">
																	<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																</div>	
															</div>
															<div class="col-md-10">
																<p>
																	<a  href="#"><strong>Jugnun Agarwal</strong></a>
																	<span><i>1 day ago</i></span>
																	<span class="float-right"><i class=" icon-comment-dots"></i></span>

															   </p>
															   <div class="clearfix"></div>
																<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																<p class="reply-section">
																	<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																	<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																	<a class="float-right "> Reply</a>
															   </p>
															</div>
														</div>
													</div>
												</div>
												<div class="card content">
													<div class="card-body">
														<div class="row">
															<div class="col-md-2">
																<div class="user-pic">
																	<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																</div>	
															</div>
															<div class="col-md-10">
																<p>
																	<a  href="#"><strong>Abhay Agarwal</strong></a>
																	<span><i>1 day ago</i></span>
																	<span class="float-right"><i class=" icon-comment-dots"></i></span>

															   </p>
															   <div class="clearfix"></div>
																<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																<p class="reply-section">
																	<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																	<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																	<a class="float-right "> Reply</a>
															   </p>
															</div>
														</div>
													</div>
												</div>
												<div class="card content">
													<div class="card-body">
														<div class="row">
															<div class="col-md-2">
																<div class="user-pic">
																	<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																</div>	
															</div>
															<div class="col-md-10">
																<p>
																	<a  href="#"><strong>Avinash</strong></a>
																	<span><i>1 day ago</i></span>
																	<span class="float-right"><i class=" icon-comment-dots"></i></span>

															   </p>
															   <div class="clearfix"></div>
																<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																<p class="reply-section">
																	<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																	<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																	<a class="float-right "> Reply</a>
															   </p>
															</div>
														</div>
													</div>
												</div>
												<div class="card content">
													<div class="card-body">
														<div class="row">
															<div class="col-md-2">
																<div class="user-pic">
																	<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" class="img img-fluid" alt="" />
																</div>	
															</div>
															<div class="col-md-10">
																<p>
																	<a  href="#"><strong>Mukul</strong></a>
																	<span><i>1 day ago</i></span>
																	<span class="float-right"><i class=" icon-comment-dots"></i></span>

															   </p>
															   <div class="clearfix"></div>
																<p>Lorem Ipsum is simply dummy text of the pr make. </p>
																<p class="reply-section">
																	<a class="float-right "> <i class="font icon-thumbs-up-alt"></i>2.5k</a>
																	<a class="float-right "> <i class="font icon-thumbs-down-alt"></i>0.5k</a>
																	<a class="float-right "> Reply</a>
															   </p>
															</div>
														</div>
													</div>
												</div>
											<a href="#" id="loadMore"><i>Load previous comments</i><span class="icon-angle-down"></span></a>
										</div>
										<div class="add-comment-section">
											<h3>Add a new comment</h3>
											<form>
												<textarea placeholder="Type Your Comment Here …..." rows="5"></textarea>
												<button type="submit" class="theme-btn">POST</button>
											</form>
										</div>
									</div>
							</div>
							<div class="col-lg-5 col-md-5 col-sm-12">
								<div class="related-vdo">
										<div class="other-video-clip">
										<div class="auto-mode">
											<span class="up-next">Related Video Clips</span>

										</div>
										<div class="any-videos">
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>	
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>	
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>	
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>	
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>	
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>	

											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>
											<a href="#">
												<div class="up-nxt-vdo">
													<div class="row">
														<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
															<div class="vdo-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />
																<span class="duration">11:15</span>
															</div>
														</div>
														<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
															<div class="content">
																<p class="title">Clip title here according to you write</p>
																<p>Jane Doe</p>
																<p class="view">4.5k Views</p>
															</div>
														</div>
													</div>
												</div>
											</a>
										</div>
									</div>

									</div>
							</div>
						</div>	
					</div>
				
				</div>	
			</div>
         </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
              
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      <?php include_once('footer.php');?>

   </body>
</html>


