<!doctype html>
<html lang="en">
   <head>
      <title>About Us</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
   </head>
   <body>
      <?php include_once('header.php')?>
      <section class="banner">
         <div class="container">
           <div class="banner-heading">
               <h2>About Us</h2>
               <p><a href="index.html">Home</a>  /  <a href="index.html">About Us</a>  /  About HowClip </p>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <div class="trending-section about-us">
				<div class="description">
					<div class="vdo-description">
						
						 <div id="parentHorizontalTab">
							<ul class="resp-tabs-list hor_1">
								<li>About HowClip</li>
								<li>Community Guidelines</li>
								<li>Brand Guidelines</li>
								<li>Careers</li>
								<li>Merchandise </li>
							</ul>
							<div class="resp-tabs-container hor_1">
								<div class="about-howclip">	
									<div class="who-we">
										<div class="who-heading">
											 <h3>WHO  <span> WE ARE?</span></h3>
											 <h5>HowClip, the premier eLearning Content Creator Monetization Platform. Organizing all learning videos in 
												 short, easily searchable, highly categorized clips. </h5>
										</div>
										<div class="content">
											<div class="row">
												<div class="col-lg-7 col-md-7 col-sm-12">
													<div class="content-para">
														<p>Be a teacher and earn money from ad revenue or adding a paywall to your clips. Create a profile and start start building your portfolio of videos. Use playlists to stitch your content along with others to increase your view count</p>
														
														<p>Be a learner and use our advanced categorization search function to find the exact micro skill you are looking for. With 5 levels of microcategorization, get as granular as possible! Segregate tasks by the smallest components and discover the best way to accomplish the skill. Learn from multiple teachers how to accomplish subtasks and create the best-practice in the world.</p>
														
														<p>Collaborate with other experts to determine what the best possible way to do something is.Innovate and research micro skills that are part of larger projects to determine the best-practices in the world. Challenge other teachers to demonstrate the best possible way to do something and let them show you the way they do it. Each level 5 subcategory can be innovated to be defined as the best practice.</p>
														
														<a href="#javascript:;" data-toggle="modal" data-target="#Modal-3" class="sign-up-now">Sign up now </a>
													</div>
												</div>
												<div class="col-lg-5 col-md-5 col-sm-12">
													<div class="content-img">
														<a href="#">
															<img src="<?php echo base_url();?>my-assets/images/who-we-pic.png" alt="" />
														</a>	
													</div>
												</div>
											</div>
										</div>
									</div>
									
									<div class="contact-us">
										<div class="who-heading">
											 <h3>CONTACT   <span> US</span></h3>
											 <h5>Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs. The passage is attributed to an unknown typesetter in the 15th century who is thought to have. </h5>
										</div>
										<div class="contact-form">
											<div class="row">
												<div class="col-lg-8 col-md-8 col-sm-12">
													<div class="profile-edit-form feed-back">
														<h4><span><i class="icon-get-in-touch"></i></span>Get In Touch</h4>
														<form>
															<div class="row">
															  <div class="col-lg-6 col-xs-12 col-sm-6">
																<div class="form-group">
																  <input type="text" class="form-control"  placeholder="Name">
																</div>
															  </div>

															 <div class="col-lg-offset-0 col-lg-6 col-xs-12 col-sm-6">
																<div class="form-group">
																  <input type="email" class="form-control"  placeholder="Email">
																</div>
															  </div>

															  <div class="col-lg-offset-0 col-lg-12 col-xs-12 col-sm-12">
																<div class="form-group">
																  <input type="text" class="form-control"  placeholder="Subject">
																</div>
															  </div>

															  <div class="col-lg-offset-0 col-lg-12 col-xs-12 col-sm-12">
																<div class="form-group">
																  <textarea rows="5" class="form-control"  placeholder="Write Message "></textarea>
																</div>
															  </div>
															 <div class="col-lg-offset-0 col-lg-12 col-xs-12 col-sm-12">  
																<div class="vdo-post-btn-section">
																	<button class=" theme-btn" type="submit">Send Message</button>
																</div>	
															</div>	 
																	
														  </div>
														</form>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-12">
													<div class="contact-info">
														<h4><span><i class="icon-contact-us"></i></span>Contact Us</h4>
														<ul>
															<li><span><i class="icon-call"></i></span>+44 345 678 903</li>
															<li><span><i class=" icon-email"></i></span>adobexd@mail.com</li>
															<li><span><i class="icon-address"></i></span>497 Evergreen Rd. Roseville, CA 95673c  44 345 678 903</li>
														</ul>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="copyright">
										<div class="who-heading">
											 <h3>COPYRIGHT <span> ISSUE</span></h3>
											 <h5>For DMCA takedown notices, please contact David@howclip.com  </h5>
										</div>
									</div>
									<div class="get-start">
										<div class="who-heading">
											 <h3>GETTING  <span> STARTED</span></h3>
											 <h5>To get started, simple create a profile, start uploading videos, and create your subcategories of interest. 
												 Choose the clip lengths (maximum 2:40 seconds) so keep the content concise create a profile and begin exploring the subcategories, user profiles, and playlists to discover custom expert learning content!  </h5>
										</div>
									</div>
									
								</div>
								
								<div class="guidelines community">
									<div class="about-channel">
										 <h3>COMMUNITY <span> GUIDELINES</span></h3>
										 <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
										<p>"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?" 1914 translation by H. Rackham. </p>
										<p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. </p>
										
										<div class="choose-our-channel">
											<h5>Lorem Ipsum - All the facts - Lipsum generator</h5>
											<ul>
												<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>
												
												<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>
												
												<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>
												
												<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>
												
												
											</ul>
										</div>	
										<div class="safety-tools">
											<h3>Safety Tools & Resources </h3>
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Harmful or dangerous content</h4>
														<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Harmful or dangerous content</h4>
														<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Harmful or dangerous content</h4>
														<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Harmful or dangerous content</h4>
														<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Harmful or dangerous content</h4>
														<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Harmful or dangerous content</h4>
														<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi</p>
													</div>
												</div>
											</div>
										</div>

									</div>
									

								</div>

								<div class="guidelines brand">
									<div class="about-channel">
										 <h3>BRAND <span> GUIDELINES</span></h3>
										 <p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. </p>
										<p>"Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?" 1914 translation by H. Rackham. </p>
										<p>"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. </p>
										<p>No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain. </p>
										
										<div class="choose-our-channel">
											<h5>Lorem Ipsum - All the facts - Lipsum generator</h5>
											<ul>
												<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>
												
												<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>
												
												<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>
												
												<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>
												
												<li><span>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga </span></li>
												
												
											</ul>
										</div>	
										

									</div>
									

								</div>
								 
								<div class="guidelines career">
									<div class="about-channel">
										 <h3>CAREERS  <span> @ HOWCLIP</span></h3>
										 <p>Pursue your career in a growing and rewarding environment while working along side a great group of professionals </p>
											
										<div class="safety-tools">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Website Content Writer</h4>
														<ul>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />No. of Vacancies: 2</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Work Status: Full time</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Location: Jaipur</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Experience: 3 - 5 Years</span></li>
														</ul>	
														<a href="#" class="theme-btn" >Apply Now</a>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Website Content Writer</h4>
														<ul>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />No. of Vacancies: 2</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Work Status: Full time</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Location: Jaipur</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Experience: 3 - 5 Years</span></li>
														</ul>	
														<a href="#" class="theme-btn" >Apply Now</a>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Website Content Writer</h4>
														<ul>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />No. of Vacancies: 2</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Work Status: Full time</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Location: Jaipur</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Experience: 3 - 5 Years</span></li>
														</ul>	
														<a href="#" class="theme-btn" >Apply Now</a>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Website Content Writer</h4>
														<ul>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />No. of Vacancies: 2</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Work Status: Full time</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Location: Jaipur</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Experience: 3 - 5 Years</span></li>
														</ul>	
														<a href="#" class="theme-btn" >Apply Now</a>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Website Content Writer</h4>
														<ul>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />No. of Vacancies: 2</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Work Status: Full time</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Location: Jaipur</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Experience: 3 - 5 Years</span></li>
														</ul>	
														<a href="#" class="theme-btn" >Apply Now</a>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<h4>Website Content Writer</h4>
														<ul>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />No. of Vacancies: 2</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Work Status: Full time</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Location: Jaipur</span></li>
															<li><span><img src="<?php echo base_url();?>my-assets/images/check.png" alt="" />Experience: 3 - 5 Years</span></li>
														</ul>	
														<a href="#" class="theme-btn" >Apply Now</a>
													</div>
												</div>
												
											</div>
											<div class="choose-our-channel">
												<h5>Why Choose Us?</h5>
												<ul>
													<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>

													<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>

													<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>

													<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>

													<li><span>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga </span></li>


												</ul>
											</div>	
										</div>

									</div>
									

								</div>

								<div class="guidelines merchandise">
									<div class="about-channel">
										 <h3>How to Make Merchandise for Your HowClip Channel</h3>
										 <p>Pursue your career in a growing and rewarding environment while working along side a great group of professionals.</p>
											
										<div class="safety-tools">
											<div class="row">
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<i class="icon-blog"></i>
														<h4>Open your own Account</h4>
														<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
														<div class="point">
															<h2>1</h2>
														</div>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<i class="icon-blog"></i>
														<h4>Open your own Account</h4>
														<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
														<div class="point">
															<h2>2</h2>
														</div>
													</div>
												</div>
												<div class="col-lg-4 col-md-4 col-sm-6">
													<div class="resource-content">
														<i class="icon-blog"></i>
														<h4>Open your own Account</h4>
														<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
														<div class="point">
															<h2>3</h2>
														</div>
													</div>
												</div>
												
											</div>
											<div class="choose-our-channel">
												<h4>Promote Your Merch</h4>
												<p>"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem 	aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
												<ul>
													<li><span>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni d </span></li>

													<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur?. </span></li>

													<li><span>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. </span></li>

													<li><span>Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiaeum fugiat quo voluptas nulla pariatur? </span></li>

													<li><span>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga </span></li>


												</ul>
											</div>
											<section class="content-section">
											   <div class="row">
												  <div class="col-lg-7 col-md-7 col-sm-12">
													 <div class="content">
														<h2>Selling merchandise from your channel .</h2>
														<p class="para-section">
														  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut ero labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco.
														</p>
														<a href="#javascript:;" class="theme-btn">LET’S TRY</a>
													 </div>
												  </div>
												  <div class="col-lg-5 col-md-5 col-sm-12">
													 <div class="content-image">
														<a href="#">
															<img src="<?php echo base_url();?>my-assets/images/vdo-img.png" alt="" />
															<i class="icon-play play-btn-lg"></i>
														</a>
														 
													 </div>
												  </div>
											   </div>
											</section>
										</div>
										

									</div>
								</div>
							</div>
						</div>			

					</div>
			 			
						
				</div>
			 </div>	
		  </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section"> 
               <div class="all-videos">    
               </div>
				
            </div>
         </div>
      </div>
     
      
	<script >
		(function($){
			$(window).on("load",function(){


				$("#side-bar").mCustomScrollbar({
					scrollButtons:{enable:true,scrollType:"stepped"},
					keyboard:{scrollType:"stepped"},
					mouseWheel:{scrollAmount:188},
					theme:"rounded-dark",
					autoExpandScrollbar:true,
					snapAmount:188,
					snapOffset:65
				});

			});
		})(jQuery);
	</script>	   
      <script  >
         $(".bar").click(function(){
         	$(".side-bar").toggleClass("show");
         })
         $(".white-close").click(function(){
         	$(".ad").addClass("display");
         	$("header").addClass("fix");
         })
         $(".arrow").click(function(){
         	$("body,html").animate({scrollTop : 0}, 500);
         })
         $(window).scroll(function(){
         	if($(document).scrollTop() > 48){
         		$("header").addClass("sticky");
         	}
         	else{
         		$("header").removeClass("sticky");
         	}
         })
		   $(".nav-search-icon").click(function(){
         	$(".pop-up-serch.search-section").addClass("visit");
         })
		  $(".remove-search .icon-close").click(function(){
         	$(".pop-up-serch.search-section").removeClass("visit");
         })
         
         
      </script>
      <script  >
         $(".forget-ps a").click(function(){
         	$(".form-sign-in").toggleClass("show");
         	$(".modal-backdrop").removeClass("show");
         })
      </script>
      <script  >
         $(document).ready(function() {
           var owl = $("#owl-demo1");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
		   $(document).ready(function() {
           var owl = $("#owl-demo2");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
		   $(document).ready(function() {
           var owl = $("#owl-demo3");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
      </script> 
      <script >
         (function($) {
         $.fn.menumaker = function(options) {  
         var cssmenu = $(this), settings = $.extend({
          format: "dropdown",
          sticky: false
         }, options);
         return this.each(function() {
          $(this).find(".button").on('click', function(){
            $(this).toggleClass('menu-opened');
            var mainmenu = $(this).next('ul');
            if (mainmenu.hasClass('open')) { 
              mainmenu.slideToggle().removeClass('open');
            }
            else {
              mainmenu.slideToggle().addClass('open');
              if (settings.format === "dropdown") {
                mainmenu.find('ul').show();
              }
            }
          });
          cssmenu.find('li ul').parent().addClass('has-sub');
         multiTg = function() {
            cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
            cssmenu.find('.submenu-button').on('click', function() {
              $(this).toggleClass('submenu-opened');
              if ($(this).siblings('ul').hasClass('open')) {
                $(this).siblings('ul').removeClass('open').slideToggle();
              }
              else {
                $(this).siblings('ul').addClass('open').slideToggle();
              }
            });
          };
          if (settings.format === 'multitoggle') multiTg();
          else cssmenu.addClass('dropdown');
          if (settings.sticky === true) cssmenu.css('position', 'fixed');
         resizeFix = function() {
         var mediasize = 1000;
            if ($( window ).width() > mediasize) {
              cssmenu.find('ul').show();
            }
            if ($(window).width() <= mediasize) {
              cssmenu.find('ul').hide().removeClass('open');
            }
          };
          resizeFix();
          return $(window).on('resize', resizeFix);
         });
         };
         })(jQuery);
         
         (function($){
         $(document).ready(function(){
         $("#cssmenu").menumaker({
          format: "multitoggle"
         });
         });
         })(jQuery);
         
      </script>
      <script  >$(document).ready(function() {
         $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
         $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
         });
		  $('.select2').select2({
		  });
		  $("#grid").click(function(){
			  $("#grid").addClass("active");
			  $("#list").removeClass("active");
		  })
		   $("#list").click(function(){
			   $("#grid").removeClass("active");
			  $("#list").addClass("active");
		  })
      </script>
	<script  >
		 $(".filter-heading .icon-list").click(function(){
			  $(".filter-heading .icon-remove").show();
			  $(".filter-heading .icon-list").hide();
			 $(".category-wrapper").addClass("display");
		  })
		   $(".filter-heading .icon-remove").click(function(){
			   $(".filter-heading .icon-list").show();
			  $(".filter-heading .icon-remove").hide();
			   
			 $(".category-wrapper").removeClass("display");
		  })
	</script>
	<script >
		$(document).bind('dragover', function (e) {
			var dropZone = $('.zone'),
				timeout = window.dropZoneTimeout;
			if (!timeout) {
				dropZone.addClass('in');
			} else {
				clearTimeout(timeout);
			}
			var found = false,
				node = e.target;
			do {
				if (node === dropZone[0]) {
					found = true;
					break;
				}

				node = node.parentNode;
			} while (node != null);
			if (found) {
				dropZone.addClass('hover');
			} else {
				dropZone.removeClass('hover');
			}
			window.dropZoneTimeout = setTimeout(function () {
				window.dropZoneTimeout = null;
				dropZone.removeClass('in hover');
			}, 100);
		});   
	</script>   
		<script >
				
				$(document).ready(function() {
        //Horizontal Tab
        $('#parentHorizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });

        // Child Tab
        $('#ChildVerticalTab_1').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true,
            tabidentify: 'ver_1', // The tab groups identifier
            activetab_bg: '#fff', // background color for active tabs in this group
            inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
            active_border_color: '#c1c1c1', // border color for active tabs heads in this group
            active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
        });

        //Vertical Tab
        $('#parentVerticalTab').easyResponsiveTabs({
            type: 'vertical', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            tabidentify: 'hor_1', // The tab groups identifier
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#nested-tabInfo2');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
				
				
				</script>	
	   <script >
	   
	   	$(document).ready(function(){
		  $(".comment-section .content").slice(0, 3).show();
		  $("#loadMore").on("click", function(e){
			e.preventDefault();
			$(".content:hidden").slice(0, 4).slideDown();
			if($(".content:hidden").length == 0) {
			  $("#loadMore").text("No Content").addClass("noContent");
			}
		  });

		})
	   </script> 
	

   </body>
   <?php include_once('footer.php'); ?>
</html>



