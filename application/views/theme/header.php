      <head>
      <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/bootstrap.min.css">
	  <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/plugin.css"> 
      <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/font-awesome.min.css"> 
	  <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/scrollbar.min.css">
	  <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/howclip-font-icon.css">
      <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/style.css">
      <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/responsive.css">
      <link rel="stylesheet" href="<?php echo base_url();?>my-assets/css/owl.carousel.min.css">
       
     </head>
     <div class="ad">
         <p><span><i class="icon-info"></i></span> For this <b>month</b> only, sign up as content creator, upload <strong>3 videos</strong> and earn <strong>100% </strong>revenue on all clips</p>
         <i class="icon-close white-close"></i> 
      </div>
      <header>
         <nav id="cssmenu">
            <div class="side-bar" id="side-bar">
               <ul>
                  <li class="logo"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>my-assets/images/logo.png" alt="" /></a></li>
                  <li class="bar"><a href="javascript:;"><i class="icon-close"></i></a></li>
               </ul>
               <ul class="side-bar-menu">
                  
                  <li><a href="<?php echo base_url('index.php/welcome/recommended')?>"><span><i class="icon-technical-jobs"></i></span> Recommended Clips </a> <span class="caret-right"><i class="icon-arrow-right"></i></span></li>
                   <?php foreach ($features as $videofeature) { 

                     $name= str_replace(" ","_",$videofeature->name); ?>
                  <li>
                    <a href="<?php echo base_url() ?>index.php/welcome/featurevideo/<?php echo $name; ?>"><span><i class="icon-technical-jobs"></i></span> <?php echo $videofeature->name; ?> </a> <span class="caret-right"><i class="icon-arrow-right"></i></span>
                  </li>
                  <?php
                      }
                       ?>
  
                     <?php if ($this->session->userdata('id') != '') { ?>   
                  <li>
                    <a href="<?php echo base_url() ?>index.php/welcome/history/<?php echo $this->session->userdata('id'); ?>"><span><i class="icon-technical-jobs"></i></span> History </a> <span class="caret-right"><i class="icon-arrow-right"></i></span>
                  </li>
                <?php 
              }
               ?>
               <?php if ($this->session->userdata('id') == '') { ?>  
                <li>
                    <a href="javascript:void(0)"><span><i class="icon-technical-jobs"></i></span> History </a> <span class="caret-right"><i class="icon-arrow-right"></i></span>
                  </li>
               <?php 
             }
              ?>
               </ul>
               <div class="form">
                <?php if ($this->session->userdata('id') == "") { ?>
                  <?php
                     if ($this->session->flashdata('regerr')) {
                         ?>
                  <div style="color:#FF0000;text-align:center;" > <?php echo $this->session->flashdata('regerr'); ?></div>
                  <?php } ?>    
                  <div style="color:#FF0000;text-align:center;" id="formerror"></div>
                  
                  <!-- <div style="color:#FF0000;text-align:center;display:none;" id="lnameerror">Enter Last Name</div> -->
                  
                 
                  <h3><span>SIGN</span> UP</h3>
                  <a href="#" id="loginBtn" class="facebook"><span><i class="icon-facebook"></i></span> Sign Up  with Facebook</a>
                  <p class="or">OR</p>
                  <form class="form-inline" onsubmit="return validationform()" name="form" method="post" action="<?php echo base_url() ?>index.php/welcome/register" enctype="multipart/form-data">
                     
                     <div class="form-group">
                        <input type="text" class="form-control" name="fname" id="exampleInputFName" value="<?php echo set_value('fname'); ?>" placeholder="Full Name" >
                     </div>
                    <div style="color:#FF0000;text-align:center;display:none;" id="fnameerror">Enter Full Name</div>
                     <div class="form-group">
                        <input type="text" class="form-control" name="email" id="email_address" placeholder="Email" >
                        
                    <p id="error" style="display:none;color:red;">Wrong email</p>
                     </div>
                    <div style="color:#FF0000;text-align:center;display:none;" id="mailerror">Enter Email Address</div>
                     <div class="form-group">
                        <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                     </div>
                      <div style="color:#FF0000;text-align:center;display:none;" id="passerror">Enter Password</div>
                     <p>By logging in, you agree to our <b><a href="<?php echo base_url() ?>index.php/welcome/terms">Terms of Service</a></b> and <b><a href="<?php echo base_url() ?>index.php/welcome/policy">Privacy Policy</a></b></p>
                    <div id="lengtherr" style="display:none;color:#FF0000;">
                        <p style="color:#FF0000">Your password must be at least 8 characters!</p>
                     </div>
                     <button type="submit" class="btn btn-default theme-btn">Sign Up</button>
                  
                  </form>
                 <?php } ?>
               </div>
            </div>
            <div class="logo-bar">
               <ul>
                  <li class="bar"><a href="javascript:;"><i class="icon-menu"></i></a></li>
                  <li class="logo"><a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>my-assets/images/logo.png" alt="" /></a></li>
               </ul>
            </div>
            <a class="navbar-brand" href="<?php echo base_url('index.php/welcome/open_advanceSearch') ?>"><span class="search-img"><i class="icon-folder-search-svgrepo-com"></i></span><span>Advanced Search</span></a>
            <div id="head-mobile"></div>
            <div class="button"></div>
            <ul class="main-menu-ul">
                <?php
                 $i = 1;
                  foreach ($categories as $menu) {
                            ?>
               <li class="nav-item active">
                  <a class="nav-link" href="<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>">
                     <figure class="menu-img"><i class="icon-manual-work"></i></figure>
                     <span><?php echo $menu['title']; ?></span>
                  </a>
                 <?php
                 if ($menu['submenu'] != '') {
                  ?>
                  <ul>
                      <?php
                              
                                  foreach ($menu['submenu'] as $sub) {
                                      ?>
                     <li>
                        <a href="<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>/<?php echo $sub['id']; ?>"><?php echo $sub['title']; ?></a>
                        <ul>
                            <?php
                                    if ($sub['submenu'] != '') {
                                        foreach ($sub['submenu'] as $sub2) {
                                            ?>
                           <li><a href="<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>/<?php echo $sub['id']; ?>/<?php echo $sub2['id']; ?>"><?php echo $sub2['title']; ?></a></li>
                           <?php
                                       }
                                       }
                                       ?>
                        </ul>
                     </li>
                     <?php
                              }
                              
                              ?>
					 
                  </ul>
                  <?php
               }
               ?>
               </li>
               <?php
                if ($i++ == 4) break;
               }
            ?>
            <li class="nav-item active">
                  <a class="nav-link" href="#">
                     <figure class="menu-img"><i class="icon-presonal-care"></i></figure>
                     <span>Others</span>
                  </a>
              <ul>
               <?php
                 $i = 1;
                  foreach (array_slice($categories, 4) as $menu) {
                         
                            ?>
                     <li>
                        <a href="<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>"><i class="icon-presonal-care sub-menu-cat-icn"></i><?php echo $menu['title']; ?></a>
                        <ul>
                           <?php
                              if ($menu['submenu'] != '') {
                                  foreach ($menu['submenu'] as $sub) {
                                      ?>
                           <li><a href="<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>/<?php echo $sub['id']; ?>"><?php echo $sub['title']; ?></a>
                        <ul>
                           <?php
                                    if ($sub['submenu'] != '') {
                                        foreach ($sub['submenu'] as $sub2) {
                                            ?>
                              <li><a href="<?php echo base_url() ?>index.php/welcome/getadvanceresult2/<?php echo $menu['id']; ?>/<?php echo $sub['id']; ?>/<?php echo $sub2['id']; ?>"><?php echo $sub2['title']; ?></a></li>
                          <?php
                                       }
                                       }
                                       ?>
                         </ul>
                     </li>
                            <?php
                                       }
                                       }
                                       ?>
                        </ul>
                     </li>
                     <?php
                  }
                  ?>
                
               </ul>
              </li>
               
         <li class="nav-item active rit-nav-s">
				 <div class="rit-nav-sec">
				  
        <?php if ($this->session->userdata('id') == "") { ?>

         <a href="#javascript:;" data-toggle="modal" data-target="#Modal-2" class="upload"><span><i class="icon-upload"></i></span>Upload</a>

			   <a href="#javascript:;"  data-toggle="modal" data-target="#Modal-2" class="gray-theme-btn">Sign In</a>
				 <a href="#javascript:;" data-toggle="modal" data-target="#Modal-3" class="theme-btn">Sign Up</a>
         
        <?php 
        }
        ?>

        <?php if ($this->session->userdata('id') != "") { 
           // echo $this->session->userdata('name');
           // die;
          ?>   
                     <?php
                        foreach ($userDetail as $user) {

                            $username = $user->username;
                            $userimg = $user->userLogo;
                            if ($user->userLogo == '') {
                                $userimg = "no-user.png";
                            }
                        }
                        ?>
                         </div>
                      </li>


        <li class="dropdown user-profile">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <div class="user"><img width="60" height="55" src="<?php echo base_url();?>uploads/<?php echo $userimg; ?>" alt=" " /></div> <span class="user-name"><?php echo ucwords($this->session->userdata('name')); ?></span></a>
          <ul class="dropdown-menu">
          <li><a href="<?php echo base_url() ?>index.php/home/dash"><span><i class="icon-my-profile"></i></span> My Profile </a></li>
          <li><a href="<?php echo base_url()?>index.php/home/showplaylist/<?php echo $this->session->userdata('id')?>"><span><i class="icon-playlist"></i></span> Playlist </a></li>
          <li><a href="<?php echo base_url()?>index.php/home/videoDetail/<?php echo $this->session->userdata('id')?>"><span><i class="icon-video-camera"></i></span>Videos</a></li>
          <li><a href="<?php echo base_url()?>index.php/home/mychannel/<?php echo $this->session->userdata('id');?>"><span><i class="icon-my-channel"></i></span>My Channel</a></li>
          <li><a href="<?php echo base_url();?>index.php/home/myorders/<?php echo $this->session->userdata('id')?>"><span><i class="icon-my-orders"></i></span>My Orders </a></li>
          <li><a href="<?php echo base_url() ?>index.php/home/userupdate/<?php echo $this->session->userdata('id') ?>"><span><i class="icon-settings"></i></span>Setting</a></li>
          <li><a href="<?php echo base_url() ?>index.php/home/logout" class="log-out"><span><i class="icon-logout"></i></span>Logout </a></li>  
          </ul>
        </li>

        <li class="nav-item active rit-nav-s">  
         <div class="rit-nav-sec">
          <a href="<?php echo base_url() ?>index.php/home/uploadpage" class="upload"><span><i class="icon-upload"></i></span>Upload</a> 
           </div>
        </li>
        
         <?php } ?>
			    
			   
            </ul>
         </nav>
      </header>

       <script src = "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script>
      
        function validationform() {
            
             var fname = document.form.fname.value;
             //var lname = document.form.lname.value;
             var femail = document.form.email.value;
             
             var fpassword = document.form.password.value;
             
             var re = /([A-Z0-9a-z_-][^@])+?@[^$#<>?]+?\.[\w]{1,2}/.test(femail);
         
             if (fname == "")
             {
         
                 document.getElementById('fnameerror').style.display = 'block';
                 setTimeout(function () {
                     $('#fnameerror').fadeOut('fast');
                 }, 4000);
                 return false;
             }
             
             if (femail == "")
             {
                 document.getElementById('mailerror').style.display = 'block';
                 setTimeout(function () {
                     $('#mailerror').fadeOut('fast');
                 }, 4000);
                 return false;
             }
             if (fpassword == "")
             {
         
                 document.getElementById('passerror').style.display = 'block';
                 setTimeout(function () {
                     $('#passerror').fadeOut('fast');
                 }, 4000);
                 return false;
         
             }
             if (fpassword.length < 8)
             {
                 $("#lengtherr").show();
                 setTimeout(function () {
                     $('#lengtherr').fadeOut('fast');
                 }, 4000);
                 return false;
             }
             if (!re)
             {
                 document.getElementById("formerror").innerHTML = "Enter valid Email";
                 setTimeout(function () {
                     $('#formerror').fadeOut('fast');
                 }, 1000);
         
                 return false;
             }
         
         }
  
      </script>
     <script>
            $(document).ready(function () {
                
                var showChar = 100;
                var ellipsestext = "...";
                var moretext = "more";
                var lesstext = "less";
                $('.more').each(function () {
                    var html='';
                    var content = $(this).html();
                    console.log(content.length);
                    if (content.length > showChar) {

                        var c = content.substr(0, showChar);
                        var h = content.substr(showChar - 1, content.length - showChar);

                         html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
                        console.log(html);
                        $(this).html(html);
                    }

                });

                $(".morelink").click(function () {
                    if ($(this).hasClass("less")) {
                        $(this).removeClass("less");
                        $(this).html(moretext);
                    } else {
                        $(this).addClass("less");
                        $(this).html(lesstext);
                    }
                    $(this).parent().prev().toggle();
                    $(this).prev().toggle();
                    return false;
                });
            });
        </script>
        