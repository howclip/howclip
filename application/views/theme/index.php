<!doctype html>
<html lang="en">
   <head>
      <title>Howclip</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
      
   </head>
   <body>
      <?php include_once('header.php'); ?>
      <section class="search-section" style="background-image: url(<?php echo base_url();?>my-assets/images/banner-bg.png);">
         <div class="container">
            <div class="content">
               <h2>Upload your video <b>CLIP</b> and earn</h2>
               <h1> Lorem Ipsum, giving information</h1>
               <div class="search-form">
                  <form  method="post" id="my_form" action="<?php echo base_url() ?>index.php/welcome/getsearchdata">
                     <div class="input-group">
                        <div class="search-panel">
                            <select class="select2 form-conrol">
                       <option value="Category"> Category</option>   
                       <option value="co"> Comedy </option>
                       <option value="da"> Dance </option>
                       <option value="si"> Singing </option>
                    </select>
                        </div>
                        <input type="text" class="form-control" id="input-31" autocomplete="off" onKeyUp="ajaxSearch();" name="search_data" placeholder="Search here...">
                        <div id="suggestions" style="background-color:#FFF;position:absolute;top:40px;left:200px;width:55%;z-index:10000;">
                        <div id="autoSuggestionsList"></div>
                     </div>
                        <span class="input-group-btn search-btn">
                        <button class="btn btn-default" type="button" id="chkval"><span><i class="icon-search"></i></span></button>
                        </span> 
                     </div>
                  </form>
               </div>
            </div>
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </section>
      <div class="container">
         <div class="wrapper">
            <section class="trending-section">
               <div class="heading">
                  <div class="heading-outer">
                     <h3>FEATURED/TRENDING<span> CLIP</span></h3>
                  </div>
                  <a href="fillter.html" class="view-all">View All <span><img src="<?php echo base_url();?>my-assets/images/red-arrow.png" alt="" /></span></a> 
               </div>
               <div class="all-videos">
                  <div class="row">
                     <?php
                     // echo "<pre>";
                     // print_r($playlist);
                     // die;
                      foreach ($playlist as $play) { //echo '<pre>'; print_r($play); ?>
                     <?php
                        if (getimagesize(base_url() . 'uploads/images/' . $play['video'][0]['video_img'])) {
                            
                             $link = base_url() . "uploads/images/" . $play['video'][0]['video_img'];
                        } else {
                            $link = base_url() . "uploads/images/download.jpg";
                        }
                        ?>
                     <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo $link; ?>" alt="No Image Available" /> 
                              <span class="duration"><?php
                                 $duration = explode(':', $play['duration']);
                                 if ($duration[0] == '0') {
                                     echo $videolong = $duration[1] . ":" . $duration[2];
                                 } else {
                                     echo $videolong = $duration[0] . ":" . $duration[1] . ":" . $duration[2];
                                 }
                                 ?></span>
                              <?php if ($play['price'] > 0) { 

                                 ?>
                              <div class="payble">
                                 <a href="<?php echo base_url() ?>index.php/welcome/playvideo/<?php echo $play['play'] . '/' . $play['user']['id']; ?>">
                                    <h4><span>$</span><?php echo $play['price'];?></h4>
                                    <span>View</span><i class="icon-play"></i><span>Clip</span>
                                 </a>
                              </div>
                              <?php }else{

                                 ?>

                              <a href="<?php echo base_url() ?>index.php/welcome/playvideo/<?php echo $play['play'] . '/' . $play['user']['id']; ?>"><i class="icon-play play-btn"></i></a>
                              <?php
                           }

                                 $vname = $play['play'];
                                 $length = strlen($play['play']);
                                 if ($length < 10) {
                                     $v = $vname;
                                 } else {
                                     $v = substr($vname, 0, 10);
                                     $v = $v . "...";
                                 }
                                 ?>
                          </div>
                           <h6 class="video-title"><a href="<?php echo base_url() ?>index.php/welcome/playvideo/<?php echo $play['play']; ?>"><?php echo ucwords($v); ?></a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By <?=$play['user']['Firstname'];?></span></a></li>
                                 <li class="date"><?php
                                    $date = explode('-', $play['date']);
                                    if ($date[1] == '01') {
                                        $month = 'Jan';
                                    } if ($date[1] == '02') {
                                        $month = 'Feb';
                                    } if ($date[1] == '03') {
                                        $month = 'Mar';
                                    } if ($date[1] == '04') {
                                        $month = 'Apr';
                                    }if ($date[1] == '05') {
                                        $month = 'May';
                                    } if ($date[1] == '06') {
                                        $month = 'Jun';
                                    } if ($date[1] == '07') {
                                        $month = 'Jul';
                                    } if ($date[1] == '08') {
                                        $month = 'Aug';
                                    } if ($date[1] == '09') {
                                        $month = 'Sep';
                                    } if ($date[1] == '10') {
                                        $month = 'Oct';
                                    } if ($date[1] == '11') {
                                        $month = 'Nov';
                                    } if ($date[1] == '12') {
                                        $month = 'Dec';
                                    }
                                    
                                    echo $date[2] . " " . $month . ", " . $date[0];
                                    ;
                                    ?></li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span><?=$play['category']['video_category']?></span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span><?php if ($play['views'] == "") {
                                     echo "0";
                                 } else {
                                     echo ucwords($play['views']);
                                 } ?>Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <?php } ?>
                     
                  </div>
               </div>
            </section>
            <section class="how-work-section">
               <div class="white-heading">
                  <div class="heading-outer">
                     <h3>HOW <span> IT WORK?</span></h3>
                  </div>
               </div>
               <div class="pop-vdo">
                  <div class="pop-vdo-img">
                     <img src="<?php echo base_url();?>my-assets/images/Group123.png" alt="" />
                     <a href="javascript:;" data-toggle="modal" data-target="#Modal-1"><i class="icon-play play-btn-lg"></i></a>
                  </div>
               </div>
            </section>
            <section class="popular-section">
               <div class="heading">
                  <div class="heading-outer">
                     <h3>POPULAR <span> CATEGORY</span></h3>
                  </div>
                  <a href="fillter.html" class="view-all">View All <span><img src="<?php echo base_url();?>my-assets/images/red-arrow.png" alt="" /></span></a> 
               </div>
               <div class="popular-vdo">
                  <div class="row">
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <a href="#javascript:;">
                           <div class="personal-care">
                              <img src="<?php echo base_url();?>my-assets/images/Image.png" alt="" />
                              <div class="content">
                                 <h4>Trandes</h4>
                                 <p>Excepteur sint occaecat cupidatat<br> non proident, sunt in culpa qui officia</p>
                              </div>
                           </div>
                        </a>
                     </div>
                     <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="row">
                           <div class="col-lg-12 col-md-12 col-sm-12">
                              <a href="#javascript:;">
                                 <div class="interpersonal-care">
                                    <img src="<?php echo base_url();?>my-assets/images/Image2.png" alt="" />
                                    <div class="content">
                                       <h4>Hobbies </h4>
                                       <p>Excepteur sint occaecat cupidatat<br> non proident, sunt in culpa qui officia</p>
                                    </div>
                                 </div>
                              </a>
                           </div>
                           <div class="col-lg-12 col-md-12 col-sm-12">
                              <a href="#javascript:;">
                                 <div class="sports">
                                    <img src="<?php echo base_url();?>my-assets/images/Image3.png" alt="" />
                                    <div class="content">
                                       <h4>Personal</h4>
                                       <p>Excepteur sint occaecat cupidatat<br> non proident, sunt in culpa qui officia</p>
                                    </div>
                                 </div>
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="recommand-section">
               <div class="heading">
                  <div class="heading-outer">
                     <h3>RECOMMENDED <span> CLIP</span></h3>
                  </div>
                  <a href="fillter.html" class="view-all">View All <span><img src="<?php echo base_url();?>my-assets/images/red-arrow.png" alt="" /></span></a> 
               </div>
               <div class="all-videos">
                  <div class="row">
                     <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img">
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <div class="payble">
                                 <a href="javascript:;">
                                    <h4><span>$</span>2.00</h4>
                                    <span>View</span><i class="icon-play"></i><span>Clip</span>
                                 </a>
                              </div>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Lalit Sahu</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Lalit Sahu</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Lalit Sahu</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="video-section">
                           <div class="video-img"> 
                              <img src="<?php echo base_url();?>my-assets/images/video-bg.png" alt="" /> 
                              <span class="duration">11:15</span>
                              <a href="javascript:;"><i class="icon-play play-btn"></i></a>
                           </div>
                           <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
                           <div class="about-video">
                              <ul>
                                 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Lalit Sahu</span></a></li>
                                 <li class="date">9 Apr, 2019</li>
                                 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
                              </ul>
                           </div>
                           <div class="video-btn">
                              <ul>
                                 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
                                 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>
            <section class="content-section ftr-about-sec">
               <div class="row">
                  <div class="col-lg-7 col-md-7 col-sm-12">
                    <div class="content">
   <div class="in-about-sec">
                        <h2>Excepteur sint occaecat <br>cupidatat non proident.</h2>
                        <p class="para-section">
                           Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor<br> incididunt ut ero labore et dolore magna aliqua. Ut enim ad minim veniam, quis<br> nostrud exercitation ullamco.
                        </p>
                        <a href="#javascript:;" class="bdr-theme-btn">MORE</a>
                        <a href="#javascript:;" class="theme-btn">LET’S TRY</a>
      </div>
                     </div>
                  </div>
                  <div class="col-lg-5 col-md-5 col-sm-12">
                     <div class="content-image">
                        <img src="<?php echo base_url();?>my-assets/images/color_home.png" alt="" />
                     </div>
                  </div>
               </div>
            </section>
         </div>
      </div>
      <section class="passage-section">
         <div class="container">
            <div class="content">
               <h3>There are many variations of passages</h3>
               <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
               <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>
            </div>
         </div>
      </section>
      <?php include_once('footer.php'); ?>
   </body>
</html>
<script type="text/javascript">
         function ajaxSearch()
         {
         
             var input_data = $('#input-31').val();
            
             if (input_data.length === 0)
             {
                 $('#suggestions').hide();
             } else
             {
         
                 var post_data = {
                     'search_data': input_data,
                     '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                 };
         
                 $.ajax({
                     type: "POST",
                     url: "<?php echo base_url() ?>index.php/welcome/searchAll",
                     data: post_data,
                     success: function (data) {
                         //console.log(data);
                         if (data.length > 0) {
                             $('#suggestions').show();
                             $('#autoSuggestionsList').addClass('auto_list');
                             $('#autoSuggestionsList').html(data);
                         }
                     }
                 });
         
         
             }
         
         }
      </script>
 <script>
         $(document).on("click", function (e) {
             if (!$("#suggestions").is(e.target)) {
                 $("#suggestions").hide();
             }
         });
          $("#check").on("click", function (e) {
            $('#emailAdd').val('');
            $('#pass').val('');
         });
          $('#chkval').click(function (e) {
             e.preventDefault();
             var input = document.getElementById("input-31").value;
             
             if (input == '') {
                 alert('Please Type Text For Search!');
                 retutn true;
                  e.preventDefault();
             }
             else{
                 document.getElementById('my_form').submit();
             }
         });
      </script>
