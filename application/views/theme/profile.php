<!doctype html>
<html lang="en">
   <head>
      <title>HowClip | Profile</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta name="keywords" content="HTML,CSS,JavaScript">
      
   </head>
   <body>
     <?php include_once('header.php') ?>

      <div class="mychannel-banner" style="background-image: url(<?php echo base_url();?>my-assets/images/profile-banner.png);">
         <div class="container">
           
         </div>
         <div class="social">
            <ul>
               <li class="google-plus"><a href="#javascript:;" ><i class="icon-google-plus"></i></a></li>
               <li class="facebook"><a href="#javascript:;" ><i class="icon-facebook"></i></a></li>
               <li class="twitter"><a href="#javascript:;" ><i class="icon-Twitter"></i></a></li>
            </ul>
         </div>
      </div>
      <div class="container">
         <div class="wrapper">
            <div class="uploading-vdo-section trending-section my-channel-des profile-page">
				<div class="description">
						<div class="row">
							<div class="col-lg-9 col-md-9 col-sm-12">
								<div class="vdo-description">
									<div class="vdo-title">
										<div class="row">
											<div class="col-lg-3 col-md-3 col-sm-3">
                                               <?php
                                               $id = $this->session->userdata('id');
                                             
						                        foreach ($userDetail as $user) {
						                        	
						                            $username = $user->username;
						                            $userimg = $user->userLogo;
						                            if ($user->userLogo == '') {
						                                $userimg = "no-user.png";
						                            }
						                        }
                       
                                               ?>
												<div class="chennel-profile">
													<img src="<?php echo base_url();?>uploads/<?php echo $userimg; ?>" alt="" />
												</div>
												<div class="joined">
													<h5>Joined <?php
													$input = $user->date;
													$time = strtotime($input);
													$output = date('F d, Y',$time);
													echo $output;
													//echo date("F j, Y",);
													?></h5>
													<div class="social-info">
														<div class="login-with">
															<ul class="social-share">
																<li class="google"><a href="#" ><i class="icon icon-google-plus"></i></a></li>
																<li class="envelope"><a href="#" ><i class="icon icon-social-mail"></i></a></li>
																<li class="facebook"><a href="#" ><i class="icon icon-facebook"></i></a></li>
																<li class="twitter"><a href="#" ><i class="icon icon-Twitter"></i></a></li>
															</ul>
														</div>
													</div>		
												</div>
											</div>
											<div class="col-lg-9 col-md-9 col-sm-9">
												<h3><?php 

												ucwords($user->Firstname)?></h3>
												<ul>
													<li><a href="#"><span class="invite"><i class="icon-subscribe"></i>48582 </span><strong>Subscribe</strong></a></li>
													<li><a href="#"><span class="invite"><i class="icon-view"></i></span><strong>4.4M Views</strong></a></li>
													<li><a href="#"><span class="invite"><i class="icon-video-camera"></i></span><strong>26 Chennels</strong></a></li>
													<li><a href="#"><span><i class="icon-playlist"></i></span><strong>2500 Total Post Videos</strong> </a></li>
													<li><a href="#"><span class="invite"><i class="icon-add-widget"></i> </span><strong>Add Widget</strong></a></li>	
												</ul>
												<p>Solve challenges Action Against Hunger citizenry Martin Luther King Jr. Combat malaria, mobilize lasting change billionaire philanthropy revita research. Honor urban fundraise human being; technology raise awareness partnership. Political global growth cross-agency coordnation the  global financial system. Frontline leverage, social entrepreneurship non-partisan meaningful <a href="#" class="footer-more"> <em>Learn More</em></a></p>
											</div>
										</div>	
									</div>
									
									<div class="recommanded">
										<section class="trending-filter-section">
										   <div class="grid-item-section">
											  <div class="well well-1 well-sm">
													<a href="" class="refresh"><span><i class="icon-refresh"></i></span></a>
												 <span class="result">Showing all 350 results</span>	
												  <div class="right-side-grid">

													  <label>Fillter By :  </label>
													  <span class="select-cat">
														  <select class="select2" >
															<optgroup label=" Today Views">
															  <option value="" selected> Today Views </option>	
															  <option value="co"> Today Views </option>
															  <option value="da"> Today Views </option>
															  <option value="si"> Today Views </option>
															</optgroup>
														  </select>
													  </span>
												  </div>
												  <div class="right-side-grid">

													  <label>Short By : </label>
													  <span class="select-cat">
														  <select class="select2" >
															<optgroup label=" All Videos">
															  <option value="" selected> All Videos </option>	
															  <option value="co"> All Videos </option>
															  <option value="da"> All Videos </option>
															  <option value="si"> All Videos </option>
															</optgroup>
														  </select>
													  </span>
												  </div>
												  
											  </div>
											  <div id="products" class="row list-group all-videos">
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div> 
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="col-lg-4 col-md-4 col-sm-6">
													<div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>
												 </div>
												 <div class="col-lg-4 col-md-4 col-sm-6">
													 <div class="video-section">
													   <div class="video-img"> 
														  <img src="<?php echo base_url();?>my-assets/images/video-bg.png" class="group list-group-image" alt="" /> 
														  <span class="duration">11:15</span>
														  <a href="javascript:;"><i class="icon-play play-btn"></i></a>
													   </div>
													   <h6 class="video-title"><a href="#javascript:;">Duis aute irure dolor in adipsicing elit...</a></h6>
													   <div class="about-video">
														  <ul>
															 <li class="user"><a href="#javascript:;"><i class="icon-Profile"></i><span>By Anil Kumar</span></a></li>
															 <li class="date">9 Apr, 2019</li>
															 <li class="review"><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star-empty"></i></li>
														  </ul>
													   </div>
													   <div class="video-btn">
														  <ul>
															 <li class="care-btn"><a href="#javascript:;"><i class="icon-presonal-care"></i><span>Personal Care</span></a></li>
															 <li class="view-btn"><a href="#javascript:;"><i class="icon-view"></i><span>36500 Views</span></a></li>
														  </ul>
													   </div>
													</div>

												 </div>
											  </div>
												<div class="pagination">
													<a href="#" class="prev-anchor"><i class="icon-angle-left"></i> </a>
													  <a href="#">1</a><a href="#" class="active">2</a><a href="#">3</a><a href="#">4</a><a href="#">5</a>
													<a href="#" class="next-anchor"><i class="icon-angle-right"></i> </a>
												</div>
											   </div>

										</section>
									 </div>

								</div>
								</div>
							<div class="col-lg-3 col-md-3 col-sm-12">
								<div class="vdo-upload-search">				
									<div class="selectFile">       
									  <label for="file">Upload New Video Clip</label>                   
									  <input type="file" name="files[]" id="file">
									</div>
									<div class="all-videos manage-vdo">
										<div class="video-section">
										   <div class="video-btn">
											  <ul>
												 <li class="care-btn"><a href="#javascript:;"><i class="icon-playlist"></i><span>Manage Videos</span></a></li>
												 <li class="view-btn"><a href="#javascript:;"><i class="icon-analytics"></i><span>Analytics</span></a></li>
											  </ul>
										   </div>
										</div>
									</div>	
								</div>
								<div class="other-topic">
									<ul>
										<li class="mini-heading"><h4>HISTORY - <span> How long been doing it?</span></h4></li>
										<li><span>Solve challenges Action Against Hunger zenry Martin Luther King Jr. Combat malaria revita research.</span></li>
										
										<li class="mini-heading"><h4>BIO  - <span> How you first get into it?</span></h4></li>
										<li><span>Combat malaria, mobilize lasting change billionaire philanthropy revita research.</span></li>
										
										<li class="mini-heading"><h4>STRATEGY  - <span> How you do differently?</span></h4></li>
										<li><span>Fundraise human being; technology raiseess partnership. Political global growth cross-agency  global financial system.</span></li>
										
										<li class="mini-heading"><h4>MOTIVATION  -  <span> Why do you it?</span></h4></li>
										<li><span>raise awareness partnership. Political global growth cross-agency coordnation the  global financial system.</span></li>
										
										<li class="mini-heading"><h4>PHILOSOPHY  -  <span> Why others should do it?</span></h4></li>
										<li><span>global financial system. Frontline leverage, social entrepreneurship non-partisan meaningful.</span></li>
									</ul>
								</div>
								<div class="related-vdo">
									<div class="other-video-clip">
										<div class="any-videos">
											<div class="auto-mode">
												<span class="up-next">Related Channels</span>

											</div>
											
											<div class="up-nxt-vdo">
												<div class="row">
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
														<a href="#">
															<div class="subscribe-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />											
															</div>
														</a>
													</div>
													<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
														<div class="content">
															<p class="subscribe-title">Channels name here</p>
															<button class="bdr-theme-btn">Subscribe</button>
														</div>
													</div>
												</div>
											</div>
											<div class="up-nxt-vdo">
												<div class="row">
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
														<a href="#">
															<div class="subscribe-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />											
															</div>
														</a>
													</div>
													<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
														<div class="content">
															<p class="subscribe-title">Channels name here</p>
															<button class="bdr-theme-btn">Subscribe</button>
														</div>
													</div>
												</div>
											</div>											
											<div class="up-nxt-vdo">
												<div class="row">
													<div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
														<a href="#">
															<div class="subscribe-img">
																<img src="<?php echo base_url();?>my-assets/images/Fashion-Video-Blogger.png" alt="" />											
															</div>
														</a>
													</div>
													<div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
														<div class="content">
															<p class="subscribe-title">Channels name here</p>
															<button class="bdr-theme-btn">Subscribe</button>
														</div>
													</div>
												</div>
											</div>													
										</div>
									</div>
								</div>
							</div>
						</div>
				</div>
			 </div>	
		  </div>
      </div>
      <div class="passage-section blank-bule-bg">
         <div class="container">
            <div class="recent-vdo-section">
               
               <div class="all-videos">
                  
               </div>
				
            </div>
         </div>
      </div>
      
	
      <script>
         $(".forget-ps a").click(function(){
         	$(".form-sign-in").toggleClass("show");
         	$(".modal-backdrop").removeClass("show");
         })
      </script>
      <script>
         $(document).ready(function() {
           var owl = $("#owl-demo1");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
		   $(document).ready(function() {
           var owl = $("#owl-demo2");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
		   $(document).ready(function() {
           var owl = $("#owl-demo3");
           owl.owlCarousel({
         
         	nav: true,
         	margin: 20,
         	responsive: {
         	  0: {
         		items: 1
         	  },
         	  600: {
         		items: 2
         	  },
         	  960: {
         		items: 2
         	  },
         	  1200: {
         		items: 3
         	  }
         	}
           });
         
         });
      </script> 
      
      
      <script  >$(document).ready(function() {
         $('#list').click(function(event){event.preventDefault();$('#products .item').addClass('list-group-item');});
         $('#grid').click(function(event){event.preventDefault();$('#products .item').removeClass('list-group-item');$('#products .item').addClass('grid-group-item');});
         });
		  $('.select2').select2({
		  });
		  $("#grid").click(function(){
			  $("#grid").addClass("active");
			  $("#list").removeClass("active");
		  })
		   $("#list").click(function(){
			   $("#grid").removeClass("active");
			  $("#list").addClass("active");
		  })
      </script>
	<script  >
		 $(".filter-heading .icon-list").click(function(){
			  $(".filter-heading .icon-remove").show();
			  $(".filter-heading .icon-list").hide();
			 $(".category-wrapper").addClass("display");
		  })
		   $(".filter-heading .icon-remove").click(function(){
			   $(".filter-heading .icon-list").show();
			  $(".filter-heading .icon-remove").hide();
			   
			 $(".category-wrapper").removeClass("display");
		  })
	</script>
	<script >
		$(document).bind('dragover', function (e) {
			var dropZone = $('.zone'),
				timeout = window.dropZoneTimeout;
			if (!timeout) {
				dropZone.addClass('in');
			} else {
				clearTimeout(timeout);
			}
			var found = false,
				node = e.target;
			do {
				if (node === dropZone[0]) {
					found = true;
					break;
				}

				node = node.parentNode;
			} while (node != null);
			if (found) {
				dropZone.addClass('hover');
			} else {
				dropZone.removeClass('hover');
			}
			window.dropZoneTimeout = setTimeout(function () {
				window.dropZoneTimeout = null;
				dropZone.removeClass('in hover');
			}, 100);
		});   
	</script>   	
	   <script >
	   
	   	$(document).ready(function(){
		  $(".comment-section .content").slice(0, 3).show();
		  $("#loadMore").on("click", function(e){
			e.preventDefault();
			$(".content:hidden").slice(0, 4).slideDown();
			if($(".content:hidden").length == 0) {
			  $("#loadMore").text("No Content").addClass("noContent");
			}
		  });

		})
	   </script> 
	
<?php include_once('footer.php') ?>
   </body>
</html>


