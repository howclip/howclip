<!DOCTYPE HTML>
<html>
<head>
<title>Pupilclip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet">
 <link href="<?php echo base_url();?>assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet">

<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<style>
.imgsize

{
height:300px;
}
</style>
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>

<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>

<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">


<!--//Metis Menu -->


 <script type="text/javascript">
        $(document).ready(function() {
            $(".dropdown1 img.flag").addClass("flagvisibility");

            $(".dropdown1 dt a").click(function() {
                $(".dropdown1 dd > ul").toggle();
            });
                        
            $(".dropdown1 dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown1 dt a").html(text);
                $(".dropdown1 dd > ul").hide();
                $("#result").html("Selected value is: " + getSelectedValue("sample"));
             //creatpop1(getSelectedValue("sample"));
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a").html();
            }

            $(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown1"))
                    $(".dropdown1 dd > ul").hide();
            });


            $("#flagSwitcher").click(function() {
                $(".dropdown1 img.flag").toggleClass("flagvisibility");
            });
        });
    </script>


<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation-->
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start-->
   <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->
      
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
  <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    <i class="fa fa-bars"></i>
  </button>
  <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
    <li><a href="#">Action</a></li>
    <li><a href="#">Another action</a></li>
    <li><a href="#">Something else here</a></li>
    <li role="separator" class="divider"></li>
    <li><a href="#">Separated link</a></li>
  </ul>
</div>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"  > <a href="index.html">
        <!--<h1>Pupilclip</h1>
        <span>Website</span> </a>--> 
        <img src="<?php echo base_url();?>assets/images/logo.png" width="90" class="img-responsive">
       </a> </div>
      <!--//logo--> 
      <!--search-box-->
     <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>Uploading Files</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
           </li>
          <li class="dropdown head-dpdn"> <a href="login.html"> <img src="<?php echo base_url();?>assets/images/signinbut.png" class="img-responsive but"></a> </li>
          <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url();?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
     
      <div class="profile_details">   
       
  </div>
  </div> 
  </div>
  <div class="uploadwrap">
  <div class="container">
  <div class="row">
  <div class="col-md-12">
  <div class="blankpage">
  <div class="row">
  <div class="col-md-3">
  <img src="<?php echo base_url();?>assets/images/summer-wallpaper-hd-2.jpg" class="img-responsive">
  <div class="blankhead">Upload Status</div>
  <p style="font-size:11px; color:#999;">vflgbfb</p>
  <p style="font-size:11px; color:#999;">vflgbfbfbkf flkjgblfbm <a href="#">dgv@kjf.com</a></p>
  <div class="blankhead">Upload Status</div>
  <div class="terms"><p>*</p></div>
  <div class="hint" style="width:70%;">Your Videos are still uploading.Please keep this page open until they are done.</div>
  </div>
  <div class="col-md-9">
  <div class="row">
  <div class="col-md-10">
  <div id="myProgress">
  <div id="myBar"></div>
  </div>
  </div>
  <div class="col-md-2">
  <a class="btn publish2" href="#" role="button">Link</a>
  </div>
  </div>
  <div class="row">
  <div class="col-md-12">
 <img src="<?php echo base_url();?>assets/images/the_avengers___banner_by_stormy94-d4uo6b2.jpg" class="img-responsive imgsize">
 <form style="margin-top:20px;">
 <div class="col-md-5" style="padding: 0 5px;">
 <div class="dropdown" style="display:inline-block;width:30%; margin-top:5px; margin-right:9px;">
 <button class="btn btn-secondary cliping dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
 Select Clip <span class="caret"></span>
  </button>
 <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
 <li><a href="#">Clip #1</a></li>
 <li><a href="#">Clip #2</a></li>
 <li><a href="#">Clip #3</a></li>
 <li><a href="#">Clip #4</a></li>
 <li><a href="#">Clip #5</a></li>
 </div>
 </div>
 <input type="email" class="form-control pageinfo" id="exampleInputEmail1" placeholder="Email" style="display:inline-block;width:63%;margin-top:5px;">
 <textarea class="form-control" rows="1" style="height:45px !important;"></textarea>
	</div>
	<div class="col-md-3"style="padding: 0 5px;" >
	<div class="form-group">
	<label for="exampleInputFile" class="labeltxt">Define Clip Time</label>
	<div style="display:inline-block;">
    <label class="form-control pageinfo" id="exampleInputEmail1"  style="display:inline-block; width:30%; line-height:1;">Start</label>
    <input type="number" class="form-control pageinfo" id="exampleInputEmail1" placeholder="000:00:00" style="display:inline-block;width:60%;">
	</div>
	<div style="display:inline-block;">
    <label class="form-control pageinfo" id="exampleInputEmail1"  style="display:inline-block; width:30%; line-height:1;">End</label>
    <input type="number" class="form-control pageinfo" id="exampleInputEmail1" placeholder="000:00:00" style="display:inline-block;width:60%;">
	</div>
	</div>
	</div>
	<!--<div class="col-md-3" style="padding: 0 5px;">
	<div class="form-group">
	<label for="exampleInputFile" class="labeltxt">Define Categories</label>
	<div class="uploader">
	<input type="text" name="search" placeholder="Search..">
	</div>
	</div>
	</div>-->
	<div class="col-md-4" style="padding:0 5px;">
	<div class="form-group" style="width:65%;display:inline-block; margin-bottom:5px;">
<div style="vertical-align:middle;margin-top:20px;">
<dl id="sample" class="dropdown1">
        <dt><a href="javascript:void()"><span>Monetize <span style="display:inline-block; background-color:#990000; padding:0px 10px; float:right;"><i class="fa fa-caret-down" aria-hidden="true" style="color:#FFF;"></i></span></span> </a></dt>
        <dd>
       <ul  >
     <li><a href="#" class="test" tabindex="-1"><span >Clip#1 </span><i class="fa fa-check-circle" aria-hidden="true"></i></a></li>             
     <li ><a tabindex="-1" href="#"><span>Clip#2 </span> <i class="fa fa-check-circle" aria-hidden="true"></i></a>
	 <ul  class="displayno" >
	 <li ><a href="#"><span>Clip#3  </span><i class="fa fa-check-circle" aria-hidden="true"></i></a></li>
	 <li ><a href="#"><span >Clip#4 </span> <i class="fa fa-check-circle" aria-hidden="true"></i></a></li>
	 </ul>
	 </li>
     <li><a tabindex="-1" href="#"><span>Clip#5  </span><i class="fa fa-check-circle" aria-hidden="true"></i></a>
	 <ul  class="displayno" >
	 <li ><a href="#"><span><span style="display:block;font-size:11px;"> Cost </span>
	 $  <input type="email" class="form-control pageinfo" id="exampleInputEmail1" placeholder="8000" style="display:inline-block; width:45%;"><button type="submit" class="btn accept" style="width:30%;display:inline-block;">Accept</button>
	  </span><i class="fa fa-check-circle" aria-hidden="true"></i></a></li>
	 </ul>
	 </li>
     </ul>
     </dd>
    </dl>
</div>
</div>
<button type="submit" class="btn postupload" style="width:30%;display:inline-block;">Post</button>
<div class="sharemenu">
<ul>
<li><a href="#"> <img src="<?php echo base_url();?>assets/images/share-512.png"  class="img-responsive"> </a></li>
<li><a href="#"> <img src="<?php echo base_url();?>assets/images/fb-art.png" class="img-responsive">  </a></li>
<li><a href="#"><img src="<?php echo base_url();?>assets/images/twitter-icon--flat-gradient-social-iconset--limav-2.png" class="img-responsive">  </a></li>
<li><a href="#"> <img src="<?php echo base_url();?>assets/images/pinterest-icon-png-3.png"  class="img-responsive"> </a></li>
<li><a href="#"> <img src="<?php echo base_url();?>assets/images/Instagram-icon.png" class="img-responsive">  </a></li>
</ul>
</div>
	</div>
	</form>
	</div>
  </div>
<div class="row">
  <div class="col-md-12">
          <div class="selectmenu">
          <ul>
          <li><a href="#"> Category 1   <input type="radio" name="one" ></a></li>
           <li><a href="#">  Category 1  <input type="radio"  name="one">  </a></li>
            <li><a href="#">  Category 1  <input type="radio"  name="one"> </a></li>
             <li><a href="#">  Category 1  <input type="radio"  name="one"> </a></li>
              <li><a href="#">  Category 1  <input type="radio"  name="one"> </a></li>
               <li><a href="#">  Category 1  <input type="radio"  name="one"> </a></li>
          </ul>
          </div>
          <div class="selectmenu">
          <ul>
          <li><a href="#"> <span>Category 1 </span> <input type="radio" ></a></li>
           <li><a href="#"> Category 1   <input type="radio" name="two"></a></li>
           <li><a href="#">  Category 1  <input type="radio" name="two">  </a></li>
            <li><a href="#">  Category 1  <input type="radio" name="two"> </a></li>
             <li><a href="#">  Category 1  <input type="radio" name="two"> </a></li>
              <li><a href="#">  Category 1  <input type="radio" name="two"> </a></li>
               <li><a href="#">  Category 1  <input type="radio" name="two"> </a></li>
               <li><a href="#">  Category 1  <input type="radio" name="two"> </a></li>
               <li><a href="#">  Category 1  <input type="radio" name="two"> </a></li>
          </ul>
          </div>
          <div class="selectmenu">
          <ul>
          <li><a href="#"> Category 1   <input type="radio" name="three"></a></li>
           <li><a href="#">  Category 1  <input type="radio" name="three">  </a></li>
            <li><a href="#">  Category 1  <input type="radio" name="three"> </a></li>
             <li><a href="#">  Category 1  <input type="radio" name="three"> </a></li>
              <li><a href="#">  Category 1  <input type="radio" name="three"> </a></li>
               <li><a href="#">  Category 1  <input type="radio" name="three"> </a></li>
          </ul>
          </div>
           <div class="selectmenu">
           <ul>
       <li><a href="#"> Category 1   <input type="radio" name="four"></a></li>
           <li><a href="#">  Category 1  <input type="radio" name="four">  </a></li>
            <li><a href="#">  Category 1  <input type="radio" name="four"> </a></li>
             <li><a href="#">  Category 1  <input type="radio" name="four"> </a></li>
              <li><a href="#">  Category 1  <input type="radio" name="four"> </a></li>
               <li><a href="#">  Category 1  <input type="radio" name="four"> </a></li>
            </ul>
            </div> 
          <div class="selectmenu">
          <ul>
        <li><a href="#"> Category 1   <input type="radio" name="five"></a></li>
           <li><a href="#">  Category 1  <input type="radio" name="five">  </a></li>
            <li><a href="#">  Category 1  <input type="radio" name="five"> </a></li>
             <li><a href="#">  Category 1  <input type="radio" name="five"> </a></li>
              <li><a href="#">  Category 1  <input type="radio" name="five"> </a></li>
               <li><a href="#">  Category 1  <input type="radio" name="five"> </a></li>
          </ul>
        </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
   <div class="footer">
    <div class="row">
      <div class="col-md-12">
        <div class="footmen">
          <ul>
            <li><a href="#"> About </a></li>
            <li><a href="#"> Press </a></li>
            <li><a href="#"> Copyright </a></li>
            <li><a href="#"> Creators</a></li>
            <li><a href="#"> Advertise</a></li>
            <li><a href="#"> Developers</a></li>
            <li><a href="#">Pupilclipe</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="minfootmen">
          <ul>
            <li><a href="#"> Terms</a></li>
            <li><a href="#"> Privacy</a></li>
            <li><a href="#"> Policy & Safety</a></li>
            <li><a href="#"> Send feedback</a></li>
            <li><a href="#"> Test new features</a></li>
            <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <!--//footer--> 
</div>
<!-- Classie --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script>
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="<?php echo base_url();?>assets/js/bootstrap-dropdownhover.min.js"></script>
<script src="<?php echo base_url();?>assets/js/custom.js"></script>

<!--<script src="js/metisMenu.js"></script>--> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script>
</body>
</html>