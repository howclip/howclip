<!DOCTYPE HTML>
<html>
<head>
<title>Pupilclip</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Novus Admin Panel Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="<?php echo base_url();?>assets/css/style.css" rel='stylesheet' type='text/css' />
<!-- font CSS -->
<!-- font-awesome icons -->
<link href="<?php echo base_url();?>assets/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/sidebar-menu.css">
<!-- //font-awesome icons -->
<!-- js-->
<script src="<?php echo base_url();?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url();?>assets/js/modernizr.custom.js"></script>
<!--webfonts-->
<link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
<!--//webfonts-->
<!--animate-->
<link href="<?php echo base_url();?>assets/css/animate.css" rel="stylesheet" type="text/css" media="all">
<script src="<?php echo base_url();?>assets/js/wow.min.js"></script>
<script>
		 new WOW().init();
</script>
<!--//end-animate-->
<!-- Metis Menu -->
<script src="<?php echo base_url();?>assets/js/metisMenu.min.js"></script>
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<style>
.widthdrop {
	min-width: auto;
	width: 100%;
}
</style>
<style>
.widthdrop {
	min-width: auto;
	width: 100%;
}
</style>

<!--//Metis Menu -->
</head>
<body class="cbp-spmenu-push">
<div class="main-content"> 
  <!--left-fixed -navigation--> 
  
  <!--left-fixed -navigation--> 
  <!-- header-starts -->
  <div class="sticky-header header-section ">
    <div class="header-left"> 
      <!--toggle button start--> 
      <!--   <button id="showLeftPush"><i class="fa fa-bars"></i></button>-->
      
      <div class="dropdown" style="float:left;margin-left: 20px; margin-top: 15px; margin-right:15px;">
        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> <i class="fa fa-bars"></i> </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li role="separator" class="divider"></li>
          <li><a href="#">Separated link</a></li>
        </ul>
      </div>
      <!--toggle button end--> 
      <!--logo -->
      <div class="logo"  > <a href="index.html"> 
        <!--<h1>Pupilclip</h1>
        <span>Website</span> </a>--> 
        <img src="<?php echo base_url();?>assets/images/logo.png" width="90" class="img-responsive"> </a> </div>
      <!--//logo--> 
      <!--search-box-->
      <div class="search-box">
        <form class="input">
          <input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
          <label class="input__label" for="input-31"> <svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
            <path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
            </svg> </label>
        </form>
      </div>
      <!--//end-search-box-->
      <div class="clearfix"> </div>
    </div>
    <div class="header-right">
      <div class="profile_details_left"><!--notifications of menu start -->
        <ul class="nofitications-dropdown">
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" > <img src="<?php echo base_url();?>assets/images/upload.png" class="img-responsive" width="105" > </a>
            <ul class="dropdown-menu">
              <li>
                <div class="notification_header">
                  <h3>Uploading Files</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
          </li>
          <!-- <li class="dropdown head-dpdn"> <a href="login.html"> <img src="images/signinbut.png" class="img-responsive but"></a> </li>-->
          <li class="dropdown head-dpdn"> <a href="signup.html"> <img src="<?php echo base_url();?>assets/images/signinup.png" class="img-responsive but hiderespo"></a> </li>
          <li class="dropdown head-dpdn"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color:#333;" >
            <div class="proimg"><img src="<?php echo base_url();?>assets/images/photos-import-vflI4uOxj.png" class="img-responsive"></div>
            </a>
            <ul class="dropdown-menu" style="left:initial; right:0;">
              <li>
                <div class="notification_header">
                  <h3>Uploading Files</h3>
                </div>
              </li>
              <li><a href="#">
                <div class="notification_desc">
                  <p>Lorem ipsum dolor amet</p>
                  <p><span>1 hour ago</span></p>
                </div>
                <div class="clearfix"></div>
                </a></li>
              <li>
                <div class="notification_bottom"> <a href="#">See all messages</a> </div>
              </li>
            </ul>
          </li>
        </ul>
        <div class="clearfix"> </div>
      </div>
      
      
    </div>
  </div>
  <div class="uploadwrap">
    <div class="container-fluid" style="margin:30px 0px;">
    <div class="row">
    <div class="col-md-12">
    <div class="blankpage">
    <div class="row">
        <div class="col-md-1"></div>
          <div class="col-md-2">
          <div class="selectmenu1advance">
          <ul>
          <li><a href="#"> Category 1   <input type="checkbox" ></a></li>
           <li><a href="#">  Category 1  <input type="checkbox" >  </a></li>
            <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
             <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
              <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
               <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
          </ul>
          </div>
          </div>
           <div class="col-md-2">
          <div class="selectmenu1advance">
          <ul>
          <li><a href="#"> <span>Category 1  </span> <input type="checkbox" >  </a></li>
           <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
            <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
             <li><a href="#">  Category 1   <input type="checkbox" ></a></li>
              <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
               <li><a href="#">  Category 1   <input type="checkbox" >  </a></li>
          </ul>
          </div>
          
          </div>
           <div class="col-md-2">
          <div class="selectmenu1advance">
          <ul>
          <li><a href="#"> Category 1   <input type="checkbox" >  </a></li>
           <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
            <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
             <li><a href="#">  Category 1   <input type="checkbox" ></a></li>
              <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
               <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
          </ul>
          </div>
          </div>
           <div class="col-md-2">
           <div class="selectmenu1advance">
           <ul>
            <li><a href="#"> Category 1   <input type="checkbox" >  </a></li>
            <li><a href="#">  Category 1   <input type="checkbox" ></a></li>
            <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
            <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
            <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
            <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
            </ul>
            </div>
            </div> 
          <div class="col-md-2">
          <div class="selectmenu1advance">
          <ul>
           <li><a href="#"> Category 1   <input type="checkbox" >  </a></li>
           <li><a href="#">  Category 1  <input type="checkbox" ></a></li>
           <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
           <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
           <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
           <li><a href="#">  Category 1  <input type="checkbox" > </a></li>
          </ul>
          </div>
        </div>
        <div class="col-md-1"></div>
      </div>
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <form class="form-inline">
          <div class="col-md-5">
              <button type="submit" class="btn menubutpage">Send invitation</button>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <select class="form-control login" style="margin:10px 0px;" >
                  <option>Short By</option>
                  <option>Newest</option>
                  <option>Last Week</option>
                  <option>This Month</option>
                  <option>This Year</option>
                </select>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group">
                <select class="form-control login" style="margin:10px 0px;" >
                  <option>Filter</option>
                  <option>Today</option>
                  <option>Short</option>
                  <option>Long</option>
                </select>
              </div>
            </div>
            
          </form>
        </div>
      </div>
    </div>
    </div>
    </div>
    </div>
    <div class="row">
    <div class="col-md-12">
    <div class="blankpage">
    <div class="row" style="margin-top:20px;">
          <div class="col-md-10 col-md-offset-1">
            <h3 class="title1"><strong>Result:</strong></h3>
          </div>
        </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-3 col-xs-6">
            <div class="thumbnail"> <a href="#"><img src="<?php echo base_url();?>assets/images/36569259-good-wallpapers.jpg" class="img-responsive"></a>
              <div class="victo">2:50pm</div>
              <div class="caption catch">
                <h3><a href="#">Thumbnail label</a></h3>
                <p><a href="#">Lorem Ipsum</a></p>
                <p><a href="#" class="btn btm" role="button">225 Views</a> 
                <a href="#" class="btn ltm" role="button">2 h ago</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-6">
            <div class="thumbnail"> <a href="#"><img src="<?php echo base_url();?>assets/images/37610397-good-wallpapers.jpg" class="img-responsive"></a>
              <div class="victo">2:50pm</div>
              <div class="caption catch">
                <h3><a href="#">Thumbnail label</a></h3>
                <p><a href="#">Lorem Ipsum</a></p>
                <p><a href="#" class="btn btm" role="button">225 Views</a> 
                <a href="#" class="btn ltm" role="button">2 h ago</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-6">
            <div class="thumbnail"> <a href="#"><img src="<?php echo base_url();?>assets/images/37860494-good-wallpapers.jpg" class="img-responsive"></a>
              <div class="victo">2:50pm</div>
              <div class="caption catch">
                <h3><a href="#">Thumbnail label</a></h3>
                <p><a href="#">Lorem Ipsum</a></p>
                <p><a href="#" class="btn btm" role="button">225 Views</a>
                 <a href="#" class="btn ltm" role="button">2 h ago</a></p>
              </div>
            </div>
          </div>
          <div class="col-md-3 col-xs-6">
            <div class="thumbnail"> <a href="#"><img src="<?php echo base_url();?>assets/images/hd-wallpaper-44.jpg" class="img-responsive"></a>
              <div class="victo">2:50pm</div>
              <div class="caption catch">
                <h3><a href="#">Thumbnail label</a></h3>
                <p><a href="#">Lorem Ipsum</a></p>
                <p><a href="#" class="btn btm" role="button">225 Views</a>
                 <a href="#" class="btn ltm" role="button">2 h ago</a></p>
              </div>
            </div>
          </div>
    
          </div>
        </div>
        </div>
     </div>
    </div>
    </div> 
  </div>
  <!--<div class="row">
 <form class="form-inline">
 <div class="col-md-5">
 <button type="submit" class="btn menubutpage">Send invitation</button>
 </div>
 <div class="col-md-1">
  <div class="form-group">
   <select class="form-control login" style="margin:10px 0px;width:100%;">
  <option>Short By</option>
  <option>Newest</option>
  <option>Last Week</option>
  <option>4</option>
  <option>5</option>
</select>
  </div>
  </div>
  <div class="col-md-1">
  <div class="form-group">
    <select class="form-control login" style="margin:10px 0px;width:100%;">
  <option>Filter</option>
  <option>2</option>
  <option>3</option>
  <option>4</option>
  <option>5</option>
</select>
  </div>
  </div>
</form>
 </div>--> 
</div>
</div>
<div class="footer">
  <div class="row">
    <div class="col-md-12">
      <div class="footmen">
        <ul>
          <li><a href="#"> About </a></li>
          <li><a href="#"> Press </a></li>
          <li><a href="#"> Copyright </a></li>
          <li><a href="#"> Creators</a></li>
          <li><a href="#"> Advertise</a></li>
          <li><a href="#"> Developers</a></li>
          <li><a href="#">Pupilclipe</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="minfootmen">
        <ul>
          <li><a href="#"> Terms</a></li>
          <li><a href="#"> Privacy</a></li>
          <li><a href="#"> Policy & Safety</a></li>
          <li><a href="#"> Send feedback</a></li>
          <li><a href="#"> Test new features</a></li>
          <li>&copy; 2016. All Rights Reserved | Design by <a href="#" target="_blank"><span style="color:#64c5b8;">Live Software Solution</span></a></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<!--//footer-->
</div>
<!-- Classie --> 
<script src="<?php echo base_url();?>assets/js/classie.js"></script> 
<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script> 
<!--scrolling js--> 
<script src="<?php echo base_url();?>assets/js/jquery.nicescroll.js"></script> 
<script src="<?php echo base_url();?>assets/js/scripts.js"></script> 
<script src="<?php echo base_url();?>assets/js/cssmenujs.js"></script> 
<script src="<?php echo base_url();?>assets/js/fileupload.js"></script> 
<!--//scrolling js--> 
<!-- Bootstrap Core JavaScript --> 
<script src="<?php echo base_url();?>assets/js/bootstrap.js"> </script> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/custom.js"></script> 
<script src="<?php echo base_url();?>assets/js/metisMenu.js"></script> 
<script>
$(function () {
$('#menu').metisMenu({
toggle: false // disable the auto collapse. Default: true.
});
});
</script> 
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/sidebar-menu.js"></script> 
<script>
    $.sidebarMenu($('.sidebar-menu'))
  </script> 

</body>
</html>