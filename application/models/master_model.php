<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Master_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->database();
        $this->load->library('session');
    }

    public function register($data) {
        $this->db->insert('users', $data);
        return true;
    }

    public function getcompanydetail() {
        $this->db->where('type', 'company');
        $this->db->select('*');
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function upload_file($foldername, $filename) {
        //echo  $foldername; echo  $filename; die;
        $config['upload_path'] = 'uploads/' . $foldername . '/';
        $config['allowed_types'] = 'png|jpeg|gif|PNG|JPEG|GIF|jpg|JPG';
        $config['max_size'] = 1024;
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($filename)) {
            return '';
        } else {
            $uploaded_data = $this->upload->data();
            return $uploaded_data['file_name'];
        }
    }

    public function getDefaultSetting() {
        $q = $this->db->get("master_settings");
        if ($q->num_rows() > 0) {
            foreach (($q->result_array()) as $row) {
                $data = $row;
            }

            return $data;
        }
    }

    function menu() {
        $menus = $this->db->get_where('category', array('category_parent' => 0))->result();
        $data = array();
        foreach ($menus as $menu) {
            $submenu = $this->db->get_where('category', array('category_parent' => $menu->id));
            if ($submenu->num_rows() > 0)
                $menu->submenu = $submenu->result();
            else
                $menu->submenu = array();

            $data[] = $menu;
        }
        return $data;
    }

    function show_categories() {

        $this->db->select('*');
        $q = $this->db->get('category');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function show_categoriesById($id) {
        $this->db->where('id', $id);
        $this->db->select('*');
        $q = $this->db->get('category');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;

                $this->db->where('id', $row->category_parent);
                $this->db->select('category_title');
                $qry = $this->db->get('category');
                foreach (($qry->result()) as $r) {
                    $par = $r->category_title;
                }
                $row->parent = $par;
            }
            //print_r($data);
            return $data;
        }
    }

    public function add_category($category) {

        $this->db->insert('category', $category);
        return true;
    }

    public function update_category($category, $id) {

        $this->db->where('id', $id);
        if ($this->db->update('category', $category)) {

            return true;
        } else {
            return false;
        }
    }

    public function delete_cat($id) {
        if ($this->db->delete('category', array('id' => $id))) {
            return true;
        } else {
            return false;
        }
    }

    function getadmindata() {
        $id = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('id', $id);
        $q = $this->db->get('users');

        if ($q->num_rows() > 0) {

            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function Update_setting($user_data) {

        $id = $this->session->userdata('id');
        $this->db->where('id', $id);


        if ($this->db->update('users', $user_data)) {

            return true;
        } else {
            return false;
        }
    }

    public function add_video($video_data) {

        $this->db->insert('video', $video_data);

        return true;
    }

    function Allvideo() {

        $this->db->select('*');
        $this->db->where('privacy', 'public');
        $this->db->order_by('rand()');
        $q = $this->db->get('video', 10);

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;

                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->videoview = $total1;
            }

            return $data;
        }
    }

    function getvideoById($id) {

        $subscriber = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('id', $id);
        $q = $this->db->get('video');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;

                $this->db->select('userLogo');
                $this->db->where('id', $uid);
                $qryuserii = $this->db->get('users')->result_array();
                $userlogoimg = $qryuserii[0]['userLogo'];
                $row->userimg = $userlogoimg;


                $this->db->select('userLogo');
                $this->db->where('id', $subscriber);
                $qryuser = $this->db->get('users')->result_array();
                $userlogo = $qryuser[0]['userLogo'];
                $row->userlogo = $userlogo;

                $this->db->select('username');
                $this->db->where('id', $subscriber);
                $qryuser = $this->db->get('users')->result_array();
                $uname = $qryuser[0]['username'];
                $row->uname = $uname;

                $this->db->select('id');
                $this->db->where('id', $uid);
                $qryu = $this->db->get('users')->result_array();
                $userId = $qryu[0]['id'];
                $row->userId = $userId;

                $this->db->select('status');
                $this->db->where('videoId', $row->id);
                $this->db->where('userId', $this->session->userdata('id'));
                $this->db->order_by('id', desc);
                $qrypay = $this->db->get('payment_info', 1)->result_array();
                $paystatus = $qrypay[0]['status'];
                //echo $this->db->last_query();

                $this->db->select('*');
                $this->db->where('uploaderId', $uid);
                $this->db->where('subcriberId', $subscriber);
                $qsub = $this->db->get('subscribers');
                if ($qsub->num_rows() > 0) {
                    $row->subscriber = "1";
                } else {
                    $row->subscriber = "0";
                }


                $this->db->select('id,category_parent,category_title,status');
                $this->db->where('category_title', $row->video_category);
                $categories = $this->db->get('category')->result_array();
                //echo $this->db->last_query();
                $categoriesId = $categories[0]['id'];
                $categoryParent = $categories[0]['category_parent'];
                $categorytitle = $categories[0]['category_title'];
                //$categorystatus = $categories[0]['status'];

                if ($categoriesId != 0) {
                    $this->db->select('id,category_parent,category_title,status');
                    $this->db->where('id', $categoryParent);
                    $par1 = $this->db->get('category')->result_array();
                    $par1Id = $par1[0]['id'];
                    $par1parent = $par1[0]['category_parent'];
                    $par1title = $par1[0]['category_title'];
                    $par1status = $par1[0]['status'];


                    if ($par1parent != 0) {
                        $this->db->select('id,category_parent,category_title,status');
                        $this->db->where('id', $par1parent);
                        $par2 = $this->db->get('category')->result_array();
                        $par2Id = $par2[0]['id'];
                        $par2parent = $par2[0]['category_parent'];
                        $par2title = $par2[0]['category_title'];
                        $par2status = $par2[0]['status'];
                        if ($par2parent != 0) {
                            $this->db->select('id,category_parent,category_title,status');
                            $this->db->where('id', $par2parent);
                            $par3 = $this->db->get('category')->result_array();
                            $par3Id = $par3[0]['id'];
                            $par3parent = $par3[0]['category_parent'];
                            $par3title = $par3[0]['category_title'];
                            $par3status = $par3[0]['status'];
                            if ($par3parent != 0) {
                                $this->db->select('id,category_parent,category_title,status');
                                $this->db->where('id', $par3parent);
                                $par4 = $this->db->get('category')->result_array();
                                $par4Id = $par4[0]['id'];
                                $par4parent = $par4[0]['category_parent'];
                                $par4title = $par4[0]['category_title'];
                                $par4status = $par4[0]['status'];
                            }
                        }
                    }
                }

                $this->db->select('status');
                $this->db->where('category_title', $categorytitle);
                $categorystatus = $this->db->get('category')->result_array();
                $categoriesstatus = $categorystatus[0]['status'];

                $row->categoryId = $categoriesId;
                $row->categoryStatus = $categoriesstatus;

                $row->par1title = $par1title;
                $row->par1Id = $par1Id;

                $row->par2title = $par2title;
                $row->par2Id = $par2Id;

                $row->par3title = $par3title;
                $row->par3Id = $par3Id;

                $row->par4title = $par4title;
                $row->par4Id = $par4Id;
                $row->Paystate = $paystatus;
            }
            //echo '<pre>';
            //print_r($data);die;
            return $data;
        }
    }

    public function update_video($video_data, $id) {


        $this->db->where('id', $id);
        if ($this->db->update('video', $video_data)) {

            return true;
        } else {
            return false;
        }
    }

    public function del_video($id) {
        $video_data = array('name' => '', 'video_img' => '');
        $this->db->where('id', $id);
        if ($this->db->update('video', $video_data)) {

            return true;
        } else {
            return false;
        }
    }

    public function delete_video($id) {
        if ($this->db->delete('video', array('id' => $id))) {
            return true;
        } else {
            return false;
        }
    }

    function show_AllcategoriesById() {

        $this->db->select('*');
        $q = $this->db->get('category');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                if ($row->category_parent != 0) {
                    $this->db->where('id', $row->category_parent);
                    $this->db->select('category_title');
                    $qry = $this->db->get('category');
                    foreach (($qry->result()) as $r) {
                        $par = $r->category_title;
                    }
                    $row->parent = $par;
                }
            }
            //print_r($data);
            return $data;
        }
    }

    public function getCategoryForParentId($parent_id = 0) {
        $categories = array();
        $this->db->where('category_parent', $parent_id);
        $this->db->where('status', 'active');

        $query = $this->db->get('category');
        $result = $query->result_array();

        foreach ($result as $mainCategory) {
            $category = array();
            $category['id'] = $mainCategory['id'];
            $category['title'] = $mainCategory['category_title'];
            $category['parentid'] = $mainCategory['category_parent'];
            $category['submenu'] = $this->getactivecategories($category['id']);
            $categories[$mainCategory['id']] = $category;
        }

        return $categories;
    }

    function getactivecategories($parent_id = 0) {
        $categories = array();
        $this->db->where('category_parent', $parent_id);
        $this->db->where('status', 'active');
        $query = $this->db->get('category');
        $result = $query->result_array();

        foreach ($result as $mainCategory) {
            $category = array();
            $category['id'] = $mainCategory['id'];
            $category['title'] = $mainCategory['category_title'];
            $category['parentid'] = $mainCategory['category_parent'];
            $category['submenu'] = $this->getCategoryForParentId($category['id']);
            $categories[$mainCategory['id']] = $category;
        }
        // print_r($categories);die;
        return $categories;
    }

    public function get_autocomplete($search_data) {
        $uid = $this->session->userdata('id');
        $search_data;
        $query = $this->db->select('*')->from('video')
                        ->where("videoname LIKE '$search_data%' and privacy = 'public' and userId = $uid")->get();


        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            $result == '';
            return $result;
        }
    }

    public function get_autocompletedata($search_data) {

        $search_data;
        $query = $this->db->select('*')->from('video')
                        ->where("videoname LIKE '$search_data%' and privacy = 'public'")->get();
        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            $result == '';
            return $result;
        }
    }

    public function get_autocompletedatalist($search_data) {

        $search_data;
        $query = $this->db->select('*')->from('playlist')
                        ->where("playlist_name LIKE '$search_data%'")->get();

        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            $result == '';
            return $result;
        }
    }

    function videoByFeature() {
        $this->db->select('*');
        $this->db->where('status', 'Active');
        $q = $this->db->get('videofeature');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {

                $fea = $row->name;
                $this->db->select('*');
                $this->db->where('videofeature', $fea);
                $this->db->where('privacy', 'public');
                $this->db->order_by('rand()');
                $this->db->limit(4);
                $qry = $this->db->get('video');

                if ($qry->num_rows() > 0) {
                    foreach (($qry->result()) as $key => $rowres) {
                        $vid = $rowres->id; //die;

                        $returndata[$fea][] = $rowres;
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $vid);
                        $query = $this->db->get('views')->result_array();
                        $total = $query[0]['view'];
                        $rowres->views = $total;
                    }
                }
            }

            return($returndata);
        }
    }

    function getAbout() {
        $this->db->select('*');
        $q = $this->db->get('about', 1);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function updatedetail($data, $id) {


        $this->db->where('id', $id);
        if ($this->db->update('video', $data)) {

            return true;
        } else {
            return false;
        }
    }

    function getterms() {
        $this->db->select('*');
        $q = $this->db->get('terms&conditions', 1);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function getUserbyId($id) {
        $this->db->where('id', $id);
        $this->db->select('*');
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function updateUser($data, $id) {
        $this->db->where('id', $id);
        if ($this->db->update('users', $data)) {

            return true;
        } else {
            return false;
        }
    }

    function getvideoByUser($id, $start) {

        $userid = $this->session->userdata('id');
        $cnd = "WHERE `userId` = $userid and `videoname` != '' order by `id` DESC LIMIT " . $start . ",10 ";

        $sql = "SELECT * FROM `video` $cnd ";
        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {
            $videores = $q->result();
            foreach ($videores as $key => $row) {
                $data[] = $row;
                $video = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $video);
                $qry = $this->db->get('views')->result_array();
                $total = $qry[0]['view'];

                $this->db->where('video_id', $video);
                $num_rows = $this->db->count_all_results('comments');

                $this->db->select_sum('totallike');
                $this->db->where('video_id', $video);
                $query = $this->db->get('likes')->result_array();
                $totallikes = $query[0]['totallike'];

                $this->db->select_sum('Unlike');
                $this->db->where('video_id', $video);
                $qty = $this->db->get('likes')->result_array();
                $Unlikes = $qty[0]['Unlike'];

                $this->db->select('userLogo');
                $this->db->where('id', $userid);
                $qtyuser = $this->db->get('users')->result_array();
                $userlogo = $qtyuser[0]['userLogo'];


                $videores[$key]->view = $total;
                $videores[$key]->comment = $num_rows;
                $videores[$key]->like = $totallikes;
                $videores[$key]->Unlike = $Unlikes;
            }
            //echo '<pre>';
            return $videores;
        } else {

            $data['noresult'] = "No results Found!";
            $dataa = (object) $data;
            $data1['0'] = $dataa;
            return $data1;
        }
    }

    function getvideoname($video) {
        $this->db->select('name,video_img');
        $this->db->where('id', $video);
        $q = $this->db->get('video');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $name = $row->name;
                $img = $row->video_img;
                $data = $img . "," . $name;
            }
            return $data;
        }
    }

    function getvideoByUserdata($userid, $pri) {
        $this->db->select('*');
        $this->db->where('userId', $userid);
        $this->db->where('privacy', $pri);
        $q = $this->db->get('video');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function insertlike($video, $user) {

        $this->db->select('totallike,Unlike');
        $this->db->where('user_id', $user);
        $this->db->where('video_id', $video);
        $q = $this->db->get('likes');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $rowlike) {

                $rowlike->totallike;
                $rowlike->Unlike;
            }
            if ($rowlike->Unlike == '1' && $rowlike->totallike == '0') {

                $data = array('totallike' => '1',
                    'Unlike' => 'Unlike-1',
                );
                $this->db->where('user_id', $user);
                $this->db->where('video_id', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('video_id', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('video_id', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }
                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->Unlike == '0' && $rowlike->totallike == '0') {
                $data = array('totallike' => '1',
                );
                $this->db->where('user_id', $user);
                $this->db->where('video_id', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('video_id', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('video_id', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }
                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;


                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->totallike == '1' && $rowlike->Unlike == '0') {
                $data = array('totallike' => '0',
                );
                $this->db->where('user_id', $user);
                $this->db->where('video_id', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('video_id', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('video_id', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }

                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;


                    echo $likearr = json_encode($likedata);
                }
            }
        } else {
            $insertdata = array('totallike' => 1,
                'user_id' => $user,
                'video_id' => $video);
            if ($this->db->insert('likes', $insertdata)) {
                $this->db->select('totallike');
                $this->db->where('video_id', $video);
                $q = $this->db->get('likes')->result_array();

                foreach ($q as $res) {
                    $ab +=$res['totallike'];
                }
                $this->db->select('Unlike');
                $this->db->where('video_id', $video);
                $qry = $this->db->get('likes')->result_array();

                foreach ($qry as $resu) {
                    $abc +=$resu['Unlike'];
                }

                $likedata['totallike'] = $ab;
                $likedata['Unlike'] = $abc;


                echo $likearr = json_encode($likedata);
            }
        }
    }

    public function insertdislike($video, $user) {


        $this->db->select('totallike,Unlike');
        $this->db->where('user_id', $user);
        $this->db->where('video_id', $video);
        $q = $this->db->get('likes');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $rowlike) {

                $rowlike->totallike;
                $rowlike->Unlike;
            }
            if ($rowlike->totallike == '1' && $rowlike->Unlike == '0') {
                $data = array('Unlike' => '1',
                    'totallike' => 'totallike-1'
                );
                $this->db->where('user_id', $user);
                $this->db->where('video_id', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('video_id', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('video_id', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }
                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->totallike == '0' && $rowlike->Unlike == '0') {
                $data = array('Unlike' => '1',
                );
                $this->db->where('user_id', $user);
                $this->db->where('video_id', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('video_id', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('video_id', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }

                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->Unlike == '1' && $rowlike->totallike == '0') {
                $data = array('Unlike' => 'Unlike-1',
                );
                $this->db->where('user_id', $user);
                $this->db->where('video_id', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('video_id', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('video_id', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }

                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
        } else {

            $insertdata = array('Unlike' => 1,
                'user_id' => $user,
                'video_id' => $video);
            if ($this->db->insert('likes', $insertdata)) {
                $this->db->select('Unlike');
                $this->db->where('video_id', $video);
                $q = $this->db->get('likes')->result_array();

                foreach ($q as $res) {
                    $ab +=$res['Unlike'];
                }
                $this->db->select('totallike');
                $this->db->where('video_id', $video);
                $qry = $this->db->get('likes')->result_array();

                foreach ($qry as $resu) {
                    $abc +=$resu['totallike'];
                }
                $likedata['totallike'] = $abc;
                $likedata['Unlike'] = $ab;
                echo $likearr = json_encode($likedata);
            }
        }
    }

    function Alllikes($id) {
        $this->db->select('totallike');
        $this->db->where('video_id', $id);
        $q = $this->db->get('likes')->result_array();

        foreach ($q as $res) {
            $ab +=$res['totallike'];
        }
        return $ab;
    }

    function Alldislikes($id) {
        $this->db->select('Unlike');
        $this->db->where('video_id', $id);
        $q = $this->db->get('likes')->result_array();

        foreach ($q as $res) {
            $ab +=$res['Unlike'];
        }
        return $ab;
    }

    function getcomment($video, $userid) {
        $this->db->select('comment,user_name');
        $this->db->where('video_id', $video);
        $this->db->where('userId', $userid);
        $this->db->order_by('id', desc);
        $q = $this->db->get('comments', 1);

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $com = $row->comment;
                $name = $row->user_name;
                $redata['user'] = $name;
                $redata['comment'] = $com;
                echo $dataarr = json_encode($redata);
            }
        }
    }

    function getcommentplay($list, $uploader) {
        $this->db->select('comment,user_name');
        $this->db->where('uploader', $uploader);
        $this->db->where('playlist', $list);
        $this->db->order_by('id', desc);
        $q = $this->db->get('comments', 1);

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $com = $row->comment;
                $name = $row->user_name;
                $redata['user'] = $name;
                $redata['comment'] = $com;
                echo $dataarr = json_encode($redata);
            }
        }
    }

    function Allcomments($id) {
        $this->db->select('comment,userId');
        $this->db->where('video_id', $id);
        $q = $this->db->get('comments', 5);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $user = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $user);
                $qr = $this->db->get('users')->result_array();
                $username = $qr[0]['username'];
                $row->user = $username;

                $this->db->select('userLogo');
                $this->db->where('id', $user);
                $qr = $this->db->get('users')->result_array();
                $userlogo = $qr[0]['userLogo'];
                $row->userlogo = $userlogo;
                $data[] = $row;
            }
            return $data;
        }
    }

    function Allcommentsplay($play) {
        $this->db->select('comment,userId');
        $this->db->where('playlist', $play);
        $q = $this->db->get('comments', 5);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $user = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $user);
                $qr = $this->db->get('users')->result_array();
                $username = $qr[0]['username'];
                $row->user = $username;

                $this->db->select('userLogo');
                $this->db->where('id', $user);
                $qr = $this->db->get('users')->result_array();
                $userlogo = $qr[0]['userLogo'];
                $row->userlogo = $userlogo;
                $data[] = $row;
            }
            return $data;
        }
    }

    function Allviews($id) {
        $this->db->select_sum('view');
        $this->db->where('video_id', $id);
        $q = $this->db->get('views');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function Allcom($id) {
        $this->db->where('video_id', $id);
        $num_rows = $this->db->count_all_results('comments');
        return $num_rows;
    }

    function getsearchvideos($pra1) {

        $res = substr($pra1, 0, 2);
        $query = $this->db->query("select * from video where videoname like '$res%' and privacy ='public' ");


        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
            return $data;
        }
    }

    function getsearchvideoshort($pra1) {


        $this->db->select('*');
        $this->db->where('video_duration <', '60');
        $this->db->where('video_duration !=', '0');
        $q = $this->db->get('video', 10);

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
            return $data;
        }
    }

    function getsearchvideolong($pra1) {

        $to_time = "00:20:00";

        $this->db->select('*');
        $this->db->where('video_duration <', $to_time);
        $this->db->where('video_duration !=', '0');
        $q = $this->db->get('video', 10);

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
            return $data;
        }
    }

    function getsearchvideobeforeday($pra1) {
        $date = date('Y-m-d', strtotime("-1 days"));
        $this->db->select('*');
        $this->db->where('Date', $date);
        $q = $this->db->get('video', 10);
        if (($q->num_rows() > '0')) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;

                $this->db->select('*');
                $this->db->where('id', $vid);
                $qvideo = $this->db->get('video');

                foreach (($qvideo->result()) as $row2) {

                    $video = $row2->id;
                    $name = $row2->videoname;
                    $cat = $row2->video_category;
                    $price = $row2->price;
                    $desc = $row2->description;
                    $thumb = $row2->videothumb;
                }

                $row->id;
                $row->videoname;
                $row->video_category;
                $row->price;
                $row->description;
                $row->videothumb;
            }
        }
        return $data;
    }

    function getsearchvideobefore($pra1) {

        $date = date('Y-m-d', strtotime("-1 days"));

        $par1;
        $this->db->select('*');
        $this->db->where('userId', $this->session->userdata('id'));
        $this->db->where('Date', $date);
        $this->db->where('video_id >', '0');
        $q = $this->db->get('views');

        if (($q->num_rows() > '0')) {
            $res = array();
            foreach (($q->result()) as $key => $row) {

                $data[] = $row;
                $vid = $row->video_id;

                $this->db->select('*');
                $this->db->where('id', $vid);
                $qtry = $this->db->get('video');
                foreach (($qtry->result()) as $rr) {
                    $rrdata[] = $rr;
                    $vid = $rr->id;
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $vid);
                    $qty = $this->db->get('views')->result_array();
                    $total1 = $qty[0]['view'];
                    $rr->view = $total1;

                    $this->db->select('username');
                    $this->db->where('id', $par1);
                    $qry = $this->db->get('users')->result_array();
                    $user = $qry[0]['username'];
                    $rr->username = $user;
                }
            }
        }
        return $rrdata;
    }

    function getsearchvideobeforeshort($pra1) {



        $par1 = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('userId', $par1);
        $this->db->where('video_id >', '0');
        $q = $this->db->get('views');

        if (($q->num_rows() > '0')) {
            $res = array();
            foreach (($q->result()) as $key => $row) {

                $data[] = $row;
                $vid22 = $row->video_id;

                $duration = "00:02:00";
                $this->db->where('id', $vid22);
                $this->db->where('video_duration <', $duration);
                $this->db->where('video_duration >', '00:00:00');
                $this->db->select('*');
                $qall = $this->db->get('video');

                foreach (($qall->result()) as $rall) {
                    $vid = $rall->id;
                }


                $this->db->select('*');
                $this->db->where('id', $vid);
                $qtry = $this->db->get('video');
                foreach (($qtry->result()) as $rr) {
                    $rrdata[] = $rr;
                    $vid = $rr->id;
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $vid);
                    $qty = $this->db->get('views')->result_array();
                    $total1 = $qty[0]['view'];
                    $rr->view = $total1;

                    $this->db->select('username');
                    $this->db->where('id', $par1);
                    $qry = $this->db->get('users')->result_array();
                    $user = $qry[0]['username'];
                    $rr->username = $user;
                }
            }
        }
        return $rrdata;
    }

    function getsearchvideobeforelong($pra1) {




        $this->db->select('*');
        $this->db->where('userId', $this->session->userdata('id'));
        $this->db->where('video_id >', '0');
        $q = $this->db->get('views');

        if (($q->num_rows() > '0')) {
            $res = array();
            foreach (($q->result()) as $key => $row) {

                $data[] = $row;
                $vid22 = $row->video_id;

                $duration = "00:20:00";
                $this->db->where('id', $vid22);
                $this->db->where('video_duration <', $duration);
                $this->db->where('video_duration >', '00:00:00');
                $this->db->select('*');
                $qall = $this->db->get('video');

                foreach (($qall->result()) as $rall) {
                    $vid = $rall->id;
                }


                $this->db->select('*');
                $this->db->where('id', $vid);
                $qtry = $this->db->get('video');
                foreach (($qtry->result()) as $rr) {
                    $rrdata[] = $rr;
                    $vid = $rr->id;
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $vid);
                    $qty = $this->db->get('views')->result_array();
                    $total1 = $qty[0]['view'];
                    $rr->view = $total1;

                    $this->db->select('username');
                    $this->db->where('id', $par1);
                    $qry = $this->db->get('users')->result_array();
                    $user = $qry[0]['username'];
                    $rr->username = $user;
                }
            }
        }
        return $rrdata;
    }

    function searchBycategory($par1) {
        $se = explode('%', $par1);
        $q = $this->db->query("select * from video where videoname like '$se%' and privacy ='public' ");
        if (($q->num_rows() > '0')) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;

                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
            return $data;
        } else {

            $data['noresult'] = "No results Found!";
            $dataa = (object) $data;
            $data1['0'] = $dataa;
            return $data1;
        }
    }

    function getdataforuser() {

        $uid = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('userId', $uid);
        $q = $this->db->get('video');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $key => $row) {
                $data[] = $row;
                $video = $row->id;
                $uid = $row->userId;
                $this->db->select_sum('view');
                $this->db->where('video_id', $video);
                $qry = $this->db->get('views')->result_array();
                $total = $qry[0]['view'];
                $row->views = $total;
                $now = new DateTime();
                $date = $now->format('Y-m-d');
                $back = date("Y-m-d", strtotime("28 day ago"));

                $this->db->select_sum('view');
                $this->db->where('video_id', $video);
                $this->db->where('Date >', $back);
                $qtry = $this->db->get('views')->result_array();
                //echo $this->db->last_query();
                $total1 = $qtry[0]['view'];
                $row->latestview = $total1;

                $this->db->where('uploaderId', $uid);
                $total_rows = $this->db->count_all_results('subscribers');
                $row->totalsub = $total_rows;
            }
            $this->db->where('uploaderId', $uid);
            $total_subs = $this->db->count_all_results('subscribers');
            $subscriber[] = $total_subs;

            $this->db->where('uploaderId', $uid);
            $this->db->where('Date >', $back);
            $curr_sub = $this->db->count_all_results('subscribers');

            $cursubscriber[] = $curr_sub;

            $this->db->select('comment,user_name,video_id');
            $this->db->where('userId', $uid);
            $this->db->order_by('id', desc);
            $qcom = $this->db->get('comments', 5);
            foreach (($qcom->result()) as $key => $rowdata) {
                $comment[] = $rowdata;
                $vv = $rowdata->video_id;
                $this->db->select('videoname');
                $this->db->where('id', $vv);
                $qm = $this->db->get('video')->result_array();
                $vname = $qm[0]['videoname'];
                $rowdata->videoname = $vname;
            }
            $this->db->select('SEC_TO_TIME( SUM( TIME_TO_SEC( `video_duration` ) ) ) AS 	                    timeSum ');
            $this->db->where('userId', $uid);
            $query = $this->db->get('video')->result_array();
            $duration = $query[0]['timeSum'];

            $data['comment'] = $comment;
            $data['subscriber'] = $subscriber;
            $data['Currsub'] = $cursubscriber;
            $data['watchtime'] = $duration;
            //print_r($data);die;
            return $data;
        }
    }

    function getgettingstarted() {
        $this->db->select('*');
        $q = $this->db->get('gettingstart', 1);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function getcontact() {
        $this->db->select('*');
        $q = $this->db->get('contactus', 1);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function getactivevideofeature() {
        $this->db->select('name');
        $this->db->where('status', 'active');
        $q = $this->db->get('videofeature');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    function getvideosbyfeature($par1) {
        $this->db->select('*');
        $this->db->where('privacy', 'public');
        $this->db->where('videoFeature', $par1);
        $q = $this->db->get('video');
        if (($q->num_rows() > '0')) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
        }
        return $data;
    }

    function getvideosbyfeatureday($par1, $par2) {
        $date = date('Y-m-d', strtotime("-1 days"));
        $this->db->select('*');
        $this->db->where('privacy', 'public');
        $this->db->where('videoFeature', $par2);
        $this->db->where('Date', $date);
        $q = $this->db->get('video');
        if (($q->num_rows() > '0')) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
        }
        return $data;
    }

    function getvideosbyfeatureshort($par1, $par2) {
        $duration = "00:02:00";
        $this->db->select('*');
        $this->db->where('privacy', 'public');
        $this->db->where('videoFeature', $par2);
        $this->db->where('video_duration <', $duration);
        $this->db->where('video_duration >', '00:00:00');
        $q = $this->db->get('video');
        if (($q->num_rows() > '0')) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
        }
        return $data;
    }

    function getvideosbyfeaturelong($par1, $par2) {
        $duration = "00:20:00";
        $this->db->select('*');
        $this->db->where('privacy', 'public');
        $this->db->where('videoFeature', $par2);
        $this->db->where('video_duration <', $duration);
        $this->db->where('video_duration >', '00:00:00');
        $q = $this->db->get('video');
        if (($q->num_rows() > '0')) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $vid = $row->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->view = $total1;
                $uid = $row->userId;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $user = $qry[0]['username'];
                $row->username = $user;
            }
        }
        return $data;
    }

    function gethistory($par1) {

        $par1;
        $this->db->select('*');
        $this->db->where('userId', $par1);
        $this->db->where('video_id >', '0');
        $q = $this->db->get('views');
        // echo $this->db->last_query();
        // die;
        
        if (($q->num_rows() > '0')) {
            $res = array();
            foreach (($q->result()) as $key => $row) {

                $data[] = $row;
                $vid = $row->video_id;

                $this->db->select('*');
                $this->db->where('id', $vid);
                $qtry = $this->db->get('video');
                foreach (($qtry->result()) as $rr) {
                    $rrdata[] = $rr;
                    $vid = $rr->id;
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $vid);
                    $qty = $this->db->get('views')->result_array();
                    $total1 = $qty[0]['view'];
                    $rr->view = $total1;

                    $this->db->select('username');
                    $this->db->where('id', $par1);
                    $qry = $this->db->get('users')->result_array();
                    $user = $qry[0]['username'];
                    $rr->username = $user;
                }
            }
        }
        return $rrdata;
    }

    function subscribe($uploader) {
        $subscriber = $this->session->userdata('id');


        $this->db->select('*');
        $this->db->where('uploaderId', $uploader);
        $this->db->where('subcriberId', $subscriber);
        $q = $this->db->get('subscribers');
        if ($q->num_rows > 0) {
            return false;
        } else {
            $date = date('Y-m-d');
            $data = array(
                'Date' => $date,
                'uploaderId' => $uploader,
                'subcriberId' => $subscriber
            );
            if ($this->db->insert('subscribers', $data)) {
                return true;
            }
        }
    }

    function unsubscribe($uploader) {

        $subscriber = $this->session->userdata('id');

        if ($this->db->delete('subscribers', array('uploaderId' => $uploader, 'subcriberId' => $subscriber))) {
            return true;
        } else {
            return false;
        }
    }

    function getuserSubscriptionforsub($par1, $start) {

        $this->db->select('*');
        $this->db->where('uploaderId', $par1);
        $q = $this->db->get('subscribers');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $uploader = $row->uploaderId;
            }

            $cnd = "WHERE `userId` = $par1 order by `id` DESC LIMIT " . $start . ",10 ";
            $sql = "SELECT * FROM `video` $cnd ";
            $qry = $this->db->query($sql);

            foreach (($qry->result()) as $res) {
                $resu[] = $res;
                $vid = $res->id;
                $this->db->select_sum('view');
                $this->db->where('video_id', $vid);
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $res->videoview = $total1;
            }


            $this->db->select('*');
            $this->db->where('id', $par1);
            $q2 = $this->db->get('users');
            foreach (($q2->result()) as $res2) {
                $resu2[] = $res2;
            }
            $row->uploader = $resu;
            $row->uploaderdetail = $resu2;
            $data = $row;

            return $data;
        }
    }

    function getuserSubscriptionforsublimit($par1, $start) {
        if ($start == '') {
            $start = 0;
        } else {
            $start = $start;
        }



        $cnd = "WHERE `userId` = $par1 order by `id` DESC LIMIT " . $start . ",10 ";
        $sql = "SELECT * FROM `video` $cnd ";
        $qry = $this->db->query($sql);
        foreach (($qry->result()) as $res) {
            $resu[] = $res;
            $vid = $res->id;
            $this->db->select_sum('view');
            $this->db->where('video_id', $vid);
            $qtry = $this->db->get('views')->result_array();
            $total1 = $qtry[0]['view'];
            $res->videoview = $total1;
        }


        $this->db->select('*');
        $this->db->where('id', $par1);
        $q2 = $this->db->get('users');
        foreach (($q2->result()) as $res2) {
            $resu2[] = $res2;
        }
        $row->uploader = $resu;
        $row->uploaderdetail = $resu2;
        $data = $row;

        return $data;
    }

    function getuserSubscription() {

        $uid = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('subcriberId', $uid);
        $q = $this->db->get('subscribers');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $uploader = $row->uploaderId;
                $this->db->select('*');
                $this->db->where('userId', $uploader);
                $qry = $this->db->get('video');
                foreach (($qry->result()) as $res) {
                    $resu[] = $res;
                    $vid = $res->id;
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $vid);
                    $qtry = $this->db->get('views')->result_array();
                    $total1 = $qtry[0]['view'];
                    $res->videoview = $total1;
                }
                $row->uploader = $resu;

                $this->db->select('*');
                $this->db->where('id', $uploader);
                $q2 = $this->db->get('users');
                foreach (($q2->result()) as $res2) {
                    $resu2[] = $res2;
                }
                $row->uploaderdetail = $resu2;
                $data = $row;
            }
            return $data;
        }
    }

    function getAllSubscriber() {

        $uid = $this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('subcriberId', $uid);
        $ab = $this->db->get('subscribers'); {
            foreach (($ab->result_array()) as $key => $abc) {
                $upid[] = $abc['uploaderId'];
            }

            $this->db->select('*');
            $this->db->where_not_in('id', $upid);
            $this->db->where('id !=', $this->session->userdata('id'));
            $q = $this->db->get('users', 10);
            if ($q->num_rows() > 0) {
                foreach (($q->result_array()) as $key => $row) {
                    $id = $row['id'];
                    $data[$id] = $row;
                    $this->db->select('*');
                    $this->db->where('userId', $row['id']);
                    $query = $this->db->get('video');
                    foreach (($query->result_array()) as $key => $qres) {
                        $data[$id]['videos'] = $qres;
                    }
                }
            }
        }
        return $data;
    }

    public function checksubs($par1) {

        $uid = $this->session->userdata('id');

        $this->db->select('*');
        $this->db->where('uploaderId', $par1);
        $this->db->where('subcriberId', $uid);
        $qsub = $this->db->get('subscribers');

        foreach (($qsub->result()) as $row) {

            $data[] = $row;
            if ($qsub->num_rows() > 0) {
                $row->subscriber = "1";
                $row->subId = $par1;
            } else {
                $row->subscriber = "0";
                $row->subId = $par1;
            }
        }
        if ($qsub->num_rows() == "0") {
            $d = array('subscriber' => "0",
                'subId' => $par1);
            $data[] = (object) $d;
        }
        return $data;
    }

    public function updatethumb($img, $video) {
        $img;
        $video;
        $data = array('videothumb' => $img);
        $this->db->where('id', $video);
        $this->db->update('video', $data);
    }

    function getdataforuploader($par1) {

        $uid = $par1;
        $this->db->select('*');
        $this->db->where('userId', $uid);
        $q = $this->db->get('video');
        
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $key => $row) {
                $data[] = $row;
                $video = $row->id;
                $uid = $row->userId;
                $this->db->select_sum('view');
                $this->db->where('video_id', $video);
                $qry = $this->db->get('views')->result_array();
                $total = $qry[0]['view'];
                $row->views = $total;
                //print_r($row->views);die;
                $now = new DateTime();
                $date = $now->format('Y-m-d');
                $back = date("Y-m-d", strtotime("28 day ago"));

                $this->db->select_sum('view');
                $this->db->where('video_id', $video);
                $this->db->where('Date >', $back);
                $qtry = $this->db->get('views')->result_array();
                //echo $this->db->last_query();
                $total1 = $qtry[0]['view'];
                $row->latestview = $total1;

                $this->db->where('uploaderId', $uid);
                $total_rows = $this->db->count_all_results('subscribers');
                $row->totalsub = $total_rows;
            }
            $this->db->where('uploaderId', $uid);
            $total_subs = $this->db->count_all_results('subscribers');
            $subscriber[] = $total_subs;

            $this->db->where('uploaderId', $uid);
            $this->db->where('Date >', $back);
            $curr_sub = $this->db->count_all_results('subscribers');

            $cursubscriber[] = $curr_sub;

            $this->db->select('comment,user_name,video_id');
            $this->db->where('userId', $uid);
            $this->db->order_by('id', desc);
            $qcom = $this->db->get('comments', 5);
            foreach (($qcom->result()) as $key => $rowdata) {
                $comment[] = $rowdata;
                $vv = $rowdata->video_id;
                $this->db->select('videoname');
                $this->db->where('id', $vv);
                $qm = $this->db->get('video')->result_array();
                $vname = $qm[0]['videoname'];
                $rowdata->videoname = $vname;
            }
            $this->db->select('SEC_TO_TIME( SUM( TIME_TO_SEC( `video_duration` ) ) ) AS 	                    timeSum ');
            $this->db->where('userId', $uid);
            $query = $this->db->get('video')->result_array();
            $duration = $query[0]['timeSum'];

            $data['comment'] = $comment;
            $data['subscriber'] = $subscriber;
            $data['Currsub'] = $cursubscriber;
            $data['watchtime'] = $duration;
           // print_r($data);die;
            return $data;
        }
    }

    public function getsubcatdata($catid) {

        $pid = $catid;
        $this->db->select('*');
        $this->db->where('category_parent', $pid);
        $qry = $this->db->get('category');
        if ($qry->num_rows() > 0) {
            foreach (($qry->result()) as $res) {
                $res->category_title;
                $resdata[] = $res;
                $subid = $res->id;
                $this->db->select('*');
                $this->db->where('category_parent', $subid);
                $q1 = $this->db->get('category');
                if ($q1->num_rows() > 0) {
                    foreach (($q1->result()) as $subdata) {

                        $subsubdata[] = $subdata;
                        $this->db->select('*');
                        $this->db->where('category_parent', $subdata->id);
                        $q2 = $this->db->get('category');
                        if ($q2->num_rows() > 0) {
                            foreach (($q2->result()) as $sub2data) {

                                $sub2d[] = $sub2data;
                                $this->db->select('*');
                                $this->db->where('category_parent', $sub2data->id);
                                $q3 = $this->db->get('category');
                                if ($q3->num_rows() > 0) {
                                    foreach (($q3->result()) as $sub3data) {
                                        $sub3data->id;
                                        $sub4data[] = $sub3data;
                                        $this->db->select('*');
                                        $this->db->where('category_parent', $sub3data->id);
                                        $q4 = $this->db->get('category');
                                        if ($q4->num_rows() > 0) {
                                            foreach (($q4->result()) as $sub5data) {
                                                $sub6data[] = $sub5data;
                                            }
                                            $sub3data->sub5 = $sub6data;
                                        }
                                    }
                                    $sub2data->sub4 = $sub4data;
                                }
                            }
                            $subdata->sub3 = $sub2d;
                        }
                    }
                }
                $res->sub2 = $subsubdata;
            }
        }
        $row->sub = $resdata;
        $catdata[] = $row;

        return $catdata;
        //return json_encode($catdata);
    }

    public function getadvancedata($term, $par2) {


        $term = str_replace('%20', ' ', $term);

        if ($par2 == '') {

            $sql = "select * from category where category_title like '%$term%' order by id asc ";
            $qry = $this->db->query($sql);
            // echo $this->db->last_query();die;
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $key0 => $resres) {
                    $pid = $res->category_parent;
                    $resdata[] = $resres;
                    $catp[] = $resres->category_parent;
                    $vcat = $resres->category_title;
                    $vdata = $this->db->get_where('video', array('video_category' => $vcat, 'privacy' => 'public'))->result_array();
                    foreach ($vdata as $keyv => $rv) {
                        $rv['id'];
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $rv['id']);
                        $qtryrv = $this->db->get('views')->row_array();
                        $totalview1 = $qtryrv['view'];
                        $vdata[$keyv]['totalv'] = $totalview1;
                    }
                    $resres->videodata = $vdata;
                }
            } else {
                $sqlelse = "select * from video where videoname like '%$term%' order by id asc";
                $qryy = $this->db->query($sqlelse);

                foreach (($qryy->result()) as $keye => $resq) {
                    $term2 = $resq->video_category;
                    $sqltt = "select * from category where category_title like '%$term2%' order by id asc ";
                    $qrytt = $this->db->query($sqltt);
                    foreach (($qrytt->result()) as $keytt => $resres) {
                        $pid = $res->category_parent;
                        $resdata[] = $resres;
                        $catp[] = $resres->category_parent;
                        $vcat = $resres->category_title;
                        $vdatat = $this->db->get_where('video', array('video_category' => $vcat))->result_array();
                        foreach ($vdatat as $keyvt => $rv) {
                            $rv['id'];
                            $this->db->select_sum('view');
                            $this->db->where('video_id', $rv['id']);
                            $qtryrvt = $this->db->get('views')->row_array();
                            $totalview1t = $qtryrvt['view'];
                            $vdatat[$keyvt]['totalv'] = $totalview1t;
                        }
                        $resres->videodata = $vdatat;
                    }
                }
            }

            $catdata[] = $resdata;
            $dataqqq = $catdata[0];
            foreach ($catdata[0] as $key => $subdata) {
                $id = $subdata->category_parent;
                $this->db->select('*');
                $this->db->where('id', $id);
                $this->db->where_not_in('category_parent', $catp);
                $res = $this->db->get('category')->row_array();
                //$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
                $dataqqq[$key]->sub = $res;
                $pcat = $dataqqq[$key]->sub['category_title'];

                $resname = $this->db->get_where('video', array('video_category' => $pcat))->result_array();

                foreach ($resname as $key1 => $resv) {
                    $resv['id'];
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resv['id']);
                    $qtry = $this->db->get('views')->row_array();
                    $totalview = $qtry['view'];
                    $resname[$key1]['totalview'] = $totalview;
                }
                $dataqqq[$key]->sub['videos'] = $resname;
                $id1 = $res['category_parent'];
                $pcat2 = $res['category_title'];

                $res1 = $this->db->get_where('category', array('id' => $id1))->row_array();
                $dataqqq[$key]->sub['child'] = $res1;

                $resname2 = $this->db->get_where('video', array('video_category' => $pcat2))->result_array();
                foreach ($resname2 as $key2 => $resview) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resview['id']);
                    $qtry2 = $this->db->get('views')->row_array();
                    $totalview1 = $qtry2['view'];
                    $resname2[$key2]['totalsubview'] = $totalview1;
                }

                $dataqqq[$key]->sub['videos'] = $resname2;
                $id2 = $res1['category_parent'];
                $pcat3 = $res1['category_title'];
                $res2 = $this->db->get_where('category', array('id' => $id2))->row_array();
                $dataqqq[$key]->sub['child']['child2'] = $res2;

                $resname3 = $this->db->get_where('video', array('video_category' => $pcat3))->result_array();
                foreach ($resname3 as $key3 => $viewresult) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresult['id']);
                    $qtry3 = $this->db->get('views')->row_array();
                    $totalview2 = $qtry3['view'];
                    $resname3[$key3]['totalchildview'] = $totalview2;
                }


                $cato = $dataqqq[$key]->sub['child']['child2']['category_title'];
                $resname0 = $this->db->get_where('video', array('video_category' => $cato))->result_array();
                foreach ($resname0 as $keykk => $viewresultkk) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultkk['id']);
                    $qtrykk = $this->db->get('views')->row_array();
                    $totalview4 = $qtrykk['view'];
                    $resname0[$keykk]['totalchild2view'] = $qtrykk;
                }

                $dataqqq[$key]->sub['child']['child2']['vvv'] = $resname0;
                $last = $dataqqq[$key]->sub['child']['child2']['category_parent'];
                $reslast = $this->db->get_where('category', array('id' => $last))->row_array();
                $dataqqq[$key]->sub['child']['child2']['child3'] = $reslast;


                $lasttitle = $dataqqq[$key]->sub['child']['child2']['child3']['category_title'];
                $resnamelast = $this->db->get_where('video', array('video_category' => $lasttitle))->result_array();
                foreach ($resnamelast as $keykklast => $viewresultlast) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultlast['id']);
                    $qtrylast = $this->db->get('views')->row_array();
                    $totallast = $qtrylast['view'];
                    $resnamelast[$keykklast]['totalchildviewlast'] = $totallast;
                }

                $dataqqq[$key]->sub['child']['child2']['child3']['video'] = $resnamelast;


                $dataqqq[$key]->sub['child']['videos'] = $resname3;
            }
            return $dataqqq;
        }

        if ($par2 == 'week') {
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;

            $sql = "select * from category where category_title like '%$term%' order by id asc ";
            $qry = $this->db->query($sql);
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $key0 => $resres) {
                    $pid = $res->category_parent;
                    $resdata[] = $resres;
                    $catp[] = $resres->category_parent;
                    $vcat = $resres->category_title;
                    $vdata = $this->db->get_where('video', array('video_category' => $vcat, 'Date >=' => $start_week, 'Date <=' => $end_week))->result_array();
                    foreach ($vdata as $keyv => $rv) {
                        $rv['id'];
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $rv['id']);
                        $qtryrv = $this->db->get('views')->row_array();
                        $totalview1 = $qtryrv['view'];
                        $vdata[$keyv]['totalv'] = $totalview1;
                    }
                    $resres->videodata = $vdata;
                }
            } else {
                $sqlelse = "select * from video where videoname like '%$term%' order by id asc";
                $qryy = $this->db->query($sqlelse);

                foreach (($qryy->result()) as $keye => $resq) {
                    $term2 = $resq->video_category;
                    $sqltt = "select * from category where category_title like '%$term2%' order by id asc ";
                    $qrytt = $this->db->query($sqltt);
                    foreach (($qrytt->result()) as $keytt => $resres) {
                        $pid = $res->category_parent;
                        $resdata[] = $resres;
                        $catp[] = $resres->category_parent;
                        $vcat = $resres->category_title;
                        $vdatat = $this->db->get_where('video', array('video_category' => $vcat))->result_array();
                        foreach ($vdatat as $keyvt => $rv) {
                            $rv['id'];
                            $this->db->select_sum('view');
                            $this->db->where('video_id', $rv['id']);
                            $qtryrvt = $this->db->get('views')->row_array();
                            $totalview1t = $qtryrvt['view'];
                            $vdatat[$keyvt]['totalv'] = $totalview1t;
                        }
                        $resres->videodata = $vdatat;
                    }
                }
            }



            $catdata[] = $resdata;
            $dataqqq = $catdata[0];

            foreach ($catdata[0] as $key => $subdata) {
                $id = $subdata->category_parent;
                $this->db->select('*');
                $this->db->where('id', $id);
                $this->db->where_not_in('category_parent', $catp);
                $res = $this->db->get('category')->row_array();
                //$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
                $dataqqq[$key]->sub = $res;
                $pcat = $dataqqq[$key]->sub['category_title'];

                $resname = $this->db->get_where('video', array('video_category' => $pcat, 'Date >=' => $start_week, 'Date <=' => $end_week))->result_array();

                foreach ($resname as $key1 => $resv) {
                    $resv['id'];
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resv['id']);
                    $qtry = $this->db->get('views')->row_array();
                    $totalview = $qtry['view'];
                    $resname[$key1]['totalview'] = $totalview;
                }
                $dataqqq[$key]->sub['videos'] = $resname;
                $id1 = $res['category_parent'];
                $pcat2 = $res['category_title'];

                $res1 = $this->db->get_where('category', array('id' => $id1))->row_array();
                $dataqqq[$key]->sub['child'] = $res1;

                $resname2 = $this->db->get_where('video', array('video_category' => $pcat2, 'Date >=' => $start_week, 'Date <=' => $end_week))->result_array();
                foreach ($resname2 as $key2 => $resview) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resview['id']);
                    $qtry2 = $this->db->get('views')->row_array();
                    $totalview1 = $qtry2['view'];
                    $resname2[$key2]['totalsubview'] = $totalview1;
                }

                $dataqqq[$key]->sub['videos'] = $resname2;
                $id2 = $res1['category_parent'];
                $pcat3 = $res1['category_title'];
                $res2 = $this->db->get_where('category', array('id' => $id2))->row_array();
                $dataqqq[$key]->sub['child']['child2'] = $res2;

                $resname3 = $this->db->get_where('video', array('video_category' => $pcat3, 'Date >=' => $start_week, 'Date <=' => $end_week))->result_array();
                foreach ($resname3 as $key3 => $viewresult) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresult['id']);
                    $qtry3 = $this->db->get('views')->row_array();
                    $totalview2 = $qtry3['view'];
                    $resname3[$key3]['totalchildview'] = $totalview2;
                }


                $cato = $dataqqq[$key]->sub['child']['child2']['category_title'];
                $resname0 = $this->db->get_where('video', array('video_category' => $cato, 'Date >=' => $start_week, 'Date <=' => $end_week))->result_array();
                foreach ($resname0 as $keykk => $viewresultkk) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultkk['id']);
                    $qtrykk = $this->db->get('views')->row_array();
                    $totalview4 = $qtrykk['view'];
                    $resname0[$keykk]['totalchild2view'] = $qtrykk;
                }
                $dataqqq[$key]->sub['child']['child2']['vvv'] = $resname0;


                $last = $dataqqq[$key]->sub['child']['child2']['category_parent'];
                $reslast = $this->db->get_where('category', array('id' => $last))->row_array();
                $dataqqq[$key]->sub['child']['child2']['child3'] = $reslast;


                $lasttitle = $dataqqq[$key]->sub['child']['child2']['child3']['category_title'];
                $resnamelast = $this->db->get_where('video', array('video_category' => $lasttitle, 'Date >=' => $start_week, 'Date <=' => $end_week))->result_array();
                foreach ($resnamelast as $keykklast => $viewresultlast) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultlast['id']);
                    $qtrylast = $this->db->get('views')->row_array();
                    $totallast = $qtrylast['view'];
                    $resnamelast[$keykklast]['totalchildviewlast'] = $totallast;
                }

                $dataqqq[$key]->sub['child']['child2']['child3']['video'] = $resnamelast;


                $dataqqq[$key]->sub['child']['videos'] = $resname3;
            }

            return $dataqqq;
        }

        if ($par2 == 'month') {
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));


            $sql = "select * from category where category_title like '%$term%' order by id asc ";
            $qry = $this->db->query($sql);
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $key0 => $resres) {
                    $pid = $res->category_parent;
                    $resdata[] = $resres;
                    $catp[] = $resres->category_parent;
                    $vcat = $resres->category_title;
                    $vdata = $this->db->get_where('video', array('video_category' => $vcat, 'Date >=' => $start, 'Date <=' => $end))->result_array();
                    foreach ($vdata as $keyv => $rv) {
                        $rv['id'];
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $rv['id']);
                        $qtryrv = $this->db->get('views')->row_array();
                        $totalview1 = $qtryrv['view'];
                        $vdata[$keyv]['totalv'] = $totalview1;
                    }
                    $resres->videodata = $vdata;
                }
            } else {
                $sqlelse = "select * from video where videoname like '%$term%' order by id asc";
                $qryy = $this->db->query($sqlelse);

                foreach (($qryy->result()) as $keye => $resq) {
                    $term2 = $resq->video_category;
                    $sqltt = "select * from category where category_title like '%$term2%' order by id asc ";
                    $qrytt = $this->db->query($sqltt);
                    foreach (($qrytt->result()) as $keytt => $resres) {
                        $pid = $res->category_parent;
                        $resdata[] = $resres;
                        $catp[] = $resres->category_parent;
                        $vcat = $resres->category_title;
                        $vdatat = $this->db->get_where('video', array('video_category' => $vcat))->result_array();
                        foreach ($vdatat as $keyvt => $rv) {
                            $rv['id'];
                            $this->db->select_sum('view');
                            $this->db->where('video_id', $rv['id']);
                            $qtryrvt = $this->db->get('views')->row_array();
                            $totalview1t = $qtryrvt['view'];
                            $vdatat[$keyvt]['totalv'] = $totalview1t;
                        }
                        $resres->videodata = $vdatat;
                    }
                }
            }
            $catdata[] = $resdata;
            $dataqqq = $catdata[0];

            foreach ($catdata[0] as $key => $subdata) {
                $id = $subdata->category_parent;
                $this->db->select('*');
                $this->db->where('id', $id);
                $this->db->where_not_in('category_parent', $catp);
                $res = $this->db->get('category')->row_array();
                //$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
                $dataqqq[$key]->sub = $res;
                $pcat = $dataqqq[$key]->sub['category_title'];

                $resname = $this->db->get_where('video', array('video_category' => $pcat, 'Date >=' => $start, 'Date <=' => $end))->result_array();

                foreach ($resname as $key1 => $resv) {
                    $resv['id'];
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resv['id']);
                    $qtry = $this->db->get('views')->row_array();
                    $totalview = $qtry['view'];
                    $resname[$key1]['totalview'] = $totalview;
                }
                $dataqqq[$key]->sub['videos'] = $resname;
                $id1 = $res['category_parent'];
                $pcat2 = $res['category_title'];

                $res1 = $this->db->get_where('category', array('id' => $id1))->row_array();
                $dataqqq[$key]->sub['child'] = $res1;

                $resname2 = $this->db->get_where('video', array('video_category' => $pcat2, 'Date >=' => $start, 'Date <=' => $end))->result_array();
                foreach ($resname2 as $key2 => $resview) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resview['id']);
                    $qtry2 = $this->db->get('views')->row_array();
                    $totalview1 = $qtry2['view'];
                    $resname2[$key2]['totalsubview'] = $totalview1;
                }

                $dataqqq[$key]->sub['videos'] = $resname2;
                $id2 = $res1['category_parent'];
                $pcat3 = $res1['category_title'];
                $res2 = $this->db->get_where('category', array('id' => $id2))->row_array();
                $dataqqq[$key]->sub['child']['child2'] = $res2;

                $resname3 = $this->db->get_where('video', array('video_category' => $pcat3, 'Date >=' => $start, 'Date <=' => $end))->result_array();
                foreach ($resname3 as $key3 => $viewresult) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresult['id']);
                    $qtry3 = $this->db->get('views')->row_array();
                    $totalview2 = $qtry3['view'];
                    $resname3[$key3]['totalchildview'] = $totalview2;
                }


                $cato = $dataqqq[$key]->sub['child']['child2']['category_title'];
                $resname0 = $this->db->get_where('video', array('video_category' => $cato, 'Date >=' => $start, 'Date <=' => $end))->result_array();
                foreach ($resname0 as $keykk => $viewresultkk) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultkk['id']);
                    $qtrykk = $this->db->get('views')->row_array();
                    $totalview4 = $qtrykk['view'];
                    $resname0[$keykk]['totalchild2view'] = $qtrykk;
                }
                $dataqqq[$key]->sub['child']['child2']['vvv'] = $resname0;


                $last = $dataqqq[$key]->sub['child']['child2']['category_parent'];
                $reslast = $this->db->get_where('category', array('id' => $last))->row_array();
                $dataqqq[$key]->sub['child']['child2']['child3'] = $reslast;


                $lasttitle = $dataqqq[$key]->sub['child']['child2']['child3']['category_title'];
                $resnamelast = $this->db->get_where('video', array('video_category' => $lasttitle, 'Date >=' => $start, 'Date <=' => $end))->result_array();
                foreach ($resnamelast as $keykklast => $viewresultlast) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultlast['id']);
                    $qtrylast = $this->db->get('views')->row_array();
                    $totallast = $qtrylast['view'];
                    $resnamelast[$keykklast]['totalchildviewlast'] = $totallast;
                }

                $dataqqq[$key]->sub['child']['child2']['child3']['video'] = $resnamelast;


                $dataqqq[$key]->sub['child']['videos'] = $resname3;
            }

            return $dataqqq;
        }

        if ($par2 == 'year') {
            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";

            $sql = "select * from category where category_title like '%$term%' order by id asc ";
            $qry = $this->db->query($sql);
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $key0 => $resres) {
                    $pid = $res->category_parent;
                    $resdata[] = $resres;
                    $vcat = $resres->category_title;
                    $catp[] = $resres->category_parent;
                    $vdata = $this->db->get_where('video', array('video_category' => $vcat, 'Date >=' => $start_year, 'Date <=' => $end_year))->result_array();
                    foreach ($vdata as $keyv => $rv) {
                        $rv['id'];
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $rv['id']);
                        $qtryrv = $this->db->get('views')->row_array();
                        $totalview1 = $qtryrv['view'];
                        $vdata[$keyv]['totalv'] = $totalview1;
                    }
                    $resres->videodata = $vdata;
                }
            } else {
                $sqlelse = "select * from video where videoname like '%$term%' order by id asc";
                $qryy = $this->db->query($sqlelse);

                foreach (($qryy->result()) as $keye => $resq) {
                    $term2 = $resq->video_category;
                    $sqltt = "select * from category where category_title like '%$term2%' order by id asc ";
                    $qrytt = $this->db->query($sqltt);
                    foreach (($qrytt->result()) as $keytt => $resres) {
                        $pid = $res->category_parent;
                        $resdata[] = $resres;
                        $catp[] = $resres->category_parent;
                        $vcat = $resres->category_title;
                        $vdatat = $this->db->get_where('video', array('video_category' => $vcat))->result_array();
                        foreach ($vdatat as $keyvt => $rv) {
                            $rv['id'];
                            $this->db->select_sum('view');
                            $this->db->where('video_id', $rv['id']);
                            $qtryrvt = $this->db->get('views')->row_array();
                            $totalview1t = $qtryrvt['view'];
                            $vdatat[$keyvt]['totalv'] = $totalview1t;
                        }
                        $resres->videodata = $vdatat;
                    }
                }
            }
            $catdata[] = $resdata;
            $dataqqq = $catdata[0];

            foreach ($catdata[0] as $key => $subdata) {
                $id = $subdata->category_parent;
                $this->db->select('*');
                $this->db->where('id', $id);
                $this->db->where_not_in('category_parent', $catp);
                $res = $this->db->get('category')->row_array();
                //$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
                $dataqqq[$key]->sub = $res;
                $pcat = $dataqqq[$key]->sub['category_title'];

                $resname = $this->db->get_where('video', array('video_category' => $pcat, 'Date >=' => $start_year, 'Date <=' => $end_year))->result_array();

                foreach ($resname as $key1 => $resv) {
                    $resv['id'];
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resv['id']);
                    $qtry = $this->db->get('views')->row_array();
                    $totalview = $qtry['view'];
                    $resname[$key1]['totalview'] = $totalview;
                }
                $dataqqq[$key]->sub['videos'] = $resname;
                $id1 = $res['category_parent'];
                $pcat2 = $res['category_title'];

                $res1 = $this->db->get_where('category', array('id' => $id1))->row_array();
                $dataqqq[$key]->sub['child'] = $res1;

                $resname2 = $this->db->get_where('video', array('video_category' => $pcat2, 'Date >=' => $start_year, 'Date <=' => $end_year))->result_array();
                foreach ($resname2 as $key2 => $resview) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resview['id']);
                    $qtry2 = $this->db->get('views')->row_array();
                    $totalview1 = $qtry2['view'];
                    $resname2[$key2]['totalsubview'] = $totalview1;
                }

                $dataqqq[$key]->sub['videos'] = $resname2;
                $id2 = $res1['category_parent'];
                $pcat3 = $res1['category_title'];
                $res2 = $this->db->get_where('category', array('id' => $id2))->row_array();
                $dataqqq[$key]->sub['child']['child2'] = $res2;

                $resname3 = $this->db->get_where('video', array('video_category' => $pcat3, 'Date >=' => $start_year, 'Date <=' => $end_year))->result_array();
                foreach ($resname3 as $key3 => $viewresult) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresult['id']);
                    $qtry3 = $this->db->get('views')->row_array();
                    $totalview2 = $qtry3['view'];
                    $resname3[$key3]['totalchildview'] = $totalview2;
                }


                $cato = $dataqqq[$key]->sub['child']['child2']['category_title'];
                $resname0 = $this->db->get_where('video', array('video_category' => $cato, 'Date >=' => $start_year, 'Date <=' => $end_year))->result_array();
                foreach ($resname0 as $keykk => $viewresultkk) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultkk['id']);
                    $qtrykk = $this->db->get('views')->row_array();
                    $totalview4 = $qtrykk['view'];
                    $resname0[$keykk]['totalchild2view'] = $qtrykk;
                }
                $dataqqq[$key]->sub['child']['child2']['vvv'] = $resname0;


                $last = $dataqqq[$key]->sub['child']['child2']['category_parent'];
                $reslast = $this->db->get_where('category', array('id' => $last))->row_array();
                $dataqqq[$key]->sub['child']['child2']['child3'] = $reslast;


                $lasttitle = $dataqqq[$key]->sub['child']['child2']['child3']['category_title'];
                $resnamelast = $this->db->get_where('video', array('video_category' => $lasttitle, 'Date >=' => $start_year, 'Date <=' => $end_year))->result_array();
                foreach ($resnamelast as $keykklast => $viewresultlast) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultlast['id']);
                    $qtrylast = $this->db->get('views')->row_array();
                    $totallast = $qtrylast['view'];
                    $resnamelast[$keykklast]['totalchildviewlast'] = $totallast;
                }

                $dataqqq[$key]->sub['child']['child2']['child3']['video'] = $resnamelast;


                $dataqqq[$key]->sub['child']['videos'] = $resname3;
            }

            return $dataqqq;
        }
        if ($par2 == "day") {
            $date = Date('Y-m-d');

            $sql = "select * from category where category_title like '%$term%' order by id desc ";
            $qry = $this->db->query($sql);
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $key0 => $resres) {
                    $pid = $res->category_parent;
                    $resdata[] = $resres;
                    $vcat = $resres->category_title;
                    $catp[] = $resres->category_parent;
                    $vdata = $this->db->get_where('video', array('video_category' => $vcat, 'Date' => $date))->result_array();
                    foreach ($vdata as $keyv => $rv) {
                        $rv['id'];
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $rv['id']);
                        $qtryrv = $this->db->get('views')->row_array();
                        $totalview1 = $qtryrv['view'];
                        $vdata[$keyv]['totalv'] = $totalview1;
                    }
                    $resres->videodata = $vdata;
                }
            } else {
                $sqlelse = "select * from video where videoname like '%$term%' order by id asc";
                $qryy = $this->db->query($sqlelse);

                foreach (($qryy->result()) as $keye => $resq) {
                    $term2 = $resq->video_category;
                    $sqltt = "select * from category where category_title like '%$term2%' order by id asc ";
                    $qrytt = $this->db->query($sqltt);
                    foreach (($qrytt->result()) as $keytt => $resres) {
                        $pid = $res->category_parent;
                        $resdata[] = $resres;
                        $catp[] = $resres->category_parent;
                        $vcat = $resres->category_title;
                        $vdatat = $this->db->get_where('video', array('video_category' => $vcat))->result_array();
                        foreach ($vdatat as $keyvt => $rv) {
                            $rv['id'];
                            $this->db->select_sum('view');
                            $this->db->where('video_id', $rv['id']);
                            $qtryrvt = $this->db->get('views')->row_array();
                            $totalview1t = $qtryrvt['view'];
                            $vdatat[$keyvt]['totalv'] = $totalview1t;
                        }
                        $resres->videodata = $vdatat;
                    }
                }
            }
            $catdata[] = $resdata;
            $dataqqq = $catdata[0];

            foreach ($catdata[0] as $key => $subdata) {
                $id = $subdata->category_parent;
                $this->db->select('*');
                $this->db->where('id', $id);
                $this->db->where_not_in('category_parent', $catp);
                $res = $this->db->get('category')->row_array();
                //$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
                $dataqqq[$key]->sub = $res;
                $pcat = $dataqqq[$key]->sub['category_title'];

                $resname = $this->db->get_where('video', array('video_category' => $pcat, 'Date' => $date))->result_array();

                foreach ($resname as $key1 => $resv) {
                    $resv['id'];
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resv['id']);
                    $qtry = $this->db->get('views')->row_array();
                    $totalview = $qtry['view'];
                    $resname[$key1]['totalview'] = $totalview;
                }
                $dataqqq[$key]->sub['videos'] = $resname;
                $id1 = $res['category_parent'];
                $pcat2 = $res['category_title'];

                $res1 = $this->db->get_where('category', array('id' => $id1))->row_array();
                $dataqqq[$key]->sub['child'] = $res1;

                $resname2 = $this->db->get_where('video', array('video_category' => $pcat2, 'Date' => $date))->result_array();
                foreach ($resname2 as $key2 => $resview) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resview['id']);
                    $qtry2 = $this->db->get('views')->row_array();
                    $totalview1 = $qtry2['view'];
                    $resname2[$key2]['totalsubview'] = $totalview1;
                }

                $dataqqq[$key]->sub['videos'] = $resname2;
                $id2 = $res1['category_parent'];
                $pcat3 = $res1['category_title'];
                $res2 = $this->db->get_where('category', array('id' => $id2))->row_array();
                $dataqqq[$key]->sub['child']['child2'] = $res2;

                $resname3 = $this->db->get_where('video', array('video_category' => $pcat3, 'Date' => $date))->result_array();
                foreach ($resname3 as $key3 => $viewresult) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresult['id']);
                    $qtry3 = $this->db->get('views')->row_array();
                    $totalview2 = $qtry3['view'];
                    $resname3[$key3]['totalchildview'] = $totalview2;
                }


                $cato = $dataqqq[$key]->sub['child']['child2']['category_title'];
                $resname0 = $this->db->get_where('video', array('video_category' => $cato, 'Date' => $date))->result_array();
                foreach ($resname0 as $keykk => $viewresultkk) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultkk['id']);
                    $qtrykk = $this->db->get('views')->row_array();
                    $totalview4 = $qtrykk['view'];
                    $resname0[$keykk]['totalchild2view'] = $qtrykk;
                }
                $dataqqq[$key]->sub['child']['child2']['vvv'] = $resname0;


                $last = $dataqqq[$key]->sub['child']['child2']['category_parent'];
                $reslast = $this->db->get_where('category', array('id' => $last))->row_array();
                $dataqqq[$key]->sub['child']['child2']['child3'] = $reslast;


                $lasttitle = $dataqqq[$key]->sub['child']['child2']['child3']['category_title'];
                $resnamelast = $this->db->get_where('video', array('video_category' => $lasttitle, 'Date' => $date))->result_array();
                foreach ($resnamelast as $keykklast => $viewresultlast) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultlast['id']);
                    $qtrylast = $this->db->get('views')->row_array();
                    $totallast = $qtrylast['view'];
                    $resnamelast[$keykklast]['totalchildviewlast'] = $totallast;
                }

                $dataqqq[$key]->sub['child']['child2']['child3']['video'] = $resnamelast;


                $dataqqq[$key]->sub['child']['videos'] = $resname3;
            }
            return $dataqqq;
        }
        if ($par2 == 'short') {

            $duration = "00:30:00";

            $sql = "select * from category where category_title like '%$term%' order by id asc ";
            $qry = $this->db->query($sql);
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $key0 => $resres) {
                    $pid = $res->category_parent;
                    $resdata[] = $resres;
                    $vcat = $resres->category_title;
                    $catp[] = $resres->category_parent;
                    $vdata = $this->db->get_where('video', array('video_category' => $vcat, 'video_duration <' => $duration, 'video_duration >' => '00:00:00'))->result_array();
                    // echo $this->db->last_query();die;
                    foreach ($vdata as $keyv => $rv) {
                        $rv['id'];
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $rv['id']);
                        $qtryrv = $this->db->get('views')->row_array();
                        $totalview1 = $qtryrv['view'];
                        $vdata[$keyv]['totalv'] = $totalview1;
                    }
                    $resres->videodata = $vdata;
                }
            } else {
                $sqlelse = "select * from video where videoname like '%$term%' order by id asc";
                $qryy = $this->db->query($sqlelse);

                foreach (($qryy->result()) as $keye => $resq) {
                    $term2 = $resq->video_category;
                    $sqltt = "select * from category where category_title like '%$term2%' order by id asc ";
                    $qrytt = $this->db->query($sqltt);
                    foreach (($qrytt->result()) as $keytt => $resres) {
                        $pid = $res->category_parent;
                        $resdata[] = $resres;
                        $catp[] = $resres->category_parent;
                        $vcat = $resres->category_title;
                        $vdatat = $this->db->get_where('video', array('video_category' => $vcat))->result_array();
                        foreach ($vdatat as $keyvt => $rv) {
                            $rv['id'];
                            $this->db->select_sum('view');
                            $this->db->where('video_id', $rv['id']);
                            $qtryrvt = $this->db->get('views')->row_array();
                            $totalview1t = $qtryrvt['view'];
                            $vdatat[$keyvt]['totalv'] = $totalview1t;
                        }
                        $resres->videodata = $vdatat;
                    }
                }
            }
            $catdata[] = $resdata;
            $dataqqq = $catdata[0];

            foreach ($catdata[0] as $key => $subdata) {
                $id = $subdata->category_parent;
                $this->db->select('*');
                $this->db->where('id', $id);
                $this->db->where_not_in('category_parent', $catp);
                $res = $this->db->get('category')->row_array();
                //$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
                $dataqqq[$key]->sub = $res;
                $pcat = $dataqqq[$key]->sub['category_title'];

                $resname = $this->db->get_where('video', array('video_category' => $pcat, 'video_duration <' => $duration, 'video_duration >' => '00:00:00'))->result_array();

                foreach ($resname as $key1 => $resv) {
                    $resv['id'];
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resv['id']);
                    $qtry = $this->db->get('views')->row_array();
                    $totalview = $qtry['view'];
                    $resname[$key1]['totalview'] = $totalview;
                }
                $dataqqq[$key]->sub['videos'] = $resname;
                $id1 = $res['category_parent'];
                $pcat2 = $res['category_title'];

                $res1 = $this->db->get_where('category', array('id' => $id1))->row_array();
                $dataqqq[$key]->sub['child'] = $res1;

                $resname2 = $this->db->get_where('video', array('video_category' => $pcat2, 'video_duration <' => $duration, 'video_duration >' => '00:00:00'))->result_array();
                foreach ($resname2 as $key2 => $resview) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resview['id']);
                    $qtry2 = $this->db->get('views')->row_array();
                    $totalview1 = $qtry2['view'];
                    $resname2[$key2]['totalsubview'] = $totalview1;
                }

                $dataqqq[$key]->sub['videos'] = $resname2;
                $id2 = $res1['category_parent'];
                $pcat3 = $res1['category_title'];
                $res2 = $this->db->get_where('category', array('id' => $id2))->row_array();
                $dataqqq[$key]->sub['child']['child2'] = $res2;

                $resname3 = $this->db->get_where('video', array('video_category' => $pcat3, 'video_duration <' => $duration, 'video_duration >' => '00:00:00'))->result_array();
                foreach ($resname3 as $key3 => $viewresult) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresult['id']);
                    $qtry3 = $this->db->get('views')->row_array();
                    $totalview2 = $qtry3['view'];
                    $resname3[$key3]['totalchildview'] = $totalview2;
                }


                $cato = $dataqqq[$key]->sub['child']['child2']['category_title'];
                $resname0 = $this->db->get_where('video', array('video_category' => $cato, 'video_duration <' => $duration, 'video_duration >' => '00:00:00'))->result_array();
                foreach ($resname0 as $keykk => $viewresultkk) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultkk['id']);
                    $qtrykk = $this->db->get('views')->row_array();
                    $totalview4 = $qtrykk['view'];
                    $resname0[$keykk]['totalchild2view'] = $qtrykk;
                }
                $dataqqq[$key]->sub['child']['child2']['vvv'] = $resname0;


                $last = $dataqqq[$key]->sub['child']['child2']['category_parent'];
                $reslast = $this->db->get_where('category', array('id' => $last))->row_array();
                $dataqqq[$key]->sub['child']['child2']['child3'] = $reslast;


                $lasttitle = $dataqqq[$key]->sub['child']['child2']['child3']['category_title'];
                $resnamelast = $this->db->get_where('video', array('video_category' => $lasttitle, 'video_duration <' => $duration, 'video_duration >' => '00:00:00'))->result_array();
                foreach ($resnamelast as $keykklast => $viewresultlast) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultlast['id']);
                    $qtrylast = $this->db->get('views')->row_array();
                    $totallast = $qtrylast['view'];
                    $resnamelast[$keykklast]['totalchildviewlast'] = $totallast;
                }

                $dataqqq[$key]->sub['child']['child2']['child3']['video'] = $resnamelast;


                $dataqqq[$key]->sub['child']['videos'] = $resname3;
            }
            return $dataqqq;
        }
        if ($par2 == 'long') {

            $duration1 = "01:00:00";
            $duration2 = "03:00:00";

            $sql = "select * from category where category_title like '%$term%' order by id asc ";
            $qry = $this->db->query($sql);
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $key0 => $resres) {
                    $pid = $res->category_parent;
                    $resdata[] = $resres;
                    $vcat = $resres->category_title;
                    $catp[] = $resres->category_parent;
                    $vdata = $this->db->get_where('video', array('video_category' => $vcat, 'video_duration <=' => $duration2, 'video_duration >=' => $duration1))->result_array();
                    // echo $this->db->last_query();die;
                    foreach ($vdata as $keyv => $rv) {
                        $rv['id'];
                        $this->db->select_sum('view');
                        $this->db->where('video_id', $rv['id']);
                        $qtryrv = $this->db->get('views')->row_array();
                        $totalview1 = $qtryrv['view'];
                        $vdata[$keyv]['totalv'] = $totalview1;
                    }
                    $resres->videodata = $vdata;
                }
            } else {
                $sqlelse = "select * from video where videoname like '%$term%' order by id asc";
                $qryy = $this->db->query($sqlelse);

                foreach (($qryy->result()) as $keye => $resq) {
                    $term2 = $resq->video_category;
                    $sqltt = "select * from category where category_title like '%$term2%' order by id asc ";
                    $qrytt = $this->db->query($sqltt);
                    foreach (($qrytt->result()) as $keytt => $resres) {
                        $pid = $res->category_parent;
                        $resdata[] = $resres;
                        $catp[] = $resres->category_parent;
                        $vcat = $resres->category_title;
                        $vdatat = $this->db->get_where('video', array('video_category' => $vcat))->result_array();
                        foreach ($vdatat as $keyvt => $rv) {
                            $rv['id'];
                            $this->db->select_sum('view');
                            $this->db->where('video_id', $rv['id']);
                            $qtryrvt = $this->db->get('views')->row_array();
                            $totalview1t = $qtryrvt['view'];
                            $vdatat[$keyvt]['totalv'] = $totalview1t;
                        }
                        $resres->videodata = $vdatat;
                    }
                }
            }
            $catdata[] = $resdata;
            $dataqqq = $catdata[0];

            foreach ($catdata[0] as $key => $subdata) {
                $id = $subdata->category_parent;
                $this->db->select('*');
                $this->db->where('id', $id);
                $this->db->where_not_in('category_parent', $catp);
                $res = $this->db->get('category')->row_array();
                //$res =  $this->db->get_where('category',array('id'=>$id))->row_array();
                $dataqqq[$key]->sub = $res;
                $pcat = $dataqqq[$key]->sub['category_title'];

                $resname = $this->db->get_where('video', array('video_category' => $pcat, 'video_duration <=' => $duration2, 'video_duration >=' => $duration1))->result_array();

                foreach ($resname as $key1 => $resv) {
                    $resv['id'];
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resv['id']);
                    $qtry = $this->db->get('views')->row_array();
                    $totalview = $qtry['view'];
                    $resname[$key1]['totalview'] = $totalview;
                }
                $dataqqq[$key]->sub['videos'] = $resname;
                $id1 = $res['category_parent'];
                $pcat2 = $res['category_title'];

                $res1 = $this->db->get_where('category', array('id' => $id1))->row_array();
                $dataqqq[$key]->sub['child'] = $res1;

                $resname2 = $this->db->get_where('video', array('video_category' => $pcat2, 'video_duration <=' => $duration2, 'video_duration >=' => $duration1))->result_array();
                foreach ($resname2 as $key2 => $resview) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $resview['id']);
                    $qtry2 = $this->db->get('views')->row_array();
                    $totalview1 = $qtry2['view'];
                    $resname2[$key2]['totalsubview'] = $totalview1;
                }

                $dataqqq[$key]->sub['videos'] = $resname2;
                $id2 = $res1['category_parent'];
                $pcat3 = $res1['category_title'];
                $res2 = $this->db->get_where('category', array('id' => $id2))->row_array();
                $dataqqq[$key]->sub['child']['child2'] = $res2;

                $resname3 = $this->db->get_where('video', array('video_category' => $pcat3, 'video_duration <=' => $duration2, 'video_duration >=' => $duration1))->result_array();
                foreach ($resname3 as $key3 => $viewresult) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresult['id']);
                    $qtry3 = $this->db->get('views')->row_array();
                    $totalview2 = $qtry3['view'];
                    $resname3[$key3]['totalchildview'] = $totalview2;
                }


                $cato = $dataqqq[$key]->sub['child']['child2']['category_title'];
                $resname0 = $this->db->get_where('video', array('video_category' => $cato, 'video_duration <=' => $duration2, 'video_duration >=' => $duration1))->result_array();
                foreach ($resname0 as $keykk => $viewresultkk) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultkk['id']);
                    $qtrykk = $this->db->get('views')->row_array();
                    $totalview4 = $qtrykk['view'];
                    $resname0[$keykk]['totalchild2view'] = $qtrykk;
                }
                $dataqqq[$key]->sub['child']['child2']['vvv'] = $resname0;


                $last = $dataqqq[$key]->sub['child']['child2']['category_parent'];
                $reslast = $this->db->get_where('category', array('id' => $last))->row_array();
                $dataqqq[$key]->sub['child']['child2']['child3'] = $reslast;


                $lasttitle = $dataqqq[$key]->sub['child']['child2']['child3']['category_title'];
                $resnamelast = $this->db->get_where('video', array('video_category' => $lasttitle, 'video_duration <=' => $duration2, 'video_duration >=' => $duration1))->result_array();
                foreach ($resnamelast as $keykklast => $viewresultlast) {
                    $this->db->select_sum('view');
                    $this->db->where('video_id', $viewresultlast['id']);
                    $qtrylast = $this->db->get('views')->row_array();
                    $totallast = $qtrylast['view'];
                    $resnamelast[$keykklast]['totalchildviewlast'] = $totallast;
                }

                $dataqqq[$key]->sub['child']['child2']['child3']['video'] = $resnamelast;


                $dataqqq[$key]->sub['child']['videos'] = $resname3;
            }
            return $dataqqq;
        }
    }

    function getsubcats($cat) {
        $this->db->where('category_parent', $cat);
        $this->db->select('*');
        $q = $this->db->get('category');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return($data);
        }
    }

    function createplay($play) {

        $this->db->insert('playlist', $play);
        $insert_id = $this->db->insert_id();
        return $insert_id;
    }
 public function getplaylistlimit($user,$start) {
     if ($start == '') {
            $start = 0;
        } else {
            $start = $start;
        }

//        $this->db->where('user_id', $user);
//        $this->db->select('*');
//        $this->db->group_by('playlist_name');
//        $q = $this->db->get('playlist');
$cnd = "WHERE `user_id` = $user group by `playlist_name` order by `id` DESC LIMIT " . $start . ",10 ";
        $sql = "SELECT * FROM `playlist` $cnd ";
        $q = $this->db->query($sql);



        if ($q->num_rows() > 0) {
            $i = 0;
            $data = array();


            foreach (($q->result()) as $row) {
                $data[$i] = $row;
                $videos = array();

                $list = $row->playlist_name;

                $this->db->where('user_id', $user);
                $this->db->where('playlist_name', $list);
                $this->db->select('*');
                $qry = $this->db->get('playlist');
                $k = 0;
                foreach (($qry->result()) as $rowdata) {
                    $videos[$k] = $rowdata->video_id;
                    //$row->videos = $videos;
                    $data[$i]->videos = $videos;
                    $k++;
                }

                $i++;
            }

            return $data;
        }
    }
    public function getplaylist($user) {
        $query_n = $this->db->query('SET sql_mode = ""');
        
        $this->db->where('user_id', $user);
        $this->db->select('*');
        $this->db->group_by('playlist_name');
        $q = $this->db->get('playlist');




        if ($q->num_rows() > 0) {
            $i = 0;
            $data = array();


            foreach (($q->result()) as $row) {
                $data[$i] = $row;
                $videos = array();

                $list = $row->playlist_name;

                $this->db->where('user_id', $user);
                $this->db->where('playlist_name', $list);
                $this->db->select('*');
                $qry = $this->db->get('playlist');
                $k = 0;
                foreach (($qry->result()) as $rowdata) {
                    $videos[$k] = $rowdata->video_id;
                    //$row->videos = $videos;
                    $data[$i]->videos = $videos;
                    $k++;
                }

                $i++;
            }

            return $data;
        }
    }

    public function get_autoplay($search_data) {

        $uid = $this->session->userdata('id');
        $sql = "SELECT * FROM `playlist` WHERE `playlist_name` LIKE '$search_data%' and `user_id` = $uid group by `playlist_name`";
        $query = $this->db->query($sql);

        $result = $query->result();
        if ($query->num_rows() > 0) {
            return $result;
        } else {
            $result == '';
            return $result;
        }
    }

    function getvideoByplayId($play, $creator) {
        $play = str_replace("%20", " ", $play);
        $this->db->select('*');
        $this->db->where('user_id', $creator);
        $this->db->where('playlist_name', $play);
         $this->db->group_by('display_order');
        $q = $this->db->get('playlist');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $playvideo[] = $row->video_id;
                $uid = $row->user_id;
                $listname = $row->playlist_name;
                $price = $row->price;
                $this->db->select('status');
                $this->db->where('userId', $uid);
                $this->db->where('playlist_name', $listname);
                $qrypay = $this->db->get('payment_info')->result_array();
                $payment = $qrypay[0]['status'];



                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $useruser = $qry[0]['username'];

                $this->db->select('userLogo');
                $this->db->where('id', $uid);
                $qryuser = $this->db->get('users')->result_array();
                $userlogo = $qryuser[0]['userLogo'];

                $this->db->select('videothumb');
                $this->db->where('id', $row->video_id);
                $qrythumb = $this->db->get('video')->result_array();
                $videothumb[] = $qrythumb[0]['videothumb'];

                $this->db->select('name');
                $this->db->where('id', $row->video_id);
                $qryimg = $this->db->get('video')->result_array();
                $videourl[] = $qryimg[0]['name'];

                $this->db->select('videoname');
                $this->db->where('id', $row->video_id);
                $qryname = $this->db->get('video')->result_array();
                $videoname[] = $qryname[0]['videoname'];

                $this->db->select_sum('view');
                $this->db->where('userId', $row->user_id);
                $this->db->where('playlist_name', $row->playlist_name);
                $qryview = $this->db->get('views')->result_array();
                $videoview = $qryview[0]['view'];


                $this->db->select_sum('totallike');
                $this->db->where('playlist_name', $row->playlist_name);
                $this->db->where('creator', $row->user_id);
                $queryli = $this->db->get('likes')->result_array();
                $totallikes = $queryli[0]['totallike'];

                $this->db->select_sum('Unlike');
                $this->db->where('playlist_name', $row->playlist_name);
                $this->db->where('creator', $row->user_id);
                $qtys = $this->db->get('likes')->result_array();
                $Unlikes = $qtys[0]['Unlike'];


                $playlistname = $row->playlist_name;

                $this->db->select('*');
                $this->db->where('uploaderId', $row->user_id);
                $this->db->where('subcriberId', $this->session->userdata('id'));
                $qsub = $this->db->get('subscribers');
                if ($qsub->num_rows() > 0) {
                    $row->subscriber = "1";
                } else {
                    $row->subscriber = "0";
                }

                $videosubs = $row->subscriber;

                $subuser = $row->user_id;


                $this->db->select('comment,userId');
                $this->db->where('uploader', $row->user_id);
                $this->db->where('playlist', $row->playlist_name);
                $qcom = $this->db->get('comments', 5);
                if ($qcom->num_rows() > 0) {
                    foreach (($qcom->result()) as $rowqcom) {
                        $user = $rowqcom->userId;
                        $this->db->select('username');
                        $this->db->where('id', $user);
                        $qr = $this->db->get('users')->result_array();
                        $username = $qr[0]['username'];
                        $rowqcom->user = $username;

                        $this->db->select('userLogo');
                        $this->db->where('id', $user);
                        $qr2 = $this->db->get('users')->result_array();
                        $userlogo = $qr2[0]['userLogo'];
                        $rowqcom->userlogo = $userlogo;
                        $datacom[] = $rowqcom;
                    }
                }
            }
            $data['price'] = $price;
            $data['pay_state'] = $payment;
            $data['playlistname'] = $listname;
            $data['comments'] = $datacom;
            $data['userId'] = $uid;
            $data['subuser'] = $subuser;
            $data['subs'] = $videosubs;
            $data['videos'] = $playvideo;
            $data['username'] = $useruser;
            $data['userlogo'] = $userlogo;
            $data['thumb'] = $videothumb;
            $data['url'] = $videourl;
            $data['view'] = $videoview;
            $data['totallikes'] = $totallikes;
            $data['Unlikes'] = $Unlikes;
            $data['videoname'] = $videoname;
            return $data;
        }
    }

    public function insertlikeplay($video, $user) {
        $creator = $this->session->userdata('id');
        $this->db->select('totallike,Unlike');
        $this->db->where('user_id', $user);
        $this->db->where('creator', $creator);
        $this->db->where('playlist_name', $video);
        $q = $this->db->get('likes');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $rowlike) {

                $rowlike->totallike;
                $rowlike->Unlike;
            }
            if ($rowlike->Unlike == '1' && $rowlike->totallike == '0') {

                $data = array('totallike' => '1',
                    'Unlike' => 'Unlike-1',
                    'creator' => $creator
                );
                $this->db->where('user_id', $user);
                $this->db->where('playlist_name', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }
                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->Unlike == '0' && $rowlike->totallike == '0') {
                $data = array('totallike' => '1',
                );
                $this->db->where('user_id', $user);
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }
                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;


                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->totallike == '1' && $rowlike->Unlike == '0') {
                $data = array('totallike' => '0',
                );
                $this->db->where('user_id', $user);
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }

                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;


                    echo $likearr = json_encode($likedata);
                }
            }
        } else {
            $insertdata = array('totallike' => 1,
                'user_id' => $user,
                'playlist_name' => $video,
                'creator' => $creator
            );
            if ($this->db->insert('likes', $insertdata)) {
                $this->db->select('totallike');
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                $q = $this->db->get('likes')->result_array();

                foreach ($q as $res) {
                    $ab +=$res['totallike'];
                }
                $this->db->select('Unlike');
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                $qry = $this->db->get('likes')->result_array();

                foreach ($qry as $resu) {
                    $abc +=$resu['Unlike'];
                }

                $likedata['totallike'] = $ab;
                $likedata['Unlike'] = $abc;


                echo $likearr = json_encode($likedata);
            }
        }
    }

    public function insertdislikeplay($video, $user) {
        $creator = $this->session->userdata('id');
        //'creator'=> $creator
        $this->db->select('totallike,Unlike');
        $this->db->where('user_id', $user);
        $this->db->where('creator', $creator);
        $this->db->where('playlist_name', $video);
        $q = $this->db->get('likes');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $rowlike) {

                $rowlike->totallike;
                $rowlike->Unlike;
            }
            if ($rowlike->totallike == '1' && $rowlike->Unlike == '0') {

                $data = array('Unlike' => '1',
                    'totallike' => 'totallike-1'
                );
                $this->db->where('user_id', $user);
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('playlist_name', $video);
                    $this->db->where('creator', $creator);
                    $q = $this->db->get('likes')->result_array();


                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }
                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->totallike == '0' && $rowlike->Unlike == '0') {

                $data = array('Unlike' => '1',
                );
                $this->db->where('user_id', $user);
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('playlist_name', $video);
                    $this->db->where('creator', $creator);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }

                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
            if ($rowlike->Unlike == '1' && $rowlike->totallike == '0') {

                $data = array('Unlike' => 'Unlike-1',
                );
                $this->db->where('user_id', $user);
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                if ($this->db->update('likes', $data)) {
                    $this->db->select('totallike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $q = $this->db->get('likes')->result_array();

                    foreach ($q as $res) {
                        $ab +=$res['totallike'];
                    }
                    $this->db->select('Unlike');
                    $this->db->where('creator', $creator);
                    $this->db->where('playlist_name', $video);
                    $qry = $this->db->get('likes')->result_array();

                    foreach ($qry as $resu) {
                        $abc +=$resu['Unlike'];
                    }

                    $likedata['totallike'] = $ab;
                    $likedata['Unlike'] = $abc;
                    echo $likearr = json_encode($likedata);
                }
            }
        } else {

            $insertdata = array('Unlike' => 1,
                'user_id' => $user,
                'playlist_name' => $video,
                'creator' => $creator
            );
            if ($this->db->insert('likes', $insertdata)) {
                $this->db->select('Unlike');
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                $q = $this->db->get('likes')->result_array();

                foreach ($q as $res) {
                    $ab +=$res['Unlike'];
                }
                $this->db->select('totallike');
                $this->db->where('creator', $creator);
                $this->db->where('playlist_name', $video);
                $qry = $this->db->get('likes')->result_array();

                foreach ($qry as $resu) {
                    $abc +=$resu['totallike'];
                }
                $likedata['totallike'] = $abc;
                $likedata['Unlike'] = $ab;
                echo $likearr = json_encode($likedata);
            }
        }
    }

    public function getplaylistvideos($par1) {
        $this->db->where('playlist_name', $par1);
        $this->db->select('*');
        $this->db->group_by('playlist_name');
        $q = $this->db->get('playlist');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $video = $row->video_id;
                $this->db->where('id', $video);
                $this->db->select('*');
                $qry = $this->db->get('video');

                if ($qry->num_rows() > 0) {
                    foreach (($qry->result()) as $rowdata) {
                        $videoimg = $rowdata->videothumb;
                    }
                }
                $user = $row->user_id;

                $this->db->where('id', $user);
                $this->db->select('*');
                $qry1 = $this->db->get('users');

                if ($qry1->num_rows() > 0) {
                    foreach (($qry1->result()) as $rowdata1) {
                        $videouser = $rowdata1->username;
                    }
                }
                $row->videothumb = $videoimg;
                $row->username = $videouser;

                $this->db->select_sum('view');
                $this->db->where('playlist_name', $par1);
                $this->db->where('userId', $this->session->userdata('id'));
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->videoview = $total1;

                $totalvid[] = $row->video_id;

                $row->totalvideos = count($totalvideos);

                $this->db->where('user_id', $this->session->userdata('id'));
                $this->db->where('playlist_name', $row->playlist_name);
                $this->db->where('video_id !=', "");
                $this->db->select('*');

                $qrysum = $this->db->get('playlist');

                $row->totalvideos = $qrysum->num_rows();
            }
            return $data;
        }
    }

    public function getallplaylists() {
        $uid = $this->session->userdata('id');

        $this->db->where('user_id', $uid);
        $this->db->select('*');
        $this->db->group_by('playlist_name');
        $q = $this->db->get('playlist');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $totalvid = array();
                $data[] = $row;
                $video = $row->video_id;

                $this->db->where('id', $video);
                $this->db->select('*');
                $qry = $this->db->get('video');

                if ($qry->num_rows() > 0) {
                    foreach (($qry->result()) as $rowdata) {
                        $videoimg = $rowdata->videothumb;
                    }
                }
                $this->db->where('user_id', $uid);
                $this->db->where('playlist_name', $row->playlist_name);
                $this->db->where('video_id !=', "");
                $this->db->select('*');

                $qrysum = $this->db->get('playlist');

                $row->totalvideos = $qrysum->num_rows();



                $user = $row->user_id;

                $this->db->where('id', $user);
                $this->db->select('*');
                $qry1 = $this->db->get('users');

                if ($qry1->num_rows() > 0) {
                    foreach (($qry1->result()) as $rowdata1) {
                        $videouser = $rowdata1->username;
                    }
                }
                $row->videothumb = $videoimg;
                $row->username = $videouser;

                $this->db->select_sum('view');
                $this->db->where('playlist_name', $row->playlist_name);
                $this->db->where('userId', $this->session->userdata('id'));
                $qtry = $this->db->get('views')->result_array();
                $total1 = $qtry[0]['view'];
                $row->videoview = $total1;
                $row->results = $q->num_rows();
            }

            return $data;
        }
    }

    function getadvanceresult($cat, $short, $filter) {
        //echo '<pre>';
        $cats1 = '';
        $a = '';
        $expt = explode(',', $cat[0]);

        foreach ($expt as $key => $val) {
            $this->db->where('id', $val);
            $qtry = $this->db->get('category')->result_array();
            foreach ($qtry as $kkey => $rrow) {
                $cats1 .= $rrow['id'] . ',';
                if ($rrow['category_parent'] == '0') {

                    $getcat = $this->getCategoryForParentId($rrow['id']);
                    $a = $rrow['id'] . ',';
                    foreach ($getcat as $key => $kval) {

                        $a .= $key . ',';
                    }

                    $a = $a . $cats1;
                    // print_r($a).'abc';
                    //$cats = $a; 
                    //$cats = rtrim($cats, ',');
                } else {
                    $rrow['id'] . ',';
                    $a = $a . $cats1;
                }
            }

            // print_r($cats1).$a;
            //$cats = rtrim($cats1, ',');
        }

        $catssss = rtrim($a, ',');
        $cats = ltrim($catssss, ',');


        if ($short == "" && $filter == "") {

            //$cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);
            //echo $this->db->last_query();die;
            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'week' && $filter == "") {

            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_week);
                    $this->db->where('Date <=', $end_week);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == 'today') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);

                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == '') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            // $cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {
                    $this->db->where('privacy', 'public');
                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('video_category', $title);

                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == 'long') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;

            $duration1 = "01:00:00";
            $duration2 = "03:00:00";


            //$cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == 'short') {
            $duration = "00:30:00";
            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            ///$cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'week' && $filter == 'today') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);

                    //$this->db->where('Date >=', $start_week);
                    //$this->db->where('Date <=', $end_week);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'week' && $filter == 'short') {

            $duration = "00:30:00";

            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'week' && $filter == 'long') {

            $duration1 = "01:00:00";
            $duration2 = "03:00:00";

            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_week);
                    $this->db->where('Date <=', $end_week);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }




        if ($short == 'month' && $filter == "") {

            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start);
                    $this->db->where('Date <=', $end);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'month' && $filter == 'today') {


            $today = Date('Y-m-d');
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    //$this->db->where('Date >=', $start);
                    //$this->db->where('Date <=', $end);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'month' && $filter == 'short') {

            $duration = "00:30:00";

            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start);
                    $this->db->where('Date <=', $end);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'month' && $filter == 'long') {

            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;
            $duration1 = "01:00:00";
            $duration2 = "03:00:00";



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start);
                    $this->db->where('Date <=', $end);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'year' && $filter == "") {

            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_year);
                    $this->db->where('Date <=', $end_year);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'year' && $filter == 'today') {

            $today = Date('Y-m-d');
            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    //$this->db->where('Date >=', $start_year);
                    //$this->db->where('Date <=', $end_year);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'year' && $filter == 'short') {

            $duration = "00:30:00";

            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'year' && $filter == 'long') {

            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;
            $duration1 = "01:00:00";
            $duration2 = "03:00:00";




            //$cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_year);
                    $this->db->where('Date <=', $end_year);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }
    }

    function getadvanceresult2($cat, $short, $filter) {

        foreach ($cat as $key => $ccat) {
            $vv .= $ccat . ',';
        }
        $ttrim = rtrim($vv, ',');
        $ecp = explode(',', $ttrim);
        $count = count($ecp);
        if ($count == '1') {

            $vvp = $ecp[0];
            $getcat = $this->getCategoryForParentId($vvp);
            // echo '<pre>';
            // print_r($getcat);
            $a = $vvp . ',';
            foreach ($getcat as $key => $kval) {

                // $cat[] =  key;
                $a .= $key . ',';
            }
            $cats = rtrim($a, ',');

            // print_r($cat);die;
            //$cats = join(",",$cat);
        }
        if ($count == '2') {
            $cats = $ecp[1];
        }
        if ($count == '3') {
            $cats = $ecp[2];
        }
        if ($count == '4') {
            $cats = $ecp[3];
        }

        // $cats = $cat[0];

        if ($short == "" && $filter == "") {

            //$cats = $cat[0];
            //$cats = join("','",$cat); 
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'week' && $filter == "") {

            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_week);
                    $this->db->where('Date <=', $end_week);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == 'today') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);

                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == '') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {
                    $this->db->where('privacy', 'public');
                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('video_category', $title);

                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == 'long') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;

            $duration1 = "01:00:00";
            $duration2 = "03:00:00";


            $cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'newest' && $filter == 'short') {
            $duration = "00:30:00";
            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];

            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'week' && $filter == 'today') {

            $today = Date('Y-m-d');
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);

                    //$this->db->where('Date >=', $start_week);
                    //$this->db->where('Date <=', $end_week);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'week' && $filter == 'short') {

            $duration = "00:30:00";

            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'week' && $filter == 'long') {

            $duration1 = "01:00:00";
            $duration2 = "03:00:00";

            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_week);
                    $this->db->where('Date <=', $end_week);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }




        if ($short == 'month' && $filter == "") {

            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start);
                    $this->db->where('Date <=', $end);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'month' && $filter == 'today') {


            $today = Date('Y-m-d');
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    //$this->db->where('Date >=', $start);
                    //$this->db->where('Date <=', $end);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'month' && $filter == 'short') {

            $duration = "00:30:00";

            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start);
                    $this->db->where('Date <=', $end);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'month' && $filter == 'long') {

            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;
            $duration1 = "01:00:00";
            $duration2 = "03:00:00";



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start);
                    $this->db->where('Date <=', $end);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'year' && $filter == "") {

            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_year);
                    $this->db->where('Date <=', $end_year);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'year' && $filter == 'today') {

            $today = Date('Y-m-d');
            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    //$this->db->where('Date >=', $start_year);
                    //$this->db->where('Date <=', $end_year);
                    $this->db->where('Date', $today);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }

        if ($short == 'year' && $filter == 'short') {

            $duration = "00:30:00";

            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;



            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('video_duration <', $duration);
                    $this->db->where('video_duration >', '00:00:00');
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }


        if ($short == 'year' && $filter == 'long') {

            $year = date('Y');
            $start_year = "{$year}-01-01";
            $end_year = "{$year}-12-31";
            $start = date('Y-m-01', strtotime('this month'));
            $end = date('Y-m-t', strtotime('this month'));
            $previous_week = strtotime("-1 week +1 day");
            $start_week = strtotime("last sunday midnight", $previous_week);
            $end_week = strtotime("next saturday", $start_week);
            $start_week = date("Y-m-d", $start_week);
            $end_week = date("Y-m-d", $end_week);
            $start_week . ' ' . $end_week;
            $duration1 = "01:00:00";
            $duration2 = "03:00:00";




            $cats = $cat[0];
            $sql = "SELECT * FROM category WHERE id IN ($cats)";
            $qry = $this->db->query($sql);

            if ($qry->num_rows() > 0) {
                foreach (($qry->result()) as $row) {

                    $title = $row->category_title;
                    $name[] = $title;
                    $this->db->where('privacy', 'public');
                    $this->db->where('video_category', $title);
                    $this->db->where('Date >=', $start_year);
                    $this->db->where('Date <=', $end_year);
                    $this->db->where('video_duration <=', $duration2);
                    $this->db->where('video_duration >=', $duration1);
                    $this->db->select('*');
                    $q = $this->db->get('video');
                    if ($q->num_rows() > 0) {
                        foreach (($q->result()) as $rowvideo) {
                            $data['video'][] = $rowvideo;
                            $videoid = $rowvideo->id;

                            $this->db->select_sum('view');
                            $this->db->where('video_id', $videoid);
                            $qtry = $this->db->get('views')->result_array();
                            $total1 = $qtry[0]['view'];
                            $rowvideo->videoview = $total1;
                        }
                    }
                }
            }

            $data['category'] = $name;
            return $data;
        }
    }

    public function getorderdetail($par1) {
        $this->db->where('userId', $par1);
        $this->db->where('status', 'Success');
        $this->db->select('*');
        $q = $this->db->get('payment_info');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $uploader = $row->uploderId;
                $videoid = $row->videoId;
                $this->db->where('id', $uploader);
                $this->db->select('*');
                $qryuser = $this->db->get('users')->result_array();
                $username = $qryuser[0]['username'];

                $this->db->where('id', $videoid);
                $this->db->select('*');
                $qryvideo = $this->db->get('video')->result_array();
                $videoname = $qryvideo[0]['videoname'];
            }

            $row->username = $username;
            $row->video = $videoname;

            return $data;
        }
    }

    public function getAllPlaylistforterm($term) {
        $query_n = $this->db->query('SET sql_mode = ""');
        
        $this->db->group_by('playlist_name');
        $query = $this->db->select('*')->from('playlist')
                        ->where("playlist_name LIKE '%$term%' and privacy = 'public'")->get();


        $result = $query->result_array();



        foreach ($result as $key => $res) {
            $list[] = $res['playlist_name'];
        }
        foreach ($list as $k1 => $val) {
            $g[$k1]['play'] = $val;
            $this->db->select_sum('view');
            $this->db->where('playlist_name', $val);
            $query = $this->db->get('views')->result_array();
            $total = $query[0]['view'];
            $g[$k1]['views'] = $total;
            $q = $this->db->get_where('playlist', array('playlist_name' => $val))->result_array();
            $g[$k1]['date'] = $q[0]['playdate'];
            $g[$k1]['price'] = $q[0]['price'];
            $i = 0;
            foreach ($q as $k2 => $val2) {

                $r = $this->db->get_where('video', array('id' => $val2['video_id']))->row_array();
                $r2 = $this->db->get_where('users', array('id' => $val2['user_id']))->row_array();
                $g[$k1]['user'] = $r2;
                $g[$k1]['video'][$i] = $r;
                $duration[] = $g[$k1]['video'][$i]['video_duration'];

                $i++;
            }

            $time = $this->AddPlayTime($duration);
            $g[$k1]['duration'] = $time;
        }

        // print_r($g);die;
        return $g;
    }

    public function getAllPlaylistforsearch($parm) {


        $sql = "SELECT * FROM category WHERE id IN ($parm)";
        $qry = $this->db->query($sql);
        //echo $this->db->last_query();die;
        if ($qry->num_rows() > 0) {
            foreach (($qry->result()) as $row) {

                $title = $row->category_title;
                $name[] = $title;
                $this->db->where('privacy', 'public');
                $this->db->where('video_category', $title);
                $this->db->select('*');
                $q = $this->db->get('video');
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $rowvideo) {
                        $data['video'][] = $rowvideo;
                        $videoid = $rowvideo->id;

                        $this->db->select_sum('view');
                        $this->db->where('video_id', $videoid);
                        $qtry = $this->db->get('views')->result_array();
                        $total1 = $qtry[0]['view'];
                        $rowvideo->videoview = $total1;
                    }
                }
            }
        }



        $this->db->group_by('playlist_name');
        $result = $this->db->get_where('playlist', array('privacy' => 'public'))->result_array();
        foreach ($result as $key => $res) {
            $list[] = $res['playlist_name'];
        }
        foreach ($list as $k1 => $val) {
            $g[$k1]['play'] = $val;
            $this->db->select_sum('view');
            $this->db->where('playlist_name', $val);
            $query = $this->db->get('views')->result_array();
            $total = $query[0]['view'];
            $g[$k1]['views'] = $total;
            $q = $this->db->get_where('playlist', array('playlist_name' => $val))->result_array();
            $g[$k1]['date'] = $q[0]['playdate'];
            $g[$k1]['price'] = $q[0]['price'];
            $i = 0;
            foreach ($q as $k2 => $val2) {
                $this->db->where_in('video_category', $name);
                $r = $this->db->get('video')->row_array();

                //$r = $this->db->get_where('video', array('id' => $val2['video_id']))->row_array();
                $r2 = $this->db->get_where('users', array('id' => $val2['user_id']))->row_array();
                $g[$k1]['user'] = $r2;
                $g[$k1]['video'][$i] = $r;
                $duration[] = $g[$k1]['video'][$i]['video_duration'];

                $i++;
            }

            $time = $this->AddPlayTime($duration);
            $g[$k1]['duration'] = $time;
        }


        return $g;
    }

    public function getAllPlaylistlimited() {
        // echo '<pre>';
        $query_n = $this->db->query('SET sql_mode = ""');
        $this->db->order_by('rand()');
        $this->db->limit(8);
        $this->db->group_by('playlist_name');
        $result = $this->db->get_where('playlist', array('privacy' => 'public'))->result_array();
       
       
        foreach ($result as $key => $res) {
            $list[] = $res['playlist_name'];
        
        }
        
        foreach ($list as $k1 => $val) {
            $g[$k1]['play'] = $val;
            $this->db->select_sum('view');
            $this->db->where('playlist_name', $val);
            $query = $this->db->get('views')->result_array();
            $total = $query[0]['view'];
            $g[$k1]['views'] = $total;
            $q = $this->db->get_where('playlist', array('playlist_name' => $val))->result_array();
            
            $g[$k1]['date'] = $q[0]['playdate'];
            $g[$k1]['price'] = $q[0]['price'];
            $g[$k1]['category'] = $this->db->get_where('video', array('id' => $q[0]['video_id']))->row_array();
            $i = 0;
            foreach ($q as $k2 => $val2) {

                $r = $this->db->get_where('video', array('id' => $val2['video_id']))->row_array();
                $r2 = $this->db->get_where('users', array('id' => $val2['user_id']))->row_array();
                $g[$k1]['user'] = $r2;
                $g[$k1]['video'][$i] = $r;
                $duration[] = $g[$k1]['video'][$i]['video_duration'];

                $i++;
            }

            $time = $this->AddPlayTime($duration);
            $g[$k1]['duration'] = $time;
        }
         
        return $g;
    }

    public function getAllPlaylist() {
        // echo '<pre>';
        $this->db->group_by('playlist_name');
        $result = $this->db->get_where('playlist', array('privacy' => 'public'))->result_array();
        foreach ($result as $key => $res) {
            $list[] = $res['playlist_name'];
        }
        foreach ($list as $k1 => $val) {
            $g[$k1]['play'] = $val;
            $this->db->select_sum('view');
            $this->db->where('playlist_name', $val);
            $query = $this->db->get('views')->result_array();
            $total = $query[0]['view'];
            $g[$k1]['views'] = $total;
            $q = $this->db->get_where('playlist', array('playlist_name' => $val))->result_array();
            $g[$k1]['date'] = $q[0]['playdate'];
            $g[$k1]['price'] = $q[0]['price'];
            $i = 0;
            foreach ($q as $k2 => $val2) {

                $r = $this->db->get_where('video', array('id' => $val2['video_id']))->row_array();
                $r2 = $this->db->get_where('users', array('id' => $val2['user_id']))->row_array();
                $g[$k1]['user'] = $r2;
                $g[$k1]['video'][$i] = $r;
                $duration[] = $g[$k1]['video'][$i]['video_duration'];

                $i++;
            }

            $time = $this->AddPlayTime($duration);
            $g[$k1]['duration'] = $time;
        }

        // print_r($g);die;
        return $g;
    }

    function AddPlayTime($times) {

        // loop throught all the times
        foreach ($times as $time) {
            list($hour, $minute, $second) = explode(':', $time);
            $seconds += $hour * 3600;
            $seconds += $minute * 60;
            $seconds += $second;
        }
        $hours = floor($seconds / 3600);
        $seconds -= $hours * 3600;
        $minutes = floor($seconds / 60);
        $seconds -= $minutes * 60;
        if ($seconds < 9) {
            $seconds = "0" . $seconds;
        }
        if ($minutes < 9) {
            $minutes = "0" . $minutes;
        }
        if ($hours < 9) {
            $hours = "0" . $hours;
        }
        return "{$hours}:{$minutes}:{$seconds}";
    }

    function getvideoByplayname($play) {
        $play = str_replace("%20", " ", $play);
        $this->db->select('*');
        $this->db->where('playlist_name', $play);
        $q = $this->db->get('playlist');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
                $playvideo[] = $row->video_id;
                $price = $row->price;
                $uid = $row->user_id;
                $listname = $row->playlist_name;
                  $stu = $row->payment_status;
                $this->db->select('username');
                $this->db->where('id', $uid);
                $qry = $this->db->get('users')->result_array();
                $useruser = $qry[0]['username'];

                $this->db->select('userLogo');
                $this->db->where('id', $uid);
                $qryuser = $this->db->get('users')->result_array();
                $userlogo = $qryuser[0]['userLogo'];

                $this->db->select('videothumb');
                $this->db->where('id', $row->video_id);
                $qrythumb = $this->db->get('video')->result_array();
                $videothumb[] = $qrythumb[0]['videothumb'];

                $this->db->select('name');
                $this->db->where('id', $row->video_id);
                $qryimg = $this->db->get('video')->result_array();
                $videourl[] = $qryimg[0]['name'];

                $this->db->select('videoname');
                $this->db->where('id', $row->video_id);
                $qryname = $this->db->get('video')->result_array();
                $videoname[] = $qryname[0]['videoname'];

                $this->db->select_sum('view');
                $this->db->where('userId', $row->user_id);
                $this->db->where('playlist_name', $row->playlist_name);
                $qryview = $this->db->get('views')->result_array();
                $videoview = $qryview[0]['view'];


                $this->db->select_sum('totallike');
                $this->db->where('playlist_name', $row->playlist_name);
                $this->db->where('creator', $row->user_id);
                $queryli = $this->db->get('likes')->result_array();
                $totallikes = $queryli[0]['totallike'];

                $this->db->select_sum('Unlike');
                $this->db->where('playlist_name', $row->playlist_name);
                $this->db->where('creator', $row->user_id);
                $qtys = $this->db->get('likes')->result_array();
                $Unlikes = $qtys[0]['Unlike'];


                $playlistname = $row->playlist_name;

                $this->db->select('*');
                $this->db->where('uploaderId', $row->user_id);
                $this->db->where('subcriberId', $this->session->userdata('id'));
                $qsub = $this->db->get('subscribers');
                if ($qsub->num_rows() > 0) {
                    $row->subscriber = "1";
                } else {
                    $row->subscriber = "0";
                }

                $videosubs = $row->subscriber;

                $subuser = $row->user_id;


                $this->db->select('comment,userId');
                $this->db->where('uploader', $row->user_id);
                $this->db->where('playlist', $row->playlist_name);
                $qcom = $this->db->get('comments', 5);
                if ($qcom->num_rows() > 0) {
                    foreach (($qcom->result()) as $rowqcom) {
                        $user = $rowqcom->userId;
                        $this->db->select('username');
                        $this->db->where('id', $user);
                        $qr = $this->db->get('users')->result_array();
                        $username = $qr[0]['username'];
                        $rowqcom->user = $username;

                        $this->db->select('userLogo');
                        $this->db->where('id', $user);
                        $qr2 = $this->db->get('users')->result_array();
                        $userlogo = $qr2[0]['userLogo'];
                        $rowqcom->userlogo = $userlogo;
                        $datacom[] = $rowqcom;
                    }
                }
            }
            $data['price'] = $price;
            $data['playlistname'] = $listname;
            $data['comments'] = $datacom;
            $data['userId'] = $uid;
            $data['subuser'] = $subuser;
            $data['subs'] = $videosubs;
            $data['videos'] = $playvideo;
            $data['username'] = $useruser;
            $data['userlogo'] = $userlogo;
            $data['thumb'] = $videothumb;
            $data['url'] = $videourl;
            $data['view'] = $videoview;
            $data['totallikes'] = $totallikes;
            $data['Unlikes'] = $Unlikes;
            $data['videoname'] = $videoname;
                $data['stu'] = $stu;

            return $data;
        }
    }

    public function getAllPlaylistbyname($name, $id) {
        $this->db->order_by('display_order');
        $q = $this->db->get_where('playlist', array('playlist_name' => $name, 'user_id' => $id))->result_array();
        $i = 0;
        foreach ($q as $k2 => $val2) {

            $r = $this->db->get_where('video', array('id' => $val2['video_id']))->row_array();
            $r2 = $this->db->get_where('users', array('id' => $val2['user_id']))->row_array();
            $g['user'] = $r2;
            $g['video'][$i] = $r;
            $i++;
        }
        return $g;
    }

}

?>