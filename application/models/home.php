<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
require_once(APPPATH . 'vendor/autoload.php');



use \DTS\eBaySDK\Constants;
use \DTS\eBaySDK\Trading\Services;
use \DTS\eBaySDK\Trading\Types;
use \DTS\eBaySDK\Trading\Enums;


class home extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -  
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('apimodel');
        $this->load->model('adminmodel');
        $this->load->helper('url');
        $this->load->helper(array('form', 'url'));
        $this->load->helper('html');
        $this->load->library('form_validation');
        $this->load->library('ckeditor');

        //  $this->load->library('configuration');
        //$this->load->library('autoload');
        $this->load->database();
        $this->load->library('session');
        error_reporting(0);

//        if (!$this->session->userdata['admin']) {
//            $this->session->set_flashdata('permission_message', 'log_again');
//            redirect('Admincontroller/index', 'refresh');
//        }
    }

    public function index() {
        $this->load->view('admin/index');
    }

    public function dashbord() {

        $data['succ'] = "Login Successfully";
        $data['Ann'] = "Welcome to Ebay Listing App";
        $this->load->view('admin/header');
        $this->load->view('admin/dashbord', $data);
    }

    public function admin_edit($id = NULL) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');

        $this->form_validation->set_rules('name', $this->lang->line("Name"), 'required|xss_clean');
        $this->form_validation->set_rules('email', $this->lang->line("email"), 'required|valid_email|xss_clean');

        $this->form_validation->set_rules('companyName', $this->lang->line("Company name"), 'required|xss_clean');
        $this->form_validation->set_rules('mobile', $this->lang->line("mobile"), 'required|xss_clean');
        $this->form_validation->set_rules('address', $this->lang->line("address"), 'required|xss_clean');
        $this->form_validation->set_rules('gender', $this->lang->line("gender"), 'required|xss_clean');
        $this->form_validation->set_rules('dob', $this->lang->line("Date Of Birth"), 'required|xss_clean');
        $this->form_validation->set_rules('username', $this->lang->line("username"), 'required|xss_clean');
        if ($this->input->post('update')) {
            if ($_FILES['image1']['name']) {
                $errors = array();
                $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
                $file_name = $random . $_FILES['image1']['name'];
                $file_size = $_FILES['image1']['size'];
                $file_tmp = $_FILES['image1']['tmp_name'];
                $file_type = $_FILES['image1']['type'];

                $file_ext = strtolower(end(explode('.', $_FILES['image1']['name'])));
                $expensions = array("jpeg", "jpg", "png");
                if (in_array($file_ext, $expensions) === false) {

                    $this->session->set_flashdata('permission_message', 'extension not allowed, please choose a JPEG or PNG file.');
                    echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
                }

                move_uploaded_file($file_tmp, "uploads/" . $file_name);
                $data['image1'] = $file_name;
            }


            $name = $this->input->post('name');
            $email = $this->input->post('email');
            $company = $this->input->post('companyName');
            $mobile = $this->input->post('mobile');
            $address = $this->input->post('address');
            $gender = $this->input->post('gender');
            $dob = $this->input->post('dob');
            $username = $this->input->post('username');
            $additional_data = array(
                'name' => $name,
                'email' => $email,
                'company' => $company,
                'mobile' => $mobile,
                'address' => $address,
                'gender' => $gender,
                'dob' => $dob,
                'username' => $username,
                'img' => $data['image1']
            );
        }
        if ($this->form_validation->run() == true && $this->adminmodel->Update_admin($additional_data, $id)) {
            $this->session->set_flashdata('msg', 'User Updated Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));


            $data['admin_data'] = $this->adminmodel->getadmindata($id);
            $data['title'] = "Edit";
            $data['id'] = $id;
            $data['page_title'] = "Edit";
            $this->load->view('admin/header', $data);
            $this->load->view('admin/admin_edit', $data);
        }
    }

    public function change_pwd($id = NULL) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_message('is_natural_no_zero', 'The %s field is required.');

        $this->form_validation->set_rules('oldpass', $this->lang->line("Old Password"), 'required|xss_clean');
        $this->form_validation->set_rules('newpass1', $this->lang->line("New Password"), 'required|matches[newpass2]|xss_clean');

        $this->form_validation->set_rules('newpass2', $this->lang->line("Confirm Password"), 'required|xss_clean');
        $oldpass = md5($this->input->post('oldpass'));
        $newpass1 = md5($this->input->post('newpass1'));
        $newpass2 = md5($this->input->post('newpass2'));
        $additional_data = array(
            'oldpass' => $oldpass,
            'newpass1' => $newpass1,
            'newpass2' => $newpass2,
        );
        if (!($this->adminmodel->chk_pass($additional_data, $id))) {
            $this->session->set_flashdata('err', 'Password Does not Match');
        }


        if ($this->form_validation->run() == true && $this->adminmodel->chk_pass($additional_data, $id) && $this->adminmodel->change_pass($additional_data, $id)) {
            $this->session->set_flashdata('msg', 'Password Changed Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));


            $meta['page_title'] = "change password";
            $data['id'] = $id;
            $data['page_title'] = "change password";
            $this->load->view('admin/header', $meta);
            $this->load->view('admin/change_pwd', $data);
        }
    }

    public function add_product() {
        $meta['email'] = "current";
        $meta['page_title'] = "Add Product";
        $data['page_title'] = "Add Product";
        $data['Eaccounts'] = $this->adminmodel->getEbayAccount();
        $this->load->view('admin/header', $meta);
        $this->load->view('admin/add_item', $data);
    }

    public function add_item() {


        $asin = $this->input->post('asin');
        $account = $this->input->post('ebayac');
        $margin = $this->input->post('pmargin');
        $amount = $this->input->post('pamount');
        $today = date("Y-m-d");

        $data = array(
            'asin_no' => $asin,
            'ebay_account' => $account,
            'profit_margin' => $margin,
            'profit_amount' => $amount,
            'added_date' => $today
        );


        if ($this->adminmodel->insert_item($data)) {
            $this->session->set_flashdata('msg', 'Inserted Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    function uploadData() {
        //$this->csv_model->uploadData();
        if ($this->adminmodel->uploadData()) {
            echo "success";
        }
    }

    public function setting() {
        $meta['setting'] = "current";
        $meta['page_title'] = "Settings";
        $data['page_title'] = "Settings";
        $data['settings'] = $this->adminmodel->getSetting();
        $this->load->view('admin/header', $meta);
        $this->load->view('admin/settings', $data);
    }

    public function edit_setting() {
        if ($this->input->post('add_phone_one')) {
            $address = array('add_phone_one' => $this->input->post('add_phone_one'), 'add_email_one' => $this->input->post('add_email_one'), 'add_one' => $this->input->post('add_one'), 'add_two' => $this->input->post('add_two'), 'add_three' => $this->input->post('add_three'), 'add_four' => $this->input->post('add_four'));
            $_POST['site_address'] = json_encode($address);
        }
        if ($this->input->post('portal_status')) {
            $portal_status = array('portal_status' => $this->input->post('portal_status'));
            $_POST['portal_status'] = json_encode($portal_status);
        }

        if ($this->input->post('s_facebook')) { //site_address
            $address = array('facebook' => $this->input->post('s_facebook'), 'twitter' => $this->input->post('s_twitter'), 'pinterest' => $this->input->post('s_pinterest'), 'google' => $this->input->post('s_google'), 'instagram' => $this->input->post('s_instagram'));
            $_POST['social'] = json_encode($address);
        }

        if ($this->input->post('email_status')) { //emailConfiguration
            $email = array('status' => $this->input->post('email_status'), 'type' => $this->input->post('email_type'), 'smtp' => $this->input->post('email_smtp'), 'port' => $this->input->post('email_port'), 'username' => $this->input->post('email_user'), 'password' => $this->input->post('email_pass'));
            $_POST['emailConfiguration'] = json_encode($email);
        }
        if ($this->input->post('paypal1')) { //payment configration
            $paypal = array('stype' => $this->input->post('paypal1'), 'currency' => $this->input->post('paypal3'), 'email' => $this->input->post('paypal2'));
            $_POST['paypal_standard'] = json_encode($paypal);
        }

        if ($_FILES['image1']['name']) {
            $errors = array();
            $random = substr(number_format(time() * rand(), 0, '', ''), 0, 5);
            $file_name = $random . $_FILES['image1']['name'];
            $file_size = $_FILES['image1']['size'];
            $file_tmp = $_FILES['image1']['tmp_name'];
            $file_type = $_FILES['image1']['type'];

            $file_ext = strtolower(end(explode('.', $_FILES['image1']['name'])));
            $expensions = array("jpeg", "jpg", "png");
            if (in_array($file_ext, $expensions) === false) {

                $this->session->set_flashdata('permission_message', 'extension not allowed, please choose a JPEG or PNG file.');
                echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
            }

            move_uploaded_file($file_tmp, "uploads/" . $file_name);
            $data['image1'] = $file_name;
            $_POST['site_logo'] = $data['image1'];
        }



        $value = $_POST;

        $name = array_keys($value);
        for ($i = 0; $i < count($value); $i++) {
            if (current($value) != '') {
                if ($this->db->query("update settings set value='" . $this->db->clearText(current($value)) . "'            where setting='$name[$i]' ")) {

                    $this->session->set_flashdata('msg', 'Settings Updated Successfully');
                    echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
                }
            }
            next($value);
        }
    }

    public function show_ebayaccount() {
        $meta['eaccount'] = "current";
        $meta['page_title'] = "Ebay Accounts";
        $data['page_title'] = "Ebay Accounts";
        $data['Eaccounts'] = $this->adminmodel->getEbayAccount();
        $this->load->view('admin/header', $meta);
        //$this->load->view('admin/page', $data);
        $this->load->view('admin/EbayAccounts', $data);
    }

    public function data() {

        if ($_POST['page']) {

            $page = $_POST['page'];
            $search = $_POST['search'];
            @$author = $_REQUEST['author'];
            $cur_page = $page;
            $page -= 1;
            $per_page = 10;

            $previous_btn = true;
            $next_btn = true;
            $first_btn = true;
            $last_btn = true;
            $start = $page * $per_page;

            if ($search == 'undefined') {
                $this->db->select('*');
                $result_product1 = $this->db->get('items', $per_page, $start);
            } else {

                $result_product1 = $this->db->select('*')->from('items', $per_page, $start)
                                ->where("asin_no LIKE '%$search%'")->get();
            }

            //$quy_rate1 = "SELECT * FROM ebay_accounts LIMIT $start, $per_page";
            //$result_product1 = $this->db->query('$quy_rate1');
            // echo $this->db->last_query();die;
            ?>

            <table cellpadding="0" cellspacing="0" border="0" class="stdtable" >
                <colgroup>
                    <col class="con1" />
                    <col class="con1" />
                    <col class="con0" />
                </colgroup>
                <thead>
                    <tr>
                        <th class="head1">S.no</th>
                        <th class="head1">Product Image</th>
                        <th class="head1">Product Name</th>
                        <th class="head1">Source Id</th>
                        <th class="head1">Amazon Price</th>
                        <th class="head1">Ebay Price</th>
                        <th class="head1">Status</th>
                        <th class="head1">Ebay Account</th>
                        <th class="head1">Profit Margin</th>
                        <th class="head1">Profit Account</th>
                        <th class="head1">Views</th>
                        <th class="head1">Creation Date</th>
                        <th class="head1">Last Updated</th>
                        <th class="head1">Action</th>
                    </tr>
                </thead>

                <?php
                if ($result_product1->num_rows() > 0) {
                    $i = 1;

                    foreach (($result_product1->result()) as $row4) {
                        ?>


                        <tbody>
                            <tr>
                                <td class="con0"><?= $i; ?></td>
                                <td class="con0"><img src=<?php echo $row4->item_img; ?>></td>
                                <td class="con0"><?php echo $row4->item_name; ?></td>
                                <td class="con0"><?php echo $row4->asin_no; ?></td>
                                <td class="con0"><?php echo $row4->amazon_price; ?></td>
                                <td class="con0"><?php echo $row4->ebay_price; ?></td>
                                <td class="con0"><?php
                                    if ($row4->availability != '') {
                                        echo "In Stock";
                                    } else {
                                        echo "Out Of Stock";
                                    }
                                    ?></td>
                                <td class="con0"><?php echo $row4->ebay_account; ?></td>
                                <td class="con0"><?php echo $row4->profit_margin; ?></td>
                                <td class="con0"><?php echo $row4->profit_amount; ?></td>
                                <td class="con0"><?php echo "0"; ?></td>
                                <td class="con0"><?php echo $row4->added_date; ?></td>
                                <td class="con0"><?php echo $row4->last_update; ?></td>
                                <td class="actions aligncenter"><div class="hidden-sm hidden-xs action-buttons"> 
                                        <a class="blue" href="update_item?id=<?php echo $row4->id; ?>" title="edit record"> <i class="fa fa-pencil fa-2x"></i>  </a>
                                        <a class="red" href="delete_item?id=<?php echo $row4->id; ?>" onclick='javascript:confirmationDelete($(this));return false;'  title="Delete record">&nbsp; <i class="fa fa-recycle fa-2x"></i> </a> </div>
                                </td>

                            </tr>
                        </tbody>
                        <?php
                        $i++;
                    }
                }
                ?>
            </table>

            <div class='data'><?= @$row1['id'] ?></div>

            <?php
            /* --------------------------------------------- */

            $quy_rate = "SELECT COUNT(*) AS count FROM items";

            $result_pag_num = $this->db->query($quy_rate);


            foreach (($result_pag_num->result()) as $row) {
                $row->count;
            }


            $count = $row->count;
            $no_of_paginations = ceil($count / $per_page);

            /* ---------------Calculating the starting and endign values for the loop----------------------------------- */
            if ($cur_page >= 7) {
                $start_loop = $cur_page - 3;
                if ($no_of_paginations > $cur_page + 3)
                    $end_loop = $cur_page + 3;
                else if ($cur_page <= $no_of_paginations && $cur_page > $no_of_paginations - 6) {
                    $start_loop = $no_of_paginations - 6;
                    $end_loop = $no_of_paginations;
                } else {
                    $end_loop = $no_of_paginations;
                }
            } else {
                $start_loop = 1;
                if ($no_of_paginations > 7)
                    $end_loop = 7;
                else
                    $end_loop = $no_of_paginations;
            }
            /* ----------------------------------------------------------------------------------------------------------- */
            @$msg .= "<div class='pagination'><ul>";

// FOR ENABLING THE FIRST BUTTON
            if ($first_btn && $cur_page > 1) {
                $msg .= "<li p='1' class='active'>First</li>";
            } else if ($first_btn) {
                $msg .= "<li p='1' class='inactive'>First</li>";
            }

// FOR ENABLING THE PREVIOUS BUTTON
            if ($previous_btn && $cur_page > 1) {
                $pre = $cur_page - 1;
                $msg .= "<li p='$pre' class='active'>Previous</li>";
            } else if ($previous_btn) {
                $msg .= "<li class='inactive'>Previous</li>";
            }
            for ($i = $start_loop; $i <= $end_loop; $i++) {

                if ($cur_page == $i)
                    $msg .= "<li p='$i' style='color:#fff;background-color:#a12a0a;' class='active'>{$i}</li>";
                else
                    $msg .= "<li p='$i' class='active'>{$i}</li>";
            }

// TO ENABLE THE NEXT BUTTON
            if ($next_btn && $cur_page < $no_of_paginations) {
                $nex = $cur_page + 1;
                $msg .= "<li p='$nex' class='active'>Next</li>";
            } else if ($next_btn) {
                $msg .= "<li class='inactive'>Next</li>";
            }

// TO ENABLE THE END BUTTON
            if ($last_btn && $cur_page < $no_of_paginations) {
                $msg .= "<li p='$no_of_paginations' class='active'>Last</li>";
            } else if ($last_btn) {
                $msg .= "<li p='$no_of_paginations' class='inactive'>Last</li>";
            }
            $goto = "<input type='text' class='goto' size='1' style='top:0; position:absolute; right:37px;'/>&nbsp;&nbsp;<input type='button' id='go_btn' class='go_button' value='Go'/>";
            $total_string = "<span class='total' a='$no_of_paginations'>Page <b>" . $cur_page . "</b> of <b>$no_of_paginations</b></span>";
            $msg = $msg . "</ul>" . $goto . $total_string . "</div>";  // Content for pagination
            echo $msg;
        }
    }

    public function add_acount() {
        $meta['addacount'] = "current";
        $meta['page_title'] = "Add Account";
        $data['page_title'] = "Add Account";

        $this->load->view('admin/header', $meta);
        $this->load->view('admin/add_account', $data);
    }

    public function add_ebayac() {


        $uname = $this->input->post('username');
        $pass = $this->input->post('pwd');


        $data = array();
        for ($i = 0; $i < count($this->input->post('username')); $i++) {
            $data[$i] = array(
                'uname' => $uname[$i],
                'pwd' => $pass[$i],
            );
        }

        if ($this->adminmodel->insert_account($data)) {
            $this->session->set_flashdata('msg', 'Inserted Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function update_Account($id = NULL) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }


        $username = $this->input->post('username');
        $pwd = $this->input->post('pwd');


        $additional_data = array(
            'user' => $username,
            'pass' => $pwd,
        );

        if ($_POST && $this->adminmodel->updateAccount($additional_data, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            echo "<script>window.location='show_ebayaccount'</script>";
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

            $data['Accounts'] = $this->adminmodel->getaccountByID($id);

            $meta['page_title'] = "Edit Account";
            $data['page_title'] = "Edit Account";
            $this->load->view('admin/header', $meta);
            $this->load->view('admin/UpdateAccount', $data);
        }
    }

    public function delete_Account($id = NULL) {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->adminmodel->delete_account($id)) {
            $this->session->set_flashdata('msg', 'Deleted Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function show_items() {
        $meta['showitem'] = "current";
        $meta['page_title'] = "All Products";
        $data['page_title'] = "All Products";
        $data['items'] = $this->adminmodel->getitems();
        $this->load->view('admin/header', $meta);
        $this->load->view('admin/show_items', $data);
    }

    public function delete_item($id = NULL) {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->adminmodel->delete_item($id)) {
            $this->session->set_flashdata('msg', 'Deleted Successfully');
            echo "<script>window.location='$_SERVER[HTTP_REFERER]'</script>";
        }
    }

    public function update_item($id = NULL) {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }


        $asin = $this->input->post('asin');
        $account = $this->input->post('ebayac');
        $margin = $this->input->post('pmargin');
        $amount = $this->input->post('pamount');
        $today = date("Y-m-d");

        $additional_data = array(
            'asin_no' => $asin,
            'ebay_account' => $account,
            'profit_margin' => $margin,
            'profit_amount' => $amount,
            'added_date' => $today
        );

        if ($_POST && $this->adminmodel->updateItem($additional_data, $id)) {
            $this->session->set_flashdata('msg', 'Updated Successfully');
            redirect('home/show_items', refresh);
        } else {

            $data['message'] = (validation_errors() ? validation_errors() : $this->session->flashdata('message'));

            $data['items'] = $this->adminmodel->getitemByID($id);
            $data['Eaccounts'] = $this->adminmodel->getEbayAccount();
            $meta['page_title'] = "Edit Item";
            $data['page_title'] = "Edit Item";
            $this->load->view('admin/header', $meta);
            $this->load->view('admin/update_item', $data);
        }
    }

    public function demo() {
        $this->load->view('admin/api_class', $meta);
        $this->load->view('admin/test', $data);
    }

    public function get_items() {
        $meta['getitems'] = "current";
        $meta['page_title'] = "Get Items";
        $data['page_title'] = "Get Items";
        $this->load->view('admin/header', $meta);
        $this->load->view('admin/get_items', $data);
    }

    function buildURLArray($filterarray) {
        global $urlfilter;
        global $i;
        $i = '0';
        foreach ($filterarray as $itemfilter) {

            foreach ($itemfilter as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $j => $content) {
                        $urlfilter .= "&itemFilter($i).$key($j)=$content";
                    }
                } else {
                    if ($value != "") {
                        $urlfilter .= "&itemFilter($i).$key=$value";
                    }
                }
            }
            $i++;
        }
        return "$urlfilter";
    }

    public function get_useritem() {

        $seller = $_POST["username"];
        $endpoint = 'http://svcs.ebay.com/services/search/FindingService/v1';
        $version = '1.0.0';
        $appid = 'shuklaga-onlinesh-PRD-a45f64428-c8f7dbc8';
        $globalid = 'EBAY-US';
        $query = '';
        $safequery = urlencode($query);
        $i = '0';

        $this->db->select('pagestatus');
        $this->db->order_by('id', desc);
        $qrypage = $this->db->get('items', 1)->result_array();

        $qrypageno = $qrypage[0]['pagestatus'];
        $pgno = explode(',', $qrypageno);

        if ($pgno[1] == '') {
            $st = '1';
        } else {
            $st = $pgno[1] + '1';
        }

        if ($seller != "") {
            $filterarray = array(
                array(
                    'name' => 'Seller',
                    'value' => "$seller",
                    'paramName' => '',
                    'paramValue' => ''),
            );

            $urlfilter = $this->buildURLArray($filterarray);
        }


        $apicall = "$endpoint?";
        $apicall .= "OPERATION-NAME=findItemsAdvanced";
        $apicall .= "&SERVICE-VERSION=$version";
        $apicall .= "&SECURITY-APPNAME=$appid";
        $apicall .= "&GLOBAL-ID=$globalid";
        $apicall .= "&keywords=$safequery";


        $apicall .= "&paginationInput.entriesPerPage=10";
        $apicall .= "&paginationInput.pageNumber= $st";
        $apicall .= "&SetUserPreferences";
        $apicall .= "&ShowOutOfStockControlPreference=true";
////        $apicall .= "&paginationInput.totalPages=2";
//        $apicall .= "&paginationInput.totalEntries=500";
//        $apicall .= "&paginationOutput.pageNumber=10";
//        $apicall .= "&paginationOutput.entriesPerPage=10";
//        $apicall .= "&paginationOutput.totalPages=10";
//        $apicall .= "&paginationOutput.totalEntries=10";
        if ($seller != "") {
            $apicall .= "$urlfilter";
        }
        $apicall;





        $resp = simplexml_load_file($apicall);
        //$table_row_count = $this->db->count_all('items');



        if ($resp->ack == "Success") {
            $results = '';
            $k = 0;
            foreach ($resp->searchResult->item as $item) {
                $ebayid = $item->itemId;
                $ebaytitle = $item->title;
                $ebayimg = $item->galleryURL;
                $ebayprice = $item->sellingStatus->currentPrice;
                $endpoint2 = 'http://open.api.ebay.com/shopping';
                $apicall2 = "$endpoint2?";
                $apicall2 .= "callname=GetSingleItem";
                $apicall2 .= "&responseencoding=XML";
                $apicall2 .= "&appid=$appid";

                $apicall2 .= "&siteid=0";
                $apicall2 .= "&version=967";
                $apicall2 .= "&ItemID=$ebayid";
                $apicall2 .= "&keywords=$safequery";
                $apicall2 .= "&IncludeSelector=Details,Description,ItemSpecifics";

                $response = simplexml_load_file($apicall2);
                $today = date("Y-m-d");
                $asin = $response->Item->SKU;
                $this->db->select('asin_no');

                $qryasin = $this->db->get('items');
                foreach (($qryasin->result()) as $rowas) {
                    $qryasinno[] = $rowas->asin_no;
                }


                if (!in_array($asin, $qryasinno)) {
                    $datainsert = array(
                        'ebay_id' => $ebayid,
                        'item_name' => "$ebaytitle",
                        'item_img' => "$ebayimg",
                        'ebay_price' => "$ebayprice",
                        'asin_no' => "$asin",
                        'added_date' => $today
                    );

                    $resultqry = $this->db->insert('items', $datainsert);
                } else {
                    $dataupdate = array(
                        'ebay_id' => $ebayid,
                        'item_name' => "$ebaytitle",
                        'item_img' => "$ebayimg",
                        'ebay_price' => "$ebayprice",
                        'added_date' => $today
                    );

                    $this->db->where('asin_no', "$asin");
                    $this->db->update('items', $dataupdate);
                }

                //$pic = $item->galleryURL;
                //$link = $item->viewItemURL;
                //$title = $item->title;
                //echo $k;
                //$results .= "<tr><td><img src=\"$pic\"></td><td><a href=\"$link\">$title</a></td></tr>";
                $k++;
            }

            $this->db->select('id');
            $this->db->order_by('id', desc);
            $qryid = $this->db->get('items', 1)->result_array();
            $qryid = $qryid[0]['id'];

            // $insert_id = $this->db->insert_id();
            $status = "stop" . "," . $st;

            $page_data = array('pagestatus' => $status);
            $this->db->where('id', $qryid);
            $this->db->update('items', $page_data);

            redirect('home/show_items', refresh);
        }
    }

    public function display() {
        $meta['showitem'] = "current";
        $meta['page_title'] = "All Products";
        $data['page_title'] = "All Products";
        $data['items'] = $this->adminmodel->getitems();
        $this->load->view('admin/header', $meta);
        $this->load->view('admin/show_items', $data);
    }
 public function sdk1() {
      $this->load->view('sdk/revise_inventory');
      }
      
    public function sdk() {
      //echo "tgr";
        $config = require_once APPPATH . "configuration.php";
//print_r($config);
        $quantity1 = 25;
        $price1 = 100.44;
        $id = '302283062156';
        $itemID = $id;


//        $quantity1 = $_POST['qty'];
//        $price1 = $_POST['price'];
//        $id = $_POST['id'];
//        $itemID = $id;
// The new quantity (Must be an integer and not a string!).

        $int = (int) $quantity1;
        $float = (float) $price1;
        $quantity = $int;

        $siteId = Constants\SiteIds::US;

        $service = new Services\TradingService([
            'apiVersion' => $config['tradingApiVersion'],
            'credentials' => $config['sandbox']['credentials'],
            'authToken' => $config['sandbox']['authToken'],
            'sandbox' => FALSE,
            'siteId' => $siteId
        ]);

        $request = new Types\ReviseItemRequestType();
        $item = new Types\ItemType();

        $ebay_amount = $item->StartPrice = new Types\AmountType(['value' => $float]);
        $item->ItemID = $itemID;
        $item->Quantity = $quantity;
        $request->Item = $item;


        $request->ErrorLanguage = 'en_US';
        $request->WarningLevel = 'High';
       echo '<pre>';
      // print_r($item);
              
//         $today = date("Y-m-d H:i:s");
//        $intesrtdata = array(
//            'sku' => "$item->SKU",
//            'asin_no' => "$itemID",
//            'item_name' => "$item->Title",
//            'last_update' => $today
//        );
//     
//        print_r($intesrtdata);
        //$this->db->insert('tblitemupdate', $intesrtdata);
        $response = $service->reviseItem($request);
echo '<pre>';
        print_r($response);
    
        if (isset($response->Errors)) {
           echo "yes";
            foreach ($response->Errors as $error) {
                printf("%s: %s\n%s\n\n", $error->SeverityCode === Enums\SeverityCodeType::C_ERROR ? 'Error' : 'Warning', $error->ShortMessage, $error->LongMessage
                );
            }
        }

        if ($response->Ack !== 'Failure') {
             echo "no";
            printf("Quantity for [%s] has been updated\n\n", $itemID);
            printf("Price for [%s] has been updated\n\n", $itemID);
        }
//            $this->load->view('sdk/autoload.php');
//            $this->load->view('sdk/configuration.php');
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */