<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class PagModel extends CI_Model
{
    public function __construct() {
       parent::__construct();
    }
    public function total_count() {
       return $this->db->count_all("video");
    }
    public function get_users($limit, $start) {
      $this->db->limit($limit, $start);
      $query = $this->db->get("video");
      if ($query->num_rows() > 0) {
        return $query->result_array();
      }
      return false;
   }
}