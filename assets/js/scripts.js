(function() {
    "use strict";

    // custom scrollbar

    $("html").niceScroll({styler:"fb",cursorcolor:"#F2B33F", cursorwidth: '6', cursorborderradius: '10px', background: '#424f63', spacebarenabled:false, cursorborder: '0',  zindex: '1000'});

    $(".scrollbar1").niceScroll({styler:"fb",cursorcolor:"rgba(97, 100, 193, 0.78)", cursorwidth: '6', cursorborderradius: '0',autohidemode: 'false', background: '#F1F1F1', spacebarenabled:false, cursorborder: '0'});

	
	
    $(".scrollbar1").getNiceScroll();
    if ($('body').hasClass('scrollbar1-collapsed')) {
        $(".scrollbar1").getNiceScroll().hide();
    }

})(jQuery);

                     
   $('#menu').metisMenu({

// auto collapse. 
toggle: true,

// double tap to go
doubleTapToGo: false,

// prevents or allows dropdowns' onclick events after expanding/collapsing.
preventDefault: true,

// CSS classes
activeClass: 'active',
collapseClass: 'collapse',
collapseInClass: 'in',
collapsingClass: 'collapsing',

// callbacks
onTransitionStart: false,
onTransitionEnd: false

});
  